# -*- coding: utf-8 -*-

"""The application's Globals object"""

__all__ = ['Globals']
import os
import urllib3

class Globals(object):
    """Container for objects available throughout the life of the application.

    One instance of Globals is created during application initialization and
    is available during requests via the 'app_globals' variable.

    Insert this at ~./bashrc

    export JUPITER_HOST=jupiter.dudewhereismy.mx
    export JUPITER_PORT=8092
    export JUPITER_SECURE=True
    export JUPITER_DIR=/home/wsgi/public_wsgi/python.jupiter


    """

    def __init__(self):
        """Do nothing, by default."""
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        # Server parameters

        self.host_secure = os.getenv('JUPITER_SECURE')
        if self.host_secure is None:
            self.host_secure="False"

        if self.host_secure=="False":
            host_prefix="http://"
        else:
            host_prefix="https://"

        host_port=os.getenv('JUPITER_PORT')
        if host_port is None:
            host_port=""

        if len(host_port)>0:
            self.host_port=":"+host_port
        else:
            self.host_port=""

        self.host=os.getenv('JUPITER_HOST')
        if self.host is None:
            self.host=host_prefix+"localhost"
        else:
            self.host=host_prefix+self.host

        self.app_dir = os.getenv('JUPITER_DIR')
        if self.app_dir is None:
            self.app_dir=os.getcwd()

        # STOMP parameters
        self.stomp_secure=os.getenv('STOMP_SECURE')
        if self.stomp_secure is None:
            self.stomp_secure="True"

        if self.stomp_secure=="False":
            stomp_prefix="ws://"
        else:
            stomp_prefix="wss://"

        stomp_port=os.getenv('STOMP_PORT')
        if stomp_port is None:
            stomp_port=""
        if len(stomp_port)>1:
            self.stomp_port=":"+stomp_port
        else:
            self.stomp_port=""

        self.stompHost = os.getenv('STOMP_HOST')
        if self.stompHost is None:
            self.stompHost = "stomp.dudewhereismy.mx"

        self.stompUserName = os.getenv('STOMP_MASTER_USER')
        if self.stompUserName is None:
            self.stompUserName = "dwim"

        self.stompPassword = os.getenv('STOMP_MASTER_PASS')
        if self.stompPassword  is None:
            self.stompPassword = "gpscontrol1"

        self.stompServer=stomp_prefix+self.stompHost+':15671'+'/ws'
        self.domainsun='https://sun.dudewhereismy.mx/services/applications?'
        self.domaincygnus='https://cygnus.dudewhereismy.mx/advance/ecos?'
