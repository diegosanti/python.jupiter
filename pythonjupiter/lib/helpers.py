# -*- coding: utf-8 -*-
"""Template Helpers used in python.jupiter."""
import logging
from markupsafe import Markup
from datetime import datetime
from tg import request
from tg import app_globals
from pythonjupiter.model.auth import DBSession,User
#from pythonjupiter.model.tables import Notification
log = logging.getLogger(__name__)


def current_year():
    now = datetime.now()
    return now.strftime('%Y')


def icon(icon_name):
    return Markup('<i class="glyphicon glyphicon-%s"></i>' % icon_name)


# Import commonly used helpers from WebHelpers2 and TG
from tg.util.html import script_json_encode

try:
    from webhelpers2 import date, html, number, misc, text
except SyntaxError:
    log.error("WebHelpers2 helpers not available with this Python Version")

def badge():
    counter=0
    if request.identity is None:
        counter = 0
    else:
        try:
            identity=""
            identity = request.identity['repoze.who.userid']
        except:
            identity = ""
        if identity is None:
            identity = ""
        user = DBSession.query(User).filter_by(user_name=identity).first()
        #counter = DBSession.query(Notification).filter_by(user_id=user.user_id, attended_state=u"N").count()
    return '' if counter == 0 else str(counter)

def url():
    return "https://jupiter.dudewhereismy.mx"


def urlsun():
    return "https://sun.dudewhereismy.mx"

def whoami():
    try:
        ret=""
        ret=request.identity["repoze.who.userid"]
    except:
        return ""
    else:
        if ret is None:
            ret=""
        return ret

def stompServer():
    return app_globals.stompServer

def stompUser():
    return app_globals.stompUserName

def stompPassword():
    return app_globals.stompPassword