from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
#from pythonjupiter.model.tables import Permisionario
from tg import render_template
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.catalog import CatalogController
from pythonjupiter.controllers.error import ErrorController
from pythonjupiter.model.tables import DatosUnidad, TipoUnidad,TipoTarjeta,Linea,HistorialUnidades,Derrotero,Aseguradora
from pythonjupiter.model.tables import Motivo,Periodo,Punto,TipoLicencia,Grupo
# from pythonjupiter.controllers.operadores import OperadoresTemplates
import os
from datetime import datetime
import requests
from tg import app_globals
from pythonjupiter.lib.services import Services
from pythonjupiter.controllers.lineas import LineasTemplates
from pythonjupiter.controllers.unidades import UnidadesTemplates
from pythonjupiter.controllers.reportes import ReportesTemplates
from pythonjupiter.controllers.personas import PersonasTemplates
from base64 import b64encode
from shutil import copyfileobj,copyfile
from pythonjupiter.lib.messagequeue import Message
from tg import expose, render_template
from pythonjupiter.lib.utility import ExportPDF

class TestClass(BaseController):
    @expose('pythonjupiter.templates.tests.messages')
    def messages(self, **kw):
        print(kw['user'])

        return dict(user=kw['user'])


    @expose('json')
    def receiveMessage(self, **kw):
        userName = kw['user']
        Message.post("testlistener_" + userName, 'MSG_' + kw['color'] + "|" + "Hola xD")
        return dict(dummy="")

    @expose('pythonjupiter.templates.tests.pdf')
    def pdf(self):
        list=[]
        file_name=ExportPDF.create({"parameter1": 'Diego'},"pdftest","pythonjupiter.templates.tests.pdfexample",'yo')
        print("fn:{}".format(file_name))
        return dict(file_name=file_name)