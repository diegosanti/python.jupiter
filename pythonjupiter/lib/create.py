from tg.configuration import config
from sqlalchemy import create_engine, text
engine = create_engine(config['sqlalchemy.url'])
from pythonjupiter.model import DBSession
from pythonjupiter.lib.views import Views


class Create(Views):

    def __init__(self,currentmodel,key,filter,kwargs):
        Views.__init__(self,currentmodel,key,kwargs,filter)
        self.filter = filter
        self.kw = kwargs

    def formFieldSet(self,save):
        #html = self.function()
        html = self.fieldset_open(save)
        html += self.table_open()
        i=0
        contador=0
        for c in self.model.__table__.columns:
            print("IMPRIMIENDO C")
            if(i==0):
                html += self.input(c.name, c.type,contador)
                i=i+1
                contador=contador+1
            elif(c.name!='internal_id'):
                html += self.input(c.name,c.type,contador)
                contador=contador+1
        html += self.table_close()
        html +=  self.fieldset_close()
        return html

    def form(self,save):
        html = self.form_open(save)
        html += self.table_open()
        contador=0
        for c in self.model.__table__.columns:
            if(c.name == 'fecha_ingreso'):
                print("-")
                contador=contador+1
            else:
                html += self.input(c.name,c.type,contador)
                contador = contador + 1
        html += self.table_close()
        html += self.button()
        return html

    def grid(self):
        pass