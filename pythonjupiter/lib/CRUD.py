from tg.configuration import config
from sqlalchemy import asc, desc, text
from sqlalchemy import create_engine, Table, MetaData, Column, Integer,select
from pythonjupiter.model.tables import DatosUnidad
engine = create_engine(config['sqlalchemy.url'])
from pythonjupiter.model import DBSession, Dictionary, DeclarativeBase
from pythonjupiter.model import tables
import sys
import inspect
from tg import app_globals
import requests


import json

class Crud(object):

    def __init__(self, currentmodel,key,filter,kwargs):
        self.model = currentmodel
        self.indexkey = key
        self.filter = filter
        self.kw = kwargs

    def getapp_idsun(self,app_name):
        domain = app_globals.domainsun+"internal_id="+self.kw['internal_id']
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = ''

        for item in empresa:
            if item['application_name'] == app_name:
                id=item['application_id']
        return id

    def read(self):
        my_filters = {self.indexkey: self.kw[self.indexkey]}
        query = DBSession.query(self.model)
        for attr, value in my_filters.items():
            query = query.filter(getattr(self.model, attr) == value)
        window = query.all()
        records = []
        fields = []
        for rw in window:
            for column in rw.__table__.columns:
                #print(column.name)
                fields.append({column.name:getattr(rw, column.name)})
            records.append({'row': fields})
            fields = []
        return dict(rows=records)

    #error = Crud(Contact,'contact_id',filter,kw).update()
    def update(self):
        if self.kw['op'] == "edit":
            my_filters = {self.indexkey: self.kw[self.indexkey]}
            query = DBSession.query(self.model)
            for attr, value in my_filters.items():
                query = query.filter(getattr(self.model, attr) == value)
            item=query.first()
            i=0
            if item is not None:
                for column in item.__table__.columns:
                    if column.name!=self.indexkey:
                        if column.name in self.kw:
                            if str(column.type) == "BOOLEAN":
                                newvalue = True if self.kw[column.name]=="True" else False
                            else:
                                newvalue =self.kw[column.name]

                                if i==1:
                                    if getattr(item,column.name)!=newvalue:
                                        queryverifica=DBSession.query(self.model).filter(getattr(self.model,column.name)==newvalue).first()
                                        if queryverifica is None:
                                            pass
                                        else:
                                            return dict(error="nodisponible")
                                else:
                                    pass

                            setattr(item,column.name,newvalue)

                    i=i+1
                DBSession.flush()
            return dict(error="disponible")

        # if self.kw['op'] == "add":
        #     item = self.model()
        #
        #     for column in item.__table__.columns:
        #         i = 1
        #         if column.name in self.kw:
        #             if (self.indexkey==column.name):
        #                 pass
        #             else:
        #                 handler = DBSession.query(Dictionary).filter_by(database=str(column.name)).first()
        #                 if handler is not None:
        #                     if (handler.filter_multioptions == True):
        #                         lista = []
        #                         for it in self.kw[column.name]:
        #                             metadata = MetaData(engine)
        #                             table = metadata.tables[handler.filter_relation2]
        #                             for _ in DBSession.query(table).all():
        #                                 print(_.name)
        #
        #
        #
        #                             #han = DBSession.query(Type).filter_by(type_id=str(it)).first()
        #                             # sql = "SELECT * FROM " + handler.relation + " WHERE " + handler.filter_relation + " = " + str(it)
        #                             # query = text(sql)
        #                             # result = engine.execute(query)
        #                             # data = result.fetchall()
        #                             # for item in data:
        #                             #     print(item)
        #                             #     lista.append(item)
        #                         print(lista)
        #                             # ftr = handler.filter_relation + " == " + it
        #                             # hn = DBSession.query(handler.filter_relation2).filter(str(handler.filter_relation)==it).all()
        #                             # print(hn)
        #                         i = 0
        #                 if i == 1:
        #                     setattr(item, column.name, self.kw[column.name])
        #
        #     DBSession.add(item)
        #     DBSession.flush()

        if self.kw['op'] == "add":
            item = self.model()
            #handler = DBSession.query(Dictionary).filter_by(database=str(name)).first()
            try:
                app_name=self.kw['app_name']
            except KeyError:
                app_name=''

            i=0
            sema=1
            for column in item.__table__.columns:
                print("***********DENTRO FOR************")

                if column.name in self.kw:

                    if (self.indexkey==column.name):
                        print("selfindexkey==columname")
                        pass
                    else:
                        if(column.name=='application_id'):

                            setattr(item, column.name, self.getapp_idsun(app_name))

                        else:

                            if i==1:
                                queryexiste=DBSession.query(self.model).filter(getattr(self.model, column.name)==self.kw[column.name]).first()
                                if queryexiste is None:
                                    print("NO EXISTE NADIE")
                                    setattr(item, column.name, self.kw[column.name])
                                else:
                                    sema=0
                                    break
                            else:
                                setattr(item, column.name, self.kw[column.name])

                i=i+1
                print("*************************")
            print("voy a agregar")
            if sema==1:
                DBSession.add(item)
                DBSession.flush()
                print("agregue")
                return dict(error="agregue")
            else:
                print("NO AGREGUE")
                return dict(error="no agregue")

        if self.kw['op'] == "del":
            my_filters = {self.indexkey: self.kw[self.indexkey]}
            #index=my_filters.self.indexkey
            print("MY FILTERS: ",my_filters)
            query = DBSession.query(self.model)
            queryunidadexiste=DBSession.query(DatosUnidad)
            for attr, value in my_filters.items():
                query = query.filter(getattr(self.model, attr) == value)
                current_module = sys.modules[tables.__name__]
                for name, obj in inspect.getmembers(current_module):
                    try:
                        if inspect.isclass(obj):
                            newname='id_'+name.lower()
                            if(newname==attr):
                                continue
                            else:
                                queryunidadexiste = query.filter(getattr(obj, attr) == value)
                                break

                    except AttributeError:
                        queryunidadexiste=''
                        continue
            item=query.first()
            try:
                existe=queryunidadexiste.first()
            except AttributeError:
                existe='Nadie'
            print(existe)
            if item is not None:
                if existe is None:
                    DBSession.delete(item)
                    DBSession.flush()
                    return dict(error="ok")
                elif existe == 'Nadie':
                    DBSession.delete(item)
                    DBSession.flush()
                    return dict(error="ok")
                else:
                    return dict(error="Usando no se puede eliminar")

            else:
                return dict(error="No existe")

            return dict(error="ok")