from tg.configuration import config
from sqlalchemy import create_engine, text
engine = create_engine(config['sqlalchemy.url'])
from pythonjupiter.model import DBSession
from pythonjupiter.model.dictionary import Dictionary
from tg import app_globals
import requests
#from pythonpluton.lib.CRUD import Crud

class Views(object):

    def __init__(self,currentmodel,key,kwargs,filter):
        #Crud.__init__(self,currentmodel,key,filter,kwargs)
        self.model = currentmodel
        self.indexkey = key
        self.filter = filter
        self.kw = kwargs
        self.title = currentmodel.__table__.name
        self.op = kwargs['op']
        self.relation = ""
        self.filter_relation = ""
        self.filter_option = ""
        self.real = ""

    def form_open(self,save):
        #html = self.function_save()
        # domain = app_globals.domainsun
        # data=[]
        # try:
        #     content = requests.get(domain, auth=("manager", "managepass"))
        #     content = content.json()
        #     empresa = content['applications']
        # except ValueError:
        #     empresa = ''
        # for item in empresa:
        #     data.append({'name': item['application_name'], 'app_id': item['application_id']})
        #html ='<script>function cambiaaplicationid(){var elegido=document.getElementById("app_name").value; alert(elegido);}</script>'
        html = "<form name="+self.title+" id="+self.title+" action='"+save+"' target=\"iframethis\" accept-charset=\"UTF-8\">"
        html += "<input type='text' name='op' value='"+self.op+"' hidden>"
        html += "<input type='text' name='action' value='new' hidden>"
        html += "<input type='text' name='perm' value='"+self.kw['perm']+"' hidden>"
        html += "<input type='text' id='application_id' name='application_id' value='"+self.kw['application_id']+"' hidden>"
        html += "<input type='text' name='internal_id' value='" + self.kw['internal_id'] + "' hidden>"
        html += "<input type='text' name='user' value='" + self.kw['user'] + "' hidden>"
        #html += "<form id="+self.title+" action='"+save+"' onsubmit='return save();'>"
        return html

    def form_close(self):
        html = "</form><br>"
        html+="<iframe id=\"iframethis\" name=\"iframethis\" frameborder=\"0\"></iframe>"
        return html

    def fieldset_open(self,save):
        html = self.form_open(save)
        html += "<fieldset>"
        html += "<legend>"+self.op.title()+" "+self.title.title()+"</legend>"
        return html

    def fieldset_close(self):
        html = "<input type='submit' value='Guardar'>&nbsp;"
        #html += "<button onclick='window.history.back();'>Cancelar</button>&nbsp;"
        #html = "<button onclick=save()>SUBMIT</button>"
        html += "</fieldset>"
        html += self.form_close()
        return html

    def button(self):
        html = "<input type='submit' value='Guardar'>&nbsp;"
        html += "<button onclick='window.history.back();'>Cancelar</button>&nbsp;"
        html += self.form_close()
        return html

    def table_open(self):
        html = "<table>"
        return html

    def table_close(self):
        html = "</table>"
        return html

    def type(self,name,type,value,required,lenght):
        type = str(type)
        print(name)
        print(type)
        for char in ' ()1234567890,':
            type = type.replace(char,'')

        if int(lenght) >= 151:
            type = "TEXTAREA"

        if name == self.indexkey:
            type = "INTEGER"

        print(self.indexkey)
        if(str(name)!=self.indexkey):
            selector = {"INTEGER":"<input type='number' value='"+str(value)+"' id='"+str(name)+"' "+required+" name='"+str(name)+"' min=\"0\" max=\"2145000000\" required></input>",
                    "DATETIME": "<input type='datetime-local'  value='" + str(value) + "'id='" + str(name) + "' "+required+" name='"+str(name)+"'></input>",
                    "BOOLEAN": "<input type='checkbox'  value='" + str(value) + "'id='" + str(name) + "' "+required+" name='"+str(name)+"'></input>",
                    "DATE": "<input type='date'  value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",
                    "NUMERIC": "<input type='number' step='any' value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"' min=\"0\" max=\"2145000000\"></input>",
                    "FLOAT": "<input type='number' step='any' value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",
                    "BIGINT": "<input type='number' value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",
                    "TEXT": "<input type='text' maxlength="+str(lenght)+" value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",
                    "BLOB": "<input type='text' maxlength="+str(lenght)+" value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",
                    "TEXTAREA": "<textarea maxlength="+str(lenght)+" width:'100%' value='" + str(value) + "'id='" + str(name) + "'  "+required+" name='"+str(name)+"'></textarea>",
                    "SELECT": self.select(name,value),
                    "GROUP": self.group(name,value),
                    "VARCHAR": "<input type='text' maxlength="+str(lenght)+" value='" + str(value) + "' id='" + str(name) + "'  "+required+" name='"+str(name)+"'></input>",}

        else:
            selector = {"INTEGER": "<input type='number' value='" + str(value) + "' id='" + str(name) + "' " + required + " name='" + str(name) + "' min=\"0\" max=\"2145000000\" hidden></input>",
                        "DATETIME": "<input type='datetime-local'  value='" + str(value) + "'id='" + str(name) + "' " + required + " name='" + str(name) + "'></input>",
                        "BOOLEAN": "<input type='checkbox'  value='" + str(value) + "'id='" + str(name) + "' " + required + " name='" + str(name) + "'></input>",
                        "DATE": "<input type='date'  value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>",
                        "NUMERIC": "<input type='number' step='any' value='" + str(value) + "'id='" + str( name) + "'  " + required + " name='" + str(name) + "' min=\"0\" max=\"2145000000\"></input>",
                        "FLOAT": "<input type='number' step='any' value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>",
                        "BIGINT": "<input type='number' value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>",
                        "TEXT": "<input type='text' maxlength=" + str(lenght) + " value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>",
                        "BLOB": "<input type='text' maxlength=" + str(lenght) + " value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>",
                        "TEXTAREA": "<textarea maxlength=" + str(lenght) + " width:'100%' value='" + str(value) + "'id='" + str(name) + "'  " + required + " name='" + str(name) + "'></textarea>",
                        "SELECT": self.select(name, value),
                        "GROUP": self.group(name, value),
                        "VARCHAR": "<input type='text' maxlength=" + str(lenght) + " value='" + str(value) + "' id='" + str(name) + "'  " + required + " name='" + str(name) + "'></input>", }



        return selector[str(type)]

    def select(self,name,value):
        html = "<select id=" + str(self.real) + " name='"+str(self.real)+"'>"
        if self.relation != "":
            sql = "SELECT " + name + ", " + self.filter_option + " FROM " + self.relation + " WHERE " + self.filter_relation + " = " + str(
                self.kw[self.filter_relation])
            query = text(sql)
            result = engine.execute(query)
            data = result.fetchall()
            for item in data:
                if item[0] == value:
                    html += "<option value=" + str(item[0]) + " selected>" + str(item[1]) + "</option>"
                else:
                    html += "<option value=" + str(item[0]) + ">" + str(item[1]) + "</option>"
        html += "</select>"
        return html

    def group(self,name,value):
        html = ""
        if self.relation != "":
            sql = "SELECT "+ name +", "+ self.filter_option +" FROM " + self.relation + " WHERE " + self.filter_relation + " = " + str(self.kw[self.filter_relation])
            #sql = "SELECT "+ name +", "+ self.filter_option +" FROM " + self.relation + " WHERE " + self.filter_relation + " = " + str(self.kw[self.filter_relation])
            query = text(sql)
            result = engine.execute(query)
            data = result.fetchall()
            for item in data:
                #VARIOS NO FUNCIONARIA AUN
                if item[0] == value:
                    html += "<input type='checkbox' class= "+str(name)+" name="+str(name)+" value="+str(item[0])+" checked>"+str(item[1])+"<br />"
                else:
                    html += "<input type='checkbox' class= "+str(name)+" name=" + str(name) + " value=" + str(item[0]) + ">" + str(item[1]) + "<br />"

        return html

    def input(self,name,type,contador):
        name2 = name
        required = ""
        visible = ""
        value = ""
        lenght = "50"
        self.real = name
        print("self.op tiene= ",self.op)
        if self.op != "add":
            sql = "SELECT " + name +" FROM " + self.model.__tablename__ + " WHERE " + self.indexkey + " = " + str(self.kw[self.indexkey])
            query = text(sql)
            result = engine.execute(query)
            data = result.fetchall()
            for item in data:
                for it in item:
                    value = it

        handler = DBSession.query(Dictionary).filter_by(database=str(name)).first()
        if name == self.kw['pwd']:
            handler = None
        if handler is not None:
            name2 = handler.name
            if(handler.required == True):
                required = "required"
            if(handler.lenght is not None):
                lenght = handler.lenght
            if handler.visible == False:
                visible = "hidden"
            if(handler.name_db is not None):

                name = handler.name_db
            if(handler.relation != ""):
                if(handler.filter_multioptions == True):
                    type = "GROUP"
                    self.relation = handler.relation
                    self.filter_relation = handler.filter_relation
                    self.filter_option = handler.filter_option
                else:
                    type = "SELECT"
                    self.relation = handler.relation
                    self.filter_relation = handler.filter_relation
                    self.filter_option = handler.filter_option

        if(self.indexkey==name):
            required = ""

        if(contador==1):
            required='required'

        if(name == 'internal_id'):
            print("ENTRE A ADD DESDE VIEW E INTERNAL_ID")
            value =  self.kw['internal_id']

        if(str(name2)!=self.indexkey):
            if(str(name2)=='application_id'):
                visible='hidden'
                html=''
            elif(str(name2)=='app_name'):
                data=self.gotosun()
                print("FUI A SUN")
                #print("recibi: ",data)
                #visible='hidden'
                #value=self.kw['app_name']
                html = data#"<tr "+visible+"><td><span>"+str(name2)+":</span></td><td>"+self.type(name,type,value,required,lenght)+"</td></tr>"
            else:
                html = "<tr " + visible + "><td><span>" + str(name2) + ":</span></td><td>" + self.type(name, type,value, required,lenght) + "</td></tr>"
        else:
            html = "<tr " + visible + "><td><span></span></td><td>" + self.type(name, type, value, required,lenght) + "</td></tr>"

        self.relation = ""
        return html

    def gotosun(self):
        data=[]
        domain = app_globals.domainsun+'internal_id='+self.kw['internal_id']
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = ''
        cadena='App_name: <select id="app_name" name="app_name">'
        lista=[]
        for item in empresa:
            if item['application_name'] != 'TODO':
                data.append({'name': item['application_name'], 'app_id': item['application_id'], 'owner': item['owner'],'internal_id': item['internal_id']})
                if self.kw['app_name']==item['application_name']:
                    cadena=cadena+"<option>"+item['application_name']+"</option>"

            lista.append(item['application_name'])
        if self.kw['app_name']=='TODO':
            for itemlista in lista:
                if itemlista !='TODO':
                    cadena = cadena + "<option>" + itemlista + "</option>"


        cadena=cadena+"</select>"


        return cadena