from pythonjupiter.lib.helpers import whoami
import os,sys
from tg import abort
from pathlib import Path
import pdfkit
from tg import render_template


class ExportPDF():
    @classmethod
    def create(cls, parameters,name,url,user):
        cwd=os.getenv('JUPITER_DIR')
        np=cwd.rfind("/")+1
        who=whoami()
        if user=="":
            error="CSV Failure"
            reason="User not logged"
            message = "The following {} occured, this is due to {}, please DEBUG the url : {}".format(error, reason,url)
            abort(status_code=500, detail=message)
        file_name=cwd+"/"+cwd[np:].replace(".","")+"/public/"+name+".pdf"


        my_file = Path(file_name)

        #print("my_file", my_file)
        if my_file.is_file():
            print("borrando...")
            os.remove(file_name)
            print("BORRE")

        ## PDF Generation ##
        options = {
            'page-width': '13.0in',
            'page-height': '8.5in',
            'margin-top': '0.1in',
            'margin-right': '0.1in',
            'margin-bottom': '0.1in',
            'margin-left': '0.1in',
            'encoding': "UTF-8",

        }

        #print("URL: ",url)
        body = render_template(parameters, "mako", url)
        #print("body: ", body)
        pdfkit.from_string(body, file_name, options=options)
        #mifile=cwd+"/"+cwd[np:].replace(".","")+"/templates/BillingStatement/htmlpdf.mak"
        #pdfkit.from_file(mifile, file_name)



        return "/"+name+".pdf"



class ExportCSV():
    """
     Export CSV
    """
    @classmethod
    def create(cls, list,name,url,internal):
        cwd = os.getenv('JUPITER_DIR')
        if cwd is None:
            cwd = os.getcwd()
        np = cwd.rfind("/") + 1
        file_name = cwd + "/" + cwd[np:].replace(".", "") + "/public/"+internal+"_"+name+".tsv"
        print("EL filename: ",file_name)
        my_file = Path(file_name)
        if my_file.is_file():
            os.remove(file_name)
        outfile = open(file_name, 'w', encoding="utf8")
        for item in list:
            outfile.write(item)
        outfile.close()
        return ""+name+".tsv"