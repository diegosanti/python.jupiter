import json
from tg import app_globals

import json
from pythonjupiter.model import DBSession
from pythonjupiter.model.auth import User
from tg.i18n import ugettext as _
from tg import expose,require
from tg import predicates
from pythonjupiter.model.tables import DatosUnidad,Persona,CargoUnidad,CargoOperador,UnidadCargo,OperadorCargo
from pythonjupiter.model.tables import Body,Head,Condonaciones,CondonacionUnidad,CondonacionOperador
from datetime import datetime
from pythonjupiter.lib.helpers import url


class Services():

    @expose('json')
    #@require(predicates.not_anonymous())
    def listjson(self):
        lista = []

        lista.append({'id': 701,'name': _('Catalog'),'url': '','rows': 2,'cols': 2,'observations': _('catalog table'),'perm': 'Table Catalogs','open': 'Menu','sons':
                    [{'id': 711,'name': _('Catalogs'),'url': '' ,'observations': _('catalog'),'perm': 'See Catalogs of','open':  'openMenu','icon': '<span class="glyphicon glyphicon-book"></span>','sons':
                    [{'id': 712,'name': _('Company'),'url': '/lineas/lineas','observations': _('company'),'perm': 'See Companies of','open': 'openTab','sons': '',
					'icon': '<span class="glyphicon glyphicon-briefcase"></span>'},
                     {'id': 713, 'name': _('Unit Type'), 'url': '/unit_type/unit_types','observations': _('units type'), 'perm': 'See unit types of', 'open': 'openTab', 'sons': '',
                      'icon': '<i class="fa fa-automobile"></i>'},
                     {'id': 714, 'name': _('Card Type'), 'url': '/card_type/card_types','observations': _('cards type'), 'perm': 'See card types of ', 'open': 'openTab', 'sons': '',
                      'icon': '	<span class="glyphicon glyphicon-credit-card"></span>'},
                     {'id': 715, 'name': _('Derrotero'), 'url': '/derrotero/derroteros','observations': _('derroteros'), 'perm': 'See derroteros of ', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-road"></span>'},
                     {'id': 716, 'name': _('Insurance Agency'), 'url': '/insurance/insurance','observations': _('insurance agency'), 'perm': 'See insurance agency of ', 'open': 'openTab',
                      'sons': '', 'icon': '<i class="fa fa-user-secret"></i>'},
                     {'id': 717, 'name': _('Reason of pay'), 'url': '/payment_motive/motives', 'observations': _('reason'),'perm': 'See reason of ', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-comment"></span>'},
                     {'id': 718, 'name': _('Period'), 'url': '/period/periods', 'observations': _('period'),'perm': 'See period of ', 'open': 'openTab', 'sons': '',
                      'icon': '<i class="fa fa-calendar"></i>'},
                     {'id': 719, 'name': _('Check point'), 'url': '/check_point/checkpoints','observations': _('check point'), 'perm': 'See check point of ', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-check"></span>'},
                     {'id': 720, 'name': _('Licence Type'), 'url': '/license/licences','observations': _('licence type'), 'perm': 'See Licence Type of ', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-list-alt"></span>'},
                     #{'id': 721, 'name': _('Reports'), 'url': '/reportes/castigos', 'observations': _('reports'),'perm': 'See the reports of', 'open': 'openTab', 'sons': '',
                      #'icon': ''},
                     {'id': 722, 'name': _('Groups'), 'url': '/group/groups', 'observations': _('groups'),'perm': 'See the groups of', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-inbox"></span>'},
                     {'id': 723, 'name': _('Operators Groups'), 'url': '/operator_group/operator_groups', 'observations': _('operators groups'),
                      'perm': 'See the operators groups of', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-inbox"></span>'},
                     {'id': 724, 'name': _('Extern Beneficiary'), 'url': '/extern_beneficiary/extern_beneficiaries/','observations': _('extern beneficiary'), 'perm': 'See extern beneficiary of ', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-user"></span>'}
                    ]},
                     {'id': 731, 'name': _('Units'), 'url': '/unidades/unidades','observations': _('units'), 'perm': 'See the units of','open': 'openTab', 'sons': '',
                     'icon': '<i class="fa fa-car"></i>'},
                     {'id': 732, 'name': _('People'), 'url': '/personas/personas', 'observations': _('people'),'perm': 'See the people of', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-user"></span>'},
                     {'id': 733, 'name': _('QR Code'), 'url': '/qr/qrcode', 'observations': _('qrcode'),
                      'perm': 'See the qr code of', 'open': 'openTab', 'sons': '',
                      'icon': '<span class="glyphicon glyphicon-qrcode"></span>'}]})

        lista.append({'id': 702,'name': _('Reports'),'url': '','rows': 2,'cols': 2,'observations': _('reports table'),'perm': 'Table Reports','open': 'Menu','sons':
             [{'id': 741,'name': _('Report'),'url': '/reportes/castigos' ,'observations': _('reports'),'perm': 'Reports of','open':  'openTab','icon': '<span class="glyphicon glyphicon-pencil"></span>','sons':''}]})

        lista.append({'id': 703, 'name': _('Charges'), 'url': '', 'rows': 3, 'cols': 3, 'observations': _('charges table'),'perm': 'Table Charges', 'open': 'Menu', 'sons':
                 [{'id': 751, 'name': _('Charge Unit'), 'url': '/cargosunidades/cargo_unidades', 'observations': _('charges'),'perm': 'Charges of', 'open': 'openTab', 'icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''},
                  {'id': 752, 'name': _('Charge Operator'), 'url': '/cargosoperadores/cargo_operadores','observations': _('charges operator'), 'perm': 'Charges of operator', 'open': 'openTab','icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''},
                  {'id': 753, 'name': _('Condonacion'), 'url': '/Condonacion','observations': _('condonaciones'), 'perm': 'Condonacion of operator or unit', 'open': 'openTab',
                   'icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''},
                  {'id': 754, 'name': _('Pay'), 'url': '/Abono','observations': _('Pay operator and Unit'), 'perm': 'Pay of operator and unit', 'open': 'openTab','icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''},
                  {'id': 755, 'name': _('Billing Statement'), 'url': '/BillingStatement', 'observations': _('Billing Statement'),
                   'perm': 'the Billing Statement of', 'open': 'openTab', 'icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''},
                  {'id': 756, 'name': _('Charges Statement'), 'url': '/Head',
                   'observations': _('Charges Statement'),
                   'perm': 'Charges Statement of units (operator?)', 'open': 'openTab',
                   'icon': '<i class="fa fa-dollar"></i>',
                   'sons': ''}
                  ]})


        lista.append({'id': 704,'name': _('State Account'),'url': '','rows': 2,'cols': 2,'observations': _('state account table'),'perm': 'Table State Account','open': 'Menu','sons':
             [{'id': 761,'name': _('State Account'),'url': '/Cuentas' ,'observations': _('state account'),'perm': 'state account of','open':  'openTab','icon': '<i class="fa fa-dollar"></i>','sons':''}]})



        lista.append({'id': 706,'name': _('Extern Pay'),'url': '','rows': 2,'cols': 2,'observations': _('extern pay table'),'perm': 'Table Extern Pay','open': 'Menu','sons':
             [{'id': 781,'name': _('Pay to'),'url': '/Salidas' ,'observations': _('pay to'),'perm': 'pay to of','open':  'openTab','icon': '<i class="fa fa-dollar"></i>','sons':''}]})


        lista.append({'id': 705, 'name': ('Permisionarios Perms'), 'rows': 1, 'cols': 4, 'url': '', 'observations': ('Permisionarios Perms'),'perm': 'Permisionarios Perms', 'open': 'Perms', 'sons': [
                {'id': 771, 'name': ('Add Unit'), 'url': '', 'observations': ('Add Unit'),
                 'perm': 'Add Unit', 'open': '', 'sons': '',
                 'icon': '<span class="glyphicon glyphicon-list-alt"></span>'},
                {'id': 772, 'name': ('Enable Unit'), 'url': '', 'observations': ('Enable Unit'),
                 'perm': 'Enable Unit', 'open': '', 'sons': '', 'icon': ''},
            {'id': 773, 'name': ('Edit Unit'), 'url': '', 'observations': ('Edit Unit'),
             'perm': 'Edit Unit', 'open': '', 'sons': '', 'icon': ''},
            {'id': 774, 'name': ('Disable Unit'), 'url': '', 'observations': ('Disable Unit'),
             'perm': 'Disable Unit', 'open': '', 'sons': '', 'icon': ''},
            {'id': 775, 'name': ('Search for Pay'), 'url': '', 'observations': ('Search for Pay'),
             'perm': 'Search for Pay', 'open': '', 'sons': '', 'icon': ''},
            {'id': 776, 'name': ('Add Pay'), 'url': '', 'observations': ('Add Pay'),
             'perm': 'Add Pay', 'open': '', 'sons': '', 'icon': ''},
            {'id': 777, 'name': ('Cancel Pay'), 'url': '', 'observations': ('Cancel Pay'),
             'perm': 'Cancel Pay', 'open': '', 'sons': '', 'icon': ''},
            {'id': 778, 'name': ('Sec1 Billing'), 'url': '', 'observations': ('Sec1 Billing'),
             'perm': 'Sec1 Billing', 'open': '', 'sons': '', 'icon': ''},
            {'id': 779, 'name': ('Add Poliza'), 'url': '', 'observations': ('Add Poliza'),
             'perm': 'AddPoliza', 'open': '', 'sons': '', 'icon': ''},
            {'id': 780, 'name': ('Edit Poliza'), 'url': '', 'observations': ('Edit Poliza'),
             'perm': 'Edit Poliza', 'open': '', 'sons': '', 'icon': ''},
            {'id': 782, 'name': ('Delete Poliza'), 'url': '', 'observations': ('Delete Poliza'),
             'perm': 'Delete Poliza', 'open': '', 'sons': '', 'icon': ''}
            ]})


        #<span class="glyphicon glyphicon-user"></span>
        # lista.append({'id': 700, 'name': _('Company'), 'url': '/lineas/lineas', 'observations': _('company'),'perm': 'See Companies of', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 701, 'name': _('Units'), 'url': '/unidades/unidades', 'observations': _('units'), 'perm': 'See the units of','open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 702, 'name': _('Unit Type'), 'url': '/tipo_unidad?action=show&perm', 'observations': _('units type'),'perm': 'See unit types of', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 703, 'name': _('Card Type'), 'url': '/tipo_tarjeta?action=show&perm', 'observations': _('cards type'),'perm': 'See card types of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 704, 'name': _('Derrotero'), 'url': '/derrotero?action=show&perm','observations': _('derroteros'), 'perm': 'See derroteros of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 705, 'name': _('Insurance Agency'), 'url': '/aseguradora?action=show&perm', 'observations': _('insurance agency'),'perm': 'See insurance agency of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # #lista.append({'id': 706, 'name': _('Beneficiary'), 'url': '/beneficiario?action=show&perm','observations': _('beneficiary'), 'perm': 'See beneficiary of ', 'open': 'openTab','sons': '','icon': ''})
        #
        # lista.append({'id': 707, 'name': _('Reason'), 'url': '/motivo?action=show&perm','observations': _('reason'), 'perm': 'See reason of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 708, 'name': _('Period'), 'url': '/periodo?action=show&perm', 'observations': _('period'),'perm': 'See period of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 709, 'name': _('Check point'), 'url': '/punto?action=show&perm', 'observations': _('check point'),'perm': 'See check point of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # #lista.append({'id': 710, 'name': _('Checker'), 'url': '/checador?action=show&perm', 'observations': _('checker'),'perm': 'See cheker of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 711, 'name': _('Licence Type'), 'url': '/tipo_licencia?action=show&perm', 'observations': _('licence type'),'perm': 'See Licence Type of ', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 712, 'name': _('Operators'), 'url': '/operadores/operadores', 'observations': _('operators'),'perm': 'See the operators of', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 713, 'name': _('Reports'), 'url': '/reportes/castigos', 'observations': _('reports'),'perm': 'See the reports of', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 714, 'name': _('Groups'), 'url': '/grupo?action=show&perm', 'observations': _('groups'),'perm': 'See the groups of', 'open': 'openTab', 'sons': '','icon': ''})
        #
        # lista.append({'id': 715, 'name': _('People'), 'url': '/personas/personas', 'observations': _('people'),'perm': 'See the people of', 'open': 'openTab', 'sons': '', 'icon': ''})
        #
        # lista.append({'id': 703, 'name': _('Assign the Sims'), 'url': '/jqgridnext?value=0', 'observations': 'jqgridnext?value=0','perm': 'Assing Sims', 'open': 'openTab', 'sons': ''})
        #
        # lista.append({'id': 704, 'name': _('Sims By Application'), 'url': '/listsim?value=2', 'observations': 'value=2','perm': 'See sims By Application', 'open': 'openTab', 'sons': ''})
        #
        # lista.append({'id': 705, 'name': _('Create Commercial Groups'), 'url': '/newcommercialGroup','observations': _('New Commercial Group'), 'perm': 'group', 'open': 'openTab', 'sons': ''})
        #
        # lista.append({'id': 706, 'name': _('Edit Commercial Groups'), 'url': '/editcommercialGroup','observations': _('Edit Commercial Group'), 'perm': 'editgroup', 'open': 'openTab', 'sons': ''})

        return dict(list=lista, error="ok", planet_name="jupiter")




    @expose('json')
    @require(predicates.not_anonymous())
    def getallunits(self,**kw):
        internal_id=kw['internal_id']
        ecosunits=[]
        allunits=DBSession.query(DatosUnidad).filter_by(internal_id=internal_id).filter_by(habilitado=1).all()
        for itemunits in allunits:
            ecosunits.append({'eco1': itemunits.eco1,'eco2': itemunits.eco2})


        return dict(error='ok',units=ecosunits)



    @expose('json')
    @require(predicates.not_anonymous())
    def getalloperators(self,**kw):
        internal_id=kw['internal_id']
        operators=[]
        alloperators=DBSession.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).filter_by(internal_id=internal_id).all()
        for itemoperator in alloperators:
            operators.append({'nombre': itemoperator.nombre,'apodo': itemoperator.apodo,'numero': itemoperator.numerooperador})


        return dict(error='ok',operators=operators)


    @expose('json')
    @require(predicates.not_anonymous())
    def getunit(self,**kw):
        internal_id=kw['internal_id']
        eco=kw['eco']
        ecosunits=[]
        unit=DBSession.query(DatosUnidad).filter_by(internal_id=internal_id).filter_by(habilitado=1).filter_by(eco1=eco).first()
        if unit is not None:
            ecosunits.append({'eco1': unit.eco1,'eco2': unit.eco2})
        else:
            return dict(error='no', unit=ecosunits)

        return dict(error='ok',unit=ecosunits)


    @expose('json')
    @require(predicates.not_anonymous())
    def getoperator(self,**kw):
        internal_id=kw['internal_id']
        numero=kw['numero']
        operators=[]
        operator=DBSession.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).filter_by(internal_id=internal_id).filter_by(numerooperador=numero).first()
        if operator is not None:
            operators.append({'nombre': operator.nombre,'apodo': operator.apodo,'numero': operator.numerooperador})
        else:
            return dict(error='no', operator=operators)

        return dict(error='ok',operator=operators)



    @expose('json')
    def partidosespana(self,**kw):
        partidos=[]

        partidos.append({'equipolocal': 'Sevilla','equipovisitante': 'Levante','marcadorlocal': 5,'marcadorvisitante': 0})
        partidos.append({'equipolocal': 'Girona', 'equipovisitante': 'FC Barcelona', 'marcadorlocal': 0, 'marcadorvisitante': 2})

        #partidos={'equipolocal': 'Sevilla', 'equipovisitante': 'Levante', 'marcadorlocal': 5, 'marcadorvisitante': 0}
        return dict(partidos=partidos)


    @expose('json')
    #@require(predicates.not_anonymous())
    def chargeheadbody(self,**kw):
        unidad=kw['unidad']
        numero=kw['operador']
        eloperador=kw['eloperador']
        eco1=kw['eco1']
        internal_id=kw['internal_id']
        error=''

        if True==True:

            queryunidad = DBSession.query(DatosUnidad).filter_by(eco1=eco1).filter_by(habilitado=1).filter_by(id_unidad=unidad).filter_by(internal_id=internal_id).first()
            queryoperador = DBSession.query(Persona).filter_by(nombre=eloperador).filter_by(habilitadooperador=1).filter_by(id_persona=numero).filter_by(internal_id=internal_id).first()
            if queryunidad is None:
                return dict(error="no_unidad")
            if queryoperador is None:
                return dict(error="no_operador")


            queryoperador.id_unidad=queryunidad.id_unidad
            if queryunidad is not None:
                idunidad=queryunidad.id_unidad
                queryunidadcargo=DBSession.query(UnidadCargo).filter_by(unidad_id=idunidad).all()
                for itemunidadcargo in queryunidadcargo:
                    querycargo=DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                    if querycargo.tipodecargo==3 or querycargo.tipodecargo==2:
                        frecuencia=querycargo.frecuencia.split(",")
                        prueba=(datetime.now().date()).strftime('%A')
                        if prueba=='Monday':
                            prueba='Lunes'
                        if prueba == 'Tuesday':
                            prueba = 'Martes'
                        if prueba == 'Wednesday':
                            prueba = 'Miercoles'
                        if prueba == 'Thursday':
                            prueba = 'Jueves'
                        if prueba == 'Friday':
                            prueba = 'Viernes'
                        if prueba == 'Saturday':
                            prueba = 'Sabado'
                        if prueba == 'Sunday':
                            prueba = 'Domingo'
                        #
                        if prueba in frecuencia:
                            querycondonacionesunidad = DBSession.query(CondonacionUnidad).filter_by(unidad_id=queryunidad.id_unidad).filter_by(cargounidad_id=querycargo.id_cargounidad).all()
                            if querycondonacionesunidad != []:
                                condonacion = 0
                                for itemcondo in querycondonacionesunidad:
                                    querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                                    .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                                    if querycondonacion is not None:
                                        condonacion = condonacion + querycondonacion.monto
                                    else:
                                        condonacion = condonacion
                            else:
                                condonacion = 0

                            querybody=DBSession.query(Body).filter_by(fecha=datetime.now().date()).filter_by(id_unidad=queryunidad.id_unidad).filter_by(id_cargounidad=querycargo.id_cargounidad).first()
                            if querybody is None:
                                if itemunidadcargo.monto > 0:
                                    newbody = Body()
                                    newbody.monto = itemunidadcargo.monto
                                    if condonacion > newbody.monto:
                                        newbody.condonacion = 0
                                    else:
                                        newbody.condonacion = condonacion
                                    newbody.saldo = itemunidadcargo.monto - newbody.condonacion
                                    newbody.pagado = 0
                                    newbody.fecha = datetime.now().date()
                                    newbody.id_unidad = queryunidad.id_unidad
                                    newbody.id_cargounidad = querycargo.id_cargounidad
                                    newbody.internal_id = internal_id
                                    DBSession.add(newbody)



                                print("El cargo " + querycargo.cargo + " esta en la unidad " + queryunidad.eco1+" y se cobra hoy y tiene condonacion de: "+str(condonacion))
                            else:
                                error=error+","+"ya_existe_cargo_unidad"
                                break

                saldo = 0
                querybody = DBSession.query(Body).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=datetime.now().date()).all()
                for itembody in querybody:
                    saldo = saldo + itembody.saldo
                if saldo > 0:
                    queryhead = DBSession.query(Head).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=datetime.now().date()).first()
                    if queryhead is not None:
                        queryhead.porpagar = saldo
                        queryhead.pagado = 0
                        queryhead.faltante = saldo
                    else:
                        newhead = Head()
                        newhead.fecha = datetime.now().date()
                        newhead.estatus = 'Por Pagar'
                        newhead.porpagar = saldo
                        newhead.pagado = 0
                        newhead.faltante = saldo
                        newhead.id_unidad = idunidad
                        newhead.internal_id = internal_id
                        newhead.app_name = 'TODO'
                        DBSession.add(newhead)


            DBSession.flush()
            if queryoperador is not None:
                idoperador = queryoperador.id_persona
                queryoperadorcargo = DBSession.query(OperadorCargo).filter_by(operador_id=idoperador).all()
                for itemoperadorcargo in queryoperadorcargo:
                    querycargo = DBSession.query(CargoOperador).filter_by(id_cargooperador=itemoperadorcargo.cargo_id).first()
                    if querycargo.tipodecargo == 3 or querycargo.tipodecargo == 2:
                        frecuencia = querycargo.frecuencia.split(",")
                        prueba = (datetime.now().date()).strftime('%A')
                        if prueba == 'Monday':
                            prueba = 'Lunes'
                        if prueba == 'Tuesday':
                            prueba = 'Martes'
                        if prueba == 'Wednesday':
                            prueba = 'Miercoles'
                        if prueba == 'Thursday':
                            prueba = 'Jueves'
                        if prueba == 'Friday':
                            prueba = 'Viernes'
                        if prueba == 'Saturday':
                            prueba = 'Sabado'
                        if prueba == 'Sunday':
                            prueba = 'Domingo'
                        #
                        if prueba in frecuencia:
                            querycondonacioneoperadores = DBSession.query(CondonacionOperador).filter_by(operador_id=queryoperador.id_persona).filter_by(cargooperador_id=querycargo.id_cargooperador).all()

                            if querycondonacioneoperadores!=[]:
                                condonacion=0
                                for itemcondo in querycondonacioneoperadores:
                                    querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                    .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                                    if querycondonacion is not None:
                                        condonacion = condonacion + querycondonacion.monto
                                    else:
                                        condonacion=condonacion
                            else:
                                condonacion=0

                            querybody = DBSession.query(Body).filter_by(fecha=datetime.now().date()).filter_by(id_operador=queryoperador.id_persona).filter_by(id_cargopersona=querycargo.id_cargooperador).first()
                            if querybody is None:
                                if itemoperadorcargo.monto > 0:
                                    newbody = Body()
                                    newbody.monto = itemoperadorcargo.monto
                                    if condonacion > newbody.monto:
                                        newbody.condonacion = 0
                                    else:
                                        newbody.condonacion = condonacion
                                    newbody.saldo = itemoperadorcargo.monto - newbody.condonacion
                                    newbody.pagado = 0
                                    newbody.fecha = datetime.now().date()
                                    newbody.id_operador = queryoperador.id_persona
                                    newbody.id_cargopersona = querycargo.id_cargooperador
                                    newbody.internal_id = internal_id
                                    DBSession.add(newbody)

                                print("El cargo " + querycargo.cargo + " esta en el operador " + queryoperador.nombre + " y se cobra hoy y tiene condonacion de: " + str(condonacion))
                            else:
                                error = error+","+"ya_existe_cargo_operador"
                                break

                saldo = 0
                querybody = DBSession.query(Body).filter_by(id_operador=queryoperador.id_persona).filter_by(fecha=datetime.now().date()).all()
                for itembody in querybody:
                    saldo = saldo + itembody.saldo
                if saldo > 0:
                    queryhead = DBSession.query(Head).filter_by(id_unidad=None).filter_by(id_operador=queryoperador.id_persona).filter_by(fecha=datetime.now().date()).first()
                    if queryhead is not None:
                        queryhead.porpagar = saldo
                        queryhead.pagado = 0
                        queryhead.faltante = saldo
                    else:
                        newhead = Head()
                        newhead.fecha = datetime.now().date()
                        newhead.estatus = 'Por Pagar'
                        newhead.porpagar = saldo
                        newhead.pagado = 0
                        newhead.faltante = saldo
                        newhead.id_operador = idoperador
                        newhead.internal_id = internal_id
                        newhead.app_name=queryunidad.app_name
                        DBSession.add(newhead)
                        DBSession.flush()



        if error=='':
            return dict(error='ok')
        else:
            return dict(error=error)


    @expose('json')
    def setUnit(self,**kw):
        #print(kw)
        newunidad=DatosUnidad()

        try:
            eco1=kw['eco1']
        except KeyError:
            return dict(error="Falta Llave eco1")

        try:
            eco2=kw['eco2']
        except KeyError:
            return dict(error="Falta Llave eco2")

        try:
            app_name=kw['app_name']
        except KeyError:
            return dict(error="Falta Llave app_name")

        try:
            app_id=kw['app_id']
        except KeyError:
            return dict(error="Falta Llave app_id")


        try:
            internal_id=kw['internal_id']
        except KeyError:
            return dict(error="Falta Llave internal_id")

        try:
            placas=kw['placas']
        except KeyError:
            return dict(error="Falta Llave placas")

        try:
            verbo=kw['verb']
        except KeyError:
            return dict(error="Falta Llave verb")


        if 'marca' in kw:
            marca=kw['marca']
        else:
            marca=None

        if 'modelo' in kw:
            modelo=kw['modelo']
        else:
            modelo=None

        if 'numeromotor' in kw:
            numeromotor=kw['numeromotor']
        else:
            numeromotor=None

        #if 'tipounidad' in kw:
        #    tipounidad=kw['tipounidad']
        #    else:
        #        tipounidad=''   """

        if eco1!='' and placas!='' and app_name!='' and app_id!='':

            if verbo=='add':


                queryexiste=DBSession.query(DatosUnidad).filter_by(eco1=eco1).filter_by(eco2=eco2).filter_by(placas=placas).filter_by(marca=marca).filter_by(modelo=modelo).filter_by(numero_motor=numeromotor)\
                            .filter_by(app_name=app_name).filter_by(app_id=int(app_id)).filter_by(habilitado=1).filter_by(internal_id=internal_id).first()

                if queryexiste is None:

                    queryexisteeco=DBSession.query(DatosUnidad).filter_by(eco1=eco1).filter_by(app_name=app_name).filter_by(app_id=int(app_id)).first()
                    if queryexisteeco is None:

                        newunidad.eco1=eco1
                        newunidad.eco2=eco2
                        newunidad.placas=placas
                        newunidad.marca=marca
                        newunidad.modelo=modelo
                        newunidad.numero_motor=numeromotor
                        newunidad.app_name=app_name
                        newunidad.app_id=int(app_id)
                        newunidad.habilitado=1
                        newunidad.internal_id=internal_id
                        DBSession.add(newunidad)
                        DBSession.flush()
                        return dict(error="ok", mensaje="Unidad Insertada")
                    else:
                        return dict(error="error", mensaje="Ya Existe la unidad")
                else:
                    return dict(error="error", mensaje="Ya Existe la unidad")

            elif verbo=='delete':
                queryexiste=DBSession.query(DatosUnidad).filter_by(eco1=eco1).filter_by(placas=placas).filter_by(app_id=int(app_id)).filter_by(habilitado=1).first()
                if queryexiste is None:
                    return dict(error="error", mensaje="No existe Unidad")
                else:
                    DBSession.delete(queryexiste)
                    DBSession.flush()
                    return dict(error="ok", mensaje="Unidad Eliminada")
            else:
                return dict(error="error",mensaje="Verbo No Encontrado")



        else:
            return dict(error="error", mensaje="Campos Vacios")



