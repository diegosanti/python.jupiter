from sqlalchemy import Column, Integer, Unicode, Boolean
from pythonjupiter.model import DeclarativeBase

#TABLE FOR CREATE VIEWS CLASS
class Dictionary(DeclarativeBase):
    __tablename__ = 'dictionary'
    id = Column(Integer,primary_key=True)
    database = Column(Unicode(50),nullable=False, unique=True)
    name = Column(Unicode(50),nullable=False)
    required = Column(Boolean)
    visible = Column(Boolean)
    lenght = Column(Integer)
    relation = Column(Unicode(50))
    filter_relation = Column(Unicode(50))#Database
    filter_relation2= Column(Unicode(50))#Pycharm
    filter_option = Column(Unicode(50))
    filter_multioptions = Column(Boolean)
    name_db = Column(Unicode(50))

    def __repr__(self):
        return '<Dictionary: name=%s, database=%s, required=%s, visible=%s, lenght=%s>, relation=%s, filter_relation=%s, filter_multioptions=%s>' \
               % (repr(self.name),repr(self.database),repr(self.required),repr(self.visible),repr(self.lenght)
                  ,repr(self.relation),repr(self.filter_relation),repr(self.filter_multioptions))

    def __unicode__(self):
        return self.name or self.database