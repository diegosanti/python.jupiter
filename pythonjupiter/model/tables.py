from sqlalchemy import Column, Integer, Date, Text, Unicode, Boolean, Numeric,Float,LargeBinary,ForeignKey
from sqlalchemy.types import DateTime
from datetime import datetime
from pythonjupiter.model import DeclarativeBase
from sqlalchemy import Table
from pythonjupiter.model import DeclarativeBase, metadata, DBSession
from sqlalchemy.orm import relation,backref,relationship
from sqlalchemy import Table, ForeignKey, Column
from sqlalchemy.types import Unicode, Integer, DateTime
from sqlalchemy.orm import relation, synonym


# class ReporteOperador(DeclarativeBase):
#     __tablename__ = 'reporte_operador'
#     id=Column(Integer,primary_key=True,autoincrement=True)
#     reporte_id = Column(Integer, ForeignKey('reporte.id_reporte'), primary_key=True)
#     operador_id = Column(Integer, ForeignKey('operador.id_operador'), primary_key=True)
#     fecha=Column(DateTime,default=datetime.now())
#     usuario=Column(Unicode(25))
#     gravedad = Column(Integer)
#     internal_id=Column(Integer)
#     unidad=Column(Unicode(20))

class ReportePersona(DeclarativeBase):
    __tablename__ = 'reporte_persona'
    id=Column(Integer,primary_key=True,autoincrement=True)
    reporte_id = Column(Integer, ForeignKey('reporte.id_reporte'), primary_key=True)
    persona_id = Column(Integer, ForeignKey('persona.id_persona'), primary_key=True)
    fecha=Column(DateTime,default=datetime.now())
    usuario=Column(Unicode(25))
    gravedad = Column(Integer)
    descripcion=Column(Unicode(250))
    internal_id=Column(Integer)
    unidad=Column(Unicode(20))

class Reporte(DeclarativeBase):
    __tablename__='reporte'
    id_reporte=Column(Integer,primary_key=True)
    reporte=Column(Unicode(150))
    internal_id=Column(Integer)
    personas=relationship('Persona', secondary='reporte_persona')

class UnidadCargo(DeclarativeBase):
    __tablename__='unidad_cargo'
    id=Column(Integer,primary_key=True)
    unidad_id=Column(Integer,ForeignKey('datosunidad.id_unidad'),primary_key=True)
    cargo_id=Column(Integer,ForeignKey('cargounidad.id_cargounidad'),primary_key=True)
    fecha=Column(Date,default=datetime.now())
    monto=Column(Integer)

class OperadorCargo(DeclarativeBase):
    __tablename__='persona_cargo'
    id=Column(Integer,primary_key=True)
    operador_id=Column(Integer,ForeignKey('persona.id_persona'),primary_key=True)
    cargo_id=Column(Integer,ForeignKey('cargooperador.id_cargooperador'),primary_key=True)
    fecha=Column(Date,default=datetime.now())
    monto=Column(Integer)

class CondonacionUnidad(DeclarativeBase):
    __tablename__='condonacion_unidad'
    condonacion_id=Column(Integer,ForeignKey('condonaciones.id_condonacion'),primary_key=True)
    unidad_id = Column(Integer, ForeignKey('datosunidad.id_unidad'), primary_key=True)
    cargounidad_id=Column(Integer,ForeignKey('cargounidad.id_cargounidad'))

class CondonacionOperador(DeclarativeBase):
    __tablename__='condonacion_operador'
    condonacion_id=Column(Integer,ForeignKey('condonaciones.id_condonacion'),primary_key=True)
    operador_id = Column(Integer, ForeignKey('persona.id_persona'), primary_key=True)
    cargooperador_id=Column(Integer,ForeignKey('cargooperador.id_cargooperador'))

class Grupo_DatosUnidad(DeclarativeBase):
    __tablename__='grupo_datosunidad'
    grupo_id=Column(Integer,ForeignKey('grupo.id_grupo'),primary_key=True)
    datosunidad_id = Column(Integer, ForeignKey('datosunidad.id_unidad'), primary_key=True)

class GrupoPersona_Persona(DeclarativeBase):
    __tablename__='grupopersona_persona'
    grupo_id=Column(Integer,ForeignKey('grupopersona.id_grupopersona'),primary_key=True)
    persona_id = Column(Integer, ForeignKey('persona.id_persona'), primary_key=True)

# class Operador(DeclarativeBase):
#     __tablename__='operador'
#     id_operador=Column(Integer,primary_key=True)
#     operador=Column(Unicode(100))
#     apodo=Column(Unicode(25))
#     foto=Column(Unicode(500))#archivo
#     rfc=Column(Unicode(20))
#     fecha_nacimiento=Column(Date)
#     correo=Column(Unicode(50))
#     sexo=Column(Unicode(10))
#     estadocivil=Column(Unicode(25))
#     estatus=Column(Unicode(10))
#     fecha_estatus=Column(Date)
#     fecha_ingreo=Column(Date,default=datetime.now())
#     direccion=Column(Unicode(200))
#     direccion2=Column(Unicode(200))
#     direccion3=Column(Unicode(200))
#     telefono=Column(Unicode(25))
#     telefono2=Column(Unicode(25))
#     celular=Column(Unicode(25))
#     otro_celular=Column(Unicode(25))
#     numero_segurosocial=Column(Unicode(50))
#     licencia=Column(Unicode(500))#archivo
#     fecha_vencimiento=Column(Date)
#     curp = Column(Unicode(500))#archivo
#     ine = Column(Unicode(500))#archivo
#     habilitado=Column(Integer)
#     observaciones=Column(Unicode(500))
#     internal_id=Column(Integer)
#     estadodelicencia=Column(Unicode(25))
#     id_linea=Column(Integer,ForeignKey('linea.id_linea'))
#     id_tipolicencia=Column(Integer,ForeignKey('tipolicencia.id_tipolicencia'))
#     reportes = relationship('Reporte', secondary='reporte_operador')

class Persona(DeclarativeBase):
    __tablename__='persona'
    id_persona=Column(Integer,primary_key=True)
    numerooperador=Column(Integer)
    nombre=Column(Unicode(100))
    apodo=Column(Unicode(25))
    foto= Column(LargeBinary(length=(2 ** 32) - 1)) ##NO
    rfc=Column(Unicode(20))
    fecha_nacimiento=Column(Date)
    correo=Column(Unicode(50))
    sexo=Column(Unicode(10))
    estadocivil=Column(Unicode(25))
    estatus=Column(Unicode(10))
    fecha_estatus=Column(Date)
    fecha_ingreso=Column(Date,default=datetime.now())
    direccion=Column(Unicode(200))
    direccion2=Column(Unicode(200))
    direccion3=Column(Unicode(200))
    telefono=Column(Unicode(25))
    telefono2=Column(Unicode(25))
    celular=Column(Unicode(25))
    otro_celular=Column(Unicode(25))
    curp = Column(Unicode(50))
    ine = Column(LargeBinary(length=(2 ** 32) - 1)) ##NO
    observaciones = Column(Unicode(500))
    eschecador=Column(Integer)
    esbeneficiario=Column(Integer)
    esoperador=Column(Integer)
    espermisionario=Column(Integer)
    habilitadochecador = Column(Integer)
    habilitadobeneficiario = Column(Integer)
    habilitadooperador = Column(Integer)
    habilitadopermisionario = Column(Integer)

    numero_segurosocial=Column(Unicode(50))
    licencia=Column(LargeBinary(length=(2 ** 32) - 1)) ##NO
    numerolicencia=Column(Unicode(55))
    fecha_vencimientolicencia=Column(Date)
    estadodelicencia = Column(Unicode(25))

    usuario=Column(Unicode(100))

    id_tipolicencia = Column(Integer, ForeignKey('tipolicencia.id_tipolicencia'))
    id_linea = Column(Integer, ForeignKey('linea.id_linea'))
    id_punto=Column(Integer,ForeignKey('punto.id_punto'))
    id_grupopersona=Column(Integer)  ##NO
    id_unidad=Column(Integer)  ##NO

    internal_id=Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(100))


    cta1=Column(Unicode(60))
    banco1=Column(Unicode(60))
    cta2=Column(Unicode(60))
    banco2=Column(Unicode(60))

    codigoqr = Column(LargeBinary(length=(2 ** 32) - 1))
    reportes = relationship('Reporte', secondary='reporte_persona')
    unidades = relationship('DatosUnidad', backref='persona')

class DatosUnidad(DeclarativeBase):
    __tablename__='datosunidad'
    id_unidad=Column(Integer,primary_key=True)
    vin=Column(Unicode(50))
    placas=Column(Unicode(50))
    numero_motor=Column(Unicode(25))
    anio=Column(Integer)
    modelo=Column(Unicode(50))
    marca=Column(Unicode(50))
    color=Column(Unicode(50))
    kilometraje=Column(Unicode(10))
    rasgos=Column(Unicode(100))
    foto=Column(LargeBinary(length=(2**32)-1))
    ruta_foto=Column(Unicode(350)) #No se ocupa
    fecha_ingreso = Column(DateTime,default=datetime.now)
    estatus = Column(Unicode(20))
    fecha_estatus = Column(Date)
    observaciones=Column(Unicode(200))
    internal_id=Column(Integer)
    app_id=Column(Integer)
    app_name=Column(Unicode(100))
    esunidadtransportes=Column(Unicode(5))
    eco1 = Column(Unicode(25))
    eco2 = Column(Unicode(25))
    concesion = Column(Unicode(45))
    cuentaespecial=Column(Integer)
    habilitado = Column(Integer)
    tarjetadecirculacion=Column(LargeBinary(length=(2**32)-1))
    codigoqr=Column(LargeBinary(length=(2**32)-1))
    id_linea = Column(Integer, ForeignKey('linea.id_linea'))
    id_tipounidad = Column(Integer, ForeignKey('tipounidad.id_tipounidad'))
    id_tipotarjeta = Column(Integer, ForeignKey('tipotarjeta.id_tipotarjeta'))
    id_derrotero = Column(Integer, ForeignKey('derrotero.id_derrotero'))
    id_grupo=Column(Integer, ForeignKey('grupo.id_grupo'))
    id_aseguradora = Column(Integer, ForeignKey('aseguradora.id_aseguradora'))
    id_persona=Column(Integer,ForeignKey('persona.id_persona'))
    unidades = relationship('CargoUnidad', secondary='unidad_cargo')

class CargoUnidad(DeclarativeBase):
    __tablename__='cargounidad'
    id_cargounidad=Column(Integer,primary_key=True)
    cargo=Column(Unicode(70))
    frecuencia=Column(Unicode(200))
    internal_id=Column(Integer)
    app_name=Column(Unicode(100))
    application_id=Column(Integer)
    empresa=Column(Unicode(100))
    derrotero=Column(Unicode(100))
    grupo=Column(Unicode(100))
    montodefault=Column(Integer)
    tipodecargo=Column(Integer)
    unidades = relationship('DatosUnidad', secondary='unidad_cargo')

class CargoOperador(DeclarativeBase):
    __tablename__='cargooperador'
    id_cargooperador=Column(Integer,primary_key=True)
    cargo=Column(Unicode(70))
    frecuencia=Column(Unicode(200))
    internal_id=Column(Integer)
    app_name=Column(Unicode(100))
    application_id = Column(Integer)
    empresa=Column(Unicode(100))
    grupopersona=Column(Unicode(80))
    montodefault=Column(Integer)
    tipodecargo = Column(Integer)
    personas = relationship('Persona', secondary='persona_cargo')

class TipoUnidad(DeclarativeBase):
    __tablename__='tipounidad'
    id_tipounidad=Column(Integer,primary_key=True)
    tipo_unidad=Column(Unicode(50))
    internal_id = Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(100))
    descripcion=Column(Unicode(150))
    unidades = relationship('DatosUnidad', backref='tipounidad')



    @classmethod
    def filterApp(cls, filter, id):
        print("Filter app")
        handler = DBSession.query(cls).filter(cls.tipo_unidad.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipounidad": item.id_tipounidad, "tipo_unidad": item.tipo_unidad, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.tipo_unidad.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipounidad": item.id_tipounidad, "tipo_unidad": item.tipo_unidad, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            #print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_tipounidad": item.id_tipounidad, "tipo_unidad": item.tipo_unidad, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipounidad": item.id_tipounidad, "tipo_unidad": item.tipo_unidad, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

class TipoTarjeta(DeclarativeBase):
    __tablename__='tipotarjeta'
    id_tipotarjeta=Column(Integer,primary_key=True)
    tipo_tarjeta=Column(Unicode(50))
    internal_id=Column(Integer)
    descripcion=Column(Unicode(150))
    #monto=Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(100))
    unidades=relationship('DatosUnidad', backref='tipotarjeta')

    @classmethod
    def filterApp(cls, filter, id):
        print("Filter app")
        handler = DBSession.query(cls).filter(cls.tipo_tarjeta.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipotarjeta": item.id_tipotarjeta, "tipo_tarjeta": item.tipo_tarjeta, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.tipo_tarjeta.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipotarjeta": item.id_tipotarjeta, "tipo_tarjeta": item.tipo_tarjeta, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            #print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_tipotarjeta": item.id_tipotarjeta, "tipo_tarjeta": item.tipo_tarjeta, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipotarjeta": item.id_tipotarjeta, "tipo_tarjeta": item.tipo_tarjeta, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

class TipoLicencia(DeclarativeBase):
    __tablename__='tipolicencia'
    id_tipolicencia=Column(Integer,primary_key=True)
    tipo_licencia=Column(Unicode(50))
    internal_id=Column(Integer)
    application_id = Column(Integer)
    app_name = Column(Unicode(100))
    descripcion=Column(Unicode(150))
    personas=relationship('Persona',backref='tipolicencia')


    @classmethod
    def filterApp(cls, filter, id):
        print("Filter app")
        handler = DBSession.query(cls).filter(cls.tipo_licencia.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipolicencia": item.id_tipolicencia, "tipo_licencia": item.tipo_licencia, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.tipo_licencia.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipolicencia": item.id_tipolicencia, "tipo_licencia": item.tipo_licencia, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            #print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_tipolicencia": item.id_tipolicencia, "tipo_licencia": item.tipo_licencia, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_tipolicencia": item.id_tipolicencia, "tipo_licencia": item.tipo_licencia, "internal_id": item.internal_id,"descripcion": item.descripcion,'application_id': item.application_id,'app_name': item.app_name})
        return hd

class Derrotero(DeclarativeBase):
    __tablename__='derrotero'
    id_derrotero=Column(Integer,primary_key=True)
    derrotero=Column(Unicode(50))
    internal_id=Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(50))
    #monto_LV=Column(Integer)
    #especial_LV=Column(Integer)
    #monto_S = Column(Integer)
    #especial_S=Column(Integer)
    #monto_D = Column(Integer)
    #especial_D=Column(Integer)
    unidades=relationship('DatosUnidad', backref='derrotero')

    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.derrotero.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_derrotero": item.id_derrotero, "derrotero": item.derrotero, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.derrotero.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_derrotero": item.id_derrotero, "derrotero": item.derrotero, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_derrotero": item.id_derrotero, "derrotero": item.derrotero, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_derrotero": item.id_derrotero, "derrotero": item.derrotero, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

class Grupo(DeclarativeBase):
    __tablename__='grupo'
    id_grupo=Column(Integer,primary_key=True)
    grupo=Column(Unicode(50))
    internal_id=Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(50))
    unidades=relationship('DatosUnidad', backref='grupo')

    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.grupo.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_grupo": item.id_grupo, "grupo": item.grupo, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.grupo.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_grupo": item.id_grupo, "grupo": item.grupo, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_grupo": item.id_grupo, "grupo": item.grupo, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_grupo": item.id_grupo, "grupo": item.grupo, "internal_id": item.internal_id,"application_id": item.application_id,"app_name": item.app_name})
        return hd

class Aseguradora(DeclarativeBase):
    __tablename__='aseguradora'
    id_aseguradora=Column(Integer,primary_key=True)
    aseguradora=Column(Unicode(50))
    internal_id=Column(Integer)
    contacto=Column(Unicode(100))
    contacto2=Column(Unicode(100))
    telefono=Column(Unicode(100))
    celular=Column(Unicode(100))
    telefonoemergencia=Column(Unicode(100))
    direccion=Column(Unicode(150))
    application_id=Column(Integer)
    app_name=Column(Unicode(100))
    unidades=relationship('DatosUnidad', backref='aseguradora')


    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.aseguradora.like('%'+filter+'%')).filter_by(application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_aseguradora": item.id_aseguradora, "aseguradora": item.aseguradora, "internal_id": item.internal_id,"contacto": item.contacto,"telefono": item.telefono,"direccion": item.direccion,
                 "contacto2": item.contacto2,"celular": item.celular,"telefonoemergencia": item.telefonoemergencia,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.aseguradora.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_aseguradora": item.id_aseguradora, "aseguradora": item.aseguradora, "internal_id": item.internal_id,"contacto": item.contacto,"telefono": item.telefono,"direccion": item.direccion,
                 "contacto2": item.contacto2,"celular": item.celular,"telefonoemergencia": item.telefonoemergencia,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ",internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_aseguradora": item.id_aseguradora, "aseguradora": item.aseguradora, "internal_id": item.internal_id,"contacto": item.contacto,"telefono": item.telefono,"direccion": item.direccion,
                 "contacto2": item.contacto2,"celular": item.celular,"telefonoemergencia": item.telefonoemergencia,"application_id": item.application_id,"app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_aseguradora": item.id_aseguradora, "aseguradora": item.aseguradora, "internal_id": item.internal_id,"contacto": item.contacto,"telefono": item.telefono,"direccion": item.direccion,
                 "contacto2": item.contacto2,"celular": item.celular,"telefonoemergencia": item.telefonoemergencia,"application_id": item.application_id,"app_name": item.app_name})
        return hd

class Motivo(DeclarativeBase):
        __tablename__ = 'motivo'
        id_motivo = Column(Integer, primary_key=True)
        motivo = Column(Unicode(50))
        internal_id = Column(Integer)
        application_id = Column(Integer)
        app_name = Column(Unicode(100))


        @classmethod
        def filterApp(cls, filter, id):
            handler = DBSession.query(cls).filter(cls.motivo.like('%' + filter + '%')).filter_by(
                application_id=id).all()
            hd = []
            for item in handler:
                hd.append(
                    {"id_motivo": item.id_motivo, "motivo": item.motivo,"internal_id": item.internal_id, "application_id": item.application_id,
                     "app_name": item.app_name})
            return hd

        @classmethod
        def filterInternal(cls, filter, id):
            handler = DBSession.query(cls).filter(cls.motivo.like('%' + filter + '%')).filter_by(
                internal_id=id).all()
            hd = []
            for item in handler:
                hd.append({"id_motivo": item.id_motivo, "motivo": item.motivo,"internal_id": item.internal_id, "application_id": item.application_id,
                           "app_name": item.app_name})
            return hd

        @classmethod
        def allApp(cls, internal_id):
            if internal_id == '0':
                handler = DBSession.query(cls).all()
            else:
                handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
                print("METODO DENTRO DE TABLES internal_id tiene: ", internal_id)
            hd = []
            for item in handler:
                hd.append(
                    {"id_motivo": item.id_motivo, "motivo": item.motivo,"internal_id": item.internal_id, "application_id": item.application_id,
                     "app_name": item.app_name})
            return hd

        @classmethod
        def allInternal(cls, internal_id):
            if internal_id == '0':
                handler = DBSession.query(cls).all()
            else:
                handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
            hd = []
            for item in handler:
                hd.append({"id_motivo": item.id_motivo, "motivo": item.motivo,"internal_id": item.internal_id, "application_id": item.application_id,
                           "app_name": item.app_name})
            return hd

class BeneficiarioMoral(DeclarativeBase):
    __tablename__ = 'beneficiariomoral'
    id_beneficiariomoral = Column(Integer, primary_key=True)
    beneficiariomoral = Column(Unicode(50))
    telefono=Column(Unicode(20))
    domicilio=Column(Unicode(100))
    representante_legal=Column(Unicode(50))
    rfc=Column(Unicode(60))
    email=Column(Unicode(30))
    banco=Column(Unicode(25))
    cta_bancaria=Column(Unicode(50))
    banco2=Column(Unicode(25))
    cta_bancaria2=Column(Unicode(50))
    internal_id = Column(Integer)
    application_id=Column(Integer)
    app_name=Column(Unicode(100))


    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.beneficiariomoral.like('%' + filter + '%')).filter_by(
            application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_beneficiariomoral": item.id_beneficiariomoral, "beneficiariomoral": item.beneficiariomoral, "telefono": item.telefono,
                 "domicilio" : item.domicilio,"representante_legal" : item.representante_legal,"rfc": item.rfc,"email":
                item.email,"banco" : item.banco,
                 "cta_bancaria": item.cta_bancaria,"banco2" : item.banco2,
                 "cta_bancaria2": item.cta_bancaria2, "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.beneficiariomoral.like('%' + filter + '%')).filter_by(
            internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_beneficiariomoral": item.id_beneficiariomoral, "beneficiariomoral": item.beneficiariomoral, "telefono": item.telefono,
                 "domicilio" : item.domicilio,"representante_legal" : item.representante_legal,"rfc": item.rfc,"email":
                item.email,"banco" : item.banco,
                 "cta_bancaria": item.cta_bancaria,"banco2" : item.banco2,
                 "cta_bancaria2": item.cta_bancaria2, "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ", internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_beneficiariomoral": item.id_beneficiariomoral, "beneficiariomoral": item.beneficiariomoral, "telefono": item.telefono,
                 "domicilio" : item.domicilio,"representante_legal" : item.representante_legal,"rfc": item.rfc,"email":
                item.email,"banco" : item.banco,
                 "cta_bancaria": item.cta_bancaria,"banco2" : item.banco2,
                 "cta_bancaria2": item.cta_bancaria2, "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_beneficiariomoral": item.id_beneficiariomoral, "beneficiariomoral": item.beneficiariomoral, "telefono": item.telefono,
                 "domicilio" : item.domicilio,"representante_legal" : item.representante_legal,"rfc": item.rfc,"email":
                item.email,"banco" : item.banco,
                 "cta_bancaria": item.cta_bancaria,"banco2" : item.banco2,
                 "cta_bancaria2": item.cta_bancaria2, "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

class Periodo(DeclarativeBase):
    __tablename__ = 'periodo'
    id_periodo = Column(Integer, primary_key=True)
    periodo = Column(Unicode(50))
    internal_id = Column(Integer)
    application_id = Column(Integer)
    app_name = Column(Unicode(100))

    @classmethod
    def filter(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.periodo.like('%'+filter+'%')).filter_by(internal_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_periodo": item.id_periodo, "periodo": item.periodo, "internal_id": item.internal_id})
        return hd

    @classmethod
    def all(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_periodo": item.id_periodo, "periodo": item.periodo, "internal_id": item.internal_id})
        return hd

class Punto(DeclarativeBase):
    __tablename__ = 'punto'
    id_punto = Column(Integer, primary_key=True)
    punto = Column(Unicode(50))
    internal_id = Column(Integer)
    application_id = Column(Integer)
    app_name = Column(Unicode(100))
    #monto=Column(Integer)
    checadores=relationship('Persona', backref='punto')


    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.punto.like('%' + filter + '%')).filter_by(
            application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_punto": item.id_punto, "punto": item.punto , "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.punto.like('%' + filter + '%')).filter_by(
            internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_punto": item.id_punto, "punto": item.punto , "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ", internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_punto": item.id_punto, "punto": item.punto , "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_punto": item.id_punto, "punto": item.punto , "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

class HistorialUnidades(DeclarativeBase):
    __tablename__='historialunidades'
    id_historialunidades=Column(Integer,primary_key=True)
    fecha=Column(DateTime,default=datetime.now)
    dato=Column(Unicode(50))
    valor_nuevo=Column(Unicode(80))
    valor_anterior=Column(Unicode(80))
    sobrecampo = Column(Unicode(30))
    usuario=Column(Unicode(50))

class HistorialPersona(DeclarativeBase):
    __tablename__ = 'historialpersona'
    id_historialpersona = Column(Integer, primary_key=True)
    fecha = Column(DateTime, default=datetime.now)
    dato = Column(Unicode(50))
    valor_nuevo = Column(Unicode(80))
    valor_anterior = Column(Unicode(80))
    sobrecampo = Column(Unicode(30))
    usuario = Column(Unicode(50))

class Linea(DeclarativeBase):
    __tablename__='linea'
    id_linea=Column(Integer,primary_key=True)
    nombre_linea=Column(Unicode(50))
    internal_id=Column(Integer)
    app_id=Column(Integer)
    app_name=Column(Unicode(50))
    unidades = relationship('DatosUnidad', backref='linea')

# class AbonoCuenta(DeclarativeBase):
#     __tablename__ = 'abonocuenta'
#     id_abonocuenta=Column(Integer,primary_key=True)
#     fechaingreso=Column(DateTime,default=datetime.now())
#     fechacuenta=Column(Date)
#     unidad_id = Column(Integer)
#     empresa=Column(Unicode(100))
#     grupo=Column(Unicode(100))
#     permisionario=Column(Unicode(100))
#     derrotero=Column(Unicode(100))
#     operador_id = Column(Integer)
#     montoabono=Column(Integer)
#     montoperdonado=Column(Integer)
#     total=Column(Integer)
#     tipoabono = Column(Unicode(1))
#     observaciones=Column(Unicode(25))
#     cancelado = Column(Integer)
#     usuario = Column(Unicode(25))
#     internal_id=Column(Integer)

class PagoHead(DeclarativeBase):
    __tablename__='pago_head'
    pago_id=Column(Integer,ForeignKey('abonocuenta.id_abonocuenta'),primary_key=True)
    head_id=Column(Integer,ForeignKey('head.id_head'),primary_key=True)
    caracter=Column(Unicode(5))
    monto=Column(Integer)

class PagoBody(DeclarativeBase):
    __tablename__='pago_body'
    pago_id=Column(Integer,ForeignKey('abonocuenta.id_abonocuenta'),primary_key=True)
    body_id=Column(Integer,ForeignKey('body.id_body'),primary_key=True)
    caracter=Column(Unicode(5))
    monto=Column(Integer)

class AbonoCuenta(DeclarativeBase):
    __tablename__ = 'abonocuenta'
    id_abonocuenta=Column(Integer,primary_key=True)
    cancelado=Column(Integer)
    fechaingreso=Column(Date)
    fechacuenta=Column(Date)
    unidad_id=Column(Integer,ForeignKey('datosunidad.id_unidad'))
    operador_id=Column(Integer,ForeignKey('persona.id_persona'))
    totalapagaruni=Column(Integer)
    montoingresado=Column(Integer)
    faltante=Column(Integer)
    total=Column(Integer)
    totalapagarope=Column(Integer)
    montoingresadoope=Column(Integer)
    faltanteope=Column(Integer)
    totalope=Column(Integer)
    observaciones=Column(Unicode(250))
    usuario = Column(Unicode(50))
    internal_id=Column(Integer)
    app_name=Column(Unicode(100))
    permisionario=Column(Integer)
    application_id=Column(Integer)

class UnidadOperador(DeclarativeBase):
    __tablename__='unidad_operador'
    id_unidadoperador=Column(Integer,primary_key=True)
    id_unidad=Column(Integer)
    id_operador=Column(Integer)

class EstadoCuenta(DeclarativeBase):
    __tablename__='estadocuenta'
    id_estadocuenta=Column(Integer,primary_key=True)
    fecha_movimiento=Column(Date)
    motivo=Column(Unicode(50))
    cargo=Column(Integer)
    abono=Column(Integer)
    saldo=Column(Integer)
    unidad_id=Column(Integer)
    operador_id=Column(Integer)
    internal_id=Column(Integer)

class GrupoPersona(DeclarativeBase):
    __tablename__='grupopersona'
    id_grupopersona=Column(Integer,primary_key=True)
    grupopersona=Column(Unicode(50))
    descripcion=Column(Unicode(100))
    application_id=Column(Integer)
    app_name=Column(Unicode(100))
    internal_id=Column(Integer)


    @classmethod
    def filterApp(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.grupopersona.like('%' + filter + '%')).filter_by(
            application_id=id).all()
        hd = []
        for item in handler:
            hd.append(
                {"id_grupopersona": item.id_grupopersona, "grupopersona": item.grupopersona, "descripcion": item.descripcion, "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def filterInternal(cls, filter, id):
        handler = DBSession.query(cls).filter(cls.grupopersona.like('%' + filter + '%')).filter_by(
            internal_id=id).all()
        hd = []
        for item in handler:
            hd.append({"id_grupopersona": item.id_grupopersona, "grupopersona": item.grupopersona, "descripcion": item.descripcion, "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

    @classmethod
    def allApp(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(application_id=internal_id).all()
            print("METODO DENTRO DE TABLES internal_id tiene: ", internal_id)
        hd = []
        for item in handler:
            hd.append(
                {"id_grupopersona": item.id_grupopersona, "grupopersona": item.grupopersona, "descripcion": item.descripcion, "internal_id": item.internal_id,
                 "application_id": item.application_id,
                 "app_name": item.app_name})
        return hd

    @classmethod
    def allInternal(cls, internal_id):
        if internal_id == '0':
            handler = DBSession.query(cls).all()
        else:
            handler = DBSession.query(cls).filter_by(internal_id=internal_id).all()
        hd = []
        for item in handler:
            hd.append({"id_grupopersona": item.id_grupopersona, "grupopersona": item.grupopersona, "descripcion": item.descripcion, "internal_id": item.internal_id,
                       "application_id": item.application_id,
                       "app_name": item.app_name})
        return hd

class Condonaciones(DeclarativeBase):
    __tablename__='condonaciones'
    id_condonacion=Column(Integer,primary_key=True)
    cancelado=Column(Integer)
    fechainicial=Column(Date)
    fechafinal=Column(Date)
    descripcion=Column(Unicode(100))
    para=Column(Unicode(20))
    monto=Column(Integer)
    internal_id=Column(Integer)
    usuario=Column(Unicode(50))
    application_id=Column(Integer)

class Head(DeclarativeBase):
    __tablename__='head'
    id_head=Column(Integer,primary_key=True)
    fecha=Column(Date)
    estatus=Column(Unicode(50))
    id_unidad=Column(Integer,ForeignKey('datosunidad.id_unidad'))
    id_operador=Column(Integer,ForeignKey('persona.id_persona'))
    porpagar = Column(Integer)
    pagado=Column(Integer)
    faltante=Column(Integer)
    internal_id=Column(Integer)
    app_name=Column(Unicode(200))
    application_id=Column(Integer)

class Body(DeclarativeBase):
    __tablename__='body'
    id_body=Column(Integer,primary_key=True)
    fecha = Column(Date)
    id_unidad=Column(Integer, ForeignKey('datosunidad.id_unidad'))
    id_cargounidad=Column(Integer,ForeignKey('cargounidad.id_cargounidad'))
    id_operador=Column(Integer, ForeignKey('persona.id_persona'))
    id_cargopersona=Column(Integer,ForeignKey('cargooperador.id_cargooperador'))
    id_head=Column(Integer,ForeignKey('head.id_head'))
    monto=Column(Integer)
    condonacion=Column(Integer)
    saldo=Column(Integer)
    pagado=Column(Integer)
    internal_id = Column(Integer)
    espoliza=Column(Integer)
    application_id = Column(Integer)
    #app_name=Column(Unicode(50))

class CuentaCargo(DeclarativeBase):
    __tablename__='cuentacargo'
    id_cuenta=Column(Integer,primary_key=True)
    nombre=Column(Unicode(100))
    total=Column(Integer)
    internal_id=Column(Integer)
    cargooperador_id=Column(Integer,ForeignKey('cargooperador.id_cargooperador'))
    cargounidad_id=Column(Integer,ForeignKey('cargounidad.id_cargounidad'))
    app_name=Column(Unicode(100))
    application_id=Column(Integer)

class Correos(DeclarativeBase):
    __tablename__='correo'
    id_correo=Column(Integer,primary_key=True)
    correo=Column(Unicode(50))
    id_persona=Column(Integer,ForeignKey('persona.id_persona'))

class PruebasCron(DeclarativeBase):
    __tablename__='pruebascron'
    id_cron=Column(Integer,primary_key=True)
    nombre=Column(Unicode(20))

class HistoricoUnidad(DeclarativeBase):
    __tablename__='historialunidadoperador'
    id_historialunidadoperador=Column(Integer,primary_key=True)
    fechaasignada=Column(DateTime,default=datetime.now())
    unidad_id=Column(Integer,ForeignKey('datosunidad.id_unidad'))
    id_operador=Column(Integer,ForeignKey('persona.id_persona'))

class Poliza(DeclarativeBase):
    __tablename__='poliza'
    id_poliza=Column(Integer,primary_key=True)
    archivo=Column(Unicode(250))
    archivofile = Column(LargeBinary(length=(2 ** 32) - 1))
    montototal=Column(Integer)
    periodo=Column(Unicode(20))
    montodividido=Column(Integer)
    fecha_inicio=Column(Date)
    fecha_fin=Column(Date)
    internal_id=Column(Integer)
    estado=Column(Unicode(15))
    id_unidad=Column(Integer,ForeignKey('datosunidad.id_unidad'))

class Salida(DeclarativeBase):
    __tablename__='salida'
    id_salida=Column(Integer,primary_key=True)
    cancelacion=Column(Integer)
    fecha=Column(Date,default=datetime.now().date())
    tipo=Column(Unicode(20))
    monto = Column(Integer)
    internal_id=Column(Integer)
    app_name=Column(Unicode(100))
    application_id=Column(Integer)
    id_motivo=Column(Integer,ForeignKey('motivo.id_motivo'))
    id_beneficiariomoral = Column(Integer, ForeignKey('beneficiariomoral.id_beneficiariomoral'))
    id_persona = Column(Integer, ForeignKey('persona.id_persona'))
    cuentacargo_id = Column(Integer,ForeignKey('cuentacargo.id_cuenta'))
    concepto=Column(Unicode(100))

class Pets(DeclarativeBase):
    __tablename__ = 'pets'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(16), default=u'', nullable=False)
    breed = Column(Unicode(16), default=u'', nullable=False)
    foto = Column(LargeBinary(length=(2 ** 32) - 1))
