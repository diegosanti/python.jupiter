<head>
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
</head>

<script>
    function verseccionesbilling() {
        var por = document.getElementById("jupiter_opcionoperadorounidadbilling").value;
        if (por == 'Unidad') {
            document.getElementById("trdeunidadbilling").style.display = 'block';
            document.getElementById("trdeoperadorbilling").style.display = 'none';
            document.getElementById("trdenounidadnioperador").style.display = 'none';
            document.getElementById("trdepermisionariobilling").style.display = 'none';
            document.getElementById("fechanormalbilling").style.display='block';
            document.getElementById("fechapormesbilling").style.display='none';
        } else {
            if (por == 'Operador') {
                document.getElementById("trdeunidadbilling").style.display = 'none';
                document.getElementById("trdeoperadorbilling").style.display = 'block';
                document.getElementById("trdenounidadnioperador").style.display = 'none';
                document.getElementById("trdepermisionariobilling").style.display = 'none';
                document.getElementById("fechanormalbilling").style.display='block';
                document.getElementById("fechapormesbilling").style.display='none';
            } else {
                if (por == 'Permisionario') {
                    document.getElementById("trdeunidadbilling").style.display = 'none';
                    document.getElementById("trdeoperadorbilling").style.display = 'none';
                    document.getElementById("trdenounidadnioperador").style.display = 'none';
                    document.getElementById("trdepermisionariobilling").style.display = 'block';
                    document.getElementById("fechanormalbilling").style.display='block';
                    document.getElementById("fechapormesbilling").style.display='none';
                }

                else {
                    if (por == 'Periodo') {
                        document.getElementById("trdeunidadbilling").style.display = 'none';
                    document.getElementById("trdeoperadorbilling").style.display = 'none';
                    document.getElementById("trdenounidadnioperador").style.display = 'none';
                    document.getElementById("trdepermisionariobilling").style.display = 'none';
                    document.getElementById("fechanormalbilling").style.display='none';
                     document.getElementById("fechapormesbilling").style.display='block';


                    }else{
                    document.getElementById("trdeunidadbilling").style.display = 'none';
                    document.getElementById("trdeoperadorbilling").style.display = 'none';
                    document.getElementById("trdenounidadnioperador").style.display = 'block';
                    document.getElementById("trdepermisionariobilling").style.display = 'none';
                    document.getElementById("fechanormalbilling").style.display='block';
                     document.getElementById("fechapormesbilling").style.display='none';
                    if (por == 'Derrotero' || por == 'Grupo' || por == 'Empresa') {
                        $.ajax({
                            type: "GET",
                            url: "${h.url()}/bodyhead/traemedog?por=" + por + "&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                            contentType: "application/json; charset=utf-8",
                            data: {},
                            success: function (parameterdata) {
                                $('#jupiter_porname option').remove();
                                for (var k in parameterdata['data']) {
                                    if (k == 0) {
                                        $('#jupiter_porname').append('<option selected>' + parameterdata['data'][k]['value'] + '</option>');
                                    } else {
                                        $('#jupiter_porname').append('<option>' + parameterdata['data'][k]['value'] + '</option>');
                                    }
                                }
                            },
                            error: function () {
                                alert("ERROR AJAX");
                            },
                            complete: function () {
                            }
                        });

                    }
                }
            }
            }
        }
    }


      function actualizaselectunidadbilling(e) {
               if (e.keyCode === 13 && !e.shiftKey) {
               var unidad = document.getElementById("jupiter_ecobillingenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectunidad?unidad="+unidad+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_ecobilling option').remove();
                        $('#jupiter_ecobilling').append('<option>-Seleccione-</option>');
                         $('#jupiter_ecobilling').append('<option>TODAS</option>');
                       for (var k in parameterdata['data']) {
                        if(k==0) {
                            $('#jupiter_ecobilling').append('<option selected>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }else{
                            $('#jupiter_ecobilling').append('<option>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectunidadbilling");
                   },
                   complete: function () {
                   }
               });
           }
        }

        function actualizaselectoperadorbilling(e) {
                if (e.keyCode === 13 && !e.shiftKey) {
            var operador = document.getElementById("jupiter_operadorbillingenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectoperador"+"?operador="+operador+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_operadorbilling option').remove();
                       $('#jupiter_operadorbilling').append('<option>-Seleccione-</option>');
                       $('#jupiter_operadorbilling').append('<option>TODOS</option>');
                       for (var k in parameterdata['data']) {
                           if(k==0) {
                               $('#jupiter_operadorbilling').append('<option selected>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }else{
                               $('#jupiter_operadorbilling').append('<option>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectoperadorbilling");
                   },
                   complete: function () {
                   }
               });
        }

          }


                function actualizaselectpermisionariobilling(e) {
                if (e.keyCode === 13 && !e.shiftKey) {
            var permisionario = document.getElementById("jupiter_permisionariobillingenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectpermi"+"?operador="+permisionario+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_permisionariobilling option').remove();
                       $('#jupiter_permisionariobilling').append('<option>-Seleccione-</option>');
                       $('#jupiter_permisionariobilling').append('<option>TODOS</option>');
                       for (var k in parameterdata['data']) {
                           if(k==0) {
                               $('#jupiter_permisionariobilling').append('<option selected>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }else{
                               $('#jupiter_permisionariobilling').append('<option>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectpermisionariobilling");
                   },
                   complete: function () {
                   }
               });
        }

          }







                     function VentanadeCorreos2() {

                var winHeight = Math.round(window.innerHeight * .70);
                    var winWidth = Math.round(window.innerWidth * .70);
                    var por=document.getElementById("jupiter_opcionoperadorounidadbilling").value;
                var unidad=document.getElementById("jupiter_ecobilling").value;
                var operador=document.getElementById("jupiter_operadorbilling").value;

                    var addFilter02Buttons = {

                        "Send": function () {

                            var file_path = '${h.url()}'+document.getElementById("the_filenamebilling").value;
                                   $("#jupiter_correosForm2").validate({
                                rules: {
                                    correo1_jupiterabono: {
                                        required: true
                                    }


                                }
                            });

                                   if($("#jupiter_correosForm2").valid()) {

                                       var correo1 = $('#correo1_jupiterabono').val();
                                       var correo3 = $('#correo3_jupiterabono').val();

                                       $.ajax({
                                           type: "GET",
                                           url: "${h.url()}/abono/sendemail2?correo1="+correo1+"&correo3="+correo3+"&file_path="+file_path+
                                                "&por="+por+"&unidad="+unidad+"&operador="+operador+"&internal_id=${internal_id}&application_id=${application_id}",
                                           contentType: "application/json; charset=utf-8",
                                           data: {},
                                           success: function (parameterdata) {

                                               $.alert(parameterdata['error'], {autoClose: true, type: 'success',});
                                               Dialog02.dialog("close");
                                           },
                                           error: function () {
                                               $.alert('error en sendemail', {autoClose: false, type: 'danger',});
                                           },
                                           complete: function () {
                                           }
                                       });
                                   }

                        },
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogcorreos2").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/abono/formulariocorreos2?por="+por+"&unidad="+unidad+"&operador="+operador+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            $("#dialogcorreos2").html(parameterdata.template);
                            Dialog02.data('rowId', 1);
                            Dialog02.dialog("open");


                        },
                        error: function () {
                            alert("ERROR VentanadeCorreos2");
                        },
                        complete: function () {
                        }
                    });
              }

          function jupiter_mysearchbilling() {
                var fechainicial=document.getElementById("jupiter_fechabilling").value;
                var fechafinal=document.getElementById("jupiter_fechabillingfinal").value;
                var por=document.getElementById("jupiter_opcionoperadorounidadbilling").value;
                var unidad=document.getElementById("jupiter_ecobilling").value;
                var operador=document.getElementById("jupiter_operadorbilling").value;
                var otro=document.getElementById("jupiter_porname").value;
                var permisionario=document.getElementById("jupiter_permisionariobilling").value;
                var mes1=document.getElementById("jupiter_fechabillingmes").value;
                var mes2=document.getElementById("jupiter_fechabillingfinalmes").value;


                   var winHeight = Math.round(window.innerHeight * .85);
                    var winWidth = Math.round(window.innerWidth * .95);

                    var addFilter02Buttons = {
                        "Export to PDF": function () {
                            //alert();
                            var file_path = '${h.url()}'+document.getElementById("the_filenamebilling").value;
                            //window.open(file_path);
                            var link = document.createElement("a");
                            link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                            link.href = file_path;
                            link.target="_blank";
                            link.setAttribute('download',"download");
                            link.click();

                        },
                        "Export to CSV": function () {
                            //alert();
                            var file_path = '${h.url()}/${application_id}_'+document.getElementById("the_filenamecsvbilling").value;
                            //window.open(file_path);
                            var link = document.createElement("a");
                            link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                            link.href = file_path;
                            link.target="_blank";
                            link.setAttribute('download',"download");
                            link.click();

                        },
                        <%doc>
                        "Send Email": function () {
                            VentanadeCorreos2();
                          },</%doc>
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogbillinghtml").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/bodyhead/billingstatement?fechainicial="+fechainicial+"&fechafinal="+fechafinal+"&por="+por+"&unidad="+unidad+"&operador="+operador+"&otro="+otro+"&permisionario="+permisionario+
                                "&internal_id=${internal_id}&app_name=${app_name}&user=${user}&application_id=${application_id}&mes1="+mes1+"&mes2="+mes2,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            if(parameterdata['error']=='ok') {
                                $("#dialogbillinghtml").html(parameterdata.template);
                                Dialog02.data('rowId', 1);
                                Dialog02.dialog("open");
                                document.getElementById("the_filenamebilling").value=parameterdata['file_name'];
                                document.getElementById("the_filenamecsvbilling").value=parameterdata['file_namecsv'];
                            }else{
                                 $.alert("Seleccione operador o unidad", {type: "warning"});
                            }

                        },
                        error: function () {
                          $.alert("Error accessing server jupiter_mysearchbilling", {type: "danger"});
                        },
                        complete: function () {
                        }
                    });


            }
            
            function jupiter_mysearchbillingpersonal() {

                var fechainicial=document.getElementById("jupiter_fechabilling").value;
                var fechafinal=document.getElementById("jupiter_fechabillingfinal").value;
                var por=document.getElementById("jupiter_opcionoperadorounidadbilling").value;
                var unidad=document.getElementById("jupiter_ecobilling").value;



                   var winHeight = Math.round(window.innerHeight * .85);
                    var winWidth = Math.round(window.innerWidth * .95);

                    var addFilter02Buttons = {
                        "Export to PDF": function () {
                            //alert();
                            var file_path = '${h.url()}'+document.getElementById("the_filenamebilling").value;
                            //window.open(file_path);
                            var link = document.createElement("a");
                            link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                            link.href = file_path;
                            link.target="_blank";
                            link.setAttribute('download',"download");
                            link.click();

                        },
                        "Export to CSV": function () {
                            //alert();
                            var file_path = '${h.url()}/${application_id}_'+document.getElementById("the_filenamecsvbilling").value;
                            //window.open(file_path);
                            var link = document.createElement("a");
                            link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                            link.href = file_path;
                            link.target="_blank";
                            link.setAttribute('download',"download");
                            link.click();

                        },
                        <%doc>
                        "Send Email": function () {
                            VentanadeCorreos2();
                          },</%doc>
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogbillinghtml").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/bodyhead/billingstatementpersonal?fechainicial="+fechainicial+"&fechafinal="+fechafinal+"&por="+por+"&unidad="+unidad+
                                "&internal_id=${internal_id}&app_name=${app_name}&user=${user}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            if(parameterdata['error']=='ok') {
                                $("#dialogbillinghtml").html(parameterdata.template);
                                Dialog02.data('rowId', 1);
                                Dialog02.dialog("open");
                                document.getElementById("the_filenamebilling").value=parameterdata['file_name'];
                                document.getElementById("the_filenamecsvbilling").value=parameterdata['file_namecsv'];
                            }else{
                                 $.alert("Seleccione operador o unidad", {type: "warning"});
                            }

                        },
                        error: function () {
                          $.alert("Error accessing server jupiter_mysearchbillingpersonal", {type: "danger"});
                        },
                        complete: function () {
                        }
                    });
                
              }
</script>

<table id="fechapormesbilling" style="display: none">
    <tr style="height:30px";>
        <td>${_('Initial Date: ')}</td><td><input type="month" value="${fechames}" id="jupiter_fechabillingmes" name="jupiter_fechabillingmes"></td>
        <td>${_('Final Date: ')}</td><td><input type="month" value="${fechames}" id="jupiter_fechabillingfinalmes" name="jupiter_fechabillingfinalmes" ></td>
    </tr>
</table>

<table id="fechanormalbilling">
    <tr style="height:30px";>
        <td>${_('Initial Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechabilling" name="jupiter_fechabilling"></td>
        <td>${_('Final Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechabillingfinal" name="jupiter_fechabillingfinal" ></td>
    </tr>
</table>
<table>
 %if perms['Sec1 Billing']:
    <tr>
       <td><select id="jupiter_opcionoperadorounidadbilling" name="jupiter_opcionoperadorounidadbilling" onchange="verseccionesbilling();">
        <option>Unidad</option><option>Operador</option><option>Empresa</option><option>Derrotero</option><option>Grupo</option>
        <option>Permisionario</option>
           %if app_name!='TODO':
           <%doc><option>Periodo</option></%doc>
            %endif
       </select></td>
    </tr>
 </table>
        <table id="trdeunidadbilling">
     <tr style="height:30px;">
         <td><a id="jupiterlabeleco" style="display: block;">${_('Eco: ')}</a></td><td><input type="text" id="jupiter_ecobillingenter" name="jupiter_ecobillingenter" onkeypress="actualizaselectunidadbilling(event);" style="display: block;"></td><td><select id="jupiter_ecobilling" name="jupiter_ecobilling" style="display: block;">
                                            <option>-Seleccione-</option>
                                            <option>TODAS</option>
                                            %for item in unidades:
                                                    <option>${item.eco1}</option>
                                            %endfor
                            </select></td>

    </tr>
        </table>
        <table id="trdeoperadorbilling" style="display: none;">

     <tr style="height:30px;">
         <td><a id="jupiterlabeloperador">${_('Operator: ')}</a></td><td><input type="text" id="jupiter_operadorbillingenter" name="jupiter_operadorbillingenter" onkeypress="actualizaselectoperadorbilling(event);"></td><td><select id="jupiter_operadorbilling" name="jupiter_operadorbilling">
         <option>-Seleccione-</option>
         <option>TODOS</option>
         %for item in operadores:
         <option>${item.nombre}</option>
         %endfor
     </select></td>
   </tr>
        </table>
            <table id="trdepermisionariobilling" style="display: none;">

     <tr style="height:30px;">
         <td><a id="jupiterlabelpermisionario">${_('Permisionario: ')}</a></td><td><input type="text" id="jupiter_permisionariobillingenter" name="jupiter_permisionariobillingenter" onkeypress="actualizaselectpermisionariobilling(event);"></td><td><select id="jupiter_permisionariobilling" name="jupiter_permisionariobilling">
         <option>-Seleccione-</option>
         <option>TODOS</option>
         %for item in permisionarios:
         <option>${item.nombre}</option>
         %endfor
     </select></td>
   </tr>
        </table>




<table id="trdenounidadnioperador" style="display: none;">
        <tr>
        <td></td><td><select id="jupiter_porname" name="jupiter_porname">
            %for item in queryempresa:
            <option>${item.nombre_linea}</option>
            %endfor
        </select>
        </td>
    </tr>
        </table>

 <table>

    <tr style="height:30px";>
        <td></td><td><button onclick="jupiter_mysearchbilling()"><span class="glyphicon glyphicon-search"></span></button>
    </td>
    </tr>

        </table>

    %else:


         <tr>
        <td></td><td><select id="jupiter_opcionoperadorounidadbilling" name="jupiter_opcionoperadorounidadbilling">
        <option>Unidad</option></select></td>
    </tr>
     <tr id="trdeunidadbilling" style="height:30px; visibility: visible;">
         <td>${_('Eco: ')}</td><td><select id="jupiter_ecobilling" name="jupiter_ecobilling">
                                                <option>TODAS</option>
                                            %for item in unidades:
                                                    <option>${item.eco1}</option>
                                            %endfor
                            </select></td>
    </tr>
      <tr style="height:30px";>
        <td></td><td><button onclick="jupiter_mysearchbillingpersonal()"><span class="glyphicon glyphicon-search"></span></button>
    </td>
    </tr>
     </table>

    %endif




<div id="dialogbillinghtml" title="Billing Statement"></div>
<input type="text" hidden id="the_filenamebilling">
<input type="text" hidden id="the_filenamecsvbilling">
<div id="dialogcorreos2" title="Correo"></div>