<h3 align="center">Periodo de ${fechainicial} a ${fechafinal}</h3>
<br>
<br>


%if por=='UNIDAD':


        <table border="1" style="width: 100%"> 

                    <tr> 
                        <th>Total por Pagar</th> 
                        <th>Pagado</th> 
                        <th>Faltante</th> 
                    </tr> 
                    <tr style="text-align: center;">  
                        <td>$${listasupertotal['supertotalporpagar']}.00</td> 
                        <td>$${listasupertotal['supertotalpagado']}.00</td> 
                        <td>$${listasupertotal['supertotalfaltante']}.00</td>  
                    </tr>

                      </table>
            <br><br>


%for itemquien in quien:

          <table border="1" style="width: 100%">
        <tr style="text-align: center;">
            <th>Permisionario</th>
            <th>Unidad</th>
            <th>Dias Recaudados</th>
        </tr>
        <tr style="text-align: center;">
            <td>${itemquien['persona']}</td>
            <td><b>${itemquien['eco']}</b></td>
            <td>${itemquien['pagos']}</td>
        </tr>

        %for itemhead in queryheads:
             %for itemhead2 in itemhead:
                 %if itemhead2.id_unidad==itemquien['id']:
                          <tr style="text-align: center;">
                            <th>Fecha</th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
                        <tr bgcolor="#AAAAAA" style="text-align: center;">
                            <td>${itemhead2.fecha}</td>
                            <td>$${itemhead2.porpagar}.00</td>
                            <td>$${itemhead2.pagado}.00</td>
                            <td>$${itemhead2.faltante}.00</td>
                        </tr>
                 %endif
             %endfor
        %endfor
                    <tr style="text-align: center;">
                            <th></th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
              <tr style="text-align: center;">
                  <td><b>TOTAL</b></td>
                  <td>$${itemquien['totalporpagar']}.00</td>
                  <td>$${itemquien['totalpagado']}.00</td>
                  <td>$${itemquien['totalfaltante']}.00</td>
              </tr>

      </table>

      <br>
      <br>
  %endfor

%elif por=='OPERADOR':

        <table border="1" style="width: 100%"> 

                    <tr> 
                        <th>Total por Pagar</th> 
                        <th>Pagado</th> 
                        <th>Faltante</th> 
                    </tr> 
                    <tr style="text-align: center;">  
                        <td>$${listasupertotal['supertotalporpagar']}.00</td> 
                        <td>$${listasupertotal['supertotalpagado']}.00</td> 
                        <td>$${listasupertotal['supertotalfaltante']}.00</td>  
                    </tr>

                      </table>
            <br><br>

%for itemquien in quien:
    <table border="1" style="width: 100%">
        <tr style="text-align: center;">
            <th>Operador</th>
            <th>Numero Operador</th>
            <th>Dias Recaudados</th>
        </tr>
        <tr style="text-align: center;">
            <td>${itemquien['nombre']}</td>
            <td>${itemquien['numerooperador']}</td>
            <td>${itemquien['pagos']}</td>
        </tr>

        %for itemhead in queryheads:
             %for itemhead2 in itemhead:
                 %if itemhead2.id_operador==itemquien['id']:
                          <tr style="text-align: center;">
                            <th>Fecha</th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
                        <tr bgcolor="#AAAAAA" style="text-align: center;">
                            <td>${itemhead2.fecha}</td>
                            <td>$${itemhead2.porpagar}.00</td>
                            <td>$${itemhead2.pagado}.00</td>
                            <td>$${itemhead2.faltante}.00</td>
                        </tr>
                 %endif
             %endfor
        %endfor
                    <tr style="text-align: center;">
                            <th></th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
              <tr style="text-align: center;">
                  <td><b>TOTAL</b></td>
                  <td>$${itemquien['totalporpagar']}.00</td>
                  <td>$${itemquien['totalpagado']}.00</td>
                  <td>$${itemquien['totalfaltante']}.00</td>
              </tr>

      </table>

      <br>
      <br>
  %endfor

%else:
<%
    depermi=0
%>

    %for itemquien in quien:
            %if depermi!=itemquien['idpermi']:
                 %for itemlistapermi in listadepermisionario:
                     %if itemlistapermi['idpermi']==itemquien['idpermi']:
                <table border="1" style="width: 100%"> 
                    <tr>
                        <th>${itemlistapermi['nombre']}</th>
                          <th>Unidades: ${itemlistapermi['cuantasunidadesson']}</th>

                    </tr>
                    <tr> 
                        <th>Total por Pagar</th> 
                        <th>Pagado</th> 
                        <th>Faltante</th> 
                    </tr> 
                    <tr style="text-align: center;">  
                        <td>$${itemlistapermi['supertotalporpagar']}.00</td> 
                        <td>$${itemlistapermi['supertotalpagado']}.00</td> 
                        <td>$${itemlistapermi['supertotalfaltante']}.00</td>  
                    </tr>

                      </table>
                         <%
                             depermi=itemlistapermi['idpermi']
                         %>
                     %endif
                %endfor
                %endif

                <br>
                <br>
          <table border="1" style="width: 100%">
        <tr style="text-align: center;">
            <th>Permisionario</th>
            <th>Unidad</th>
            <th>Dias Recaudados</th>
        </tr>
        <tr style="text-align: center;">
            <td>${itemquien['persona']}</td>
            <td><b>${itemquien['eco']}</b></td>
            <td>${itemquien['pagos']}</td>
        </tr>

        %for itemhead in queryheads:
             %for itemhead2 in itemhead:
                 %if itemhead2.id_unidad==itemquien['id']:
                          <tr style="text-align: center;">
                            <th>Fecha</th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
                        <tr bgcolor="#AAAAAA" style="text-align: center;">
                            <td>${itemhead2.fecha}</td>
                            <td>$${itemhead2.porpagar}.00</td>
                            <td>$${itemhead2.pagado}.00</td>
                            <td>$${itemhead2.faltante}.00</td>
                        </tr>
                 %endif
             %endfor
        %endfor
                    <tr style="text-align: center;">
                            <th></th>
                            <th>Por Pagar</th>
                            <th>Pagado</th>
                            <th>Faltante</th>
                        </tr>
              <tr style="text-align: center;">
                  <td><b>TOTAL</b></td>
                  <td>$${itemquien['totalporpagar']}.00</td>
                  <td>$${itemquien['totalpagado']}.00</td>
                  <td>$${itemquien['totalfaltante']}.00</td>
              </tr>

      </table>

      <br>
      <br>

     %endfor


%endif
