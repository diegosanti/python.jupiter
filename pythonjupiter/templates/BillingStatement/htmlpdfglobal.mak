<h3 align="center">Periodo de ${fechainicial} al ${fechafinal}</h3>
<h3 align="center">${para}</h3>
<table border="1" style="width: 100%">
    <tr>
        <th>Total Por Pagar</th>
        <th>Total Pagado</th>
        <th>Total Faltante</th>
    </tr>
    <tr style="text-align: center;">
        <td>$${supertotalporpagar}.00</td>
        <td>$${supertotalpagado}.00</td>
        <td>$${supertotalfaltante}.00</td>
    </tr>
</table>

<table style="width: 100%">
    %for item in quien:
    <tr>
        <td><b><h3>${item.eco1}</h3></b></td>
    </tr>

        <tr>
            <th>Total Por Pagar</th>
            <th>Total Pagado</th>
            <th>Total Faltante</th>
        </tr>

   <tr bgcolor="#AAAAAA">
        %for item2 in listatotalesporpagar:
            %if item.id_unidad==item2['unidad']:

                <td>$${item2['totalporpagar']}.00</td>

            %endif
          %endfor


    %for item2 in listatotalespagado:
        %if item.id_unidad==item2['unidad']:

        <td>$${item2['totalpagado']}.00</td>

        %endif
      %endfor


        %for item2 in listatotalesfaltante:
            %if item.id_unidad==item2['unidad']:

            <td>$${item2['totalfaltante']}.00</td>

            %endif
          %endfor

        </tr>
        <tr>
         <th>Fecha</th>
        <th>Por Pagar</th>
        <th>Pagado</th>
        <th>Faltante</th>
        </tr>

%for item3 in listaheads:
 %for item4 in item3['queryheads']:
     %if item.id_unidad==item3['unidad']:
        <tr>
            <td>${item4.fecha}</td>
            <td>$${item4.porpagar}.00</td>
            <td>$${item4.pagado}.00</td>
             <td>$${item4.faltante}.00</td>

        </tr>
     %endif

    %endfor
    %endfor

    %endfor
</table>