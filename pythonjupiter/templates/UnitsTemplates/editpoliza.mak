<script>
    function calculopolizaseguroevent2(e) {
        //alert(e.value);

        var totalmonto=parseInt(document.getElementById("jupiter_montototalpoliza2").value);
        var periodo=document.getElementById("jupiter_selectperiodpoliza2").value;
        var montodividido=0;
        if(periodo=='anual'){
             montodividido=totalmonto/365;
        }
        if(periodo=='semestral'){
             montodividido=totalmonto/183;

        }
        if(periodo=='mensual'){
             montodividido=totalmonto/30;

        }
          if(periodo==''){
             montodividido=0;

        }

        document.getElementById("jupiter_montocalculadopoliza2").value=montodividido.toFixed(3);
        //document.getElementById("jupiter_montocalculadopoliza").value=Math.round(montodividido.toFixed(3));
        //e.preventDefault();
      }

        function calculopolizaseguro2() {
        var totalmonto=parseInt(document.getElementById("jupiter_montototalpoliza2").value);
        var periodo=document.getElementById("jupiter_selectperiodpoliza2").value;
        var montodividido=0;
        if(periodo=='anual'){
             montodividido=totalmonto/365;
        }
        if(periodo=='semestral'){
             montodividido=totalmonto/183;

        }
        if(periodo=='mensual'){
             montodividido=totalmonto/30;

        }
          if(periodo==''){
             montodividido=0;

        }

         document.getElementById("jupiter_montocalculadopoliza2").value=montodividido.toFixed(3);
        //document.getElementById("jupiter_montocalculadopoliza").value=Math.round(montodividido.toFixed(3));
            calculofechafinpoliza();

      }

      function calculofechafinpoliza2() {
          var date=document.getElementById("jupiter_fechainiciopoliza2").value;
          var periodo=document.getElementById("jupiter_selectperiodpoliza2").value;
           $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/calculofechafinpoliza?date="+date+"&periodo="+periodo,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                            if(parameterdata['error']=='ok'){
                                document.getElementById("jupiter_fechafinpoliza").value=parameterdata['fechafin'];
                            }

                            },
                        error: function () {
                                alert("Error accessing server /opendiv")
                            },
                        complete: function () {
                            }
                        });


        }

        function verseccionfilepoliza2() {
            if(document.getElementById("textodefilepoliza2").innerText!='Cancelar') {
                document.getElementById("trdefilepoliza2").style.visibility = 'visible';
                document.getElementById("textodefilepoliza2").innerText='Cancelar';
                document.getElementById("jupiter_filepoliza2").required=true;
            }else{
                document.getElementById("trdefilepoliza2").style.visibility = 'hidden';
                document.getElementById("textodefilepoliza2").innerText='Edit File';
                document.getElementById("jupiter_filepoliza2").required=false;

            }
          }
</script>

<form action='${h.url()}/unidades/saveeditpoliza?internal_id=${internal_id}&id_unidad=${idunidad}&user=${user}&id_poliza=${id_poliza}' name="formnewpoliza" id="formnewpoliza" method="POST" enctype="multipart/form-data" target="elframedepoliza" accept-charset="UTF-8">
    <table>
        <tr>
            <td><a id="textodefilepoliza" onclick="verseccionfilepoliza2();">Edit File</a></td>
        </tr>
        <tr id="trdefilepoliza" style="height: 35px;visibility: hidden">
            <td>${_('File')}: </td><td><input type="file" name="jupiter_filepoliza2" id="jupiter_filepoliza2"></td>
        </tr>
        <tr style="height: 35px;">
            <td>${_('Period')}: </td><td><select id="jupiter_selectperiodpoliza2" name="jupiter_selectperiodpoliza2" onchange="calculopolizaseguro2();" required>
                    %if periodo=='anual':
                    <option value="anual" selected>ANUAL</option>
                         <option value="semestral" >SEMESTRAL</option>
                        <option value="mensual" >MENSUAL</option>
                        %elif periodo=='semestral':
                        <option value="anual" >ANUAL</option>
                    <option value="semestral" selected>SEMESTRAL</option>
                        <option value="mensual" >MENSUAL</option>
                        %elif periodo=='mensual':
                        <option value="anual" >ANUAL</option>
                         <option value="semestral" >SEMESTRAL</option>
                    <option value="mensual" selected>MENSUAL</option>
                        %endif
                    </select></td>
        </tr>
        <tr style="height: 35px;">
            <td>${_('Amount')}: </td><td><input type="number" name="jupiter_montototalpoliza2" id="jupiter_montototalpoliza2" value="${montototal}" required onkeyup="calculopolizaseguroevent2(this);"></td>
        </tr>

         <tr style="height: 35px;">
        <td>Fecha Inicial de la poliza: </td><td><input type="date" name="jupiter_fechainiciopoliza2" id="jupiter_fechainiciopoliza2" value="${fecha_inicio}" required onchange="calculofechafinpoliza2();"></td>
        </tr>
            <tr style="height: 35px;">
        <td>Fecha Fin de la poliza: </td><td><input type="date" name="jupiter_fechafinpoliza2" id="jupiter_fechafinpoliza2" value="${fecha_fin}" readonly></td>
        </tr>
        <tr style="height: 35px;">
        <td>El monto diario sera: </td><td><input type="number" name="jupiter_montocalculadopoliza2" id="jupiter_montocalculadopoliza2" value="${montodividido}" readonly required></td>
        </tr>
      <%doc>  <tr>
            <td><input type="checkbox" id="jupiter_checkcargopoliza"></td><td>${_('Cargo')}: </td>
        </tr></%doc>
        <tr style="height: 45px;">
        <td><button type="submit" class="btn btn-primary">GUARDAR</button></td>
        </tr>
    </table>
</form>

<iframe id="elframedepoliza" name="elframedepoliza" hidden></iframe>