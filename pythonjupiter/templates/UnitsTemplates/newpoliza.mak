<script>
    function calculopolizaseguroevent(e) {
        //alert(e.value);

        var totalmonto=parseInt(document.getElementById("jupiter_montototalpoliza").value);
        var periodo=document.getElementById("jupiter_selectperiodpoliza").value;
        var montodividido=0;
        if(periodo=='anual'){
             montodividido=totalmonto/365;
        }
        if(periodo=='semestral'){
             montodividido=totalmonto/183;

        }
        if(periodo=='mensual'){
             montodividido=totalmonto/30;

        }
          if(periodo==''){
             montodividido=0;

        }

        document.getElementById("jupiter_montocalculadopoliza").value=montodividido.toFixed(3);
        //document.getElementById("jupiter_montocalculadopoliza").value=Math.round(montodividido.toFixed(3));
        //e.preventDefault();
      }

        function calculopolizaseguro() {
        var totalmonto=parseInt(document.getElementById("jupiter_montototalpoliza").value);
        var periodo=document.getElementById("jupiter_selectperiodpoliza").value;
        var montodividido=0;
        if(periodo=='anual'){
             montodividido=totalmonto/365;
        }
        if(periodo=='semestral'){
             montodividido=totalmonto/183;

        }
        if(periodo=='mensual'){
             montodividido=totalmonto/30;

        }
          if(periodo==''){
             montodividido=0;

        }

         document.getElementById("jupiter_montocalculadopoliza").value=montodividido.toFixed(3);
        //document.getElementById("jupiter_montocalculadopoliza").value=Math.round(montodividido.toFixed(3));
            calculofechafinpoliza();

      }

      function calculofechafinpoliza() {
          var date=document.getElementById("jupiter_fechainiciopoliza").value;
          var periodo=document.getElementById("jupiter_selectperiodpoliza").value;
           $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/calculofechafinpoliza?date="+date+"&periodo="+periodo,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                            if(parameterdata['error']=='ok'){
                                document.getElementById("jupiter_fechafinpoliza").value=parameterdata['fechafin'];
                            }

                            },
                        error: function () {
                                alert("Error accessing server /opendiv")
                            },
                        complete: function () {
                            }
                        });


        }
</script>

%if puedes==1:
<form action='${h.url()}/unidades/savenewpoliza?internal_id=${internal_id}&id_unidad=${idunidad}&user=${user}&application_id=${application_id}' name="formnewpoliza" id="formnewpoliza" method="POST" enctype="multipart/form-data" target="elframedepoliza" accept-charset="UTF-8">
    <table>
        <tr style="height: 35px;">
            <td>${_('File')}: </td><td><input type="file" name="jupiter_filepoliza" id="jupiter_filepoliza" required></td>
        </tr>
        <tr style="height: 35px;">
            <td>${_('Period')}: </td><td><select id="jupiter_selectperiodpoliza" name="jupiter_selectperiodpoliza" onchange="calculopolizaseguro();" required>
                    <option value="">-SELECCIONE-</option>
                    <option value="anual">ANUAL</option>
                    <option value="semestral">SEMESTRAL</option>
                    <option value="mensual">MENSUAL</option>
                    </select></td>
        </tr>
        <tr style="height: 35px;">
            <td>${_('Amount')}: </td><td><input type="number" name="jupiter_montototalpoliza" id="jupiter_montototalpoliza" required onkeyup="calculopolizaseguroevent(this);"></td>
        </tr>

         <tr style="height: 35px;">
        <td>Fecha Inicial de la poliza: </td><td><input type="date" name="jupiter_fechainiciopoliza" id="jupiter_fechainiciopoliza" required onchange="calculofechafinpoliza();"></td>
        </tr>
            <tr style="height: 35px;">
        <td>Fecha Fin de la poliza: </td><td><input type="date" name="jupiter_fechafinpoliza" id="jupiter_fechafinpoliza" readonly></td>
        </tr>
        <tr style="height: 35px;">
        <td>El monto diario sera: </td><td><input type="number" name="jupiter_montocalculadopoliza" id="jupiter_montocalculadopoliza" readonly required></td>
        </tr>
      <%doc>  <tr>
            <td><input type="checkbox" id="jupiter_checkcargopoliza"></td><td>${_('Cargo')}: </td>
        </tr></%doc>
        <tr style="height: 45px;">
        <td><button type="submit" class="btn btn-primary">GUARDAR</button></td>
        </tr>
    </table>
</form>

<iframe id="elframedepoliza" name="elframedepoliza" hidden></iframe>
%else:
    <h2>La unidad cuenta con una poliza con fecha fin el ${queryexistepolizavigente.fecha_fin}</h2>

    %endif