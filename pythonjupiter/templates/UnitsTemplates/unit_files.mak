<table style="width: 100%">
    <tr>
        <th></th>
        <th>${_('FILE')}</th>
    </tr>
    <tr>
        <td>${_('Photo')}</td>
        <td><img src="${photoUnit}" width="100px" height="100px" style="max-width: 300px; max-height: 300px"/></td>
    </tr>
    <tr>
        <td>${_('Circulation Card')}</td>
        <td><img src="${card}" width="100px" height="100px" style="max-width: 300px; max-height: 300px"/></td>
    </tr>
</table>