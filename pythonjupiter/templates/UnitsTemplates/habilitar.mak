
<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>

<table class="userTable">

    <tr>
        <th>${'VIN'}</th>
        <th>${'ECO'}</th>
        <th>${'ACTION'}</th>
    </tr>

    %for item in allrecords:
        <tr>
        <td>${item.vin}</td>
            <td>${item.eco1}</td>
            <td><a onclick="openWindowunits('Habilitar Unidad','${h.url()}/unidades/enable?eco=${item.eco1}&id=${item.id_unidad}');"> <span class="glyphicon glyphicon-ok">${_('Enable')}</span></a></td>
        </tr>
    %endfor


</table>
