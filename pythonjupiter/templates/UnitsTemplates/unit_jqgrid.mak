<script>
    function formatImageJupiterUnit(cellValue, options, rowObject) {
        var imageHtml = "";
        if(cellValue != 0){
            imageHtml = '<img src="data:image/png;base64,'+cellValue+'" width="150px" height="100px" />';
        }else{
            imageHtml = '<img src="${h.url()}/img/carrosilueta.svg" style="width: 100px; height: 100px">';
        }
        return imageHtml;
    }
    function actionsJupiterUnit(cellvalue, options, rowObject) {
        i = options.rowId;
        var ed = '<button onClick="doDoubleClickJupiterUnit('+options.rowId+');"><span class="glyphicon glyphicon-edit" style="cursor: pointer;"></span></button><br>';
        var hi = '<button onClick="openWindow(\'Historial\',\'${h.url()}/unidades/historic?sobrecampo='+options.rowId+'\');"><span class="glyphicon glyphicon-th-list" style="cursor: pointer;"></span></button><br>';
        var arc = '<button onClick="openWindow(\'Archivos\',\'${h.url()}/unidades/files?id='+options.rowId+'\');"><span class="glyphicon glyphicon-paperclip" style="cursor: pointer;"></span></button><br>';
        var gen = '<button onClick="openWindow(\'Credencial\',\'${h.url()}/unidades/createCredential?id='+options.rowId+'\');"><span class="glyphicon glyphicon-credit-card" style="cursor: pointer;"></span></button><br>';

        return ed + hi + arc + gen;
    }
    $(window).on("resize", function () {
            var $gridUnit = $("#jqGridUnitJupiter"),
                newWidth = $gridUnit.closest(".ui-jqgrid").parent().width();
                $gridUnit.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameUnit = '#jqGridUnitJupiter';
    var grid_pagerUnit = '#listPagerUnitJupiter';
    var update_urlUnit='${h.url()}/unidades/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlUnit  ='${h.url()}/unidades/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsUnit = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlUnit,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsUnit = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlUnit,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsUnit = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlUnit,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsUnit = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlUnit,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsUnit = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlUnit,modal: true};
    var gridUnit = jQuery(grid_nameUnit);
            $(document).ready(function () {
                gridUnit.jqGrid({
                url: load_urlUnit,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Vin')}','${_('Plates')}','${_('Motor Number')}','${_('Year')}','${_('Model')}',
                    '${_('Brand')}','${_('Color')}','${_('Kilometer')}','${_('Traits')}','${_('Photo')}','${_('Photo Route')}','${_('Date of Admission')}',
                    '${_('Status')}','${_('Date of Status')}','${_('Observations')}','${_('Internal ID')}','${_('APP Id')}','${_('Application Name')}','${_('Is Unit')}',
                    '${_('Eco')} 1','${_('ECO')} 2','${_('Concesion')}','${_('Special Account')}','${_('Enabled')}','${_('Circulation Card')}','${_('QR Code')}',
                    '${_('id_linea')}','${_('id_tipounidad')}','${_('id_tipotarjeta')}','${_('id_derrotero')}',
                    '${_('id_grupo')}','${_('id_aseguradora')}','${_('id_persona')}','${_('Units')}',''],
                colModel: [
                    {name: 'id_unidad', index: 'id_unidad', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'vin',width:30, index: 'vin',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'placas', index: 'placas',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'numero_motor', index: 'numero_motor',width:90,align: 'center',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'anio',width:150, index: 'anio',align: 'center',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'modelo', index: 'modelo',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'marca', index: 'marca',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'color', index: 'color',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'kilometraje', index: 'kilometraje',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'rasgos', index: 'rasgos',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'foto', index: 'foto',align: 'center',hidden: false,formatter: formatImageJupiterUnit, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'ruta_foto', index: 'ruta_foto',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_ingreso', index: 'fecha_ingreso',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estatus', index: 'estatus',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_estatus', index: 'fecha_estatus',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'observaciones', index: 'observaciones',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_id', index: 'app_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name', index: 'app_name',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'esunidadtransportes', index: 'esunidadtransportes',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'eco1', index: 'eco1',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'eco2', index: 'eco2',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'concesion', index: 'concesion',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'cuentaespecial', index: 'cuentaespecial',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitado', index: 'habilitado',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'tarjetadecirculacion', index: 'esbeneficiario',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'codigoqr', index: 'codigoqr',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_linea', index: 'id_linea',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_tipounidad', index: 'id_tipounidad',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_tipotarjeta', index: 'id_tipotarjeta',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_derrotero', index: 'id_derrotero',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_grupo', index: 'id_grupo',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_aseguradora', index: 'id_aseguradora',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_persona', index: 'id_persona',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                    {name: 'act',index: 'act', align: 'center',title:"act",formatter: actionsJupiterUnit,width:50,sortable:false,search:false}
                ],
                pager: jQuery(grid_pagerUnit),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_unidad',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterUnit(rowId);
                },
                gridComplete: function (rowId){
                    var table = document.getElementById("jqGridUnitJupiter");
                    var rows = table.getElementsByTagName("tr");
                    for(i = 1; i < rows.length; i++){
                        var dates = rows[i].cells[11].innerHTML;
                        if (dates != 0){
                            if (dates== "LEJOS"){
                                rows[i].cells[11].innerHTML='<img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;">';
                            }
                            if (dates== "VENCIO"){
                                rows[i].cells[11].innerHTML='<img src="${h.url()}/img/red-cross.png" style="width: 45px; height: 45px;">';
                            }
                            if (dates== "VENCERA"){
                                rows[i].cells[11].innerHTML='<img src="${h.url()}/img/aviso3.svg" style="width: 50px; height: 50px;">';
                            }
                        }else{
                            rows[i].cells[11].innerHTML="";
                        }
                    }
                }
            });
            gridUnit.jqGrid('navGrid',grid_pagerUnit,{edit:false,add:false,del:false, search:true},
                            editParamsUnit,
                            addParamsUnit,
                            deleteParamsUnit,
                            searchParamsUnit,
                            viewParamsUnit);
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterUnit(0)
                    }
                });
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Credential')}",
                    caption: "${_('Credential')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridUnit.jqGrid ('getGridParam', 'selrow');
                        var id_unidad = gridUnit.jqGrid('getCell',RowId,'id_unidad');
                        if (id_unidad == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            openWindow("Credencial","${h.url()}/unidades/createCredential?id="+RowId);
                        }
                    }
                });
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Files')}",
                    caption: "${_('Files')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridUnit.jqGrid ('getGridParam', 'selrow');
                        var id_unidad = gridUnit.jqGrid('getCell',RowId,'id_unidad');
                        if (id_unidad == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            openWindow("Archivos","${h.url()}/unidades/files?id="+RowId);
                        }
                    }
                });
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Historial')}",
                    caption: "${_('Historial')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridUnit.jqGrid ('getGridParam', 'selrow');
                        var id_unidad = gridUnit.jqGrid('getCell',RowId,'id_unidad');
                        alert(id_unidad);
                        if (id_unidad == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{

                            openWindow("Historial","${h.url()}/unidades/historic?sobrecampo="+RowId);
                        }
                    }
                });
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Edit')}",
                    caption: "${_('Edit')}",
                    position: "first",
                    onClickButton: function(){
                        var selRowId = gridUnit.jqGrid ('getGridParam', 'selrow');
                        if (selRowId == null){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            doDoubleClickJupiterUnit(selRowId);
                        }

                    }
                });
            gridUnit.navButtonAdd(grid_pagerUnit,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterUnit(0)
                    }
                });
            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterUnit() {
                var rowid = $('#jqGridUnitJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/unidades/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridUnitJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /unidades/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterUnit(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/unidades/form?id='+rowId+'&application_id=${application_id}&user=${user}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogUnitJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogUnitJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /unidades/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogUnitJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.6),
                            width: Math.round(window.innerWidth*.8),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#UnitFormJupiter").valid()) {
                                        var info = $('#UnitFormJupiter').serialize();
                                        var input = document.querySelector('input[type=file]');
                                        var foto = input.files[0];
                                        var tarjetadecirculacion = input.files[1];
                                        var formData = new FormData();
                                        formData.append("tarjetadecirculacion", tarjetadecirculacion);
                                        formData.append("foto", foto);
                                        var request = new XMLHttpRequest();
                                        request.open("POST", '${h.url()}/unidades/save?'+info+"&application_id=${application_id}&app_name=${app_name}");
                                        request.send(formData);
                                        request.onload  = function() {
                                            var response = JSON.parse(request.responseText);
                                            if (response.error == "ok"){
                                                $.alert("${_('Done')}!",{type: "success"});
                                                $('#jqGridUnitJupiter').trigger( 'reloadGrid' );
                                                $('#UnitFormJupiter')[0].reset();
                                                ContDialog.dialog( "close" );
                                            }else{
                                                $.alert(response.error,{type: "warning"});
                                            }
                                        };
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#UnitFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#UnitFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogUnitJupiter"  title="${_('Unit')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridUnitJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerUnitJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->