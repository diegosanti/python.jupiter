<script>
$(document).ready(function() {
                         $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/unidades/getqr?id=${unidad.id_unidad}",
                                            contentType: "application/json; charset=utf-8",
                                            data: {},
                                            async: false,
                                            beforeSend: function() {
                                                //document.getElementById("salidademontomaxm"+elid[1]).innerHTML="<img src=\"${h.url()}/img/ajax-loader.gif\">";
                                            },
                                            success: function(data) {
                                                $("#elcodigocredencial").html(data['codigoqr']);
                                            },
                                            error: function() {
                                                 $.alert("${_('Error accessing to')} /unidades/getqr", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
});
</script>
<%doc>background="${h.url()}/img/autobusfix4.jpg"</%doc>
<table border="2"  style="background-repeat: no-repeat;" >
    <tr height="50px" width="300px">
         <td background="${h.url()}/img/fondocredencial3.jpg" style="text-align: center; height: 50px; width: 380px;">
             <h1><b>${empresa}</b></h1>
        %if unidad.ruta_foto!=None:
           <img src="${h.url()}/img/${internal_id}/unidades/${unidad.id_unidad}.-/${unidad.ruta_foto}" style="width: 120px; height: 120px">

        %else:
           <img src="${h.url()}/img/carrosilueta.png" style="width: 120px; height: 120px">
        %endif
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>ECO1:</b> <i>${unidad.eco1}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>ECO2:</b> <i>${unidad.eco2}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>VIN:</b> <i>${unidad.vin}</i></p>
             <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>PLACAS:</b> <i>${unidad.placas}</i></p>
             <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>MODELO:</b> <i>${unidad.modelo}</i></p>
             <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>MARCA:</b> <i>${unidad.marca}</i></p>
              <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>PERMISIONARIO:</b> <i>${permi}</i></p>
        </td>
        <td background="${h.url()}/img/fondocredencial3.jpg" style="text-align: center; height: 50px; width: 380px;">
            <p>QR</p>
            <img src="data:;base64,${codigoqr}" style="width: 220px; height: 220px">
        </td>
    </tr>
</table>
