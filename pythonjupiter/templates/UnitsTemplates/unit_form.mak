<form id="UnitFormJupiter" action="${h.url()}/personas/save">
    <input hidden type="text" id="jupiterUnit_id" name="jupiterUnit_id" value='${id}'>
    <fieldset>
        <legend>${_('Unit Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Eco')} 1:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_eco1" name="jupiterUnit_eco1" value="${handler.eco1}" required>
                </td>
                <td>
                    ${_('Eco')} 2:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_eco2" name="jupiterUnit_eco2" value="${handler.eco2}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('VIN')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_vin" name="jupiterUnit_vin" value="${handler.vin}">
                </td>
                <td>
                    ${_('Plates')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_placas" name="jupiterUnit_placas" value="${handler.placas}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Motor Number')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_numero_motor" name="jupiterUnit_numero_motor" value="${handler.numero_motor}">
                </td>
                <td>
                    ${_('Model')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_modelo" name="jupiterUnit_modelo" value="${handler.modelo}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Brand')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_marca" name="jupiterUnit_marca" value="${handler.marca}" required>
                </td>
               <td>
                    ${_('Color')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_color" name="jupiterUnit_color" value="${handler.color}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
               <td>
                    ${_('Kilometers')}:
                </td>
                <td>
                    <input type="text" id="jupiterUnit_kilometraje" name="jupiterUnit_kilometraje" value="${handler.kilometraje}">
                </td>
                <td>
                   ${_('Year')}:
                </td>
                <td>
                    <input type="number" id="jupiterUnit_anio" name="jupiterUnit_anio" value="${handler.anio}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                   ${_('Characteristics')}:
                </td>
                <td>
                    <textarea rows="3" cols="20" id="jupiterUnit_rasgos" name="jupiterUnit_rasgos" required>${handler.rasgos}</textarea>
                </td>
                <td>
                    ${_('Photo')}:
                </td>
                <td>
                    <input type="file" name="jupiterUnit_foto" id="jupiterUnit_foto" accept="image/jpg,image/png,image/jpeg"/><label style="font-size: 8px">${_('Only')} (jpeg,jpg or png)</label>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Date of Admission')}:
                </td>
                <td>
                    <input type="date" id="jupiterUnit_fecha_ingreso" name="jupiterUnit_fecha_ingreso" value="${handler.fecha_ingreso}" required>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterUnit_internal_id" name="jupiterUnit_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterUnit_owner" name="jupiterUnit_owner" value='${user}'>
    </fieldset><br>
    <fieldset>
        <legend>${_('Extra Information')} </legend>
        <table style="width:100%">
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Enterprise')}:
                </td>
                <td colspan="3">
                    <select id="jupiterUnit_id_linea" name="jupiterUnit_id_linea" >
                        <option value="0">No</option>
                    %for item in enterprice:
                        <option ${item['checked']} value="${item['id']}">${item['nombre_linea']}</option>
                    %endfor
                    </select>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Application Name')}:
                </td>
                <td colspan="3">
                    <select id="jupiterUnit_app_id" name="jupiterUnit_app_id" required>
                        <option value="${application_id}">${handler.app_name}</option>
                    </select>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Permisionario')}:
                </td>
                <td>
                    <select id="jupiterUnit_id_persona" name="jupiterUnit_id_persona" >
                        <option value="0">No</option>
                    %for item in permisionario:
                        <option ${item['checked']} value="${item['id']}">${item['nombre']}</option>
                    %endfor
                    </select>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Unit Type')}:
                </td>
                <td>
                    <select id="jupiterUnit_id_tipounidad" name="jupiterUnit_id_tipounidad" >
                        <option value="0">No</option>
                    %for item in tipounidad:
                        <option ${item['checked']} value="${item['id']}">${item['tipo_unidad']}</option>
                    %endfor
                    </select>
                </td>
                <td>
                    ${_('Derrotero')}:
                </td>
                <td>
                    <select id="jupiterUnit_id_derrotero" name="jupiterUnit_id_derrotero" >
                        <option value="0">No</option>
                    %for item in derrotero:
                        <option ${item['checked']} value="${item['id']}">${item['derrotero']}</option>
                    %endfor
                    </select>
                </td>

            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Card')}:
                </td>
                <td>
                    <select id="jupiterUnit_id_tipotarjeta" name="jupiterUnit_id_tipotarjeta" >
                        <option value="0">No</option>
                    %for item in tipotarjeta:
                        <option ${item['checked']} value="${item['id']}">${item['tipo_tarjeta']}</option>
                    %endfor
                    </select>
                </td>
                <td>
                    ${_('Grupo')}:
                </td>
                <td>
                    %for item in group:
                       <input type="checkbox" id="jupiterUnit_id_grupo" class="jupiterUnit_id_grupo" name="jupiterUnit_id_grupo"  ${item['checked']}>${item['grupo']}<br>
                    %endfor
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Assurance')}:
                </td>
                <td colspan="3">
                    <select id="jupiterUnit_assurance" name="jupiterUnit_assurance" >
                        <option value="0">No</option>
                    %for item in assurance:
                        <option ${item['checked']} value="${item['id']}">${item['aseguradora']}</option>
                    %endfor
                    </select>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                   ${_('Observations')}:
                </td>
                <td>
                    <textarea rows="5" cols="20" id="jupiterUnit_observaciones" name="jupiterUnit_observaciones">${handler.observaciones}</textarea>
                </td>
                <td>
                    ${_('Circulation Card')}:
                </td>
                <td>
                    <input type="file" name="jupiterUnit_tarjetadecirculacion" id="jupiterUnit_tarjetadecirculacion" accept="image/jpg,image/png,image/jpeg"/><label style="font-size: 8px">${_('Only')} (jpeg,jpg or png)</label>
                </td>
            </tr>
        </table>
    </fieldset>
</form>