
<script>

    function openeditpoliza(text,url) {
        var winHeight = Math.round(window.innerHeight * .85);
        var winWidth = Math.round(window.innerWidth * .55);

        var addFilter01Buttons = {

        };
        var Dialog03 = $("#dialogarchivopoliza").dialog({
            autoOpen: false,
            height: winHeight,
            width: winWidth,
            modal: true,
            buttons: addFilter01Buttons,
            close: function () {

            }
        });

        // Append html
        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogarchivopoliza").html(parameterdata.template);
                Dialog03.data('rowId', 1);
                Dialog03.dialog("open");

            },
            error: function () {
                alert("Error accessing server /smsTemplate")
            },
            complete: function () {
            }
        });
      }
    function jupiterabrirarchivopoliza(id) {
       var ur="${h.url()}/unidades/archivopoliza?id="+id+"&internal_id=${internal_id}&idunidad=${idunidad}";
        var winHeight = Math.round(window.innerHeight * .85);
        var winWidth = Math.round(window.innerWidth * .55);

        var addFilter01Buttons = {

        };
        var Dialog03 = $("#dialogarchivopoliza").dialog({
            autoOpen: false,
            height: winHeight,
            width: winWidth,
            modal: true,
            buttons: addFilter01Buttons,
            close: function () {

            }
        });

        // Append html
        $.ajax({
            type: "GET",
            url: ur,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogarchivopoliza").html(parameterdata.template);
                Dialog03.data('rowId', 1);
                Dialog03.dialog("open");

            },
            error: function () {
                alert("Error accessing server /smsTemplate")
            },
            complete: function () {
            }
        });
      }



      function jupiterabrirarchivotarjeta(tarjeta) {
          var ur="${h.url()}/unidades/tarjetadecirculacion?tarjeta="+tarjeta+"&internal_id=${internal_id}&idunidad=${idunidad}";
        var winHeight = Math.round(window.innerHeight * .85);
        var winWidth = Math.round(window.innerWidth * .55);

        var addFilter01Buttons = {

        };
        var Dialog03 = $("#dialogarchivopoliza").dialog({
            autoOpen: false,
            height: winHeight,
            width: winWidth,
            modal: true,
            buttons: addFilter01Buttons,
            close: function () {

            }
        });

        // Append html
        $.ajax({
            type: "GET",
            url: ur,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogarchivopoliza").html(parameterdata.template);
                Dialog03.data('rowId', 1);
                Dialog03.dialog("open");

            },
            error: function () {
                alert("Error accessing server /")
            },
            complete: function () {
            }
        });
        }

        function eliminarpoliza(url) {
        var confirmando=confirm("¿Estas Seguro que desea Eliminar la poliza?");
        if(confirmando) {
                    $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                if(parameterdata['tipo']!=1) {
                    $.alert("" + parameterdata['error'], {type: 'danger'});
                }else{
                     $.alert("" + parameterdata['error'], {type: 'success'});
                }
            },
            error: function () {
                $.alert("Error accessing server /",{type: 'danger'})
            },
            complete: function () {
            }
        });
            //$.alert("eliminare", {type: 'danger'});
        }
          }
</script>
<h3>Lista Polizas</h3>
<table style="width: 100%">
    <tr>
        <th></th>
        <th>No</th>
        <th>Periodo</th>
        <th>Monto Total</th>
        <th>Monto por Dia</th>
        <th>Fecha Inicio</th>
        <th>Fecha Fin</th>
        <th>Archivo</th>
        %if perms['Delete Poliza']:
            <th>Eliminar</th>
        %else:
            <th></th>
        %endif
    </tr>
    %for item in listapoliza:
        <tr>
            %if item['variableeditar']==0:
                %if perms['Edit Poliza']:
                    <td><a onclick="openeditpoliza('Editar Poliza','${h.url()}/unidades/editarpoliza?id=${item['id_poliza']}&user=${user}&internal_id=${internal_id}');"><span class="glyphicon glyphicon-edit"></span></a></td>
                %else:
                    <td></td>
                %endif
            %else:
                <td></td>
            %endif
            <td>${item['contador']}.-</td>
            <td>${item['periodo']}</td>
            <td>$${item['montototal']}.00</td>
            <td>$${item['montodividido']}.00</td>
             <td>${item['fecha_inicio']}</td>
            <td>${item['fecha_fin']}</td>
            <td><a onclick="jupiterabrirarchivopoliza(${item['id_poliza']});">
                <span class="glyphicon glyphicon-paperclip"></span></a></td>
            %if perms['Delete Poliza']:
                <td><a onclick="eliminarpoliza('${h.url()}/unidades/eliminarpoliza?id=${item['id_poliza']}');"><span class="glyphicon glyphicon-remove"></span></a></td>
            %else:
                <td></td>
            %endif
        </tr>
    %endfor


</table>


<h3>Tarjeta de Circulacion</h3>
<table style="width: 100%">
    <tr>
        <td>Tarjeta de Circulacion</td><td><a onclick="jupiterabrirarchivotarjeta('${tarjetacirculacion}');">
                <span class="glyphicon glyphicon-paperclip"></span></a></td>
    </tr>
</table>

<div id="dialogarchivopoliza" title="Archivo Poliza"></div>