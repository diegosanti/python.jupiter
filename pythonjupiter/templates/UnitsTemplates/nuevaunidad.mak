<html>
<%doc> <script src="https://code.jquery.com/jquery.js"></script>
  <script src="${tg.url('/javascript/bootstrap.min.js')}"></script></%doc>
<head>
    <style>
          .thumb {
            height: 300px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
          }
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.sombratr{
          background-color: #dddddd;

}

   <%doc>   tr:nth-child(even) {
        background-color: #dddddd;
    }</%doc>

</style>

</head>

        <script>

            var winHeighttogridecos = Math.round(window.innerHeight * .80);
            var winWidthtogridecos = Math.round(window.innerWidth * .60);

        var addFilter01Buttonstogridecos = {
            "Close": function () {
                Dialog01togridecos.dialog("close");
            }
        };

            var Dialog01togridecos = $("#dialogEcos").dialog({
            autoOpen: false,
            height: winHeighttogridecos ,
            width: winWidthtogridecos ,
            modal: true,
            buttons: addFilter01Buttonstogridecos,
            close: function () {

            }
        });

            function ecosClicked() {

        // Append html
        $.ajax({
            type: "GET",
            url: "${h.url()}/ecostemplate?client_id=${client_id}",
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogEcos").html(parameterdata.template);
                Dialog01togridecos.data('rowId',1);
                Dialog01togridecos.dialog("open");

            },
            error: function () {
                alert("Error accessing server /ecostemplate")
            },
            complete: function () {
            }
        });



}


                        function validarlineas() {
               var empresa=document.getElementById("jupiter_empresa").value;
               document.getElementById("seriedegrupos").value='';
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizalineas"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#jupiter_linea option').remove();
                    for (var k in parameterdata['lineas']) {
                         $("#jupiter_linea").append('<option value="' + parameterdata['lineas'][k]['nombre_linea'] + '">' + parameterdata['lineas'][k]['nombre_linea'] + '</option>');
                    }
                    validarderroteros();
                    validargrupo();

                },
                error: function () {
                        alert("ERROR");
                },
                complete: function () {
                 }
                });

              }



                          function validarderroteros() {
               var empresa=document.getElementById("jupiter_empresa").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizaderrotero"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#jupiter_derrotero option').remove();
                    for (var k in parameterdata['derroteros']) {
                         $("#jupiter_derrotero").append('<option value="' + parameterdata['derroteros'][k]['derrotero'] + '">' + parameterdata['derroteros'][k]['derrotero'] + '</option>');
                    }


                },
                error: function () {
                        alert("ERROR");
                },
                complete: function () {
                 }
                });

              }



            function validargrupo() {
               var empresa=document.getElementById("jupiter_empresa").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizagrupos"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#latabladegrupos td').remove();

                   for(var item in parameterdata['grupos']){

                       $('#latabladegrupos').append("<tr><td width=\"130\"></td><td><input type=\"checkbox\" name=\""+parameterdata['grupos'][item].grupo+"\" value=\""+parameterdata['grupos'][item].grupo+" \" " +
                                                "onchange=\"insertandoeninputoculto(this);\">"+parameterdata['grupos'][item].grupo+"</td></tr> ");
                   }


                    actualizandocargos();
                },
                error: function () {
                        alert("ERROR grupo");
                },
                complete: function () {
                 }
                });

              }



            function actualizandocargos() {
                var app_name=document.getElementById("jupiter_empresa").value;
                 var empresa=document.getElementById("jupiter_linea").value;
                  var derrotero=document.getElementById("jupiter_derrotero").value;
                   var grupo=document.getElementById("seriedegrupos").value;
                             $.ajax({
                        type: "GET",
                        url: "${h.url()}/cargosunidades/actualizacargosunidades"+"?empresa="+empresa+"&internal_id=${internal_id}&app_name="
                                    +app_name+"&derrotero="+derrotero+"&grupo="+grupo+"&application_id=${application_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                            $('#jupiter_tabladecargosenunidades td').remove();
                    for(var k in parameterdata['construir']){
                        //alert(parameterdata['construir'][k]);
                        $('#jupiter_tabladecargosenunidades').append('<tr><td><input type=\"text\" id=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\" name=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\"' +
                                'value=\"'+parameterdata['construir'][k]['cargo']+'\" readonly></td><td>Monto: <input name=\"montode'+parameterdata['construir'][k]['cargo']+'\" id=\"montode'+parameterdata['construir'][k]['cargo']+'\" type=\"number\" size=\"8\" value=\"'+parameterdata['construir'][k]['monto']+'\"></td></tr>');

                    }
                },
                error: function () {
                        alert("ERROR actualizando los cargos");
                },
                complete: function () {
                 }
                });
              }

                  function opentheTABS_jupiter(evt, Name,type) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");

        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(Name).style.display = "block";
        evt.currentTarget.className += " active";

            }


              function archivo(evt) {
                  var files = evt.target.files; // FileList object

                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("jupiter_fotoshow").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);

                    reader.readAsDataURL(f);
                  }
              }


              function insertandoeninputoculto(esto) {
                                  //alert(esto.value);
                  if (esto.checked == true) {
                      var esta = document.getElementById("seriedegrupos").value.split(",");
                      if (esta.includes(esto.value)) {

                      } else {
                          document.getElementById("seriedegrupos").value = document.getElementById("seriedegrupos").value+','+esto.value ;
                      }

                      //alert(document.getElementById("seriedegrupos").value);
                  }else{
                       var esta = document.getElementById("seriedegrupos").value;
                       var nuevacadena=esta.replace(','+esto.value,"");
                       document.getElementById("seriedegrupos").value=nuevacadena;
                       //alert(document.getElementById("seriedegrupos").value);


                  }
                  actualizandocargos();

                }


              document.getElementById('jupiter_fotounit').addEventListener('change', archivo, false);
      </script>
  <fieldset>
        <legend>${_('Add Unit')}</legend>
      <div class="tab">
  <button id="datos"  class="tablinks active"  onclick="opentheTABS_jupiter(event, 'losdatos',this)" >${_('DATA')}</button>
  <button id="cargos" class="tablinks" onclick="opentheTABS_jupiter(event, 'loscargos',this)" >${_('CHARGES')}</button>

</div>

      <output id="list"></output>
      <form action="${h.url()}/unidades/uploadUnit?internal_id=${internal_id}&user=${user}&application_id=${application_id}" name="imagesForm" id="imagesForm" method="POST" enctype="multipart/form-data" target="requestprueba" accept-charset="UTF-8">
          <p>* ${_('Required Files')}</p>
          <br>
           <div id="losdatos" class="tabcontent" style="display: block;">
            <p>${_('Upload Photo of the Unit')}:  ${_('Only')} (jpeg,bmp and png)</p>
        <input type="file" name="jupiter_fotounit" id="jupiter_fotounit" accept="image/vnd-wap-wbmp,image/png, image/jpeg" /><br/>
          <output id="jupiter_fotoshow"></output>
             <table>
                <tr style="display: none;">
                      <td>${_('Transport Unit: ')} &nbsp;</td><td><select name="jupiter_esunidad" id="jupiter_esunidad"><option>SI</option></select><br></td>
                 </tr>


                 <tr>
                     <td>*${_('ECO1: ')} </td>
                     <td><input id="jupiter_eco1" name="jupiter_eco1" maxlength="25" required></td><td><a onclick="ecosClicked();">Plataforma</a></td>
                 </tr>
                  <tr>
            <td>*${_('ECO2: ')} &nbsp;</td><td><input type="text" name="jupiter_eco2" length="50" maxlength="25" required><br></td>
        </tr>
                  <tr>
                <td>${_('Unit Type: ')}</td><td><select name="jupiter_tipo_unidad">
            %for item in typeunit:
            <option name="${item.tipo_unidad}">${item.tipo_unidad}</option>
            %endfor
            </select></td>
        </tr>
          <tr>
              <td>${_('App_Name: ')} &nbsp;</td><td><select id="jupiter_empresa" name="jupiter_empresa" length="50" value="0" onchange="validarlineas();"><br>
                                                    %for item in typeempresa:
                                                        <option>${item['name']}</option>
                                                    %endfor
                                                    </select></td>
        </tr>

          <tr>
              <td>${_('Empresa: ')} &nbsp;</td><td><select id="jupiter_linea" name="jupiter_linea" length="50" required onchange="actualizandocargos();"><br>
                                                %for item in typelinea:
                                                    <option>${item.nombre_linea}</option>
                                                %endfor
                                            </select></td>
        </tr>
                     <tr>
                     <td>${_('Derrotero: ')}</td><td>
                        <select name="jupiter_derrotero" id="jupiter_derrotero" onchange="actualizandocargos();">
                        %for item in typederrotero:
                            <option>${item.derrotero}</option>
                        %endfor

                        </select>

                 </td>
                 </tr>
      <%doc>                  <tr>
              <td>${_('Group: ')} &nbsp;</td><td><select id="jupiter_grupo" name="jupiter_grupo" length="50" required onchange="actualizandocargos();"><br>
                                            <option value="Sin grupo">Sin grupo</option>
                                                %for item in typegrupo:
                                                    <option value="${item.grupo}">${item.grupo}</option>
                                                %endfor
                                            </select></td>
        </tr></%doc>


                            <tr id="trdelosgrupos">
                                   <td>${_('Group: ')}</td>
                                <!--<select id="jupiteroperadores_grupo" name="jupiteroperadores_grupo" length="50" onchange="actualizandocargos();"></select><br>
                                                <option>Sin Grupo</option>-->
                                <input type="text" id="seriedegrupos" hidden>
                            </tr>
             </table>
               <table id="latabladegrupos">
                   %for item in typegrupo:
                       <tr><td width="130"></td><td><input type="checkbox" name="${item.grupo}" value="${item.grupo}" onchange="insertandoeninputoculto(this);">${item.grupo}</td></tr>
                   %endfor
             </table>

<table>
                             <tr>
                <td>${_('Unit Card: ')}</td><td><select name="jupiter_tipo_tarjeta">
            %for item in typecard:
            <option name="${item.tipo_tarjeta}">${item.tipo_tarjeta}</option>
            %endfor
            </select></td>
        </tr>
                         <tr>
                     <td>${_('Permisionario: ')}</td><td>
                        <select name="jupiter_permisionario" id="jupiter_permisionario">
                        %for item in allpermisionarios:
                            <option>${item.nombre}</option>
                        %endfor

                        </select>

                 </td>
                 </tr>
        <%doc>         <tr>

                 <td>${_('Cuenta Especial: ')}</td><td><input type="checkbox" name="jupiter_cuentaespecial" id="jupiter_cuentaespecial"> </td>
                 </tr></%doc>
         <tr style="display: none;">
            <td>${_('Concession: ')} &nbsp;</td><td><select name="jupiter_concesion"><option>${_('YES')}</option><option>${_('NO')}</option></select><br></td>
        </tr>
        <tr>
        <td>*${_('VIN: ')} &nbsp;</td><td><input type="text" name="jupiter_vin" length="50" maxlength="50" required><br/></td>
        </tr>

        <tr>
            <td>*${_('Plates: ')} &nbsp;</td><td><input type="text" name="jupiter_placas" length="50" maxlength="50" required><br></td>
        </tr>
         <tr>
            <td>*${_('Motor Number: ')} &nbsp;</td><td><input type="text" name="jupiter_numero_motor" length="50" maxlength="25" required><br></td>
        </tr>
         <tr>
            <td>*${_('Model: ')} &nbsp;</td><td><input type="text" name="jupiter_modelo" length="50" maxlength="50" required><br></td>
        </tr>
         <tr>
            <td>*${_('Brand: ')} &nbsp;</td><td><input type="text" name="jupiter_marca" length="50" maxlength="50" required><br></td>
        </tr>
         <tr>
            <td>${_('Color: ')} &nbsp;</td><td><input type="text" name="jupiter_color" length="50" maxlength="50" ><br></td>
        </tr>
         <tr>
            <td>${_('Kilometers: ')} &nbsp;</td><td><input type="text" name="jupiter_kilometraje" maxlength="10" length="50"><br></td>
        </tr>
         <tr>
            <td>${_('Characteristic: ')} &nbsp;</td><td><input type="text" name="jupiter_rasgos" length="50" maxlength="100"><br></td>
        </tr>
             <tr>
            <td>*${_('Circulation Card: ')} &nbsp;</td><td><input type="file" name="jupiterpersona_tarjetacirculacion" length="50" accept="application/pdf,image/vnd-wap-wbmp,image/png,image/jpeg" required><br></td>
        </tr>

          <%doc>  <tr>
            <td>${_('Status: ')} &nbsp;</td><td><select name="jupiter_estatus" id="jupiter_estatus"><option>ALTA</option><option>BAJA</option></select><br></td>
        </tr></%doc>
          <%doc>  <tr>
            <td>${_('Date Status: ')} &nbsp;</td><td><input type="date" name="jupiter_fecha_estatus"></td>
        </tr></%doc>

                        <tr>
                     <td>${_('Insurance Agency')}</td><td>
                        <select name="jupiter_aseguradora" id="jupiter_aseguradora">
                            <option value="Sin">Sin</option>
                        %for item in typeaseguradora:
                            <option value="${item.aseguradora}">${item.aseguradora}</option>
                        %endfor

                        </select>

                 </td>
                 </tr>

            <tr>
            <td>${_('Observations: ')} &nbsp;</td><td><textarea name="jupiter_observaciones" maxlength="200" style="margin: 0px; width: 422px; height: 150px;"></textarea></td>
        </tr>

    </table>
           </div>
          <div id="loscargos" class="tabcontent">
                         <legend>${_('CHARGES: ')}</legend>
<table id="jupiter_tabladecargosenunidades">
                 %for item in construir:
                     <tr>

                         <td><input type="text" id="jupitercargo${item['cargo']}" name="jupitercargo${item['cargo']}" value="${item['cargo']}" readonly></td><td>Monto: <input name="montode${item['cargo']}" id="montode${item['cargo']}" type="number" size="8" value="${item['monto']}"></td>

                    </tr>

                    %endfor
    </table>
<legend></legend>
              <input type="submit" name="jupiter_submit_uploadunidad" value="GUARDAR"/>
          </div>
          <br>

        </form>
      <br>
      <iframe name="requestprueba" id="requestprueba" frameborder="0"></iframe>

<div id="dialogEcos" name="dialogEcos"></div>



  </fieldset>
</html>