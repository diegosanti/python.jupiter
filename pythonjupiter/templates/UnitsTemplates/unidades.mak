
<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
   <%doc> <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script></%doc>

   <style type="text/css">
        a{
           color: black;
           text-decoration: none;
       }
   </style>
<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>

   <script type="text/javascript">

                         var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                 $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');



       function searchenter(e) {

    if (e.keyCode === 13 && !e.shiftKey)  {

        searchunit();

         }
       }

       function openWindowunits(text,url,id) {


           if(text!='Credencial') {

               var confirmar = 'nada';

               if (url.indexOf("disableunit") != -1) {
                   $('<div></div>').appendTo('body')
                           .html('<div><h6>¿Seguro desea deshabilitar esta unidad?</h6></div>')
                           .dialog({
                               modal: true, title: 'Deshabilitar Persona', zIndex: 10000, autoOpen: true,
                               width: 'auto', resizable: false,
                               buttons: {
                                   Yes: function () {
                                       // $(obj).removeAttr('onclick');
                                       // $(obj).parents('.Parent').remove();
                                       doAjax(url, 'openTab');
                                       var $dialog = $('<div></div>')
                                               .html('<div id="openTab" style="margin:10px"></div>')
                                               .dialog({
                                                   autoOpen: false,
                                                   modal: true,
                                                   height: 580,
                                                   width: 820,
                                                   resizable: false,
                                                   draggable: true,
                                                   position: {my: "center", at: "top", of: window},
                                                   close: function () {


                                                       $(this).dialog('destroy').remove();
                                                       var filterunits = document.getElementById("jupiter_filterunits").value;
                                                       if (filterunits != "") {
                                                           searchunit();
                                                       } else {
                                                           reloadallunits();
                                                       }

                                                   },
                                                   title: text,
                                                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                                                   buttons: {}
                                               });
                                       if (text != 'Editar Poliza') {
                                           $dialog.dialog('open');
                                       }

                                       confirmar = 'se';


                                       $(this).dialog("close");
                                   },
                                   No: function () {

                                       $(this).dialog("close");
                                   }
                               },
                               close: function (event, ui) {
                                   $(this).remove();
                               }
                           });
               } else {
                   if (confirmar == 'nada') {
                       doAjax(url, 'openTab');
                       var $dialog = $('<div></div>')
                               .html('<div id="openTab" style="margin:10px"></div>')
                               .dialog({
                                   autoOpen: false,
                                   modal: true,
                                   height: 580,
                                   width: 820,
                                   resizable: false,
                                   draggable: true,
                                   position: {my: "center", at: "top", of: window},
                                   close: function () {


                                       $(this).dialog('destroy').remove();
                                       var filterunits = document.getElementById("jupiter_filterunits").value;
                                       if (filterunits != "") {
                                           searchunit();
                                       } else {
                                           reloadallunits();
                                       }

                                   },
                                   title: text,
                                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                                   buttons: {}
                               });
                       if (text != 'Editar Poliza') {
                           $dialog.dialog('open');
                       }
                   }
               }
           }else{

            doAjax(url, 'openTab');
                                       var $dialog = $('<div></div>')
                                               .html('<div id="openTab" style="margin:10px"></div>')
                                               .dialog({
                                                   autoOpen: false,
                                                   modal: true,
                                                   height: 580,
                                                   width: 820,
                                                   resizable: false,
                                                   draggable: true,
                                                   position: {my: "center", at: "top", of: window},
                                                   close: function () {


                                                       $(this).dialog('destroy').remove();
                                             <%doc>          var filterunits = document.getElementById("jupiter_filterunits").value;
                                                       if (filterunits != "") {
                                                           searchunit();
                                                       } else {
                                                           reloadallunits();
                                                       }</%doc>

                                                   },
                                                   title: text,
                                                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                                                   buttons: {
                                                       <%doc>"PDF": function ()
                                                           {
                                                                       $.ajax({
                                                                                type: "GET",
                                                                                url: "${h.url()}/unidades/credencialpdf?id="+id+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                                                                                contentType: "application/json; charset=utf-8",
                                                                                data: {},
                                                                                success: function (parameterdata) {
                                                                                    //Insert HTML code

                                                                                    var link = document.createElement("a");
                                                                                    var file_path = '${h.url()}/'+parameterdata['file_name'];
                                                                                    link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                                                                                    link.href = file_path;
                                                                                    link.target="_blank";
                                                                                    link.setAttribute('download',"download");
                                                                                    link.click();


                                                                                },
                                                                                error: function () {
                                                                                    alert("Error accessing server /");
                                                                                },
                                                                                complete: function () {
                                                                                }
                                                                            });
                                                      <%doc>                $(this).dialog('destroy').remove();
                                                       var filterunits = document.getElementById("jupiter_filterunits").value;
                                                       if (filterunits != "") {
                                                           searchunit();
                                                       } else {
                                                           reloadallunits();
                                                       }
                                                        }
                                                       </%doc>



                                                   }
                                               });
                                        $dialog.dialog('open');
           }
       }


       function reloadallunits() {
            document.getElementById("jupiter_filterunits").value="";
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/unidades/reloadunits?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tableunits td').remove();
                      $("#bookmarkunidades").html(data['contador']+ " Unidad(es)");

                           for(var k in data['listabusca']){

                            if(data['listabusca'][k]['habilitado']=='ACTIVA'){
                                    var estadopoliza;
                                      if(data['listabusca'][k]['estatus']=='VENCERA') {
                                            estadopoliza='<td><img src="${h.url()}/img/aviso3.png" style="width: 70px; height: 70px;"></td>';
                                        }
                                        if (data['listabusca'][k]['estatus']=='BIEN') {
                                            estadopoliza='<td><img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;"></td>';
                                        }
                                                if (data['listabusca'][k]['estatus']=='VENCIO') {
                                                    estadopoliza='<td><img src="${h.url()}/img/red-cross.png" style="width: 70px; height: 70px;"></td>';
                                                }

                                          if (data['listabusca'][k]['estatus']=='SIN') {
                                                estadopoliza='<td><img src="${h.url()}/img/sin2.svg" style="width: 70px; height: 70px;"></td>';

                                        }

                                        if(data['listabusca'][k]['ruta_foto']==null){
                                          var fotouni="<td><img src=\"${h.url()}/img/carrosilueta.png\" style=\"width: 110px; height: 110px\"></td>";
                                        }else{
                                            var fotouni="<td><img src=\"${h.url()}/img/${internal_id}/unidades/"+data['listabusca'][k]['id_unidad']+".-/"+data['listabusca'][k]['ruta_foto']+"\" style=\"width: 110px; height: 110px\"> </td>";
                                        }
                            $('#jupiter_tableunits').append('<tr>' +
                                        %if perms['Edit Unit']:
                                         '<td><a onclick=\"openWindowunits(\'Editar Unidad\',\'${h.url()}\/unidades\/editunit?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                        %else:
                                            '<td></td>'+
                                        %endif
                                    '<td><a onclick=\"openWindowunits(\'Polizas\',\'${h.url()}\/unidades\/polizas?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><span class="glyphicon glyphicon-list-alt"></span></a></td>'+
                                    %if perms['AddPoliza']:
                                    '<td><a onclick=\"openWindowunits(\'New Poliza\',\'${h.url()}\/unidades\/templatenewpoliza?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><i class="fa fa-user-secret"></i></a></td>'+
                                    %else:
                                        '<td></td>'+
                                     %endif
                                    '<td><a onclick=\"openWindowunits(\'Mostrar Historial\',\'${h.url()}\/unidades\/historialde?id='+data['listabusca'][k]['id_unidad']+'\',0);\"> <span class="glyphicon glyphicon-th-list"></span></a></td>' +
                                      '<td><a onclick=\"openWindowunits(\'Credencial\',\'${h.url()}/unidades/generarcredencial?id='+data['listabusca'][k]['id_unidad']+'\','+data['listabusca'][k]['id_unidad']+');\"><span class=\"glyphicon glyphicon-credit-card\"></span></a></td>'+
                                       '<td><a>' + data['listabusca'][k]['id_unidad'] + '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['vin']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['placas']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['marca']+ '</a></td>' +
                                       fotouni+
                                       estadopoliza +
                                       '<td><a>' + data['listabusca'][k]['permisionario']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['observaciones']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_tipounidad']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['eco1']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['eco2']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['concesion']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_tipotarjeta']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['habilitado']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['nombre_empresa']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['nombre_linea']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_grupo']+ '</a></td>' +
                                            %if perms['Disable Unit']:
                                       '<td align=\"center\"><a onclick=\"openWindowunits(\'Deshabilitar Unidad\',\'${h.url()}\/unidades\/disableunit?id='+data['listabusca'][k]['id_unidad']+'&eco='+data['listabusca'][k]['eco1']+'\',0);\"><span class="glyphicon glyphicon-remove"></span></td>' +
                                            %endif
                                            '</tr>' );

                        }
                           }


                    },
                    error: function () {
                        $.alert("Error accessing searchlinea", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }

       function searchunit(){
           var searchfor = document.getElementById("jupiter_selectbuscar").value;
           var value = document.getElementById("jupiter_filterunits").value;
           $.ajax({
                    type: "GET",
                    url: "${h.url()}/unidades/searchunits?name="+value+"&searchfor="+searchfor+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tableunits td').remove();
                      $("#bookmarkunidades").html(data['contador']+ " Unidad(es)");

                        for(var k in data['listabusca']){

                            if(data['listabusca'][k]['habilitado']=='ACTIVA'){
                                var estadopoliza;
                                      if(data['listabusca'][k]['estatus']=='VENCERA') {
                                            estadopoliza='<td><img src="${h.url()}/img/aviso3.png" style="width: 70px; height: 70px;"></td>';
                                        }
                                        if (data['listabusca'][k]['estatus']=='BIEN') {
                                            estadopoliza='<td><img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;"></td>';
                                        }
                                                if (data['listabusca'][k]['estatus']=='VENCIO') {
                                                    estadopoliza='<td><img src="${h.url()}/img/red-cross.png" style="width: 70px; height: 70px;"></td>';
                                                }

                                          if (data['listabusca'][k]['estatus']=='SIN') {
                                                estadopoliza='<td><img src="${h.url()}/img/sin2.svg" style="width: 70px; height: 70px;"></td>';

                                        }
                                            if(data['listabusca'][k]['ruta_foto']==null){
                                          var fotouni="<td><img src=\"${h.url()}/img/carrosilueta.png\" style=\"width: 110px; height: 110px\"></td>";
                                        }else{
                                            var fotouni="<td><img src=\"${h.url()}/img/${internal_id}/unidades/"+data['listabusca'][k]['id_unidad']+".-/"+data['listabusca'][k]['ruta_foto']+"\" style=\"width: 110px; height: 110px\"> </td>";
                                        }


                            $('#jupiter_tableunits').append('<tr>' +
                                        %if perms['Edit Unit']:
                                        '<td><a onclick=\"openWindowunits(\'Editar Unidad\',\'${h.url()}\/unidades\/editunit?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                        %else:
                                            '<td></td>'+
                                        %endif
                                       '<td><a onclick=\"openWindowunits(\'Poliza\',\'${h.url()}\/unidades\/polizas?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><span class="glyphicon glyphicon-list-alt"></span></a></td>'+
                                    %if perms['AddPoliza']:
                                    '<td><a onclick=\"openWindowunits(\'Nueva Poliza\',\'${h.url()}\/unidades\/templatenewpoliza?id='+data['listabusca'][k]['id_unidad']+'\',0);\"><i class="fa fa-user-secret"></i></a></td>'+
                                    %else:
                                        '<td></td>'+
                                        %endif
                                    '<td><a onclick=\"openWindowunits(\'Mostrar Historial\',\'${h.url()}\/unidades\/historialde?id='+data['listabusca'][k]['id_unidad']+'\',0);\"> <span class="glyphicon glyphicon-th-list"></span></a></td>' +
                                     '<td><a onclick=\"openWindowunits(\'Credencial\',\'${h.url()}/unidades/generarcredencial?id='+data['listabusca'][k]['id_unidad']+'\','+data['listabusca'][k]['id_unidad']+');\"><span class=\"glyphicon glyphicon-credit-card\"></span></a></td>'+
                                    '<td><a>' + data['listabusca'][k]['id_unidad'] + '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['vin']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['placas']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['marca']+ '</a></td>' +
                                      fotouni+
                                        estadopoliza+
                                       '<td><a>' + data['listabusca'][k]['permisionario']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['observaciones']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_tipounidad']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['eco1']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['eco2']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['concesion']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_tipotarjeta']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['habilitado']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['nombre_empresa']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['nombre_linea']+ '</a></td>' +
                                       '<td><a>' + data['listabusca'][k]['id_grupo']+ '</a></td>' +
                                            %if perms['Disable Unit']:
                                       '<td align=\"center\"><a onclick=\"openWindowunits(\'Deshabilitar Unidad\',\'${h.url()}\/unidades\/disableunit?id='+data['listabusca'][k]['id_unidad']+'&eco='+data['listabusca'][k]['eco1']+'\',0);\"><span class="glyphicon glyphicon-remove"></span></td>' +
                                            %endif
                                            '</tr>' );


                        }
                        }


                    },
                    error: function () {
                        $.alert("Error accessing searchlinea", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });

        }
        function openWindowpoliza(title,url,idunidad) {
            var winHeight=Math.round(window.innerHeight*.7);
            var winWidth=Math.round(window.innerWidth*.6);

            var addFilter01Buttons = {
                "Add New": function () {
                    var winHeight2 = Math.round(window.innerHeight * .7);
                    var winWidth2 = Math.round(window.innerWidth * .6);
                    var addFilter01Buttons2 = {
                        "Save": function () {
                            $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/savenewpoliza?internal_id=${internal_id}&id_unidad="+idunidad,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                                success: function (parameterdata) {
                            //Insert HTML code
                            $("#polizadivlista2").html(parameterdata.template);
                            addFilter01Dialog.dialog("close");
                             addFilter01Dialog2.dialog("close");

                            },
                            error: function () {
                                alert("Error accessing server /opendiv")
                            },
                            complete: function () {
                            }
                        });

                        }


                        };
                         var addFilter01Dialog2 = $("#polizadivlista2" ).dialog({
                                autoOpen: false,
                                height: winHeight2-100,
                                width: winWidth2-200,
                                modal: true,
                                buttons: addFilter01Buttons2,
                                close: function() {
                                }
                        });
                                   $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/templatenewpoliza?internal_id=${internal_id}&id_unidad="+idunidad,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                                success: function (parameterdata) {
                            //Insert HTML code
                            $("#polizadivlista2").html(parameterdata.template);
                            addFilter01Dialog2.data('rowId', 1);
                             addFilter01Dialog2.dialog("open");

                            },
                            error: function () {
                                alert("Error accessing server /opendiv")
                            },
                            complete: function () {
                            }
                        });
                        }
                        };

                                var addFilter01Dialog = $("#polizadivlista" ).dialog({
                                autoOpen: false,
                                height: winHeight-100,
                                width: winWidth-200,
                                modal: true,
                                buttons: addFilter01Buttons,
                                close: function() {
                                }
                        });
                        //addFilter01Dialog.data('rowId',1);
                        //addFilter01Dialog.dialog( "open" );
                            $.ajax({
                        type: "GET",
                        url: url+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                                success: function (parameterdata) {
                            //Insert HTML code
                            $("#polizadivlista").html(parameterdata.template);
                            addFilter01Dialog.data('rowId', 1);
                             addFilter01Dialog.dialog("open");

                            },
                            error: function () {
                                alert("Error accessing server /opendiv")
                            },
                            complete: function () {
                            }
                        });

          }

          function jupiter_abrirnewpolizaunidad() {

            }

            function jupiter_verdivcargarexcel() {

                     document.getElementById("jupiter_addexcelform").style.visibility='visible';
                     var winHeight=Math.round(window.innerHeight*.7);
                    var winWidth=Math.round(window.innerWidth*.6);
                     var addFilter01Buttons = {
                        "Close": function() {

                            addFilter01Dialog.dialog( "close" );
                        }
            };
            var addFilter01Dialog = $( "#jupiter_addexcelform" ).dialog({
                    autoOpen: false,
                    height: winHeight-100,
                    width: winWidth-200,
                    modal: true,
                    buttons: addFilter01Buttons,
                    close: function() {

                    }
             });
            addFilter01Dialog.data('rowId',1);
            addFilter01Dialog.dialog( "open" );

                   // document.getElementById("earth_addfilter01Form").style.visibility='hidden';

              }


              $(function () {
                  $('#jupiter_formdeexcelunidades').on('submit',function (e) {
                      e.preventDefault();
                      var formulario=new FormData($(this)[0]);
                       $.ajax({
                        type: "POST",
                        url: "${h.url()}/unidades/unidadestsv",
                           processData: false,  // tell jQuery not to process the data
                        contentType: false ,  // tell jQuery not to set contentType
                        data: formulario,
                                success: function (parameterdata) {
                                //alert(parameterdata['error']);
                            },
                            error: function () {
                               // alert("Error accessing server /unidadestsv")
                            },
                            complete: function () {
                            }
                        });

                    });

                });

   </script>

                %if perms['Add Unit']:
                    <button style="float: left;" onclick="openWindowunits('Add Unit','${h.url()}/unidades/nuevaunidad',0);">
                        <span class="glyphicon glyphicon-plus">${_('NewUnit')}</span>
                    </button>
      <%doc>            <button style="float: left;" onclick="jupiter_verdivcargarexcel();">
                        <span class="glyphicon glyphicon-plus">${_('ExcelUnits')}</span>
                    </button></%doc>
                %endif

                %if perms['Enable Unit']:
                    <button style="float: left;">
                        <a onclick="openWindowunits('Habilitar Unidades','${h.url()}/unidades/habilitarunidad',0)"><span class="glyphicon glyphicon-ok">${_('EnableUnit')}</span></a>
                    </button>
                %endif
                    <br>
                <br>
            BUSCAR POR: <select id="jupiter_selectbuscar">
    <option>eco1</option>
    <option>eco2</option>
    <option>vin</option>
    <option>placas</option>
    <option>modelo</option>
    <option>marca</option>
    <option>nombre empresa</option>
    <option>permisionario</option>


</select>

<input type="text" id="jupiter_filterunits" onkeypress="searchenter(event);" placeholder="Unidades"><button onclick=searchunit()><i class="fa fa-search"></i></button><button onclick="reloadallunits()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    &nbsp;&nbsp;<a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: none;" ></a>

<div id="bookmarkunidades" class="bookmark" style="float: right;">${contador} Unidad(es)</div>

                    <table id="jupiter_tableunits" class="userTable">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        % for it in list:
                            <th>${it['show']}</th>
                        %endfor
                            %if perms['Disable Unit']:
                                <th>${_('DISABLE')}</th>
                            %endif
                        </tr>

                        % for it in hd:
                        %if it['HABILITADO']=='ACTIVA':
                        <tr>
                            %if perms['Edit Unit']:
                            <td> <a onclick="openWindowunits('Editar Unidad','${h.url()}/unidades/editunit?id=${it['id_unidad']}',0);"><span class="glyphicon glyphicon-edit"></span></a></td>
                            %else:
                            <td></td>
                            %endif
                           <td><a onclick="openWindowunits('Poliza','${h.url()}/unidades/polizas?id=${it['id_unidad']}',0);"><span class="glyphicon glyphicon-list-alt"></span></a></td>
                            %if perms['AddPoliza']:
                            <td><a onclick="openWindowunits('Nueva Poliza','${h.url()}/unidades/templatenewpoliza?id=${it['id_unidad']}',0);"><i class="fa fa-user-secret"></i></a></td>
                            %else:
                                <td></td>
                                    %endif
                            <td> <a onclick="openWindowunits('Mostrar Historial','${h.url()}/unidades/historialde?id=${it['id_unidad']}',0);"><span class="glyphicon glyphicon-th-list"></span></a></td>
                            <td><a onclick="openWindowunits('Credencial','${h.url()}/unidades/generarcredencial?id=${it['id_unidad']}',${it['id_unidad']});"><span class="glyphicon glyphicon-credit-card"></span></a></td>

                             % for i in list:
                                <td>

                                       %if i['show']=='FOTO':
                                           %if it[i['show']]!=None:
                                          <img src="${h.url()}/img/${internal_id}/unidades/${it['id_unidad']}.-/${it[i['show']]}" style="width: 110px; height: 110px"></td>
                                           %else:
                                           <img src="${h.url()}/img/carrosilueta.png" style="width: 110px; height: 110px"></td>
                                           %endif
                                       %elif i['show']=='ESTADO_POLIZA' :
                                            %if it[i['show']]=='VENCERA':
                                                <img src="${h.url()}/img/aviso3.png" style="width: 70px; height: 70px;"></td>
                                            %elif it[i['show']]=='BIEN':
                                                <img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;"></td>
                                            %elif it[i['show']]=='VENCIO':
                                                <img src="${h.url()}/img/red-cross.png" style="width: 70px; height: 70px;"></td>
                                            %elif it[i['show']]=='SIN':
                                                <img src="${h.url()}/img/sin2.svg" style="width: 70px; height: 70px;"></td>
                                            %endif


                                        %else:
                                       <a>${it[i['show']]}</a></td>
                                      %endif

                             %endfor

                            %if perms['Disable Unit']:
                                <td align="center">
                                   <a onclick="openWindowunits('Deshabilitar Unidad','${h.url()}/unidades/disableunit?id=${it['id_unidad']}&eco=${it['eco1']}',0);">
                                       <span class="glyphicon glyphicon-remove"></span></a>
                                </td>
                            %else:
                            <td align="center"></td>
                            %endif
                        </tr>
                             %endif
                        %endfor
                    </table><br><br>


              <br>

      <!-- page start-->
      <!-- page end-->
<br>
<div id="polizadivlista" title="Polizas"></div>
<div id="polizadivlista2" title="Nueva Poliza"></div>

<div id="jupiter_addexcelform" class="dialog-hidden" title="Subir Excel con Unidades" style="visibility: hidden;">
    <%doc>action="${h.url()}/unidades/earthuploadfields"</%doc>
        <form id="jupiter_formdeexcelunidades" method="POST" enctype="multipart/form-data">
        <label for="upload_file">${_('Filename:')}</label>
            <br>
            <p>${_('Archivo TSV: ')}</p>
            <br>
      <input type="file" id="file-input" name="file-input" accept=".tsv" required/>
        <%doc><input type="file" name="earth_filefields" id="earth_filefields" accept="text/csv" required/>  <br/>
        <button id="earth_botonenviafields" onclick="pruebacsv()">${_('UPLOAD')}</button></%doc>
           <input type="text" id="user" name="user" style="display: none" value="${user}" readonly>
            <input type="text" id="internal_id" name="internal_id" style="display: none" value="${internal_id}"readonly>
            <input type="text" id="app_name" name="app_name" style="display: none" value="${app_name}" readonly>
            <input type="text" id="application_id" name="application_id" style="display: none" value="${application_id}" readonly>
       <input type="submit" name="submit_upload" value="UPLOAD"/>
        </form>
    </div>
