<style type="text/css">
    #content {
    width: 50%;
    margin: 0 auto;
    }
</style>
<script type="text/javascript">


	var startDateTextBox = $('#date_start');
	var endDateTextBox = $('#date_end');

	$( function() {

		$('.btn').click(function() {
			var code='';
            var porquien=document.getElementById("unidadoperadorqr").value;
            var dequien;
            if(porquien=='Unidad'){
                dequien=document.getElementById("selectunidadqr").value;
            }else{
                dequien=document.getElementById("selectoperadorqr").value;

            }
			$.ajax({
					type: "GET",
					url: '${h.url()}/qr/getQRGraphs',
					contentType: "application/json; charset=utf-8",
					data: { 'msg': dequien,'size':$('#size').val(),'por': $('#unidadoperadorqr').val()},
					success: function(data) {
						// data.value is the success return json. json string contains key value
                        document.getElementById("the_filenameelcodigo").value=data.file_name;
                        $('#content').html(data.graph_data);
                        document.getElementById("aparadescargarqr").href='${h.url()}'+data.file_name;
                        //document.getElementById("aparadescargarqr").style.display='block';
                        //abreventanadeqr(data.template);

					},
					error: function() {
						//alert("#"+ckbid);
						$.alert("Error accessing getQRGraphs",{type:'danger'})
						},
					complete: function() {
						}
			});
		});

	  });
	
	function abreventanadeqr(ponesto) {
            var winHeight = Math.round(window.innerHeight * .55);
            var winWidth = Math.round(window.innerWidth * .40);

            var addFilter02Buttons = {
                          "Export to PDF": function () {
                              var file_path = '${h.url()}'+document.getElementById("the_filenameelcodigo").value;
                              window.open(file_path);

                        },
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

            var Dialog02 = $("#ventanaopenqr").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });
            $("#ventanaopenqr").html(ponesto);
            Dialog02.data('rowId', 1);
            Dialog02.dialog("open");
            //$("#content").html(ponesto);
      }

	function verdivdeqr() {
        var por=document.getElementById("unidadoperadorqr").value;
        if(por=='Unidad'){
            document.getElementById("divoperadoresqr").style.display='none';
            document.getElementById("divunidadesqr").style.display='block';

        }else{
            document.getElementById("divoperadoresqr").style.display='block';
            document.getElementById("divunidadesqr").style.display='none';
        }
      }
</script>

<table>
<tr>
    <td>QR de: </td>
    <td><select id="unidadoperadorqr" name="unidadoperadorqr" onchange="verdivdeqr();"><option>Unidad</option><option>Operador</option></select></td>
</tr>
</table>
<br>
<table id="divunidadesqr" style="display: block;">
    <tr>
        <td>Unidad: </td>
        <td><select id="selectunidadqr" name="selectunidadqr">
                    %for itemunidad in unidades:
                        <option value="${itemunidad.id_unidad}">${itemunidad.eco1}</option>
                    %endfor
            </select></td>
    </tr>
</table>

<table id="divoperadoresqr" style="display: none;">
    <tr>
    <td>Operador:</td><td>
        <select id="selectoperadorqr">
    %for itemoperador in operadores:
    <option value="${itemoperador.id_persona}">${itemoperador.numerooperador}.-${itemoperador.nombre}</option>
    %endfor
    </select></td>
    </tr>
</table>
<br>
<table>
    <tr>
    <td></td><td><button class="btn btn-primary" id="btn-test1">Get QR code</button></td>
    </tr>
</table>

<div id="ventanaopenqr">
<div id="content">

</div>
</div>

<input id="size" value="30" type="number" style="display: none;"/>
<a id="aparadescargarqr" name="aparadescargarqr" style="display: none;" download>Descargar</a>
<input type="text" id="the_filenameelcodigo" name="the_filenameelcodigo" style="display: none;">

<%doc>
Message:<input id="msg" value="Hello"/>
Size   :
<button class="btn btn-primary" id="btn-test1">Get QR code</button>



</%doc>