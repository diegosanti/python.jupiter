<script>
    function formatImageJupiterPeople(cellValue, options, rowObject) {
        var imageHtml = "";
        if(cellValue != 0){
            imageHtml = '<img src="data:image/png;base64,'+cellValue+'" width="100px" height="100px" />';
        }else{
            imageHtml = '<img src="${h.url()}/img/personasilueta.gif" style="width: 100px; height: 100px">';
        }
        return imageHtml;
    }
    function actionsJupiterPeople(cellvalue, options, rowObject) {
        i = options.rowId;
        var ed = '<button onClick="doDoubleClickJupiterPeople('+options.rowId+');"><span class="glyphicon glyphicon-edit" style="cursor: pointer;"></span></button><br>';
        var hi = '<button onClick="openWindow(\'Historial\',\'${h.url()}/personas/historic?sobrecampo='+options.rowId+'\');"><span class="glyphicon glyphicon-th-list" style="cursor: pointer;"></span></button><br>';
        var arc = '<button onClick="openWindow(\'Archivos\',\'${h.url()}/personas/files?id='+options.rowId+'\');"><span class="glyphicon glyphicon-paperclip" style="cursor: pointer;"></span></button><br>';
        var gen = '<button onClick="openWindow(\'Credencial\',\'${h.url()}/personas/createCredential?id='+options.rowId+'\');"><span class="glyphicon glyphicon-credit-card" style="cursor: pointer;"></span></button><br>';
        return ed + hi + arc + gen;
    }
    $(window).on("resize", function () {
            var $gridPeople = $("#jqGridPeopleJupiter"),
                newWidth = $gridPeople.closest(".ui-jqgrid").parent().width();
                $gridPeople.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_namePeople = '#jqGridPeopleJupiter';
    var grid_pagerPeople = '#listPagerPeopleJupiter';
    var update_urlPeople='${h.url()}/personas/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlPeople  ='${h.url()}/personas/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlPeople,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsPeople = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlPeople,modal: true};

    var gridPeople = jQuery(grid_namePeople);
            $(document).ready(function () {
                gridPeople.jqGrid({
                url: load_urlPeople,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('No.')}','${_('Name')}','${_('Nickname')}','${_('Photo')}','${_('RFC')}',
                    '${_('Birth Date')}','${_('Email')}','${_('Gender')}','${_('Civil Status')}','${_('Status')}','${_('Status Date')}','${_('Admission Date')}',
                    '${_('Address')}','${_('Address')} 2','${_('Address')} 3','${_('Phone')}','${_('Phone')} 2','${_('Cellphone')}','${_('Cellphone')} 2',
                    '${_('CURP')}','${_('INE')}','${_('Observations')}','${_('Checker')}','${_('Beneficiary')}','${_('Operator')}','${_('Permisionario')}',
                    '${_('Cheker Enabled')}','${_('Beneficiary Enabled')}','${_('Operator Enabled')}','${_('Permisionario Enabled')}',
                    '${_('Number')}','${_('Licence')}','${_('Licence Number')}','${_('Licence Expiration')}','${_('Licence State')}',
                    '${_('User')}','${_('Licence Type')}','${_('id')}','${_('id')}','${_('id')}','${_('id')}',
                    '${_('internal_id')}', '${_('app_id')}', '${_('Company')}', '${_('Bank Account')}', '${_('Bank')}', '${_('Bank Account')}', '${_('Bank')}',
                    '${_('QR Code')}', '${_('Reports')}', '${_('Units')}',''],
                colModel: [
                    {name: 'id_persona', index: 'id_persona', align: 'center',key:true,hidden: true,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'numerooperador',width:30, index: 'numerooperador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'nombre', index: 'nombre',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'apodo', index: 'apodo',width:90,align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'foto',width:150, index: 'foto',align: 'center',formatter: formatImageJupiterPeople,hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'rfc', index: 'rfc',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_nacimiento', index: 'fecha_nacimiento',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'correo', index: 'correo',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'sexo', index: 'sexo',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estadocivil', index: 'estadocivil',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estatus', index: 'estatus',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_estatus', index: 'fecha_estatus',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_ingreso', index: 'fecha_ingreso',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion', index: 'direccion',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion2', index: 'direccion2',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion3', index: 'direccion3',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono', index: 'telefono',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono2', index: 'telefono2',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'celular', index: 'celular',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'otro_celular', index: 'otro_celular',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'curp', index: 'curp',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'ine', index: 'ine',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'observaciones', index: 'observaciones',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'eschecador', index: 'eschecador',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'esbeneficiario', index: 'esbeneficiario',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'esoperador', index: 'esoperador',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'espermisionario', index: 'espermisionario',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadochecador', index: 'habilitadochecador',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadobeneficiario', index: 'habilitadobeneficiario',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadooperador', index: 'habilitadooperador',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadopermisionario', index: 'habilitadopermisionario',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'numero_segurosocial', index: 'numero_segurosocial',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'licencia', index: 'licencia',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'numerolicencia', index: 'numerolicencia',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_vencimientolicencia', index: 'fecha_vencimientolicencia',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estadodelicencia', index: 'estadodelicencia',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'usuario', index: 'usuario',align: 'left',hidden: true, editable: false, edittype: 'text', editrules: {required: true}},
                    {name: 'id_tipolicencia', index: 'id_tipolicencia',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_linea', index: 'id_linea',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_punto', index: 'id_punto',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_grupopersona', index: 'id_grupopersona',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_unidad', index: 'id_unidad',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'cta1',index: 'cta1', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'banco1',index: 'banco1', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'cta2',index: 'cta2', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'banco2',index: 'banco2', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'codigoqr',index: 'codigoqr', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'reportes',index: 'reportes', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                    {name: 'act',index: 'act', align: 'center',title:"act",formatter: actionsJupiterPeople,width:50,sortable:false,search:false}
                ],
                pager: jQuery(grid_pagerPeople),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_persona',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterPeople(rowId);
                },
                gridComplete: function (rowId){
                    var table = document.getElementById("jqGridPeopleJupiter");
                    var rows = table.getElementsByTagName("tr");
                    for(i = 1; i < rows.length; i++){
                        var dates = rows[i].cells[35].innerHTML;
                        if (dates != 0){
                            if (dates== "LEJOS"){
                                rows[i].cells[35].innerHTML='<img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;">';
                            }
                            if (dates== "VENCIO"){
                                rows[i].cells[35].innerHTML='<img src="${h.url()}/img/red-cross.png" style="width: 45px; height: 45px;">';
                            }
                            if (dates== "VENCERA"){
                                rows[i].cells[35].innerHTML='<img src="${h.url()}/img/aviso3.svg" style="width: 50px; height: 50px;">';
                            }
                        }else{
                            rows[i].cells[35].innerHTML="";
                        }
                    }
                }
                //caption: header_container,
            });
            gridPeople.jqGrid('navGrid',grid_pagerPeople,{edit:false,add:false,del:false, search:true},
                            editParamsPeople,
                            addParamsPeople,
                            deleteParamsPeople,
                            searchParamsPeople,
                            viewParamsPeople);
            // add custom button
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterPeople(0)
                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-contact",
                    title: "${_('Credential')}",
                    caption: "${_('Credential')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridPeople.jqGrid ('getGridParam', 'selrow');
                        var id_persona = gridPeople.jqGrid('getCell',RowId,'id_persona');
                        if (id_persona == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            openWindow("Credencial","${h.url()}/personas/createCredential?id="+RowId);
                        }
                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-folder-open",
                    title: "${_('Files')}",
                    caption: "${_('Files')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridPeople.jqGrid ('getGridParam', 'selrow');
                        var id_persona = gridPeople.jqGrid('getCell',RowId,'id_persona');
                        if (id_persona == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            openWindow("Archivos","${h.url()}/personas/files?id="+RowId);
                        }
                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-clipboard",
                    title: "${_('Historial')}",
                    caption: "${_('Historial')}",
                    position: "first",
                    onClickButton: function(){
                        var RowId = gridPeople.jqGrid ('getGridParam', 'selrow');
                        var id_persona = gridPeople.jqGrid('getCell',RowId,'id_persona');
                        if (id_persona == false){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            openWindow("Historial","${h.url()}/personas/historic?sobrecampo="+RowId);
                        }
                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-pencil",
                    title: "${_('Edit')}",
                    caption: "${_('Edit')}",
                    position: "first",
                    onClickButton: function(){
                        var selRowId = gridPeople.jqGrid ('getGridParam', 'selrow');
                        if (selRowId == null){
                            $.alert("${_('No selected row')}",{type: "warning"});
                        }else{
                            doDoubleClickJupiterPeople(selRowId);
                        }

                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterPeople(0)
                    }
                });
            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterPeople() {
                var rowid = $('#jqGridPeopleJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/personas/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridPeopleJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /personas/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterPeople(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/personas/form?id='+rowId+'&application_id=${application_id}&user=${user}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogPeopleJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogPeopleJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /personas/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogPeopleJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.6),
                            width: Math.round(window.innerWidth*.8),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#PeopleFormJupiter").valid()) {
                                        var info = $('#PeopleFormJupiter').serialize();
                                        var input = document.querySelector('input[type=file]');
                                        var people_es = "";
                                        $("input:checkbox[class=jupiterPeople_es]:checked").each(function () {
                                            people_es += "" + $(this).attr("id")+",";
                                        });

                                        foto = input.files[0];
                                        ine = input.files[1];
                                        licencia = input.files[2];
                                        var formData = new FormData();
                                        formData.append("foto", foto);
                                        formData.append("ine", ine);
                                        formData.append("licencia", licencia);
                                        var request = new XMLHttpRequest();
                                        request.open("POST", '${h.url()}/personas/save?'+info+"&people_es="+people_es+"&application_id=${application_id}&app_name=${app_name}");
                                        request.send(formData);
                                        request.onload  = function() {
                                            var response = JSON.parse(request.responseText);
                                            if (response.error == "ok"){
                                                $.alert("${_('Done')}!",{type: "success"});
                                                $('#jqGridPeopleJupiter').trigger( 'reloadGrid' );
                                                $('#PeopleFormJupiter')[0].reset();
                                                ContDialog.dialog( "close" );
                                            }else{
                                                $.alert(response.error,{type: "warning"});
                                            }
                                        };
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#PeopleFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#PeopleFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogPeopleJupiter"  title="${_('People')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridPeopleJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerPeopleJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->