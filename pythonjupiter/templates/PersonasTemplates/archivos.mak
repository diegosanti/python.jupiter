<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
<script>
    function openWindowarchivo(url,archivo,id,internal_id){

                    var winHeight = Math.round(window.innerHeight * .85);
                  var winWidth = Math.round(window.innerWidth * .95);

                  var addFilter02Buttons = {
                      "Close": function () {
                          Dialog02.dialog("close");
                      }
                  };

                  var Dialog02 = $("#dialogForm02").dialog({
                      autoOpen: false,
                      height: winHeight,
                      width: winWidth,
                      modal: true,
                      buttons: addFilter02Buttons,
                      close: function () {

                      }
                  });

                  $.ajax({
                      type: "GET",
                      url: url+"?archivo="+archivo+"&id="+id+"&internal_id="+internal_id,
                      contentType: "application/json; charset=utf-8",
                      data: {},
                      success: function (parameterdata) {

                          //Insert HTML code
                          $("#dialogForm02").html(parameterdata.template);
                          Dialog02.data('rowId', 1);
                          Dialog02.dialog("open");

                      },
                      error: function () {
                          alert("Error openwindow");
                      },
                      complete: function () {
                      }
                  });

        }
        </script>
<table class="userTable">
    <tr>
        <th></th>
        <th>${_('FILE')}</th>
    </tr>
    <tr>
        <td>${_('PHOTO')}</td>
            %if soy.foto!=None:
                <td><img src="${h.url()}/img/${internal_id}/personas/${soy.id_persona}.-/${soy.foto}" style="max-width: 300px; max-height: 300px"></td>
            %else:
                <td> <img src="${h.url()}/img/personasilueta.gif" style="width: 100px; height: 100px"></td>
            %endif

    </tr>
    <tr>
        <td>${_('LICENCE')}</td>
        <td><a onclick="openWindowarchivo('${h.url()}/personas/mostrararchivo','${soy.licencia}',${soy.id_persona},'${soy.internal_id}')">${soy.licencia}</a></td>
    </tr>
        <tr>
            <td>INE</td>
            <td><a onclick="openWindowarchivo('${h.url()}/personas/mostrararchivo','${soy.ine}',${soy.id_persona},'${soy.internal_id}')" >${soy.ine}</a></td>
    </tr>
</table>

<div id="dialogForm02"></div>