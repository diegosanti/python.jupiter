<script>
    $(window).on("resize", function () {
            var $gridPeople = $("#jqGridPeopleJupiter"),
                newWidth = $gridPeople.closest(".ui-jqgrid").parent().width();
                $gridPeople.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_namePeople = '#jqGridPeopleJupiter';
    var grid_pagerPeople = '#listPagerPeopleJupiter';
    var update_urlPeople='${h.url()}/personas/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlPeople  ='${h.url()}/personas/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlPeople,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsPeople = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeople,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsPeople = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlPeople,modal: true};
    var gridPeople = jQuery(grid_namePeople);
            $(document).ready(function () {
                gridPeople.jqGrid({
                url: load_urlPeople,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Derrotero')}', '${_('internal_id')}', '${_('application_id')}', '${_('app_name')}', '${_('unidades')}'],
                colModel: [
                    {name: 'id_persona',index: 'id_persona', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'numerooperador', index: 'numerooperador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'nombre', index: 'nombre',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'apodo', index: 'apodo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'foto', index: 'foto',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'rfc', index: 'rfc',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_nacimiento', index: 'fecha_nacimiento',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'correo', index: 'correo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'sexo', index: 'sexo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estadocivil', index: 'estadocivil',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estatus', index: 'estatus',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_estatus', index: 'fecha_estatus',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_ingreso', index: 'fecha_ingreso',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion', index: 'direccion',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion2', index: 'direccion2',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion3', index: 'direccion3',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono', index: 'telefono',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono2', index: 'telefono2',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'celular', index: 'celular',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'otro_celular', index: 'otro_celular',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'curp', index: 'curp',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'ine', index: 'ine',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'observaciones', index: 'observaciones',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'eschecador', index: 'eschecador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'esbeneficiario', index: 'esbeneficiario',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'esoperador', index: 'esoperador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'espermisionario', index: 'espermisionario',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadochecador', index: 'habilitadochecador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadobeneficiario', index: 'habilitadobeneficiario',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadooperador', index: 'habilitadooperador',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'habilitadopermisionario', index: 'habilitadopermisionario',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'numero_segurosocial', index: 'numero_segurosocial',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'licencia', index: 'licencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'numerolicencia', index: 'numerolicencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fecha_vencimientolicencia', index: 'fecha_vencimientolicencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'estadodelicencia', index: 'estadodelicencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'usuario', index: 'usuario',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_tipolicencia', index: 'id_tipolicencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_linea', index: 'id_linea',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_punto', index: 'id_punto',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_grupopersona', index: 'id_grupopersona',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'id_unidad', index: 'id_unidad',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'cta1',index: 'cta1', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'banco1',index: 'banco1', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'cta2',index: 'cta2', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'banco2',index: 'banco2', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'codigoqr',index: 'codigoqr', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'reportes',index: 'reportes', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerPeople),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_persona',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterPeople(rowId)
                }
                //caption: header_container,
            });
            gridPeople.jqGrid('navGrid',grid_pagerPeople,{edit:false,add:false,del:false, search:true},
                            editParamsPeople,
                            addParamsPeople,
                            deleteParamsPeople,
                            searchParamsPeople,
                            viewParamsPeople);
            // add custom button
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterPeople(0)
                    }
                });
            gridPeople.navButtonAdd(grid_pagerPeople,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterPeople(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterPeople() {
                var rowid = $('#jqGridPeopleJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/personas/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridPeopleJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /personas/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterPeople(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/personas/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogPeopleJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogPeopleJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /personas/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogPeopleJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.35),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#PeopleFormJupiter").valid()) {
                                        var id = $('#jupiterPeople_id').val();
                                        var personas = $('#jupiterPeople_personas').val();
                                        var app_id = $('#jupiterPeople_app_id').val();
                                        var app_name = $('#jupiterPeople_app_name').val();
                                        var descripcion = $('#jupiterPeople_description').val();
                                        var internal_id = $('#jupiterPeople_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/personas/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'derrotero':derrotero,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                 $('#jqGridPeopleJupiter').trigger( 'reloadGrid' );
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /personas/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#PeopleFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#PeopleFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#PeopleFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogPeopleJupiter"  title="${_('People')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridPeopleJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerPeopleJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->