<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>


<table class="userTable">
    <tr>
        <th>${_('DATE')}</th>
         <th>${_('DATA')}</th>
        <th>${_('OLD VALUE')}</th>
         <th>${_('NEW VALUE')}</th>
         <th>${_('USER')}</th>

    </tr>
     %for item in allrecords:
    <tr>

        <td>${item.fecha}</td>
        <td>${item.dato}</td>
        <td>${item.valor_anterior}</td>
        <td>${item.valor_nuevo}</td>
        <td>${item.usuario}</td>

    </tr>
         %endfor


</table>