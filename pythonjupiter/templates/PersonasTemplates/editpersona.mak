<html>
<%doc> <script src="https://code.jquery.com/jquery.js"></script>
  <script src="${tg.url('/javascript/bootstrap.min.js')}"></script></%doc>
<head>
    <style>
          .thumb {
            height: 300px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
          }
        </style>

</head>

        <script>



            function verseccionoperador() {
                var checkBox=document.getElementById("jupiterpersona_esoperador");

                if (checkBox.checked == true){
                    document.getElementById("jupiterpersona_divoperador").style.display='block';
                    document.getElementById("jupiteroperadores_licencia").required=true;
                    document.getElementById("jupiteroperadores_linea").required=true;
                    document.getElementById("jupiteroperadores_duedate").required=true;
                    //document.getElementById("jupiteroperadores_noss").required=true;
                    document.getElementById("jupiteroperadores_licence").required=true;
                    document.getElementById("numerodeoperadorasignado").required=true;
                    document.getElementById("jupiter_tabladecargosenoperadores").style.display='block';
                    document.getElementById("legendcharges").style.display='block';
                    %if list['esoperador']!=1:
                    document.getElementById("jupiter_seccionlicencia").style.visibility='visible';
                    document.getElementById("jupiteroperadores_licencearchivo").required=true;
                    %endif

                }else{
                    document.getElementById("jupiterpersona_divoperador").style.display='none';
                    document.getElementById("jupiteroperadores_licencia").required=false;
                    document.getElementById("jupiteroperadores_linea").required=false;
                    document.getElementById("jupiteroperadores_duedate").required=false;
                    //document.getElementById("jupiteroperadores_noss").required=false;
                    document.getElementById("jupiteroperadores_licence").required=false;
                    document.getElementById("numerodeoperadorasignado").required=false;
                    document.getElementById("jupiter_tabladecargosenoperadores").style.display='none';
                    document.getElementById("legendcharges").style.display='none';
                    document.getElementById("jupiter_seccionlicencia").style.visibility='hidden';
                     document.getElementById("jupiteroperadores_licencearchivo").required=false;
                     document.getElementById("numerodeoperadorasignado").value='${list['numerooperador']}';


                }

              }

                          function verseccionchecador() {
                var checkBox=document.getElementById("jupiterpersona_eschecador");

                if (checkBox.checked == true){
                    document.getElementById("jupiter_datoschecador").style.visibility='visible';

                }else{
                    document.getElementById("jupiter_datoschecador").style.visibility='hidden';
                }

              }




              function archivo(evt) {
                  var files = evt.target.files; // FileList object

                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("jupiter_fotoshowpersona").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);

                    reader.readAsDataURL(f);
                  }
              }

              function showseccionfoto() {
                if(document.getElementById("labelseccionfoto").innerText!='Cancelar'){
                document.getElementById("jupiter_seccionfotopersona").style.visibility='visible';
                  document.getElementById("labelseccionfoto").innerText='Cancelar';
                }else{
                    document.getElementById("jupiter_seccionfotopersona").style.visibility='hidden';
                  document.getElementById("labelseccionfoto").innerText='Editar Foto';

                }
                }


                        function showseccioneditine() {
                if(document.getElementById("labelseccionine").innerText!='Cancelar'){
                document.getElementById("jupiter_seccionine").style.visibility='visible';
                  document.getElementById("labelseccionine").innerText='Cancelar';
                }else{
                    document.getElementById("jupiter_seccionine").style.visibility='hidden';
                  document.getElementById("labelseccionine").innerText='Editar INE';

                }
                }

                                        function showseccionlicencia() {
                if(document.getElementById("labelseccionlicencia").innerText!='Cancelar'){
                document.getElementById("jupiter_seccionlicencia").style.visibility='visible';
                  document.getElementById("labelseccionlicencia").innerText='Cancelar';
                }else{
                    document.getElementById("jupiter_seccionlicencia").style.visibility='hidden';
                  document.getElementById("labelseccionlicencia").innerText='Editar INE';

                }
                }

                            function actualizandocargos() {

                 var empresa=document.getElementById("jupiteroperadores_linea").value;
                 var grupo=document.getElementById("seriedegrupos").value;

                             $.ajax({
                        type: "GET",
                                 %if list['esoperador']==1:
                                        url: "${h.url()}/cargosoperadores/actualizacargosoperadores"+"?empresa="+empresa+"&internal_id=${internal_id}&id=${toedit.id_persona}&grupo="+grupo+"&application_id=${application_id}",
                                 %else:
                                        url: "${h.url()}/cargosoperadores/actualizacargosoperadores"+"?empresa="+empresa+"&internal_id=${internal_id}&grupo="+grupo+"&application_id=${application_id}",
                                 %endif
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                            $('#jupiter_tabladecargosenoperadores td').remove();

                            if(parameterdata['construir2']!=''){
                                      for(var k2 in parameterdata['construir2']){

                                                 $('#jupiter_tabladecargosenoperadores').append('<tr><td><input type=\"text\" id=\"jupitercargo' + parameterdata['construir2'][k2]['cargo'] + '\" name=\"jupitercargo' + parameterdata['construir2'][k2]['cargo'] + '\"' +
                                    'value=\"' + parameterdata['construir2'][k2]['cargo'] + '\" readonly></td><td>Monto: <input name=\"montode' + parameterdata['construir2'][k2]['cargo'] + '\" id=\"montode' + parameterdata['construir2'][k2]['cargo'] +
                                                         '\" type=\"number\" size=\"8\" value=\"'+parameterdata['construir2'][k2]['monto']+'\"></td></tr>');
                                }

                            }else {

                                for (var k in parameterdata['construir']) {
                                    //alert(parameterdata['construir'][k]);

                                    $('#jupiter_tabladecargosenoperadores').append('<tr><td><input type=\"text\" id=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\" name=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\"' +
                                'value=\"'+parameterdata['construir'][k]['cargo']+'\" readonly></td><td>Monto: <input name=\"montode'+parameterdata['construir'][k]['cargo']+'\" id=\"montode'+parameterdata['construir'][k]['cargo']+'\" type=\"number\" size=\"8\" value=\"'+parameterdata['construir'][k]['monto']+'\"></td></tr>');
                                }
                            }
                },
                error: function () {
                        alert("ERROR actualizando los cargos");
                },
                complete: function () {
                 }
                });
              }


              function versecciondebeneficiarioenedit() {
                     var esto=document.getElementById("jupiterpersona_esbeneficiario").checked;

                if(esto==true){
                  document.getElementById("jupiter_datosbeneficiario").style.display='block';
                  document.getElementById("jupiter_cuenta1").required=true;
                  document.getElementById("jupiter_banco1").required=true;


                }else{
                    document.getElementById("jupiter_datosbeneficiario").style.display='none';
                    document.getElementById("jupiter_cuenta1").required=false;
                  document.getElementById("jupiter_banco1").required=false;

                }
                }


              function verseccionpermisionario() {
                  if(document.getElementById("jupiterpersona_espermisionario").checked==true){
                      document.getElementById("jupiterpersona_usuariosun").style.display='block';

                  }else{
                     document.getElementById("jupiterpersona_usuariosun").style.display='none';
                  }
                }

                  function insertandoeninputoculto(esto) {
                  //alert(esto.value);
                  if (esto.checked == true) {
                      var esta = document.getElementById("seriedegrupos").value.split(",");
                      if (esta.includes(esto.value)) {

                      } else {
                          document.getElementById("seriedegrupos").value = document.getElementById("seriedegrupos").value+','+esto.value ;
                      }

                      //alert(document.getElementById("seriedegrupos").value);
                  }else{
                       var esta = document.getElementById("seriedegrupos").value;
                       var nuevacadena=esta.replace(','+esto.value,"");
                       document.getElementById("seriedegrupos").value=nuevacadena;
                       //alert(document.getElementById("seriedegrupos").value);


                  }
                  actualizandocargos();
              }



              document.getElementById('jupiter_fotopersona').addEventListener('change', archivo, false);
      </script>
  <fieldset>
        <legend>${_('Edit Person')}</legend>

      <form action="${h.url()}/personas/editPersona?internal_id=${internal_id}&id=${id}&who=${user}&user=${user}&application_id=${application_id}" name="personasform" id="personasform" method="POST" enctype="multipart/form-data" target="requestpersonasedit" accept-charset="UTF-8">
       <p>* ${_('Required Files')}</p>

          <br>
          <a id="labelseccionfoto" onclick="showseccionfoto();">${_('Edit Photo')}</a>
          <div id="jupiter_seccionfotopersona" style="visibility: hidden">
            <p>${_('Upload Photo of the Person')}:  ${_('Only')} (jpeg,bmp,png and pdf)</p>
        <input type="file" name="jupiter_fotopersona" id="jupiter_fotopersona" accept="image/vnd-wap-wbmp,image/png, image/jpeg" />  <br/>
          <output id="jupiter_fotoshowpersona"></output>
              </div>
             <table>
                 <tr>
                      <td>*${_('Name: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_nombre" id="jupiterpersona_nombre" length="50" value="${list['persona']}" maxlength="100" required ><br></td>
                 </tr>
        <tr>
            <td>${_('Apodo: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_apodo" length="50" value="${list['apodo']}" maxlength="25" ><br></td>
        </tr>

                 <tr>
                     <td>${_('RFC: ')} </td>
                     <td><input type="text" id="jupiterpersona_rfc" name="jupiterpersona_rfc" value="${list['rfc']}" maxlength="20"></td>
                 </tr>
                  <tr>
                <td>*${_('Birthday: ')}</td><td><input type="date" name="jupiterpersona_fecha_nacimiento" value="${list['fecha_nacimiento']}" required></td>
        </tr>
          <tr>
              <td>${_('Email: ')} &nbsp;</td><td><input type="email" id="jupiterpersona_email" name="jupiterpersona_email" length="50" value="${list['correo']}" maxlength="50"><br></td>
        </tr>
          <tr>
              <td>${_('Gender: ')} &nbsp;</td><td><select id="jupiterpersona_genero" name="jupiterpersona_genero" length="50" required><br>
                                            %if list['sexo']=='MASCULINO':
                                                  <option selected>MASCULINO</option>
                                                    <option>FEMENINO</option>
                                            %else:
                                                  <option >MASCULINO</option>
                                                    <option selected>FEMENINO</option>
                                            %endif

                                            </select></td>
        </tr>

                  <tr>
                <td>${_('Marital Status : ')}</td><td><select name="jupiterpersona_estadocivil">
                      %if list['estadocivil']=='Soltero(a)':
                      <option selected>Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option>Viudo(a)</option>
                      %endif

                      %if list['estadocivil']=='Comprometido(a)':
                      <option >Soltero(a)</option>
                      <option selected>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option>Viudo(a)</option>
                      %endif

                      %if list['estadocivil']=='Casado(a)':
                      <option >Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option selected>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option>Viudo(a)</option>
                      %endif

                      %if list['estadocivil']=='Divorciado(a)':
                      <option >Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option selected>Divorciado(a)</option>
                      <option>Viudo(a)</option>
                      %endif

                      %if list['estadocivil']=='Viudo(a)':
                      <option>Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option selected>Viudo(a)</option>
                      %endif

                      %if list['estadocivil']==None:
                      <option>Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option >Viudo(a)</option>
                      %endif

            </select></td>
        </tr>

     <%doc>            <tr>
                     <td>${_('Status')}</td><td>
                        <select name="jupiterpersona_estatus" id="jupiterpersona_estatus">
                        %if list['estatus']=='ALTA':
                            <option selected>ALTA</option>
                            <option>BAJA</option>
                        %else:
                            <option>ALTA</option>
                            <option selected>BAJA</option>
                        %endif
                        </select>

                 </td>
                 </tr></%doc>

         <tr>
            <td>*${_('Address: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address" value="${list['direccion']}" maxlength="200" required><br></td>
        </tr>
             <tr>
            <td>${_('Address 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address2" value="${list['direccion2']}" maxlength="200" ><br></td>
        </tr>
                      <tr>
            <td>${_('Address 3: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address3" value="${list['direccion3']}" maxlength="200"><br></td>
        </tr>

        <tr>
            <td>${_('Phone: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_phone" length="50" value="${list['telefono']}" maxlength="25" ><br></td>
        </tr>
         <tr>
            <td>${_('Phone 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_phone2" length="50" value="${list['telefono2']}" maxlength="25"><br></td>
        </tr>
          <tr>
            <td>${_('Mobile Phone: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_mobilephone" length="50" value="${list['celular']}" maxlength="25" ><br></td>
        </tr>
           <tr>
            <td>${_('Mobile Phone 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_mobilephone2" length="50" value="${list['otro_celular']}" maxlength="25"><br></td>
        </tr>
                 <tr>
            <td>${_('CURP: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_curp" length="50" value="${list['curp']}" maxlength="50"><br></td>
        </tr>
                 <tr>
                 <td><a id="labelseccionine" onclick="showseccioneditine();">${_('Edit INE')}</a></td>
                 </tr>

                 <tr id="jupiter_seccionine" style="visibility: hidden">
            <td>${_('INE: ')} &nbsp;</td><td><input type="file" name="jupiterpersona_ine" length="50" accept="application/pdf,image/vnd-wap-wbmp,image/png,image/jpeg" ><br></td>
        </tr>
                     <tr>
            <td>${_('Observations: ')} &nbsp;</td><td><textarea name="jupiterpersona_observaciones" maxlength="500" style="margin: 0px; width: 422px; height: 150px;">${list['observaciones']}</textarea></td>
        </tr>

             </table>

          <table>
             <tr>
                     <td>${_('Operator: ')}</td>
                        %if list['esoperador']==1:
                        <td><input type="checkbox" name="jupiterpersona_esoperador" id="jupiterpersona_esoperador" onclick="verseccionoperador();" checked></td>
                         %else:
                            <td><input type="checkbox" name="jupiterpersona_esoperador" id="jupiterpersona_esoperador" onclick="verseccionoperador();" ></td>
                            %endif
                 </tr>
              </table>
            %if list['esoperador']==1:
                     <table id="jupiterpersona_divoperador" style="display: block;">
            %else:
                <table id="jupiterpersona_divoperador" style="display: none;">
            %endif
                   <tr>
                            <td><label id="labelnumerodeoperadorasignado" name="labelnumerodeoperadorasignado">
                                ${_('Operator Number: ')}
                            </label></td>
                             <td><input type="number" id="numerodeoperadorasignado" name="numerodeoperadorasignado" value="${list['numerooperador']}"></td>
                         </tr>

         <tr>
            <td>${_('Number SS: ')} &nbsp;</td><td><input type="text" id="jupiteroperadores_noss" name="jupiteroperadores_noss" length="50" value="${list['numero_segurosocial']}" maxlength="50"><br></td>
        </tr>
          <tr>
              %if list['esoperador']!=1:
                    <td><a id="labelseccionlicencia" onclick="showseccionlicencia();" style="display: none;">${_('Edit Licence')}</a></td>
                  %else:
                   <td><a id="labelseccionlicencia" onclick="showseccionlicencia();">${_('Edit Licence')}</a></td>
              %endif
          </tr>


          <tr id="jupiter_seccionlicencia" style="visibility: hidden;">
            <td>${_('File Licence: ')} &nbsp;</td><td><input type="file" id="jupiteroperadores_licencearchivo" name="jupiteroperadores_licencearchivo" length="50" accept="application/pdf,image/vnd-wap-wbmp,image/png,image/jpeg"><br></td>
          </tr>
                         <tr>
            <td>*${_('Licence: ')} &nbsp;</td><td><input type="text" id="jupiteroperadores_licence" name="jupiteroperadores_licence" length="50" value="${list['numerolicencia']}" maxlength="55"><br></td>
        </tr>
         <tr>
            <td>*${_('Due Date of Licence: ')} &nbsp;</td><td><input type="date" id="jupiteroperadores_duedate" name="jupiteroperadores_duedate" length="50" value="${list['fecha_vencimiento']}" ><br></td>
        </tr>

                      <tr>
              <td>${_('Empresa: ')} &nbsp;</td><td><select id="jupiteroperadores_linea" name="jupiteroperadores_linea" length="50" onchange="actualizandocargos();"><br>
                                                %for item in typelinea:
                                                    %if item.nombre_linea==list['id_linea']:
                                                    <option selected>${item.nombre_linea}</option>
                                                    %else:
                                                    <option>${item.nombre_linea}</option>
                                                    %endif
                                                %endfor
                                            </select></td>
                    </tr>
        <%doc>         <tr>
              <td>${_('Group: ')} &nbsp;</td><td><select id="jupiteroperadores_grupo" name="jupiteroperadores_grupo" length="50" onchange="actualizandocargos();"><br>
                                                <option>Sin Grupo</option>
                                                %for item in typegrupo:
                                                    %if item.grupopersona==list['id_grupopersona']:
                                                    <option selected>${item.grupopersona}</option>
                                                    %else:
                                                     <option >${item.grupopersona}</option>
                                                    %endif
                                                %endfor
                                            </select></td>
                    </tr></%doc>
                                 <tr>
              <td>*${_('Group: ')} &nbsp;</td><!--<select id="jupiteroperadores_grupo" name="jupiteroperadores_grupo" length="50" onchange="actualizandocargos();"></select><br>
                                                <option>Sin Grupo</option>-->
                                                 <input type="text" id="seriedegrupos" value="${variabledeserie}" hidden>
                                                %for item in typegrupo:
                                                    %if item.grupopersona in list['listadegrupos']:
                                                    <tr><td></td><td><input type="checkbox" name="${item.grupopersona}" value="${item.grupopersona}" onchange="insertandoeninputoculto(this);" checked>${item.grupopersona}</td></tr>
                                                    %else:
                                                    <tr><td></td><td><input type="checkbox" name="${item.grupopersona}" value="${item.grupopersona}" onchange="insertandoeninputoculto(this);">${item.grupopersona}</td></tr>
                                                    %endif
                                                %endfor



                    </tr>



                      <tr>
              <td>${_('Licence: ')} &nbsp;</td><td><select id="jupiteroperadores_licencia" name="jupiteroperadores_licencia" length="50" ><br>
                                                %for item in typelicence:
                                                    %if item.tipo_licencia==list['id_tipolicencia']:
                                                    <option selected>${item.tipo_licencia}</option>
                                                    %else:
                                                    <option>${item.tipo_licencia}</option>
                                                    %endif

                                                %endfor
                                            </select></td>
                    </tr>
              </table>
                   %if list['esoperador']==1:
                    <table id="jupiter_tabladecargosenoperadores" style="display: block;">
                       <legend id="legendcharges" style="display: block">${_('CHARGES: ')}</legend>
            %else:
                <table id="jupiter_tabladecargosenoperadores" style="display: none;">
                       <legend id="legendcharges" style="display: none">${_('CHARGES: ')}</legend>
            %endif


                 %for item in construir:
                     <tr>

                         <td><input type="text" id="jupitercargo${item['cargo']}" name="jupitercargo${item['cargo']}" value="${item['cargo']}" readonly></td><td>Monto: <input name="montode${item['cargo']}" id="montode${item['cargo']}" type="number" size="8" value="${item['monto']}"></td>

                    </tr>

                    %endfor
              <legend id="legendchargesclose" style="display: none"></legend>

        </table>
<br>


    </table>
          <br>
          <table>
               <tr>
                     <td>${_('Permisionario: ')}</td>
                   %if list['espermisionario']==1:
                        <td><input type="checkbox" name="jupiterpersona_espermisionario" id="jupiterpersona_espermisionario" checked onchange="verseccionpermisionario();"></td>
                         %else:
                            <td><input type="checkbox" name="jupiterpersona_espermisionario" id="jupiterpersona_espermisionario" onchange="verseccionpermisionario();"></td>
                            %endif

                 </tr>
              <tr>
                            %if list['espermisionario']==1:
                                %if usuario==None:
                                    <td><input type="text" name="jupiterpersona_usuariosun" id="jupiterpersona_usuariosun"  value="${usuario}"></td>
                                %else:
                                    <td><input type="text" name="jupiterpersona_usuariosun" id="jupiterpersona_usuariosun" readonly value="${usuario}"></td>
                                %endif

                            %else:
                                <td><input type="text" name="jupiterpersona_usuariosun" id="jupiterpersona_usuariosun" value="${usuario}" style="display: none;"></td>
                            %endif

              </tr>
              </table>

           <table>
                   <tr>
                     <td>${_('Beneficiary: ')}</td>
                       %if list['esbeneficiario']==1:
                        <td><input type="checkbox" name="jupiterpersona_esbeneficiario" id="jupiterpersona_esbeneficiario" checked onchange="versecciondebeneficiarioenedit();"></td>
                         %else:
                           <td><input type="checkbox" name="jupiterpersona_esbeneficiario" id="jupiterpersona_esbeneficiario" onchange="versecciondebeneficiarioenedit();"></td>
                            %endif


                 </tr>

          </table>

          %if list['esbeneficiario']==1:
                  <table id="jupiter_datosbeneficiario" style="display: block;">
          %else:
              <table id="jupiter_datosbeneficiario" style="display: none;">
          %endif

              <tr height="40px">
                  <td width="80px">Cuenta 1:</td><td><input type="text" id="jupiter_cuenta1" name="jupiter_cuenta1" value="${list['cta1']}"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Banco 1:</td><td><input type="text" id="jupiter_banco1" name="jupiter_banco1" value="${list['banco1']}"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Cuenta 2:</td><td><input type="text" id="jupiter_cuenta2" name="jupiter_cuenta2" value="${list['cta2']}"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Banco 2:</td><td><input type="text" id="jupiter_banco2" name="jupiter_banco2" value="${list['banco2']}"></td>
              </tr>
          </table>
           <table>
                   <tr>
                     <td>${_('Checker: ')}</td>

                       %if list['eschecador']==1:
                        <td><input type="checkbox" name="jupiterpersona_eschecador" id="jupiterpersona_eschecador" onclick="verseccionchecador();" checked></td>
                         %else:
                           <td><input type="checkbox" name="jupiterpersona_eschecador" id="jupiterpersona_eschecador" onclick="verseccionchecador();" ></td>
                            %endif


                 </tr>
          </table>

          %if list['eschecador']==1:
              <table id="jupiter_datoschecador" style="visibility: visible">
           %else:
              <table id="jupiter_datoschecador" style="visibility: hidden">
           %endif

          <tr>
          <td>${_('Base: ')}</td><td><select id="jupiterchecador_base" name="jupiterchecador_base">
                    %for item in allbase:
                         %if item.punto==list['id_punto']:
                             <option selected>${item.punto}</option>
                         %else:
                             <option>${item.punto}</option>
                         %endif


                        %endfor
                    </select></td>
          </tr>
          </table>
          <br><br>
          <input type="submit" name="jupiter_submit_uploadoperator" value="GUARDAR"/>

        </form>
      <br>
      <iframe name="requestpersonasedit" id="requestpersonasedit" frameborder="0"></iframe>


  </fieldset>
</html>