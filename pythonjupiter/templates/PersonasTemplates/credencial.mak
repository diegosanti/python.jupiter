<script>
$(document).ready(function() {
                         $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/personas/getqr?id=${persona.id_persona}",
                                            contentType: "application/json; charset=utf-8",
                                            data: {},
                                            async: false,
                                            beforeSend: function() {
                                                //document.getElementById("salidademontomaxm"+elid[1]).innerHTML="<img src=\"${h.url()}/img/ajax-loader.gif\">";
                                            },
                                            success: function(data) {
                                                $("#elcodigoqrpersona").html("<img src=\"data:;base64,"+data['codigoqr']+"\"");
                                            },
                                            error: function() {
                                                 $.alert("${_('Error accessing to')} /personas/getqr", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
});
</script>

<table border="2" style="background-repeat: no-repeat;" >
    <tr height="50px" width="300px">
        <td background="${h.url()}/img/fondocredencial3.jpg" style="text-align: center; height: 50px; width: 380px;">
            <h1><b>${empresa}</b></h1>
        %if persona.foto!=None:
           <img src="${h.url()}/img/${internal_id}/personas/${persona.id_persona}.-/${persona.foto}" style="width: 120px; height: 120px">

        %else:
            <img src="${h.url()}/img/personasilueta.gif" style="width: 120px; height: 120px">
        %endif
           <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NUMERO OPERADOR:</b> <i>${persona.numerooperador}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NOMBRE:</b> <i>${persona.nombre}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>RFC:</b> <i>${persona.rfc}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>CURP:</b> <i>${persona.curp}</i></p>
             <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>FECHA DE NACIMIENTO:</b> <i>${persona.fecha_nacimiento}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NUMERO DE LICENCIA:</b> <i>${persona.numerolicencia}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>FECHA VENCIMIENTO LICENCIA:</b> <i>${persona.fecha_vencimientolicencia}</i></p>
        </td>
        <td background="${h.url()}/img/fondocredencial3.jpg" style="text-align: center; height: 50px; width: 380px;">
            <p>QR</p>
            <img src="data:;base64,${codigoqr}" style="width: 220px; height: 220px">
        </td>
    </tr>
</table>