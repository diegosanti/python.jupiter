
<h4><b>Unidad Actual</b>:</h4> <a>${queryunidad}</a>
<h4><b>Operador</b>:</h4><a>${querypersona.nombre}</a>

<h4><b>Historico de Unidades: </b></h4>
<table style="width: 100%">
    <tr>
        <th>Fecha Asignado</th>
        <th>Unidad</th>
    </tr>
    %for item in listahistorico:
        <tr>
            <td>${item['fecha']}</td>
            <td>${item['unidad']}</td>
        </tr>
    %endfor
</table>
