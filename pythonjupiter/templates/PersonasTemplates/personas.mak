<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
   <%doc> <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script></%doc>

   <style type="text/css">
        a{
           color: black;
           text-decoration: none;
       }
   </style>
<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>


   <script type="text/javascript">

                      var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');



       function searchenterpersona(e) {

    if (e.keyCode === 13 && !e.shiftKey)  {

        searchpersona();

         }
       }


       function jupiter_cambiovistapersona() {
                var tipo=document.getElementById("jupiter_vertipopersona").value;

                           $.ajax({
                    type: "GET",
                    url: "${h.url()}/personas/tipopersona?tipo="+tipo+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {
                                $('#jupiter_tablepersonas tbody').remove();
                                $('#jupiter_selectbuscarpersonas option').remove();


            if(document.getElementById("jupiter_vertipopersona").value=='Permisionario' || document.getElementById("jupiter_vertipopersona").value=='Beneficiario'){
            $('#jupiter_tablepersonas').append("<tr><th></th><th></th><th>${_('NO.')}</th><th>${_('Name')}</th><th>${_('Apodo')}</th><th>${_('Foto')}</th><th>${_('Status')}</th><th>${_('Telefono')}</th><th>${_('Celular')}</th>"+
                    "<th>${_('Observaciones')}</th><th>${_('Action')}</th></tr>");

            $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(s)");
            $('#jupiter_selectbuscarpersonas').append("<option>Nombre</option><option>Apodo</option>");
                for (var con in data['hd']){
                    if(data['hd'][con]['habilitado']=='ACTIVA'){
                        if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }

                }
            }
           if(document.getElementById("jupiter_vertipopersona").value=='Checador'){
                 $('#jupiter_tablepersonas').append("<tr><th></th><th></th><th>${_('NO.')}</th><th>${_('Name')}</th><th>${_('Apodo')}</th><th>${_('Foto')}</th><th>${_('Status')}</th><th>${_('Telefono')}</th><th>${_('Celular')}</th>"+
                    "<th>${_('Base')}</th><th>${_('Observaciones')}</th><th>${_('Action')}</th></tr>");
                 $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value +"(es)");
               $('#jupiter_selectbuscarpersonas').append("<option>Nombre</option><option>Apodo</option><option>Base</option>");
                for (var con in data['hd']){
                            if(data['hd'][con]['habilitado']=='ACTIVA'){
                                                                if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['base']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }
               if(document.getElementById("jupiter_vertipopersona").value=='Operador'){
                 $('#jupiter_tablepersonas').append("<tr><th></th><th></th><th></th><th></th><th>${_('NO.')}</th><th>${_('Name')}</th><th>${_('Apodo')}</th><th>${_('Foto')}</th><th>${_('Status')}</th><th>${_('Empresa')}</th>"+
                            "<th>${_('RFC')}</th><th>${_('Telefono')}</th><th>${_('Celular')}</th><th>${_('Licencia')}</th><th>${_('Fecha Vencimiento')}</th><th>${_('Estado de Licencia')}</th>"+
                            "<th>${_('Observaciones')}</th><th>${_('Action')}</th></tr>");
                 $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(es)");
                    $('#jupiter_selectbuscarpersonas').append(" <option>credencial</option><option>nombre</option><option>apodo</option><option>linea</option><option>estatus</option>"+
                                                                "<option>Licencia por Vencer</option><option>Licencias Vencidas</option>");
                   var anexo;
                    for (var con in data['hd']){
                                        if(data['hd'][con]['habilitado']=='ACTIVA'){
                                             if(data['hd'][con]['estadodelicencia']=='LEJOS'){
                                           anexo='<td><img src=\"${h.url()}/img/check.png\" style=\"width: 70px; height: 70px;\"></td>';
                                            }else{
                                                if(data['hd'][con]['estadodelicencia']=='VENCIO'){
                                               anexo='<td><img src=\"${h.url()}/img/red-cross.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }else{
                                                  anexo='<td><img src=\"${h.url()}/img/aviso3.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }
                                            }
                                                                            if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                             // "<td><a onclick=\"operadorunidad("+data['hd'][con]['id_persona']+");\"><i class=\"fa fa-car\"></i></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Mostrar Archivos','${h.url()}/personas/archivosde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-paperclip\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Credencial','${h.url()}/personas/generarcredencial?id="+data['hd'][con]['id_persona']+"',"+data['hd'][con]['id_persona']+");\"><span class=\"glyphicon glyphicon-credit-card\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['empresa']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['rfc']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['licencia']+"</a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['fecha_vencimiento']+"</a></td>"+
                             anexo+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }


                    },
                    error: function () {
                        $.alert("Error accessing reloadoperadores", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });


         }


var $dialogjupiterpersona;


                      function closeJupiterPersonaWindow(){
            $dialogjupiterpersona.dialog('close');
        }

       function openWindowpersonas(text,url,id) {
           doAjax(url, 'openTab');
           if (text != 'Credencial') {
               $dialogjupiterpersona = $('<div></div>')
                       .html('<div id="openTab" style="margin:10px"></div>')
                       .dialog({
                           autoOpen: false,
                           modal: true,
                           height: 580,
                           width: 820,
                           resizable: false,
                           draggable: true,
                           position: {my: "center", at: "top", of: window},
                           close: function () {
                               $(this).dialog('destroy').remove();
                               Dialog03templatehabilita.dialog("close");
                               reloadallpersonas();
                           },
                           title: text,
                           data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                           buttons: {

                               <%doc>                 "Close": function ()
                                                {
                                                    $(this).dialog('destroy').remove();
                                                    Dialog03templatehabilita.dialog("close");
                                                    reloadallpersonas();
                                                }</%doc>
                           }
                       });
               $dialogjupiterpersona.dialog('open');
           }else{

                             $dialogjupiterpersona = $('<div></div>')
                       .html('<div id="openTab" style="margin:10px"></div>')
                       .dialog({
                           autoOpen: false,
                           modal: true,
                           height: 580,
                           width: 820,
                           resizable: false,
                           draggable: true,
                           position: {my: "center", at: "top", of: window},
                           close: function () {
                               $(this).dialog('destroy').remove();
                               Dialog03templatehabilita.dialog("close");
                               reloadallpersonas();
                           },
                           title: text,
                           data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                           buttons: {
                               <%doc>

                               "PDF": function ()
                               {
                                    $.ajax({
                                        type: "GET",
                                        url: "${h.url()}/personas/credencialpdf?id="+id+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                                        contentType: "application/json; charset=utf-8",
                                        data: {},
                                        success: function (parameterdata) {
                                            //Insert HTML code
                                            //alert(parameterdata['id']);
                                            //alert(parameterdata['file_name']);
                                            var link = document.createElement("a");
                                            var file_path = '${h.url()}/'+parameterdata['file_name'];
                                            link.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                                            link.href = file_path;
                                            link.target="_blank";
                                            link.setAttribute('download',"download");
                                            link.click();


                                        },
                                        error: function () {
                                            alert("Error accessing server /");
                                        },
                                        complete: function () {
                                        }
                                    });
                               }</%doc>
                           }
                       });
               $dialogjupiterpersona.dialog('open');

           }
       }


       function reloadallpersonas() {
            document.getElementById("jupiter_filterpersona").value="";
            var by=document.getElementById("jupiter_vertipopersona").value;
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/personas/reloadpersonas?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}&by="+by,
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablepersonas td').remove();

      if(document.getElementById("jupiter_vertipopersona").value=='Permisionario' || document.getElementById("jupiter_vertipopersona").value=='Beneficiario'){
                 $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(s)");
                for (var con in data['hd']){
                    if(data['hd'][con]['habilitado']=='ACTIVA'){
                                   if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }

                }
            }
           if(document.getElementById("jupiter_vertipopersona").value=='Checador'){
           $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(es)");
                for (var con in data['hd']){
                            if(data['hd'][con]['habilitado']=='ACTIVA'){
                                                                if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['base']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }
               if(document.getElementById("jupiter_vertipopersona").value=='Operador'){
                 $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(es)");
                   var anexo;
                    for (var con in data['hd']){
                                        if(data['hd'][con]['habilitado']=='ACTIVA'){
                                                    if(data['hd'][con]['estadodelicencia']=='LEJOS'){
                                           anexo='<td><img src=\"${h.url()}/img/check.png\" style=\"width: 70px; height: 70px;\"></td>';
                                            }else{
                                                if(data['hd'][con]['estadodelicencia']=='VENCIO'){
                                               anexo='<td><img src=\"${h.url()}/img/red-cross.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }else{
                                                  anexo='<td><img src=\"${h.url()}/img/aviso3.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }
                                            }
                                                                            if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                              //"<td><a onclick=\"operadorunidad("+data['hd'][con]['id_persona']+");\"><i class=\"fa fa-car\"></i></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Mostrar Archivos','${h.url()}/personas/archivosde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-paperclip\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Credencial','${h.url()}/personas/generarcredencial?id="+data['hd'][con]['id_persona']+"',"+data['hd'][con]['id_persona']+");\"><span class=\"glyphicon glyphicon-credit-card\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['empresa']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['rfc']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['licencia']+"</a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['fecha_vencimiento']+"</a></td>"+
                             anexo+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }


                    },
                    error: function () {
                        $.alert("Error accessing reloadpersonas", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }




       function searchpersona(){
           var searchfor = document.getElementById("jupiter_selectbuscarpersonas").value;
           var by=document.getElementById("jupiter_vertipopersona").value;
           var value = document.getElementById("jupiter_filterpersona").value;
           $.ajax({
                    type: "GET",
                    url: "${h.url()}/personas/searchpersona?name="+value+"&searchfor="+searchfor+"&by="+by+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablepersonas td').remove();

                                   $('#jupiter_tablepersonas td').remove();

      if(document.getElementById("jupiter_vertipopersona").value=='Permisionario' || document.getElementById("jupiter_vertipopersona").value=='Beneficiario'){
 $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(s)");
                for (var con in data['hd']){
                    if(data['hd'][con]['habilitado']=='ACTIVA'){
                                   if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }

                }
            }
           if(document.getElementById("jupiter_vertipopersona").value=='Checador'){
           $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(es)");
                for (var con in data['hd']){
                            if(data['hd'][con]['habilitado']=='ACTIVA'){
                                                                if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['base']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }
               if(document.getElementById("jupiter_vertipopersona").value=='Operador'){
                     $("#bookmarkpersonas").html(data['contador'] + " "+document.getElementById("jupiter_vertipopersona").value+"(es)");
                   var anexo;
                    for (var con in data['hd']){
                                        if(data['hd'][con]['habilitado']=='ACTIVA'){
                                            if(data['hd'][con]['estadodelicencia']=='LEJOS'){
                                           anexo='<td><img src=\"${h.url()}/img/check.png\" style=\"width: 70px; height: 70px;\"></td>';
                                            }else{
                                                if(data['hd'][con]['estadodelicencia']=='VENCIO'){
                                               anexo='<td><img src=\"${h.url()}/img/red-cross.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }else{
                                                  anexo='<td><img src=\"${h.url()}/img/aviso3.png\" style=\"width: 70px; height: 70px;\"></td>';
                                                }
                                            }

                                                      if(data['hd'][con]['foto']==null){
                            var foto="<td> <img src=\"${h.url()}/img/personasilueta.gif\" style=\"width: 100px; height: 100px\"></td>";
                        }
                        else{
                            var foto="<td><img src=\"${h.url()}/img/"+data['internal_id']+"/personas/"+data['hd'][con]['id_persona']+".-/"+data['hd'][con]['foto']+"\" style=\"width: 100px; height: 100px\"></td>";
                        }
                    $('#jupiter_tablepersonas').append("<tr><td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-edit\"></span></a></td>"+
                            //"<td><a onclick=\"operadorunidad("+data['hd'][con]['id_persona']+");\"><i class=\"fa fa-car\"></i></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-th-list\"></span></a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Mostrar Archivos','${h.url()}/personas/archivosde?id="+data['hd'][con]['id_persona']+"');\"><span class=\"glyphicon glyphicon-paperclip\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Credencial','${h.url()}/personas/generarcredencial?id="+data['hd'][con]['id_persona']+"',"+data['hd'][con]['id_persona']+");\"><span class=\"glyphicon glyphicon-credit-card\"></span></a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['numerooperador']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['persona']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['apodo']+"</a></td>"+
                            foto+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['estatus']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['empresa']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['rfc']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['telefono']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['celular']+"</a></td>"+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['licencia']+"</a></td>"+
                             "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['fecha_vencimiento']+"</a></td>"+
                             anexo+
                            "<td><a onclick=\"openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id="+data['hd'][con]['id_persona']+"');\">"+data['hd'][con]['observaciones']+"</a></td>"+
                            "<td><a onclick=\"funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id="+data['hd'][con]['id_persona']+"&persona="+data['hd'][con]['persona']+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></td></tr>");
                    }
                }

           }


                    },
                    error: function () {
                        $.alert("Error accessing searchoperador", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });

        }

        function ocultartext() {
            if(document.getElementById('jupiter_selectbuscarpersonas').value=='Licencia por Vencer' || document.getElementById('jupiter_selectbuscarpersonas').value=='Licencias Vencidas'){
                document.getElementById('jupiter_filterpersona').style.visibility='hidden';
            }else{
                document.getElementById('jupiter_filterpersona').style.visibility='visible';
            }
          }


       function funcdeshabilitar(title,url) {
               $('<div></div>').appendTo('body')
    .html('<div><h6>¿Seguro desea deshabilitar esta persona?</h6></div>')
    .dialog({
        modal: true, title: 'Deshabilitar Persona', zIndex: 10000, autoOpen: true,
        width: 'auto', resizable: false,
        buttons: {
            Yes: function () {
                // $(obj).removeAttr('onclick');
                // $(obj).parents('.Parent').remove();
                       var by = document.getElementById("jupiter_vertipopersona").value;
               $.ajax({
                   type: "GET",
                   url: url + "&by=" + by,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (data) {
                       $.alert(data,{type: 'success'});
                       reloadallpersonas();


                   },
                   error: function (data) {
                       //$.alert("Error accessing funcdeshabilitar", {autoClose: false,});
                       //return true;
                        $.alert('Hecho',{type: 'success'});
                       reloadallpersonas();
                   },
                   complete: function () {
                   }
               });


                $(this).dialog("close");
            },
            No: function () {

                $(this).dialog("close");
            }
        },
        close: function (event, ui) {
            $(this).remove();
        }
    });
           //var confirma = confirm("¿Seguro quieres deshabilitar a esta persona?");

       }



                var winHeighttemplatehabilita = Math.round(window.innerHeight * .85);
        var winWidthtemplatehabilita = Math.round(window.innerWidth * .55);

        var addFilter01Buttonstemplatehabilita = {

            "Cancel": function () {

                Dialog03templatehabilita.dialog("close");
            }
        };
        var Dialog03templatehabilita = $("#dialoghabilita").dialog({
            autoOpen: false,
            height: winHeighttemplatehabilita,
            width: winWidthtemplatehabilita,
            modal: true,
            buttons: addFilter01Buttonstemplatehabilita,
            close: function () {

            }
        });


       function templatehabilita(title,ur) {
           var by=document.getElementById("jupiter_vertipopersona").value;


        // Append html
        $.ajax({
            type: "GET",
            url: ur+"&by="+by,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialoghabilita").html(parameterdata.template);
                Dialog03templatehabilita.data('rowId', 1);
                Dialog03templatehabilita.dialog("open");

            },
            error: function () {
                alert("Error accessing server /Templatehabilita")
            },
            complete: function () {
            }
        });
         }


         function operadorunidad(id) {
            var winHeight=Math.round(window.innerHeight*.7);
             var winWidth=Math.round(window.innerWidth*.6);
                     var addFilter01Buttons = {

                         "Asignar": function () {
                                var winHeight=Math.round(window.innerHeight*.7);
                                var winWidth=Math.round(window.innerWidth*.6);
                                var addFilter01Buttons = {
                                    "Guardar": function() {
                                        var nuevaunidad = $('#jupiterselectunidadasignar').val();
                                                 $.ajax({
                                                     type: "GET",
                                                     url: "${h.url()}/personas/templateunidaddeoperadorasignarguardar?id="+id+"&unidad="+nuevaunidad+"&internal_id=${internal_id}&user=${user}",
                                                     contentType: "application/json; charset=utf-8",
                                                     data: {},
                                                success: function (parameterdata) {
                                                    //addFilter01Dialog.dialog("close");
                                                    //addFilter01Dialogasignar.dialog("close");
                                                if(parameterdata['error']=='ok'){
                                                      addFilter01Dialog.dialog("close");
                                                      addFilter01Dialogasignar.dialog("close");
                                                }

                                                },
                                                error: function () {
                                                        alert("Error accessing server /templateunidaddeoperadorasignarguardar")
                                                },
                                                complete: function () {
                                                }
                                            });

                                        }
                                };

                                var addFilter01Dialogasignar = $("#unidadoperadordivasignar" ).dialog({
                                autoOpen: false,
                                height: winHeight-100,
                                width: winWidth-200,
                                modal: true,
                                buttons: addFilter01Buttons,
                                close: function() {
                                }
                        });
                        //addFilter01Dialog.data('rowId',1);
                        //addFilter01Dialog.dialog( "open" );
                            $.ajax({
                        type: "GET",
                        url: "${h.url()}/personas/templateunidaddeoperadorasignar?id="+id+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                                success: function (parameterdata) {
                            //Insert HTML code
                            $("#unidadoperadordivasignar").html(parameterdata.template);
                            addFilter01Dialogasignar.data('rowId', 1);
                             addFilter01Dialogasignar.dialog("open");

                            },
                            error: function () {
                                alert("Error accessing server /templateunidaddeoperadorasignar")
                            },
                            complete: function () {
                            }
                        });

                           }
            };
            var addFilter01Dialog = $("#unidadoperadordiv" ).dialog({
                    autoOpen: false,
                    height: winHeight-100,
                    width: winWidth-200,
                    modal: true,
                    buttons: addFilter01Buttons,
                    close: function() {
                    }
             });
            //addFilter01Dialog.data('rowId',1);
            //addFilter01Dialog.dialog( "open" );
                $.ajax({
            type: "GET",
            url: "${h.url()}/personas/templateunidaddeoperador?id="+id,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#unidadoperadordiv").html(parameterdata.template);
                addFilter01Dialog.data('rowId', 1);
                addFilter01Dialog.dialog("open");

            },
            error: function () {
                alert("Error accessing server /templateunidaddeoperador")
            },
            complete: function () {
            }
        });
           }


       function jupiter_gridunidadoperador() {
                var winHeight=Math.round(window.innerHeight*.85);
                var winWidth=Math.round(window.innerWidth*.85);
                     var addFilter01Buttons = {};

                var addFilter01Dialogasignar = $("#jupitergridunidadconoperador" ).dialog({
                                autoOpen: false,
                                height: winHeight-100,
                                width: winWidth-200,
                                modal: true,
                                buttons: addFilter01Buttons,
                                close: function() {
                                }
                        });
                        $.ajax({
                        type: "GET",
                        url: "${h.url()}/personas/gridunidadoperador?internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                                success: function (parameterdata) {
                            //Insert HTML code
                            $("#jupitergridunidadconoperador").html(parameterdata.template);
                            addFilter01Dialogasignar.data('rowId', 1);
                             addFilter01Dialogasignar.dialog("open");

                            },
                            error: function () {
                                alert("Error accessing server /gridunidadoperador")
                            },
                            complete: function () {
                            }
                        });
         }



   </script>


<!--main content start-->
VER: <select id="jupiter_vertipopersona" onchange="jupiter_cambiovistapersona()"><option selected>Operador</option><option>Permisionario</option><option>Checador</option><option>Beneficiario</option></select>

<br><br>


                    <button style="float: left;" onclick="openWindowpersonas('Nueva Persona','${h.url()}/personas/nuevapersona');">
                        <span class="glyphicon glyphicon-plus">${_('New')}</span>
                    </button>
                    <button style="float: left;">
                        <span class="glyphicon glyphicon-ok"><a onclick="templatehabilita('Habilitar Personas','${h.url()}/personas/habilitawho?internal_id=${internal_id}&application_id=${application_id}')">${_('Habilitar')}</a></span>
                    </button>
                    <!--<button style="float: left;" onclick="jupiter_gridunidadoperador();">
                        <i class="fa fa-car">Unidades</i>
                    </button>-->
                    <br><br>

BUSCAR POR: <select id="jupiter_selectbuscarpersonas" onchange="ocultartext();">
    <option>credencial</option>
    <option>nombre</option>
    <option>apodo</option>
    <option>linea</option>
    <option>Licencia por Vencer</option>
    <option>Licencias Vencidas</option>

</select>

  <input type="text" id="jupiter_filterpersona" onkeypress="searchenterpersona(event);" placeholder="Personas"><button onclick=searchpersona()><i class="fa fa-search"></i></button><button onclick="reloadallpersonas()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    &nbsp;&nbsp;<a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: none;" ></a>

<div id="bookmarkpersonas" class="bookmark" style="float: right;">${contador} Operador(es)</div>



<table id="jupiter_tablepersonas" class="userTable" >
                        <tr>
                            <th></th>
                            <!--<th></th>-->
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>${_('NO.')}</th>
                            <th>${_('Name')}</th>
                            <th>${_('Apodo')}</th>
                            <th>${_('Foto')}</th>
                            <th>${_('Status')}</th>
                            <th>${_('Empresa')}</th>
                            <th>${_('RFC')}</th>
                            <th>${_('Telefono')}</th>
                            <th>${_('Celular')}</th>
                            <th>${_('Licencia')}</th>
                            <th>${_('Fecha Vencimiento')}</th>
                            <th>${_('Estado de Licencia')}</th>
                            <th>${_('Observaciones')}</th>
                            <th>${_('Action')}</th>


                        </tr>

                        %for item in hd:

                        %if item['habilitado']=='ACTIVA':
                        <tr>
                            <td> <a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');"><span class="glyphicon glyphicon-edit"></span></a></td>
                            <!--<td><a onclick="operadorunidad(${item['id_persona']});"><i class="fa fa-car"></i></a></td>-->
                            <td> <a onclick="openWindowpersonas('Mostrar Historial','${h.url()}/personas/historialde?id=${item['id_persona']}');"><span class="glyphicon glyphicon-th-list"></span></a></td>
                            <td> <a onclick="openWindowpersonas('Mostrar Archivos','${h.url()}/personas/archivosde?id=${item['id_persona']}');"><span class="glyphicon glyphicon-paperclip"></span></a></td>
                            <td><a onclick="openWindowpersonas('Credencial','${h.url()}/personas/generarcredencial?id=${item['id_persona']}',${item['id_persona']});"><span class="glyphicon glyphicon-credit-card"></span></a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['numerooperador']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['persona']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['apodo']}</a></td>
                            %if item['foto']!=None:
                            <td> <img src="${h.url()}/img/${internal_id}/personas/${item['id_persona']}.-/${item['foto']}" style="width: 100px; height: 100px"></td>
                            %else:
                            <td> <img src="${h.url()}/img/personasilueta.gif" style="width: 100px; height: 100px"></td>
                            %endif
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['estatus']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['empresa']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['rfc']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['telefono']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['celular']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['licencia']}</a></td>
                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['fecha_vencimiento']}</a></td>

                        %if item['estadodelicencia']=='VENCERA':

                            <td><img src="${h.url()}/img/aviso3.png" style="width: 70px; height: 70px;"></td>
                        %elif item['estadodelicencia']=='LEJOS':
                            <td><img src="${h.url()}/img/check.png" style="width: 70px; height: 70px;"></td>
                        %else:
                            <td><img src="${h.url()}/img/red-cross.png" style="width: 70px; height: 70px;"></td>
                        %endif

                            <td><a onclick="openWindowpersonas('Editar Persona','${h.url()}/personas/editpersona?id=${item['id_persona']}');">${item['observaciones']}</a></td>

                            <td><a onclick="funcdeshabilitar('Deshabilitar Persona','${h.url()}/personas/disablepersona?id=${item['id_persona']}&persona=${item['persona']}');"><span class="glyphicon glyphicon-remove"></span></a></td>
                        </tr>
                            %endif
                    %endfor:
                    </table><br><br>


              <br>
                            <div id="dialoghabilita" title="Habilitar"></div>
                            <div id="unidadoperadordiv" title="Operador Tiene La Unidad"></div>
                             <div id="unidadoperadordivasignar" title="Asignar Unidad"></div>
                            <div id="jupitergridunidadconoperador" title="Unidades con Operador"></div>

      <!-- page start-->
      <!-- page end-->
<br>