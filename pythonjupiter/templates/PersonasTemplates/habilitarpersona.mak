<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>

<h1>Para Activar</h1>


<table class="userTable">

    <tr>
        <th>${'NAME'}</th>
        <th>${'APODO'}</th>
        <th>${'ACTION'}</th>
    </tr>

    %for item in allrecords:
        <tr>
        <td>${item.nombre}</td>
            <td>${item.apodo}</td>
            <td><a onclick="openWindowpersonas('Habilitar Persona','${h.url()}/personas/enable?apodo=${item.apodo}&id=${item.id_persona}&by=${by}');"><span class="glyphicon glyphicon-ok">${_('Enable')}</span></a></td>
        </tr>
    %endfor


</table>