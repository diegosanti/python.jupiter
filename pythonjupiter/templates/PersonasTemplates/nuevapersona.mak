<html>
<%doc> <script src="https://code.jquery.com/jquery.js"></script>
  <script src="${tg.url('/javascript/bootstrap.min.js')}"></script></%doc>
<head>
    <style>
          .thumb {
            height: 300px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
          }
        </style>

</head>

        <script>



            function verseccionoperador() {
                var checkBox=document.getElementById("jupiterpersona_esoperador");

                if (checkBox.checked == true){
                    document.getElementById("jupiterpersona_divoperador").style.display='block';
                    document.getElementById("jupiteroperadores_licencia").required=true;
                    document.getElementById("jupiteroperadores_linea").required=true;
                    document.getElementById("jupiteroperadores_duedate").required=true;
                    document.getElementById("jupiteroperadores_licencearchivo").required=true;
                    //document.getElementById("jupiteroperadores_noss").required=true;
                    document.getElementById("jupiteroperadores_licence").required=true;
                    document.getElementById("jupiter_tabladecargosenoperadores").style.display='block';
                    document.getElementById("legendcharges").style.display='block';



                }else{
                    document.getElementById("jupiterpersona_divoperador").style.display='none';
                    document.getElementById("jupiteroperadores_licencia").required=false;
                    document.getElementById("jupiteroperadores_linea").required=false;
                    document.getElementById("jupiteroperadores_duedate").required=false;
                    document.getElementById("jupiteroperadores_licencearchivo").required=false;
                    //document.getElementById("jupiteroperadores_noss").required=false;
                    document.getElementById("jupiteroperadores_licence").required=false;
                    document.getElementById("jupiter_tabladecargosenoperadores").style.display='none';
                    document.getElementById("legendcharges").style.display='none';


                }

              }

                          function verseccionchecador() {
                var checkBox=document.getElementById("jupiterpersona_eschecador");

                if (checkBox.checked == true){
                    document.getElementById("jupiter_datoschecador").style.visibility='visible';

                }else{
                    document.getElementById("jupiter_datoschecador").style.visibility='hidden';
                }

              }




              function archivo(evt) {
                  var files = evt.target.files; // FileList object

                  // Obtenemos la imagen del campo "file".
                  for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {
                          // Insertamos la imagen
                         document.getElementById("jupiter_fotoshowpersona").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                        };
                    })(f);

                    reader.readAsDataURL(f);
                  }
              }


              document.getElementById('jupiter_fotopersona').addEventListener('change', archivo, false);



            function validar(esto){
                    valido=false;
                for(a=0;a<esto.elements.length;a++){
                        if(esto[a].type=="checkbox" && esto[a].checked==true){
                            valido=true;
                            break
                    }

                }
            if(!valido){
                    $.alert("Seleccione un tipo de persona!",{type: 'warning'});return false
                }else{

            }

            }

                  function actualizandocargos() {

                 var empresa=document.getElementById("jupiteroperadores_linea").value;
                 var grupo=document.getElementById("seriedegrupos").value;

                             $.ajax({
                        type: "GET",
                        url: "${h.url()}/cargosoperadores/actualizacargosoperadores?empresa="+empresa+"&internal_id=${internal_id}&app_name=${app_name}&grupo="+grupo+"&application_id=${application_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                            $('#jupiter_tabladecargosenoperadores td').remove();
                    for(var k in parameterdata['construir']){
                        //alert(parameterdata['construir'][k]);
                        $('#jupiter_tabladecargosenoperadores').append('<tr><td><input type=\"text\" id=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\" name=\"jupitercargo'+parameterdata['construir'][k]['cargo']+'\"' +
                                'value=\"'+parameterdata['construir'][k]['cargo']+'\" readonly></td><td>Monto: <input name=\"montode'+parameterdata['construir'][k]['cargo']+'\" id=\"montode'+parameterdata['construir'][k]['cargo']+'\" type=\"number\" size=\"8\" value=\"'+parameterdata['construir'][k]['monto']+'\"></td></tr>');

                    }
                },
                error: function () {
                        alert("ERROR actualizando los cargos");
                },
                complete: function () {
                 }
                });
              }


            function actualizandocargosgrupo(esto) {

              }

              function ver_parteusuariopermi() {
                if(document.getElementById("jupiterpersona_espermisionario").checked==true) {

                    document.getElementById("trofuserpermi").style.display = 'block';
                    //document.getElementById("jupiter_theuserpermi").required=true;
                }else {
                    document.getElementById("trofuserpermi").style.display = 'none';
                    //document.getElementById("jupiter_theuserpermi").required=false;
                }

                }


                function jupiter_envioformnuevapersona() {
                    var info = $('#personasform').serialize();
                    var foto=document.getElementById("jupiter_fotopersona");
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/personas/uploadPersona?'+info,
                        contentType: "application/json; charset=utf-8",
                        data:{ 'internal_id':"${internal_id}",'user':"${user}",'jupiter_fotopersona': foto},
                        success: function(data) {
                            //Insert HTML code
                        if (data['error'] == "ok"){
                            //$( "#VenusTicketFormulary" ).html("<h3>Ticket: "+data.ticket+"</h3>");
                            closeJupiterPersonaWindow();
                        }else{
                            $.alert('error',{type: "warning"});
                        }
                    },
                    error: function() {
                            $.alert("${_('Error accessing server')} /personas/uploadPersona",{type: "danger"});
                    },
                    complete: function() {

                    }
                    });
                  }

            function cuentaconnumerooperador() {
                var theselect=document.getElementById("tienenumerooperadorq").value;
                if(theselect=='si') {
                    document.getElementById("numerodeoperadorasignado").style.display = 'block';
                    document.getElementById("labelnumerodeoperadorasignado").style.display = 'block';
                     document.getElementById("numerodeoperadorasignado").required=true;
                }else {
                    document.getElementById("numerodeoperadorasignado").style.display = 'none';
                    document.getElementById("labelnumerodeoperadorasignado").style.display = 'none';
                    document.getElementById("numerodeoperadorasignado").required=false;

                }

              }

              function insertandoeninputoculto(esto) {
                  //alert(esto.value);
                  if (esto.checked == true) {
                      var esta = document.getElementById("seriedegrupos").value.split(",");
                      if (esta.includes(esto.value)) {

                      } else {
                          document.getElementById("seriedegrupos").value = document.getElementById("seriedegrupos").value+','+esto.value ;
                      }

                      //alert(document.getElementById("seriedegrupos").value);
                  }else{
                       var esta = document.getElementById("seriedegrupos").value;
                       var nuevacadena=esta.replace(','+esto.value,"");
                       document.getElementById("seriedegrupos").value=nuevacadena;
                       //alert(document.getElementById("seriedegrupos").value);


                  }
                  actualizandocargos();
              }


              function versecciondebeneficiario() {
                var esto=document.getElementById("jupiterpersona_esbeneficiario").checked;

                if(esto==true){
                  document.getElementById("jupiter_datosbeneficiario").style.display='block';
                  document.getElementById("jupiter_cuenta1").required=true;
                  document.getElementById("jupiter_banco1").required=true;


                }else{
                    document.getElementById("jupiter_datosbeneficiario").style.display='none';
                    document.getElementById("jupiter_cuenta1").required=false;
                  document.getElementById("jupiter_banco1").required=false;

                }

                }

      </script>
  <fieldset>
        <legend>${_('Add Person')}</legend>

      <form action="${h.url()}/personas/uploadPersona?internal_id=${internal_id}&user=${user}&application_id=${application_id}&app_name=${app_name}" name="personasform" id="personasform" method="POST" enctype="multipart/form-data" target="requestpersonas" accept-charset="UTF-8" onsubmit="return validar(this)">
    <p>* ${_('Required Files')}</p>

          <br>
            <p>${_('Upload Photo of the Person')}:  ${_('Only')} (jpeg,bmp,png)</p>
        <input type="file" name="jupiter_fotopersona" id="jupiter_fotopersona" accept="image/vnd-wap-wbmp,image/png, image/jpeg" />  <br/>
          <output id="jupiter_fotoshowpersona"></output>
             <table>
                 <tr>
                      <td>*${_('Name: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_nombre" id="jupiterpersona_nombre" length="50" maxlength="100" required><br></td>
                 </tr>
        <tr>
            <td>${_('Apodo: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_apodo" length="50" maxlength="25"><br></td>
        </tr>

                 <tr>
                     <td>${_('RFC: ')} </td>
                     <td><input type="text" id="jupiterpersona_rfc" name="jupiterpersona_rfc" maxlength="13" style="text-transform: uppercase"></td>
                 </tr>
                  <tr>
                <td>*${_('Birthday: ')}</td><td><input type="date" name="jupiterpersona_fecha_nacimiento"  required></td>
        </tr>
          <tr>
              <td>${_('Email: ')} &nbsp;</td><td><input type="email" id="jupiterpersona_email" name="jupiterpersona_email" length="50" maxlength="50"><br></td>
        </tr>
          <tr>
              <td>*${_('Gender: ')} &nbsp;</td><td><select id="jupiterpersona_genero" name="jupiterpersona_genero" length="50" required><br>

                                                    <option>MASCULINO</option>
                                                    <option>FEMENINO</option>

                                            </select></td>
        </tr>

                  <tr>
                <td>${_('Marital Status : ')}</td><td><select name="jupiterpersona_estadocivil">

                      <option>Soltero(a)</option>
                      <option>Comprometido(a)</option>
                      <option>Casado(a)</option>
                      <option>Divorciado(a)</option>
                      <option>Viudo(a)</option>

            </select></td>
        </tr>

             <%doc>    <tr>
                     <td>${_('Status')}</td><td>
                        <select name="jupiterpersona_estatus" id="jupiterpersona_estatus">
                            <option>ALTA</option>
                            <option>BAJA</option>

                        </select>

                 </td>
                 </tr></%doc>

         <tr>
            <td>*${_('Address: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address" maxlength="200" required><br></td>
        </tr>
             <tr>
            <td>${_('Address 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address2" maxlength="200" ><br></td>
        </tr>
                      <tr>
            <td>${_('Address 3: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_address3" maxlength="200"><br></td>
        </tr>

        <tr>
            <td>${_('Phone: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_phone" length="50" maxlength="25" ><br></td>
        </tr>
         <tr>
            <td>${_('Phone 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_phone2" length="50" maxlength="25"><br></td>
        </tr>
          <tr>
            <td>${_('Mobile Phone: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_mobilephone" length="50" maxlength="25" ><br></td>
        </tr>
           <tr>
            <td>${_('Mobile Phone 2: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_mobilephone2" length="50" maxlength="25"><br></td>
        </tr>
                 <tr>
            <td>${_('CURP: ')} &nbsp;</td><td><input type="text" name="jupiterpersona_curp" length="50" maxlength="18" style="text-transform: uppercase"><br></td>
        </tr>
                      <tr>
            <td>${_('INE: ')} &nbsp;</td><td><input type="file" name="jupiterpersona_ine" length="50" accept="application/pdf,image/vnd-wap-wbmp,image/png,image/jpeg" ><br></td>
        </tr>
                     <tr>
            <td>${_('Observations: ')} &nbsp;</td><td><textarea name="jupiterpersona_observaciones" maxlength="500" style="margin: 0px; width: 422px; height: 150px;"></textarea></td>
        </tr>

             </table>

          <table>
             <tr>
                     <td>${_('Operator: ')}</td><td><input type="checkbox" name="jupiterpersona_esoperador" id="jupiterpersona_esoperador" onclick="verseccionoperador();"></td>
                 </tr>
              </table>

                     <table id="jupiterpersona_divoperador" style="display: none;">
                         <tr>
                             <td>${_('Do you have a operator number?: ')}</td><td><select id="tienenumerooperadorq" name="tienenumerooperadorq" onchange="cuentaconnumerooperador();"><option value="no">${_('NO')}</option><option value="si">${_('YES')}</option></select></td>
                         </tr>
                         <tr>
                            <td><label id="labelnumerodeoperadorasignado" name="labelnumerodeoperadorasignado" style="display: none;">
                                *${_('Operator Number: ')}
                            </label></td>
                             <td><input type="number" id="numerodeoperadorasignado" name="numerodeoperadorasignado" style="display: none;"></td>
                         </tr>

         <tr>
            <td>${_('Number SS: ')} &nbsp;</td><td><input type="text" id="jupiteroperadores_noss" name="jupiteroperadores_noss" length="50" maxlength="50"><br></td>
        </tr>
         <tr>
            <td>*${_('File Licence: ')} &nbsp;</td><td><input type="file" id="jupiteroperadores_licencearchivo" name="jupiteroperadores_licencearchivo" length="50" accept="application/pdf,image/vnd-wap-wbmp,image/png,image/jpeg"><br></td>
        </tr>
                         <tr>
            <td>*${_('Licence: ')} &nbsp;</td><td><input type="text" id="jupiteroperadores_licence" name="jupiteroperadores_licence" length="50" maxlength="55"><br></td>
        </tr>
         <tr>
            <td>*${_('Due Date of Licence: ')} &nbsp;</td><td><input type="date" id="jupiteroperadores_duedate" name="jupiteroperadores_duedate" length="50" ><br></td>
        </tr>

                      <tr>
              <td>*${_('Empresa: ')} &nbsp;</td><td><select id="jupiteroperadores_linea" name="jupiteroperadores_linea" length="50" onchange="actualizandocargos();"><br>
                                                %for item in typelinea:
                                                    <option>${item.nombre_linea}</option>
                                                %endfor
                                            </select></td>
                    </tr>

                                    <tr>
              <td>${_('Group: ')} &nbsp;</td><!--<select id="jupiteroperadores_grupo" name="jupiteroperadores_grupo" length="50" onchange="actualizandocargos();"></select><br>
                                                <option>Sin Grupo</option>-->
                                             <input type="text" id="seriedegrupos" hidden>
                                                %for item in typegrupo:
                                                    <tr><td></td><td><input type="checkbox" name="${item.grupopersona}" value="${item.grupopersona}" onchange="insertandoeninputoculto(this);">${item.grupopersona}</td></tr>
                                                %endfor


                    </tr>
                      <tr>
              <td>*${_('Licence: ')} &nbsp;</td><td><select id="jupiteroperadores_licencia" name="jupiteroperadores_licencia" length="50" ><br>
                                                %for item in typelicence:
                                                    <option>${item.tipo_licencia}</option>
                                                %endfor
                                            </select></td>
                    </tr>


    </table>

          <table id="jupiter_tabladecargosenoperadores" style="display: none;">
              <legend id="legendcharges" style="display: none">${_('CHARGES: ')}</legend>

                 %for item in construir:
                     <tr>

                         <td><input type="text" id="jupitercargo${item['cargo']}" name="jupitercargo${item['cargo']}" value="${item['cargo']}" readonly></td><td>Monto: <input name="montode${item['cargo']}" id="montode${item['cargo']}" type="number" size="8" value="${item['monto']}"></td>

                    </tr>

                    %endfor
              <legend id="legendchargesclose" style="display: none"></legend>

        </table>
<br>
          <table>
               <tr>
                     <td>${_('Permisionario: ')}</td><td><input type="checkbox" name="jupiterpersona_espermisionario" id="jupiterpersona_espermisionario" onchange="ver_parteusuariopermi();"></td>
                 </tr>
          </table>

              <table id="trofuserpermi" style="display: none;">
                  <tr>
                      <td>${_('User: ')}</td><td><input type="text" id="jupiter_theuserpermi" name="jupiter_theuserpermi"></td>
              </tr>
              </table>

           <table>
                   <tr>
                     <td>${_('Beneficiary: ')}</td><td><input type="checkbox" name="jupiterpersona_esbeneficiario" id="jupiterpersona_esbeneficiario" onchange="versecciondebeneficiario();"></td>
                 </tr>

          </table>
          <table id="jupiter_datosbeneficiario" style="display: none;">
              <tr height="40px">
                  <td width="80px">Cuenta 1:</td><td><input type="text" id="jupiter_cuenta1" name="jupiter_cuenta1"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Banco 1:</td><td><input type="text" id="jupiter_banco1" name="jupiter_banco1"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Cuenta 2:</td><td><input type="text" id="jupiter_cuenta2" name="jupiter_cuenta2"></td>
              </tr>
              <tr height="40px">
                  <td width="80px">Banco 2:</td><td><input type="text" id="jupiter_banco2" name="jupiter_banco2"></td>
              </tr>
          </table>
           <table>
                   <tr>
                     <td>${_('Checker: ')}</td><td><input type="checkbox" name="jupiterpersona_eschecador" id="jupiterpersona_eschecador" onclick="verseccionchecador();"></td>
                 </tr>
          </table>
          <table id="jupiter_datoschecador" style="visibility: hidden">
          <tr>
          <td>${_('Base: ')}</td><td><select id="jupiterchecador_base" name="jupiterchecador_base">
                    %for item in allbase:
                                <option>${item.punto}</option>
                        %endfor
                    </select></td>
          </tr>
          </table>
          <br><br>
          <input type="submit" name="jupiter_submit_uploadoperator" value="GUARDAR"/>

        </form>
      <br>
      <iframe name="requestpersonas" id="requestpersonas" frameborder="0"></iframe>


  </fieldset>
</html>