<script>
    function personTypeJupiter(checkbox,id) {
        if (checkbox.checked == true) {
            document.getElementById(id).hidden = false;
        }else{
            document.getElementById(id).hidden = true;
        }
    }
    function operatorNumberJupiter() {
        var choice = document.getElementById("jupiterPeople_opnum").value;
        if (choice=="SI"){
            document.getElementById("jupiterPeople_numerooperador").hidden = false;
        }else{
            document.getElementById("jupiterPeople_numerooperador").hidden = true;
        }
    }
</script>
<form id="PeopleFormJupiter" action="${h.url()}/personas/save">
    <input hidden type="text" id="jupiterPeople_id" name="jupiterPeople_id" value='${id}'>
    <fieldset>
        <legend>${_('Personal Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    *${_('Name')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_nombre" name="jupiterPeople_nombre" value="${handler.nombre}" required>
                </td>
                <td>
                    ${_('Gender')}:
                </td>
                <td>
                    <select id="jupiterPeople_sexo" name="jupiterPeople_sexo"  required>
                        <option ${male}>MASCULINO</option>
                        <option ${female}>FEMENINO</option>
                    </select>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Nickname')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_apodo" name="jupiterPeople_apodo" value="${handler.apodo}">
                </td>
                <td>
                    *${_('Civil Status')}:
                </td>
                <td>
                    <select id="jupiterPeople_estadocivil" name="jupiterPeople_estadocivil" required>
                        <option ${single}>Soltero(a)</option>
                        <option ${married}>Casado(a)</option>
                        <option ${divorced}>Divorsado(a)</option>
                        <option ${widower}>Viudo(a)</option>
                    </select>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('RFC')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_rfc" name="jupiterPeople_rfc" value="${handler.rfc}">
                </td>
                <td>
                    ${_('CURP')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_curp" name="jupiterPeople_curp" value="${handler.curp}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    *${_('Cellphone')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_celular" name="jupiterPeople_celular" value="${handler.celular}" required>
                </td>
                <td>
                    ${_('Cellphone')} 2:
                </td>
                <td>
                    <input type="email" id="jupiterPeople_otro_celular" name="jupiterPeople_otro_celular" value="${handler.otro_celular}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Email')}:
                </td>
                <td>
                    <input type="email" id="jupiterPeople_correo" name="jupiterPeople_correo" value="${handler.correo}">
                </td>
                <td>
                    *${_('Birth Date')}:
                </td>
                <td>
                    <input type="date" id="jupiterPeople_fecha_nacimiento" name="jupiterPeople_fecha_nacimiento" value="${handler.fecha_nacimiento}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                 <td>
                    ${_('Foto')}:
                </td>
                <td>
                    <input type="file" name="jupiterPeople_foto" id="jupiterPeople_foto" accept="image/jpg,image/png,image/jpeg"/><label style="font-size: 8px">${_('Only')} (jpeg,jpg or png)</label>
                </td>
                <td>
                    ${_('Person Type')}:
                </td>
                <td >
                    <input type="checkbox" id="jupiterPeople_esoperador" class="jupiterPeople_es" name="jupiterPeople_esoperador" onchange="personTypeJupiter(this,'jupiterOperator_checker');" ${habilitadooperador}> ${_('Operator')}<br>
                    <input type="checkbox" id="jupiterPeople_eschecador" class="jupiterPeople_es" name="jupiterPeople_eschecador" onchange="personTypeJupiter(this,'jupiterPeople_checker');" ${habilitadochecador}> ${_('Checker')}<br>
                    <input type="checkbox" id="jupiterPeople_espermisionario" class="jupiterPeople_es" name="jupiterPeople_espermisionario" onchange="personTypeJupiter(this,'jupiterPermisionario_checker');" ${habilitadopermisionario}> ${_('Permisionario')}<br>
                    <input type="checkbox" id="jupiterPeople_esbeneficiario" class="jupiterPeople_es" name="jupiterPeople_esbeneficiario" onchange="personTypeJupiter(this,'jupiterBeneficiary_checker');" ${habilitadobeneficiario}> ${_('Beneficiary')}
                </td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterPeople_internal_id" name="jupiterPeople_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterPeople_owner" name="jupiterPeople_owner" value='${user}'>
    </fieldset><br>
    <fieldset>
        <legend>${_('Extra Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('INE')}:
                </td>
                <td>
                    <input type="file" name="jupiterPeople_ine" id="jupiterPeople_ine" accept="image/jpg,image/png,image/jpeg"/><label style="font-size: 8px">${_('Only')} (jpeg,jpg or png)</label>
                </td>
                <td>
                    ${_('Date of Admission')}:
                </td>
                <td>
                    <input type="date" id="jupiterPeople_fecha_ingreso" name="jupiterPeople_fecha_ingreso" value="${handler.fecha_ingreso}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Phone')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_telefono" name="jupiterPeople_telefono" value="${handler.telefono}">
                </td>
                <td>
                    ${_('Phone')} 2:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_telefono2" name="jupiterPeople_telefono2" value="${handler.telefono2}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Address')}:
                </td>
                <td>
                    <textarea rows="3" cols="20" id="jupiterPeople_direccion" name="jupiterPeople_direccion">${handler.direccion}</textarea>
                </td>
               <td>
                    ${_('Address')} 2:
                </td>
                <td>
                    <textarea rows="3" cols="20" id="jupiterPeople_direccion2" name="jupiterPeople_direccion2">${handler.direccion2}</textarea>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
               <td>
                    ${_('Address')} 3:
                </td>
                <td>
                    <textarea rows="3" cols="20" id="jupiterPeople_direccion3" name="jupiterPeople_direccion3">${handler.direccion3}</textarea>
                </td>
                <td>
                   ${_('Observations')}:
                </td>
                <td>
                    <textarea rows="5" cols="20" id="jupiterPeople_observaciones" name="jupiterPeople_observaciones">${handler.observaciones}</textarea>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
    </fieldset>
    <fieldset id="jupiterOperator_checker" ${habope}>
        <legend>${_('Operator Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Operator Number')}:
                </td>
                <td>
                    <select id="jupiterPeople_opnum" name="jupiterPeople_opnum" onchange="operatorNumberJupiter()" >
                        <option value="NO" ${opnun}>NO</option>
                        <option value="SI" ${opnus}>SI</option>
                    </select>
                    <input type="number" id="jupiterPeople_numerooperador" name="jupiterPeople_numerooperador" value="${handler.numerooperador}" ${opnush}>
                </td>
                <td>
                    ${_('Number SS')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_numero_segurosocial" name="jupiterPeople_numero_segurosocial" value="${handler.numero_segurosocial}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Licence Type')}:
                </td>
                <td>
                    <select id="jupiterPeople_id_tipolicencia" name="jupiterPeople_id_tipolicencia" >
                    %for item in licencetype:
                        <option ${item['checked']} value="${item['id']}">${item['tipo_licencia']}</option>
                    %endfor
                    </select>
                </td>
                <td>
                    ${_('Licence File')}:
                </td>
                <td>
                    <input type="file" name="jupiterPeople_licencia" id="jupiterPeople_licencia" accept="image/jpg,image/png,image/jpeg"/><label style="font-size: 8px">${_('Only')} (jpeg,jpg or png)</label>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Licence Number')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_numerolicencia" name="jupiterPeople_numerolicencia" value="${handler.numerolicencia}">
                </td>
               <td>
                    ${_('Licence Expiration')}:
                </td>
                <td>
                    <input type="date" id="jupiterPeople_fecha_vencimientolicencia" name="jupiterPeople_fecha_vencimientolicencia" value="${handler.fecha_vencimientolicencia}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Company')}:
                </td>
                <td>
                    <select id="jupiterPeople_id_linea" name="jupiterPeople_id_linea">
                    %for item in company:
                        <option ${item['checked']} value="${item['id']}">${item['nombre_linea']}</option>
                    %endfor
                    </select>
                </td>
                <td>
                    ${_('Group')}:
                </td>
                <td>
                    % for it in group:
                        <input type="checkbox" id="jupiterPeople_id_grupopersona" name="jupiterPeople_id_grupopersona" value="${it['id']}" ${it['checked']} >&nbsp;&nbsp;${it['grupopersona']}<br>
                    %endfor
                </td>
            </tr>
        </table>
    </fieldset><br>
    <fieldset id="jupiterPermisionario_checker" ${habchec}>
        <legend>${_('Permisionario Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('User')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_usuario" name="jupiterPeople_usuario" value="${handler.usuario}">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </fieldset><br>
    <fieldset id="jupiterBeneficiary_checker" ${habben}><br>
        <legend>${_('Beneficiary Information')} </legend>
        <table style="width:100%">
            <table style="width:100%">
            <tr>
                <td>
                    ${_('Bank Account')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_cta1" name="jupiterPeople_cta1" value="${handler.cta1}">
                </td>
                <td>
                    ${_('Bank')}:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_banco1" name="jupiterPeople_banco1" value="${handler.banco1}">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Bank Account')} 2:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_cta2" name="jupiterPeople_cta2" value="${handler.cta2}">
                </td>
                <td>
                    ${_('Bank')} 2:
                </td>
                <td>
                    <input type="text" id="jupiterPeople_banco2" name="jupiterPeople_banco2" value="${handler.banco2}">
                </td>
            </tr>
        </table>
        </table>
    </fieldset><br>
    <fieldset id="jupiterPeople_checker" ${habper}>
        <legend>${_('Checker Information')} </legend>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Check Point')}:
                </td>
                <td colspan="3">
                    <select id="jupiterPeople_id_punto" name="jupiterPeople_id_punto">
                    %for item in checkpoint:
                        <option ${item['checked']} value="${item['id']}">${item['punto']}</option>
                    %endfor
                    </select>
                </td>
            </tr>
        </table>
    </fieldset>
</form>