<table style="width: 100%">
    <tr>
        <th></th>
        <th>${_('FILE')}</th>
    </tr>
    <tr>
        <td>${_('PHOTO')}</td>
        <td><img src="${photoPeople}" width="100px" height="100px" style="max-width: 300px; max-height: 300px"/></td>
    </tr>
    <tr>
        <td>${_('LICENCE')}</td>
        <td><img src="${licencePeople}" width="100px" height="100px" style="max-width: 300px; max-height: 300px"/></td>
    </tr>
    <tr>
        <td>${_('INE')}</td>
        <td><img src="${inePeople}" width="100px" height="100px" style="max-width: 300px; max-height: 300px"/></td>
    </tr>
</table>