<table border="2" style="background-repeat: no-repeat;" >
    <tr height="200px" width="50px">
        <td background="${h.url()}/img/Credentials/Rojo_3.jpg" style="text-align:center;height: 25px; width: 380px;background-repeat: no-repeat;">
            <p style="font-size: 12px; margin-left: 30px;"><b>${empresa}</b></p>
            <img src="${photoPeople}" style="width: 120px; height: 120px;border-radius: 50%;margin-left: 30px;">
            <p style="margin-top: 0.6em; margin-bottom: 0em;margin-left: 30px;font-size: 12px;"><b>${persona.nombre}</b></p>
            <label style="text-align: left;font-size: 12px;margin-left: 30px;">
                <table style="font-size: 10px;">
                    <tr>
                        <td>No. Operador</td>
                        <td style="width: 20px"></td>
                        <td><i>${persona.numerooperador}</i></td>
                    </tr>
                    <tr>
                        <td>RFC</td>
                        <td style="width: 20px"></td>
                        <td><i>${persona.rfc}</i></td>
                    </tr>
                    <tr>
                        <td>Antiguedad</td>
                        <td style="width: 20px"></td>
                        <td>${persona.fecha_ingreso}</td>
                    </tr>
                    <%doc><tr>
                        <td>CURP</td>
                        <td style="width: 20px"></td>
                        <td>${persona.curp}</td>
                    </tr></%doc>
                    <tr>
                        <td>Licencia No.</td>
                        <td style="width: 20px"></td>
                        <td><i>${persona.numerolicencia}</i></td>
                    </tr>
                    <tr>
                        <td>Vigencia</td>
                        <td style="width: 20px"></td>
                        <td>${persona.fecha_vencimientolicencia}</td>
                    </tr>
                </table>
            </label>
        </td>
        <td background="${h.url()}/img/Credentials/Rojo_3.jpg" style="text-align: center; height: 25px; width: 380px;background-repeat: no-repeat;">
            <img src="${QRcode}" style="width: 150px; height: 150px;margin-left: 30px;"><br>
            <label style="text-align: center;font-size: 10px; margin-right: 5px;padding:5px;border:solid thin;border-radius: 2px;margin-left: 45px;">
                Esta credencial acredita como personal de la empresa exclusivamente a la persona cuyo nombre y fotografía y firma estan presentes en ella.
                Es responsabilidad de quien la porta el uso adecuado de la misma.
            </label>
        </td>
    </tr>
</table>