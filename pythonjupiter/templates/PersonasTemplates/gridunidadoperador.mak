<script type="text/javascript">


$(document).ready(function () {

		$("#jqGridunidadoperador").jqGrid({
		url: '${h.url()}/personas/loadgridunidadoperador?internal_id=${internal_id}',
		datatype: "json",
		 colModel: [
			{ label: 'Foto', name: 'foto', width: 17 ,formatter: jupiter_getimagefromgridoperadorunidad},
			{ label: 'Nombre', name: 'nombre', width: 90 },
			{ label: 'Unidad', name: 'unidad', width: 100 }
		],
		viewrecords: true, // show the current page, data rang and total records on the toolbar
		width: 1280,
		height: 500,
		rowNum: 5,
		loadonce: true, // this is just for the demo
		pager: "#jqGridPagerunidadoperador"
	});
});

function jupiter_getimagefromgridoperadorunidad(cellvalue) {
    var array=cellvalue.split(',');
    return '<img src="${h.url()}/img/${internal_id}/personas/'+array[0]+'.-/'+array[1]+'" style="width: 100px; height: 100px">'

  }

  function jupitersearchunidadoperador() {
    var por=document.getElementById("jupiter_porunidadoperador").value;
     var what=document.getElementById("jupiter_buscarporunidadoperador").value;
     var urlsearch="${h.url()}/personas/searchunidadoperador?internal_id=${internal_id}&por="+por+"&what="+what;
              $.ajax({
                   type: "GET",
                   url: urlsearch,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       jQuery("#jqGridunidadoperador").jqGrid("clearGridData", true).trigger("reloadGrid");
                  jQuery("#jqGridunidadoperador").jqGrid('setGridParam',{data:parameterdata['rows']}).trigger("reloadGrid");

                   },
                   error: function () {
                       alert("ERROR searchunidadoperador");
                   },
                   complete: function () {
                   }
               });
    }

    function jupiterreloadunidadoperador() {
        var url='${h.url()}/personas/loadgridunidadoperador?internal_id=${internal_id}';
        document.getElementById("jupiter_buscarporunidadoperador").value='';
             $.ajax({
                   type: "GET",
                   url: url,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       jQuery("#jqGridunidadoperador").jqGrid("clearGridData", true).trigger("reloadGrid");
                  jQuery("#jqGridunidadoperador").jqGrid('setGridParam',{data:parameterdata['rows']}).trigger("reloadGrid");

                   },
                   error: function () {
                       alert("ERROR loadgridunidadoperador");
                   },
                   complete: function () {
                   }
               });


      }

      function jupiterbuscarenterunidadoperador(e) {
             if (e.keyCode === 13 && !e.shiftKey) {
                 jupitersearchunidadoperador();
             }
        }

        function jupiter_opcionsinunidad() {
            var select=document.getElementById("jupiter_porunidadoperador").value;
            if(select=='Sin Unidad'){
                document.getElementById("jupiter_buscarporunidadoperador").style.visibility='hidden';

                var por=document.getElementById("jupiter_porunidadoperador").value;
                var urlsearch="${h.url()}/personas/searchunidadoperador?internal_id=${internal_id}&por="+por;
              $.ajax({
                   type: "GET",
                   url: urlsearch,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       jQuery("#jqGridunidadoperador").jqGrid("clearGridData", true).trigger("reloadGrid");
                  jQuery("#jqGridunidadoperador").jqGrid('setGridParam',{data:parameterdata['rows']}).trigger("reloadGrid");

                   },
                   error: function () {
                       alert("ERROR searchunidadoperador");
                   },
                   complete: function () {
                   }
               });

            }else{
                document.getElementById("jupiter_buscarporunidadoperador").style.visibility='visible';
            }
          }

 </script>
Buscar por: <select id="jupiter_porunidadoperador" onchange="jupiter_opcionsinunidad();"><option>Nombre</option><option>Unidad</option><option>Sin Unidad</option></select><input type="text" id="jupiter_buscarporunidadoperador" onkeypress="jupiterbuscarenterunidadoperador(event);">
<button onclick="jupitersearchunidadoperador();"><i class="fa fa-search"></i></button>
<button onclick="jupiterreloadunidadoperador();"><span class="glyphicon glyphicon-refresh"></span></button>

  <table id="jqGridunidadoperador"></table>
    <div id="jqGridPagerunidadoperador"></div>