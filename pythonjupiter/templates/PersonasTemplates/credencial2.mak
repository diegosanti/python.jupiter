<br>
<br>
<br>
<div align="center">
<table border="2" style="background-repeat: no-repeat;">
    <tr height="70px" width="300px">
        <td style="text-align: center; height: 60px; width: 420px; background-image: url(data:;base64,${elfondocredencial})">
             <h1><b>${empresa}</b></h1>

       %if persona.foto!=None:
           <img src="data:;base64,${personastring}" style="width: 120px; height: 120px">

        %else:
            <img src="data:;base64,${lasilueta}" style="width: 120px; height: 120px">
        %endif
           <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NUMERO OPERADOR:</b> <i>${persona.numerooperador}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NOMBRE:</b> <i>${persona.nombre}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>RFC:</b> <i>${persona.rfc}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>CURP:</b> <i>${persona.curp}</i></p>
             <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>FECHA DE NACIMIENTO:</b> <i>${persona.fecha_nacimiento}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>NUMERO DE LICENCIA:</b> <i>${persona.numerolicencia}</i></p>
            <p style="margin-top: 0.6em; margin-bottom: 0em;"><b>FECHA VENCIMIENTO LICENCIA:</b> <i>${persona.fecha_vencimientolicencia}</i></p>
        </td>
          <td style="text-align: center; height: 60px; width: 420px; background-image: url(data:;base64,${elfondocredencial})">
              <p>QR</p>
              <img src="data:;base64,${codigoqr}" style="width: 220px; height: 220px">
          </td>
    </tr>
</table>
</div>