<style>
    #table2 {
    border-collapse: collapse;
}
</style>

<p align="left"><b>Folio: ${queryabono.id_abonocuenta}</b></p>
<p align="left"><b>Recibo de Pago para la unidad <u>${queryunidad.eco1}</u> y el operador <u>${queryoperador.nombre}</u></b></p>

<br>

<p><b>PAGO DE UNIDAD</b></p>

<table id="table2" style="width: 50%;" border="1">
    <tr align="center">
       <td><b>FECHA</b></td>
        <td><b>UNIDAD</b></td>
        <td><b>OPERADOR</b></td>
        <td><b>SE PAGO</b></td>
    </tr>
%for itembody in listaqueryheadunidad:
    <tr bgcolor="#AAAAAA" style="text-align: center;" align="center">
        <td>${itembody['fecha']}</td>
        <td>${itembody['unidad']}</td>
        <td>${itembody['operador']}</td>
        <td>$${itembody['sepago']}.00</td>
    </tr>
%endfor
</table>
<hr>
<table>
    <tr>
        <td width="600px"></td><td><b>TOTAL: $${totalheadunidad}.00</b></td>
</tr>
</table>
<br>

<%doc><table id="table2" style="width: 50%;">
    <tr align="center">
        <td><b>FECHA</b></td>
        <td><b>CARGO</b></td>
        <td><b>PAGA</b></td>

    </tr>
%for itembody in listaquerybodyunidad:
    <tr align="center">
        <td>${itembody['fecha']}</td>
        <td>${itembody['cargo']}</td>
        <td>$${itembody['sepago']}.00</td>

    </tr>
%endfor
</table></%doc>

<br>


<p><b>PAGO DE OPERADOR</b></p>

<table id="table2" style="width: 50%;" border="1">
    <tr align="center">
       <td><b>FECHA</b></td>
        <td><b>UNIDAD</b></td>
        <td><b>OPERADOR</b></td>
        <td><b>SE PAGO</b></td>
    </tr>
%for itembody in listaqueryheadoperador:
    <tr bgcolor="#AAAAAA" style="text-align: center;" align="center">
        <td>${itembody['fecha']}</td>
        <td>${itembody['unidad']}</td>
        <td>${itembody['operador']}</td>
        <td>$${itembody['sepago']}.00</td>
    </tr>
%endfor
</table>
<hr>
<table>
    <tr>
        <td width="600px"></td><td><b>TOTAL: $${totalheadoperador}.00</b></td>
</tr>
</table>
<br>

<%doc><table id="table2" style="width: 50%;">
    <tr align="center">
        <td><b>FECHA</b></td>
        <td><b>CARGO</b></td>
        <td><b>PAGA</b></td>
    </tr>
%for itembody in listaquerybodyoperador:
    <tr align="center">
        <td>${itembody['fecha']}</td>
        <td>${itembody['cargo']}</td>
        <td>$${itembody['sepago']}.00</td>
    </tr>
%endfor
</table></%doc>
<br>
<br>
<br>
<br>
<br>
<br>

<p align="right"><b>Pago recibido por ${queryabono.usuario} el ${queryabono.fechaingreso}</b></p>
<br>
