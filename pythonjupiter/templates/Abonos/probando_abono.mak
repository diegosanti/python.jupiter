<style type="text/css">

    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
    .ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
    .ui-timepicker-rtl dl dt{ float: right; clear: right; }
    .ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

    /* Shortened version style */
    .ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time,
    .ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
    link { color: #0000EE; }

    .text{
        position:relative;
        left:190px;
        top:0px;
        width:290px;
        font-size:14px;
        color:black;

    }
    .imgContainer{
        float:left;
    }

    .azimuthimage {
        position: relative;
    }

    .azimuthimage .base {
       position: absolute;
       top: 0;
       left: 0;

       z-index : 0;
        width: 300px;
       height: 221px;
    }

    .azimuthimage .overlay{
       position: absolute;
       top: 55px;
       left:    81px;
       width: 105px;
       height: 105px;
    }

    div.dialog-hidden { display:none}
    div.redsquare    { border: solid 10px red; width: 67px; height: 10px; }
    div.orangesquare { border: solid 10px orange; width: 67px; height: 10px;}
    div.yellowsquare { border: solid 10px yellow; width: 67px; height: 10px;}
    div.greensquare  { border: solid 10px green; width: 67px; height: 10px; }
    div.bluesquare   { border: solid 10px blue; width: 67px; height: 10px; }
</style>
<script>


                          var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');
    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridAbono"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name = '#jupiter_jqGridAbono';
    var grid_pager= '#jupiter_PagerAbono';
    var update_url='${h.url()}/abono/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_url  ='${h.url()}/abono/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';

    var addParams = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_url,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    //var editParams = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParams = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url,modal: true};
    var grid = jQuery(grid_name);
            $(document).ready(function () {
                grid.jqGrid({
                url: load_url,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Cancel')}','${_('Capture Date')}','${_('Account Date')}','${_('Unit_ID')}','${_('Operator_ID')}',
                              '${_('Pay Unit')}','${_('Amount')}','${_('Faltante')}', '${_('Total')}','${_('Pay Operator')}','${_('Amount')}','${_('Faltante')}', '${_('Total')}'
                            ,'${_('Observations')}','${_('User')}','${_('Internal_id')}','${_('Application ID')}','${_('Recibo')}'],
                colModel: [
                    {name: 'id_abonocuenta',index: 'id_abonocuenta', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'cancelado',index: 'cancelado',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getimage,width: 40,editrules: {required: true}},
                    {name: 'fechaingreso', index: 'fechaingreso',align: 'left',hidden: false, formatter: 'date',width:80,editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'fechacuenta',index: 'fechacuenta', align: 'left',hidden: false,formatter: 'date',width:80,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidad_id',index: 'unidad_id', align: 'left',hidden: false,editable: true, edittype: 'text',formatter:getecounit,editrules: {required: false}},
                    {name: 'operador_id',index: 'operador_id',hidden:false,align: 'left',editable: true,edittype: 'date',formatter:getoperador,editrules: {required: true}},
                    {name: 'totalapagaruni',index: 'totalapagaruni',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'montoingresado',index: 'montoingresado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'faltante',index: 'faltante',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'total',index: 'total',hidden:true,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'totalapagarope',index: 'totalapagarope',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'montoingresadoope',index: 'montoingresadoope',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'faltanteope',index: 'faltanteope',hidden:false,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'totalope',index: 'totalope',hidden:true,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',editable: true,edittype: 'date',width: 35,editrules: {required: true}},
                    {name: 'observaciones',index: 'observaciones',hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true}},
                    {name: 'usuario',index: 'usuario',hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true}},
                    {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true}},
                    {name: 'application_id',index: 'application_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true}},
                    {name:'otro',index: 'otro',hidden: false,formatter:function (cellvalue, options, rowObject) {
                        return "<input type='button' value='Recibo' onclick='hirecibode(\""+rowObject+"\")' \>";
                    }}
                    ],
                pager: jQuery(grid_pager),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_abonocuenta',
                sortorder: "desc",
                viewrecords: true,
                autowidth: true,
                height: 250
               <%doc> ondblClickRow: function(rowId) {
                    doDoubleClickAbono(rowId)
                }</%doc>
                //caption: header_container,
            });
            grid.jqGrid('navGrid',grid_pager,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:false},
                            //editParams,
                            addParams,
                            //deleteParams,
                            searchParams,
                            viewParams);
            // add custom button
                %if perms['Add Pay']:
            grid.navButtonAdd(grid_pager,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add Pay')}",
                    caption: "${_('Add Pay')}",
                    position: "first",
                    onClickButton: doDoubleClickAbono
                    <%doc>onClickButton: function(){
                        doDoubleClickAbono(0)
                    }</%doc>
                });
                %endif


                 %if perms['Cancel Pay']:
                grid.navButtonAdd(grid_pager,
                {
                    buttonicon: "ui-icon-closethick",
                    title: "${_('Cancel Pay')}",
                    caption: "${_('Cancel Pay')}",
                    position: "second",
                   onClickButton: function(){
                        cancelaabono()
                    }
                });
                 %endif

            });
            $.extend($.jgrid.nav,{alerttop:1});


            function VentanadeCorreos(rowObject) {

                var winHeight = Math.round(window.innerHeight * .70);
                    var winWidth = Math.round(window.innerWidth * .70);

                    var addFilter02Buttons = {

                        "Send": function () {

                            var file_path = '${h.url()}'+document.getElementById("the_filenameprobandoabono").value;
                                   $("#jupiter_correosForm").validate({
                                rules: {
                                    correo1_jupiterabono: {
                                        required: true
                                    },
                                    correo2_jupiterabono:{
                                       required: true
                                    },


                                }
                            });

                                   if($("#jupiter_correosForm").valid()) {

                                       var correo1 = $('#correo1_jupiterabono').val();
                                       var correo2 = $('#correo2_jupiterabono').val();
                                       var correo3 = $('#correo3_jupiterabono').val();

                                       $.ajax({
                                           type: "GET",
                                           url: "${h.url()}/abono/sendemail?correo1="+correo1+"&correo2="+correo2+"&correo3="+correo3+"&file_path="+file_path+"&object="+rowObject,
                                           contentType: "application/json; charset=utf-8",
                                           data: {},
                                           success: function (parameterdata) {

                                               $.alert(parameterdata['error'], {autoClose: true, type: 'success',});
                                               Dialog02.dialog("close");
                                           },
                                           error: function () {
                                               $.alert('error en sendemail', {autoClose: false, type: 'danger',});
                                           },
                                           complete: function () {
                                           }
                                       });
                                   }

                        },
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogcorreos").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/abono/formulariocorreos?rowObject="+rowObject,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            $("#dialogcorreos").html(parameterdata.template);
                            Dialog02.data('rowId', 1);
                            Dialog02.dialog("open");


                        },
                        error: function () {
                            alert("ERROR VentanadeCorreos");
                        },
                        complete: function () {
                        }
                    });
              }

            function hirecibode(rowObject) {
                //alert(rowObject);
                var thesplit = rowObject.split(',');
                if (thesplit[1] == '0') {
                    var winHeight = Math.round(window.innerHeight * .85);
                    var winWidth = Math.round(window.innerWidth * .95);

                    var addFilter02Buttons = {
                          "Export to PDF": function () {

                              var file_path = '${h.url()}'+document.getElementById("the_filenameprobandoabono").value;
                              window.open(file_path);

                        },
                        <%doc>
                        "Send Email": function () {
                            VentanadeCorreos(thesplit);
                        },</%doc>
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogreciboabono").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/abono/htmlreciboabono" + "?rowObject=" + rowObject,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            $("#dialogreciboabono").html(parameterdata.template);
                            Dialog02.data('rowId', 1);
                            Dialog02.dialog("open");
                            document.getElementById("the_filenameprobandoabono").value=parameterdata['file_name'];

                        },
                        error: function () {
                            alert("ERROR htmlreciboabono");
                        },
                        complete: function () {
                        }
                    });

                }
            }


            function cancelaabono() {
                var rowid = $("#jupiter_jqGridAbono").jqGrid('getGridParam', 'selrow');
                if(rowid) {
                    var confirmando = confirm("¿Estas seguro de cancelar el abono " + rowid);
                    if (confirmando) {
                        $.ajax({

                            type: "GET",
                            url: '${h.url()}/abono/cancel?id=' + rowid + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                            async: false,
                            contentType: "application/json; charset=utf-8",
                            data: {},
                            success: function (parameterdata) {
                                //Insert HTML code

                                $('#jupiter_jqGridAbono').trigger('reloadGrid');
                            },
                            error: function () {
                                $.alert("Error accessing server cancelaabono", {type: "danger"});
                            },
                            complete: function () {

                            }

                        });
                    }
                }
            }


            function getimage(cellValue) {
                if(cellValue==0){
                    return "<img src=\"${h.url()}/img/check.png\" width=\"20px\" height=\"20px\"/>";
                }else{
                    return "<img src=\"${h.url()}/img/red-cross.png\"/ width=\"20px\" height=\"20px\">";
                }

              }


                       function getoperador(cellvalue) {
                var operador=cellvalue;
                $.ajax({

                    type: "GET",
                        url: '${h.url()}/abono/getoperador?operador_id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            operador=parameterdata['operador'];



                        },
                        error: function() {
                            $.alert("Error accessing server getoperador",{type: "danger"});
                        },
                        complete: function() {

                        }

                        });

                return operador;

              }




            function getecounit(cellvalue) {
                var eco2=cellvalue;
                $.ajax({

                    type: "GET",
                        url: '${h.url()}/abono/getunit?unidad_id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            eco2=parameterdata['eco'];



                        },
                        error: function() {
                            $.alert("Error accessing server getecounit",{type: "danger"});
                        },
                        complete: function() {

                        }

                        });

                return eco2;

              }

            function  doDoubleClickAbono(rowId) {

                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/abono/form?id='+rowId+'&application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#jupiter_dialogabono" ).html(parameterdata.dialogtemplate);
                            $("#jupiter_dialogabono").show();

                            $("#jupiter_AbonoForm").validate({
                                rules: {
                                    jupiter_abonosamount: {
                                        required: true,min: 0,max: 21450000
                                    },
                                    jupiter_abonosaccountdate:{
                                       required: true
                                    },
                                    jupiter_abonbospermisionario:{
                                       required: true
                                    },
                                    jupiter_abonosamountoperator:{
                                        required: true
                                    }
                                }
                            });
                        },
                        error: function() {
                            $.alert("Error accessing server /abono/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#jupiter_dialogabono" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.90),
                            width: Math.round(window.innerWidth*.90),
                            modal: true,
                            buttons: {
                                "${_('PAY')}": function() {
                                    if($("#jupiter_AbonandoForm").valid()) {

                                        var abono_id = $('#jupiter_abono_id').val();
                                        var datecuenta=$('#jupiter_abonosaccountdate').val();
                                        var empresa=$('#jupiter_abonbosempresa').val();
                                        var eco=$('#jupiter_abonosunidad').val();
                                        var permisionario=$('#jupiter_abonbospermisionario').val();
                                        var derrotero=$('#jupiter_abonbosderrotero').val();
                                        var grupo=$('#jupiter_abonbosgrupo').val();
                                        var operador = $('#jupiter_abonosoperador').val();
                                        var totalapagar=$('#jupiter_eltotalapagar').val();
                                        var monto = $('#jupiter_abonosamount').val();
                                        var montooperador=$('#jupiter_abonosamountoperator').val();
                                        var observaciones=$('#jupiter_abonosobservations').val();
                                        var internal_id=$('#jupiterabono_internal_id').val();
                                        var totaluni=document.getElementById("jupiter_faltanteuni").innerText;
                                        var totalope=document.getElementById("jupiter_faltanteope").innerText;

                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/abono/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'abono_id':abono_id,'datecuenta': datecuenta,'eco': eco,'operador': operador,
                                                    'totalapagar': totalapagar,'monto': monto,'observaciones': observaciones,'user': '${user}','internal_id': internal_id,
                                                    'permisionario': permisionario,'derrotero': derrotero,'grupo': grupo,'montooperador': montooperador,'totaluni':totaluni,
                                                    'totalope': totalope,'application_id': ${application_id}},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if(data['error']=='ok'){
                                                     ContDialog.dialog( "close" );
                                                      $('#jupiter_AbonandoForm')[0].reset();
                                                     $('#jupiter_jqGridAbono').trigger( 'reloadGrid' );
                                                }else{
                                                    if(data['error']=='nounidad'){
                                                        $.alert("${_('Error seleccione una unidad')} ", { autoClose:false,type: "warning"});
                                                    return true;
                                                    }else{
                                                        if(data['error']=='nooperador') {
                                                            $.alert("${_('Error seleccione un operador')} ", {
                                                                autoClose: false,
                                                                type: "warning"
                                                            });
                                                            return true;
                                                        }else{
                                                            $.alert("${_('Introduce un monto no mayor')} ", {
                                                                autoClose: false,
                                                                type: "warning"
                                                                   });
                                                            return true;
                                                        }
                                                    }
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /abono/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#jupiter_AbonandoForm')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#jupiter_AbonandoForm')[0].reset();
                            }
                        });

                    ContDialog.dialog( "open" );
            }

            function actualizaselectunidadabono(e) {
                      if (e.keyCode === 13 && !e.shiftKey) {
               var unidad = document.getElementById("jupiter_ecoenterabono").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectunidad?unidad="+unidad+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_ecoabono option').remove();
                        $('#jupiter_ecoabono').append('<option>-Seleccione-</option>');
                       for (var k in parameterdata['data']) {
                        if(k==0) {
                            $('#jupiter_ecoabono').append('<option selected>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }else{
                            $('#jupiter_ecoabono').append('<option>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectunidadabono");
                   },
                   complete: function () {
                   }
               });
           }
              }

              function actualizaselectoperadorabono(e) {
                          if (e.keyCode === 13 && !e.shiftKey) {
            var operador = document.getElementById("jupiter_operadorabonoenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectoperador"+"?operador="+operador+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_operadorabono option').remove();
                       $('#jupiter_operadorabono').append('<option>-Seleccione-</option>');
                       for (var k in parameterdata['data']) {
                           if(k==0) {
                               $('#jupiter_operadorabono').append('<option selected>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }else{
                               $('#jupiter_operadorabono').append('<option>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectoperadorbodyhead");
                   },
                   complete: function () {
                   }
               });
        }
                }


                function jupiter_mysearchabono() {
                 var date=document.getElementById("jupiter_fechaabono").value;
                 var datefinal=document.getElementById("jupiter_fechaabonofinal").value;
                 var eco=document.getElementById("jupiter_ecoabono").value;
                 var por=document.getElementById("jupiter_opcionoperadorounidadabono").value;
                 var operator=document.getElementById("jupiter_operadorabono").value;
                 var url="${h.url()}/abono/searchgrid/?date="+date+"&eco="+eco+
                         "&operator="+operator+"&por="+por+"&application_id=${application_id}&internal_id=${internal_id}&user=${user}&app_name=${app_name}&datefinal="+datefinal;
                      jQuery("#jupiter_jqGridAbono").setGridParam({url: url}).trigger("reloadGrid");
               }

               function jupiter_reloadabono(){
                  var url='${h.url()}/abono/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
                   document.getElementById("jupiter_fechaabono").value='${fecha}';
                  jQuery("#jupiter_jqGridAbono").setGridParam({url: url}).trigger("reloadGrid");
                }

                function verseccionesabono() {
                    var por=document.getElementById("jupiter_opcionoperadorounidadabono").value;
                if(por=='Unidad'){
                    document.getElementById("trdeunidadabono").style.display='block';
                    document.getElementById("trdeoperadorabono").style.display='none';
                }else{
                    document.getElementById("trdeunidadabono").style.display='none';
                    document.getElementById("trdeoperadorabono").style.display='block';

                }
                  }


                  function jupiter_mysearchabonopersonal() {
                   var date=document.getElementById("jupiter_fechaabono").value;
                   var datefinal=document.getElementById("jupiter_fechaabonofinal").value;
                      var url="${h.url()}/abono/searchgridpersonal/?date="+date+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&datefinal="+datefinal;
                      jQuery("#jupiter_jqGridAbono").setGridParam({url: url}).trigger("reloadGrid");
                    }

                    function jupiter_reloadabonopersonal() {
                   var url='${h.url()}/abono/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
                   document.getElementById("jupiter_fechaabono").value='${fecha}';
                  jQuery("#jupiter_jqGridAbono").setGridParam({url: url}).trigger("reloadGrid");
                      }

            </script>
</div>
    <!-- page start-->
<div id="jupiter_dialogabono"  title="${_('Pay')}"></div>
    <!-- JQGRID table start-->
<table>
    <tr style="height:30px";>
        <td>${_('Initial Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechaabono" name="jupiter_fechaabono"></td>
       <td>${_('Final Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechaabonofinal" name="jupiter_fechaabonofinal" ></td>
    </tr>
    %if perms['Search for Pay']:
       <tr>
        <td><select id="jupiter_opcionoperadorounidadabono" name="jupiter_opcionoperadorounidadabono" onchange="verseccionesabono();"><option>Unidad</option><option>Operador</option></select></td>
    </tr>
    </table>

           <table id="trdeunidadabono" style="display: block;">
     <tr style="height:30px;">
         <td><label id="jupiter_labelunidad" style="display: block;">${_('Eco: ')}</label></td><td><input type="text" id="jupiter_ecoenterabono" name="jupiter_ecoenterabono" onkeypress="actualizaselectunidadabono(event);" style="display: block;"></td><td><select id="jupiter_ecoabono" name="jupiter_ecoabono" style="display: block;">
                                            <option>-Seleccione-</option>
                                            %for item in unidades:
                                                    <option>${item.eco1}</option>
                                            %endfor
                            </select></td>
    </tr>
           </table>

           <table id="trdeoperadorabono" style="display: none;">
     <tr style="height:30px;">
         <td><label id="jupiter_labeloperator">${_('Operator: ')}</label></td><td><input type="text" id="jupiter_operadorabonoenter" name="jupiter_operadorabonoenter" onkeypress="actualizaselectoperadorabono(event);"></td><td><select id="jupiter_operadorabono" name="jupiter_operadorabono">
         <option>-Seleccione-</option>
         %for item in operadores:
         <option>${item.nombre}</option>
         %endfor
     </select></td>
   </tr>
           </table>
           <table>
    <tr style="height:30px";>
        <td></td><td><button onclick="jupiter_mysearchabono()"><span class="glyphicon glyphicon-search"></span></button>
            <button onclick="jupiter_reloadabono()"><span class="glyphicon glyphicon-refresh"></span></button></td>
    </tr>
           </table>
    %else:
        <tr style="height:30px";>
        <td></td><td><button onclick="jupiter_mysearchabonopersonal()"><span class="glyphicon glyphicon-search"></span></button>
            <button onclick="jupiter_reloadabonopersonal()"><span class="glyphicon glyphicon-refresh"></span></button></td>
    </tr>
        </table>
    %endif

    <table style="width:100%">
    <table id="jupiter_jqGridAbono" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="jupiter_PagerAbono" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
<div id="dialogreciboabono" title="Recibo"></div>
<div id="dialogcorreos" title="Correos"></div>
<input type="text" id="the_filenameprobandoabono" hidden>
  <!-- page end-->