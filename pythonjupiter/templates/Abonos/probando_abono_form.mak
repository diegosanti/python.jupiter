<script>
    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridinformacion"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_namepay = '#jupiter_jqGridinformacion';
    var grid_pagerpay= '#jupiter_Pagergridinfo';
    var update_urlpay='${h.url()}/bodyhead/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_urlpay  ='${h.url()}/bodyhead/load2?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';

    var addParamspay = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlpay,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamspay = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlpay,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamspay = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlpay,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamspay = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlpay,modal: true};
    var gridpay = jQuery(grid_namepay);
            $(document).ready(function () {
                gridpay.jqGrid({
                url: load_urlpay,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Date')}','${_('Status')}','${_('Unit')}','${_('Operator')}','${_('Amount')}','${_('Pagado')}','${_('Faltante')}','${_('Internal_id')}','${_('Application ID')}'],
                colModel: [
                    {name: 'id_head',index: 'id_head', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: true, edittype: 'text', editrules: {required: true},search:true},
                    {name: 'estatus',index: 'estatus', align: 'left',hidden: false,width:80,editable: true, edittype: 'text',editrules: {required: true},search:true},
                    {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getunidad2,editrules: {required: true},search:true},
                    {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getoperador2,editrules: {required: true},search:true},
                    {name: 'porpagar',index: 'porpagar',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'faltante',index: 'faltante',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true}
                    {name: 'application_id',index: 'application_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true}
                ],
                pager: jQuery(grid_pagerpay),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_head',
                sortorder: "desc",
                viewrecords: true,
                //autowidth: true,
                    width: 1500,
                height: 250,
                                footerrow: true,
                    userDataOnFooter: true,
             <%doc>       ondblClickRow: function(rowId) {
                    doDoubleClickAbono2(rowId)
                },</%doc>
                subGrid : true,
	            subGridRowExpanded: function(subgrid_id, row_id,rowObject) {
		            // we pass two parameters
		            // subgrid_id is a id of the div tag created whitin a table data
		            // the id of this elemenet is a combination of the "sg_" + id of the row
		            // the row_id is the id of the row
		            // If we wan to pass additinal parameters to the url we can use
		            // a method getRowData(row_id) - which returns associative array in type name-value
		            // here we can easy construct the flowing
		            var subgrid_table_id, pager_id;
		            subgrid_table_id = subgrid_id+"_t";
		            pager_id = "p_"+subgrid_table_id;
		            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
		            jQuery("#"+subgrid_table_id).jqGrid({
			        url:"${h.url()}/bodyhead/loadbody?id_head="+row_id,
                        editurl:"${h.url()}/bodyhead/prueba?id_head="+row_id+'&application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        datatype: 'json',
                    mtype: 'GET',

			        colNames: ['${_('ID')}','${_('Date')}','${_('Unit')}','${_('Charge Unit')}','${_('Operator')}','${_('Charge Operator')}','${_('id_head')}',
                            '${_('Amount')}','${_('Condonacion')}','${_('Saldo')}','${_('Pagado')}','${_('Internal_id')}'],
			        colModel: [
                        {name: 'id_body',index: 'id_body', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                        {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: false, edittype: 'text', editrules: {required: true},search:true},
                        {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: false,edittype: 'date',formatter: getunidad2,editrules: {required: true},search:true},
                        {name: 'id_cargounidad',index: 'id_cargounidad',hidden:false,align: 'left',editable: false,formatter: getcargounidad2,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: false,edittype: 'date',formatter: getoperador2,editrules: {required: true},search:true},
                        {name: 'id_cargopersona',index: 'id_cargopersona',hidden:false,align: 'left',editable: false,formatter: getcargopersona2,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_head',index: 'id_head',hidden:true,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'monto',index: 'monto',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,editable: false, edittype: 'text',editrules: {required: false},search:true},
                        {name: 'condonacion',index: 'condonacion',hidden:false,align: 'left',editable: true,edittype: 'text',editrules: {required: true},search:true},
                        {name: 'saldo',index: 'saldo',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},

			            ],
                        rowNum:20,
		   	            pager: pager_id,
                        sortname: 'id_body',
		                sortorder: "asc",
		                height: '100%',
                        onSelectRow: jupieditrow
		            });
		            var lastSelection;
		            function jupieditrow(id) {

                            if (id && id !== lastSelection) {
                                $('#'+subgrid_table_id).jqGrid('restoreRow',lastSelection);
                                $('#'+subgrid_table_id).jqGrid('editRow', id, {
                                    keys: true,
                                    onEnter: function (rowid, options, event) {
                                        if (confirm("Save the row with ID: " + rowid) === true) {
                                        }
                                    },
                                    successfunc: function (data) {

                                        jQuery("#jupiter_jqGridinformacion").trigger('reloadGrid');
                                        actualizalistacargos();
                                      }
                                });
                                lastSelection = id;

                            }

                        }
		            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,search:false})
	            },
	                subGridRowColapsed: function(subgrid_id, row_id) {
		                // this function is called before removing the data
		                //var subgrid_table_id;
		                //subgrid_table_id = subgrid_id+"_t";
		                //jQuery("#"+subgrid_table_id).remove();
	                }
                //caption: header_container,
            });
            gridpay.jqGrid('navGrid',grid_pagerpay,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:false},
                            editParamspay,
                            addParamspay,
                            //deleteParams,
                            searchParamspay,
                            viewParamspay);
            // add custom button

            });
            $.extend($.jgrid.nav,{alerttop:1});

/////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridinformacion2uni"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name = '#jupiter_jqGridinformacion2uni';
    var grid_pager= '#jupiter_Pagergridinfo2uni';
    var update_url='${h.url()}/bodyhead/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_url  ='${h.url()}/bodyhead/load3?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';

    var addParams = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_url,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParams = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParams = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url,modal: true};
    var grid = jQuery(grid_name);
            $(document).ready(function () {
                grid.jqGrid({
                url: load_url,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Date')}','${_('Status')}','${_('Unit')}','${_('Operator')}','${_('Amount')}','${_('Pagado')}','${_('Faltante')}','${_('Internal_id')}'],
                colModel: [
                    {name: 'id_head',index: 'id_head', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: true, edittype: 'text', editrules: {required: true},search:true},
                    {name: 'estatus',index: 'estatus', align: 'left',hidden: false,width:80,editable: true, edittype: 'text',editrules: {required: true},search:true},
                    {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getunidad2,editrules: {required: true},search:true},
                    {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getoperador2,editrules: {required: true},search:true},
                    {name: 'porpagar',index: 'porpagar',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'faltante',index: 'faltante',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,summaryType: "sum",editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true}


                ],
                pager: jQuery(grid_pager),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_head',
                sortorder: "desc",
                viewrecords: true,
                //autowidth: true,
                    width: 1500,
                height: 250,
         	    footerrow : true,
	            userDataOnFooter : true,

             <%doc>       ondblClickRow: function(rowId) {
                    doDoubleClickAbono2(rowId)
                },</%doc>
                subGrid : true,
	            subGridRowExpanded: function(subgrid_id, row_id,rowObject) {
		            // we pass two parameters
		            // subgrid_id is a id of the div tag created whitin a table data
		            // the id of this elemenet is a combination of the "sg_" + id of the row
		            // the row_id is the id of the row
		            // If we wan to pass additinal parameters to the url we can use
		            // a method getRowData(row_id) - which returns associative array in type name-value
		            // here we can easy construct the flowing
		            var subgrid_table_id, pager_id;
		            subgrid_table_id = subgrid_id+"_t";
		            pager_id = "p_"+subgrid_table_id;
		            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
		            jQuery("#"+subgrid_table_id).jqGrid({
			        url:"${h.url()}/bodyhead/loadbody?id_head="+row_id,
                         editurl:"${h.url()}/bodyhead/prueba?id_head="+row_id+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        datatype: 'json',
                    mtype: 'GET',
			        colNames: ['${_('ID')}','${_('Date')}','${_('Unit')}','${_('Charge Unit')}','${_('Operator')}','${_('Charge Operator')}','${_('id_head')}',
                            '${_('Amount')}','${_('Condonacion')}','${_('Saldo')}','${_('Pagado')}','${_('Internal_id')}'],
			        colModel: [
                        {name: 'id_body',index: 'id_body', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                        {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: false, edittype: 'text', editrules: {required: true},search:true},
                        {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: false,edittype: 'date',formatter: getunidad2,editrules: {required: true},search:true},
                        {name: 'id_cargounidad',index: 'id_cargounidad',hidden:false,align: 'left',editable: false,formatter: getcargounidad2,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: false,edittype: 'date',formatter: getoperador2,editrules: {required: true},search:true},
                        {name: 'id_cargopersona',index: 'id_cargopersona',hidden:false,align: 'left',editable: false,formatter: getcargopersona2,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_head',index: 'id_head',hidden:true,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'monto',index: 'monto',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,editable: false, edittype: 'text',editrules: {required: false},search:true},
                        {name: 'condonacion',index: 'condonacion',hidden:false,align: 'left',editable: true,edittype: 'text',editrules: {required: true},search:true},
                        {name: 'saldo',index: 'saldo',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: false,edittype: 'date',editrules: {required: true},search:true},

			            ],
                        rowNum:20,
		   	            pager: pager_id,
                        sortname: 'id_body',
		                sortorder: "asc",
		                height: '100%',
                        onSelectRow: jupieditrow
		            });
		            var lastSelection;
		            function jupieditrow(id) {

                            if (id && id !== lastSelection) {
                                $('#'+subgrid_table_id).jqGrid('editRow', id, {
                                    keys: true,
                                    onEnter: function (rowid, options, event) {
                                        if (confirm("Save the row with ID: " + rowid) === true) {
                                        }
                                    },
                                    successfunc: function (data) {
                                       jQuery("#jupiter_jqGridinformacion2uni").trigger( 'reloadGrid' );
                                       actualizalistacargos();
                                      }
                                });
                                lastSelection = id;

                            }
                        }
		            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,search:false})
	            },
	                subGridRowColapsed: function(subgrid_id, row_id) {
		                // this function is called before removing the data
		                //var subgrid_table_id;
		                //subgrid_table_id = subgrid_id+"_t";
		                //jQuery("#"+subgrid_table_id).remove();
                        var subgrid_table_id, pager_id;
		                subgrid_table_id = subgrid_id+"_t";
		                pager_id = "p_"+subgrid_table_id;


                    }
                //caption: header_container,
            });
            grid.jqGrid('navGrid',grid_pager,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:false},
                            editParams,
                            addParams,
                            //deleteParams,
                            searchParams,
                            viewParams);
            // add custom button

            });
            $.extend($.jgrid.nav,{alerttop:1});


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    function getunidad2(cellvalue,options,rowObject) {
                  var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getunidad?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['unidad'];

                        },
                        error: function () {
                            $.alert("Error accessing server getunidad", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                }



    function getoperador2(cellvalue,options,rowObject) {
                  var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getoperador?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['operador'];



                        },
                        error: function () {
                            $.alert("Error accessing server getoperador2", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                }

    function getcargounidad2(cellvalue,row_id) {
                                    var thereturn;
                                           var espoliza;

                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/espolizasiono?id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&row_id='+row_id.rowId,
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                espoliza = parameterdata['poliza'];



                        },
                        error: function () {
                            $.alert("Error accessing server espolizasiono", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });

                                 if(espoliza=='SI'){

                                    return 'POLIZA';
                                }
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getcargounidad?id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&row_id='+row_id.rowId,
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['cargounidad'];



                        },
                        error: function () {
                            $.alert("Error accessing server getcargounidad", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;


    }



    function getcargopersona2(cellvalue) {
        var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getcargopersona?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['cargopersona'];



                        },
                        error: function () {
                            $.alert("Error accessing server getcargopersona", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                  }



            function actualizaallselect() {
        var empresa=document.getElementById("jupiter_abonoslalinea").value;

                      $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaallselect" + "?internal_id=${internal_id}&user=${user}&app_name=${app_name}&empresa="+empresa,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                    $('#jupiter_abonoslaunidad option').remove();
                    $('#jupiter_abonosoperador option').remove();

                       for (var k in parameterdata['unidades']) {

                           $('#jupiter_abonoslaunidad').append('<option>'+parameterdata['unidades'][k]['eco']+'</option>');
                       }
                           for (var k in parameterdata['operadores']) {

                           $('#jupiter_abonosoperador').append('<option>'+parameterdata['operadores'][k]['nombre']+'</option>');
                       }
                    actualizatodopdgo();
                       actualizalistacargos();

                   },
                   error: function () {
                       alert("ERROR actualizaallselect");
                   },
                   complete: function () {
                   }
               });
      }

      function actualizaselectoperador(e) {
        if (e.keyCode === 13 && !e.shiftKey) {

            var operador = document.getElementById("jupiter_operadorenter").value;

               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectoperador" + "?operador=" + operador + "&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_abonosoperador option').remove();
                       for (var k in parameterdata['data']) {

                           $('#jupiter_abonosoperador').append('<option>'+parameterdata['data'][k]['nombre']+'</option>');
                       }
                       $('#jupiter_abonosunidad option').remove();
                        $('#jupiter_abonosunidad').append('<option>'+parameterdata['unidad']+'</option>');
                    actualizatodopdgo();
                      // actualizalistacargos();

                   },
                   error: function () {
                       alert("ERROR actualizaselectoperador");
                   },
                   complete: function () {
                   }
               });
        }

        }

         function actualizaselectunidad(e) {
        if (e.keyCode === 13 && !e.shiftKey) {

            var unidad = document.getElementById("jupiter_unidadenter").value;

               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectunidad" + "?unidad=" + unidad + "&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_abonosunidad option').remove();
                       for (var k in parameterdata['data']) {

                           $('#jupiter_abonosunidad').append('<option>'+parameterdata['data'][k]['eco1']+'</option>');
                       }

                    actualizatodopdgo();
                      // actualizalistacargos();

                   },
                   error: function () {
                       alert("ERROR actualizaselectoperador");
                   },
                   complete: function () {
                   }
               });
        }

        }


      function actualizatodopdgo(){
        var operador=document.getElementById("jupiter_abonosoperador").value;
        var unidad=document.getElementById("jupiter_abonosunidad").value;
                       $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizatodopdgo?operador="+operador+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}&unidad="+unidad,
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {

                        document.getElementById("jupiter_abonbospermisionario").value=parameterdata['permisionario'];
                        document.getElementById("jupiter_abonbosderrotero").value=parameterdata['derrotero'];
                        document.getElementById("jupiter_abonbosgrupo").value=parameterdata['grupo'];
                        document.getElementById("jupiter_abonbosempresa").value=parameterdata['linea'];
                        //document.getElementById("jupiter_unidadenter").value=parameterdata['unidad'];
                        actualizalistacargos();
                   },
                   error: function () {
                       alert("ERROR actualizatodopdgo");
                   },
                   complete: function () {
                   }
               });

      }

      function verdetalladocargos() {
        var innerhtml=document.getElementById("jupiter_flechita").innerHTML;
        if (innerhtml!='<span class="glyphicon glyphicon-arrow-up"></span>') {
            document.getElementById("divjqgrid1").style.display = 'block';
            document.getElementById("divjqgrid2").style.display = 'block';
            document.getElementById("jupiter_flechita").innerHTML = "<span class=\"glyphicon glyphicon-arrow-up\"></span>";
        }else{
            document.getElementById("divjqgrid1").style.display = 'none';
            document.getElementById("divjqgrid2").style.display = 'none';
            document.getElementById("jupiter_flechita").innerHTML = "<span class=\"glyphicon glyphicon-arrow-down\"></span>";

        }


        }

        function tipopormonto(e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                var total=parseInt(document.getElementById("jupiter_eltotalapagar").value);
                var monto=parseInt(document.getElementById("jupiter_abonosamount").value);

                if(total==monto){
                   // alert("Igual");
                     document.getElementById("jupiter_abonosamountcondonado").value=0;
                     document.getElementById("jupiter_abonoadelantomonto").value=0;
                    document.getElementById("jupiter_abonocondonacion").checked=false;
                    verpartecondonacion(document.getElementById("jupiter_abonocondonacion"));
                                    document.getElementById("jupiter_abonoadelanto").checked=false;
                            verparteadelanto(document.getElementById("jupiter_abonoadelanto"));


                }else{
                    if(total>monto){
                        //alert("Mayor Total");
                        document.getElementById("jupiter_abonosamountcondonado").value=total - monto;
                        document.getElementById("jupiter_abonoadelantomonto").value=0;
                        document.getElementById("jupiter_abonocondonacion").checked=true;
                        verpartecondonacion(document.getElementById("jupiter_abonocondonacion"));
                           document.getElementById("jupiter_abonoadelanto").checked=false;
                            verparteadelanto(document.getElementById("jupiter_abonoadelanto"));


                    }else{
                        if(total<monto){
                            //alert("Adelanto");
                            document.getElementById("jupiter_abonosamountcondonado").value=0;
                            document.getElementById("jupiter_abonoadelantomonto").value=monto - total;
                            document.getElementById("jupiter_abonocondonacion").checked=false;
                            verpartecondonacion(document.getElementById("jupiter_abonocondonacion"));
                            document.getElementById("jupiter_abonoadelanto").checked=true;
                            verparteadelanto(document.getElementById("jupiter_abonoadelanto"));
                        }
                    }
                }

            }
          }

          function verpartecondonacion(esto) {

          if(esto.id=='jupiter_abonocondonacion'){
              document.getElementById("jupiter_abonodeuda").checked=false;
          }else{
              document.getElementById("jupiter_abonocondonacion").checked=false;
          }
          if(esto.checked==true){
              document.getElementById("jupiter_abonosamountcondonado").style.display='block';
              document.getElementById("motivoperdonado").style.display='block';
              document.getElementById("labelmotivocondonado").style.display='block';
              document.getElementById("labelamountperdonado").style.display='block';
              document.getElementById("jupiter_abonoadelanto").checked=false;
              verparteadelanto(document.getElementById("jupiter_abonoadelanto"));

          }else{
              if(document.getElementById("jupiter_abonodeuda").checked==true || document.getElementById("jupiter_abonocondonacion").checked==true){

              }else {

                  document.getElementById("jupiter_abonosamountcondonado").style.display = 'none';
                  document.getElementById("motivoperdonado").style.display = 'none';
                  document.getElementById("labelmotivocondonado").style.display = 'none';
                  document.getElementById("labelamountperdonado").style.display = 'none';
              }
          }

            }


            function verparteadelanto(esto) {
          if(esto.checked==true){
              document.getElementById("jupiter_abonoadelantomonto").style.display='block';
              document.getElementById("jupiter_abonocondonacion").checked=false;
               document.getElementById("jupiter_abonodeuda").checked=false;
               verpartecondonacion(document.getElementById("jupiter_abonocondonacion"));

          }else{
              document.getElementById("jupiter_abonoadelantomonto").style.display='none';
          }


              }


              function actualizalistacargos() {
                  var unidad=document.getElementById("jupiter_abonosunidad").value;
                  var operador=document.getElementById("jupiter_abonosoperador").value;
                  var fecha=document.getElementById("jupiter_abonosaccountdate").value;

                  var url='${h.url()}/bodyhead/load2?appID=${application_id}&unidad='+unidad+'&operador='+operador+'&fecha='+fecha+'&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}';
                      $.ajax({
                          type: "GET",
                          url: "${h.url()}/abono/actualizalistacargos?fecha="+fecha+"&unidad="+unidad+"&operador="+operador+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                          contentType: "application/json; charset=utf-8",
                          data: {},
                          async:true,
                          success: function (parameterdata) {
                              var total = 0;
                              var totalpagado=0;
                              var totalfaltante=0;
                              var auxtotal=0;
                              $('#jupiter_detalladocargos td').remove();
                                $('#jupiter_detalladocargos th').remove();
                              $('#jupiter_detalladocargos').append('<tr><td><b>CARGOS UNIDAD</b></td></tr>');
                              $('#jupiter_detalladocargos').append('<tr><th class=\"col-md-3\">FECHA</th><th class=\"col-md-3\">ESTATUS</th><th class=\"col-md-3\">PAGA</th><th>PAGADO</th><th>FALTA</th></tr>');

                              for (var k in parameterdata['paraunidades']) {

                                  $('#jupiter_detalladocargos').append('<tr style=\"background-color: #d3d3d3;\"><td class=\"col-md-3\">'+parameterdata['paraunidades'][k].fecha+'</td><td class=\"col-md-3\">'+parameterdata['paraunidades'][k].estatus+'</td><td class=\"col-md-3\">'+parameterdata['paraunidades'][k].porpagar+'</td><td class=\"col-md-3\">'
                                          +parameterdata['paraunidades'][k].pagado+'</td><td class=\"col-md-3\">'+parameterdata['paraunidades'][k].faltante+'</td></tr>');

                                  //$('#jupiter_detalladocargos').append('<tr><td class=\"col-md-3\"></td><td class=\"col-md-3\">lafecha</td><td class=\"col-md-3\">charge</td><td class=\"col-md-3\">monto</td><td class=\"col-md-3\">condonacion</td><td class=\"col-md-3\">elsaldo</td></tr>');
                                  var bandera=0;
                                  for(var i=0 in parameterdata['body']) {
                                      if (parameterdata['body'][i]['fecha'] == parameterdata['paraunidades'][k].fecha) {
                                          if(bandera==0) {
                                              $('#jupiter_detalladocargos').append('<tr><th class=\"col-md-3\">Fecha</th><th class=\"col-md-3\">Cargo</th><th class=\"col-md-3\">Monto</th><th class=\"col-md-3\">Condonacion</th><th class=\"col-md-3\">Por Pagar</th></tr>');
                                                bandera=1;
                                          }
                                          $('#jupiter_detalladocargos').append('<tr><td class=\"col-md-3\">'+parameterdata['body'][i]['fecha']+'</td><td class=\"col-md-3\">'+parameterdata['body'][i]['cargo']+'</td>' +
                                                                                '<td class=\"col-md-3\">'+parameterdata['body'][i]['monto']+'</td><td class=\"col-md-3\">'+parameterdata['body'][i]['condonacion']+'</td>' +
                                                                                '<td class=\"col-md-3\">'+parameterdata['body'][i]['saldo']+'</td></tr>');
                                      }
                                  }
                                  auxtotal=auxtotal+parameterdata['paraunidades'][k].porpagar;
                                  total = total + parameterdata['paraunidades'][k].faltante;
                                  totalpagado=totalpagado+parameterdata['paraunidades'][k].pagado;
                                  totalfaltante=totalfaltante+parameterdata['paraunidades'][k].faltante
                              }
                              document.getElementById("eltotaldeunis").innerText='Total Unidad: $'+totalfaltante+'.00';
                                document.getElementById("eltotaldeunisblock").innerText='Total Unidad: $'+totalfaltante+'.00';
                               $('#jupiter_detalladocargos').append('<tr style=\"background-color: #d3d3d3;\"><td class=\"col-md-3\"></td><td class=\"col-md-3\"><b>TOTAL</b></td><td class=\"col-md-3\">'+auxtotal+'</td><td class=\"col-md-3\">'+totalpagado+'</td><td id=\"jupiter_faltanteuni\" class=\"col-md-3\">'+totalfaltante+'</td></tr>');
                              $('#jupiter_detalladocargos').append('<tr><td><b>CARGOS OPERADOR</b></td></tr>');
                              $('#jupiter_detalladocargos').append('<tr><th class=\"col-md-3\">FECHA</th><th class=\"col-md-3\">ESTATUS</th><th class=\"col-md-3\">PAGA</th><th>PAGADO</th><th>FALTA</th></tr>');
                                var total2=0;

                               totalpagado=0;
                               totalfaltante=0;
                              for (var k in parameterdata['paraoperadores']) {
                                  $('#jupiter_detalladocargos').append('<tr style=\"background-color: #d3d3d3;\"><td class=\"col-md-3\">'+parameterdata['paraoperadores'][k].fecha+'</td><td class=\"col-md-3\">'+parameterdata['paraoperadores'][k].estatus+'</td><td class=\"col-md-3\">'+parameterdata['paraoperadores'][k].porpagar+'</td><td class=\"col-md-3\">'
                                          +parameterdata['paraoperadores'][k].pagado+'</td><td class=\"col-md-3\">'+parameterdata['paraoperadores'][k].faltante+'</td></tr>');

                                  var bandera=0;
                                  for(var i=0 in parameterdata['bodyope']) {
                                      if (parameterdata['bodyope'][i]['fecha'] == parameterdata['paraoperadores'][k].fecha) {
                                          if(bandera==0) {
                                              $('#jupiter_detalladocargos').append('<tr><th class=\"col-md-3\">Fecha</th><th class=\"col-md-3\">Cargo</th><th class=\"col-md-3\">Monto</th><th class=\"col-md-3\">Condonacion</th><th class=\"col-md-3\">Por Pagar</th></tr>');
                                              bandera=1;
                                          }
                                          $('#jupiter_detalladocargos').append('<tr><td class=\"col-md-3\">'+parameterdata['bodyope'][i]['fecha']+'</td><td class=\"col-md-3\">'+parameterdata['bodyope'][i]['cargo']+'</td>' +
                                                                                '<td class=\"col-md-3\">'+parameterdata['bodyope'][i]['monto']+'</td><td class=\"col-md-3\">'+parameterdata['bodyope'][i]['condonacion']+'</td>' +
                                                                                '<td class=\"col-md-3\">'+parameterdata['bodyope'][i]['saldo']+'</td></tr>');
                                      }
                                  }

                                  total = total + parameterdata['paraoperadores'][k].faltante;
                                  total2=total2+parameterdata['paraoperadores'][k].porpagar;
                                  totalpagado=totalpagado+parameterdata['paraoperadores'][k].pagado;
                                  totalfaltante=totalfaltante+parameterdata['paraoperadores'][k].faltante
                              }
                              $('#jupiter_detalladocargos').append('<tr style=\"background-color: #d3d3d3;\"><td class=\"col-md-3\"></td><td class=\"col-md-3\"><b>TOTAL</b></td><td class=\"col-md-3\">'+total2+'</td><td class=\"col-md-3\">'+totalpagado+'</td><td id=\"jupiter_faltanteope\" class=\"col-md-3\">'+totalfaltante+'</td></tr>');
                              document.getElementById("jupiter_eltotalapagar").value = total;
                              document.getElementById("jupiter_eltotalapagarblock").value = '$'+total+'.00';
                              document.getElementById("eltotaldeoperadores").innerText='Total Operador: $'+totalfaltante+'.00';
                              document.getElementById("eltotaldeoperadoresblock").innerText='Total Operador: $'+totalfaltante+'.00';

                          },

                          error: function () {
                              alert("ERROR actualizalistacargos");
                          },
                          complete: function () {
                          }
                      });

                 var url='${h.url()}/bodyhead/load2?appID=${application_id}&unidad='+unidad+'&operador='+operador+'&fecha='+fecha+'&internal_id=${internal_id}&user=${user}&app_name=${app_name}';
                  var url2='${h.url()}/bodyhead/load3?appID=${application_id}&unidad='+unidad+'&operador='+operador+'&fecha='+fecha+'&internal_id=${internal_id}&user=${user}&app_name=${app_name}';
                       jQuery("#jupiter_jqGridinformacion").setGridParam({url: url}).trigger("reloadGrid");
                        jQuery("#jupiter_jqGridinformacion2uni").setGridParam({url: url2}).trigger("reloadGrid");

                }

                function jupiteractualizapreciosdepagooperador() {
                     var montoin=parseInt(document.getElementById("jupiter_abonosamountoperator").value);
                     var totalope=parseInt(document.getElementById("jupiter_faltanteope").innerText);
                     var totaltodo=parseInt(document.getElementById("jupiter_eltotalapagar").value);
                     var resultado=totalope - montoin;
                      document.getElementById("eltotaldeoperadoresblock").innerText='Total Operador: $'+resultado+'.00';
                  }

                  function jupiteractualizapreciosdepagounidad() {

                      var montoin=parseInt(document.getElementById("jupiter_abonosamount").value);
                      var totaluni=parseInt(document.getElementById("jupiter_faltanteuni").innerText);
                      var totaltodo=parseInt(document.getElementById("jupiter_eltotalapagar").value);
                      var resultado=totaluni - montoin;
                       document.getElementById("eltotaldeunisblock").innerText='Total Unidad: $'+resultado+'.00';

                    }

</script>

<form id="jupiter_AbonandoForm" name="jupiter_AbonandoForm" action="${h.url()}/abono/save">
    <input hidden type="text" id="jupiter_abono_id" value='0'>
    <fieldset>
        <legend>${_('PAY')} </legend>
        <table>
               <tr>
                <td>${_('Account Date')}</td>
                <td><input id="jupiter_abonosaccountdate" name="jupiter_abonosaccountdate" value="${yesterday}" type="date" length="50" required readonly></td>
                </tr>

            <tr>
                <td>${_('Operator')}</td>
                <td><input id="jupiter_operadorenter" name="jupiter_operadorenter" type="number" length="50" onkeypress="actualizaselectoperador(event);"></td>
                <td><select id="jupiter_abonosoperador" name="jupiter_abonosoperador" onchange="actualizatodopdgo();">
                  <option>-</option>
                    <%doc>%for item in operadores:
                        <option>${item.nombre}</option>
                    %endfor
                    </%doc>
                    </select></td>
            </tr>

            <tr>
                <td>${_('Unit: ')}</td>
                <td><input type="text" id="jupiter_unidadenter" name="jupiter_unidadenter" length="50" onkeypress="actualizaselectunidad(event);"></td>
                <td><select id="jupiter_abonosunidad" name="jupiter_abonosunidad" onchange="actualizatodopdgo();">
                  <option>-</option>
                    %for item in unidades:
                        <option>${item.eco1}</option>
                    %endfor
                    </select></td>

            </tr>

            <tr>
                <td>${_('Permisionario: ')}</td><td><input type="text" name="jupiter_abonbospermisionario" id="jupiter_abonbospermisionario" length="50" readonly></td>

            </tr>

            <tr>
                <td>${_('Company: ')}</td><td><input type="text" name="jupiter_abonbosempresa" id="jupiter_abonbosempresa" length="50" readonly></td>
            </tr>
            <tr>
                <td>${_('Derrotero: ')}</td><td><input type="text" name="jupiter_abonbosderrotero" id="jupiter_abonbosderrotero" length="50" readonly></td>

            </tr>
            <tr>
                <td>${_('Group: ')}</td><td><input type="text" name="jupiter_abonbosgrupo" id="jupiter_abonbosgrupo" length="50" readonly></td>

            </tr>

            <tr>
                <td >${_('Total')}</td><td><a id="jupiter_flechita" onclick="verdetalladocargos();"><span class="glyphicon glyphicon-arrow-down"></span></a></td><td><input type="number" name="jupiter_eltotalapagar" id="jupiter_eltotalapagar" readonly></td>
                <td><input type="text" name="jupiter_eltotalapagarblock" id="jupiter_eltotalapagarblock" readonly style="display: none;"></td>
            </tr>
        </table>
                <table>
                 <legend></legend>
            <tr>
                <td>${_('Amount for the Unit: ')} </td>
                <td><input id="jupiter_abonosamount" name="jupiter_abonosamount" type="number" onkeypress="jupiteractualizapreciosdepagounidad();"></td>
                 <td>${_('Amount for the Operator: ')} </td>
                <td><input id="jupiter_abonosamountoperator" name="jupiter_abonosamountoperator" type="number" onkeypress="jupiteractualizapreciosdepagooperador();"></td>
            </tr>
                <tr></tr>
                </table>

          <table id="jupiter_detalladocargos" style="display: none; width: 80%">
                <legend></legend>

            </table>
            <p align="left" id="eltotaldeunis"></p>
            <p align="left" id="eltotaldeunisblock" style="display: none;"></p>
            <div id="divjqgrid1" style="display: none">
            <table id="jupiter_jqGridinformacion2uni" class="scroll" cellpadding="0" cellspacing="0"></table>
            </div>
            <div id="jupiter_Pagergridinfo2uni" class="scroll" style="text-align:center;"></div>
            <p align="left" id="eltotaldeoperadores"></p>
            <p align="left" id="eltotaldeoperadoresblock" style="display: none;"></p>
            <div id="divjqgrid2" style="display: none">
            <table id="jupiter_jqGridinformacion" class="scroll" cellpadding="0" cellspacing="0"></table>
            </div>
            <div id="jupiter_Pagergridinfo" class="scroll" style="text-align:center;"></div>
        <br>
              <table>
            <tr>
                <td>${_('Observations')}</td>
                <td><textarea id="jupiter_abonosobservations" name="jupiter_abonosobservations"></textarea></td>
            </tr>

        </table>

        <input type="text" hidden id="jupiterabono_internal_id" name="jupiterabono_internal_id" value='${internal_id}'>

    </fieldset>
</form>



   <table id="jqGrid"></table>
    <div id="jqGridPager"></div>