<%inherit file="local:templates.master"/>
<style type="text/css">
</style>
<script type="text/javascript">
    function funciongenerar2() {
        $.ajax({
            type: "GET",
            url: '${h.url()}/bodyhead/generopdf?name=diegod',
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {

                var file_path = '${h.url()}/'+parameterdata['name'];
                var a = document.createElement('A');
                a.href = file_path;
                a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            },
            error: function () {
                $.alert("Error accessing server funciongenerar", {type: "danger"});
            },
            complete: function () {

            }

        });
      }
</script>

<div class="row">
  <div class="col-md-12">
    <div class="page-header">
      <h2>PDF Example</h2>
    </div>
  <div class="container" align="left">
      <a onclick="funciongenerar2();" >
    <img border="0" src="${tg.url('/img/download.png')}" alt="Press here to Download" width="104" height="142">
    </a>
  </div>
  </div>
</div>