<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
   <style type="text/css">
        a{
           color: black;
           text-decoration: none;
       }
   </style>
   <style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>

   <script type="text/javascript">



                                var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');




          function openWindowfunc(text,url,mifuncion){
            doAjax(url,'openTab');
            var $dialog = $('<div></div>')
               .html('<div id="openTab" style="margin:10px"></div>')
               .dialog({
                   autoOpen: false,
                   modal: true,
                   height:  580,
                   width: 820,
                   resizable: false,
                   draggable: true,
                   position : { my: "center", at: "top", of: window },
                   close: function(){
                       $(this).dialog('destroy').remove();
                   },
                   title: text,
                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                   buttons: {
                       "Close": function ()
                       {
                           $(this).dialog('destroy').remove();
                           mifuncion();
                       }
                   }
               });
        $dialog.dialog('open');
        }

       function reload${title}() {

           var title='${title}';
            document.getElementById("filter${title}").value="";
            document.getElementById("filter${title}").placeholder=title;
            var theurl='${kw['url']}';
                   var thepwd='${kw['pwd']}';
                   var theperm='${kw['perm']}';
                   var theindexkey='${kw['indexkey']}';
                    $.ajax({
                    type: "GET",
                    url: "${h.url()}/reloadwhat?thetitle=${title}&action=show&perm=${kw['perm']}&application_id=${kw['application_id']}&internal_id=${kw['internal_id']}&user=${user}&app_name=${app_name}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                        $('#table${title} td').remove();
                        $("#bookmark${title}").html(data['contador']+ " Registros");

                        for(var k in data['hd']){


                            $('#table${title} tbody').append('<tr>');
                             if(k%2==0) {
                                 $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\"openWindow(\'Edit\',\'' + theurl + '/' + thepwd + '?action=edit&act=edit&perm=' + theperm + '&op=edit&' + theindexkey + '=' + data['hd'][k][data['kw']['indexkey']] + '\',reload${title});\"><span class="glyphicon glyphicon-edit"></span></a></td>');

                             }else{
                                 $('#table${title} tbody').append('<td><a onclick=\"openWindow(\'Edit\',\'' + theurl + '/' + thepwd + '?action=edit&act=edit&perm=' + theperm + '&op=edit&' + theindexkey + '=' + data['hd'][k][data['kw']['indexkey']] + '\',reload${title});\"><span class="glyphicon glyphicon-edit"></span></a></td>');
                             }
                            for(var k2 in data['column']){

                                if(k%2!=0){
                               $('#table${title} tbody').append('<td><a onclick=\"openWindow(\'Edit'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=edit&act=edit&perm='+theperm+'&op=edit&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});\">' + data['hd'][k][data['column'][k2]['db']] + '</a></td>' );

                                }
                               else{
                                    $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\"openWindow(\'Edit'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=edit&act=edit&perm='+theperm+'&op=edit&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});\">' + data['hd'][k][data['column'][k2]['db']] + '</a></td>' );
                                }


                           }


                           if(k%2!=0){
                            $('#table${title} tbody').append('<td><a onclick=\" if(confirm(\'¿Estas seguro de eliminar este elemento?\')){openWindow(\'Delete'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=new&act=del&perm='+theperm+'&op=del&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});}\"><i class=\"fa fa-trash \"></i></a></td>');

                           }else{
                               $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\" if(confirm(\'¿Estas seguro de eliminar este elemento?\')){openWindow(\'Delete'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=new&act=del&perm='+theperm+'&op=del&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});}\"><i class=\"fa fa-trash \"></i></a></td>');
                           }
                            $('#table${title} tbody').append('</tr>');

                        }

                    },
                    error: function () {
                        $.alert("Error accessing reload", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });

         }

               function search${title}(){
           var name=document.getElementById("filter${title}").value;
                   var theurl='${kw['url']}';
                   var thepwd='${kw['pwd']}';
                   var theperm='${kw['perm']}';
                   var theindexkey='${kw['indexkey']}';

              $.ajax({
                    type: "GET",
                    url: "${h.url()}/searchwhat?filter="+name+"&thetitle=${title}&action=show&perm=${kw['perm']}&application_id=${kw['application_id']}&internal_id=${kw['internal_id']}&user=${user}&app_name=${app_name}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                        $('#table${title} td').remove();
                        $("#bookmark${title}").html(data['contador']+ " Registros");

                        for(var k in data['hd']){
                                $('#table${title} tbody').append('<tr>');
                                if(k%2==0) {
                                    $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\"openWindow(\'Edit\',\'' + theurl + '/' + thepwd + '?action=edit&act=edit&perm=' + theperm + '&op=edit&' + theindexkey + '=' + data['hd'][k][data['kw']['indexkey']] + '\',reload${title});\"><span class="glyphicon glyphicon-edit"></span> </a></td>');
                                }else{
                                     $('#table${title} tbody').append('<td><a onclick=\"openWindow(\'Edit\',\'' + theurl + '/' + thepwd + '?action=edit&act=edit&perm=' + theperm + '&op=edit&' + theindexkey + '=' + data['hd'][k][data['kw']['indexkey']] + '\',reload${title});\"><span class="glyphicon glyphicon-edit"></span> </a></td>');
                                }
                                for(var k2 in data['column']){

                                if(k%2!=0){
                                $('#table${title} tbody').append('<td><a onclick=\"openWindow(\'Edit'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=edit&act=edit&perm='+theperm+'&op=edit&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});\">' + data['hd'][k][data['column'][k2]['db']] + '</a></td>' );
                                }else{
                                   $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\"openWindow(\'Edit'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=edit&act=edit&perm='+theperm+'&op=edit&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});\">' + data['hd'][k][data['column'][k2]['db']] + '</a></td>' );
                                }

                            }
                              if(k%2!=0){
                            $('#table${title} tbody').append('<td><a onclick=\" if(confirm(\'¿Estas seguro de eliminar este elemento?\')){openWindow(\'Delete'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=new&act=del&perm='+theperm+'&op=del&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});}\"> <i class=\"fa fa-trash \"></i></a></td>');

                           }else{
                               $('#table${title} tbody').append('<td style=\"background-color: #dce4f9;\"><a onclick=\" if(confirm(\'¿Estas seguro de eliminar este elemento?\')){openWindow(\'Delete'+data['hd'][k][data['column'][k2]['db']]+ '\',\''+theurl+'/'+thepwd+'?action=new&act=del&perm='+theperm+'&op=del&'+theindexkey+'='+data['hd'][k][data['kw']['indexkey']]+'\',reload${title});}\"><i class=\"fa fa-trash \"></i></a></td>');
                           }
                             $('#table${title} tbody').append('</tr>');


                        }

                    },
                    error: function () {
                        $.alert("Error accessing search", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });


       }


       function searchenter${title}(e) {
            if (e.keyCode === 13 && !e.shiftKey)  {

        search${title}();

         }
         }
   </script>
<button style="float: left;"><a onclick="openWindow('New ${kw['title']}','${kw['url']}/${kw['pwd']}?action=edit&act=new&perm=${kw['perm']}&op=add',reload${title})"><span class="glyphicon glyphicon-plus">Nuevo</span></a></button>
                    <br><br>
                    <input type="text" id="filter${title}" onkeypress="searchenter${title}(event)" placeholder="${kw['title']}"><button onclick=search${title}()><i class="fa fa-search"></i></button><button onclick="reload${title}()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                   &nbsp;&nbsp;
   <a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: ${reload}" ></a>

<div id="bookmark${title}" class="bookmark" style="float: right;">${contador} Registros</div>

                    <br><br>
                    <table id="table${title}" class="userTable">
                        <tr>
                            <th></th>
                        % for it in column:
                            <th>${it['show']}</th>
                        %endfor
                            <th>Accion</th>
                        </tr>
                        % for it in hd:
                        <tr>
                            <td><a onclick="openWindow('Edit ','${kw['url']}/${kw['pwd']}?action=edit&act=edit&perm=${kw['perm']}&op=edit&${kw['indexkey']}=${it[kw['indexkey']]}',reload${title})"><span class="glyphicon glyphicon-edit"></span></a></td>
                             % for i in column:

                                <td><a onclick="openWindow('Edit ${it[i['db']]}','${kw['url']}/${kw['pwd']}?action=edit&act=edit&perm=${kw['perm']}&op=edit&${kw['indexkey']}=${it[kw['indexkey']]}',reload${title})">${it[i['db']]}</a></td>
                             %endfor
                                <td><a onclick="if(confirm('¿Estas seguro de eliminar este elemento?')){openWindow('Delete ','${kw['url']}/${kw['pwd']}?action=new&perm=${kw['perm']}&act=del&op=del&${kw['indexkey']}=${it[kw['indexkey']]}',reload${title})}"><i class="fa fa-trash "></i></a></td>
                        </tr>
                        %endfor
                    </table><br><br>



            <br>

<br>
