<style type="text/css">

    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
    .ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
    .ui-timepicker-rtl dl dt{ float: right; clear: right; }
    .ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

    /* Shortened version style */
    .ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time,
    .ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
    link { color: #0000EE; }

    .text{
        position:relative;
        left:190px;
        top:0px;
        width:290px;
        font-size:14px;
        color:black;

    }
    .imgContainer{
        float:left;
    }

    .azimuthimage {
        position: relative;
    }

    .azimuthimage .base {
       position: absolute;
       top: 0;
       left: 0;

       z-index : 0;
        width: 300px;
       height: 221px;
    }

    .azimuthimage .overlay{
       position: absolute;
       top: 55px;
       left:    81px;
       width: 105px;
       height: 105px;
    }

    div.dialog-hidden { display:none}
    div.redsquare    { border: solid 10px red; width: 67px; height: 10px; }
    div.orangesquare { border: solid 10px orange; width: 67px; height: 10px;}
    div.yellowsquare { border: solid 10px yellow; width: 67px; height: 10px;}
    div.greensquare  { border: solid 10px green; width: 67px; height: 10px; }
    div.bluesquare   { border: solid 10px blue; width: 67px; height: 10px; }
</style>


<script>
    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridcuentas"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name = '#jupiter_jqGridcuentas';
    var grid_pager= '#jupiter_Pagercuentas';
    var update_url='${h.url()}/cuenta/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&fecha=${fecha}';

    var grid = jQuery(grid_name);
            $(document).ready(function () {
                grid.jqGrid({
                url: "${h.url()}/cuenta/datajson?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&empresa=${appnames[0]['empresa']}&fecha=${fecha}&fechauno=${fechauno}",
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Name')}','${_('Total')}','${_('Condonacion')}'],
                colModel: [
                    {name: 'id_cuenta',index: 'id_cuenta', align: 'center',key:true,hidden: true,width:28},
                    {name: 'nombre', index: 'nombre',align: 'left',hidden: false,width:80},
                    {name: 'total',index: 'total',hidden:false,align: 'left'},
                    {name: 'condonacion',index: 'condonacion',hidden:false,align: 'left'}

                ],
                pager: jQuery(grid_pager),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_cuenta',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                    footerrow: true,
                gridComplete: function() {
                        var $grid = $('#jupiter_jqGridcuentas');
                        var colSum = $grid.jqGrid('getCol', 'total', false, 'sum');
                        var colSumcondo = $grid.jqGrid('getCol', 'condonacion', false, 'sum');
                        $grid.jqGrid('footerData', 'set', { 'nombre': 'TOTAL' });
                        $grid.jqGrid('footerData', 'set', { 'total': colSum ,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}});
                        $grid.jqGrid('footerData', 'set', { 'condonacion': colSumcondo ,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}});
                },
              ondblClickRow: function(rowId) {

                }
                //caption: header_container,
            });
            });
            $.extend($.jgrid.nav,{alerttop:1});


    function getsignoformato(cellValue) {
        return "$"+cellValue+".00";
      }


    function cambiodecontenidoaccounts() {
       var por= document.getElementById("jupiter_paraaccounts").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/bodyhead/cuentasactualizaselect?por="+por+"&internal_id=${internal_id}&app_name=${app_name}&application_id=${application_id}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                    $("#jupiter_contenidoaccounts option").remove();
                       for(var k in parameterdata['contenido']){
                     $("#jupiter_contenidoaccounts").append("<option>"+parameterdata['contenido'][k]['nombre']+"</option>");
                       }
                   },
                   error: function () {
                       $.alert("Error en cambiodecontenidoaccounts", {type: "danger"});
                   },
                   complete: function () {
                   }
               });
      }


    function buscarcuentade() {
        var por=document.getElementById("jupiter_paraaccounts").value;
        var conte=document.getElementById("jupiter_contenidoaccounts").value;
        var fechainicial=document.getElementById("jupiter_fechainiciocuentade").value;
        var fechafinal=document.getElementById("jupiter_fechafinalcuentade").value;
        var url="${h.url()}/cuenta/datajsondinamico?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&conte="+conte+"&por="+por+
                "&fechainicial="+fechainicial+"&fechafinal="+fechafinal;
        jQuery("#jupiter_jqGridcuentas").setGridParam({url: url}).trigger("reloadGrid");
        document.getElementById("dequienesaccount").value=conte;
      }
</script>



<table>
    <tr style="height:50px";>
        <td width="100px">${_('Initial Date: ')}</td><td><input type="date" value="${fechauno}" id="jupiter_fechainiciocuentade" name="jupiter_fechainiciocuentade"></td>
        <td width="100px">${_('Final Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechafinalcuentade" name="jupiter_fechafinalcuentade"></td>
    </tr>

</table>

${_('Search for: ')}<select id="jupiter_paraaccounts" name="jupiter_paraaccounts" onchange="cambiodecontenidoaccounts();">
                            <option>App_name</option>
                            <option>Empresa</option>
                            <option>Derrotero</option>
                            <option>Grupo</option>
                            <option>Unidad</option>
                            <option>Operador</option>
                            </select>

<select id="jupiter_contenidoaccounts" name="jupiter_contenidoaccounts" >
%for item in appnames:
<option>${item['empresa']}</option>
%endfor
</select>
<button onclick="buscarcuentade();"><span class="glyphicon glyphicon-search"></span></button>

<br>
<p align="center">Pagos de: <input type="text" id="dequienesaccount" value="${appnames[0]['empresa']}" readonly></p>


<div id="jupiter_dialogcuentas"  title="${_('Cuentas')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jupiter_jqGridcuentas" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="jupiter_Pagercuentas" class="scroll" style="text-align:center;"></div>
    </table>
    <br>