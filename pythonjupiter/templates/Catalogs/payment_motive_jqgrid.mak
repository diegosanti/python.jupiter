<script>
    $(window).on("resize", function () {
            var $gridPaymentMotive = $("#jqGridPaymentMotiveJupiter"),
                newWidth = $gridPaymentMotive.closest(".ui-jqgrid").parent().width();
                $gridPaymentMotive.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_namePaymentMotive = '#jqGridPaymentMotiveJupiter';
    var grid_pagerPaymentMotive = '#listPagerPaymentMotiveJupiter';
    var update_urlPaymentMotive='${h.url()}/payment_motive/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlPaymentMotive  ='${h.url()}/payment_motive/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsPaymentMotive = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlPaymentMotive,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsPaymentMotive = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlPaymentMotive,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsPaymentMotive = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPaymentMotive,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsPaymentMotive = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPaymentMotive,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsPaymentMotive = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlPaymentMotive,modal: true};
    var gridPaymentMotive = jQuery(grid_namePaymentMotive);
            $(document).ready(function () {
                gridPaymentMotive.jqGrid({
                url: load_urlPaymentMotive,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Motive')}', '${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_motivo',index: 'id_motivo', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'motivo', index: 'motivo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerPaymentMotive),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_motivo',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterPaymentMotive(rowId)
                }
                //caption: header_container,
            });
            gridPaymentMotive.jqGrid('navGrid',grid_pagerPaymentMotive,{edit:false,add:false,del:false, search:true},
                            editParamsPaymentMotive,
                            addParamsPaymentMotive,
                            deleteParamsPaymentMotive,
                            searchParamsPaymentMotive,
                            viewParamsPaymentMotive);
            // add custom button
            gridPaymentMotive.navButtonAdd(grid_pagerPaymentMotive,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterPaymentMotive(0)
                    }
                });
            gridPaymentMotive.navButtonAdd(grid_pagerPaymentMotive,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterPaymentMotive(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterPaymentMotive() {
                var rowid = $('#jqGridPaymentMotiveJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/payment_motive/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridPaymentMotiveJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /payment_motive/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterPaymentMotive(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/payment_motive/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogPaymentMotiveJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogPaymentMotiveJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /payment_motive/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogPaymentMotiveJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.3),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#PaymentMotiveFormJupiter").valid()) {
                                        var id = $('#jupiterPaymentMotive_id').val();
                                        var motivo = $('#jupiterPaymentMotive_motivo').val();
                                        var app_id = $('#jupiterPaymentMotive_app_id').val();
                                        var app_name = $('#jupiterPaymentMotive_app_name').val();
                                        var internal_id = $('#jupiterPaymentMotive_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/payment_motive/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'motivo':motivo,'app_id':app_id,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridPaymentMotiveJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /payment_motive/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#PaymentMotiveFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#PaymentMotiveFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#PaymentMotiveFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogPaymentMotiveJupiter"  title="${_('Payment Motive')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridPaymentMotiveJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerPaymentMotiveJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->