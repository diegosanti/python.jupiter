<script>
    $(window).on("resize", function () {
            var $gridCheckPoint = $("#jqGridCheckPointJupiter"),
                newWidth = $gridCheckPoint.closest(".ui-jqgrid").parent().width();
                $gridCheckPoint.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameCheckPoint = '#jqGridCheckPointJupiter';
    var grid_pagerCheckPoint = '#listPagerCheckPointJupiter';
    var update_urlCheckPoint='${h.url()}/check_point/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlCheckPoint  ='${h.url()}/check_point/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsCheckPoint = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlCheckPoint,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsCheckPoint = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlCheckPoint,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsCheckPoint = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlCheckPoint,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsCheckPoint = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlCheckPoint,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsCheckPoint = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlCheckPoint,modal: true};
    var gridCheckPoint = jQuery(grid_nameCheckPoint);
            $(document).ready(function () {
                gridCheckPoint.jqGrid({
                url: load_urlCheckPoint,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Check Point')}', '${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_punto',index: 'id_punto', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'punto', index: 'punto',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerCheckPoint),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_punto',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterCheckPoint(rowId)
                }
                //caption: header_container,
            });
            gridCheckPoint.jqGrid('navGrid',grid_pagerCheckPoint,{edit:false,add:false,del:false, search:true},
                            editParamsCheckPoint,
                            addParamsCheckPoint,
                            deleteParamsCheckPoint,
                            searchParamsCheckPoint,
                            viewParamsCheckPoint);
            // add custom button
            gridCheckPoint.navButtonAdd(grid_pagerCheckPoint,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterCheckPoint(0)
                    }
                });
            gridCheckPoint.navButtonAdd(grid_pagerCheckPoint,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterCheckPoint(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterCheckPoint() {
                var rowid = $('#jqGridCheckPointJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/check_point/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridCheckPointJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /check_point/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterCheckPoint(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/check_point/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogCheckPointJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogCheckPointJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /check_point/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogCheckPointJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.3),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#CheckPointFormJupiter").valid()) {
                                        var id = $('#jupiterCheckPoint_id').val();
                                        var punto = $('#jupiterCheckPoint_punto').val();
                                        var app_id = $('#jupiterCheckPoint_app_id').val();
                                        var app_name = $('#jupiterCheckPoint_app_name').val();
                                        var internal_id = $('#jupiterCheckPoint_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/check_point/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'punto':punto,'app_id':app_id,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridCheckPointJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /check_point/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#CheckPointFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#CheckPointFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#CheckPointFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogCheckPointJupiter"  title="${_('Payment Motive')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridCheckPointJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerCheckPointJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->