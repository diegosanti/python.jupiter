<script>
    $(window).on("resize", function () {
            var $gridLicence = $("#jqGridLicenceJupiter"),
                newWidth = $gridLicence.closest(".ui-jqgrid").parent().width();
                $gridLicence.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameLicence = '#jqGridLicenceJupiter';
    var grid_pagerLicence = '#listPagerLicenceJupiter';
    var update_urlLicence='${h.url()}/license/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlLicence  ='${h.url()}/license/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsLicence = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlLicence,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsLicence = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlLicence,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsLicence = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlLicence,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsLicence = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlLicence,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsLicence = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlLicence,modal: true};
    var gridLicence = jQuery(grid_nameLicence);
            $(document).ready(function () {
                gridLicence.jqGrid({
                url: load_urlLicence,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Licence Type')}', '${_('internal_id')}','${_('app_id')}','${_('app_name')}','${_('Description')}'],
                colModel: [
                    {name: 'id_tipolicencia',index: 'id_tipolicencia', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'tipo_licencia', index: 'tipo_licencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'descripcion',index: 'descripcion', align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerLicence),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_tipolicencia',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterLicence(rowId)
                }
                //caption: header_container,
            });
            gridLicence.jqGrid('navGrid',grid_pagerLicence,{edit:false,add:false,del:false, search:true},
                            editParamsLicence,
                            addParamsLicence,
                            deleteParamsLicence,
                            searchParamsLicence,
                            viewParamsLicence);
            // add custom button
            gridLicence.navButtonAdd(grid_pagerLicence,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterLicence(0)
                    }
                });
            gridLicence.navButtonAdd(grid_pagerLicence,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterLicence(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterLicence() {
                var rowid = $('#jqGridLicenceJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/license/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridLicenceJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /license/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterLicence(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/license/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogLicenceJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogLicenceJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /license/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogLicenceJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.4),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#LicenceFormJupiter").valid()) {
                                        var id = $('#jupiterLicence_id').val();
                                        var tipo_licencia = $('#jupiterLicence_tipo_licencia').val();
                                        var descripcion = $('#jupiterLicence_description').val();
                                        var app_id = $('#jupiterLicence_app_id').val();
                                        var app_name = $('#jupiterLicence_app_name').val();
                                        var internal_id = $('#jupiterLicence_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/license/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'tipo_licencia':tipo_licencia,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridLicenceJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /license/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#LicenceFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#LicenceFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#LicenceFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogLicenceJupiter"  title="${_('Licence Type')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridLicenceJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerLicenceJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->