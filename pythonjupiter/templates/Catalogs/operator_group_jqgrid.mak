<script>
    $(window).on("resize", function () {
            var $gridOperatorGroup = $("#jqGridOperatorGroupJupiter"),
                newWidth = $gridOperatorGroup.closest(".ui-jqgrid").parent().width();
                $gridOperatorGroup.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameOperatorGroup = '#jqGridOperatorGroupJupiter';
    var grid_pagerOperatorGroup = '#listPagerOperatorGroupJupiter';
    var update_urlOperatorGroup='${h.url()}/operator_group/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlOperatorGroup  ='${h.url()}/operator_group/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsOperatorGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlOperatorGroup,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsOperatorGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlOperatorGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsOperatorGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOperatorGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsOperatorGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOperatorGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsOperatorGroup = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlOperatorGroup,modal: true};
    var gridOperatorGroup = jQuery(grid_nameOperatorGroup);
            $(document).ready(function () {
                gridOperatorGroup.jqGrid({
                url: load_urlOperatorGroup,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Group')}','${_('Description')}','${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_grupopersona',index: 'id_grupopersona', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'grupopersona', index: 'grupopersona',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'descripcion', index: 'descripcion',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerOperatorGroup),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_grupopersona',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterOperatorGroup(rowId)
                }
                //caption: header_container,
            });
            gridOperatorGroup.jqGrid('navGrid',grid_pagerOperatorGroup,{edit:false,add:false,del:false, search:true},
                            editParamsOperatorGroup,
                            addParamsOperatorGroup,
                            deleteParamsOperatorGroup,
                            searchParamsOperatorGroup,
                            viewParamsOperatorGroup);
            // add custom button
            gridOperatorGroup.navButtonAdd(grid_pagerOperatorGroup,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterOperatorGroup(0)
                    }
                });
            gridOperatorGroup.navButtonAdd(grid_pagerOperatorGroup,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterOperatorGroup(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterOperatorGroup() {
                var rowid = $('#jqGridOperatorGroupJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/operator_group/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridOperatorGroupJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /operator_group/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterOperatorGroup(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/operator_group/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogOperatorGroupJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogOperatorGroupJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /operator_group/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogOperatorGroupJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.4),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#OperatorGroupFormJupiter").valid()) {
                                        var id = $('#jupiterOperatorGroup_id').val();
                                        var grupopersona = $('#jupiterOperatorGroup_grupopersona').val();
                                        var descripcion = $('#jupiterOperatorGroup_description').val();
                                        var app_id = $('#jupiterOperatorGroup_app_id').val();
                                        var app_name = $('#jupiterOperatorGroup_app_name').val();
                                        var internal_id = $('#jupiterOperatorGroup_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/operator_group/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'grupopersona':grupopersona,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridOperatorGroupJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /operator_group/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#OperatorGroupFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#OperatorGroupFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#OperatorGroupFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogOperatorGroupJupiter"  title="${_('Operator Group')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridOperatorGroupJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerOperatorGroupJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->