<script>
    $(window).on("resize", function () {
            var $gridHistoric = $("#jqGridHistoricJupiter"),
                newWidth = $gridHistoric.closest(".ui-jqgrid").parent().width();
                $gridHistoric.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameHistoric = '#jqGridHistoricJupiter';
    var grid_pagerHistoric = '#listPagerHistoricJupiter';
    var update_urlHistoric='${h.url()}/${loadfrom}/loadHistoric?internal_id=${internal_id}&sobrecampo=${sobrecampo}';
    var load_urlHistoric  ='${h.url()}/${loadfrom}/loadHistoric?internal_id=${internal_id}&sobrecampo=${sobrecampo}';

    var addParamsHistoric = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlHistoric,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsHistoric = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlHistoric,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsHistoric = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlHistoric,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsHistoric = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlHistoric,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsHistoric = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlHistoric,modal: true};
    var gridHistoric = jQuery(grid_nameHistoric);

            $(document).ready(function () {
                gridHistoric.jqGrid({
                url: load_urlHistoric,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Date')}', '${_('Field')}','${_('New Value')}','${_('Old Value')}','${_('Field')}','${_('User')}'],
                colModel: [
                    {name: '${idreal}',index: '${idreal}', align: 'center',key:true,hidden: true, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'fecha', index: 'fecha',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'dato', index: 'dato',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'valor_nuevo', index: 'valor_nuevo',align: 'center',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'valor_anterior',index: 'valor_anterior', align: 'center',hidden: false,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'sobrecampo',index: 'sobrecampo', align: 'center',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'usuario',index: 'usuario', align: 'center',hidden: false,editable: true, edittype: 'text',editrules: {required: true}}
                ],
                pager: jQuery(grid_pagerHistoric),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: '${idreal}',
                sortorder: "dec",
                viewrecords: true,
                autowidth: true,
                height: 250
            });
            gridHistoric.jqGrid('navGrid',grid_pagerHistoric,{edit:false,add:false,del:false, search:true},
                            editParamsHistoric,
                            addParamsHistoric,
                            deleteParamsHistoric,
                            searchParamsHistoric,
                            viewParamsHistoric);
            });

</script>
</div>
    <!-- page start-->
<div id="dialogHistoricJupiter"  title="${_('Historic')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridHistoricJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerHistoricJupiter" class="scroll" style="text-align:center;"></div>
    </table>
