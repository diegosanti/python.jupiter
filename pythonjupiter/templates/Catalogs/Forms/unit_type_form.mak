<form id="UnitTypeFormJupiter">
    <input hidden type="text" id="jupiterUnitType_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Name')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterUnitType_tipo_unidad" name="jupiterUnitType_tipo_unidad" maxlength="50" required value='${handler.tipo_unidad}'>
                </td>
                <tr style="height: 10px">
                    <td></td>
                </tr>
                <td>
                    ${_('Description')}:
                </td>
                <td colspan="5">
                     <textarea rows="3" id="jupiterUnitType_description" maxlength="150" name="jupiterUnitType_description" required cols="20">${handler.descripcion}</textarea>
                </td>
                <tr style="height: 10px">
                    <td></td>
                </tr>
            </tr>
        </table>
        <input type="text" hidden id="jupiterUnitType_internal_id" name="jupiterUnitType_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterUnitType_app_id" name="jupiterUnitType_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterUnitType_app_name" name="jupiterUnitType_app_name" value='${app_name}'>
</form>