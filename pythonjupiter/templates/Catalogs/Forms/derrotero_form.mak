<form id="DerroteroFormJupiter">
    <input hidden type="text" id="jupiterDerrotero_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Name')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterDerrotero_derrotero" name="jupiterDerrotero_derrotero" maxlength="50" required value='${handler.derrotero}'>
                </td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterDerrotero_internal_id" name="jupiterDerrotero_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterDerrotero_app_id" name="jupiterDerrotero_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterDerrotero_app_name" name="jupiterDerrotero_app_name" value='${app_name}'>
</form>