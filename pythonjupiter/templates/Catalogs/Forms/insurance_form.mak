<form id="InsuranceFormJupiter">
    <input hidden type="text" id="jupiterInsurance_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Insurance Agency')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_aseguradora" name="jupiterInsurance_derrotero" maxlength="50" required value='${handler.aseguradora}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Contact')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_contacto" name="jupiterInsurance_contacto" maxlength="50" required value='${handler.contacto}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Secundary Contact')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_contacto2" name="jupiterInsurance_contacto2" maxlength="50" required value='${handler.contacto2}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Insurance Phone')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_telefono" name="jupiterInsurance_telefono" maxlength="50" required value='${handler.telefono}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Mobile Phone')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_celular" name="jupiterInsurance_celular" maxlength="50" required value='${handler.celular}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Emergency Phone')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterInsurance_telefonoemergencia" name="jupiterInsurance_telefonoemergencia" maxlength="50" required value='${handler.telefonoemergencia}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Address')}:
                </td>
                <td colspan="5">
                    <textarea rows="3" id="jupiterInsurance_direccion" maxlength="150" name="jupiterInsurance_direccion" cols="20">${handler.direccion}</textarea>
                </td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterInsurance_internal_id" name="jupiterInsurance_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterInsurance_app_id" name="jupiterInsurance_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterInsurance_app_name" name="jupiterInsurance_app_name" value='${app_name}'>
</form>