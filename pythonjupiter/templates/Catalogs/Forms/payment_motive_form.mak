<form id="PaymentMotiveFormJupiter">
    <input hidden type="text" id="jupiterPaymentMotive_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Motive')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterPaymentMotive_motivo" name="jupiterPaymentMotive_motivo" maxlength="50" required value='${handler.motivo}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterPaymentMotive_internal_id" name="jupiterPaymentMotive_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterPaymentMotive_app_id" name="jupiterPaymentMotive_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterPaymentMotive_app_name" name="jupiterPaymentMotive_app_name" value='${app_name}'>
</form>