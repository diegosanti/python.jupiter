<form id="CheckPointFormJupiter">
    <input hidden type="text" id="jupiterCheckPoint_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Check Point')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterCheckPoint_punto" name="jupiterCheckPoint_punto" maxlength="50" required value='${handler.punto}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterCheckPoint_internal_id" name="jupiterCheckPoint_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterCheckPoint_app_id" name="jupiterCheckPoint_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterCheckPoint_app_name" name="jupiterCheckPoint_app_name" value='${app_name}'>
</form>