<form id="ExternBeneficiaryFormJupiter">
    <input hidden type="text" id="jupiterExternBeneficiary_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Beneficiary')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_beneficiariomoral" name="jupiterExternBeneficiary_beneficiariomoral" maxlength="50" required value='${handler.beneficiariomoral}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <td>
                    ${_('Address')}:
            </td>
            <td colspan="5">
                <textarea rows="3" id="jupiterExternBeneficiary_domicilio" maxlength="150" name="jupiterExternBeneficiary_domicilio" cols="20">${handler.domicilio}</textarea>
            </td>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Phone')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_telefono" name="jupiterExternBeneficiary_telefono" maxlength="50" value='${handler.telefono}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Legal Representative')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_representante_legal" name="jupiterExternBeneficiary_representante_legal" maxlength="50" value='${handler.representante_legal}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('RFC')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_rfc" name="jupiterExternBeneficiary_rfc" maxlength="50" value='${handler.rfc}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Email')}:
                </td>
                <td colspan="5">
                    <input type="email" id="jupiterExternBeneficiary_email" name="jupiterExternBeneficiary_email" maxlength="50" value='${handler.email}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Bank')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_banco" name="jupiterExternBeneficiary_banco" maxlength="50" value='${handler.banco}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Bank Account')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_cta_bancaria" name="jupiterExternBeneficiary_cta_bancaria" maxlength="50" value='${handler.cta_bancaria}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Bank')} 2:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_banco2" name="jupiterExternBeneficiary_banco2" maxlength="50"  value='${handler.banco2}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Bank Account')} 2:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterExternBeneficiary_cta_bancaria2" name="jupiterExternBeneficiary_cta_bancaria2" maxlength="50"  value='${handler.cta_bancaria2}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterExternBeneficiary_internal_id" name="jupiterExternBeneficiary_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterExternBeneficiary_app_id" name="jupiterExternBeneficiary_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterExternBeneficiary_app_name" name="jupiterExternBeneficiary_app_name" value='${app_name}'>
</form>