<form id="OperatorGroupFormJupiter">
    <input hidden type="text" id="jupiterOperatorGroup_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Group')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterOperatorGroup_grupopersona" name="jupiterOperatorGroup_grupopersona" maxlength="50" required value='${handler.grupopersona}'>
                </td>
            </tr>
            <tr style="height: 10px">
                    <td></td>
            </tr>
            <td>
                    ${_('Description')}:
            </td>
            <td colspan="5">
                <textarea rows="3" id="jupiterOperatorGroup_description" maxlength="150" name="jupiterOperatorGroup_description" required cols="20">${handler.descripcion}</textarea>
            </td>
        </table>
        <input type="text" hidden id="jupiterOperatorGroup_internal_id" name="jupiterOperatorGroup_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterOperatorGroup_app_id" name="jupiterOperatorGroup_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterOperatorGroup_app_name" name="jupiterOperatorGroup_app_name" value='${app_name}'>
</form>