<form id="LicenceFormJupiter">
    <input hidden type="text" id="jupiterLicence_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Licence Type')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterLicence_tipo_licencia" name="jupiterLicence_tipo_licencia" maxlength="50" required value='${handler.tipo_licencia}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td>
                    ${_('Description')}:
                </td>
                <td colspan="5">
                    <textarea rows="3" id="jupiterLicence_description" maxlength="150" name="jupiterLicence_description" cols="20">${handler.descripcion}</textarea>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterLicence_internal_id" name="jupiterLicence_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterLicence_app_id" name="jupiterLicence_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterLicence_app_name" name="jupiterLicence_app_name" value='${app_name}'>
</form>