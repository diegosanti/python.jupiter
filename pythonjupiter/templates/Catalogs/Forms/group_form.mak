<form id="GroupFormJupiter">
    <input hidden type="text" id="jupiterGroup_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Group')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterGroup_grupo" name="jupiterGroup_grupo" maxlength="50" required value='${handler.grupo}'>
                </td>
            </tr>

        </table>
        <input type="text" hidden id="jupiterGroup_internal_id" name="jupiterGroup_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterGroup_app_id" name="jupiterGroup_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterGroup_app_name" name="jupiterGroup_app_name" value='${app_name}'>
</form>