<form id="CardTypeFormJupiter">
    <input hidden type="text" id="jupiterCardType_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Name')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterCardType_tipo_tarjeta" name="jupiterCardType_tipo_tarjeta" maxlength="50" required value='${handler.tipo_tarjeta}'>
                </td>
                <tr style="height: 10px">
                    <td></td>
                </tr>
                <td>
                    ${_('Description')}:
                </td>
                <td colspan="5">
                     <textarea rows="3" id="jupiterCardType_description" maxlength="150" name="jupiterCardType_description" required cols="20">${handler.descripcion}</textarea>
                </td>
                <tr style="height: 10px">
                    <td></td>
                </tr>
            </tr>
        </table>
        <input type="text" hidden id="jupiterCardType_internal_id" name="jupiterCardType_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterCardType_app_id" name="jupiterCardType_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterCardType_app_name" name="jupiterCardType_app_name" value='${app_name}'>
</form>