<form id="PeriodFormJupiter">
    <input hidden type="text" id="jupiterPeriod_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Motive')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterPeriod_periodo" name="jupiterPeriod_periodo" maxlength="50" required value='${handler.periodo}'>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterPeriod_internal_id" name="jupiterPeriod_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterPeriod_app_id" name="jupiterPeriod_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterPeriod_app_name" name="jupiterPeriod_app_name" value='${app_name}'>
</form>