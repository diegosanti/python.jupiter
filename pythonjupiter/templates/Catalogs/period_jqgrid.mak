<script>
    $(window).on("resize", function () {
            var $gridPeriod = $("#jqGridPeriodJupiter"),
                newWidth = $gridPeriod.closest(".ui-jqgrid").parent().width();
                $gridPeriod.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_namePeriod = '#jqGridPeriodJupiter';
    var grid_pagerPeriod = '#listPagerPeriodJupiter';
    var update_urlPeriod='${h.url()}/period/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlPeriod  ='${h.url()}/period/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsPeriod = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlPeriod,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsPeriod = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlPeriod,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsPeriod = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeriod,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsPeriod = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlPeriod,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsPeriod = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlPeriod,modal: true};
    var gridPeriod = jQuery(grid_namePeriod);
            $(document).ready(function () {
                gridPeriod.jqGrid({
                url: load_urlPeriod,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Period')}', '${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_periodo',index: 'id_periodo', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'periodo', index: 'periodo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerPeriod),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_periodo',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterPeriod(rowId)
                }
                //caption: header_container,
            });
            gridPeriod.jqGrid('navGrid',grid_pagerPeriod,{edit:false,add:false,del:false, search:true},
                            editParamsPeriod,
                            addParamsPeriod,
                            deleteParamsPeriod,
                            searchParamsPeriod,
                            viewParamsPeriod);
            // add custom button
            gridPeriod.navButtonAdd(grid_pagerPeriod,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterPeriod(0)
                    }
                });
            gridPeriod.navButtonAdd(grid_pagerPeriod,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterPeriod(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterPeriod() {
                var rowid = $('#jqGridPeriodJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/period/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridPeriodJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /period/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterPeriod(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/period/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogPeriodJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogPeriodJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /period/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogPeriodJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.3),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#PeriodFormJupiter").valid()) {
                                        var id = $('#jupiterPeriod_id').val();
                                        var periodo = $('#jupiterPeriod_periodo').val();
                                        var app_id = $('#jupiterPeriod_app_id').val();
                                        var app_name = $('#jupiterPeriod_app_name').val();
                                        var internal_id = $('#jupiterPeriod_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/period/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'periodo':periodo,'app_id':app_id,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridPeriodJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /period/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#PeriodFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#PeriodFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#PeriodFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogPeriodJupiter"  title="${_('Period')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridPeriodJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerPeriodJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->