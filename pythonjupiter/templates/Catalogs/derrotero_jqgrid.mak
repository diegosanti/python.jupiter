<script>
    $(window).on("resize", function () {
            var $gridDerrotero = $("#jqGridDerroteroJupiter"),
                newWidth = $gridDerrotero.closest(".ui-jqgrid").parent().width();
                $gridDerrotero.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameDerrotero = '#jqGridDerroteroJupiter';
    var grid_pagerDerrotero = '#listPagerDerroteroJupiter';
    var update_urlDerrotero='${h.url()}/derrotero/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlDerrotero  ='${h.url()}/derrotero/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsDerrotero = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlDerrotero,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsDerrotero = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlDerrotero,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsDerrotero = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlDerrotero,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsDerrotero = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlDerrotero,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsDerrotero = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlDerrotero,modal: true};
    var gridDerrotero = jQuery(grid_nameDerrotero);
            $(document).ready(function () {
                gridDerrotero.jqGrid({
                url: load_urlDerrotero,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Derrotero')}', '${_('internal_id')}', '${_('application_id')}', '${_('app_name')}', '${_('unidades')}'],
                colModel: [
                    {name: 'id_derrotero',index: 'id_derrotero', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'derrotero', index: 'derrotero',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerDerrotero),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_derrotero',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterDerrotero(rowId)
                }
                //caption: header_container,
            });
            gridDerrotero.jqGrid('navGrid',grid_pagerDerrotero,{edit:false,add:false,del:false, search:true},
                            editParamsDerrotero,
                            addParamsDerrotero,
                            deleteParamsDerrotero,
                            searchParamsDerrotero,
                            viewParamsDerrotero);
            // add custom button
            gridDerrotero.navButtonAdd(grid_pagerDerrotero,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterDerrotero(0)
                    }
                });
            gridDerrotero.navButtonAdd(grid_pagerDerrotero,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterDerrotero(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterDerrotero() {
                var rowid = $('#jqGridDerroteroJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/derrotero/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridDerroteroJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /derrotero/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterDerrotero(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/derrotero/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogDerroteroJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogDerroteroJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /derrotero/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogDerroteroJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.35),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#DerroteroFormJupiter").valid()) {
                                        var id = $('#jupiterDerrotero_id').val();
                                        var derrotero = $('#jupiterDerrotero_derrotero').val();
                                        var app_id = $('#jupiterDerrotero_app_id').val();
                                        var app_name = $('#jupiterDerrotero_app_name').val();
                                        var descripcion = $('#jupiterDerrotero_description').val();
                                        var internal_id = $('#jupiterDerrotero_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/derrotero/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'derrotero':derrotero,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridDerroteroJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /derrotero/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#DerroteroFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#DerroteroFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#DerroteroFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogDerroteroJupiter"  title="${_('Derrotero')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridDerroteroJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerDerroteroJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->