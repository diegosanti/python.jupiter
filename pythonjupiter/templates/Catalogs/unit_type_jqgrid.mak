<script>
    $(window).on("resize", function () {
            var $gridUnitType = $("#jqGridUnitTypeJupiter"),
                newWidth = $gridUnitType.closest(".ui-jqgrid").parent().width();
                $gridUnitType.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameUnitType = '#jqGridUnitTypeJupiter';
    var grid_pagerUnitType = '#listPagerUnitTypeJupiter';
    var update_urlUnitType='${h.url()}/unit_type/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlUnitType  ='${h.url()}/unit_type/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsUnitType = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlUnitType,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsUnitType = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlUnitType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsUnitType = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlUnitType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsUnitType = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlUnitType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsUnitType = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlUnitType,modal: true};
    var gridUnitType = jQuery(grid_nameUnitType);
            $(document).ready(function () {
                gridUnitType.jqGrid({
                url: load_urlUnitType,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Unit Type')}', '${_('internal_id')}', '${_('app_id')}', '${_('app_name')}', '${_('Description')}', '${_('unidades')}'],
                colModel: [
                    {name: 'id_tipounidad',index: 'id_tipounidad', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'tipo_unidad', index: 'tipo_unidad',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'descripcion',index: 'descripcion', align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerUnitType),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_tipounidad',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterUnitType(rowId)
                }
                //caption: header_container,
            });
            gridUnitType.jqGrid('navGrid',grid_pagerUnitType,{edit:false,add:false,del:false, search:true},
                            editParamsUnitType,
                            addParamsUnitType,
                            deleteParamsUnitType,
                            searchParamsUnitType,
                            viewParamsUnitType);
            // add custom button
            gridUnitType.navButtonAdd(grid_pagerUnitType,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterUnitType(0)
                    }
                });
            gridUnitType.navButtonAdd(grid_pagerUnitType,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterUnitType(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterUnitType() {
                var rowid = $('#jqGridUnitTypeJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/unit_type/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridUnitTypeJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /unit_type/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterUnitType(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/unit_type/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogUnitTypeJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogUnitTypeJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /unit_type/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogUnitTypeJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.35),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#UnitTypeFormJupiter").valid()) {
                                        var id = $('#jupiterUnitType_id').val();
                                        var tipo_unidad = $('#jupiterUnitType_tipo_unidad').val();
                                        var app_id = $('#jupiterUnitType_app_id').val();
                                        var app_name = $('#jupiterUnitType_app_name').val();
                                        var descripcion = $('#jupiterUnitType_description').val();
                                        var internal_id = $('#jupiterUnitType_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/unit_type/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'tipo_unidad':tipo_unidad,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridUnitTypeJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /unit_type/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#UnitTypeFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#UnitTypeFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#UnitTypeFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogUnitTypeJupiter"  title="${_('Unit Type')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridUnitTypeJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerUnitTypeJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->