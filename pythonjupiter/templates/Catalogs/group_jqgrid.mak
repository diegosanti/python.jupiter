<script>
    $(window).on("resize", function () {
            var $gridGroup = $("#jqGridGroupJupiter"),
                newWidth = $gridGroup.closest(".ui-jqgrid").parent().width();
                $gridGroup.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameGroup = '#jqGridGroupJupiter';
    var grid_pagerGroup = '#listPagerGroupJupiter';
    var update_urlGroup='${h.url()}/group/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlGroup  ='${h.url()}/group/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlGroup,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsGroup = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlGroup,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsGroup = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlGroup,modal: true};
    var gridGroup = jQuery(grid_nameGroup);
            $(document).ready(function () {
                gridGroup.jqGrid({
                url: load_urlGroup,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Group')}', '${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_grupo',index: 'id_grupo', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'grupo', index: 'grupo',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerGroup),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_grupo',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterGroup(rowId)
                }
                //caption: header_container,
            });
            gridGroup.jqGrid('navGrid',grid_pagerGroup,{edit:false,add:false,del:false, search:true},
                            editParamsGroup,
                            addParamsGroup,
                            deleteParamsGroup,
                            searchParamsGroup,
                            viewParamsGroup);
            // add custom button
            gridGroup.navButtonAdd(grid_pagerGroup,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterGroup(0)
                    }
                });
            gridGroup.navButtonAdd(grid_pagerGroup,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterGroup(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterGroup() {
                var rowid = $('#jqGridGroupJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/group/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridGroupJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /group/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterGroup(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/group/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogGroupJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogGroupJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /group/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogGroupJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.4),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#GroupFormJupiter").valid()) {
                                        var id = $('#jupiterGroup_id').val();
                                        var grupo = $('#jupiterGroup_grupo').val();
                                        var app_id = $('#jupiterGroup_app_id').val();
                                        var app_name = $('#jupiterGroup_app_name').val();
                                        var internal_id = $('#jupiterGroup_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/group/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'grupo':grupo,'app_id':app_id,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridGroupJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /group/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#GroupFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#GroupFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#GroupFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogGroupJupiter"  title="${_('Group')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridGroupJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerGroupJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->