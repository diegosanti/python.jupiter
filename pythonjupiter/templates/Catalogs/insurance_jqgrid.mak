<script>
    $(window).on("resize", function () {
            var $gridInsurance = $("#jqGridInsuranceJupiter"),
                newWidth = $gridInsurance.closest(".ui-jqgrid").parent().width();
                $gridInsurance.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameInsurance = '#jqGridInsuranceJupiter';
    var grid_pagerInsurance = '#listPagerInsuranceJupiter';
    var update_urlInsurance='${h.url()}/insurance/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlInsurance  ='${h.url()}/insurance/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsInsurance = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlInsurance,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsInsurance = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlInsurance,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsInsurance = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlInsurance,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsInsurance = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlInsurance,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsInsurance = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlInsurance,modal: true};
    var gridInsurance = jQuery(grid_nameInsurance);
            $(document).ready(function () {
                gridInsurance.jqGrid({
                url: load_urlInsurance,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Insurance')}', '${_('internal_id')}', '${_('Contact')}', '${_('Secundary Contact')}','${_('Phone')}'
                , '${_('Mobile Phone')}', '${_('Emergency Phone')}', '${_('Address')}','${_('app_id')}','${_('app_name')}','${_('unidades')}'],
                colModel: [
                    {name: 'id_aseguradora',index: 'id_aseguradora', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'aseguradora', index: 'aseguradora',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'contacto', index: 'contacto',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'contacto2', index: 'contacto2',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono', index: 'telefono',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'celular', index: 'celular',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefonoemergencia', index: 'telefonoemergencia',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'direccion', index: 'direccion',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerInsurance),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_aseguradora',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterInsurance(rowId)
                }
                //caption: header_container,
            });
            gridInsurance.jqGrid('navGrid',grid_pagerInsurance,{edit:false,add:false,del:false, search:true},
                            editParamsInsurance,
                            addParamsInsurance,
                            deleteParamsInsurance,
                            searchParamsInsurance,
                            viewParamsInsurance);
            // add custom button
            gridInsurance.navButtonAdd(grid_pagerInsurance,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterInsurance(0)
                    }
                });
            gridInsurance.navButtonAdd(grid_pagerInsurance,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterInsurance(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterInsurance() {
                var rowid = $('#jqGridInsuranceJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/insurance/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridInsuranceJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /insurance/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterInsurance(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/insurance/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogInsuranceJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogInsuranceJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /insurance/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogInsuranceJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.5),
                            width: Math.round(window.innerWidth*.5),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#InsuranceFormJupiter").valid()) {
                                        var id = $('#jupiterInsurance_id').val();
                                        var aseguradora = $('#jupiterInsurance_aseguradora').val();
                                        var contacto = $('#jupiterInsurance_contacto').val();
                                        var contacto2 = $('#jupiterInsurance_contacto2').val();
                                        var telefono = $('#jupiterInsurance_telefono').val();
                                        var celular = $('#jupiterInsurance_celular').val();
                                        var telefonoemergencia = $('#jupiterInsurance_telefonoemergencia').val();
                                        var direccion = $('#jupiterInsurance_direccion').val();
                                        var app_id = $('#jupiterInsurance_app_id').val();
                                        var app_name = $('#jupiterInsurance_app_name').val();
                                        var descripcion = $('#jupiterInsurance_description').val();
                                        var internal_id = $('#jupiterInsurance_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/insurance/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'aseguradora':aseguradora,'direccion':direccion,'telefonoemergencia':telefonoemergencia,'celular':celular,'telefono':telefono,'contacto2':contacto2,'contacto':contacto,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridInsuranceJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /insurance/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#InsuranceFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#InsuranceFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#InsuranceFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogInsuranceJupiter"  title="${_('Insurance')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridInsuranceJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerInsuranceJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->