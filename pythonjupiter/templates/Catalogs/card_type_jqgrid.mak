<script>
    $(window).on("resize", function () {
            var $gridCardType = $("#jqGridCardTypeJupiter"),
                newWidth = $gridCardType.closest(".ui-jqgrid").parent().width();
                $gridCardType.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameCardType = '#jqGridCardTypeJupiter';
    var grid_pagerCardType = '#listPagerCardTypeJupiter';
    var update_urlCardType='${h.url()}/card_type/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlCardType  ='${h.url()}/card_type/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsCardType = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlCardType,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsCardType = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlCardType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsCardType = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlCardType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsCardType = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlCardType,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsCardType = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlCardType,modal: true};
    var gridCardType = jQuery(grid_nameCardType);
            $(document).ready(function () {
                gridCardType.jqGrid({
                url: load_urlCardType,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Card Type')}', '${_('internal_id')}', '${_('Description')}','${_('application_id')}', '${_('app_name')}',  '${_('unidades')}'],
                colModel: [
                    {name: 'id_tipotarjeta',index: 'id_tipotarjeta', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'tipo_tarjeta', index: 'tipo_tarjeta',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'descripcion',index: 'descripcion', align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerCardType),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_tipotarjeta',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterCardType(rowId)
                }
                //caption: header_container,
            });
            gridCardType.jqGrid('navGrid',grid_pagerCardType,{edit:false,add:false,del:false, search:true},
                            editParamsCardType,
                            addParamsCardType,
                            deleteParamsCardType,
                            searchParamsCardType,
                            viewParamsCardType);
            // add custom button
            gridCardType.navButtonAdd(grid_pagerCardType,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterCardType(0)
                    }
                });
            gridCardType.navButtonAdd(grid_pagerCardType,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterCardType(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterCardType() {
                var rowid = $('#jqGridCardTypeJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/card_type/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridCardTypeJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /card_type/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterCardType(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/card_type/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogCardTypeJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogCardTypeJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /card_type/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogCardTypeJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.35),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#CardTypeFormJupiter").valid()) {
                                        var id = $('#jupiterCardType_id').val();
                                        var tipo_tarjeta = $('#jupiterCardType_tipo_tarjeta').val();
                                        var app_id = $('#jupiterCardType_app_id').val();
                                        var app_name = $('#jupiterCardType_app_name').val();
                                        var descripcion = $('#jupiterCardType_description').val();
                                        var internal_id = $('#jupiterCardType_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/card_type/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'tipo_tarjeta':tipo_tarjeta,'app_id':app_id,'descripcion':descripcion,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridCardTypeJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /card_type/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#CardTypeFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#CardTypeFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#CardTypeFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogCardTypeJupiter"  title="${_('Card Type')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridCardTypeJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerCardTypeJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->