<script>
    $(window).on("resize", function () {
            var $gridExternBeneficiary = $("#jqGridExternBeneficiaryJupiter"),
                newWidth = $gridExternBeneficiary.closest(".ui-jqgrid").parent().width();
                $gridExternBeneficiary.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameExternBeneficiary = '#jqGridExternBeneficiaryJupiter';
    var grid_pagerExternBeneficiary = '#listPagerExternBeneficiaryJupiter';
    var update_urlExternBeneficiary='${h.url()}/extern_beneficiary/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlExternBeneficiary  ='${h.url()}/extern_beneficiary/load?internal_id=${internal_id}&application_id=${application_id}';

    var addParamsExternBeneficiary = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlExternBeneficiary,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsExternBeneficiary = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlExternBeneficiary,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsExternBeneficiary = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlExternBeneficiary,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsExternBeneficiary = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlExternBeneficiary,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsExternBeneficiary = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlExternBeneficiary,modal: true};
    var gridExternBeneficiary = jQuery(grid_nameExternBeneficiary);
            $(document).ready(function () {
                gridExternBeneficiary.jqGrid({
                url: load_urlExternBeneficiary,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Beneficiary')}','${_('Phone')}', '${_('Address')}','${_('Legal Representative')}', '${_('RFC')}','${_('Email')}',
                    '${_('Bank')}','${_('Bank Account')}','${_('Bank')} 2','${_('Bank Account')} 2','${_('internal_id')}','${_('app_id')}','${_('app_name')}'],
                colModel: [
                    {name: 'id_beneficiariomoral',index: 'id_beneficiariomoral', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'beneficiariomoral', index: 'beneficiariomoral',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'telefono', index: 'telefono',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'domicilio', index: 'domicilio',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'representante_legal', index: 'representante_legal',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'rfc', index: 'rfc',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'email', index: 'email',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'banco', index: 'banco',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'cta_bancaria', index: 'cta_bancaria',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'banco2', index: 'banco2',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'cta_bancaria2', index: 'cta_bancaria2',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerExternBeneficiary),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_beneficiariomoral',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterExternBeneficiary(rowId)
                }
                //caption: header_container,
            });
            gridExternBeneficiary.jqGrid('navGrid',grid_pagerExternBeneficiary,{edit:false,add:false,del:false, search:true},
                            editParamsExternBeneficiary,
                            addParamsExternBeneficiary,
                            deleteParamsExternBeneficiary,
                            searchParamsExternBeneficiary,
                            viewParamsExternBeneficiary);
            // add custom button
            gridExternBeneficiary.navButtonAdd(grid_pagerExternBeneficiary,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterExternBeneficiary(0)
                    }
                });
            gridExternBeneficiary.navButtonAdd(grid_pagerExternBeneficiary,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterExternBeneficiary(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterExternBeneficiary() {
                var rowid = $('#jqGridExternBeneficiaryJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/extern_beneficiary/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridExternBeneficiaryJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /extern_beneficiary/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterExternBeneficiary(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/extern_beneficiary/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogExternBeneficiaryJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogExternBeneficiaryJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /extern_beneficiary/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogExternBeneficiaryJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.6),
                            width: Math.round(window.innerWidth*.4),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#ExternBeneficiaryFormJupiter").valid()) {
                                        var id = $('#jupiterExternBeneficiary_id').val();
                                        var beneficiariomoral = $('#jupiterExternBeneficiary_beneficiariomoral').val();
                                        var telefono = $('#jupiterExternBeneficiary_telefono').val();
                                        var domicilio = $('#jupiterExternBeneficiary_telefono').val();
                                        var representante_legal = $('#jupiterExternBeneficiary_representante_legal').val();
                                        var rfc = $('#jupiterExternBeneficiary_rfc').val();
                                        var email = $('#jupiterExternBeneficiary_email').val();
                                        var banco = $('#jupiterExternBeneficiary_banco').val();
                                        var cta_bancaria = $('#jupiterExternBeneficiary_cta_bancaria').val();
                                        var banco2 = $('#jupiterExternBeneficiary_banco2').val();
                                        var cta_bancaria2 = $('#jupiterExternBeneficiary_cta_bancaria2').val();
                                        var app_id = $('#jupiterExternBeneficiary_app_id').val();
                                        var app_name = $('#jupiterExternBeneficiary_app_name').val();
                                        var internal_id = $('#jupiterExternBeneficiary_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/extern_beneficiary/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'cta_bancaria2':cta_bancaria2,'banco2':banco2,'cta_bancaria':cta_bancaria,'email':email,'banco':banco,'domicilio':domicilio,'representante_legal':representante_legal,'rfc':rfc,'beneficiariomoral':beneficiariomoral,'app_id':app_id,'telefono':telefono,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                 if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridExternBeneficiaryJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /extern_beneficiary/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#ExternBeneficiaryFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#ExternBeneficiaryFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#ExternBeneficiaryFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogExternBeneficiaryJupiter"  title="${_('Operator Group')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridExternBeneficiaryJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerExternBeneficiaryJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->