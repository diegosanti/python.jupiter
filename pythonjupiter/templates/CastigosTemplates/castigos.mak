<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>
<script>
                         var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');

                  function searchreportes() {

                      var searchfor=document.getElementById("jupiter_selectbuscarreporte").value;
                      var name=document.getElementById("jupiter_filtercastigos").value;
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/reportes/searchreporte?user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}&searchfor="+searchfor+"&name="+name,
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablereportes td').remove();
                        $("#bookmarkreportes").html(data['contador']+ " Reporte(s)");


                       for(var k in data['lista']){



                            $('#jupiter_tablereportes').append('<tr><td><a onclick=\"openWindowreportes(\'Editar Reporte\',\'${h.url()}/reportes/editreporte?id='+data['lista'][k]['id']+'\');\">'+
                                    '<span class="glyphicon glyphicon-edit"></span></a></td><td>'+data['lista'][k]['fecha']+'</td><td>'+data['lista'][k]['castigo']+'</td><td>'+data['lista'][k]['magnitud']+'</td>'+
                                    '<td>'+data['lista'][k]['descripcion']+'</td><td>'+data['lista'][k]['operador']+'</td><td>'+data['lista'][k]['apodo']+'</td><td>'+data['lista'][k]['unidad']+'</td></tr>' );


                        }


                    },
                    error: function () {
                        $.alert("Error accessing searchreportes", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }




              function reloadallreportes() {
            document.getElementById("jupiter_filtercastigos").value="";
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/reportes/reloadreportes?user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablereportes td').remove();
                      $("#bookmarkreportes").html(data['contador']+ " Reporte(s)");


                       for(var k in data['lista']){



                            $('#jupiter_tablereportes').append('<tr><td><a onclick=\"openWindowreportes(\'Editar Reporte\',\'${h.url()}/reportes/editreporte?id='+data['lista'][k]['id']+'\');\">'+
                                    '<span class="glyphicon glyphicon-edit"></span></a></td><td>'+data['lista'][k]['fecha']+'</td><td>'+data['lista'][k]['castigo']+'</td><td>'+data['lista'][k]['magnitud']+'</td>'+
                                    '<td>'+data['lista'][k]['descripcion']+'</td><td>'+data['lista'][k]['operador']+'</td><td>'+data['lista'][k]['apodo']+'</td><td>'+data['lista'][k]['unidad']+'</td></tr>' );


                        }


                    },
                    error: function () {
                        $.alert("Error accessing reloadreportes", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }

                      function searchentercastigo(e) {

    if (e.keyCode === 13 && !e.shiftKey)  {

        searchreportes();

         }
       }


       function openWindowreportes(text,url){
            doAjax(url,'openTab');
            var $dialog = $('<div></div>')
               .html('<div id="openTab" style="margin:10px"></div>')
               .dialog({
                   autoOpen: false,
                   modal: true,
                   height:  880,
                   width: 1220,
                   resizable: false,
                   draggable: true,
                   position : { my: "center", at: "top", of: window },
                   close: function(){
                       $(this).dialog('destroy').remove();
                       reloadallreportes();
                   },
                   title: text,
                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                   buttons: {

                   }
               });
        $dialog.dialog('open');
        }



</script>

                <button style="float: left;" onclick="openWindowreportes('Nuevo Reporte','${h.url()}/reportes/nuevoreporte');">
                        <span class="glyphicon glyphicon-plus">${_('New')}</span>
                    </button>
<br>
<br>
          BUSCAR POR: <select id="jupiter_selectbuscarreporte" onchange="ocultartext();">

    <option>fecha</option>
    <option>reporte</option>
    <option>gravedad</option>
    <option>operador</option>
    <option>apodo</option>
    <option>unidad</option>

</select>               <input type="text" id="jupiter_filtercastigos" onkeypress="searchentercastigo(event);" placeholder="Reportes"><button onclick=searchreportes()><i class="fa fa-search"></i></button><button onclick="reloadallreportes()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
<div id="bookmarkreportes" class="bookmark" style="float: right;">${contador} Reporte(s)</div>

<table id="jupiter_tablereportes" class="userTable">
    <tr>
        <th></th>
        <th>${_('DATE')}</th>
        <th>${_('REPORT')}</th>
        <th>${_('GRAVITY')}</th>
        <th>${_('DESCRIPTION')}</th>
        <th>${_('OPERATOR')}</th>
        <th>${_('APODO')}</th>
        <th>${_('UNIT')}</th>
    </tr>
    %for item in lista:
          <tr>
              <td> <a onclick="openWindowreportes('Editar Reporte','${h.url()}/reportes/editreporte?id=${item['id']}');"><span class="glyphicon glyphicon-edit"></span></a></td>
              <td>${item['fecha']}</td>
              <td>${item['castigo']}</td>
              <td>${item['magnitud']}</td>
              <td>${item['descripcion']}</td>
              <td>${item['operador']}</td>
              <td>${item['apodo']}</td>
              <td>${item['unidad']}</td>

          </tr>
    %endfor


</table>