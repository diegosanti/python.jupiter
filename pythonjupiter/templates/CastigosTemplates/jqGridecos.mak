<body>
    <table id="jqGridecosreporte"></table>
    <div id="jqGridPagerecosreporte"></div>
<script type="text/javascript">

 var griddereportes=$("#jqGridecosreporte");
 var grid_pagerreportes=$("#jqGridPagerecosreporte");
$(document).ready(function () {

		$("#jqGridecosreporte").jqGrid({
		url: '${h.url()}/ecos2',
		datatype: "json",
		 colModel: [
			{ label: 'Ecos', name: 'ecos', width: 75 ,  searchoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
							sopt : ['cn']
                        }},

		],
		viewrecords: true, // show the current page, data rang and total records on the toolbar
		width: 780,
		height: 400,
		rowNum: 30,
            //ondblClickRow: dobleclick,
		loadonce: true, // this is just for the demo
		pager: "#jqGridPagerecosreporte",


		});


                 var grid_name = '#jqGridecosreporte';
                 var grid = jQuery(grid_name);
            var grid_pager= '#jqGridPagerecosreporte';

               grid.jqGrid('navGrid',grid_pager,{edit:false,add:false,del:false, search:false});

                     grid.navButtonAdd(grid_pager,
                {
                    buttonicon: "glyphicon glyphicon-arrow-up",
                    title: "${_('Insertar')}",
                    caption: "${_('Insertar')}",
                    position: "first",
                    onClickButton: dobleclick
                    <%doc>onClickButton: function(){
                        doDoubleClickAbono(0)
                    }</%doc>
                });





		 $('#jqGridecos').jqGrid('filterToolbar');
});



function dobleclick() {
    var myGrid = $('#jqGridecosreporte');
    var selRowId = myGrid.jqGrid ('getGridParam', 'selrow');
    var rowData = myGrid.jqGrid("getRowData", selRowId);
    document.getElementById("readonlyunidad").value=rowData.ecos;
    Dialog01togridecosreporte.dialog("close");

  }
 </script>


</body>