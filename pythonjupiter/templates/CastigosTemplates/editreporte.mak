<script>


                var winHeighttogridecosreporte = Math.round(window.innerHeight * .69);
            var winWidthtogridecosreporte = Math.round(window.innerWidth * .60);

        var addFilter01Buttonstogridecosreporte = {
            "Close": function () {
                Dialog01togridecosreporte.dialog("close");
            }
        };

            var Dialog01togridecosreporte = $("#dialogEcosreporte").dialog({
            autoOpen: false,
            height: winHeighttogridecosreporte ,
            width: winWidthtogridecosreporte ,
            modal: true,
            buttons: addFilter01Buttonstogridecosreporte,
            close: function () {

            }
        });

            function ecosClickedreportes() {

        // Append html
        $.ajax({
            type: "GET",
            url: "${h.url()}/reportes/ecostemplate",
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogEcosreporte").html(parameterdata.template);
                Dialog01togridecosreporte.data('rowId',1);
                Dialog01togridecosreporte.dialog("open");

            },
            error: function () {
                alert("Error accessing server /ecostemplatereporte")
            },
            complete: function () {
            }
        });



}



    function cambiaranew() {
        if(document.getElementById('textoselectotext').innerText=='Nuevo Castigo') {
            document.getElementById('reporteconselect').style.visibility = 'hidden';
            document.getElementById('reporteconinput').style.visibility = 'visible';
            document.getElementById('textoselectotext').innerText='Cancelar';

        }else{
                document.getElementById('reporteconselect').style.visibility = 'visible';
            document.getElementById('reporteconinput').style.visibility = 'hidden';
            document.getElementById('textoselectotext').innerText='Nuevo Castigo';
            document.getElementById('inputcastigo').innerText='';

        }

      }

      function insertaoperador(e) {
        if (e.keyCode === 13 && !e.shiftKey) {
            var searchfor = document.getElementById('selectsearchforoperador').value;
            var what = document.getElementById('whatoperador').value;
            $.ajax({
                type: "GET",
                url: "${h.url()}/reportes/insertaoperador?searchfor=" + searchfor + "&what=" + what,
                contentType: "application/json; charset=utf-8",
                data: {},
                 success: function (parameterdata) {
                    ##alert("Peticion Exitosa");
                        //Insert HTML code
                    $('#readonlyoperador option').remove();
                    //document.getElementById("readonlyoperador").value='';
                    if(parameterdata['data']==[]){
                        alert("No existe el operador a buscar");
                    }else{
                        for (var r in parameterdata['data']) {
                            $('#readonlyoperador').append('<option>'+parameterdata['data'][r]['operador']+'/'+parameterdata['data'][r]['apodo']+'</option>');
                        }

                    }
                    //alert(parameterdata['id_operador']);

                },
                error: function () {
                    alert("ERROR insertar operador");
                },
                complete: function () {
                }
            });

        }
        }


        function insertaunidad(e) {
        if (e.keyCode === 13 && !e.shiftKey) {
            var searchfor = document.getElementById('selectsearchforunidad').value;
            var what = document.getElementById('readonlyunidad').value;

           $.ajax({
                type: "GET",
                url: "${h.url()}/reportes/insertaunidad?searchfor="+searchfor + "&what=" + what,
                contentType: "application/json; charset=utf-8",
                data: {},
                success: function (parameterdata) {
                    ##alert("Peticion Exitosa");
                        //Insert HTML code
                    document.getElementById("readonlyunidad").value='';
                    if(parameterdata['eco1']==null){
                        alert("No existe la unidad");
                    }else{
                        if(parameterdata['eco1']=='nada'){
                            alert("No existe la unidad");
                        }else {
                            document.getElementById("readonlyunidad").value = parameterdata['eco1'];
                        }

                    }
                    //alert(parameterdata['id_operador']);

                },
                error: function () {
                    alert("ERROR insertar unidad");
                },
                complete: function () {
                }
            });

        }
        }

</script>

<fieldset>
        <legend>${_('Edit Report')}</legend>

      <form action="${h.url()}/reportes/editandoreporte?internal_id=${internal_id}&user=${user}&id=${id}" name="reportesform" id="reportesform" method="POST" enctype="multipart/form-data" target="requestreportesedit" onKeypress="if(event.keyCode == 13) event.returnValue = false;">
        <table>

              <tr id="reporteconselect">
                  <td>${_('Punishment: ')}</td>
                  <td>
                        <select id="selectcastigos" name="selectcastigos">
                            %for item in allcastigos:
                                %if item.reporte==variablereporte:
                                    <option selected>${item.reporte}</option>
                                %else:
                                    <option>${item.reporte}</option>

                                %endif

                            %endfor
                        </select>
                  </td>

              </tr>
            <tr>
               <td> <a onclick="cambiaranew()" id="textoselectotext">${_('Nuevo Castigo')}</a></td>

            </tr>
            <tr id="reporteconinput" style="visibility: hidden">
                <td>${_('Punishment: ')}</td>
                <td><textarea id="inputcastigo" name="inputcastigo"></textarea></td>
            </tr>
            <tr>
            <td>${_('Gravity: ')}</td>
            <td><select id="selectgravedad" name="selectgravedad">
                %if variablegravedad==1:
                    <option selected>1</option><option>2</option><option>3</option><option>4</option><option>5</option>
                %endif
                                %if variablegravedad==2:
                    <option >1</option><option selected>2</option><option>3</option><option>4</option><option>5</option>
                %endif
                                %if variablegravedad==3:
                    <option >1</option><option>2</option><option selected>3</option><option>4</option><option>5</option>
                %endif
                                %if variablegravedad==4:
                    <option >1</option><option>2</option><option>3</option><option selected>4</option><option>5</option>
                %endif
                                %if variablegravedad==5:
                    <option >1</option><option>2</option><option>3</option><option>4</option><option selected>5</option>
                %endif
            </select></td>

            </tr>

            <tr>
            <td>${_('Operator')}</td>
                <td><select id="selectsearchforoperador"><option>credencial</option><option>apodo</option></select></td>
                <td><input type="text" length="10" id="whatoperador" onkeypress="insertaoperador(event)"> </td>
                <td><select name="readonlyoperador" id="readonlyoperador" readonly required><option>${variableoperador}/${variableapodo}</option></select></td>

            </tr>

               <tr>
            <td>${_('Unit')}</td>
                <td><select id="selectsearchforunidad"><option>eco1</option></select></td>
                <td><input type="text" length="10" id="readonlyunidad" name="readonlyunidad" onclick="ecosClickedreportes()" value="${variableunidad}" readonly> </td>


            </tr>

        </table>
                    <table>
            <tr>
                <td>${_('Description: ')}</td>

                <td><textarea id="jupiter_descripcionreporte" name="jupiter_descripcionreporte" rows="8" cols="80">${variabledescripcion}</textarea></td>

            </tr>


        </table>
          <input type="submit" name="jupiter_submit_uploadreporte" value="GUARDAR"/>
        </form>
      <br>
      <iframe name="requestreportesedit" id="requestreportesedit" frameborder="0"></iframe>
    <div id="dialogEcosreporte"></div>
  </fieldset>
