<script>
    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridsalidas"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name_salidas = '#jupiter_jqGridsalidas';
    var grid_pager_salidas= '#jupiter_Pagersalidas';
    var update_url_salidas='${h.url()}/bodyhead/updatesalida?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_url_salidas ='${h.url()}/bodyhead/loadsalida?application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';

    var addParams_salidas = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_url_salidas,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParams_salidas = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_url_salidas,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url_bodyhead,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParams_salidas = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url_salidas,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParams_salidas = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url_salidas,modal: true};
    var grid_salidas = jQuery(grid_name_salidas);
            $(document).ready(function () {
                grid_salidas.jqGrid({
                url: load_url_salidas,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Cancel')}','${_('Date')}','${_('Type')}','${_('Amount')}','${_('Internal_id')}','${_('App_name')}','${_('App_id')}','${_('Reason')}','${_('Beneficiary')}','${_('Person')}','${_('Bolsa')}',
                            '${_('Concepto')}','${_('Recibo')}'],
                colModel: [
                    {name: 'id_salida',index: 'id_salida', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'cancelacion',index: 'cancelacion', align: 'left',hidden: false,width:80,editable: true,formatter: getimage,edittype: 'text',editrules: {required: true},search:true},
                    {name: 'fecha', index: 'fecha',align: 'left',formatter: 'datetime',hidden: false,width:80,editable: true, edittype: 'text', editrules: {required: true},search:true},
                    {name: 'tipo',index: 'tipo',hidden:false,align: 'left',editable: true,edittype: 'text',editrules: {required: true},search:true},
                    {name: 'monto',index: 'monto',hidden:false,align: 'left',editable: true,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},edittype: 'date',editrules: {required: true},search:true},
                    {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'text',editrules: {required: true},search:true},
                    {name: 'app_name',index: 'app_name',align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'application_id',index: 'application_id',align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'id_motivo',index: 'id_motivo',align: 'left',hidden: false,formatter: getmotivodesalida,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'id_beneficiariomoral',index: 'id_beneficiariomoral',formatter: getbeneficiariosalida,align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'id_persona',index: 'id_persona',formatter: getpersonasalida,align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'cuentacargo_id',index: 'cuentacargo_id',formatter: getcuentacargo,align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'concepto',index: 'concepto',hidden:false,align: 'left',editable: true,edittype: 'text',editrules: {required: true},search:true},
                    {name:'otro',index: 'otro',hidden: false,formatter:function (cellvalue, options, rowObject) {
                        return "<input type='button' value='Recibo' onclick='elrecibodesalidapdf(\""+rowObject+"\")' \>";
                    }}



                ],
                pager: jQuery(grid_pager_salidas),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_salida',
                sortorder: "desc",
                viewrecords: true,
                autowidth: true,
                height: 250

            });


                           grid_salidas.jqGrid('navGrid',grid_pager_salidas,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:false},
                            editParams_salidas,
                            addParams_salidas,
                            //deleteParams,
                            searchParams_salidas,
                            viewParams_salidas);
            // add custom button

            });

            grid_salidas.navButtonAdd(grid_pager_salidas,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add Pay to')}",
                    caption: "${_('Add Pay to')}",
                    position: "first",
                    onClickButton: addpayto
                    <%doc>onClickButton: function(){
                        doDoubleClickAbono(0)
                    }</%doc>
                });


    grid_salidas.navButtonAdd(grid_pager_salidas,
                {
                    buttonicon: "ui-icon-closethick",
                    title: "${_('Cancel Pay')}",
                    caption: "${_('Cancel Pay')}",
                    position: "second",
                   onClickButton: function(){
                        cancelapayto()
                    }
                });


            $.extend($.jgrid.nav,{alerttop:1});
            function getimage(cellValue) {
                if(cellValue==0){
                    return "<img src=\"${h.url()}/img/check.png\" width=\"20px\" height=\"20px\"/>";
                }else{
                    return "<img src=\"${h.url()}/img/red-cross.png\"/ width=\"20px\" height=\"20px\">";
                }

              }





    function elrecibodesalidapdf(rowObject) {

                  var thesplit = rowObject.split(',');
                if (thesplit[1] == '0') {
                    var winHeight = Math.round(window.innerHeight * .85);
                    var winWidth = Math.round(window.innerWidth * .95);

                    var addFilter02Buttons = {
                          "Export to PDF": function () {

                              var file_path = '${h.url()}'+document.getElementById("the_filenamerecibosalida").value;
                              window.open(file_path);

                        },
                       <%doc> "Send Email": function () {
                            VentanadeCorreos(thesplit);
                        },</%doc>
                        "Close": function () {
                            Dialog02.dialog("close");
                        }
                    };

                    var Dialog02 = $("#dialogrecibodesalida").dialog({
                        autoOpen: false,
                        height: winHeight,
                        width: winWidth,
                        modal: true,
                        buttons: addFilter02Buttons,
                        close: function () {

                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: "${h.url()}/bodyhead/htmlrecibodesalida" + "?rowObject=" + rowObject,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {

                            //Insert HTML code
                            $("#dialogrecibodesalida").html(parameterdata.template);
                            Dialog02.data('rowId', 1);
                            Dialog02.dialog("open");
                            document.getElementById("the_filenamerecibosalida").value=parameterdata['file_name'];

                        },
                        error: function () {
                            $.alert("ERROR htmlreciboabono",{type: 'danger'});
                        },
                        complete: function () {
                        }
                    });

                }
                }


function addpayto(rowId) {


                        $.ajax({
                        type: "GET",
                        url: '${h.url()}/bodyhead/formsalida?id='+rowId+'&application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#jupiter_dialogaddsalida" ).html(parameterdata.template);
                            $("#jupiter_dialogaddsalida").show();

                            $("#jupiter_SalidaForm").validate({
                                rules: {
                                    salidademonto: {
                                        required: true,min: 0,max: 21450000
                                    }
                                }
                            });
                        },
                        error: function() {
                            $.alert("Error accessing server /bodyhead/formsalida",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#jupiter_dialogaddsalida" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.90),
                            width: Math.round(window.innerWidth*.90),
                            modal: true,
                            buttons: {
                                "${_('PAY')}": function() {
                                    if($("#jupiter_SalidaForm").valid()) {

                                        var motivo = $('#salidademotivo').val();
                                        var tipobeneficiario=$('#salidadetipo').val();
                                        var persona=$('#salidadebeneficiariopersona').val();
                                        var empresaaquien=$('#salidadebeneficiario').val();
                                        var abolsa=$('#salidadebolsa').val();
                                        var monto=$('#salidademonto').val();
                                        var app_nameformsalida=$('#app_nameformsalida').val();
                                        var serial=$("#jupiter_SalidaForm").serialize();
                                        //alert(serial);

                                        valido=false;
                                        var esto=document.getElementById("jupiter_SalidaForm");
                                        var esteform=document.getElementById("jupiter_SalidaForm").elements.length;
                                        for(a=0;a<esteform;a++){
                                                if(esto[a].type=="checkbox" && esto[a].checked==true){
                                                    valido=true;
                                                    break
                                                }
                                        }
                                        if(!valido){
                                            $.alert("Seleccione al menos una bolsa!",{type: 'warning'});return false
                                        }else {
                                            $.ajax({
                                                type: "GET",
                                                url: "${h.url()}/bodyhead/savesalida?internal_id=${internal_id}&app_id=${application_id}&user=${user}",
                                                contentType: "application/json; charset=utf-8",
                                                data: serial,
                                                success: function (data) {
                                                    if (data['error'] == 'ok') {
                                                        ContDialog.dialog("close");
                                                        $('#jupiter_jqGridsalidas').trigger('reloadGrid');
                                                    }
                                                },
                                                error: function () {
                                                    //alert("#"+ckbid);
                                                    $.alert("${_('Error accessing to')} /bodyhead/savesalida", {
                                                        autoClose: false,
                                                        type: "danger"
                                                    });
                                                    return true;
                                                },
                                                complete: function () {
                                                }
                                            });
                                        }
                                    }
                                },
                                "${_('Close')}": function() {
                                    //$('#jupiter_SalidaForm')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                //$('#jupiter_SalidaForm')[0].reset();
                            }
                        });

                    ContDialog.dialog( "open" );



  }
  
  function cancelapayto() {
             var rowid = $("#jupiter_jqGridsalidas").jqGrid('getGridParam', 'selrow');
                if(rowid) {
                    var confirmando = confirm("¿Estas seguro de cancelar la salida " + rowid);
                    if (confirmando) {
                        $.ajax({
                            type: "GET",
                            url: '${h.url()}/bodyhead/cancelapayto?id=' + rowid + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                            async: false,
                            contentType: "application/json; charset=utf-8",
                            data: {},
                            success: function (parameterdata) {
                                //Insert HTML code

                                $('#jupiter_jqGridsalidas').trigger('reloadGrid');
                            },
                            error: function () {
                                $.alert("Error accessing server cancelapayto", {type: "danger"});
                            },
                            complete: function () {

                            }

                        });
                    }
                }
    }
    
    
    function getmotivodesalida(cellvalue) {
                var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getmotivosalida?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['motivo'];



                        },
                        error: function () {
                            $.alert("Error accessing server getmotivodesalida", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;
      }


function getcuentacargo(cellvalue) {
                    var thereturn;
                if(cellvalue!=0) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/bodyhead/getcuentacargo?id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['cuentacargo'];

                        },
                        error: function () {
                            $.alert("Error accessing server getcuentacargo", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;
  }


          function getbeneficiariosalida(cellvalue) {
                var thereturn;
                if(cellvalue!=0) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/bodyhead/getbeneficiariosalida?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['beneficiario'];
                        },
                        error: function () {
                            $.alert("Error accessing server getbeneficiariosalida", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;
      }


      function getpersonasalida(cellvalue) {
                   var thereturn;
                if(cellvalue!=0) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/bodyhead/getpersonasalida?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['persona'];
                        },
                        error: function () {
                            $.alert("Error accessing server getpersonasalida", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

        }

        function buscalasalidade() {
                var buscarpor=document.getElementById("jupiter_searchforsalidade").value;
                var busca=document.getElementById("jupiter_buscasalida").value;
                var fechaini=document.getElementById("jupiter_fechasalidade").value;
                var fechafin=document.getElementById("jupiter_fechafinalsalidade").value;
            var url='${h.url()}/bodyhead/loadsalidasearch?application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&buscarpor='+buscarpor+'&busca='+busca+'&fechaini='+fechaini+
                    '&fechafin='+fechafin;
                  // document.getElementById("jupiter_fechasalidade").value='${fecha}';
                 //document.getElementById("jupiter_fechafinalsalidade").value='${fecha}';
                  jQuery("#jupiter_jqGridsalidas").setGridParam({url: url}).trigger("reloadGrid");

          }

          function reloadsalidade() {
              var url='${h.url()}/bodyhead/loadsalida?application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
                         document.getElementById("jupiter_fechasalidade").value='${fecha}';
                 document.getElementById("jupiter_fechafinalsalidade").value='${fecha}';
                 document.getElementById("jupiter_buscasalida").value='';
                  jQuery("#jupiter_jqGridsalidas").setGridParam({url: url}).trigger("reloadGrid");
            }


</script>
<table>
    <tr style="height:50px";>
        <td width="100px">${_('Initial Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechasalidade" name="jupiter_fechasalidade"></td>
        <td width="100px">${_('Final Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechafinalsalidade" name="jupiter_fechafinalsalidade"></td>
    </tr>
    </table>
<table>
     <tr style="height:50px";>
    <td>${_('Search for: ')}</td><td><select id="jupiter_searchforsalidade" name="jupiter_searchforsalidade"><option>-Seleccione-</option><option>Tipo</option><option>Motivo</option><option>Beneficiario</option><option>Persona</option>
     <option>Bolsa</option></select></td>
         <td><input type="text" id="jupiter_buscasalida" name="jupiter_buscasalida" onkeypress="buscalasalidadeenter();"> </td>
         <td><button onclick="buscalasalidade();"><span class="fa fa-search"></span></button></td>
         <td><button onclick="reloadsalidade();"><span class="glyphicon glyphicon-refresh"></span></button></td>
    </tr>

</table>


    <table style="width:100%">
    <table id="jupiter_jqGridsalidas" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="jupiter_Pagersalidas" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
    <div id="jupiter_dialogaddsalida" title="Pago a"></div>
    <div id="dialogrecibodesalida" title="Recibo de Salida"></div>
    <input type="text" id="the_filenamerecibosalida" name="the_filenamerecibosalida" style="display: none;" readonly>