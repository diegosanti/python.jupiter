
<script>
    function vistadebeneficiariosalida() {
        var elselect=document.getElementById("salidadetipo").value;
        if(elselect=="Persona") {
            document.getElementById("latablabeneficiariosalida").style.display = "none";
             document.getElementById("latablapersonasalida").style.display = "block";
        }else{
            document.getElementById("latablabeneficiariosalida").style.display = "block";
             document.getElementById("latablapersonasalida").style.display = "none";
        }
      }

      function mostrarinputdesalida(esto) {
        var elid;
        elid=esto.id.split("check");
          if(esto.checked==true){
              document.getElementById("tabla"+elid[1]).style.display='block';
              var app_name=document.getElementById("app_nameformsalida").value;
                               $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/bodyhead/getmaxcuenta?cuenta="+elid[1]+"&app_name="+app_name+"&internal_id=${internal_id}&application_id=${application_id}",
                                            contentType: "application/json; charset=utf-8",
                                            data: {},
                                            beforeSend: function() {
                                                document.getElementById("salidademontomaxm"+elid[1]).innerHTML="<img src=\"${h.url()}/img/ajax-loader.gif\">";
                                            },
                                            success: function(data) {
                                            document.getElementById("salidademontomax"+elid[1]).value=data['total'];
                                            document.getElementById("salidademontomaxm"+elid[1]).innerText='$'+data['total']+'.00';
                                            document.getElementById("salidademonto"+elid[1]).max=data['total'];
                                            document.getElementById("salidademonto"+elid[1]).required=true;
                                            },
                                            error: function() {
                                                 $.alert("${_('Error accessing to')} /bodyhead/getmaxcuenta", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });

          }else{
              document.getElementById("tabla"+elid[1]).style.display='none';
              document.getElementById("salidademontomax"+elid[1]).value=0;
              document.getElementById("salidademontomaxm"+elid[1]).innerText='';
              document.getElementById("salidademonto"+elid[1]).max=0;
              document.getElementById("salidademonto"+elid[1]).required=false;
          }
        }
        
        
        
        function cambiodemaxdesalidaapp(esteapp) {
            var elid;
            var esto=document.getElementById("jupiter_SalidaForm");
            var esteform=document.getElementById("jupiter_SalidaForm").elements.length;

            for(a=0;a<esteform;a++){
                if(esto[a].type=="checkbox" && esto[a].checked==true){
                    elid=esto[a].id.split("check");
                    document.getElementById("salidademontomax"+elid[1]).value=0;
                    document.getElementById("salidademontomaxm"+elid[1]).innerText='';
                    document.getElementById("salidademonto"+elid[1]).max=0;
                    document.getElementById("salidademonto"+elid[1]).required=false;
                    var app_name=esteapp.value;
                               $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/bodyhead/getmaxcuenta?cuenta="+elid[1]+"&app_name="+app_name+"&internal_id=${internal_id}",
                                            contentType: "application/json; charset=utf-8",
                                            data: {},
                                            async: false,
                                            beforeSend: function() {
                                                document.getElementById("salidademontomaxm"+elid[1]).innerHTML="<img src=\"${h.url()}/img/ajax-loader.gif\">";
                                            },
                                            success: function(data) {
                                                document.getElementById("salidademontomax"+elid[1]).value=data['total'];
                                                document.getElementById("salidademontomaxm"+elid[1]).innerText='$'+data['total']+'.00';
                                                document.getElementById("salidademonto"+elid[1]).max=data['total'];
                                                document.getElementById("salidademonto"+elid[1]).required=true;
                                            },
                                            error: function() {
                                                 $.alert("${_('Error accessing to')} /bodyhead/getmaxcuenta", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });

                }
            }

          }

</script>
<body>
<form id="jupiter_SalidaForm" name="jupiter_SalidaForm">

<table>
    <h3>Pago a:</h3>
    <hr>
    <tr height="40px">
        <td width="120px">Motivo: </td><td><select id="salidademotivo" name="salidademotivo">
        %for itemmotivo in querymotivos:
        <option>${itemmotivo.motivo}</option>
        %endfor
    </select></td>
    </tr>


        <tr height="60px">
        <td width="120px">Tipo de Beneficiario: </td><td><select id="salidadetipo" name="salidadetipo" onchange="vistadebeneficiariosalida()">
            <option value="Persona">Persona</option>
            <option value="Empresa">Empresa u Otro</option>
    </select></td>
    </tr>
</table>
    <table id="latablapersonasalida">
    <tr>
    <td width="120px">Beneficiario: </td><td><select id="salidadebeneficiariopersona" name="salidadebeneficiariopersona">
    %for itempersona in querypersona:
        <option>${itempersona.nombre}</option>
    %endfor
    </select></td>
    </tr>
    </table>

        <table id="latablabeneficiariosalida" style="display: none;">
    <tr>
    <td width="120px">Beneficiario: </td><td><select id="salidadebeneficiario" name="salidadebeneficiario">
    %for itempersona in querybeneficiariomoral:
        <option>${itempersona.beneficiariomoral}</option>
    %endfor
    </select></td>
    </tr>

    </table>
    <table>
        <tr height="60px"><td width="120px">App_name: </td><td><select id="app_nameformsalida" name="app_nameformsalida" onchange="cambiodemaxdesalidaapp(this);">
        %for itemem in lasempresas:
        <option>${itemem['empresa']}</option>
        %endfor
    </select></td>

           </tr>
    </table>

    <table>
    <tr height="60px">
    <td width="120px">Bolsa: </td><td>
        %for itemcaja in querycaja:
            <input type="checkbox" id="check${itemcaja.nombre}" name="check${itemcaja.nombre}" onchange="mostrarinputdesalida(this)">${itemcaja.nombre}
            <br>
        %endfor
    </td>
    </tr>

    </table>
    <br>
    <table>
        <tr>
            <td width="120px">Concepto: </td><td><textarea id="conceptodesalida" name="conceptodesalida"></textarea></td>
        </tr>

    </table>

    %for itemcaja in querycaja:
         <table id="tabla${itemcaja.nombre}" style="display: none;">
        <tr height="60px">
            <td width="120px"><p id="parrafo${itemcaja.nombre}" name="parrafo${itemcaja.nombre}" >Monto de $ ${itemcaja.nombre}</p></td>
        <td><input type="number" id="salidademonto${itemcaja.nombre}" name="salidademonto${itemcaja.nombre}" ></td>
            <td>Cantidad en la bolsa: <div id="salidademontomaxm${itemcaja.nombre}" name="salidademontomaxm${itemcaja.nombre}"></div></td>
            <td><input type="number" id="salidademontomax${itemcaja.nombre}" name="salidademontomax${itemcaja.nombre}" style="display: none;"></td>
          </tr>
        </table>
    %endfor









</form>

</body>