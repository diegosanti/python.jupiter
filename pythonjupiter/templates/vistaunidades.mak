<!DOCTYPE html>

<html lang="en">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="//cdn.jsdelivr.net/free-jqgrid/4.8.0/js/i18n/grid.locale-es.js"></script>
  <script src="//cdn.jsdelivr.net/free-jqgrid/4.8.0/js/jquery.jqgrid.min.js"></script>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/free-jqgrid/4.8.0/css/ui.jqgrid.css">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/redmond/jquery-ui.css" type="text/css"/>
    <meta charset="utf-8" />
    <title>jqGrid Loading Data - Million Rows from a REST service</title>
</head>
<body>
    <table style="width:100%;overflow:auto;">
    <table id="jqGridTable" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerTables" class="scroll" style="text-align:center;"></div>
    <div id="listPsetcols" class="scroll" style="text-align:center;"></div>
    </table>
<script type="text/javascript">
$('#tr').dblclick(function(){
  var id = $(this).attr('id');
  alert(id);
  //do something with id
})
<%doc>        $(document).ready(
        function () {
            var grid_name = '#jqGridTable';
            var grid_pager= '#listPagerTables';
            var update_url='/updatePhoneBook';
            var load_url='/loadPhoneBook/';
            var header_container='Phone Book';
            var addParams = {left: 0,width: window.innerWidth-600,top: 20,height: 220,url: update_url, closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true}
            var editParams = {left: 0,width: window.innerWidth-400,top: 20,height: 220,url: update_url,closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true,
                    width: "500",
                    editfunc: function (rowid) {
                    alert('The "Edit" button was clicked with rowid=' + rowid);
                    }
                };
            var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true}
            var viewParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true}
            var searchParams = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url,modal: true, };
            var grid = jQuery(grid_name);
            grid.jqGrid({
                url: load_url,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['id', 'Name','Birthday','Age','Phone'],
                                colModel: [
                    {name: 'id',index: 'id', width: 5,align: 'left',key:true,hidden: true, editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'name',index: 'name', width: 25, align: 'right', hidden: false, editable: true, edittype: 'text',editrules: {required: false}},
                    {name: 'birthday',index: 'birthday', formatter: 'date', width: 10,sortable: false,align: 'right',editable: true, editoptions: {size: 20,maxlengh: 10,
                                            dataInit: function (element) {
                                                $(element).datepicker({
                                                    dateFormat: 'yy-mm-dd',
                                                    constrainInput: false,
                                                    showOn: 'button',
                                                    buttonText: '...'
                                                });
                                            }
                            },
                            formatoptions: {
                                newformat: "Y-m-d"
                            },

                    },
                    {name: 'age',index: 'age', width: 5, align: 'right',hidden: false,editable: true, edittype: 'text',editrules: {required: false,minValue: 1,maxValue: 99}}
                    {name: 'phone', index: 'phone', width: 20,align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: false}},

                ],


                pager: jQuery(grid_pager),
                rowNum: 10,
                rowList: [10, 50, 100],
                sortname: 'name',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClick(rowId)
                },
                caption: header_container,

            });
            grid.jqGrid('navGrid',grid_pager,{edit:true,add:true,del:true, search:true},
                            editParams,
                            addParams,
                            deleteParams,
                            searchParams,
                            viewParams);
        });
        $.extend($.jgrid.nav,{alerttop:1});</%doc>
    </script>

<table class="table table-hover" style="width: 100%;">
    	<thead>
		<tr>
            <th>-</th>
			<th>${_('ECO')}</th>
			<th>${_('ECO 2')}</th>
			<th>${_('CARD')}</th>
            <th>${_('STATUS')}</th>
            <th>${_('VIN')}</th>
			<th>${_('PLATES')}</th>
            <th>${_('MODEL')}</th>
            <th>${_('BRAND NAME')}</th>
            <th>${_('UNIT TYPE')}</th>
              <th>${_('OBSERVATIONS')}</th>

		</tr>
		</thead>
    % for item in allrecords:

    <tr id="${item['eco1']}">
        <td class="col_0"><span class="glyphicon glyphicon-pencil"></span></td>
         <td>${item['eco1']}</td>
        <td>${item['eco2']}</td>
         <td>${item['tarjeta']}</td>
        <td>${item['estatus']}</td>
         <td>${item['vin']}</td>
         <td>${item['placas']}</td>
        <td>${item['observaciones']}</td>
         <td>${item['marca']}</td>
        <td>${item['tipounidad']}</td>
        <td>${item['observaciones']}</td>


<%doc>        <td class="col_0">
     <a href="1/edit" class="btn btn-primary">

     </a>
     </td>

         <td>Diego</td>
         <td>Santillan</td></%doc>

    </tr>
        % endfor


</table>

</body>
</html>