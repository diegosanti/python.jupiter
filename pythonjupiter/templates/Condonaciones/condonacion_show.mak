<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
%if lencondouni>0:
<table class="userTable">
    <tr>
        <th>Condonacion</th>
        <th>Unidad</th>
    </tr>
    %for item in querycondonacionesuni:
        <tr>
        <td>${item['condonacion']}</td><td>${item['eco']}</td>
        </tr>
    %endfor
</table>

%else:
<table class="userTable">
    <tr>
        <th>Condonacion</th>
        <th>Operador</th>
    </tr>
    %for item in querycondonacionesope:
        <tr>
        <td>${item['condonacion']}</td><td>${item['nombre']}</td>
        </tr>
    %endfor
</table>
%endif