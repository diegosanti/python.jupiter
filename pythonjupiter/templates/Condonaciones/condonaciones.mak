<style type="text/css">

    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
    .ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
    .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
    .ui-timepicker-rtl dl dt{ float: right; clear: right; }
    .ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

    /* Shortened version style */
    .ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time,
    .ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
    .ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
    .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
    .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
    link { color: #0000EE; }

    .text{
        position:relative;
        left:190px;
        top:0px;
        width:290px;
        font-size:14px;
        color:black;

    }
    .imgContainer{
        float:left;
    }

    .azimuthimage {
        position: relative;
    }

    .azimuthimage .base {
       position: absolute;
       top: 0;
       left: 0;

       z-index : 0;
        width: 300px;
       height: 221px;
    }

    .azimuthimage .overlay{
       position: absolute;
       top: 55px;
       left:    81px;
       width: 105px;
       height: 105px;
    }

    div.dialog-hidden { display:none}
    div.redsquare    { border: solid 10px red; width: 67px; height: 10px; }
    div.orangesquare { border: solid 10px orange; width: 67px; height: 10px;}
    div.yellowsquare { border: solid 10px yellow; width: 67px; height: 10px;}
    div.greensquare  { border: solid 10px green; width: 67px; height: 10px; }
    div.bluesquare   { border: solid 10px blue; width: 67px; height: 10px; }
</style>
<script>
                          var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');


    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridCondonaciones"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name = '#jupiter_jqGridCondonaciones';
    var grid_pager= '#jupiter_PagerCondonaciones';
    var update_url='${h.url()}/condonacion/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_url  ='${h.url()}/condonacion/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';

    var addParams = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_url,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParams = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParams = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url,modal: true};
    var grid = jQuery(grid_name);
            $(document).ready(function () {
                grid.jqGrid({
                url: load_url,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Cancel')}','${_('Initial Date')}','${_('Final Date')}','${_('Description')}','${_('To')}','${_('Amount')}','${_('internal_id')}','${_('User')}','${_('Application_id')}'],
                colModel: [
                    {name: 'id_condonacion',index: 'id_condonacion', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'cancelado', index: 'cancelado',align: 'left',hidden: false,width:80,editable: true, formatter: getimage,edittype: 'text', editrules: {required: true},search:true},
                    {name: 'fechainicial',index: 'fechainicial',hidden:false,align: 'left',formatter: 'date',editable: true,edittype: 'date',editrules: {required: true},search:false},
                    {name: 'fechafinal',index: 'fechafinal',hidden:false,align: 'left',formatter: 'date',editable: true,edittype: 'date',editrules: {required: true},search:false},
                    {name: 'descripcion',index: 'descripcion', align: 'left',hidden: false,width:80,editable: true, edittype: 'text',editrules: {required: true},search:true},
                    {name: 'para',index: 'para', align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'monto',index: 'monto',hidden:false,formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                     {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:false},
                    {name: 'usuario',index: 'usuario',hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                     {name: 'application_id',index: 'application_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:false}



                ],
                pager: jQuery(grid_pager),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_condonacion',
                sortorder: "desc",
                viewrecords: true,
                autowidth: true,
                height: 250,
              ondblClickRow: function(rowId) {
                    doDoubleClickCondonacion(rowId)
                }
                //caption: header_container,
            });
            grid.jqGrid('navGrid',grid_pager,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:true},
                            editParams,
                            addParams,
                            //deleteParams,
                            searchParams,
                            viewParams);
            // add custom button
            grid.navButtonAdd(grid_pager,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                   onClickButton: function(){
                        doDoubleClickCon(0)
                    }
                });

              <%doc>
                grid.navButtonAdd(grid_pager,
                {
                    buttonicon: "ui-icon-closethick",
                    title: "${_('Cancel')}",
                    caption: "${_('Cancel')}",
                    position: "second",
                   onClickButton: function(){
                        cancelacondonacion()
                    }
                });
                </%doc>

            });
            $.extend($.jgrid.nav,{alerttop:1});


            function getimage(cellValue) {
                if(cellValue==0){
                    return "<img src=\"${h.url()}/img/check.png\" width=\"20px\" height=\"20px\"/>";
                }else{
                    return "<img src=\"${h.url()}/img/red-cross.png\"/ width=\"20px\" height=\"20px\">";
                }

              }

              <%doc>

            function cancelacondonacion() {
                var rowid = $("#jupiter_jqGridCondonaciones").jqGrid('getGridParam', 'selrow');
                var confirmando = confirm("¿Estas seguro de cancelar la condonacion " + rowid);
                if (confirmando) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/condonacion/cancel?id=' + rowid + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code

                            $('#jupiter_jqGridCondonaciones').trigger('reloadGrid');
                        },
                        error: function () {
                            $.alert("Error accessing server cancelacondonacion", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });

                }
            }</%doc>


            function getthecharge(cellvalue,options,rowObject) {
                var elid=rowObject[0];
                var quien=cellvalue;
                var thereturn;
                $.ajax({

                    type: "GET",
                        url: '${h.url()}/condonacion/getidcargo?quien='+quien+'&id='+elid+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            thereturn=parameterdata['cargo'];



                        },
                        error: function() {
                            $.alert("Error accessing server getthecharge",{type: "danger"});
                        },
                        complete: function() {

                        }

                        });
                return thereturn;
              }

          function getthenameeco(cellvalue,options,rowObject) {

                  var elid=rowObject[0];
                var quien=cellvalue;
                var thereturn;
              $.ajax({

                    type: "GET",
                        url: '${h.url()}/condonacion/getnameeco?quien='+quien+'&id='+elid+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            thereturn=parameterdata['to'];



                        },
                        error: function() {
                            $.alert("Error accessing server getthenameeco",{type: "danger"});
                        },
                        complete: function() {

                        }

                        });
                return thereturn;

                }


            function  doDoubleClickCon(rowId) {

                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/condonacion/form?id='+rowId+'&application_id=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#jupiter_dialogcondonaciones" ).html(parameterdata.dialogtemplate);
                            $("#jupiter_dialogcondonaciones").show();

                            $("#jupiter_condonacionForm").validate({
                                rules: {
                                    jupitercondonacion_monto: {
                                        required: true,min: 0,max: 21450000
                                    }
                                    ,
                                    jupitercondonacion_descripcion: {
                                        required: true
                                    },
                                     jupitercondonacion_startdate: {
                                        required: true
                                    },
                                       jupitercondonacion_finaldate: {
                                        required: true
                                    }
                                }
                            });
                        },
                        error: function() {
                            $.alert("Error accessing server /condonacion/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#jupiter_dialogcondonaciones" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.64),
                            width: Math.round(window.innerWidth*.75),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#jupiter_condonacionForm").valid()) {

                                        var jupiter_condonacion_id=$('#jupiter_condonacion_id').val()
                                        var para=$('#jupitercondonacion_para').val();
                                        var opeuni=$('#selectunidades').val();
                                        var theapp_name=$('#selectcondonacionapp_name').val();
                                        var to=$('#selectcondonacionedg').val();
                                        var descripcion=$('#jupitercondonacion_descripcion').val();
                                        var monto=$('#jupitercondonacion_monto').val();
                                        var fechaini=$('#jupitercondonacion_startdate').val();
                                        var fechafin=$('#jupitercondonacion_finaldate').val();
                                        var cargo=$('#selectcondonacioncargo').val();

                                        var internal_id=$('#jupitercondonacion_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/condonacion/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'para': para,'descripcion': descripcion,'monto': monto,'fechaini': fechaini,'to': to,
                                                    'fechafin': fechafin,'cargo': cargo,'theapp_name': theapp_name,
                                                    'selectunidades': opeuni,'user': '${user}','internal_id': internal_id,'jupiter_condonacion_id':jupiter_condonacion_id,'application_id': '${application_id}'},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if(data['error']=='ok'){
                                                    ContDialog.dialog( "close" );
                                                    $('#jupiter_condonacionForm')[0].reset();
                                                    $('#jupiter_jqGridCondonaciones').trigger( 'reloadGrid' );
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /condonacion/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });


                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#jupiter_condonacionForm')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#jupiter_condonacionForm')[0].reset();
                            }
                        });

                    ContDialog.dialog( "open" );
            }




    function  doDoubleClickCondonacion(rowId) {

                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/condonacion/showunitsoroperators?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#jupiter_dialogcondonaciones" ).html(parameterdata.template);
                            $("#jupiter_dialogcondonaciones").show();

                        },
                        error: function() {
                            $.alert("Error accessing server /condonacion/showunitsoroperators",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#jupiter_dialogcondonaciones" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.64),
                            width: Math.round(window.innerWidth*.75),
                            modal: true,
                            buttons: {
                                "${_('Close')}": function() {
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {

                            }
                        });

                    ContDialog.dialog( "open" );
            }

            </script>
</div>
    <!-- page start-->
<div id="jupiter_dialogcondonaciones"  title="${_('Condonacion')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jupiter_jqGridCondonaciones" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="jupiter_PagerCondonaciones" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->