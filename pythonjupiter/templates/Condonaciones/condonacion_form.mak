<script>
    
    function esvisible(esto) {
            if(esto.id=='radiouna'){
                document.getElementById("selectunidades").style.visibility='visible';
                document.getElementById("tablaapp_nameandall").style.display='none';
            }else{
                 document.getElementById("selectunidades").style.visibility='hidden';
                 document.getElementById("tablaapp_nameandall").style.display='block';
            }

      }

    function cambioaquiencondonacion() {
        var what=document.getElementById("jupitercondonacion_para").value;
        var appname=document.getElementById("selectcondonacionapp_name").value;
        var to=document.getElementById("selectcondonacionedg").value;

        if(what!='Unidad' && what!='Operador'){
            document.getElementById("trapp_name").style.display='block';
             document.getElementById("trunidadoperador").style.visibility='hidden';
        }else{
            document.getElementById("trapp_name").style.display='none';
            document.getElementById("trunidadoperador").style.visibility='visible';

        }
        $.ajax({
                type: "GET",
                url: "${h.url()}/condonacion/actualizaselects?to="+to+"&appname="+appname+"&what="+what+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                contentType: "application/json; charset=utf-8",
                data: {},
                success: function (parameterdata) {
                    $('#selectcondonacioncargo option').remove();
                    $('#selectunidades option').remove();
                     $('#selectcondonacionedg option').remove();

                    for(var k in parameterdata['unidades']){
                        $('#selectunidades').append("<option>"+parameterdata['unidades'][k]['eco1']+"</option>");
                    }


                    for(var k in parameterdata['operadores']){
                        $('#selectunidades').append("<option>"+parameterdata['operadores'][k].nombre+"</option>");
                    }

                    for(var k in parameterdata['cargos']){
                        $('#selectcondonacioncargo').append("<option>"+parameterdata['cargos'][k]['cargo']+"</option>");
                    }

                    for (var k in parameterdata['empresas']) {
                            $('#selectcondonacionedg').append("<option>" + parameterdata['empresas'][k].nombre_linea + "</option>");
                        }

                    for (var k in parameterdata['derroteros']) {
                            $('#selectcondonacionedg').append("<option>" + parameterdata['derroteros'][k].derrotero + "</option>");
                        }

                    for (var k in parameterdata['grupos']) {
                            $('#selectcondonacionedg').append("<option>" + parameterdata['grupos'][k].grupo + "</option>");
                        }
                //cambiarselects();
                },
                error: function () {
                alert("ERROR cambioaquiencondonacion");
                },
                complete: function () {
                }
        });

      }

      function actualizacargos() {
            var para=document.getElementById("jupitercondonacion_para").value;
                var what=document.getElementById("selectunidades").value;
                  $.ajax({
                type: "GET",
                url: "${h.url()}/condonacion/actualizacargos?para="+para+"&what="+what+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                contentType: "application/json; charset=utf-8",
                data: {},
                success: function (parameterdata) {
                    $('#selectcondonacioncargo option').remove();

                    for(var k in parameterdata['cargos']){
                        $('#selectcondonacioncargo').append("<option>"+parameterdata['cargos'][k]['cargo']+"</option>");
                    }
                //actualizacargos();
                },
                error: function () {
                alert("ERROR actualizacargos");
                },
                complete: function () {
                }
        });
        }

        function buscarto(e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                var para = document.getElementById("jupitercondonacion_para").value;
                var what = document.getElementById("jupiter_enterwho").value;
                $.ajax({
                    type: "GET",
                    url: "${h.url()}/condonacion/buscarto?para=" + para + "&what=" + what + "&internal_id=${internal_id}&user=${user}&app_name=${app_name}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (parameterdata) {
                        $('#selectunidades option').remove();

                        for (var k in parameterdata['resultadosuni']) {
                            $('#selectunidades').append("<option>" + parameterdata['resultadosuni'][k]['eco'] + "</option>");
                        }

                        for (var k in parameterdata['resultadosope']) {
                            $('#selectunidades').append("<option>" + parameterdata['resultadosope'][k]['nombre'] + "</option>");
                        }


                        actualizacargos();
                    },
                    error: function () {
                        alert("ERROR buscarto");
                    },
                    complete: function () {
                    }
                });

            }
        }
        function cambiacargos() {
        var theapp_name=document.getElementById("selectcondonacionapp_name").value;
        var to=document.getElementById("selectcondonacionedg").value;
        var para=document.getElementById("jupitercondonacion_para").value;
            $.ajax({
                type: "GET",
                url: "${h.url()}/condonacion/actualizacargosconchange?theapp_name="+theapp_name+"&para="+para+"&to="+to+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}",
                contentType: "application/json; charset=utf-8",
                data: {},
                success: function (parameterdata) {
                    $('#selectcondonacioncargo option').remove();

                    for(var k in parameterdata['cargos']){
                        $('#selectcondonacioncargo').append("<option>"+parameterdata['cargos'][k]['cargo']+"</option>");
                    }
                //actualizacargos();
                },
                error: function () {
                alert("ERROR actualizacargos");
                },
                complete: function () {
                }
                });
          }


          function condicionar() {
                var app=document.getElementById("jupitercondonacion_para").value;
                if(app=='Empresa'){
                    validarlineas();
                }
                if(app=='Derrotero'){
                    validarderroteros();
                }
                if(app=='Grupo'){
                    validargrupo();
                }

            }

    function validarlineas() {
               var empresa=document.getElementById("selectcondonacionapp_name").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizalineas"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#selectcondonacionedg option').remove();
                    for (var k in parameterdata['lineas']) {
                         $("#selectcondonacionedg").append('<option value="' + parameterdata['lineas'][k]['nombre_linea'] + '">' + parameterdata['lineas'][k]['nombre_linea'] + '</option>');
                    }
                    cambiacargos();

                },
                error: function () {
                        alert("ERROR");
                },
                complete: function () {
                 }
                });

              }



    function validarderroteros() {
               var empresa=document.getElementById("selectcondonacionapp_name").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizaderrotero"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#selectcondonacionedg option').remove();
                    for (var k in parameterdata['derroteros']) {
                         $("#selectcondonacionedg").append('<option value="' + parameterdata['derroteros'][k]['derrotero'] + '">' + parameterdata['derroteros'][k]['derrotero'] + '</option>');
                    }

                    cambiacargos();
                },
                error: function () {
                        alert("ERROR");
                },
                complete: function () {
                 }
                });

              }



    function validargrupo() {
               var empresa=document.getElementById("selectcondonacionapp_name").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizagrupos"+"?empresa="+empresa+"&internal_id=${internal_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#selectcondonacionedg option').remove();
                    for (var k in parameterdata['grupos']) {
                         $("#selectcondonacionedg").append('<option value="' + parameterdata['grupos'][k]['grupo'] + '">' + parameterdata['grupos'][k]['grupo'] + '</option>');
                    }
                    cambiacargos();


                },
                error: function () {
                        alert("ERROR grupo");
                },
                complete: function () {
                 }
                });

              }

</script>


<form id="jupiter_condonacionForm" name="jupiter_condonacionForm" action="${h.url()}/condonacion/save">
 <input hidden type="text" id="jupiter_condonacion_id" value='${id}'>
  <table cellspacing="10">
      <tr>
          <td>${_('For: ')}</td><td><select id="jupitercondonacion_para" name="jupitercondonacion_para" onchange="cambioaquiencondonacion();">
                                        <option>${_('Unidad')}</option>
                                        <option>${_('Operador')}</option>
                                        <option>${_('Empresa')}</option>
                                        <option>${_('Derrotero')}</option>
                                        <option>${_('Grupo')}</option>
                                        </select></td>
      </tr>
      <tr id="trunidadoperador">
          <td>Unidad/Operador</td>
          <td><input type="text" id="jupiter_enterwho" onkeypress="buscarto(event);"></td>
            <td><select id="selectunidades" name="selectunidades"  onchange="actualizacargos();">
                %for item in unidades:
                <option>${item.eco1}</option>
                %endfor
            </select></td>

      </tr>
  </table>
      <table id="trapp_name" style="display: none;">
        <tr>
        <td>${_('App_name: ')}</td><td><select id="selectcondonacionapp_name" name="selectcondonacionapp_name" onchange="condicionar();">
                                        %for item in apps_names:
                                            %if item['application_name']!='TODO':
                                                <option>${item['application_name']}</option>
                                            %endif

                                        %endfor
                                    </select></td>
          </tr>

          <tr>
          <td>${_('To: ')}</td><td><select id="selectcondonacionedg" name="selectcondonacionedg" onchange="cambiacargos();">

                                            <option>${empresas.nombre_linea}</option>


                                </select></td>
            </tr>
      </table>
    <br><br>
<table>
      <tr style="height: 10px">
        <td>${_('Description: ')}</td><td><textarea id="jupitercondonacion_descripcion" name="jupitercondonacion_descripcion"></textarea></td>
      </tr>
      <tr>
        <td>${_('Amount: ')}</td><td><input type="number" id="jupitercondonacion_monto" name="jupitercondonacion_monto" min="0" max="2145000000"></td>
      </tr>
      <tr>
        <td>${_('Start Date')}</td><td><input type="date" id="jupitercondonacion_startdate" name="jupitercondonacion_startdate"> </td><td></td>
          <td>${_('Final Date')}</td><td><input type="date" id="jupitercondonacion_finaldate" name="jupitercondonacion_finaldate"> </td>
      </tr>
      <tr>
        <td>${_('Charge: ')}</td><td><select id="selectcondonacioncargo" name="selectcondonacioncargo">
                                        %for item in cargos:
                                            <option>${item['cargo']}</option>
                                        %endfor
                                    </select></td>
      </tr>
  </table>

    <table>



  </table>

      <input type="text" hidden id="jupitercondonacion_internal_id" name="jupitercondonacion_internal_id" value='${internal_id}'>
</form>