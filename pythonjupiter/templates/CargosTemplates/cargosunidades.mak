
<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>
<script>

                          var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');

         function openWindowcargounidades(text,url){
            doAjax(url,'openTab');
            var $dialog = $('<div></div>')
               .html('<div id="openTab" style="margin:10px"></div>')
               .dialog({
                   autoOpen: false,
                   modal: true,
                   height:  780,
                   width: 820,
                   resizable: false,
                   draggable: true,
                   position : { my: "center", at: "top", of: window },
                   close: function(){
                       $(this).dialog('destroy').remove();
                       //Dialog03templatehabilita.dialog("close");
                       reloadallcargounidad();
                   },
                   title: text,
                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                   buttons: {
      <%doc>                 "Close": function ()
                       {
                           $(this).dialog('destroy').remove();
                           Dialog03templatehabilita.dialog("close");
                           reloadallpersonas();


                       }</%doc>
                   }
               });
        $dialog.dialog('open');
        }




            function reloadallcargounidad() {
           document.getElementById("jupiter_filtercargouni").value="";
            document.getElementById("jupiter_filtercargouni").placeholder="CargoUnidad";
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosunidades/reloadcargosunidades?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablecargounidad td').remove();
                      $("#bookmarkcargounidades").html(data['contador']+ " Cargo(s)");

                        for(var k in data['listabusca']){
                                                            var anexando;
                            if(data['listabusca'][k].tipodecargo==1) {
                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Administrativo')}</a></td>';

                            }
                            if (data['listabusca'][k].tipodecargo==2) {

                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Cuenta')}</a></td>';
                            }
                            if (data['listabusca'][k].tipodecargo==3) {
                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Por Dia Trabajado')}</a></td>';
                            }

                            $('#jupiter_tablecargounidad').append('<tr><td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['cargo'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['frecuencia'] + '</a></td>' +
                                            anexando+
                                              '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['montodefault'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['app_name']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['empresa']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['derrotero']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['grupo']+ '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_cargounidad']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing reloadcargounidad", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }

       function searchcargounidad(){
           var name=document.getElementById("jupiter_filtercargouni").value;
           $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosunidades/searchcargounidad?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablecargounidad td').remove();
                      $("#bookmarkcargounidades").html(data['contador']+ " Cargo(s)");

                        for(var k in data['listabusca']){
                                               var anexando;
                            if(data['listabusca'][k].tipodecargo==1) {
                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Administrativo')}</a></td>';

                            }
                            if (data['listabusca'][k].tipodecargo==2) {

                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Cuenta')}</a></td>';
                            }
                            if (data['listabusca'][k].tipodecargo==3) {
                                anexando='<td> <a onclick="openWindowcargounidades(\'Editar\',\'${h.url()}/cargosunidades/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');">${_('Por Dia Trabajado')}</a></td>';
                            }

                            $('#jupiter_tablecargounidad').append('<tr><td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['cargo'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['frecuencia'] + '</a></td>' +
                                            anexando+
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['montodefault'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['app_name']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['empresa']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['derrotero']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargounidades(\'Editar \',\'${h.url()}\/cargosunidades\/editcargo?id='+data['listabusca'][k]['id_cargounidad']+'\');\">' + data['listabusca'][k]['grupo']+ '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_cargounidad']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing searchcargounidad", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });


       }

    function eliminar(id) {
    var confirmando=confirm("¿Seguro que deseas eliminar este Cargo a la Unidad?");
    if(confirmando){
                   $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosunidades/eliminacargounidad?id="+id+"&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {
                        alert(data['error']);
                        reloadallcargounidad();


                    },
                    error: function () {
                        $.alert("Error no se puede eliminar", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
    }

  }


function searchentercargouni(e) {
     if (e.keyCode === 13 && !e.shiftKey)  {
         searchcargounidad();
     }
  }


</script>

<button style="float: left;" onclick="openWindowcargounidades('Nuevo Cargo','${h.url()}/cargosunidades/nuevocargo');">
    <span class="glyphicon glyphicon-plus">${_('New')}</span>
</button>
<br><br>


  <input type="text" id="jupiter_filtercargouni" onkeypress="searchentercargouni(event);" placeholder="CargoUnidad"><button onclick=searchcargounidad()><i class="fa fa-search"></i></button><button onclick="reloadallcargounidad()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    &nbsp;&nbsp;<a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: none;" ></a>

<div id="bookmarkcargounidades" class="bookmark" style="float: right;">${contador} Cargo(s)</div>
<br>


<table id="jupiter_tablecargounidad" class="userTable">
    <tr>
        <th></th>
        <th>${_('NAME')}</th>
        <th>${_('FREQUENCY')}</th>
        <th>${_('TYPE')}</th>
        <th>${_('AMOUNT')}</th>
        <th>${_('APP_NAME')}</th>
        <th>${_('EMPRESA')}</th>
        <th>${_('DERROTERO')}</th>
        <th>${_('GRUPO')}</th>
        <th>${_('DELETE')}</th>
    </tr>
    %for item in allrecords:

    <tr>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');"><span class="glyphicon glyphicon-edit"></span></a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.cargo}</a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.frecuencia}</a></td>
        %if item.tipodecargo==1:
         <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${_('Administrativo')}</a></td>
        %elif item.tipodecargo==2:
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${_('Cuenta')}</a></td>
        %elif item.tipodecargo==3:
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${_('Por Dia Trabajado')}</a></td>
        %endif
         <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.montodefault}</a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.app_name}</a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.empresa}</a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.derrotero}</a></td>
        <td> <a onclick="openWindowcargounidades('Editar','${h.url()}/cargosunidades/editcargo?id=${item.id_cargounidad}');">${item.grupo}</a></td>
        <td><a onclick="eliminar(${item.id_cargounidad});"><span class="glyphicon glyphicon-trash"></span></a></td>
    </tr>
        %endfor

</table>