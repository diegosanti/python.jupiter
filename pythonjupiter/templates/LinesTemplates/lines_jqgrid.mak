<script>
    $(window).on("resize", function () {
            var $gridLine = $("#jqGridLineJupiter"),
                newWidth = $gridLine.closest(".ui-jqgrid").parent().width();
                $gridLine.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameLine = '#jqGridLineJupiter';
    var grid_pagerLine = '#listPagerLineJupiter';
    var update_urlLine='${h.url()}/lineas/update?internal_id=${internal_id}&application_id=${application_id}';
    var load_urlLine  ='${h.url()}/lineas/load?&internal_id=${internal_id}&application_id=${application_id}';

    var addParamsLine = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlLine,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsLine = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlLine,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsLine = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlLine,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsLine = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlLine,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsLine = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlLine,modal: true};
    var gridLine = jQuery(grid_nameLine);
            $(document).ready(function () {
                gridLine.jqGrid({
                url: load_urlLine,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Line')}', '${_('internal_id')}', '${_('app_id')}', '${_('Application')}', '${_('unidades')}'],
                colModel: [
                    {name: 'id_linea',index: 'id_linea', align: 'center',key:true,hidden: false,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'nombre_linea', index: 'nombre_linea',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'internal_id', index: 'internal_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_id', index: 'app_id',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: true}},
                    {name: 'app_name',index: 'app_name', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: true}},
                    {name: 'unidades',index: 'unidades', align: 'left',hidden: true,editable: true, edittype: 'text',editrules: {required: false}}
                ],
                pager: jQuery(grid_pagerLine),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_linea',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                ondblClickRow: function(rowId) {
                    doDoubleClickJupiterLine(rowId)
                }
                //caption: header_container,
            });
            gridLine.jqGrid('navGrid',grid_pagerLine,{edit:false,add:false,del:false, search:true},
                            editParamsLine,
                            addParamsLine,
                            deleteParamsLine,
                            searchParamsLine,
                            viewParamsLine);
            // add custom button
            gridLine.navButtonAdd(grid_pagerLine,
                {
                    buttonicon: "ui-icon-trash",
                    title: "${_('Delete')}",
                    caption: "",
                    position: "first",
                    onClickButton: function(){
                        deleteClickJupiterLine(0)
                    }
                });
            gridLine.navButtonAdd(grid_pagerLine,
                {
                    buttonicon: "ui-icon-plus",
                    title: "${_('Add')}",
                    caption: "${_('Add')}",
                    position: "first",
                    onClickButton: function(){
                        doDoubleClickJupiterLine(0)
                    }
                });

            });
            $.extend($.jgrid.nav,{alerttop:1});
            function deleteClickJupiterLine() {
                var rowid = $('#jqGridLineJupiter').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/lineas/delete?id='+rowid+'&internal_id=${internal_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(data) {
                            //Insert HTML code
                            if(data.error=="ok"){
                                $.alert("${_('Deleted')}",{type: "success"});
                                $('#jqGridLineJupiter').trigger( 'reloadGrid' );
                            }else{
                                $.alert(data.error,{type: "warning"});
                            }
                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /lineas/delete",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });
                }
                else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
            function doDoubleClickJupiterLine(rowId) {
                    $.ajax({
                        type: "GET",
                        url: '${h.url()}/lineas/form?id='+rowId+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&app_id=${application_id}',
                        contentType: "application/json; charset=utf-8",
                        data: { },
                        success: function(parameterdata) {
                            //Insert HTML code
                            $( "#dialogLineJupiter" ).html(parameterdata.dialogtemplate);
                            $("#dialogLineJupiter").show();

                        },
                        error: function() {
                            $.alert("${_('Error accessing server')} /lineas/form",{type: "danger"});
                        },
                        complete: function() {
                        }
                    });

                    var ContDialog = $( "#dialogLineJupiter" ).dialog({
                            autoOpen: false,
                            height: Math.round(window.innerHeight*.3),
                            width: Math.round(window.innerWidth*.35),
                            modal: true,
                            buttons: {
                                "${_('Add')}": function() {
                                    if($("#LineFormJupiter").valid()) {
                                        var id = $('#jupiterLine_id').val();
                                        var nombre_linea = $('#jupiterLine_nombre_linea').val();
                                        var app_id = $('#jupiterLine_app_id').val();
                                        var app_name = $('#jupiterLine_app_name').val();
                                        var internal_id = $('#jupiterLine_internal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/lineas/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'id':id,'nombre_linea':nombre_linea,'app_id':app_id,'app_name':app_name,'internal_id':internal_id},
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                if (data.error == "ok")
                                                {
                                                    $.alert("${_('Done')}!", { autoClose:true,type: "success"});
                                                    $('#jqGridLineJupiter').trigger( 'reloadGrid' );
                                                }else{
                                                    $.alert(data.error, { autoClose:true,type: "warning"});
                                                }

                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /lineas/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#LineFormJupiter')[0].reset();
                                        ContDialog.dialog( "close" );
                                    }
                                },
                                "${_('Close')}": function() {
                                    $('#LineFormJupiter')[0].reset();
                                    ContDialog.dialog( "close" );
                                }
                            },
                            close: function() {
                                $('#LineFormJupiter')[0].reset();
                            }
                        });
                    ContDialog.dialog( "open" );
            }
</script>
</div>
    <!-- page start-->
<div id="dialogLineJupiter"  title="${_('Line')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridLineJupiter" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerLineJupiter" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->