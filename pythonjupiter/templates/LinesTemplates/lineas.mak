
<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
 <%doc>   <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script></%doc>

   <style type="text/css">
        a{
           color: black;
           text-decoration: none;
       }
   </style>

<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>

   <script type="text/javascript">

            var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');

       function searchenterlineas(e) {

    if (e.keyCode === 13 && !e.shiftKey)  {

        searchlines();

         }
       }

       function openWindowlines(text,url){
            doAjax(url,'openTab');
            var $dialog = $('<div></div>')
               .html('<div id="openTab" style="margin:10px"></div>')
               .dialog({
                   autoOpen: false,
                   modal: true,
                   height:  580,
                   width: 820,
                   resizable: false,
                   draggable: true,
                   position : { my: "center", at: "top", of: window },
                   close: function(){
                       $(this).dialog('destroy').remove();
                       reloadalllines();
                   },
                   title: text,
                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                   buttons: {

                   }
               });
        $dialog.dialog('open');
        }

        function reloadalllines() {
           document.getElementById("jupiter_filterline").value="";
            document.getElementById("jupiter_filterline").placeholder="empresas";
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/lineas/reloadlinea?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablelines td').remove();
                      $("#bookmarkempresas").html(data['contador']+ " Empresa(s)");

                        for(var k in data['listabusca']){

                            $('#jupiter_tablelines').append('<tr><td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\">' + data['listabusca'][k]['id_linea'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\">' + data['listabusca'][k]['nombre_linea'] + '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_linea']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing searchlinea", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }

       function searchlines(){
           var name=document.getElementById("jupiter_filterline").value;
           $.ajax({
                    type: "GET",
                    url: "${h.url()}/lineas/searchlinea?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablelines td').remove();
                       $("#bookmarkempresas").html(data['contador']+ " Empresa(s)");

                        for(var k in data['listabusca']){
                            $('#jupiter_tablelines').append('<tr><td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\">' + data['listabusca'][k]['id_linea'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowlines(\'Editar '+data['listabusca'][k]['nombre_linea']+'\',\'${h.url()}\/lineas\/editlinea?id='+data['listabusca'][k]['id_linea']+'\');\">' + data['listabusca'][k]['nombre_linea'] + '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_linea']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing searchlinea", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });


       }


function eliminar(id) {
    var confirmando=confirm("¿Seguro que deseas eliminar esta empresa?");
    if(confirmando){
                   $.ajax({
                    type: "GET",
                    url: "${h.url()}/lineas/eliminalinea?id="+id+"&user=${user}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {
                        //alert(data['error']);
                        reloadalllines();


                    },
                    error: function () {
                        $.alert("Error no se puede elimina", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
    }

  }


   </script>


<!--main content start-->


                    <br>


                    <button style="float: left;" onclick="openWindowlines('Nueva Empresa','${h.url()}/lineas/newlinea?user=${user}')">
                       <span class="glyphicon glyphicon-plus">Nueva</span></button>
                    <br><br>
<input type="text" id="jupiter_filterline" onkeypress="searchenterlineas(event);" placeholder="empresas"><button onclick=searchlines()><i class="fa fa-search"></i></button><button onclick="reloadalllines()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    &nbsp;&nbsp;<a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: none;" ></a>

<div id="bookmarkempresas" class="bookmark" style="float: right;">${contador} Empresa(s)</div>
                    <table id="jupiter_tablelines" class="userTable">
                        <tr>
                            <th></th>
                        % for it in list:
                            %if it['show']=='nombre_linea':
                              <th>${_('Company')}</th>
                            %else:
                            <th>${it['show']}</th>
                            %endif
                        %endfor
                            <th>${_('Action')}</th>
                        </tr>

                        % for it in hd:

                        <tr>
                            <td> <a onclick="openWindowlines('Editar ${it['nombre_linea']}','${h.url()}/lineas/editlinea?id=${it['id_linea']}'); "><span class="glyphicon glyphicon-edit"></span></a></td>
                             % for i in list:
                                <td>
                                       <a onclick="openWindowlines('Editar ${it['nombre_linea']}','${h.url()}/lineas/editlinea?id=${it['id_linea']}');">${it[i['show']]}</a></td>


                             %endfor
                                <td>
                                    <a onclick="eliminar(${it['id_linea']});"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>

                        %endfor
                    </table><br><br>


              <br>

      <!-- page start-->
      <!-- page end-->
<br>

