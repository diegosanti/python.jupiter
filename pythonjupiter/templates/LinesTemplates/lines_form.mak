<form id="LineFormJupiter">
    <input hidden type="text" id="jupiterLine_id" value='${id}'>
        <table style="width:100%">
            <tr>
                <td>
                    ${_('Name')}:
                </td>
                <td colspan="5">
                    <input type="text" id="jupiterLine_nombre_linea" name="jupiterLine_nombre_linea" maxlength="35" required value='${handler.nombre_linea}'>
                </td>
            </tr>
        </table>
        <input type="text" hidden id="jupiterLine_internal_id" name="jupiterLine_internal_id" value='${internal_id}'>
        <input type="text" hidden id="jupiterLine_app_id" name="jupiterLine_app_id" value='${app_id}'>
        <input type="text" hidden id="jupiterLine_app_name" name="jupiterLine_app_name" value='${app_name}'>
</form>