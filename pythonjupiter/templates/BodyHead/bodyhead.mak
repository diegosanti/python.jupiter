<script>
    $(window).on("resize", function () {
            var $grid = $("#jupiter_jqGridbodyhead"),
                newWidth = $grid.closest(".ui-jqgrid").parent().width();
                $grid.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_name_bodyhead = '#jupiter_jqGridbodyhead';
    var grid_pager_bodyhead= '#jupiter_Pagerbodyhead';
    var update_url_bodyhead='${h.url()}/bodyhead/update?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}';
    var load_url_bodyhead  ='${h.url()}/bodyhead/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&soy=${soy}';

    var addParams_bodyhead = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_url_bodyhead,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParams_bodyhead = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_url_bodyhead,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    //var deleteParams = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url_bodyhead,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParams_bodyhead = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_url_bodyhead,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParams_bodyhead = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_url_bodyhead,modal: true};
    var grid_bodyhead = jQuery(grid_name_bodyhead);
            $(document).ready(function () {
                grid_bodyhead.jqGrid({
                url: load_url_bodyhead,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}','${_('Date')}','${_('Status')}','${_('Unit')}','${_('Operator')}','${_('Amount')}','${_('Pagado')}','${_('Faltante')}','${_('Internal_id')}',
                    '${_('App_name')}','${_('Application_id')}'],
                colModel: [
                    {name: 'id_head',index: 'id_head', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: true, edittype: 'text', editrules: {required: true},search:true},
                    {name: 'estatus',index: 'estatus', align: 'left',hidden: false,width:80,editable: true, edittype: 'text',editrules: {required: true},search:true},
                    {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getunidad,editrules: {required: true},search:true},
                    {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getoperador,editrules: {required: true},search:true},
                    {name: 'porpagar',index: 'porpagar', formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'faltante',index: 'faltante',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "}, align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                    {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                    {name: 'app_name',index: 'app_name',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                    {name: 'application_id',index: 'application_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true}


                ],
                pager: jQuery(grid_pager_bodyhead),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id_head',
                sortorder: "desc",
                viewrecords: true,
                autowidth: true,
                height: 250,
             <%doc>       ondblClickRow: function(rowId) {
                    doDoubleClickAbono2(rowId)
                },</%doc>
                subGrid : true,
	            subGridRowExpanded: function(subgrid_id, row_id,rowObject) {
		            // we pass two parameters
		            // subgrid_id is a id of the div tag created whitin a table data
		            // the id of this elemenet is a combination of the "sg_" + id of the row
		            // the row_id is the id of the row
		            // If we wan to pass additinal parameters to the url we can use
		            // a method getRowData(row_id) - which returns associative array in type name-value
		            // here we can easy construct the flowing
		            var subgrid_table_id, pager_id;
		            subgrid_table_id = subgrid_id+"_t";
		            pager_id = "p_"+subgrid_table_id;
		            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
		            jQuery("#"+subgrid_table_id).jqGrid({
			        url:"${h.url()}/bodyhead/loadbody?id_head="+row_id,
                        datatype: 'json',
                    mtype: 'GET',
			        colNames: ['${_('ID')}','${_('Date')}','${_('Unit')}','${_('Charge Unit')}','${_('Operator')}','${_('Charge Operator')}','${_('id_head')}',
                            '${_('Amount')}','${_('Condonacion')}','${_('Saldo')}','${_('Pagado')}','${_('Internal_id')}','${_('Espoliza')}','${_('Application_id')}'],
			        colModel: [
                        {name: 'id_body',index: 'id_body', align: 'center',key:true,hidden: false,width:28, editable: false,edittype: 'text',editrules: {required: true},search:false},
                        {name: 'fecha', index: 'fecha',align: 'left',formatter: 'date',hidden: false,width:80,editable: true, edittype: 'text', editrules: {required: true},search:true},
                        {name: 'id_unidad',index: 'id_unidad',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getunidad,editrules: {required: true},search:true},
                        {name: 'id_cargounidad',index: 'id_cargounidad',hidden:false,align: 'left',editable: true,formatter: getcargounidad,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_operador',index: 'id_operador',hidden:false,align: 'left',editable: true,edittype: 'date',formatter: getoperador,editrules: {required: true},search:true},
                        {name: 'id_cargopersona',index: 'id_cargopersona',hidden:false,align: 'left',editable: true,formatter: getcargopersona,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'id_head',index: 'id_head',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'monto',index: 'monto', formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},align: 'left',hidden: false,editable: true, edittype: 'text',editrules: {required: false},search:true},
                        {name: 'condonacion',index: 'condonacion',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'saldo',index: 'saldo',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'pagado',index: 'pagado',formatter: 'currency',formatoptions:{decimalSeparator:".",prefix: "$ "},hidden:false,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'internal_id',index: 'internal_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'espoliza',index: 'espoliza',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true},
                        {name: 'application_id',index: 'application_id',hidden:true,align: 'left',editable: true,edittype: 'date',editrules: {required: true},search:true}


			            ],
                        rowNum:20,
		   	            pager: pager_id,
                        sortname: 'id_body',
		                sortorder: "asc",
		                height: '100%'
		            });
		            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false,search:false})
	            },
	                subGridRowColapsed: function(subgrid_id, row_id) {
		                // this function is called before removing the data
		                //var subgrid_table_id;
		                //subgrid_table_id = subgrid_id+"_t";
		                //jQuery("#"+subgrid_table_id).remove();
	                }
                //caption: header_container,
            });
            grid_bodyhead.jqGrid('navGrid',grid_pager_bodyhead,{edit:false,add:false,del:false,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:false},
                            editParams_bodyhead,
                            addParams_bodyhead,
                            //deleteParams,
                            searchParams_bodyhead,
                            viewParams_bodyhead);
            // add custom button

            });
            $.extend($.jgrid.nav,{alerttop:1});

<%doc>
  <%doc>          function doDoubleClickAbono2(rowid) {

                var winHeight = Math.round(window.innerHeight * .85);
                var winWidth = Math.round(window.innerWidth * .55);

                var addFilter01Buttons = {
                              "${_('Add')}": function() {
                                    if($("#jupiter_AbonoForm").valid()) {
                                        var pago=$('#jupiter_pago').val();
                                        var elid=$('#jupiter_elidhead').val();
                                        var internal_id=$('#jupiter_elinternal_id').val();
                                        $.ajax({
                                            type: "GET",
                                            url: "${h.url()}/bodyhead/save",
                                            contentType: "application/json; charset=utf-8",
                                            data: {'elid': elid,'jupiter_pago': pago,'user': '${user}','internal_id': internal_id
                                                    },
                                            success: function(data) {
                                                // data.value is the success return json. json string contains key value
                                                 $('#jupiter_jqGridbodyhead').trigger( 'reloadGrid' );
                                                 if(data['error']=='ok'){
                                                      Dialog03.dialog( "close" );
                                                 }
                                            },
                                            error: function() {
                                            //alert("#"+ckbid);
                                                 $.alert("${_('Error accessing to')} /abono/save", { autoClose:false,type: "danger"});
                                                return true;
                                            },
                                            complete: function() {
                                            }
                                            });
                                        $('#jupiter_AbonoForm')[0].reset();

                                    }
                                },

                        "Cancel": function () {

                        Dialog03.dialog("close");
                    }
                };
        var Dialog03 = $("#dialogForm03abonar").dialog({
            autoOpen: false,
            height: winHeight,
            width: winWidth,
            modal: true,
            buttons: addFilter01Buttons,
            close: function () {

            }
        });</%doc><%doc>

        // Append html
        $.ajax({
            type: "GET",
            url: '${h.url()}/bodyhead/templateabonar?id='+rowid+'&internal_id=${internal_id}&user=${user}',
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
                $("#dialogForm03abonar").html(parameterdata.template);
                Dialog03.data('rowId', 1);
                Dialog03.dialog("open");

            },
            error: function () {
                alert("Error accessing server /templateabonar")
            },
            complete: function () {
            }
        });
              }</%doc>


              function getunidad(cellvalue,options,rowObject) {
                  var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getunidad?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['unidad'];



                        },
                        error: function () {
                            $.alert("Error accessing server getunidad", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                }



    function getoperador(cellvalue,options,rowObject) {
                  var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getoperador?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['operador'];



                        },
                        error: function () {
                            $.alert("Error accessing server getoperador", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                }

                function getcargounidad(cellvalue,row_id) {
                    var thereturn;
                    var espoliza;

                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/espolizasiono?id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&row_id='+row_id.rowId,
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                espoliza = parameterdata['poliza'];



                        },
                        error: function () {
                            $.alert("Error accessing server espolizasiono", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });

                                 if(espoliza=='SI'){

                                    return 'POLIZA';
                                }



                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getcargounidad?id='+cellvalue+'&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&row_id='+row_id.rowId,
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['cargounidad'];



                        },
                        error: function () {
                            $.alert("Error accessing server getcargounidad", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                  }


    function getcargopersona(cellvalue) {
        var thereturn;
                if(cellvalue!=0) {
                    $.ajax({

                        type: "GET",
                        url: '${h.url()}/bodyhead/getcargopersona?id=' + cellvalue + '&appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        data: {},
                        success: function (parameterdata) {
                            //Insert HTML code
                                thereturn = parameterdata['cargopersona'];



                        },
                        error: function () {
                            $.alert("Error accessing server getcargopersona", {type: "danger"});
                        },
                        complete: function () {

                        }

                    });
                }else{
                    thereturn='NO'
                }
                return thereturn;

                  }



                   function actualizaselectoperadorbodyhead(e) {
        if (e.keyCode === 13 && !e.shiftKey) {
            var operador = document.getElementById("jupiter_operadorbodyheadenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectoperador"+"?operador="+operador+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_operadorbodyhead option').remove();
                       $('#jupiter_operadorbodyhead').append('<option>-Seleccione-</option>');
                       for (var k in parameterdata['data']) {
                           if(k==0) {
                               $('#jupiter_operadorbodyhead').append('<option selected>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }else{
                               $('#jupiter_operadorbodyhead').append('<option>' + parameterdata['data'][k]['nombre'] + '</option>');
                           }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectoperadorbodyhead");
                   },
                   complete: function () {
                   }
               });
        }

        }


            function actualizaselectunidadbodyhead(e) {
           if (e.keyCode === 13 && !e.shiftKey) {
               var unidad = document.getElementById("jupiter_ecobodyheadenter").value;
               $.ajax({
                   type: "GET",
                   url: "${h.url()}/abono/actualizaselectunidad?unidad="+unidad+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&soy=${soy}",
                   contentType: "application/json; charset=utf-8",
                   data: {},
                   success: function (parameterdata) {
                       $('#jupiter_ecobodyhead option').remove();
                        $('#jupiter_ecobodyhead').append('<option>-Seleccione-</option>');
                       for (var k in parameterdata['data']) {
                        if(k==0) {
                            $('#jupiter_ecobodyhead').append('<option selected>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }else{
                            $('#jupiter_ecobodyhead').append('<option>' + parameterdata['data'][k]['eco1'] + '</option>');
                        }
                       }

                   },
                   error: function () {
                       alert("ERROR actualizaselectunidadbodyhead");
                   },
                   complete: function () {
                   }
               });
           }

      }

      function verseccionesbodyhead() {
                var por=document.getElementById("jupiter_opcionoperadorounidad").value;
                if(por=='Unidad'){
                    document.getElementById("trdeunidad").style.display='block';
                    document.getElementById("trdeoperador").style.display='none';
                }else{
                    document.getElementById("trdeunidad").style.display='none';
                    document.getElementById("trdeoperador").style.display='block';
                }
        }


                  function jupiter_mysearchbodyhead() {
                 var date=document.getElementById("jupiter_fechabodyhead").value;
                 var datefinal=document.getElementById("jupiter_fechabodyheadfinal").value;
                 var status=document.getElementById("jupiter_statusbodyhead").value;
                 var eco=document.getElementById("jupiter_ecobodyhead").value;
                 var por=document.getElementById("jupiter_opcionoperadorounidad").value;
                 %if soy=='nouser':
                    var operator=document.getElementById("jupiter_operadorbodyhead").value;
                 %else:
                     var operator='';
                 %endif
                 var url="${h.url()}/bodyhead/searchgrid/?date="+date+"&status="+status+"&eco="+eco+"&operator="+operator+"&por="+por+"&internal_id=${internal_id}&user=${user}&app_name=${app_name}&datefinal="+datefinal+"&soy=${soy}";
                      jQuery("#jupiter_jqGridbodyhead").setGridParam({url: url}).trigger("reloadGrid");
               }

               function jupiter_reloadbodyhead(){
                  var url='${h.url()}/bodyhead/load?appID=${application_id}&internal_id=${internal_id}&app_name=${app_name}&user=${user}&soy=${soy}';
                   document.getElementById("jupiter_fechabodyhead").value='${fecha}';
                 document.getElementById("jupiter_fechabodyheadfinal").value='${fecha}';
                  jQuery("#jupiter_jqGridbodyhead").setGridParam({url: url}).trigger("reloadGrid");
                }



    function funciongenerar() {
                   var fechainicial=document.getElementById("jupiter_fechabodyhead").value;
                   var fechafinal=document.getElementById("jupiter_fechabodyheadfinal").value;
                   var por=document.getElementById("jupiter_opcionoperadorounidad").value;
                   if(por=='Unidad'){
                   var valor=document.getElementById("jupiter_ecobodyhead").value;
                   }else{
                       var valor=document.getElementById("jupiter_operadorbodyhead").value;
                   }
        $.ajax({
            type: "GET",
            url: '${h.url()}/bodyhead/generopdf?fechainicial='+fechainicial+'&fechafinal='+fechafinal+'&por='+por+'&valor='+valor+'&user=${user}',
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                if(parameterdata['error']!='no') {
                    //var file_path = '${h.url()}'+parameterdata['name'];
                    window.open('${h.url()}' + parameterdata['name']).location.reload();
                }else{
                      $.alert("Seleccione una unidad o un operador", {type: "danger"});
                }
                //var a = document.createElement('A');
                //a.href = file_path;
                //a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
                //document.body.appendChild(a);
                //a.click();
                //document.body.removeChild(a);
            },
            error: function () {
                $.alert("Error accessing server funciongenerar", {type: "danger"});
            },
            complete: function () {

            }

        });
      }

 </script>

<div id="dialogForm03abonar"  title="${_('PAY')}"></div>
    <!-- JQGRID table start-->
<table>
    <tr style="height:30px";>
        <td>${_('Initial Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechabodyhead" name="jupiter_fechabodyhead"></td>
        <td>${_('Final Date: ')}</td><td><input type="date" value="${fecha}" id="jupiter_fechabodyheadfinal" name="jupiter_fechabodyheadfinal"></td>
    </tr>
     <tr style="height:30px";>
    <td>${_('Status: ')}</td><td><select id="jupiter_statusbodyhead" name="jupiter_statusbodyhead"><option>-Seleccione-</option><option>Por Pagar</option><option>PAGADO</option><option>ADEUDO</option></select></td>
    </tr>
    <tr>
        <td><select id="jupiter_opcionoperadorounidad" name="jupiter_opcionoperadorounidad" onchange="verseccionesbodyhead();"><option>Unidad</option>
            %if soy=='nouser':
            <option>Operador</option>
            %endif
            </select></td>
    </tr>
</table>
<table id="trdeunidad" style="display: block;">
     <tr style="height:30px;">
         <td>${_('Eco: ')}</td><td><input type="text" id="jupiter_ecobodyheadenter" name="jupiter_ecobodyheadenter" onkeypress="actualizaselectunidadbodyhead(event);"></td><td><select id="jupiter_ecobodyhead" name="jupiter_ecobodyhead">
                                            <option>-Seleccione-</option>
                                            %for item in unidades:
                                                    <option>${item.eco1}</option>
                                            %endfor
                            </select></td>
    </tr>
</table>
%if soy=='nouser':
<table id="trdeoperador" style="display: none;" >
     <tr style="height:30px;">
         <td>${_('Operator: ')}</td><td><input type="text" id="jupiter_operadorbodyheadenter" name="jupiter_operadorbodyheadenter"  onkeypress="actualizaselectoperadorbodyhead(event);"></td><td><select id="jupiter_operadorbodyhead" name="jupiter_operadorbodyhead">
         <option>-Seleccione-</option>
         %for item in operadores:
         <option>${item.nombre}</option>
         %endfor
     </select></td>
   </tr>
    </table>
%endif
<table>
    <tr style="height:30px";>
        <td></td><td><button onclick="jupiter_mysearchbodyhead()"><span class="glyphicon glyphicon-search"></span></button>
            <button onclick="jupiter_reloadbodyhead()"><span class="glyphicon glyphicon-refresh"></span></button></td>

    </tr>
    </table>

<br>
    <table style="width:100%">
    <table id="jupiter_jqGridbodyhead" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="jupiter_Pagerbodyhead" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
<%doc>
<select><option>Fecha</option><option>Estatus</option><option>Eco</option><option>Operador</option></select></%doc>



