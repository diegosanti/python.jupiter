
<p align="center"><h3>Estado de ${valor}</h3></p>

<p align="center"><b>${fechainicial} al ${fechafinal}</b></p>


<table border="1" style="width: 100%">
    <tr>
        <th>Total por Pagar</th>
        <th>Pagado</th>
        <th>Faltante</th>
    </tr>
    <tr style="text-align: center;">
    %for itemtotal in listatotales:
        <td>${itemtotal}</td>
    %endfor
    </tr>


</table>
<table style="width: 100%">

%for item in heads:
        <tr>
        <th>Fecha</th>
        <th>Por Pagar</th>
        <th>Pagado</th>
        <th>Faltante</th>
    </tr>
    <tr style="text-align: center;">
        <td>${item.fecha}</td>
        <td>${item.porpagar}</td>
        <td>${item.pagado}</td>
        <td>${item.faltante}</td>
    </tr>
        <tr>
            <th>FECHA</th>
            <th>CARGO</th>
            <th>MONTO</th>
            <th>CONDONACION</th>
            <th>SALDO</th>
            <th>PAGADO</th>
        </tr>

    %for itembody in body:
        <tr style="text-align: center;">
        %if itembody['fecha']==item.fecha:
                <td>${itembody['fecha']}</td>
                <td>${itembody['cargo']}</td>
                <td>${itembody['monto']}</td>
                <td>${itembody['condonacion']}</td>
                <td>${itembody['saldo']}</td>
                <td>${itembody['pagado']}</td>
        %endif

        </tr>
    %endfor
%endfor
</table>