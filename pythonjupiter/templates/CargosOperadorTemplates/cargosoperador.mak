
<link rel="stylesheet" href="${h.url()}/css/tables_jupiterblue.css" type="text/css"/>
<style>
    .bookmark {
        width: 110px;
        height: 56px;
        padding-top: 15px;
        position: relative;
        background: #5c9ccc;
        color: white;
        font-size: 11px;
        letter-spacing: 0.2em;
        text-align: center;
        text-transform: uppercase;
    }

    .bookmark:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        width: 0;
        height: 0;
        border-bottom: 13px solid #fff;
        border-left: 55px solid transparent;
        border-right: 55px solid transparent;
    }

</style>
<script>

                          var jupiter_subscribe ="/topic/testlistener_"+"${user}";
            var jupiter_message_test_client = Stomp.client('${h.stompServer()}');
            jupiter_message_test_client.debug=null;
            var jupiter_message_test_connect_callback = function() {
                jupiter_message_test_client.subscribe(jupiter_subscribe, message_test_subscribe_callback);
                // called back after the client is connected and authenticated to the STOMP server
              };
            var jupiter_message_test_error_callback = function(error) {
                  $.alert(error,{type: 'danger'});
            };
            var message_test_subscribe_callback = function(message) {

                var msg = message.body;
                var payload = msg.split('|');
                var command = payload[0];
                var data = payload[1];

                switch (command) {
                        case 'RELOAD':
                            $('#jqGridAlerts').trigger( 'reloadGrid' );
                            break;
                        case 'MSG_GREEN':
                            $.alert(data, { autoClose:false,type: 'success',});
                            break;
                        case 'MSG_RED':
                            $.alert(data, { autoClose:false,type: 'danger',});
                            break;
                        case 'MSG_YELLOW':
                            $.alert(data, { autoClose:false,type: 'warning',});
                            break;
                        case 'MSG_BLUE':
                            $.alert(data, { autoClose:false,type: 'info',});
                            break;
                        case 'MSG_BLUE_WITH_AUTOCLOSE':
                            $.alert(data, { autoClose:true,type: 'info',});
                            break;
                }
              };
            var jupiter_stompUser='${h.stompUser()}';
            var jupiter_stompPass='${h.stompPassword()}';
            jupiter_message_test_client.connect(jupiter_stompUser, jupiter_stompPass, jupiter_message_test_connect_callback, jupiter_message_test_error_callback, '/');

         function openWindowcargooperadores(text,url){
            doAjax(url,'openTab');
            var $dialog = $('<div></div>')
               .html('<div id="openTab" style="margin:10px"></div>')
               .dialog({
                   autoOpen: false,
                   modal: true,
                   height:  780,
                   width: 820,
                   resizable: false,
                   draggable: true,
                   position : { my: "center", at: "top", of: window },
                   close: function(){
                       $(this).dialog('destroy').remove();
                       //Dialog03templatehabilita.dialog("close");
                       reloadallcargooperador();
                   },
                   title: text,
                   data: "user=${h.whoami()}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                   buttons: {
      <%doc>                 "Close": function ()
                       {
                           $(this).dialog('destroy').remove();
                           Dialog03templatehabilita.dialog("close");
                           reloadallpersonas();


                       }</%doc>
                   }
               });
        $dialog.dialog('open');
        }




            function reloadallcargooperador() {
           document.getElementById("jupiter_filtercargooperador").value="";
            document.getElementById("jupiter_filtercargooperador").placeholder="CargoOperador";
                     $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosoperadores/reloadcargosoperador?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablecargooperador td').remove();
                      $("#bookmarkcargooperadores").html(data['contador']+ " Cargo(s)");

                        for(var k in data['listabusca']){
                                       var anexando;
                            if(data['listabusca'][k].tipodecargo==1) {
                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Administrativo')}</a></td>';

                            }
                            if (data['listabusca'][k].tipodecargo==2) {

                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Cuenta')}</a></td>';
                            }
                            if (data['listabusca'][k].tipodecargo==3) {
                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Por Dia Trabajado')}</a></td>';
                            }
                            $('#jupiter_tablecargooperador').append('<tr><td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['cargo'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['frecuencia'] + '</a></td>' +
                                     anexando+
                                    '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['montodefault'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['empresa']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['grupopersona']+ '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_cargooperador']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing reloadallcargooperador", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
        }

       function searchcargooperador(){
           var name=document.getElementById("jupiter_filtercargooperador").value;
           $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosoperadores/searchcargooperador?name="+name+"&user=${user}&app_name=${app_name}&internal_id=${internal_id}&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {

                      $('#jupiter_tablecargooperador td').remove();
                      $("#bookmarkcargooperadores").html(data['contador']+ " Cargo(s)");

                        for(var k in data['listabusca']){
                            var anexando;
                            if(data['listabusca'][k].tipodecargo==1) {
                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Administrativo')}</a></td>';

                            }
                            if (data['listabusca'][k].tipodecargo==2) {

                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Cuenta')}</a></td>';
                            }
                            if (data['listabusca'][k].tipodecargo==3) {
                                anexando='<td> <a onclick="openWindowcargooperadores(\'Editar\',\'${h.url()}/cargosoperadores/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');">${_('Por Dia Trabajado')}</a></td>';
                            }

                            $('#jupiter_tablecargooperador').append('<tr><td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\"><span class="glyphicon glyphicon-edit"></span></a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['cargo'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['frecuencia'] + '</a></td>' +
                                    anexando+
                                    '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['montodefault'] + '</a></td>' +
                                       '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['empresa']+ '</a></td>' +
                                            '<td><a onclick=\"openWindowcargooperadores(\'Editar \',\'${h.url()}\/cargosoperadores\/editcargo?id='+data['listabusca'][k]['id_cargooperador']+'\');\">' + data['listabusca'][k]['grupopersona']+ '</a></td>' +
                                       '<td><a onclick=\"eliminar('+data['listabusca'][k]['id_cargooperador']+');\"><span class=\"glyphicon glyphicon-trash\"></span></td></tr>' );

                        }

                    },
                    error: function () {
                        $.alert("Error accessing searchcargooperador", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });


       }

    function eliminar(id) {
    var confirmando=confirm("¿Seguro que deseas eliminar este Cargo del Operador?");
    if(confirmando){
                   $.ajax({
                    type: "GET",
                    url: "${h.url()}/cargosoperadores/eliminacargooperador?id="+id+"&application_id=${application_id}",
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (data) {
                        alert(data['error']);
                        reloadallcargooperador();


                    },
                    error: function () {
                        $.alert("Error no se puede eliminar", {autoClose: false,});
                        return true;
                    },
                    complete: function () {
                    }
                });
    }

  }

function searchentercargooperador(e) {
     if (e.keyCode === 13 && !e.shiftKey)  {
         searchcargooperador();
     }
  }



</script>

<button style="float: left;" onclick="openWindowcargooperadores('Nuevo Cargo','${h.url()}/cargosoperadores/nuevocargooperador');">
    <span class="glyphicon glyphicon-plus">${_('New')}</span>
</button>
<br><br>


  <input type="text" id="jupiter_filtercargooperador" onkeypress="searchentercargooperador(event);" placeholder="CargoOperador"><button onclick=searchcargooperador()><i class="fa fa-search"></i></button><button onclick="reloadallcargooperador()" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    &nbsp;&nbsp;<a onclick="reload()" class="fa fa-refresh" aria-hidden="true" style="display: none;" ></a>

<div id="bookmarkcargooperadores" class="bookmark" style="float: right;">${contador} Cargo(s)</div>
<br>


<table id="jupiter_tablecargooperador" class="userTable">
    <tr>
        <th></th>
        <th>${_('NAME')}</th>
        <th>${_('FREQUENCY')}</th>
        <th>${_('TYPE')}</th>
        <th>${_('AMOUNT')}</th>
        <th>${_('EMPRESA')}</th>
        <th>${_('GRUPO')}</th>
        <th>${_('DELETE')}</th>
    </tr>
    %for item in allrecords:

    <tr>
         <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');"><span class="glyphicon glyphicon-edit"></span></a></td>
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${item.cargo}</a></td>
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${item.frecuencia}</a></td>
        %if item.tipodecargo==1:
         <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${_('Administrativo')}</a></td>
        %elif item.tipodecargo==2:
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${_('Cuenta')}</a></td>
        %elif item.tipodecargo==3:
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${_('Por Dia Trabajado')}</a></td>
        %endif
         <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${item.montodefault}</a></td>
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${item.empresa}</a></td>
        <td> <a onclick="openWindowcargooperadores('Editar','${h.url()}/cargosoperadores/editcargo?id=${item.id_cargooperador}');">${item.grupopersona}</a></td>
        <td><a onclick="eliminar(${item.id_cargooperador});"><span class="glyphicon glyphicon-trash"></span></a></td>
    </tr>
        %endfor

</table>