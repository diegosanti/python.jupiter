<script>

            function validar(esto) {
                var radio;

                var ctrl = document.cargosoperadorform.tipocargooperador;
                for (i = 0; i < ctrl.length; i++) {
                    if (ctrl[i].checked) {
                        radio = ctrl[i].value;
                    }
                }

                if (radio == 1 || radio==2) {
                    valido = false;
                    for (var a = 0; a < esto.elements.length; a++) {
                        if (esto[a].type == "checkbox" && esto[a].checked == true && esto[a].name.includes("jupiter")) {
                            valido = true;
                            break
                        }

                    }
                    if (!valido) {
                        $.alert("Elige un dia por lo menos!",{type: 'warning'});
                        return false
                    } else {
                        aplicaratodas();
                    }

                }
            }

                   function validarlineas() {
               var empresa=document.getElementById("jupiter_selectapp_nameforcharges").value;
                $.ajax({
                        type: "GET",
                        url: "${h.url()}/unidades/actualizalineas"+"?empresa="+empresa+"&internal_id=${internal_id}&application_id=${application_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $('#jupiter_selectcompaniesforcharges option').remove();
                     $("#jupiter_selectcompaniesforcharges").append('<option>TODAS</option>');

                     if (empresa!='TODO') {

                         for (var k in parameterdata['lineas']) {
                             $("#jupiter_selectcompaniesforcharges").append('<option value="' + parameterdata['lineas'][k]['nombre_linea'] + '">' + parameterdata['lineas'][k]['nombre_linea'] + '</option>');
                         }

                     }

                },
                error: function () {
                        alert("ERROR validarlineas");
                },
                complete: function () {
                 }
                });

              }


                          function verselect(cualselect,este) {
                if(este.checked==true){
                    document.getElementById(cualselect).style.display="block"
                }else{
                  document.getElementById(cualselect).style.display="none";

                  if(cualselect=='jupiter_selectcompaniesforcharges') {
                      validarlineas();
                  }

                }

              }

                           function seleecionartodasemana(){

        if(document.getElementById('jupiter_chargeunidadallweek').checked==true ) {
            for (var i = 0; i < document.cargosoperadorform.elements.length; i++)
                if (document.cargosoperadorform.elements[i].type == "checkbox" && document.cargosoperadorform.elements[i].name.includes("jupiter"))
                    document.cargosoperadorform.elements[i].checked = 1
        }else{
               for (var i=0;i<document.cargosoperadorform.elements.length;i++)
                   if(document.cargosoperadorform.elements[i].type == "checkbox" && document.cargosoperadorform.elements[i].name.includes("jupiter"))
                        document.cargosoperadorform.elements[i].checked=0
        }
        }

        function checadas(esto) {
            var contador=0;
            for (var i = 0; i < document.cargosoperadorform.elements.length; i++)
                if (document.cargosoperadorform.elements[i].type == "checkbox" && document.cargosoperadorform.elements[i].name.includes("jupiter")) {
                if(document.cargosoperadorform.elements[i].checked==true) {
                    contador++;

                }
                if(contador==7){
                       document.getElementById("jupiter_chargeunidadallweek").checked=true;
                }
            }

            if(esto.checked==false){
                document.getElementById("jupiter_chargeunidadallweek").checked=false;
            }



          }


    function aplicaratodas() {
                var id='${querytoedit.id_cargooperador}';

                    var confirmando=document.getElementById("chargeoperadorall_j").checked;
                var monto=document.getElementById("jupiter_amountdefault").value;
                  if(confirmando==true){
                             $.ajax({
                        type: "GET",
                        url: "${h.url()}/cargosoperadores/editamonto"+"?monto="+monto+"&internal_id=${internal_id}&id=${querytoedit.id_cargooperador}&user=${user}&application_id=${application_id}",
                        contentType: "application/json; charset=utf-8",
                        data: {},
                success: function (parameterdata) {
                    $.alert(parameterdata['data'],{type: 'success'});


                },
                error: function () {
                        alert("ERROR aplicaratodos");
                },
                complete: function () {
                 }
                });

                  }

                }

                     function jupiterversecciondiasadmioperador(id) {
                        if(id.value==1 || id.value==2 || id.value==3){
                        document.getElementById("jupiter_secciontablaadmioperadores").style.display='block';
                    }else{
                         document.getElementById("jupiter_secciontablaadmioperadores").style.display='none';
                    }
                    }

</script>


<legend>${_('Edit Charge')}</legend>
 <form action="${h.url()}/cargosoperadores/editandoCargoOperador?internal_id=${internal_id}&user=${user}&id=${querytoedit.id_cargooperador}&application_id=${application_id}" name="cargosoperadorform" id="cargosoperadorform" method="POST" enctype="multipart/form-data" target="requestcargosoperador" accept-charset="UTF-8" onsubmit="return validar(this)">

    <table>
        <tr>
        <td>${_('Name: ')}</td><td><input id="jupiter_chargename" name="jupiter_chargename" type="text" maxlength="70" length="50" value="${querytoedit.cargo}" required></td>
        </tr>
           <tr>
        <td>${_('Amount: ')}</td><td><input id="jupiter_amountdefault" name="jupiter_amountdefault" type="number" value="${querytoedit.montodefault}" min="0" max="2145000000" required></td>
        </tr>
       <%doc>    <tr>
        <td>${_('App_name: ')}</td><td><select name="jupiter_selectapp_nameforcharges" id="jupiter_selectapp_nameforcharges" onchange="validarlineas();">
                                        %for item in empresa:
                                            %if querytoedit.app_name==item['application_name']:
                                                <option selected>${item['application_name']}</option>
                                            %else:
                                                <option >${item['application_name']}</option>
                                            %endif
                                        %endfor

                                    </select></td>
        </tr></%doc>
                 <tr>
                     %if querytoedit.empresa =='':

                        <td><input type="checkbox" name="companiescheck" onchange="verselect('jupiter_selectcompaniesforcharges',this);">
                                 ${_('Companies: ')}</td>
                     <td><select name="jupiter_selectcompaniesforcharges" id="jupiter_selectcompaniesforcharges" style="display: none;">
                     <option >TODAS</option>
                                        %for item in allempresas:
                                            %if querytoedit.empresa==item.nombre_linea:
                                                <option selected>${item.nombre_linea}</option>
                                            %else:
                                                <option >${item.nombre_linea}</option>
                                            %endif
                                        %endfor

                                    </select></td>
                     %elif querytoedit.empresa =='TODAS' or querytoedit.empresa !='':
                        <td><input type="checkbox" name="companiescheck" onchange="verselect('jupiter_selectcompaniesforcharges',this);" checked>
                                 ${_('Companies: ')}</td>
                     <td><select name="jupiter_selectcompaniesforcharges" id="jupiter_selectcompaniesforcharges" style="display: block;">
                     <option >TODAS</option>
                                        %for item in allempresas:
                                            %if querytoedit.empresa==item.nombre_linea:
                                                <option selected>${item.nombre_linea}</option>
                                            %else:
                                                <option>${item.nombre_linea}</option>
                                            %endif
                                        %endfor

                                    </select></td>
                     %endif

                 </tr>


                         <tr>
                             %if querytoedit.grupopersona =='':

                        <td><input type="checkbox" name="grupospersonascheck" onchange="verselect('jupiter_selectgrupospersonasforcharges',this);">
                                 ${_('Groups of Operators: ')}</td>
                     <td><select name="jupiter_selectgrupospersonasforcharges" id="jupiter_selectgrupospersonasforcharges" style="display: none;">
                     <option >TODAS</option>
                                        %for item in allgrupos:
                                            %if querytoedit.grupopersona==item.grupopersona:
                                                <option selected>${item.grupopersona}</option>
                                            %else:
                                                <option >${item.grupopersona}</option>
                                            %endif
                                        %endfor

                                    </select></td>
                     %elif querytoedit.grupopersona =='TODAS' or querytoedit.grupopersona !='':
                        <td><input type="checkbox" name="grupospersonascheck" onchange="verselect('jupiter_selectgrupospersonasforcharges',this);" checked>
                                 ${_('Groups of Operators: ')}</td>
                     <td><select name="jupiter_selectgrupospersonasforcharges" id="jupiter_selectgrupospersonasforcharges" style="display: block;">
                     <option >TODAS</option>
                                        %for item in allgrupos:
                                            %if querytoedit.grupopersona==item.grupopersona:
                                                <option selected>${item.grupopersona}</option>
                                            %else:
                                                <option>${item.grupopersona}</option>
                                            %endif
                                        %endfor

                                    </select></td>
                     %endif

        </tr>

                   <tr>
                        %if querytoedit.tipodecargo==1:
        <td><input type="radio" id="jupiter_tipoadmi"
                   name="tipocargooperador" value="1" onchange="jupiterversecciondiasadmioperador(this);" checked/>
            <label for="louie">Administrativo</label></td>
                            %else:
                               <td><input type="radio" id="jupiter_tipoadmi"
                   name="tipocargooperador" value="1" onchange="jupiterversecciondiasadmioperador(this);" />
            <label for="louie">Administrativo</label></td>

                   %endif

        </tr>
        <tr>
             %if querytoedit.tipodecargo==2:

                  <td><input type="radio" id="jupiter_tipocuenta"
                   name="tipocargooperador" value="2" onchange="jupiterversecciondiasadmioperador(this);" checked
                  />
            <label for="louie">Cuenta</label></td>
                 %else:
                   <td><input type="radio" id="jupiter_tipocuenta"
                   name="tipocargooperador" value="2" onchange="jupiterversecciondiasadmioperador(this);"
                  />
            <label for="louie">Cuenta</label></td>
                 %endif
        </tr>
        <tr>
                 %if querytoedit.tipodecargo==3:
                  <td><input type="radio" id="jupiter_tipotrabaja"
                   name="tipocargooperador" value="3" onchange="jupiterversecciondiasadmioperador(this);" checked/>
            <label for="louie">Por dia Trabajado</label></td>
                     %else:
                          <td><input type="radio" id="jupiter_tipotrabaja"
                   name="tipocargooperador" value="3" onchange="jupiterversecciondiasadmioperador(this);" />
            <label for="louie">Por dia Trabajado</label></td>

                     %endif
        </tr>
    </table>

      %if querytoedit.tipodecargo==1 or querytoedit.tipodecargo==2 or querytoedit.tipodecargo==3:
     <table id="jupiter_secciontablaadmioperadores">
         %else:
          <table id="jupiter_secciontablaadmioperadores" style="display: none;">
          %endif


%if contadordias==7:

        <tr>
            <td>${_('Frequency: ')}</td><td><input type="checkbox" id="jupiter_chargeunidadallweek" name="jupiter_chargeunidadallweek" onchange="seleecionartodasemana()" checked>${_('All week')}</td>
        </tr>
         <tr id="jupiter_trdedias">
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadlunes" name="jupiter_chargeunidadlunes" onchange="checadas(this);" checked>${_('Monday')}</td>
        </tr>
        <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmartes" name="jupiter_chargeunidadmartes" onchange="checadas(this);" checked>${_('Tuesday')}</td>
        </tr>
         <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmiercoles" name="jupiter_chargeunidadmiercoles" onchange="checadas(this);" checked>${_('Wednesday')}</td>
        </tr>
         <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadjueves" name="jupiter_chargeunidadjueves" onchange="checadas(this);" checked>${_('Thursday')}</td>
        </tr>
         <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadviernes" name="jupiter_chargeunidadviernes" onchange="checadas(this);" checked>${_('Friday')}</td>
        </tr>
         <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadsabado" name="jupiter_chargeunidadsabado" onchange="checadas(this);" checked>${_('Saturday')}</td>
        </tr>
         <tr>
            <td></td><td><input type="checkbox" id="jupiter_chargeunidaddomingo" name="jupiter_chargeunidaddomingo" onchange="checadas(this);" checked >${_('Sunday')}</td>
        </tr>
   %else:
           <tr>
            <td>${_('Frequency: ')}</td><td><input type="checkbox" id="jupiter_chargeunidadallweek" name="jupiter_chargeunidadallweek" onchange="seleecionartodasemana()" >${_('All week')}</td>
        </tr>
         <tr id="jupiter_trdedias">
             %if 'Lunes' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadlunes" name="jupiter_chargeunidadlunes" onchange="checadas(this);" checked>${_('Monday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadlunes" name="jupiter_chargeunidadlunes" onchange="checadas(this);">${_('Monday')}</td>
            %endif
        </tr>

        <tr>
            %if 'Martes' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmartes" name="jupiter_chargeunidadmartes" onchange="checadas(this);" checked>${_('Tuesday')}</td>
            %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmartes" name="jupiter_chargeunidadmartes" onchange="checadas(this);">${_('Tuesday')}</td>
            %endif
        </tr>
         <tr>
             %if 'Miercoles' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmiercoles" name="jupiter_chargeunidadmiercoles" onchange="checadas(this);" checked>${_('Wednesday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadmiercoles" name="jupiter_chargeunidadmiercoles" onchange="checadas(this);">${_('Wednesday')}</td>
            %endif
        </tr>
         <tr>
             %if 'Jueves' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadjueves" name="jupiter_chargeunidadjueves" onchange="checadas(this);" checked>${_('Thursday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadjueves" name="jupiter_chargeunidadjueves" onchange="checadas(this);">${_('Thursday')}</td>
            %endif
        </tr>
         <tr>
             %if 'Viernes' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadviernes" name="jupiter_chargeunidadviernes" onchange="checadas(this);" checked>${_('Friday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadviernes" name="jupiter_chargeunidadviernes" onchange="checadas(this);">${_('Friday')}</td>
            %endif
        </tr>
         <tr>
             %if 'Sabado' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadsabado" name="jupiter_chargeunidadsabado" onchange="checadas(this);" checked>${_('Saturday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidadsabado" name="jupiter_chargeunidadsabado" onchange="checadas(this);">${_('Saturday')}</td>
            %endif
        </tr>
         <tr>
             %if 'Domingo' in dias:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidaddomingo" name="jupiter_chargeunidaddomingo" onchange="checadas(this);" checked>${_('Sunday')}</td>
             %else:
            <td></td><td><input type="checkbox" id="jupiter_chargeunidaddomingo" name="jupiter_chargeunidaddomingo" onchange="checadas(this);">${_('Sunday')}</td>
            %endif
        </tr>


   %endif
    </table>
           <br><br>
     <input type="checkbox" id="chargeoperadorall_j" name="chargeoperadorall_j">Modificar el monto a todos los operadores que le correspondan
     <br>
     <br>
          <input type="submit" name="jupiter_submit_uploadcharge" value="GUARDAR"/>
</form>
<iframe name="requestcargosoperador" id="requestcargosoperador" frameborder="0"></iframe>