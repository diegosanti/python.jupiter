from pythonjupiter.lib.base import BaseController
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import AbonoCuenta
from tg import app_globals
import requests
from tg import expose
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from pythonjupiter.model.tables import Linea,DatosUnidad,Persona,CargoOperador,Derrotero,Grupo,UnidadOperador,EstadoCuenta,Condonaciones,CargoUnidad,GrupoPersona
from pythonjupiter.model.tables import UnidadCargo,OperadorCargo,CondonacionOperador,CondonacionUnidad,Body,Head,Grupo_DatosUnidad
from pythonjupiter.lib.messagequeue import Message
from tg import render_template

class CondonacionController(BaseController):

    def __init__(self):
        pass

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['appID'])]
        return jqgridDataGrabber(Condonaciones, 'id_condonacion', filter, kw).loadGrid()


    @expose('json')
    def form(self, **kw):
        print("FORM")
        # if kw['id'] == "0":
        #     kw['actions'] = AbonoCuenta()
        # else:
        #     kw['actions'] = DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=kw['id']).first()
        if kw['app_name']=='TODOveritodo':
            queryunidades=DBSession.query(DatosUnidad).filter_by(internal_id=kw['internal_id']).filter_by(habilitado=1).all()
        else:
            queryunidades=DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).all()

        cargosof=[]
        queryunidadescargos=DBSession.query(UnidadCargo).filter_by(unidad_id=queryunidades[0].id_unidad).all()
        for item in queryunidadescargos:
            querycargounidad=DBSession.query(CargoUnidad).filter_by(id_cargounidad=item.cargo_id).first()
            cargosof.append({'cargo': querycargounidad.cargo})


        kw['unidades']=queryunidades
        kw['cargos']=cargosof
        if kw['app_name']=='TODO':
            domain = app_globals.domainsun+"internal_id="+kw['internal_id']
            try:
                content = requests.get(domain, auth=("manager", "managepass"))
                content = content.json()
                empresa = content['applications']
            except ValueError:
                empresa = []
            kw['apps_names']=empresa
        else:
            kw['apps_names']=[{'application_name': kw['app_name']}]

        empresas=DBSession.query(Linea).filter_by(app_id=kw['application_id']).first()
        kw['empresas']=empresas
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.Condonaciones.condonacion_form')
        return dict(dialogtemplate=dialogtemplate)


    @expose('json')
    def actualizaselects(self,**kw):


        cargosof=[]
        unidades=[]
        application_id=kw['application_id']
        what=kw['what']
        if what=='Unit' or what=='Unidad':
            if kw['app_name'] == 'TODOveritodo':
                queryunidades = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).all()
            else:
                queryunidades = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).all()

            queryunidadescargos = DBSession.query(UnidadCargo).filter_by(unidad_id=queryunidades[0].id_unidad).all()
            for item in queryunidadescargos:
                querycargounidad = DBSession.query(CargoUnidad).filter_by(id_cargounidad=item.cargo_id).first()
                cargosof.append({'cargo': querycargounidad.cargo})

            for item in queryunidades:
                unidades.append({'eco1': item.eco1})

            return dict(cargos=cargosof,unidades=unidades,operadores=[],empresas=[],derroteros=[],grupos=[])

        elif what=='Operador':
            opera=[]

            queryoperadores = DBSession.query(Persona).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).filter_by(habilitadooperador=1).all()


            queryoperadorescargos = DBSession.query(OperadorCargo).filter_by(operador_id=queryoperadores[0].id_persona).all()
            for item in queryoperadorescargos:
                querycargooperador = DBSession.query(CargoOperador).filter_by(id_cargooperador=item.cargo_id).first()
                cargosof.append({'cargo': querycargooperador.cargo})

            for item in queryoperadores:
                opera.append({'nombre': item.nombre})

            print("RETORNARE POR OPERADORESS")
            return dict(cargos=cargosof,operadores=opera,unidades=[],empresas=[],derroteros=[],grupos=[])
        elif what=='Empresa':


            queryempresas=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()


            querycargos=DBSession.query(CargoUnidad).filter_by(empresa=queryempresas[0].nombre_linea).filter_by(application_id=application_id).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            querymascargos = DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(application_id=kw['application_id']).all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})



            querycargos=DBSession.query(CargoOperador).filter_by(empresa=queryempresas[0].nombre_linea).filter_by(application_id=application_id).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoOperador).filter_by(empresa='TODAS').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            #querymascargos = DBSession.query(CargoOperador).filter_by(empresa='TODAS').all()
            #for item in querymascargos:
            #    cargosof.append({'cargo': item.cargo})


            return dict(cargos=cargosof, operadores=[], unidades=[],empresas=queryempresas,derroteros=[],grupos=[])

        elif what=='Derrotero':
            if kw['appname'] == 'TODOveritodo':
                queryderrotero=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
            else:
                queryderrotero = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()

            querycargos=DBSession.query(CargoUnidad).filter_by(derrotero=queryderrotero[0].derrotero).filter_by(application_id=application_id).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            querymascargos = DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(application_id=kw['application_id']).all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})

            return dict(cargos=cargosof, operadores=[], unidades=[],empresas=[],derroteros=queryderrotero,grupos=[])

        elif what=='Grupo':
            if kw['appname'] == 'TODOveritodo':
                querygrupo=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

            else:
                querygrupo = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

            try:
                querycargos=DBSession.query(CargoUnidad).filter_by(grupo=querygrupo[0].grupo).filter_by(application_id=application_id).all()
            except:
                querycargos={}
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            querymascargos = DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(application_id=kw['application_id']).all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            return dict(cargos=cargosof, operadores=[], unidades=[],empresas=[],derroteros=[],grupos=querygrupo)



    @expose('json')
    def getidcargo(self,**kw):
        querycondonacion=DBSession.query(Condonaciones).filter_by(id_condonacion=kw['id']).first()
        if querycondonacion.para=='Unidad':
            querycargo=DBSession.query(CargoUnidad).filter_by(id_cargounidad=kw['quien']).first()
            cargo=querycargo.cargo
        elif querycondonacion.para=='Operador':
            querycargo=DBSession.query(CargoOperador).filter_by(id_cargooperador=kw['quien']).first()
            cargo=querycargo.cargo
        else:
            try:
                querycargounidad=DBSession.query(CargoUnidad).filter_by(id_cargounidad=kw['quien']).filter_by(cargo=querycondonacion.nameofcharge).first()
                cargo=querycargounidad.cargo
            except:
                querycargo = DBSession.query(CargoOperador).filter_by(id_cargooperador=kw['quien']).filter_by(cargo=querycondonacion.nameofcharge).first()
                cargo = querycargo.cargo


        return dict(cargo=cargo)

    @expose('json')
    def getnameeco(self, **kw):
        querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=kw['id']).first()
        if querycondonacion.para == 'Unidad':
            query = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['quien']).first()
            to = query.eco1
        else:
            query = DBSession.query(Persona).filter_by(id_persona=kw['quien']).first()
            to = query.nombre

        return dict(to=to)


    @expose('json')
    def actualizacargosconchange(self,**kw):
        cargosof=[]
        what=kw['para']
        to=kw['to']
        theapp_name=kw['theapp_name']
        if what=='Empresa':

            querycargos=DBSession.query(CargoUnidad).filter_by(empresa=to).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})


            if theapp_name!='TODO':
                querymascargos = DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name=theapp_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})

            else:
                auxtheappname=DBSession.query(Linea).filter_by(nombre_linea=to).first()
                querymascargos=DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name=auxtheappname.app_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})


            querycargos=DBSession.query(CargoOperador).filter_by(empresa=to).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})
            querymascargos=DBSession.query(CargoOperador).filter_by(empresa='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})

            if theapp_name!='TODO':
                querymascargos = DBSession.query(CargoOperador).filter_by(empresa='TODAS').filter_by(app_name=theapp_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})

            else:
                pass


            print("CARGOSOF=", cargosof)
            return dict(cargos=cargosof)

        elif what=='Derrotero':


            querycargos=DBSession.query(CargoUnidad).filter_by(derrotero=to).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})

            if theapp_name != 'TODO':
                querymascargos = DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name=theapp_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})

            else:
                auxtheappname=DBSession.query(Derrotero).filter_by(derrotero=to).first()
                querymascargos=DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name=auxtheappname.app_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})
            print("CARGOSOF=", cargosof)
            return dict(cargos=cargosof)

        elif what=='Grupo':


            querycargos=DBSession.query(CargoUnidad).filter_by(grupo=to).all()
            for item in querycargos:
                cargosof.append({'cargo': item.cargo})

            querymascargos = DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name='TODO').all()
            for item in querymascargos:
                cargosof.append({'cargo': item.cargo})

            if theapp_name != 'TODO':
                querymascargos = DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name=theapp_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})
            else:
                auxtheappname=DBSession.query(Grupo).filter_by(grupo=to).first()
                querymascargos=DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name=auxtheappname.app_name).all()
                for item in querymascargos:
                    cargosof.append({'cargo': item.cargo})

            print("CARGOSOF=",cargosof)
            return dict(cargos=cargosof)


    @expose('json')
    def actualizacargos(self,**kw):
        cargos=[]
        para=kw['para']
        what=kw['what']
        print("el what: ",what)
        application_id=kw['application_id']
        if para!='Unidad' and para!='Operador':
            return dict(cargos=cargos)
        else:
            if para=='Operador':
                querypersona=DBSession.query(Persona).filter_by(esoperador=1).filter_by(nombre=what).filter_by(application_id=application_id).first()
                try:
                    queryoperadorescargos = DBSession.query(OperadorCargo).filter_by(operador_id=querypersona.id_persona).all()
                except:
                    queryoperadorescargos={}
                for item in queryoperadorescargos:
                    querycargooperador = DBSession.query(CargoOperador).filter_by(id_cargooperador=item.cargo_id).first()
                    cargos.append({'cargo': querycargooperador.cargo})
            else:
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=what).filter_by(app_id=application_id).first()
                try:
                    queryunidadescargos = DBSession.query(UnidadCargo).filter_by(unidad_id=queryunidad.id_unidad).all()
                except:
                    queryunidadescargos={}
                for item in queryunidadescargos:
                    querycargounidad = DBSession.query(CargoUnidad).filter_by(id_cargounidad=item.cargo_id).first()
                    cargos.append({'cargo': querycargounidad.cargo})

            return dict(cargos=cargos)

    @expose('json')
    def buscarto(self,**kw):
        resultadosuni=[]
        resultadosope=[]
        para=kw['para']
        what=kw['what']
        application_id=kw['application_id']
        if para!='Unidad' and para!='Operador':
            return dict(resultadosuni=resultadosuni,resultadosope=resultadosope)
        else:
            if para=='Unidad':
                if kw['app_name'] == 'TODOveritodo':
                    queryunidades = DBSession.query(DatosUnidad).filter(DatosUnidad.eco1.like('%'+what+'%')).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).all()
                else:
                    queryunidades = DBSession.query(DatosUnidad).filter(DatosUnidad.eco1.like('%'+what+'%')).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).all()
                for item in queryunidades:
                    resultadosuni.append({'eco': item.eco1})
            else:
                print("AQUI TENGO HACER")
                queryoperadores = DBSession.query(Persona).filter(Persona.nombre.like('%'+what+'%')).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).filter_by(habilitadooperador=1).all()
                print("queryoperadores==>",queryoperadores)
                for item in queryoperadores:
                    resultadosope.append({'nombre': item.nombre})
            return dict(resultadosuni=resultadosuni,resultadosope=resultadosope)


    @expose('json')
    def actualizaselectsfor(self,**kw):
        para=kw['para']
        cargo=kw['cargo']
        apps_names = []
        empresas = []
        derrotero = []
        grupo = []
        unidades = []
        operadores=[]

        return dict(apps_names=apps_names, empresas=empresas, derrotero=derrotero, grupo=grupo, unidades=unidades,
                        operadores=operadores)


    @expose('json')
    def save(self,**kw):
        print("SAVE")

        if kw['jupiter_condonacion_id'] == "0":
             condonacion = Condonaciones()
             i = 0
        else:
            condonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=kw['jupiter_condonacion_id']).first()
            i = 1


        if kw['para']=='Unidad' or kw['para']=='Operador':
            if kw['para']=='Unidad':
                condonacion.cancelado=0
                condonacion.fechainicial=kw['fechaini']
                condonacion.fechafinal=kw['fechafin']
                condonacion.descripcion=kw['descripcion']
                condonacion.para=kw['para']
                condonacion.monto=kw['monto']
                condonacion.internal_id=kw['internal_id']
                condonacion.usuario=kw['user']
                condonacion.application_id=kw['application_id']

                queryunidad = DBSession.query(DatosUnidad).filter_by(eco1=kw['selectunidades']).filter_by(app_id=kw['application_id']).first()
                querycargo = DBSession.query(CargoUnidad).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                querybody=DBSession.query(Body).filter_by(id_unidad=queryunidad.id_unidad).filter_by(id_cargounidad=querycargo.id_cargounidad).\
                            filter(Body.fecha<=kw['fechafin']).filter(Body.fecha>=kw['fechaini']).all()

                for itembody in querybody:
                    queryhead=DBSession.query(Head).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=itembody.fecha).filter(Head.faltante>0).first()
                    if queryhead is not None:
                        condonacionpasada=itembody.condonacion
                        saldopasado=itembody.saldo
                        itembody.condonacion=int(kw['monto'])
                        itembody.saldo=itembody.monto - int(kw['monto'])
                        if itembody.saldo<0:
                            itembody.condonacion=condonacionpasada
                            itembody.saldo=saldopasado
                            print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                            return dict(error="cantidadmayor")
                        else:
                            print("Si se puede")
                            querybodies = DBSession.query(Body).filter_by(id_head=queryhead.id_head).filter_by(fecha=queryhead.fecha).filter_by(id_unidad=queryhead.id_unidad).all()
                            saldoentero=0
                            for itembodies in querybodies:
                                saldoentero = saldoentero + itembodies.saldo
                            if saldoentero < queryhead.pagado:
                                print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                itembody.condonacion = condonacionpasada
                                itembody.saldo = saldopasado
                                Message.post("testlistener_" + kw['user'],'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                return dict(error="cantidadmayor")
                            else:
                                queryhead.porpagar = saldoentero
                                queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                if queryhead.faltante == 0:
                                    queryhead.faltante = 0
                                    queryhead.porpagar = 0
                                    queryhead.estatus = 'CONDONADO'

                    else:
                        print("noexistehead")
                        Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas ")
                        return dict(error="noexistehead")
                if i == 0:
                    DBSession.add(condonacion)

                querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(descripcion=kw['descripcion'])\
                                .filter_by(para=kw['para']).filter_by(monto=kw['monto']).filter_by(fechainicial=kw['fechaini'])\
                                .filter_by(fechafinal=kw['fechafin']).filter_by(internal_id=kw['internal_id']).filter_by(application_id=kw['application_id']).first()

                newcondonacionunidad=CondonacionUnidad()
                newcondonacionunidad.condonacion_id=querycondonacion.id_condonacion
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['selectunidades']).filter_by(app_id=kw['application_id']).first()
                newcondonacionunidad.unidad_id=queryunidad.id_unidad
                querycargo=DBSession.query(CargoUnidad).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                newcondonacionunidad.cargounidad_id=querycargo.id_cargounidad
                DBSession.add(newcondonacionunidad)




            else:
                condonacion.cancelado=0
                condonacion.fechainicial=kw['fechaini']
                condonacion.fechafinal=kw['fechafin']
                condonacion.descripcion=kw['descripcion']
                condonacion.para=kw['para']
                condonacion.monto=kw['monto']
                condonacion.internal_id=kw['internal_id']
                condonacion.application_id = kw['application_id']
                condonacion.usuario = kw['user']

                queryoperador = DBSession.query(Persona).filter_by(nombre=kw['selectunidades']).filter_by(application_id=kw['application_id']).first()
                querycargo = DBSession.query(CargoOperador).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                querybody=DBSession.query(Body).filter_by(id_operador=queryoperador.id_persona).filter_by(id_cargopersona=querycargo.id_cargooperador).\
                            filter(Body.fecha<=kw['fechafin']).filter(Body.fecha>=kw['fechaini']).all()

                for itembody in querybody:
                    queryhead=DBSession.query(Head).filter_by(id_operador=queryoperador.id_persona).filter_by(fecha=itembody.fecha).filter(Head.faltante>0).first()
                    if queryhead is not None:
                        condonacionpasada=itembody.condonacion
                        saldopasado=itembody.saldo
                        itembody.condonacion=int(kw['monto'])
                        itembody.saldo=itembody.monto - int(kw['monto'])
                        if itembody.saldo<0:
                            itembody.condonacion=condonacionpasada
                            itembody.saldo=saldopasado
                            print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                            return dict(error="cantidadmayor")
                        else:
                            print("Si se puede")
                            querybodies = DBSession.query(Body).filter_by(fecha=queryhead.fecha).filter_by(id_operador=queryhead.id_operador).all()
                            saldoentero=0
                            for itembodies in querybodies:
                                saldoentero = saldoentero + itembodies.saldo
                            if saldoentero < queryhead.pagado:
                                print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                itembody.condonacion = condonacionpasada
                                itembody.saldo = saldopasado
                                Message.post("testlistener_" + kw['user'],'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                return dict(error="cantidadmayor")
                            else:
                                queryhead.porpagar = saldoentero
                                queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                if queryhead.faltante == 0:
                                    queryhead.faltante = 0
                                    queryhead.porpagar = 0
                                    queryhead.estatus = 'CONDONADO'

                    else:
                        print("noexistehead")
                        Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas")
                        return dict(error="noexistehead")

                if i == 0:
                    DBSession.add(condonacion)

                querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(descripcion=kw['descripcion'])\
                                .filter_by(para=kw['para']).filter_by(monto=kw['monto']).filter_by(fechainicial=kw['fechaini'])\
                                .filter_by(fechafinal=kw['fechafin']).filter_by(internal_id=kw['internal_id']).filter_by(application_id=kw['application_id']).first()
                newcondonacionoperador=CondonacionOperador()
                newcondonacionoperador.condonacion_id=querycondonacion.id_condonacion
                queryoperador=DBSession.query(Persona).filter_by(nombre=kw['selectunidades']).filter_by(application_id=kw['application_id']).first()
                newcondonacionoperador.operador_id=queryoperador.id_persona
                querycargo=DBSession.query(CargoOperador).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                newcondonacionoperador.cargooperador_id=querycargo.id_cargooperador
                DBSession.add(newcondonacionoperador)

        else:
            if kw['para']=='Empresa':
                querycargooperador=DBSession.query(CargoOperador).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                if querycargooperador is None:
                    cargooperador=0
                else:
                    cargooperador=querycargooperador.id_cargooperador

                querycargounidad=DBSession.query(CargoUnidad).filter_by(cargo=kw['cargo']).filter_by(application_id=kw['application_id']).first()
                if querycargounidad is None:
                    cargounidad=0
                else:
                    cargounidad=querycargounidad.id_cargounidad

                querylinea = DBSession.query(Linea).filter_by(nombre_linea=kw['to']).filter_by(app_id=kw['application_id']).first()


                if cargooperador!=0:

                    queryoperadoresenlinea=DBSession.query(Persona).filter_by(esoperador=1).filter_by(id_linea=querylinea.id_linea).all()

                    for itemoperadorenlinea in queryoperadoresenlinea:

                        queryoperadoresconcargo=DBSession.query(OperadorCargo).filter_by(cargo_id=cargooperador).filter_by(operador_id=itemoperadorenlinea.id_persona).all()

                        for itemoperador in queryoperadoresconcargo:

                            querybody=DBSession.query(Body).filter_by(id_operador=itemoperador.operador_id).filter_by(id_cargopersona=cargooperador).\
                                filter(Body.fecha<=kw['fechafin']).filter(Body.fecha>=kw['fechaini']).all()

                            for itembody in querybody:
                                queryhead = DBSession.query(Head).filter_by(id_operador=itemoperador.operador_id).filter_by(fecha=itembody.fecha).filter(Head.faltante > 0).first()
                                if queryhead is not None:
                                    condonacionpasada = itembody.condonacion
                                    saldopasado = itembody.saldo
                                    itembody.condonacion = int(kw['monto'])
                                    itembody.saldo = itembody.monto - int(kw['monto'])
                                    if itembody.saldo < 0:
                                        itembody.condonacion = condonacionpasada
                                        itembody.saldo = saldopasado
                                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                                        Message.post("testlistener_" + kw['user'],'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                        #return dict(error="cantidadmayor")
                                    else:
                                        print("Si se puede")
                                        querybodies = DBSession.query(Body).filter_by(id_head=queryhead.id_head).filter_by(fecha=queryhead.fecha).filter_by(id_operador=queryhead.id_operador).all()
                                        saldoentero = 0
                                        for itembodies in querybodies:
                                            saldoentero = saldoentero + itembodies.saldo
                                        if saldoentero < queryhead.pagado:
                                            print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                            itembody.condonacion = condonacionpasada
                                            itembody.saldo = saldopasado
                                            #Message.post("testlistener_" + kw['user'],
                                            #             'MSG_' + 'RED' + "|" + "No se puede condonar")
                                            #return dict(error="cantidadmayor")
                                        else:
                                            queryhead.porpagar = saldoentero
                                            queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                            if queryhead.faltante == 0:
                                                queryhead.faltante = 0
                                                queryhead.porpagar = 0
                                                queryhead.estatus = 'CONDONADO'

                                else:
                                    print("noexistehead")
                                    #Message.post("testlistener_" + kw['user'],
                                    #             'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas")
                                    #return dict(error="noexistehead")
                elif cargounidad!=0:


                    queryunidadesenlinea=DBSession.query(DatosUnidad).filter_by(id_linea=querylinea.id_linea).all()

                    for itemunidadenlinea in queryunidadesenlinea:

                        queryunidadesconcargo = DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).filter_by(unidad_id=itemunidadenlinea.id_unidad).all()
                        for itemunidad in queryunidadesconcargo:
                            querybody=DBSession.query(Body).filter_by(id_unidad=itemunidad.unidad_id).filter_by(id_cargounidad=cargounidad).\
                                filter(Body.fecha<=kw['fechafin']).filter(Body.fecha>=kw['fechaini']).all()

                            for itembody in querybody:
                                queryhead = DBSession.query(Head).filter_by(id_unidad=itemunidad.unidad_id).filter_by(
                                    fecha=itembody.fecha).filter(Head.faltante > 0).first()
                                if queryhead is not None:
                                    condonacionpasada = itembody.condonacion
                                    saldopasado = itembody.saldo
                                    itembody.condonacion = int(kw['monto'])
                                    itembody.saldo = itembody.monto - int(kw['monto'])
                                    if itembody.saldo < 0:
                                        itembody.condonacion = condonacionpasada
                                        itembody.saldo = saldopasado
                                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                                        Message.post("testlistener_" + kw['user'],
                                                     'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                        return dict(error="cantidadmayor")
                                    else:
                                        print("Si se puede")
                                        querybodies = DBSession.query(Body).filter_by(id_head=queryhead.id_head).filter_by(fecha=queryhead.fecha).filter_by(id_unidad=queryhead.id_unidad).all()
                                        saldoentero = 0
                                        for itembodies in querybodies:
                                            saldoentero = saldoentero + itembodies.saldo
                                        if saldoentero < queryhead.pagado:
                                            print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                            itembody.condonacion = condonacionpasada
                                            itembody.saldo = saldopasado
                                            #Message.post("testlistener_" + kw['user'],
                                            #             'MSG_' + 'RED' + "|" + "No se puede condonar")
                                            #return dict(error="cantidadmayor")
                                        else:
                                            queryhead.porpagar = saldoentero
                                            queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                            if queryhead.faltante == 0:
                                                queryhead.faltante = 0
                                                queryhead.porpagar = 0
                                                queryhead.estatus = 'CONDONADO'

                                else:
                                    print("noexistehead")
                                    #Message.post("testlistener_" + kw['user'],
                                    #             'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas")

                                    #return dict(error="noexistehead")






                condonacion = Condonaciones()
                condonacion.cancelado = 0
                condonacion.fechainicial = kw['fechaini']
                condonacion.fechafinal = kw['fechafin']
                condonacion.descripcion = kw['descripcion']
                condonacion.para = 'Empresa'
                condonacion.monto = kw['monto']
                condonacion.internal_id = kw['internal_id']
                condonacion.application_id=kw['application_id']
                condonacion.usuario = kw['user']
                DBSession.add(condonacion)



                querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(descripcion = kw['descripcion']).filter_by(para='Empresa')\
                                .filter_by(monto=kw['monto']).filter_by(fechainicial=kw['fechaini']).filter_by(fechafinal=kw['fechafin'])\
                               .filter_by(internal_id=kw['internal_id']).filter_by(application_id=kw['application_id']).first()

                querycargosparaoperador=DBSession.query(OperadorCargo).filter_by(cargo_id=cargooperador).all()
                if querycargosparaoperador !=[]:
                    querylinea=DBSession.query(Linea).filter_by(nombre_linea=kw['to']).filter_by(app_id=kw['application_id']).first()
                    for item in querycargosparaoperador:
                        querypersona=DBSession.query(Persona).filter_by(id_persona=item.operador_id).filter_by(id_linea=querylinea.id_linea).first()
                        if querypersona is not None:
                            newcondonacionoperador=CondonacionOperador()
                            newcondonacionoperador.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionoperador.operador_id= querypersona.id_persona
                            newcondonacionoperador.cargooperador_id=cargooperador
                            DBSession.add(newcondonacionoperador)


                querycargosparaunidad=DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).all()
                if querycargosparaunidad !=[]:
                    querylinea=DBSession.query(Linea).filter_by(nombre_linea=kw['to']).filter_by(app_id=kw['application_id']).first()
                    for item in querycargosparaunidad:
                        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item.unidad_id).filter_by(id_linea=querylinea.id_linea).first()
                        if queryunidad is not None:
                            newcondonacionunidad=CondonacionUnidad()
                            newcondonacionunidad.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionunidad.unidad_id= queryunidad.id_unidad
                            newcondonacionunidad.cargounidad_id=cargounidad
                            DBSession.add(newcondonacionunidad)

            if kw['para']=='Derrotero':
                querycargooperador=DBSession.query(CargoOperador).filter_by(cargo=kw['cargo']).first()
                if querycargooperador is None:
                    cargooperador=0
                else:
                    cargooperador=querycargooperador.id_cargooperador

                querycargounidad=DBSession.query(CargoUnidad).filter_by(cargo=kw['cargo']).first()
                if querycargounidad is None:
                    cargounidad=0
                else:
                    cargounidad=querycargounidad.id_cargounidad

                queryderrotero = DBSession.query(Derrotero).filter_by(derrotero=kw['to']).filter_by(application_id=kw['application_id']).first()

                queryunidadesconderrotero=DBSession.query(DatosUnidad).filter_by(id_derrotero=queryderrotero.id_derrotero).all()

                for itemunidadenderrotero in queryunidadesconderrotero:

                    queryunidadesconcargo = DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).filter_by(unidad_id=itemunidadenderrotero.id_unidad).all()
                    for itemunidad in queryunidadesconcargo:
                        querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.unidad_id).filter_by(
                            id_cargounidad=cargounidad). \
                            filter(Body.fecha <= kw['fechafin']).filter(Body.fecha >= kw['fechaini']).all()

                        for itembody in querybody:
                            queryhead = DBSession.query(Head).filter_by(id_unidad=itemunidad.unidad_id).filter_by(
                                fecha=itembody.fecha).filter(Head.faltante > 0).first()
                            if queryhead is not None:
                                condonacionpasada = itembody.condonacion
                                saldopasado = itembody.saldo
                                itembody.condonacion = int(kw['monto'])
                                itembody.saldo = itembody.monto - int(kw['monto'])
                                if itembody.saldo < 0:
                                    itembody.condonacion = condonacionpasada
                                    itembody.saldo = saldopasado
                                    print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                                    Message.post("testlistener_" + kw['user'],
                                                 'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                    return dict(error="cantidadmayor")
                                else:
                                    print("Si se puede")
                                    querybodies = DBSession.query(Body).filter_by(id_head=queryhead.id_head).filter_by(fecha=queryhead.fecha).filter_by(id_unidad=queryhead.id_unidad).all()

                                    saldoentero = 0
                                    for itembodies in querybodies:
                                        saldoentero = saldoentero + itembodies.saldo
                                    if saldoentero < queryhead.pagado:
                                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                        itembody.condonacion = condonacionpasada
                                        itembody.saldo = saldopasado
                                        #Message.post("testlistener_" + kw['user'],
                                        #             'MSG_' + 'RED' + "|" + "No se puede condonar")
                                        #return dict(error="cantidadmayor")
                                    else:
                                        queryhead.porpagar = saldoentero
                                        queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                        if queryhead.faltante == 0:
                                            queryhead.faltante = 0
                                            queryhead.porpagar = 0
                                            queryhead.estatus = 'CONDONADO'

                            else:
                                print("noexistehead")
                                #Message.post("testlistener_" + kw['user'],
                                #             'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas")
                                #return dict(error="noexistehead")



                condonacion = Condonaciones()
                condonacion.cancelado = 0
                condonacion.descripcion = kw['descripcion']
                condonacion.para = 'Derrotero'
                condonacion.monto = kw['monto']
                condonacion.fechainicial = kw['fechaini']
                condonacion.fechafinal = kw['fechafin']
                condonacion.internal_id = kw['internal_id']
                condonacion.usuario = kw['user']
                condonacion.application_id=kw['application_id']
                DBSession.add(condonacion)

                querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(descripcion = kw['descripcion']).filter_by(para='Derrotero')\
                                .filter_by(monto=kw['monto']).filter_by(fechainicial=kw['fechaini']).filter_by(fechafinal=kw['fechafin'])\
                                .filter_by(internal_id=kw['internal_id']).filter_by(application_id=kw['application_id']).first()

                querycargosparaoperador=DBSession.query(OperadorCargo).filter_by(cargo_id=cargooperador).all()
                if querycargosparaoperador !=[]:
                    queryderrotero=DBSession.query(Derrotero).filter_by(derrotero=kw['to']).filter_by(application_id=kw['application_id']).first()
                    for item in querycargosparaoperador:
                        querypersona=DBSession.query(Persona).filter_by(id_persona=item.operador_id).filter_by(id_derrotero=queryderrotero.id_derrotero).first()
                        if querypersona is not None:
                            newcondonacionoperador=CondonacionOperador()
                            newcondonacionoperador.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionoperador.operador_id= querypersona.id_persona
                            newcondonacionoperador.cargooperador_id=cargooperador
                            DBSession.add(newcondonacionoperador)



                querycargosparaunidad=DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).all()
                if querycargosparaunidad !=[]:
                    queryderrotero=DBSession.query(Derrotero).filter_by(derrotero=kw['to']).first()
                    for item in querycargosparaunidad:
                        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item.unidad_id).filter_by(id_derrotero=queryderrotero.id_derrotero).first()
                        if queryunidad is not None:
                            newcondonacionunidad=CondonacionUnidad()
                            newcondonacionunidad.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionunidad.unidad_id= queryunidad.id_unidad
                            newcondonacionunidad.cargounidad_id=cargounidad
                            DBSession.add(newcondonacionunidad)

            if kw['para']=='Grupo':
                querycargooperador=DBSession.query(CargoOperador).filter_by(cargo=kw['cargo']).first()
                if querycargooperador is None:
                    cargooperador=0
                else:
                    cargooperador=querycargooperador.id_cargooperador

                querycargounidad=DBSession.query(CargoUnidad).filter_by(cargo=kw['cargo']).first()
                if querycargounidad is None:
                    cargounidad=0
                else:
                    cargounidad=querycargounidad.id_cargounidad

                querygrupo = DBSession.query(Grupo).filter_by(grupo=kw['to']).filter_by(application_id=kw['application_id']).first()

                querygrupounidad=DBSession.query(Grupo_DatosUnidad).filter_by(grupo_id=querygrupo.id_grupo).all()

                for itemgrupounidad in querygrupounidad:
                    #queryunidadescongrupo=DBSession.query(DatosUnidad).filter_by(id_unidad=itemgrupounidad.datosunidad_id).first()

                    queryunidadesconcargo = DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).filter_by(unidad_id=itemgrupounidad.datosunidad_id).all()
                    for itemunidad in queryunidadesconcargo:
                        querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.unidad_id).filter_by(
                            id_cargounidad=cargounidad). \
                            filter(Body.fecha <= kw['fechafin']).filter(Body.fecha >= kw['fechaini']).all()

                        for itembody in querybody:
                            queryhead = DBSession.query(Head).filter_by(id_unidad=itemunidad.unidad_id).filter_by(
                                fecha=itembody.fecha).filter(Head.faltante > 0).first()
                            if queryhead is not None:
                                condonacionpasada = itembody.condonacion
                                saldopasado = itembody.saldo
                                itembody.condonacion = int(kw['monto'])
                                itembody.saldo = itembody.monto - int(kw['monto'])
                                if itembody.saldo < 0:
                                    itembody.condonacion = condonacionpasada
                                    itembody.saldo = saldopasado
                                    print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                                    Message.post("testlistener_" + kw['user'],
                                                 'MSG_' + 'RED' + "|" + "No se puede condonar esa cantidad")
                                    return dict(error="cantidadmayor")
                                else:
                                    print("Si se puede")
                                    querybodies = DBSession.query(Body).filter_by(id_head=queryhead.id_head).filter_by(fecha=queryhead.fecha).filter_by(id_unidad=queryhead.id_unidad).all()

                                    saldoentero = 0
                                    for itembodies in querybodies:
                                        saldoentero = saldoentero + itembodies.saldo
                                    if saldoentero < queryhead.pagado:
                                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")

                                        itembody.condonacion = condonacionpasada
                                        itembody.saldo = saldopasado
                                        #Message.post("testlistener_" + kw['user'],
                                        #             'MSG_' + 'RED' + "|" + "No se puede condonar")
                                        #return dict(error="cantidadmayor")
                                    else:
                                        queryhead.porpagar = saldoentero
                                        queryhead.faltante = queryhead.porpagar - queryhead.pagado
                                        if queryhead.faltante == 0:
                                            queryhead.faltante = 0
                                            queryhead.porpagar = 0
                                            queryhead.estatus = 'CONDONADO'

                            else:
                                print("noexistehead")
                                #Message.post("testlistener_" + kw['user'],
                                #             'MSG_' + 'RED' + "|" + "No se puede condonar,no hay cargos entre las fechas")
                                #return dict(error="noexistehead")


                condonacion = Condonaciones()
                condonacion.cancelado = 0
                condonacion.descripcion = kw['descripcion']
                condonacion.para = 'Grupo'
                condonacion.monto = kw['monto']
                condonacion.fechainicial = kw['fechaini']
                condonacion.fechafinal = kw['fechafin']
                condonacion.internal_id = kw['internal_id']
                condonacion.application_id=kw['application_id']
                condonacion.usuario = kw['user']
                DBSession.add(condonacion)

                querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(descripcion = kw['descripcion']).filter_by(para='Grupo')\
                                .filter_by(monto=kw['monto']).filter_by(fechainicial=kw['fechaini']).filter_by(fechafinal=kw['fechafin'])\
                                .filter_by(internal_id=kw['internal_id']).filter_by(application_id=kw['application_id']).first()
                querycargosparaoperador=DBSession.query(OperadorCargo).filter_by(cargo_id=cargooperador).all()
                if querycargosparaoperador !=[]:
                    querygrupo=DBSession.query(Grupo).filter_by(grupo=kw['to']).filter_by(application_id=kw['application_id']).first()
                    for item in querycargosparaoperador:
                        querypersona=DBSession.query(Persona).filter_by(id_persona=item.operador_id).filter_by(id_grupo=querygrupo.id_grupo).first()
                        if querypersona is not None:
                            newcondonacionoperador=CondonacionOperador()
                            newcondonacionoperador.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionoperador.operador_id= querypersona.id_persona
                            newcondonacionoperador.cargooperador_id=cargooperador
                            DBSession.add(newcondonacionoperador)


                querycargosparaunidad=DBSession.query(UnidadCargo).filter_by(cargo_id=cargounidad).all()
                if querycargosparaunidad !=[]:
                    querygrupo=DBSession.query(Grupo).filter_by(grupo=kw['to']).first()
                    for item in querycargosparaunidad:
                        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item.unidad_id).filter_by(id_grupo=querygrupo.id_grupo).first()
                        if queryunidad is not None:
                            newcondonacionunidad=CondonacionUnidad()
                            newcondonacionunidad.condonacion_id=querycondonacion.id_condonacion
                            newcondonacionunidad.unidad_id= queryunidad.id_unidad
                            newcondonacionunidad.cargounidad_id=cargounidad
                            DBSession.add(newcondonacionunidad)

        DBSession.flush()
        return dict(error="ok")


    @expose('json')
    def showunitsoroperators(self,**kw):
        condouni=[]
        condoope=[]
        querycondonaciones=DBSession.query(Condonaciones).filter_by(id_condonacion=kw['id']).first()
        querycondonacionesuni=DBSession.query(CondonacionUnidad).filter_by(condonacion_id=querycondonaciones.id_condonacion).all()
        for item in querycondonacionesuni:
            queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item.unidad_id).first()
            condouni.append({'condonacion': item.condonacion_id,'eco': queryunidad.eco1})
        kw['querycondonacionesuni']=condouni
        kw['lencondouni']=len(condouni)
        print("len(condouni)=", len(condouni))

        querycondonacionesope=DBSession.query(CondonacionOperador).filter_by(condonacion_id=querycondonaciones.id_condonacion).all()
        for item in querycondonacionesope:
            querypersona=DBSession.query(Persona).filter_by(id_persona=item.operador_id).first()
            condoope.append({'condonacion': item.condonacion_id,'nombre': querypersona.nombre})
        kw['querycondonacionesope']=condoope
        kw['lencondoope']=len(condoope)
        print("len(condoope)=",len(condoope))

        template = render_template(kw, "mako", 'pythonjupiter.templates.Condonaciones.condonacion_show')
        return dict(template=template)

    @expose('json')
    def cancel(self,**kw):
        id=kw['id']
        querycondo=DBSession.query(Condonaciones).filter_by(id_condonacion=id).first()
        if querycondo.cancelado==0:
            querycondo.cancelado=1
            if querycondo.para=='Unidad':
                print("UNIDAD")
                querycondonacionunidad=DBSession.query(CondonacionUnidad).filter_by(condonacion_id=id).first()
                querybody=DBSession.query(Body).filter_by(id_unidad=querycondonacionunidad.unidad_id).\
                            filter_by(id_cargounidad=querycondonacionunidad.cargounidad_id).filter(Body.fecha>=querycondo.fechainicial).\
                            filter_by(Body.fecha<=querycondo.fechafinal).first()
            elif querycondo.para=='Operador':
                print("OPERADOR")
            else:
                print("OTRO")
        else:
            querycondo.cancelado=0
        return dict(error='ok')