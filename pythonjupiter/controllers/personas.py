from tg import expose
from tg import request
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import DatosUnidad,Linea,HistoricoUnidad,GrupoPersona_Persona
from pythonjupiter.model.tables import TipoLicencia,Persona,Punto,HistorialPersona,CargoOperador,OperadorCargo,GrupoPersona,Correos,Head
import os
import requests

from pythonjupiter.lib.base import BaseController
from base64 import b64encode

from shutil import copyfileobj
from datetime import datetime,timedelta, date
from tg import render_template
from pythonjupiter.lib.messagequeue import Message
from pythonjupiter.lib.helpers import urlsun, url
import time
import qrcode
import qrcode.image.svg
from io import BytesIO
from pythonjupiter.lib.utility import ExportPDF
import base64

from pythonjupiter.lib.jqgrid import jqgridDataGrabber

class PersonasTemplates(BaseController):

    @expose('json')
    def loadHistoric(self, **kw):
        filter = [('sobrecampo', 'eq', kw['sobrecampo'])]
        return jqgridDataGrabber(HistorialPersona, 'id_historialpersona', filter, kw).loadGrid()

    @expose('pythonjupiter.templates.Catalogs.historic_jqgrid')
    # @require(predicates.not_anonymous())
    def historic(self, **kw):
        return dict(page='historic_jqgrid',idreal='id_historialpersona',loadfrom='personas', internal_id=kw['internal_id'], sobrecampo=kw['sobrecampo'])

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Persona, 'id_persona', filter, kw).loadImageGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Persona, 'id_persona', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def personHistorial(self,dato,valor_anterior,valor_nuevo,sobrecampo,usuario):
        if valor_anterior != valor_nuevo :
            print("CAMBIO:", dato, valor_anterior, valor_nuevo)
            historialpersona = HistorialPersona()
            historialpersona.dato = dato
            historialpersona.valor_anterior = valor_anterior
            historialpersona.valor_nuevo = valor_nuevo
            historialpersona.sobrecampo = sobrecampo
            historialpersona.usuario = usuario
            DBSession.add(historialpersona)
            DBSession.flush()
            return dict(error="ok",id_historial=historialpersona.id_historialpersona)
        return dict(error="ok", id_historial=0)

    @expose('pythonjupiter.templates.PersonasTemplates.people_credential')
    def createCredential(self, **kw):
        person = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        enterprise = DBSession.query(Linea).filter_by(id_linea=person.id_linea).first()
        photoPeople = url() + "/img/personasilueta.gif"
        if str(person.foto) != "None":
            photoPeople = "data:image/png;base64," + str(b64encode(person.foto), 'utf-8')
        if enterprise != None:
            laempresa = enterprise.nombre_linea
        else:
            laempresa = person.app_name

        json = {"ID":person.id_persona,"Name":person.nombre,"OperatorNumber":person.numerooperador,"Nickname":person.apodo,"Cellphone":person.celular}
        qr_info = json
        qr = qrcode.QRCode(
            box_size=40,
            border=4
        )
        qr.add_data(qr_info)
        buf = BytesIO()
        img = qr.make_image(fill_color="black",back_color="transparent")
        img.save(buf)
        person.codigoqr = buf.getvalue()
        DBSession.flush()

        QRcode = "data:image/png;base64," + str(b64encode(buf.getvalue()), 'utf-8')

        return dict(page="credential",QRcode=QRcode,photoPeople=photoPeople,internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],application_id=kw['application_id'],persona=person,empresa=laempresa)

    @expose('pythonjupiter.templates.PersonasTemplates.people_files')
    def files(self, **kw):
        handler = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        photoPeople = url()+"/img/personasilueta.gif"
        licencePeople = url()+"/img/credencial.jpg"
        inePeople = url()+"/img/credencial.jpg"
        if handler != None:
            if str(handler.foto) != "None":
                photoPeople = "data:image/png;base64,"+str(b64encode(handler.foto), 'utf-8')
            if str(handler.licencia) != "None":
                licencePeople = "data:image/png;base64,"+str(b64encode(handler.licencia), 'utf-8')
            if str(handler.ine) != "None":
                inePeople = "data:image/png;base64,"+str(b64encode(handler.ine), 'utf-8')

        return dict(page='people_files',photoPeople=photoPeople,licencePeople=licencePeople,inePeople=inePeople)

    @expose('json')
    def save(self, **kw):
        i = 0
        list = {}
        people = DBSession.query(Persona).filter_by(id_persona=kw['jupiterPeople_id']).first()
        if people == None:
            i = 1
            people = Persona()
            people.estatus = 'ALTA'
            people.fecha_estatus = datetime.now()
            people.habilitadochecador = 0
            people.habilitadobeneficiario = 0
            people.habilitadooperador = 0
            people.habilitadopermisionario = 0
        else:
            list = {'jupiterPeople_id': people.id_persona,'jupiterPeople_nombre': people.nombre,'jupiterPeople_apodo': people.apodo,'jupiterPeople_rfc': people.rfc,
                    'jupiterPeople_banco2': people.banco2,'jupiterPeople_cta1': people.cta1,'jupiterPeople_fecha_nacimiento': people.fecha_nacimiento,'jupiterPeople_correo': people.correo,'jupiterPeople_sexo': people.sexo,
                    'jupiterPeople_cta2': people.cta2,'jupiterPeople_banco1': people.banco1,'jupiterPeople_id_punto': people.id_punto,'jupiterPeople_estadocivil': people.estadocivil,'jupiterPeople_direccion': people.direccion,'jupiterPeople_direccion2': people.direccion2,
                    'jupiterPeople_direccion3': people.direccion3,'jupiterPeople_telefono':people.telefono,'jupiterPeople_celular': people.celular,
                    'jupiterPeople_numerolicencia': people.numerolicencia,'jupiterPeople_otro_celular': people.otro_celular,'jupiterPeople_curp': people.curp,'jupiterPeople_observaciones': people.observaciones,
                    'estadodelicencia': people.estadodelicencia,'jupiterPeople_numerooperador': people.numerooperador,'jupiterPeople_eschecador': people.eschecador,'jupiterPeople_esbeneficiario': people.esbeneficiario,
                    'fecha_vencimientolicencia': people.fecha_vencimientolicencia,'jupiterPeople_id_tipolicencia': people.id_tipolicencia,'jupiterPeople_numero_segurosocial': people.numero_segurosocial,'jupiterPeople_usuario': people.usuario,'jupiterPeople_espermisionario': people.espermisionario,'habilitadochecador': people.habilitadochecador,'habilitadobeneficiario': people.habilitadobeneficiario,'habilitadooperador': people.habilitadooperador,'jupiterPeople_internal_id': people.internal_id,'application_id': people.application_id,'app_name': people.app_name}

        foto = kw["foto"]
        if foto != "undefined":
            if foto.file:
                fileName = foto.filename
                if fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".png") > 0:
                    ftyoe = "image"
                else:
                    return dict(error="Formato de imagen no valido")
        ine = kw["ine"]
        if ine != "undefined":
            if ine.file:
                fileName = ine.filename
                if fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".png") > 0:
                    ftyoe = "image"
                else:
                    return dict(error="Formato de imagen no valido")
        licencia = kw["licencia"]
        if licencia != "undefined":
            if licencia.file:
                fileName = licencia.filename
                if fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".png") > 0:
                    ftyoe = "image"
                else:
                    return dict(error="Formato de imagen no valido")

        kw['habilitadochecador'] = 0
        people.habilitadochecador = 0
        kw['jupiterPeople_eschecador'] = 0
        kw['habilitadobeneficiario'] = 0
        people.habilitadobeneficiario = 0
        kw['jupiterPeople_esbeneficiario'] = 0
        kw['habilitadooperador'] = 0
        people.habilitadooperador = 0
        kw['jupiterPeople_esoperador'] = 0
        kw['habilitadopermisionario'] = 0
        people.habilitadopermisionario = 0
        kw['jupiterPeople_espermisionario'] = 0

        kw['fecha_vencimientolicencia'] = None
        kw['estadodelicencia'] = None

        people.nombre = kw['jupiterPeople_nombre']
        people.apodo = kw['jupiterPeople_apodo']
        if foto != "undefined":
            people.foto = foto.file.read()
        people.rfc = kw['jupiterPeople_rfc']
        people.fecha_nacimiento = kw['jupiterPeople_fecha_nacimiento']
        people.correo = kw['jupiterPeople_correo']
        people.sexo = kw['jupiterPeople_sexo']
        people.estadocivil = kw['jupiterPeople_estadocivil']
        people.direccion = kw['jupiterPeople_direccion']
        people.direccion2 = kw['jupiterPeople_direccion2']
        people.direccion3 = kw['jupiterPeople_direccion3']
        people.telefono = kw['jupiterPeople_telefono']
        people.telefono2 = kw['jupiterPeople_telefono2']
        people.celular = kw['jupiterPeople_celular']
        people.observaciones = kw['jupiterPeople_observaciones']
        people.otro_celular = kw['jupiterPeople_otro_celular']
        people.curp = kw['jupiterPeople_curp']
        people.fecha_ingreso = kw['jupiterPeople_fecha_ingreso']

        if ine != "undefined":
            people.ine = ine.file.read()

        people_es = kw['people_es'].split(",")
        if "jupiterPeople_eschecador" in people_es:
            people.eschecador = 1
            if kw['jupiterPeople_id'] == 0:
                kw['habilitadochecador'] = 1
                people.habilitadochecador = 1

            id_punto = DBSession.query(Punto).filter_by(id_punto=kw['jupiterPeople_id_punto']).first()
            if id_punto != None:
                people.id_punto = id_punto.id_punto
            else:
                return dict(error="Linea No Valida")
        else:
            people.eschecador = 0

        if "jupiterPeople_esbeneficiario" in people_es:
            kw['jupiterPeople_esbeneficiario'] = 1
            people.esbeneficiario = 1
            if kw['jupiterPeople_id'] == 0:
                kw['habilitadobeneficiario'] = 1
                people.habilitadobeneficiario = 1

            people.cta1 = kw['jupiterPeople_cta1']
            people.banco1 = kw['jupiterPeople_banco1']
            people.cta2 = kw['jupiterPeople_cta2']
            people.banco2 = kw['jupiterPeople_banco2']
        else:
            people.esbeneficiario = 0

        if "jupiterPeople_esoperador" in people_es:
            people.esoperador = 1
            if kw['jupiterPeople_id'] == 0:
                kw['habilitadooperador'] = 1
                people.habilitadooperador = 1

            if kw['jupiterPeople_opnum'] == "SI":
                if kw['jupiterPeople_numerooperador'] != "":
                    people.numerooperador = kw['jupiterPeople_numerooperador']
                else:
                    return dict(error="Número de Operador No Valido")
            else:
                people.numerooperador = 0

            people.numero_segurosocial = kw['jupiterPeople_numero_segurosocial']
            people.numerolicencia = kw['jupiterPeople_numerolicencia']

            id_tipolicencia = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=kw['jupiterPeople_id_tipolicencia']).first()
            if id_tipolicencia != None:
                people.id_tipolicencia = id_tipolicencia.id_tipolicencia
            else:
                return dict(error="Tipo de Licencia No Valido")

            id_linea = DBSession.query(Linea).filter_by(id_linea=kw['jupiterPeople_id_linea']).first()
            if id_linea != None:
                people.id_linea = id_linea.id_linea
            else:
                return dict(error="Linea No Valida")

            if kw['jupiterPeople_fecha_vencimientolicencia'] != "":
                people.fecha_vencimientolicencia = datetime.strptime(kw['jupiterPeople_fecha_vencimientolicencia'], '%Y-%m-%d').date()
                kw['fecha_vencimientolicencia'] = people.fecha_vencimientolicencia
            else:
                return dict(error="Fecha de Vencimiento de Licencia No Valida")

            if licencia != "undefined":
                people.licencia = licencia.file.read()
        else:
            people.esoperador = 0

        if "jupiterPeople_espermisionario" in people_es:
            people.espermisionario = 1
            if kw['jupiterPeople_id'] == 0:
                kw['habilitadopermisionario'] = 1
                people.habilitadopermisionario = 1
            people.usuario = kw['jupiterPeople_usuario']
        else:
            people.espermisionario = 0


        people.internal_id = kw['jupiterPeople_internal_id']
        people.application_id = kw['application_id']
        people.app_name = kw['app_name']

        if i == 1:
            DBSession.add(people)
        DBSession.flush()

        if "jupiterPeople_esoperador" in people_es:
            all = DBSession.query(GrupoPersona_Persona).filter_by(persona_id=kw['jupiterPeople_id']).all()
            for item in all:
                DBSession.delete(item)

            if 'jupiterPeople_id_grupopersona' in kw:
                for id in kw['jupiterPeople_id_grupopersona']:
                    id_grupo_persona= GrupoPersona_Persona()
                    get_id = DBSession.query(GrupoPersona).filter_by(id_grupopersona=id).first()
                    if get_id != None:
                        id_grupo_persona.grupo_id = get_id.id_grupopersona
                        id_grupo_persona.persona_id = people.id_persona
                        DBSession.add(id_grupo_persona)
                        DBSession.flush()
            if kw['jupiterPeople_fecha_vencimientolicencia'] != "":
                people.estadodelicencia = self.calculalicencia(people)
                kw['estadodelicencia'] = people.estadodelicencia
        DBSession.flush()

        if i == 0:
            for attr, value in kw.items():
                dato = attr.replace("jupiterPeople_","")
                dato = dato.replace("_", " ")
                dato = dato.capitalize()
                if attr in list:
                    cambio = self.personHistorial(dato=dato,valor_anterior=list[attr],valor_nuevo=kw[attr],sobrecampo=people.id_persona,usuario=kw['jupiterPeople_owner'])

        return dict(error="ok")

    @expose('json')
    def form(self, **kw):
        kw['licencetype'] = []
        kw['company'] = []
        kw['group'] = []
        kw['checkpoint'] = []

        kw['habilitadochecador'] = ""
        kw['habilitadobeneficiario'] = ""
        kw['habilitadooperador'] = ""
        kw['habilitadopermisionario'] = ""

        kw['habchec'] = "hidden"
        kw['habben'] = "hidden"
        kw['habope'] = "hidden"
        kw['habper'] = "hidden"
        kw['opnush'] = "hidden"

        kw['female'] = ""
        kw['male'] = ""
        kw['opnus'] = ""
        kw['opnun'] = ""
        kw['single'] = ""
        kw['married'] = ""
        kw['divorced'] = ""
        kw['widower'] = ""

        if kw['id'] == "0":
            kw['handler'] = Persona()
            kw['handler'].fecha_ingreso = date.today()
        else:
            kw['handler'] = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
            if kw['handler'].sexo == "MASCULINO":
                kw['male'] = "selected"
            else:
                kw['female'] = "selected"

            if kw['handler'].estadocivil == "Soltero(a)":
                kw['single'] = "selected"
            if kw['handler'].estadocivil == "Casado(a)":
                kw['married'] = "selected"
            if kw['handler'].estadocivil == "Divorsado(a)":
                kw['divorced'] = "selected"
            if kw['handler'].estadocivil == "Viudo(a)":
                kw['widower'] = "selected"
            print(kw['handler'].numerooperador)
            if kw['handler'].numerooperador != 0:
                kw['opnus'] = "selected"
                kw['opnush'] = ""
            else:
                kw['opnun'] = "selected"

        if kw['handler'].eschecador == 1:
            kw['habilitadochecador'] = "checked"
            kw['habchec'] = ""

        if kw['handler'].esbeneficiario == 1:
            kw['habilitadobeneficiario'] = "checked"
            kw['habben'] = ""

        if kw['handler'].esoperador == 1:
            kw['habilitadooperador'] = "checked"
            kw['habope'] = ""

        if kw['handler'].espermisionario == 1:
            kw['habilitadopermisionario'] = "checked"
            kw['habper'] = ""

        handler = DBSession.query(TipoLicencia).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_tipolicencia == kw['handler'].id_tipolicencia:
                    kw['licencetype'].append({'tipo_licencia': item.tipo_licencia, 'id': item.id_tipolicencia, 'checked': 'selected'})
                else:
                    kw['licencetype'].append({'tipo_licencia': item.tipo_licencia, 'id': item.id_tipolicencia, 'checked': ''})
            else:
                kw['licencetype'].append({'tipo_licencia': item.tipo_licencia, 'id': item.id_tipolicencia, 'checked': ''})

        handler = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_linea == kw['handler'].id_linea:
                    kw['company'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': 'selected'})
                else:
                    kw['company'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': ''})
            else:
                kw['company'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': ''})

        groups = []
        all_groups = DBSession.query(GrupoPersona_Persona).filter_by(persona_id=kw['id']).all()
        for item in all_groups:
            groups.append(item.grupo_id)

        handler = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_grupopersona in groups:
                    kw['group'].append({'grupopersona': item.grupopersona, 'id': item.id_grupopersona, 'checked': 'checked'})
                else:
                    kw['group'].append({'grupopersona': item.grupopersona, 'id': item.id_grupopersona, 'checked': ''})
            else:
                kw['group'].append({'grupopersona': item.grupopersona, 'id': item.id_grupopersona, 'checked': ''})

        handler = DBSession.query(Punto).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_punto == kw['handler'].id_punto:
                    kw['checkpoint'].append({'punto': item.punto, 'id': item.id_punto, 'checked': 'checked'})
                else:
                    kw['checkpoint'].append({'punto': item.punto, 'id': item.id_punto, 'checked': ''})
            else:
                kw['checkpoint'].append({'punto': item.punto, 'id': item.id_punto, 'checked': ''})

        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.PersonasTemplates.people_form')
        return dict(dialogtemplate=dialogtemplate)

    # PERSONAS MARIA
    # @expose('pythonjupiter.templates.PersonasTemplates.people_jqgrid')
    # def personas(self, **kw):
    #     return dict(page='people_jqgrid', internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])

    # PERSONAS DIEGO
    @expose('pythonjupiter.templates.PersonasTemplates.personas')
    def personas(self, **kw):
        list = []
        internal_id = kw['internal_id']
        empresa = kw['app_name']
        app_id=kw['application_id']

        if internal_id == '0':
            handler = DBSession.query(Persona).all()
        else:
            handler = DBSession.query(Persona).filter_by(application_id=app_id).filter_by(esoperador=1).all()
        hd = []
        for item in handler:
            item.estadodelicencia=self.calculalicencia(item)
            querytipolicencia = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=item.id_tipolicencia).first()
            querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()

            varhabilitado=''


            if (item.habilitadooperador == 1):
                varhabilitado = "ACTIVA"
            else:
                varhabilitado = "DESACTIVADA"

            if querylinea is not None:
                variablelinea = querylinea.nombre_linea
            else:
                variablelinea = ''

            if querytipolicencia is not None:
                variablelicencia = querytipolicencia.tipo_licencia
            else:
                variablelicencia = ''



            hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,"estatus": item.estatus,
                       "empresa": variablelinea, "rfc": item.rfc, "telefono": item.telefono,"celular": item.celular,"licencia": variablelicencia,
                       "fecha_vencimiento": item.fecha_vencimientolicencia,"estadodelicencia": item.estadodelicencia,"habilitado": varhabilitado,"observaciones": item.observaciones})

        contador = len(hd)
        return dict(page='personas', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'],user=kw['user'], application_id=kw['application_id'],contador=contador)

    def edita_archivolicencia(self,file,filename,internal_id,antes,id,acumula):
        archivo=''
        if file != b'':
            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-"
            try:
                os.makedirs(destino)
            except FileExistsError:
                pass
            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+ str(id) + ".-/" + file.filename.lstrip(os.sep)
            with open(destino, 'wb') as fdestino:
                copyfileobj(file.file, fdestino)
                print("Archivo de lic copiado")
            fdestino.close()
            #file.close()

            (this_file_name, this_file_extension) = os.path.splitext(filename)
            if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension == '.pdf'):

                f = open(ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/"+ filename, 'rb').read()
                try:
                    f2 = open(ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/" + antes,'rb').read()
                    ok=1
                except FileNotFoundError:
                    ok=0
                except TypeError:
                    ok = 0
                    if acumula==2:
                        print("filenotfound")
                        return 'filenotfound'
                    else:
                        print("faltauno")
                        return 'faltauno'

                except IsADirectoryError:
                    f2='nohay'
                    ok=1
                if(f!=f2):
                    if ok==1:
                        if acumula==2:
                            try:
                                os.remove(ruta +"pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/" + antes)
                            except IsADirectoryError:
                                pass
                            print("DISTINTOS F Y F2")
                            return 'distintos'
                        else:
                            print("faltauno")
                            return 'faltauno'
                else:
                    print("IGUALES F Y F2")
                    return 'iguales'
        print("VACIO FILE")
        return 'vacio'

    def edita_archivo(self,file,filename,internal_id,antes,id):
        archivo=''
        if file != b'':
            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-"
            try:
                os.makedirs(destino)
            except FileExistsError:
                pass

            #print(destino)
            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+ str(id) + ".-/" + file.filename.lstrip(os.sep)
            with open(destino, 'wb') as fdestino:
                copyfileobj(file.file, fdestino)
                print("Archivo copiado")
            fdestino.close()
            #file.close()

            (this_file_name, this_file_extension) = os.path.splitext(filename)
            if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension == '.pdf'):

                f = open(ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/"+ filename, 'rb').read()
                try:
                    f2 = open(ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/" + antes,'rb').read()
                    ok=1
                except FileNotFoundError:
                    ok=0
                    return 'filenotfound'
                except TypeError:
                    ok=0
                    return 'filenotfound'

                if(f!=f2):
                    if ok==1:
                        os.remove(ruta +"pythonjupiter/public/img/"+internal_id+"/personas/"+str(id)+".-/" + antes)
                        print("DISTINTOS F Y F2")
                        return 'distintos'
                else:
                    print("IGUALES F Y F2")
                    return 'iguales'
        print("VACIO FILE")
        return 'vacio'

    def ingresa_archivo(self,file,filename,internal_id,contador):
        archivo=''
        if file != b'':
            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep

            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+str(contador)+".-"
            #print(destino)
            try:
                os.makedirs(destino)
            except FileExistsError:
                pass
            destino = ruta + "pythonjupiter/public/img/"+internal_id+"/personas/"+ str(contador) + ".-/" + file.filename.lstrip(os.sep)
            with open(destino, 'wb') as fdestino:
                copyfileobj(file.file, fdestino)
                print("Archivo copiado")
            fdestino.close()
            #file.close()

            # (this_file_name, this_file_extension) = os.path.splitext(filename)
            # if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg'):
            #      print("ES IMAGEN")
            #      print(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename)
            #      f = open(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename, 'rb').read()

        return filename

    def calculalicencia(self,item):
        print(item.fecha_vencimientolicencia)
        if item.fecha_vencimientolicencia != "":
            hoy=datetime.now().date()
            unmes=hoy + timedelta(days=30)

            if item.fecha_vencimientolicencia<hoy:
                return "VENCIO"

            if item.fecha_vencimientolicencia<=unmes and item.fecha_vencimientolicencia>=hoy:
                return "VENCERA"

            if item.fecha_vencimientolicencia>unmes:
                return "LEJOS"

    # @expose('pythonjupiter.templates.PersonasTemplates.personas')
    # def personas(self, **kw):
    #     list = []
    #     internal_id = kw['internal_id']
    #     empresa = kw['app_name']
    #     app_id = kw['application_id']
    #
    #     if internal_id == '0':
    #         handler = DBSession.query(Persona).all()
    #     else:
    #         handler = DBSession.query(Persona).filter_by(application_id=app_id).filter_by(esoperador=1).all()
    #     hd = []
    #     for item in handler:
    #         item.estadodelicencia = self.calculalicencia(item)
    #         querytipolicencia = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=item.id_tipolicencia).first()
    #         querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
    #
    #         varhabilitado = ''
    #
    #         if (item.habilitadooperador == 1):
    #             varhabilitado = "ACTIVA"
    #         else:
    #             varhabilitado = "DESACTIVADA"
    #
    #         if querylinea is not None:
    #             variablelinea = querylinea.nombre_linea
    #         else:
    #             variablelinea = ''
    #
    #         if querytipolicencia is not None:
    #             variablelicencia = querytipolicencia.tipo_licencia
    #         else:
    #             variablelicencia = ''
    #
    #         hd.append({"numerooperador": item.numerooperador, "id_persona": item.id_persona, "persona": item.nombre,
    #                    "apodo": item.apodo, "foto": item.foto, "estatus": item.estatus,
    #                    "empresa": variablelinea, "rfc": item.rfc, "telefono": item.telefono, "celular": item.celular,
    #                    "licencia": variablelicencia,
    #                    "fecha_vencimiento": item.fecha_vencimientolicencia, "estadodelicencia": item.estadodelicencia,
    #                    "habilitado": varhabilitado, "observaciones": item.observaciones})
    #
    #     contador = len(hd)
    #     return dict(page='personas', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'],
    #                 user=kw['user'], application_id=kw['application_id'], contador=contador)

    @expose('pythonjupiter.templates.PersonasTemplates.nuevapersona')
    def nuevapersona(self,**kw):
        allbase=DBSession.query(Punto).filter_by(application_id=kw['application_id']).all()
        typelicence=DBSession.query(TipoLicencia).filter_by(application_id=kw['application_id']).all()
        typelinea=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        typegrupo = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()

        try:
            cargosempresa = DBSession.query(CargoOperador).filter_by(empresa=typelinea[0].nombre_linea).filter_by(application_id=kw['application_id']).all()
        except:
            cargosempresa = {}
        #cargosgrupo=DBSession.query(CargoOperador).filter_by(grupopersona=typegrupo[0].grupopersona).filter_by(internal_id=kw['internal_id']).all()

        #querywhatapp_name=DBSession.query(Linea).filter_by(nombre_linea=typelinea[0].nombre_linea).filter_by(internal_id=kw['internal_id']).first()
        #cargosapp_name= DBSession.query(CargoOperador).filter_by(empresa=None).filter_by(internal_id=kw['internal_id']).all()

        cargosempresatodas = DBSession.query(CargoOperador).filter_by(empresa='TODAS').filter_by(application_id=kw['application_id']).all()
        #cargosgrupostodas = DBSession.query(CargoOperador).filter_by(grupopersona='TODAS').filter_by(internal_id=kw['internal_id']).all()

        #cargostodas=DBSession.query(CargoOperador).filter_by(internal_id=kw['internal_id']).all()

        construiraux = []
        construir = []

        for item in cargosempresa:
            if item.cargo in construiraux:
                pass
            else:
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        # for item in cargosgrupo:
        #     if item.cargo in construiraux:
        #         pass
        #     else:
        #         construiraux.append(item.cargo)
        #         construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        for item in cargosempresatodas:
            if item.cargo in construiraux:
                pass
            else:
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        # for item in cargosgrupostodas:
        #     if item.cargo in construiraux:
        #         pass
        #     else:
        #         construiraux.append(item.cargo)
        #         construir.append({'cargo': item.cargo, 'monto': item.montodefault})


        return dict(page='personas',allbase=allbase,typelicence=typelicence,typelinea=typelinea,internal_id=kw['internal_id'], app_name=kw['app_name'],user=kw['user'], application_id=kw['application_id'],
                    construir=construir,typegrupo=typegrupo)

    @expose('json')
    def uploadPersona(self,**kw):
        listagrupos=[]
        for item in kw:
            querygrupo=DBSession.query(GrupoPersona).filter_by(grupopersona=item).filter_by(application_id=kw['application_id']).first()
            if querygrupo is not None:
                listagrupos.append(item)
        #print(listagrupos)
        querypersonaexiste=DBSession.query(Persona).filter_by(nombre=kw['jupiterpersona_nombre']).filter_by(application_id=kw['application_id']).first()
        if querypersonaexiste is None:
            newpersona=Persona()
            if kw['tienenumerooperadorq'] == 'si':
                querynumerodeoperadordisponible = DBSession.query(Persona).filter_by(numerooperador=kw['numerodeoperadorasignado']).filter_by(application_id=kw['application_id']).first()
                if querynumerodeoperadordisponible is not None:
                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "El No. de Operador ya Existe")
                    return ''
                else:
                    try:
                        print("checked operador: ", kw['jupiterpersona_esoperador'])
                        uno=1
                    except:
                        uno=0

                    if uno==1:
                        newpersona.numerooperador=kw['numerodeoperadorasignado']
            else:
                queryallnumerooperador=DBSession.query(Persona).filter_by(application_id=kw['application_id']).all()
                mayor=0
                if queryallnumerooperador == []:
                    newpersona.numerooperador=mayor+1
                else:
                    for item in queryallnumerooperador:
                        auxmayor=item.numerooperador
                        try:
                            if auxmayor>mayor:
                                mayor=auxmayor
                        except:
                            pass
                        try:
                            print("checked operador: ", kw['jupiterpersona_esoperador'])
                            uno=1
                        except:
                            uno=0

                        if uno==1:
                            newpersona.numerooperador=mayor+1
            newpersona.nombre=kw['jupiterpersona_nombre']
            newpersona.apodo=kw['jupiterpersona_apodo']
            #fotoo
            getlastid=DBSession.query(Persona).all()
            last=0
            for item in getlastid:
                last=item.id_persona

            last=last+1
            try:
                file = request.POST['jupiter_fotopersona']
                filename=request.params["jupiter_fotopersona"].filename
                newpersona.foto=self.ingresa_archivo(file,filename,kw['internal_id'],last)
            except:
                pass

            newpersona.rfc=kw['jupiterpersona_rfc']
            newpersona.fecha_nacimiento=kw['jupiterpersona_fecha_nacimiento']
            newpersona.correo=kw['jupiterpersona_email']
            newpersona.sexo=kw['jupiterpersona_genero']
            newpersona.estadocivil=kw['jupiterpersona_estadocivil']
            newpersona.estatus='ALTA'
            newpersona.direccion=kw['jupiterpersona_address']
            newpersona.direccion2=kw['jupiterpersona_address2']
            newpersona.direccion3=kw['jupiterpersona_address3']
            newpersona.telefono=kw['jupiterpersona_phone']
            newpersona.telefono2=kw['jupiterpersona_phone2']
            newpersona.celular=kw['jupiterpersona_mobilephone']
            newpersona.otro_celular=kw['jupiterpersona_mobilephone2']
            newpersona.curp=kw['jupiterpersona_curp']
            #archivo ine
            try:
                fileine = request.POST['jupiterpersona_ine']
                filenameine = request.params["jupiterpersona_ine"].filename
                newpersona.ine=self.ingresa_archivo(fileine,filenameine,kw['internal_id'],last)
            except:
                pass


            newpersona.observaciones=kw['jupiterpersona_observaciones']
            newpersona.habilitadooperador=1
            newpersona.habilitadobeneficiario=1
            newpersona.habilitadochecador=1
            newpersona.habilitadopermisionario=1
            newpersona.internal_id=kw['internal_id']
            newpersona.application_id=kw['application_id']
            newpersona.app_name=kw['app_name']
            try:
                print("checked operador: ",kw['jupiterpersona_esoperador'])
                newpersona.esoperador=1
                newpersona.numero_segurosocial=kw['jupiteroperadores_noss']
                #licenciaarchivo
                file = request.POST['jupiteroperadores_licencearchivo']
                try:
                    filename = request.params["jupiteroperadores_licencearchivo"].filename
                    newpersona.licencia=self.ingresa_archivo(file,filename,kw['internal_id'],last)
                except:
                    filename=''

                newpersona.numerolicencia=kw['jupiteroperadores_licence']
                newpersona.fecha_vencimientolicencia=kw['jupiteroperadores_duedate']
                newpersona.estadodelicencia=''
                print(kw['application_id'])
                print(kw['jupiteroperadores_licencia'])
                querylicencia=DBSession.query(TipoLicencia).filter_by(application_id=kw['application_id']).filter_by(tipo_licencia=kw['jupiteroperadores_licencia']).first()
                print(querylicencia)
                newpersona.id_tipolicencia=querylicencia.id_tipolicencia
                querylinea=DBSession.query(Linea).filter_by(app_id=kw['application_id']).filter_by(nombre_linea=kw['jupiteroperadores_linea']).first()
                newpersona.id_linea=querylinea.id_linea
                # # if kw['jupiteroperadores_grupo']!='Sin Grupo':
                # #     querygrupo=DBSession.query(GrupoPersona).filter_by(grupopersona=kw['jupiteroperadores_grupo']).first()
                # #     newpersona.id_grupopersona=querygrupo.id_grupopersona
                # else:
                #     newpersona.id_grupopersona=0
            except KeyError:
                print("no es operador")
                newpersona.esoperador = 0
                newpersona.numero_segurosocial=''
                newpersona.numerolicencia=''
                newpersona.estadodelicencia = ''
                newpersona.licencia=''

            try:

                if kw['jupiterpersona_espermisionario']=='on':
                    print("checked permisionario: ", kw['jupiterpersona_espermisionario'])
                    newpersona.espermisionario=1

                    if len(kw['jupiter_theuserpermi'])>0:
                        newpersona.usuario = kw['jupiter_theuserpermi']
                        # service to SUN
                        url = urlsun() + '/services/create_group?group_name=Permisionarios&display_name=Permisionarios%20Transporte%20Publico&internal_id='+kw['internal_id']
                        # https://sun.dudewhereismy.mx/services/create_group?group_name=Permisionarios&display_name=Permisionarios%20Transporte%20Publico&internal_id=5
                        print("INTERNAL__ID----->",kw['internal_id'])
                        print(url)
                        r = requests.get(url, auth=('manager', 'managepass'))
                        print(r)
                        readjson = r.json()
                        print(readjson)
                        if readjson['error'] == 'ok':
                            group_id = readjson['group_id']
                            # https://sun.dudewhereismy.mx/services/create_user?user_name=Permisionario1&display_name=Permisionario&internal_id=5&group_ids=16,&email_address=as@d.mx&application_id=1422&password=1234
                            url2 = urlsun() + '/services/create_user?user_name=' + kw['jupiter_theuserpermi'] + '&display_name=' + \
                                kw['jupiterpersona_nombre'] + '&internal_id=' + kw['internal_id'] + '&group_ids=' + str(group_id) + \
                                ',&email_address=' + kw['jupiterpersona_email'] + '&application_id='+kw['application_id']+'&password=permi' + \
                                kw['jupiter_theuserpermi']

                            r2 = requests.get(url2, auth=('manager', 'managepass'))
                            readjson2 = r2.json()
                            if readjson2['error'] != 'ok':
                                Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + readjson2['error'])
                                time.sleep(2)
                                return ''

                            else:
                                Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cuenta Creada")
                                time.sleep(2)
                                #return ''
                        else:
                            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + readjson['error'])
                            time.sleep(2)
                            return ''

                else:
                    print("no es permisionario")
                    newpersona.espermisionario = 0

            except KeyError:
                newpersona.espermisionario = 0


            try:
                print("checked beneficiario: ", kw['jupiterpersona_esbeneficiario'])
                newpersona.esbeneficiario=1
                newpersona.cta1=kw['jupiter_cuenta1']
                newpersona.banco1 = kw['jupiter_banco1']
                newpersona.cta2 = kw['jupiter_cuenta2']
                newpersona.banco2 = kw['jupiter_banco2']
            except KeyError:
                print("no es beneficiario")
                newpersona.esbeneficiario = 0


            try:
                print("checked cehcador: ", kw['jupiterpersona_eschecador'])
                #print(kw['jupiterchecador_base'])
                newpersona.eschecador=1
                querypunto=DBSession.query(Punto).filter_by(application_id=kw['application_id']).filter_by(punto=kw['jupiterchecador_base']).first()
                newpersona.id_punto=querypunto.id_punto
            except KeyError:
                print("no es checador")
                newpersona.eschecador = 0



            DBSession.add(newpersona)

        #print('user=',kw['user'])
            querypersona = DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(esoperador=1).filter_by(nombre=kw['jupiterpersona_nombre']).filter_by(apodo=kw['jupiterpersona_apodo']).first()
            if querypersona is not None:
                querycorreopersona = DBSession.query(Correos).filter_by(id_persona=querypersona.id_persona).first()
                if querycorreopersona is None:
                    newcorreo=Correos()
                    newcorreo.correo=kw['jupiterpersona_email']
                    newcorreo.id_persona=querypersona.id_persona
                    DBSession.add(newcorreo)

                for itemlistagrupo in listagrupos:
                    querygrupopersona=DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).filter_by(grupopersona=itemlistagrupo).first()
                    newgrupopersona_persona=GrupoPersona_Persona()
                    newgrupopersona_persona.grupo_id=querygrupopersona.id_grupopersona
                    newgrupopersona_persona.persona_id=querypersona.id_persona
                    DBSession.add(newgrupopersona_persona)
                for item in kw:
                    if 'montode' in item:
                        cargo = item.split('montode')
                        querycargo = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=cargo[1]).first()
                        queryexisteoperadorcargo=DBSession.query(OperadorCargo).filter_by(cargo_id=querycargo.id_cargooperador)\
                                                .filter_by(operador_id=querypersona.id_persona).first()
                        if queryexisteoperadorcargo is None:
                            newoperadorcargo = OperadorCargo()
                            querycontador = DBSession.query(OperadorCargo).all()
                            contador = 0
                            for item2 in querycontador:
                                contador = int(item2.id)
                            contador = contador + 1
                            newoperadorcargo.id = contador
                            newoperadorcargo.cargo_id = querycargo.id_cargooperador
                            newoperadorcargo.operador_id = querypersona.id_persona
                            try:
                                newoperadorcargo.monto = int(kw[item])
                            except ValueError:
                                newoperadorcargo.monto=0
                            DBSession.add(newoperadorcargo)
                            DBSession.flush()

            else:
                pass
        else:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "Persona Ya Registrada en el Sistema")
            return ''

        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Persona Ingresada")
        return ''

    @expose('pythonjupiter.templates.PersonasTemplates.editpersona')
    def editpersona(self,**kw):
        allbase=DBSession.query(Punto).filter_by(application_id=kw['application_id']).all()
        typelicence=DBSession.query(TipoLicencia).filter_by(application_id=kw['application_id']).all()
        typelinea=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        toedit = DBSession.query(Persona).filter_by(id_persona=kw['id']).filter_by(application_id=kw['application_id']).first()

        typegrupo = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()

        variablelinea=''
        variabletipolicencia=''
        variabless=''
        variablelicencia=''
        variablenumerolicencia=''
        variablefechavencimineto=''
        variablepunto = ''
        usuario=''
        listadegrupos = []

        if toedit.esoperador==1:
            valueoflinea = DBSession.query(Linea).filter_by(id_linea=toedit.id_linea).first()
            valueoftipolicencia=DBSession.query(TipoLicencia).filter_by(id_tipolicencia=toedit.id_tipolicencia).first()
            valueofgrupo=DBSession.query(GrupoPersona_Persona).filter_by(persona_id=toedit.id_persona).all()
            for itemgrupo in valueofgrupo:
                querygrupo=DBSession.query(GrupoPersona).filter_by(id_grupopersona=itemgrupo.grupo_id).first()
                listadegrupos.append(querygrupo.grupopersona)
            variabless=toedit.numero_segurosocial
            variablelicencia=toedit.licencia
            variablenumerolicencia=toedit.numerolicencia
            variablefechavencimineto=toedit.fecha_vencimientolicencia

            if valueoflinea is not None:
                variablelinea = valueoflinea.nombre_linea
            else:
                variablelinea = ''

            if valueoftipolicencia is not None:
                variabletipolicencia = valueoftipolicencia.tipo_licencia
            else:
                variabletipolicencia = ''


        if toedit.eschecador==1:
            valueofpunto=DBSession.query(Punto).filter_by(id_punto=toedit.id_punto).first()
            if valueofpunto is not None:
                variablepunto=valueofpunto.punto
            else:
                variablepunto=''

        if toedit.numerooperador==None:
            variablenumerooperador=''
        else:
            variablenumerooperador=toedit.numerooperador


        variabledeserie=''

        if toedit.esbeneficiario==1:
            variablecta1=toedit.cta1
            variablebanco1 = toedit.banco1
            variablecta2 = toedit.cta2
            variablebanco2 = toedit.banco2
        else:
            variablecta1=''
            variablebanco1 = ''
            variablecta2 = ''
            variablebanco2 = ''


        for itemdegrupos in listadegrupos:
            variabledeserie=variabledeserie+','+itemdegrupos

        list = {"persona": toedit.nombre, "apodo": toedit.apodo, "foto": toedit.foto,"rfc": toedit.rfc,"fecha_nacimiento": toedit.fecha_nacimiento,
                       "correo":toedit.correo,"sexo": toedit.sexo,"estadocivil": toedit.estadocivil,"estatus": toedit.estatus,"fecha_estatus": toedit.fecha_ingreso ,
                       "direccion": toedit.direccion,"direccion2": toedit.direccion2,"direccion3": toedit.direccion3,"telefono": toedit.telefono,"telefono2": toedit.telefono2,
                        "celular": toedit.celular,"otro_celular": toedit.otro_celular,"curp": toedit.curp,"ine": toedit.ine,"observaciones": toedit.observaciones,
                        "esbeneficiario": toedit.esbeneficiario,"esoperador": toedit.esoperador,"eschecador": toedit.eschecador,"espermisionario": toedit.espermisionario,
                        "numero_segurosocial": variabless,"licencia": variablelicencia,"numerolicencia": variablenumerolicencia,"fecha_vencimiento": variablefechavencimineto,
                        "id_tipolicencia": variabletipolicencia,"id_linea": variablelinea,"id_punto": variablepunto,"typegrupo": typegrupo,"listadegrupos": listadegrupos,
                    "numerooperador": variablenumerooperador,'cta1': variablecta1,'cta2': variablecta2,'banco1': variablebanco1,'banco2': variablebanco2}



        usuario=toedit.usuario


        if toedit.esoperador==1:
            todossuscargos=DBSession.query(OperadorCargo).filter_by(operador_id=toedit.id_persona).all()
            construir=[]
            for item in todossuscargos:
                cargo=DBSession.query(CargoOperador).filter_by(id_cargooperador=item.cargo_id).first()
                construir.append({'cargo': cargo.cargo,'monto': item.monto})

        else:

            cargosempresa = DBSession.query(CargoOperador).filter_by(empresa=typelinea[0].nombre_linea).filter_by(application_id=kw['application_id']).all()
            cargosapp_nametodas = DBSession.query(CargoOperador).filter_by(empresa='TODAS').filter_by(application_id=kw['application_id']).all()

            construiraux = []
            construir = []

            for item in cargosempresa:
                if item.cargo in construiraux:
                    pass
                else:
                    construiraux.append(item.cargo)
                    construir.append({'cargo': item.cargo, 'monto': item.montodefault})

            for item in cargosapp_nametodas:
                if item.cargo in construiraux:
                    pass
                else:
                    construiraux.append(item.cargo)
                    construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        #print('construir--->',construir)

        return dict(page='editperson',internal_id=kw['internal_id'],app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],id=kw['id'],allbase=allbase,typelicence=typelicence,
                    typelinea=typelinea,list=list,construir=construir,toedit=toedit,typegrupo=typegrupo,usuario=usuario,variabledeserie=variabledeserie)

    @expose('json')
    def editPersona(self,**kw):

        toedit=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        acumula = 0
        mibandera=0

        queryexistenumerooperador=DBSession.query(Persona).filter_by(numerooperador=kw['numerodeoperadorasignado'])\
                                    .filter(Persona.id_persona!=kw['id']).filter_by(application_id=kw['application_id']).first()
        if queryexistenumerooperador is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "No. de Operador Existente")
            return ''
        else:
            try:
                if(toedit.numerooperador!=int(kw['numerodeoperadorasignado'])):
                    newhistorialpersona = HistorialPersona()
                    newhistorialpersona.dato = 'numerooperador'
                    newhistorialpersona.valor_anterior = toedit.numerooperador
                    newhistorialpersona.valor_nuevo = kw['numerodeoperadorasignado']
                    newhistorialpersona.sobrecampo = toedit.id_persona
                    newhistorialpersona.usuario = kw['who']
                    toedit.numerooperador = int(kw['numerodeoperadorasignado'])
                    DBSession.add(newhistorialpersona)
            except:
                pass

        # FALTAFOTO
        file=request.POST['jupiter_fotopersona']
        try:
            filename = request.params["jupiter_fotopersona"].filename
            estado = self.edita_archivo(file, filename, kw['internal_id'], toedit.foto, toedit.id_persona)
        except AttributeError:
            estado=''
            pass

        if estado=='distintos' or estado=='filenotfound':
            newhistorialpersona=HistorialPersona()
            newhistorialpersona.dato='foto'
            if estado!='filenotfound':
                newhistorialpersona.valor_anterior=toedit.foto
            else:
                newhistorialpersona.valor_anterior=''
            newhistorialpersona.valor_nuevo=filename
            newhistorialpersona.sobrecampo=toedit.id_persona
            newhistorialpersona.usuario=kw['who']
            toedit.foto=filename
            DBSession.add(newhistorialpersona)


        if(toedit.nombre!=kw['jupiterpersona_nombre']):
            newhistorialpersona=HistorialPersona()
            newhistorialpersona.dato='nombre'
            newhistorialpersona.valor_anterior=toedit.nombre
            newhistorialpersona.valor_nuevo=kw['jupiterpersona_nombre']
            newhistorialpersona.sobrecampo=toedit.id_persona
            newhistorialpersona.usuario=kw['who']
            toedit.nombre=kw['jupiterpersona_nombre']
            DBSession.add(newhistorialpersona)

        if (toedit.apodo != kw['jupiterpersona_apodo']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'apodo'
            newhistorialpersona.valor_anterior = toedit.apodo
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_apodo']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.apodo = kw['jupiterpersona_apodo']
            DBSession.add(newhistorialpersona)

        if (toedit.rfc != kw['jupiterpersona_rfc']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'rfc'
            newhistorialpersona.valor_anterior = toedit.rfc
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_rfc']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.rfc = kw['jupiterpersona_rfc']
            DBSession.add(newhistorialpersona)

        fechanewbirth = datetime.strptime(kw['jupiterpersona_fecha_nacimiento'], '%Y-%m-%d').date()

        if (toedit.fecha_nacimiento != fechanewbirth):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'fecha-nacimiento'
            newhistorialpersona.valor_anterior = toedit.fecha_nacimiento
            newhistorialpersona.valor_nuevo = fechanewbirth
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.fecha_nacimiento = fechanewbirth
            DBSession.add(newhistorialpersona)

        if (toedit.correo != kw['jupiterpersona_email']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'correo'
            newhistorialpersona.valor_anterior = toedit.correo
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_email']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.correo = kw['jupiterpersona_email']
            DBSession.add(newhistorialpersona)

        if (toedit.sexo != kw['jupiterpersona_genero']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'sexo'
            newhistorialpersona.valor_anterior = toedit.sexo
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_genero']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.sexo = kw['jupiterpersona_genero']
            DBSession.add(newhistorialpersona)

        if (toedit.estadocivil != kw['jupiterpersona_estadocivil']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'estadocivil'
            newhistorialpersona.valor_anterior = toedit.estadocivil
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_estadocivil']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.estadocivil = kw['jupiterpersona_estadocivil']
            DBSession.add(newhistorialpersona)

        # if (toedit.estatus != kw['jupiterpersona_estatus']):
        #     newhistorialpersona = HistorialPersona()
        #     newhistorialpersona.dato = 'estatus'
        #     newhistorialpersona.valor_anterior = toedit.estatus
        #     newhistorialpersona.valor_nuevo = kw['jupiterpersona_estatus']
        #     newhistorialpersona.sobrecampo = toedit.id_persona
        #     newhistorialpersona.usuario = kw['who']
        #     toedit.estatus = kw['jupiterpersona_estatus']
        #     toedit.fecha_estatus=datetime.now()
        #     DBSession.add(newhistorialpersona)

        if (toedit.direccion != kw['jupiterpersona_address']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'direccion'
            newhistorialpersona.valor_anterior = toedit.direccion
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_address']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.direccion = kw['jupiterpersona_address']
            DBSession.add(newhistorialpersona)

        if (toedit.direccion2 != kw['jupiterpersona_address2']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'direccion2'
            newhistorialpersona.valor_anterior = toedit.direccion2
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_address2']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.direccion2 = kw['jupiterpersona_address2']
            DBSession.add(newhistorialpersona)

        if (toedit.direccion3 != kw['jupiterpersona_address3']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'direccion3'
            newhistorialpersona.valor_anterior = toedit.direccion3
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_address3']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.direccion3 = kw['jupiterpersona_address3']
            DBSession.add(newhistorialpersona)

        if (toedit.telefono != kw['jupiterpersona_phone']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'telefono'
            newhistorialpersona.valor_anterior = toedit.telefono
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_phone']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.telefono = kw['jupiterpersona_phone']
            DBSession.add(newhistorialpersona)

        if (toedit.telefono2 != kw['jupiterpersona_phone2']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'telefono2'
            newhistorialpersona.valor_anterior = toedit.telefono2
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_phone2']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.telefono2 = kw['jupiterpersona_phone2']
            DBSession.add(newhistorialpersona)

        if (toedit.celular != kw['jupiterpersona_mobilephone']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'celular'
            newhistorialpersona.valor_anterior = toedit.celular
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_mobilephone']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.celular = kw['jupiterpersona_mobilephone']
            DBSession.add(newhistorialpersona)

        if (toedit.otro_celular != kw['jupiterpersona_mobilephone2']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'otro_celular'
            newhistorialpersona.valor_anterior = toedit.otro_celular
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_mobilephone2']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.otro_celular = kw['jupiterpersona_mobilephone2']
            DBSession.add(newhistorialpersona)

        if (toedit.curp != kw['jupiterpersona_curp']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'curp'
            newhistorialpersona.valor_anterior = toedit.curp
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_curp']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario =kw['who']
            toedit.curp = kw['jupiterpersona_curp']
            DBSession.add(newhistorialpersona)

            #FALTAINE

        try:
            file = request.POST['jupiterpersona_ine']
            filename = request.params["jupiterpersona_ine"].filename
            estadoine=self.edita_archivo(file,filename,kw['internal_id'],toedit.ine,toedit.id_persona)
        except AttributeError:
            estadoine=''
            pass


        if estadoine == 'distintos' or estadoine=='filenotfound':
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'INE'
            if estadoine!='filenotfound':
                newhistorialpersona.valor_anterior = toedit.ine
            else:
                newhistorialpersona.valor_anterior = ''
            newhistorialpersona.valor_nuevo = filename
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.ine = filename
            DBSession.add(newhistorialpersona)

        if (toedit.observaciones != kw['jupiterpersona_observaciones']):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'observaciones'
            newhistorialpersona.valor_anterior = toedit.observaciones
            newhistorialpersona.valor_nuevo = kw['jupiterpersona_observaciones']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.observaciones = kw['jupiterpersona_observaciones']
            DBSession.add(newhistorialpersona)

        try:
            print(kw['jupiterpersona_esoperador'])
            operador=1
            sinooperador='SI'
        except KeyError:
            print("no es operador")
            operador=0
            sinooperador = 'NO'

        if (toedit.esoperador != operador):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Operador'
            if toedit.esoperador==1:
                antes='SI'
            else:
                antes='NO'
            newhistorialpersona.valor_anterior = antes
            newhistorialpersona.valor_nuevo = sinooperador
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            if sinooperador=='SI':
                habilitadooperador=1
            else:
                habilitadooperador=None
            toedit.esoperador = operador
            toedit.habilitadooperador=habilitadooperador
            DBSession.add(newhistorialpersona)


        if(toedit.esoperador==1):

            listagrupos = []
            for item in kw:
                querygrupo = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).filter_by(grupopersona=item).first()
                if querygrupo is not None:
                    listagrupos.append(item)

            for itemlistagrupo in listagrupos:
                querygrupopersona = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).filter_by(grupopersona=itemlistagrupo).first()
                queryexisteelid=DBSession.query(GrupoPersona_Persona).filter_by(grupo_id=querygrupopersona.id_grupopersona).filter_by(persona_id=toedit.id_persona).first()
                if queryexisteelid is None:
                    newgrupopersona_persona = GrupoPersona_Persona()
                    newgrupopersona_persona.grupo_id = querygrupopersona.id_grupopersona
                    newgrupopersona_persona.persona_id = toedit.id_persona
                    DBSession.add(newgrupopersona_persona)
                    newhistorialpersona = HistorialPersona()
                    newhistorialpersona.dato = 'Nuevo Grupo'
                    newhistorialpersona.valor_anterior = ''
                    newhistorialpersona.valor_nuevo = querygrupopersona.grupopersona
                    newhistorialpersona.sobrecampo = toedit.id_persona
                    newhistorialpersona.usuario = kw['who']
                    DBSession.add(newhistorialpersona)
                else:
                    pass

            querygruposdepersona=DBSession.query(GrupoPersona_Persona).filter_by(persona_id=toedit.id_persona).all()
            for itemgruposdepersona in querygruposdepersona:
                queryelgrupo=DBSession.query(GrupoPersona).filter_by(id_grupopersona=itemgruposdepersona.grupo_id).first()
                if queryelgrupo.grupopersona not in listagrupos:
                    newhistorialpersona = HistorialPersona()
                    newhistorialpersona.dato = 'Quita Grupo'
                    newhistorialpersona.valor_anterior = queryelgrupo.grupopersona
                    newhistorialpersona.valor_nuevo = ''
                    newhistorialpersona.sobrecampo = toedit.id_persona
                    newhistorialpersona.usuario = kw['who']
                    DBSession.add(newhistorialpersona)
                    DBSession.delete(itemgruposdepersona)


            if(toedit.numero_segurosocial!=kw['jupiteroperadores_noss']):
                newhistorialpersona=HistorialPersona()
                newhistorialpersona.dato='Numero SS'
                newhistorialpersona.valor_anterior=toedit.numero_segurosocial
                newhistorialpersona.valor_nuevo=kw['jupiteroperadores_noss']
                newhistorialpersona.sobrecampo=toedit.id_persona
                newhistorialpersona.usuario=kw['who']
                toedit.numero_segurosocial=kw['jupiteroperadores_noss']
                DBSession.add(newhistorialpersona)

                #LICENCIA
            if toedit.numerolicencia != kw['jupiteroperadores_licence']:
                acumula = acumula + 1

            fechanewdue = datetime.strptime(kw['jupiteroperadores_duedate'], '%Y-%m-%d').date()
            if toedit.fecha_vencimientolicencia != fechanewdue:
                acumula = acumula + 1

            try:
                file = request.POST['jupiteroperadores_licencearchivo']
                #print("file=",file)
                filename = request.params["jupiteroperadores_licencearchivo"].filename
                #print("filename=",filename)
                estadolic=self.edita_archivolicencia(file,filename,kw['internal_id'],toedit.licencia,toedit.id_persona,acumula)
                #print('estadolic= ',estadolic)
                if estadolic == 'distintos' or estadolic == 'filenotfound':
                    acumula=acumula+1
                elif estadolic=='faltauno':
                    mibandera=1
            except AttributeError:
                estadolic=''
                #print('AttributeError= ', estadolic)
                pass
            if (estadolic == 'distintos' or estadolic=='filenotfound') and acumula==3:
                newhistorialpersona = HistorialPersona()
                newhistorialpersona.dato = 'archivo-licencia'
                if estadolic!='filenotfound':
                    newhistorialpersona.valor_anterior = toedit.licencia
                else:
                    newhistorialpersona.valor_anterior=''
                newhistorialpersona.valor_nuevo = filename
                newhistorialpersona.sobrecampo = toedit.id_persona
                newhistorialpersona.usuario = kw['who']
                toedit.licencia = filename
                DBSession.add(newhistorialpersona)


            if(toedit.numerolicencia!=kw['jupiteroperadores_licence'] and acumula==3):
                newhistorialpersona=HistorialPersona()
                newhistorialpersona.dato='Numero Licencia'
                newhistorialpersona.valor_anterior=toedit.numerolicencia
                newhistorialpersona.valor_nuevo=kw['jupiteroperadores_licence']
                newhistorialpersona.sobrecampo=toedit.id_persona
                newhistorialpersona.usuario=kw['who']
                toedit.numerolicencia=kw['jupiteroperadores_licence']
                DBSession.add(newhistorialpersona)

            fechanewdue = datetime.strptime(kw['jupiteroperadores_duedate'], '%Y-%m-%d').date()

            if(toedit.fecha_vencimientolicencia!=fechanewdue and acumula==3):
                newhistorialpersona=HistorialPersona()
                newhistorialpersona.dato='Vencimiento Licencia'
                newhistorialpersona.valor_anterior=toedit.fecha_vencimientolicencia
                newhistorialpersona.valor_nuevo=fechanewdue
                newhistorialpersona.sobrecampo=toedit.id_persona
                newhistorialpersona.usuario=kw['who']
                toedit.fecha_vencimientolicencia=fechanewdue
                DBSession.add(newhistorialpersona)

            querylinea=DBSession.query(Linea).filter_by(nombre_linea=kw['jupiteroperadores_linea']).first()
            if(toedit.id_linea!=querylinea.id_linea):
                newhistorialpersona=HistorialPersona()
                newhistorialpersona.dato='Empresa'
                queryantes=DBSession.query(Linea).filter_by(id_linea=toedit.id_linea).first()
                if queryantes is not None:
                    antes=queryantes.nombre_linea
                else:
                    antes=''
                newhistorialpersona.valor_anterior=antes
                newhistorialpersona.valor_nuevo=querylinea.nombre_linea
                newhistorialpersona.sobrecampo=toedit.id_persona
                newhistorialpersona.usuario=kw['who']
                toedit.id_linea=querylinea.id_linea
                DBSession.add(newhistorialpersona)


            # querygrupo=DBSession.query(GrupoPersona).filter_by(grupopersona=kw['jupiteroperadores_grupo']).first()
            # if querygrupo is None:
            #     iddelgrupo=0
            # else:
            #     iddelgrupo=querygrupo.id_grupopersona
            #
            # if(toedit.id_grupopersona!=iddelgrupo):
            #     newhistorialpersona=HistorialPersona()
            #     newhistorialpersona.dato='Grupo Operador'
            #     if toedit.id_grupopersona==0:
            #         newhistorialpersona.valor_anterior = 'Sin Grupo'
            #     else:
            #         queryantes=DBSession.query(GrupoPersona).filter_by(id_grupopersona=toedit.id_grupopersona).first()
            #         if queryantes is not None:
            #             newhistorialpersona.valor_anterior=queryantes.grupopersona
            #         else:
            #             newhistorialpersona.valor_anterior = ''
            #     if iddelgrupo==0:
            #         newhistorialpersona.valor_nuevo='Sin Grupo'
            #     else:
            #         newhistorialpersona.valor_nuevo=querygrupo.grupopersona
            #     newhistorialpersona.sobrecampo=toedit.id_persona
            #     newhistorialpersona.usuario=kw['who']
            #     toedit.id_grupopersona = iddelgrupo
            #     DBSession.add(newhistorialpersona)



            querylicencia=DBSession.query(TipoLicencia).filter_by(application_id=kw['application_id']).filter_by(tipo_licencia=kw['jupiteroperadores_licencia']).first()
            if(toedit.id_tipolicencia!=querylicencia.id_tipolicencia):
                newhistorialpersona=HistorialPersona()
                newhistorialpersona.dato='TipoLicencia'
                queryantes=DBSession.query(TipoLicencia).filter_by(id_tipolicencia=toedit.id_tipolicencia).first()
                if queryantes is not None:
                    antes=queryantes.tipo_licencia
                else:
                    antes=''
                newhistorialpersona.valor_anterior=antes
                newhistorialpersona.valor_nuevo=querylicencia.tipo_licencia
                newhistorialpersona.sobrecampo=toedit.id_persona
                newhistorialpersona.usuario=kw['who']
                toedit.id_tipolicencia=querylicencia.id_tipolicencia
                DBSession.add(newhistorialpersona)

            querycargosdeloperador = DBSession.query(OperadorCargo).filter_by(operador_id=toedit.id_persona).all()
            cargosoperador = []
            for item2 in querycargosdeloperador:
                querynombre = DBSession.query(CargoOperador).filter_by(id_cargooperador=item2.cargo_id).first()
                cargosoperador.append(querynombre.cargo)

            #print("cargosunidad==", cargosoperador)
            for item in kw:
                if 'montode' in item:
                    cargo = item.split('montode')
                    if cargo[1] in cargosoperador:
                        cargosoperador.remove(cargo[1])

            #print("cargosunidad==", cargosoperador)
            for elem in cargosoperador:
                newhistoriapersona = HistorialPersona()
                newhistoriapersona.dato = 'Quita cargo'
                newhistoriapersona.valor_anterior = elem
                newhistoriapersona.valor_nuevo = ''
                newhistoriapersona.usuario = kw['user']
                newhistoriapersona.sobrecampo = toedit.id_persona
                DBSession.add(newhistoriapersona)

                querycargo = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=elem).first()
                queryaeliminar = DBSession.query(OperadorCargo).filter_by(cargo_id=querycargo.id_cargooperador).filter_by(operador_id=toedit.id_persona).first()
                DBSession.delete(queryaeliminar)

            for item in kw:
                if 'montode' in item:
                    cargo = item.split('montode')
                    querycargo = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=cargo[1]).first()

                    queryexisteelcargo = DBSession.query(OperadorCargo).filter_by(operador_id=toedit.id_persona).filter_by(cargo_id=querycargo.id_cargooperador).first()
                    if queryexisteelcargo is None:
                        queryexisteoperadorcargo=DBSession.query(OperadorCargo).filter_by(cargo_id=querycargo.cargo)\
                                                    .filter_by(operador_id=toedit.id_persona).first()
                        if queryexisteoperadorcargo is None:
                            newoperadorcargo = OperadorCargo()
                            querycontador = DBSession.query(OperadorCargo).all()
                            contador = 0
                            for item2 in querycontador:
                                contador = item2.id
                            contador = contador + 1
                            newoperadorcargo.id = contador
                            newoperadorcargo.cargo_id = querycargo.id_cargooperador
                            newoperadorcargo.operador_id = toedit.id_persona
                            newoperadorcargo.monto = kw[item]
                            DBSession.add(newoperadorcargo)
                            newhistoriapersona = HistorialPersona()
                            newhistoriapersona.dato = 'Nuevo cargo'
                            newhistoriapersona.valor_anterior = ''
                            newhistoriapersona.valor_nuevo = querycargo.cargo
                            newhistoriapersona.usuario = kw['user']
                            newhistoriapersona.sobrecampo = toedit.id_persona
                            DBSession.add(newhistoriapersona)

                    else:

                        if (queryexisteelcargo.monto != int(kw[item])):
                            newhistoriapersona = HistorialPersona()
                            newhistoriapersona.dato = cargo[1] + ' monto'
                            newhistoriapersona.valor_anterior = queryexisteelcargo.monto
                            newhistoriapersona.valor_nuevo = kw[item]
                            newhistoriapersona.usuario = kw['user']
                            newhistoriapersona.sobrecampo = toedit.id_persona
                            queryexisteelcargo.monto = kw[item]
                            DBSession.add(newhistoriapersona)
                            DBSession.flush()


        try:
            print(kw['jupiterpersona_espermisionario'])
            permisionario = 1
            sinopermisionario = 'SI'
        except KeyError:
            print("no es permisionario")
            permisionario = 0
            sinopermisionario = 'NO'

        if (toedit.espermisionario != permisionario):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Permisionario'
            if toedit.espermisionario==1:
                antes='SI'
            else:
                antes='NO'
            newhistorialpersona.valor_anterior = antes
            newhistorialpersona.valor_nuevo = sinopermisionario
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.espermisionario = permisionario
            DBSession.add(newhistorialpersona)
        if permisionario==1:
            if len(kw['jupiterpersona_usuariosun']) > 0:
                url = urlsun() + '/services/create_group?group_name=Permisionarios&display_name=Permisionarios%20Transporte%20Publico&internal_id='+kw['internal_id']
                # https://sun.dudewhereismy.mx/services/create_group?group_name=Permisionarios&display_name=Permisionarios%20Transporte%20Publico&internal_id=5
                r = requests.get(url, auth=('manager', 'managepass'))
                readjson = r.json()
                if readjson['error'] == 'ok':
                    group_id = readjson['group_id']
                    # https://sun.dudewhereismy.mx/services/create_user?user_name=Permisionario1&display_name=Permisionario&internal_id=5&group_ids=16,&email_address=as@d.mx&application_id=1422&password=1234
                    url2 = urlsun() + '/services/create_user?user_name=' + kw['jupiterpersona_usuariosun'] + '&display_name=' + \
                                kw['jupiterpersona_nombre'] + '&internal_id=' + kw['internal_id'] + '&group_ids=' + str(
                                group_id) + \
                                ',&email_address=' + kw['jupiterpersona_email'] + '&application_id='+kw['application_id']+'&password=permi' + \
                                kw['jupiterpersona_usuariosun']

                    r2 = requests.get(url2, auth=('manager', 'managepass'))
                    readjson2 = r2.json()
                    if readjson2['error'] != 'ok':
                        Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + readjson2['error'])
                        time.sleep(3)


                    else:
                        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cuenta Creada")
                        toedit.usuario=kw['jupiterpersona_usuariosun']
                        time.sleep(3)
                else:
                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + readjson['error'])
                    time.sleep(3)

        try:
            print(kw['jupiterpersona_esbeneficiario'])
            beneficiario = 1
            sinobeneficiario = 'SI'
        except KeyError:
            print("no es beneficiario")
            beneficiario = 0
            sinobeneficiario = 'NO'

        if (toedit.esbeneficiario != beneficiario):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Beneficiario'
            if toedit.esbeneficiario==1:
                antes='SI'
            else:
                antes='NO'
            newhistorialpersona.valor_anterior = antes
            newhistorialpersona.valor_nuevo = sinobeneficiario
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.esbeneficiario = beneficiario
            DBSession.add(newhistorialpersona)

        if(toedit.cta1!=kw['jupiter_cuenta1'] and kw['jupiter_cuenta1']!=''):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Cuenta 1'
            newhistorialpersona.valor_anterior = toedit.cta1
            newhistorialpersona.valor_nuevo = kw['jupiter_cuenta1']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.cta1 = kw['jupiter_cuenta1']
            DBSession.add(newhistorialpersona)

        if(toedit.cta2!=kw['jupiter_cuenta2'] and kw['jupiter_cuenta2']!=''):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Cuenta 2'
            newhistorialpersona.valor_anterior = toedit.cta2
            newhistorialpersona.valor_nuevo = kw['jupiter_cuenta2']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.cta2 = kw['jupiter_cuenta2']
            DBSession.add(newhistorialpersona)

        if(toedit.banco1!=kw['jupiter_banco1'] and kw['jupiter_banco1']!=''):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Banco 1'
            newhistorialpersona.valor_anterior = toedit.banco1
            newhistorialpersona.valor_nuevo = kw['jupiter_banco1']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.banco1 = kw['jupiter_banco1']
            DBSession.add(newhistorialpersona)

        if(toedit.banco2!=kw['jupiter_banco2'] and kw['jupiter_banco2']!=''):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Banco 2'
            newhistorialpersona.valor_anterior = toedit.banco2
            newhistorialpersona.valor_nuevo = kw['jupiter_banco2']
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.banco2 = kw['jupiter_banco2']
            DBSession.add(newhistorialpersona)





        try:
            print(kw['jupiterpersona_eschecador'])
            checador = 1
            sinochecador = 'SI'

        except KeyError:
            print("no es checador")
            checador = 0
            sinochecador = 'NO'

        if (toedit.eschecador != checador):
            newhistorialpersona = HistorialPersona()
            newhistorialpersona.dato = 'Checador'
            if toedit.eschecador==1:
                antes='SI'
            else:
                antes='NO'
            newhistorialpersona.valor_anterior = antes
            newhistorialpersona.valor_nuevo = sinochecador
            newhistorialpersona.sobrecampo = toedit.id_persona
            newhistorialpersona.usuario = kw['who']
            toedit.eschecador = checador
            DBSession.add(newhistorialpersona)

        querybase = DBSession.query(Punto).filter_by(punto=kw['jupiterchecador_base']).filter_by(application_id=kw['application_id']).first()
        if querybase is not None:
            if(toedit.eschecador==1):
                print("ES CHECADOR")
                if (toedit.id_punto != querybase.id_punto):
                    newhistorialpersona = HistorialPersona()
                    newhistorialpersona.dato = "Base"
                    queryantes = DBSession.query(Punto).filter_by(id_punto=toedit.id_punto).first()
                    if queryantes is not None:
                        antes = queryantes.punto
                    else:
                        antes = ''
                    newhistorialpersona.valor_anterior = antes
                    newhistorialpersona.valor_nuevo = querybase.punto
                    newhistorialpersona.usuario = kw['who']
                    newhistorialpersona.sobrecampo = toedit.id_persona
                    toedit.id_punto = querybase.id_punto
                    DBSession.add(newhistorialpersona)
            else:
                print("NO ES CHECADOR")
                queryantes=DBSession.query(Punto).filter_by(id_punto=toedit.id_punto).first()
                if queryantes is not None:
                    print("queryantes is not None")
                    newhistorialpersona=HistorialPersona()
                    newhistorialpersona.dato='Base'
                    newhistorialpersona.valor_anterior=queryantes.punto
                    newhistorialpersona.valor_nuevo=''
                    newhistorialpersona.sobrecampo=toedit.id_persona
                    newhistorialpersona.usuario=kw['who']
                    toedit.id_punto=None
                    DBSession.add(newhistorialpersona)
                else:
                    print("queryantes is None")
                    pass

        #print("Acumula: ",acumula)
        if acumula==0 and mibandera==0:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Persona Editada")
        elif (acumula>=1 and acumula<=2) or mibandera==1:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "Debes de Editar el archivo,el numero y fecha de la licencia")
            return ''
        else:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Persona Editada")

        return ''

    @expose('pythonjupiter.templates.PersonasTemplates.historialpersonas')
    def historialde(self,**kw):
        allrecords=DBSession.query(HistorialPersona).filter_by(sobrecampo=kw['id']).all()
        return dict(page='historialpersonas',allrecords=allrecords)

    @expose('pythonjupiter.templates.PersonasTemplates.archivos')
    def archivosde(self,**kw):
        soy = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        return dict(page='archivosde', soy=soy, app_name=kw['app_name'], application_id=kw['application_id'],internal_id=kw['internal_id'], user=kw['user'])

    @expose('json')
    def mostrararchivo(self,**kw):
        archivo=kw['archivo']
        id=kw['id']
        internal_id=kw['internal_id']
        template=render_template({'archivo': archivo,'id': id,'internal_id': internal_id},"mako",'pythonjupiter.templates.PersonasTemplates.elarchivo')
        return dict(template=template)

    @expose('json')
    def tipopersona(self,**kw):
        tipo=kw['tipo']
        hd = []
        internal_id = kw['internal_id']
        application_id=kw['application_id']
        if tipo=='Permisionario':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(espermisionario=1).all()

            for item in handler:
                if (item.habilitadopermisionario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})



        if tipo=='Checador':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(eschecador=1).all()

            for item in handler:

                querybase = DBSession.query(Punto).filter_by(application_id=application_id).filter_by(id_punto=item.id_punto).first()
                varhabilitado = ''

                if (item.habilitadochecador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querybase is not None:
                    variablepunto = querybase.punto
                else:
                    variablepunto = ''


                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"base": variablepunto,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})



        if tipo == 'Operador':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(esoperador=1).all()

            for item in handler:
                item.estadodelicencia = self.calculalicencia(item)
                querytipolicencia = DBSession.query(TipoLicencia).filter_by(
                    id_tipolicencia=item.id_tipolicencia).first()
                querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()

                varhabilitado = ''

                if (item.habilitadooperador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querylinea is not None:
                    variablelinea = querylinea.nombre_linea
                else:
                    variablelinea = ''

                if querytipolicencia is not None:
                    variablelicencia = querytipolicencia.tipo_licencia
                else:
                    variablelicencia = ''

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus,
                     "empresa": variablelinea, "rfc": item.rfc, "telefono": item.telefono, "celular": item.celular,
                     "licencia": variablelicencia,
                     "fecha_vencimiento": item.fecha_vencimientolicencia, "estadodelicencia": item.estadodelicencia,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})

        if tipo == 'Beneficiario':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(esbeneficiario=1).all()

            for item in handler:
                if (item.habilitadobeneficiario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})


        contador=len(hd)
        return dict(page='personas', hd=hd, internal_id=kw['internal_id'], app_name=kw['app_name'],user=kw['user'], application_id=kw['application_id'],contador=contador)

    @expose('json')
    def reloadpersonas(self, **kw):
        tipo=kw['by']
        hd = []
        internal_id = kw['internal_id']
        application_id=kw['application_id']
        if tipo=='Permisionario':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(espermisionario=1).all()

            for item in handler:
                if (item.habilitadopermisionario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})


        if tipo=='Checador':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(eschecador=1).all()

            for item in handler:

                querybase = DBSession.query(Punto).filter_by(id_punto=item.id_punto).first()
                varhabilitado = ''

                if (item.habilitadochecador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querybase is not None:
                    variablepunto = querybase.punto
                else:
                    variablepunto = ''


                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"base": variablepunto,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})

        if tipo == 'Operador':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(esoperador=1).all()

            for item in handler:
                item.estadodelicencia = self.calculalicencia(item)
                querytipolicencia = DBSession.query(TipoLicencia).filter_by(
                    id_tipolicencia=item.id_tipolicencia).first()
                querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()

                varhabilitado = ''

                if (item.habilitadooperador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querylinea is not None:
                    variablelinea = querylinea.nombre_linea
                else:
                    variablelinea = ''

                if querytipolicencia is not None:
                    variablelicencia = querytipolicencia.tipo_licencia
                else:
                    variablelicencia = ''

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus,
                     "empresa": variablelinea, "rfc": item.rfc, "telefono": item.telefono, "celular": item.celular,
                     "licencia": variablelicencia,
                     "fecha_vencimiento": item.fecha_vencimientolicencia, "estadodelicencia": item.estadodelicencia,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})

        if tipo == 'Beneficiario':
            if internal_id == '0':
                handler = DBSession.query(Persona).filter_by(esbeneficiario=1).all()
            else:
                handler = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(esbeneficiario=1).all()

            for item in handler:
                if (item.habilitadobeneficiario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})

        contador = len(hd)
        return dict(error='ok', hd=hd,internal_id=kw['internal_id'],contador=contador)

    @expose('json')
    def searchpersona(self, **kw):
        tipo=kw['by']
        searchfor=kw['searchfor']
        name=kw['name']
        hd = []
        internal_id = kw['internal_id']
        application_id=kw['application_id']
        if tipo=='Permisionario':
            if (searchfor == 'Nombre'):
                handler=DBSession.query(Persona).filter(Persona.nombre.like("%"+name+"%")).filter_by(espermisionario=1).filter_by(application_id=application_id).all()
            if (searchfor == 'Apodo'):
                handler=DBSession.query(Persona).filter(Persona.apodo.like("%"+name+"%")).filter_by(espermisionario=1).filter_by(application_id=application_id).all()

            for item in handler:
                if (item.habilitadopermisionario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})


        if tipo=='Checador':
            if (searchfor == 'Nombre'):
                handler = DBSession.query(Persona).filter(Persona.nombre.like("%" + name + "%")).filter_by(eschecador=1).filter_by(application_id=application_id).all()
            if (searchfor == 'Apodo'):
                handler = DBSession.query(Persona).filter(Persona.apodo.like("%" + name + "%")).filter_by(eschecador=1).filter_by(application_id=application_id).all()
            if (searchfor == 'Base'):
                querybase=DBSession.query(Punto).filter(Punto.punto.like("%"+name+"%")).filter_by(application_id=application_id).first()
                if querybase is not None:
                    handler = DBSession.query(Persona).filter_by(id_punto=querybase.id_punto).filter_by(eschecador=1).filter_by(application_id=application_id).all()
                else:
                    handler={}

            for item in handler:

                querybase = DBSession.query(Punto).filter_by(id_punto=item.id_punto).first()
                varhabilitado = ''

                if (item.habilitadochecador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querybase is not None:
                    variablepunto = querybase.punto
                else:
                    variablepunto = ''


                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"base": variablepunto,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})

        if tipo == 'Operador':
            if name!= '' or name!=' ':
                if(searchfor=='credencial'):
                    try:
                        handler = DBSession.query(Persona).filter_by(numerooperador=int(name)).filter_by(application_id=application_id).filter_by(esoperador=1).all()
                    except:
                        handler={}
                if(searchfor=='nombre'):
                    handler = DBSession.query(Persona).filter(Persona.nombre.like('%'+name+'%')).filter_by(application_id=application_id).filter_by(esoperador=1).all()
                if(searchfor=='apodo'):
                    handler = DBSession.query(Persona).filter(Persona.apodo.like('%'+name+'%')).filter_by(application_id=application_id).filter_by(esoperador=1).all()
                if(searchfor=='linea'):
                    querylinea = DBSession.query(Linea).filter(Linea.nombre_linea==name).filter_by(app_id=application_id).first()

                    if querylinea is not None:
                        handler = DBSession.query(Persona).filter_by(id_linea=querylinea.id_linea).filter_by(application_id=application_id).filter_by(esoperador=1).all()
                    else:
                        handler={}
                    #print("handler: ",handler)
                if(searchfor=='estatus'):
                    handler = DBSession.query(Persona).filter(Persona.estatus.like('%'+name+'%')).filter_by(application_id=application_id).filter_by(esoperador=1).all()
                if(searchfor=='Licencia por Vencer'):
                    handler = DBSession.query(Persona).filter(Persona.estadodelicencia=='VENCERA').filter_by(application_id=application_id).filter_by(esoperador=1).all()
                if (searchfor == 'Licencias Vencidas'):
                    handler = DBSession.query(Persona).filter(Persona.estadodelicencia=='VENCIO').filter_by(application_id=application_id).filter_by(esoperador=1).all()

            for item in handler:
                item.estadodelicencia = self.calculalicencia(item)
                querytipolicencia = DBSession.query(TipoLicencia).filter_by(
                    id_tipolicencia=item.id_tipolicencia).first()
                querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()

                varhabilitado = ''

                if (item.habilitadooperador == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querylinea is not None:
                    variablelinea = querylinea.nombre_linea
                else:
                    variablelinea = ''

                if querytipolicencia is not None:
                    variablelicencia = querytipolicencia.tipo_licencia
                else:
                    variablelicencia = ''

                hd.append(
                    {"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus,
                     "empresa": variablelinea, "rfc": item.rfc, "telefono": item.telefono, "celular": item.celular,
                     "licencia": variablelicencia,
                     "fecha_vencimiento": item.fecha_vencimientolicencia, "estadodelicencia": item.estadodelicencia,
                     "habilitado": varhabilitado, "observaciones": item.observaciones})

        if tipo == 'Beneficiario':
            if (searchfor == 'Nombre'):
                handler = DBSession.query(Persona).filter(Persona.nombre.like("%" + name + "%")).filter_by(esbeneficiario=1).filter_by(application_id=application_id).all()
            if (searchfor == 'Apodo'):
                handler = DBSession.query(Persona).filter(Persona.apodo.like("%" + name + "%")).filter_by(esbeneficiario=1).filter_by(application_id=application_id).all()

            for item in handler:
                if (item.habilitadobeneficiario == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                hd.append({"numerooperador": item.numerooperador,"id_persona": item.id_persona, "persona": item.nombre, "apodo": item.apodo, "foto": item.foto,
                     "estatus": item.estatus, "telefono": item.telefono, "celular": item.celular,"habilitado": varhabilitado ,"observaciones": item.observaciones})

        contador = len(hd)
        return dict(error='ok', hd=hd,internal_id=kw['internal_id'],contador=contador)

    @expose('json')
    def disablepersona(self, **kw):
        id = kw['id']
        operador = kw['persona']
        by=kw['by']
        if by=='Operador':
            queryoperador = DBSession.query(Persona).filter_by(esoperador=1).filter_by(id_persona=id).first()
            queryoperador.habilitadooperador = 0
        if by=='Permisionario':
            queryoperador = DBSession.query(Persona).filter_by(espermisionario=1).filter_by(id_persona=id).first()
            queryoperador.habilitadopermisionario = 0
        if by=='Checador':
            queryoperador = DBSession.query(Persona).filter_by(eschecador=1).filter_by(id_persona=id).first()
            queryoperador.habilitadochecador = 0
        if by=='Beneficiario':
            queryoperador = DBSession.query(Persona).filter_by(esbeneficiario=1).filter_by(id_persona=id).first()
            queryoperador.habilitadobeneficiario = 0
        return by+" deshabilitado"

    @expose('json')
    def habilitawho(self,**kw):
        if kw['by']=='Operador':
            allrecords = DBSession.query(Persona).filter_by(habilitadooperador=0).filter_by(application_id=kw['application_id']).all()
        if kw['by']=='Permisionario':
            allrecords = DBSession.query(Persona).filter_by(habilitadopermisionario=0).filter_by(application_id=kw['application_id']).all()
        if kw['by']=='Checador':
            allrecords = DBSession.query(Persona).filter_by(habilitadochecador=0).filter_by(application_id=kw['application_id']).all()
        if kw['by']=='Beneficiario':
            allrecords = DBSession.query(Persona).filter_by(habilitadobeneficiario=0).filter_by(application_id=kw['application_id']).all()

        print("allrecords=>",allrecords)
        template = render_template({'by': kw['by'],'allrecords': allrecords,'internal_id': kw['internal_id'],'application_id': kw['application_id']}, "mako",'pythonjupiter.templates.PersonasTemplates.habilitarpersona')
        return dict(template=template)

    @expose('json')
    def enable(self, **kw):
        apodo = kw['apodo']
        id = kw['id']
        internal_id=kw['internal_id']
        by=kw['by']
        #print('by:',by)
        #print('internal_id:', internal_id)
        if kw['by']=='Operador':
            query = DBSession.query(Persona).filter_by(id_persona=id).filter_by(application_id=kw['application_id']).first()
            query.habilitadooperador=1
        if kw['by']=='Permisionario':
            query = DBSession.query(Persona).filter_by(id_persona=id).filter_by(application_id=kw['application_id']).first()
            query.habilitadopermisionario = 1
        if kw['by']=='Checador':
            query = DBSession.query(Persona).filter_by(id_persona=id).filter_by(application_id=kw['application_id']).first()
            query.habilitadochecador = 1
        if kw['by']=='Beneficiario':
            query = DBSession.query(Persona).filter_by(id_persona=id).filter_by(application_id=kw['application_id']).first()
            query.habilitadobeneficiario = 1


        return by+" Habilitado"

    @expose('json')
    def templateunidaddeoperador(self,**kw):
        listahistorico=[]
        querypersona=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        kw['querypersona']=querypersona
        try:
            queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=querypersona.id_unidad).first()
            eco=queryunidad.eco1
        except:
            queryunidad={}
            eco='Sin asignar'
        kw['queryunidad']=eco
        queryhistorico=DBSession.query(HistoricoUnidad).filter_by(id_operador=querypersona.id_persona).all()
        kw['queryhistorico']=queryhistorico
        for item in queryhistorico:
            if item.unidad_id==None:
                variablesinasignar='Sin Unidad'
                listahistorico.append({'fecha': item.fechaasignada, "unidad": variablesinasignar})
            else:
                variablesinasignar=item.unidad_id
                queryunidaditem=DBSession.query(DatosUnidad).filter_by(id_unidad=variablesinasignar).first()
                listahistorico.append({'fecha': item.fechaasignada,"unidad": queryunidaditem.eco1})

        kw['listahistorico']=listahistorico
        template=render_template(kw,"mako","pythonjupiter.templates.PersonasTemplates.UnidadOperador")
        return dict(template=template)

    @expose('json')
    def templateunidaddeoperadorasignar(self,**kw):
        listaunidades=[]
        querypersona=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        kw['querypersona']=querypersona
        try:
            queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=querypersona.id_unidad).first()
            eco=queryunidad.eco1
        except:
            eco='Sin asignar'
        kw['eco']=eco
        listaunidades.append({'eco': eco})
        queryunidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(application_id=kw['application_id']).all()
        for itemunidades in queryunidades:
            queryidunidades=DBSession.query(Persona).filter_by(id_unidad=itemunidades.id_unidad).first()
            if queryidunidades is None:
                listaunidades.append({'eco': itemunidades.eco1})

        kw['queryunidades']=listaunidades
        template=render_template(kw,"mako","pythonjupiter.templates.PersonasTemplates.asignarnueva")
        return dict(template=template)

    @expose('json')
    def templateunidaddeoperadorasignarguardar(self,**kw):
        id=kw['id']
        unidad=kw['unidad']
        internal_id=kw['internal_id']
        application_id=kw['application_id']
        queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=unidad).filter_by(application_id=application_id).filter_by(habilitado=1).first()
        querypersona = DBSession.query(Persona).filter_by(id_persona=id).first()

        if queryunidad is not None:
            queryexisteunidad=DBSession.query(Persona).filter_by(id_unidad=queryunidad.id_unidad).first()
            if queryexisteunidad is not None:
                Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|"
                             + "La unidad esta asignada al operador "+queryexisteunidad.nombre+"/"+queryexisteunidad.apodo)
                return dict(error='Unidad ya Asignada')
            else:
                try:
                    queryunidadpasada = DBSession.query(DatosUnidad).filter_by(id_unidad=querypersona.id_unidad).first()
                    queryhead = DBSession.query(Head).filter_by(id_unidad=queryunidadpasada.id_unidad).filter_by(pagado=0).all()
                    for itemhead in queryhead:
                        itemhead.id_operador = None
                except:
                    pass

                querypersona.id_unidad=queryunidad.id_unidad
                newhistorialunidadoperador = HistoricoUnidad()
                newhistorialunidadoperador.unidad_id = queryunidad.id_unidad
                newhistorialunidadoperador.id_operador = querypersona.id_persona
                DBSession.add(newhistorialunidadoperador)
                queryhead=DBSession.query(Head).filter_by(id_unidad=queryunidad.id_unidad).filter_by(pagado=0).all()
                for itemhead in queryhead:
                    itemhead.id_operador=id
        elif unidad=='Sin Unidad':
            queryunidadpasada=DBSession.query(DatosUnidad).filter_by(id_unidad=querypersona.id_unidad).first()
            querypersona.id_unidad=None
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Operador sin unidad")
            newhistorialunidadoperador=HistoricoUnidad()
            newhistorialunidadoperador.unidad_id=None
            newhistorialunidadoperador.id_operador=querypersona.id_persona
            DBSession.add(newhistorialunidadoperador)
            queryhead = DBSession.query(Head).filter_by(id_unidad=queryunidadpasada.id_unidad).filter_by(pagado=0).all()
            for itemhead in queryhead:
                itemhead.id_operador = None
            return dict(error='ok')

        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Unidad asignada a Operador")
        return dict(error='ok')

    @expose('json')
    def gridunidadoperador(self,**kw):
        template=render_template(kw,"mako","pythonjupiter.templates.PersonasTemplates.gridunidadoperador")
        return dict(template=template)

    @expose('json')
    def loadgridunidadoperador(self,**kw):
        listadata = []
        querypersonas=DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(esoperador=1).filter_by(habilitadooperador=1).all()
        for itempersonas in querypersonas:
            queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=itempersonas.id_unidad).first()
            if queryunidad is not None:
                listadata.append({'foto': str(itempersonas.id_persona)+','+itempersonas.foto,'nombre': itempersonas.nombre,'unidad': queryunidad.eco1})
            else:
                listadata.append({'foto': str(itempersonas.id_persona)+','+itempersonas.foto, 'nombre': itempersonas.nombre, 'unidad': 'Sin Unidad'})
        data={'rows': listadata}
        return data

    @expose('json')
    def searchunidadoperador(self,**kw):
        listadata=[]
        por=kw['por']
        internal_id=kw['internal_id']
        application_id=kw['application_id']
        if 'what' in kw:
            what=kw['what']
        if por=='Nombre':
            querypersona=DBSession.query(Persona).filter_by(habilitadooperador=1)\
                        .filter_by(application_id=application_id).filter_by(esoperador=1).filter(Persona.nombre.like('%'+what+'%')).all()
            for item in querypersona:
                if item.id_unidad==None:
                    variable='Sin Unidad'
                    listadata.append({'foto': str(item.id_persona)+','+item.foto, 'nombre': item.nombre, 'unidad': variable})
                else:
                    queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item.id_unidad).first()
                    listadata.append({'foto': str(item.id_persona)+','+item.foto,'nombre': item.nombre,'unidad': queryunidad.eco1})
        elif por=='Unidad':
            queryunidad=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(application_id=application_id).filter(DatosUnidad.eco1.like('%'+what+'%')).all()
            for item in queryunidad:
                querypersona=DBSession.query(Persona).filter_by(id_unidad=item.id_unidad).first()
                if querypersona is not None:
                    listadata.append({'foto': str(querypersona.id_persona)+','+querypersona.foto, 'nombre': querypersona.nombre, 'unidad': item.eco1})

        else:
            querypersona=DBSession.query(Persona).filter_by(id_unidad=None).filter_by(esoperador=1).filter_by(habilitadooperador=1).all()
            for item in querypersona:
                variable='Sin Unidad'
                listadata.append({'foto': str(item.id_persona)+','+item.foto, 'nombre': item.nombre, 'unidad': variable})

        data={'rows': listadata}
        #print("LA DATA:",data)
        return data



    @expose('pythonjupiter.templates.PersonasTemplates.credencial')
    def generarcredencial(self,**kw):
        persona=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        empresa=DBSession.query(Linea).filter_by(id_linea=persona.id_linea).first()
        try:
            laempresa=empresa.nombre_linea
        except:
            laempresa=''

        sobreque = str(persona.id_persona) + '-' + persona.nombre
        qr = qrcode.QRCode(box_size=40)
        qr.add_data(sobreque)
        #qr.make(fit=True)
        img = qr.make_image()
        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/personas/" + str(persona.id_persona) + ".-/"
        img.save(destino+"qr_"+str(persona.id_persona)+".jpg")
        laimagen=destino+"qr_"+str(persona.id_persona)+".jpg"
        with open(laimagen, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
            persona.codigoqr=encoded_string

        return dict(page='hey',internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],application_id=kw['application_id'],persona=persona,empresa=laempresa,codigoqr=encoded_string.decode("utf-8"))

    @expose('json')
    def getqr(self,**kw):
        persona = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        if persona.codigoqr == None or 1==1:
            sobreque = str(persona.numerooperador) + '.-' + persona.nombre
            qr = qrcode.QRCode(box_size=30, image_factory=qrcode.image.svg.SvgPathImage)
            qr.add_data(sobreque)
            img = qr.make_image()
            buf = BytesIO()
            img.save(buf)
            image_stream = buf.getvalue()
            persona.codigoqr = image_stream
        else:
            image_stream = persona.codigoqr

        image_stream = base64.b64encode(image_stream)
        return dict(codigoqr=image_stream.decode("utf-8"))

    @expose('json')
    def credencialpdf(self,**kw):
        persona=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        empresa=DBSession.query(Linea).filter_by(id_linea=persona.id_linea).first()
        try:
            laempresa=empresa.nombre_linea
        except:
            laempresa=''

        sobreque = str(persona.numerooperador) + '.-' + persona.nombre
        qr = qrcode.QRCode(box_size=40)
        qr.add_data(sobreque)
        #qr.make(fit=True)
        img = qr.make_image()
        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/personas/" + str(persona.id_persona) + ".-/"
        img.save(destino + "qr_" + str(persona.id_persona) + ".jpg")
        laimagen = destino + "qr_" + str(persona.id_persona) + ".jpg"
        with open(laimagen, "rb") as image_file:
            encoded_stringqr = base64.b64encode(image_file.read())


        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/personas/" + str(persona.id_persona) + ".-/"
        imagen=destino+str(persona.foto)
        #with open(imagen, 'r') as fdestino:
        #print(imagen)
        try:
            with open(imagen, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
        except:
            encoded_string=""

        #print(encoded_string.decode("utf-8"))
        siluetapersona = ruta + "pythonjupiter/public/img/personasilueta.gif"
        with open(siluetapersona, "rb") as image_file2:
            lasilueta = base64.b64encode(image_file2.read())

        try:
            encoded_string=encoded_string.decode("utf-8")
        except:
            encoded_string=''

        fondocredencial = ruta + "pythonjupiter/public/img/fondocredencial3.jpg"
        with open(fondocredencial, "rb") as image_file2:
            elfondocredencial = base64.b64encode(image_file2.read())

        file_name = ExportPDF.create(dict(i=51,internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],application_id=kw['application_id'],persona=persona,empresa=laempresa,
                    codigoqr=encoded_stringqr.decode("utf-8"),elfondocredencial=elfondocredencial.decode("utf-8"),personastring=encoded_string,lasilueta=lasilueta.decode("utf-8")),'Credencial_'+persona.nombre, 'pythonjupiter.templates.PersonasTemplates.credencial2',
                'prueba')

        return dict(id=kw['id'],file_name=file_name)