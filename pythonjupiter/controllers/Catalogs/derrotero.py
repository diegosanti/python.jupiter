from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Derrotero
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class DerroteroController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Derrotero, 'id_derrotero', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Derrotero, 'id_derrotero', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Derrotero).filter_by(id_derrotero=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This derrotero is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Derrotero).filter_by(derrotero=kw['derrotero']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Derrotero()
            else:
                return dict(error=l_('This derrotero alredy exist'))
            i = 0
        else:
            handler = DBSession.query(Derrotero).filter_by(id_derrotero=kw['id']).first()
            i = 1
        handler.derrotero = kw['derrotero']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_derrotero)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Derrotero()
        else:
            kw['handler'] = DBSession.query(Derrotero).filter_by(id_derrotero=kw['id']).first()
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.Catalogs.Forms.derrotero_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.derrotero_jqgrid')
    # @require(predicates.not_anonymous())
    def derroteros(self, **kw):
        return dict(page='unit_type_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
