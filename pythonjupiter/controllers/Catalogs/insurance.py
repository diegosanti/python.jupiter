from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Aseguradora
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class InsuranceController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Aseguradora, 'id_aseguradora', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Aseguradora, 'id_aseguradora', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Aseguradora).filter_by(id_aseguradora=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This insurance is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Aseguradora).filter_by(id_aseguradora=kw['id']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Aseguradora()
            else:
                return dict(error=l_('This insurance already exist'))
            i = 0
        else:
            handler = DBSession.query(Aseguradora).filter_by(id_aseguradora=kw['id']).first()
            i = 1
        handler.aseguradora = kw['aseguradora']
        handler.contacto = kw['contacto']
        handler.contacto2 = kw['contacto2']
        handler.telefono = kw['telefono']
        handler.celular = kw['celular']
        handler.telefonoemergencia = kw['telefonoemergencia']
        handler.direccion = kw['direccion']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_aseguradora)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Aseguradora()
        else:
            kw['handler'] = DBSession.query(Aseguradora).filter_by(id_aseguradora=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.insurance_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.insurance_jqgrid')
    # @require(predicates.not_anonymous())
    def insurance(self, **kw):
        return dict(page='unit_type_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
