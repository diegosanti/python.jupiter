from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Grupo
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class GroupController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Grupo, 'id_grupo', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Grupo, 'id_grupo', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Grupo).filter_by(id_grupo=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This group is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Grupo).filter_by(grupo=kw['grupo']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Grupo()
            else:
                return dict(error=l_('This group alredy exist'))
            i = 0
        else:
            handler = DBSession.query(Grupo).filter_by(id_grupo=kw['id']).first()
            i = 1
        handler.grupo = kw['grupo']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_grupo)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Grupo()
        else:
            kw['handler'] = DBSession.query(Grupo).filter_by(id_grupo=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.group_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.group_jqgrid')
    # @require(predicates.not_anonymous())
    def groups(self, **kw):
        return dict(page='check_point_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
