from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import TipoLicencia
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class LicenceTypeController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoLicencia, 'id_tipolicencia', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoLicencia, 'id_tipolicencia', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=kw['id']).first()
        if handler.personas != []:
            return dict(error=l_('This licence type is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(TipoLicencia).filter_by(tipo_licencia=kw['tipo_licencia']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = TipoLicencia()
            else:
                return dict(error=l_('This licence type alredy exist'))
            i = 0
        else:
            handler = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=kw['id']).first()
            i = 1
        handler.tipo_licencia = kw['tipo_licencia']
        handler.descripcion = kw['descripcion']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_tipolicencia)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = TipoLicencia()
        else:
            kw['handler'] = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.licence_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.licence_jqgrid')
    # @require(predicates.not_anonymous())
    def licences(self, **kw):
        return dict(page='check_point_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
