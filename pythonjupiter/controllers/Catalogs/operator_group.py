from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import GrupoPersona
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class OperatorGroupController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(GrupoPersona, 'id_grupopersona', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(GrupoPersona, 'id_grupopersona', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(GrupoPersona).filter_by(id_grupopersona=kw['id']).first()
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(GrupoPersona).filter_by(grupopersona=kw['grupopersona']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = GrupoPersona()
            else:
                return dict(error=l_('This group type alredy exist'))
            i = 0
        else:
            handler = DBSession.query(GrupoPersona).filter_by(id_grupopersona=kw['id']).first()
            i = 1
        handler.descripcion = kw['descripcion']
        handler.grupopersona = kw['grupopersona']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_grupopersona)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = GrupoPersona()
        else:
            kw['handler'] = DBSession.query(GrupoPersona).filter_by(id_grupopersona=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.operator_group_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.operator_group_jqgrid')
    # @require(predicates.not_anonymous())
    def operator_groups(self, **kw):
        return dict(page='check_point_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
