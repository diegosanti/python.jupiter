from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import TipoTarjeta
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class CardTypeController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoTarjeta, 'id_tipotarjeta', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoTarjeta, 'id_tipotarjeta', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This card type is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(TipoTarjeta).filter_by(tipo_tarjeta=kw['tipo_tarjeta']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = TipoTarjeta()
            else:
                return dict(error=l_('This card type alredy exist'))
            i = 0
        else:
            handler = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=kw['id']).first()
            i = 1
        handler.tipo_tarjeta = kw['tipo_tarjeta']
        handler.descripcion = kw['descripcion']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_tipotarjeta)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = TipoTarjeta()
        else:
            kw['handler'] = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=kw['id']).first()
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.Catalogs.Forms.card_type_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.card_type_jqgrid')
    # @require(predicates.not_anonymous())
    def card_types(self, **kw):
        return dict(page='unit_type_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
