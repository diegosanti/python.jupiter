from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Periodo
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class PeriodController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Periodo, 'id_periodo', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Periodo, 'id_periodo', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Periodo).filter_by(id_periodo=kw['id']).first()

        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Periodo).filter_by(periodo=kw['periodo']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Periodo()
            else:
                return dict(error=l_('This period alredy exist'))
            i = 0
        else:
            handler = DBSession.query(Periodo).filter_by(id_periodo=kw['id']).first()
            i = 1
        handler.periodo = kw['periodo']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_periodo)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Periodo()
        else:
            kw['handler'] = DBSession.query(Periodo).filter_by(id_periodo=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.period_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.period_jqgrid')
    # @require(predicates.not_anonymous())
    def periods(self, **kw):
        return dict(page='payment_motive_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
