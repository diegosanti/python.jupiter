from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import DatosUnidad,TipoUnidad,Persona
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.messagequeue import Message
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class UnitTypeController(BaseController):


    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoUnidad, 'id_tipounidad', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(TipoUnidad, 'id_tipounidad', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(TipoUnidad).filter_by(id_tipounidad=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This car type is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(TipoUnidad).filter_by(tipo_unidad=kw['tipo_unidad']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = TipoUnidad()
            else:
                return dict(error=l_('This car type alredy exist'))
            i = 0
        else:
            handler = DBSession.query(TipoUnidad).filter_by(id_tipounidad=kw['id']).first()
            i = 1
        handler.tipo_unidad = kw['tipo_unidad']
        handler.descripcion = kw['descripcion']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_tipounidad)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = TipoUnidad()
        else:
            kw['handler'] = DBSession.query(TipoUnidad).filter_by(id_tipounidad=kw['id']).first()
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.Catalogs.Forms.unit_type_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.unit_type_jqgrid')
    # @require(predicates.not_anonymous())
    def unit_types(self, **kw):
        return dict(page='unit_type_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
