from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Punto
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class CheckPointController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Punto, 'id_punto', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Punto, 'id_punto', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Punto).filter_by(id_punto=kw['id']).first()
        if handler.checadores != []:
            return dict(error=l_('This checkpoint is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Punto).filter_by(punto=kw['punto']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Punto()
            else:
                return dict(error=l_('This checkpoint alredy exist'))
            i = 0
        else:
            handler = DBSession.query(Punto).filter_by(id_punto=kw['id']).first()
            i = 1
        handler.punto = kw['punto']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_punto)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Punto()
        else:
            kw['handler'] = DBSession.query(Punto).filter_by(id_punto=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.check_point_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.check_point_jqgrid')
    # @require(predicates.not_anonymous())
    def checkpoints(self, **kw):
        return dict(page='check_point_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
