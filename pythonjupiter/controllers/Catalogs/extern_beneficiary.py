from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import BeneficiarioMoral
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template
from tg.i18n import lazy_ugettext as l_

class ExternBeneficiaryController(BaseController):

    @expose('json')
    def load(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(BeneficiarioMoral, 'id_beneficiariomoral', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(BeneficiarioMoral, 'id_beneficiariomoral', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(BeneficiarioMoral).filter_by(id_beneficiariomoral=kw['id']).first()
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(BeneficiarioMoral).filter_by(beneficiariomoral=kw['beneficiariomoral']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                if kw['rfc'] != "":
                    handler = DBSession.query(BeneficiarioMoral).filter_by(rfc=kw['rfc']).filter_by(internal_id=kw['internal_id']).first()
                if handler == None:
                    handler = BeneficiarioMoral()
                else:
                    return dict(error=l_('This rfc alredy exist'))
            else:
                return dict(error=l_('This beneficiary alredy exist'))
            i = 0
        else:
            handler = DBSession.query(BeneficiarioMoral).filter_by(id_beneficiariomoral=kw['id']).first()
            i = 1

        handler.beneficiariomoral = kw['beneficiariomoral']
        handler.telefono = kw['telefono']
        handler.email = kw['email']
        handler.domicilio = kw['domicilio']
        handler.representante_legal = kw['representante_legal']
        handler.rfc = kw['rfc']
        handler.banco = kw['banco']
        handler.cta_bancaria = kw['cta_bancaria']
        handler.banco2 = kw['banco2']
        handler.cta_bancaria2 = kw['cta_bancaria2']
        handler.internal_id = kw['internal_id']
        handler.application_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_beneficiariomoral)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = BeneficiarioMoral()
        else:
            kw['handler'] = DBSession.query(BeneficiarioMoral).filter_by(id_beneficiariomoral=kw['id']).first()
        dialogtemplate = render_template(kw, "mako",'pythonjupiter.templates.Catalogs.Forms.extern_beneficiary_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.Catalogs.extern_beneficiary_jqgrid')
    # @require(predicates.not_anonymous())
    def extern_beneficiaries(self, **kw):
        return dict(page='extern_beneficiary_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])
