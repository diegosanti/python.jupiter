from pythonjupiter.lib.base import BaseController
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import AbonoCuenta
from tg import app_globals
import requests
from tg import expose
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from pythonjupiter.model.tables import Linea,DatosUnidad,Persona,CargoOperador,Derrotero,Grupo,UnidadOperador,EstadoCuenta,Condonaciones,CargoUnidad,GrupoPersona
from pythonjupiter.model.tables import UnidadCargo,OperadorCargo,Body,Head,CondonacionOperador,CondonacionUnidad,Grupo_DatosUnidad,Salida,Motivo,BeneficiarioMoral
from pythonjupiter.model.tables import CuentaCargo
from pythonjupiter.lib.messagequeue import Message
from datetime import datetime,timedelta
from tg import render_template
from pythonjupiter.lib.utility import ExportPDF,ExportCSV
from dateutil import relativedelta as rdelta
import calendar
from datetime import date
import csv
import os
from shutil import copyfileobj,copyfile

class BodyHeadController(BaseController):

    def __init__(self):
        pass


    @expose('json')
    def loadsalida(self, **kw):
        if kw['app_name']=='TODOveritodo':
            filter = [('internal_id', 'eq', kw['internal_id']),('fecha','eq',datetime.now().date())]
        else:
            filter = [('application_id','eq',kw['application_id']),('fecha','eq',datetime.now().date())]

        return jqgridDataGrabber(Salida, 'id_salida', filter, kw).loadGrid()




    @expose('json')
    def loadsalidasearch(self, **kw):
        if kw['app_name']=='TODOveritodo':
            if kw['buscarpor']=='-Seleccione-':
                filter = [('internal_id', 'eq', kw['internal_id']),('fecha','le',kw['fechafin']),('fecha','ge',kw['fechaini'])]
            elif kw['buscarpor']=='Tipo':
                filter = [('tipo','eq',kw['busca']),('internal_id', 'eq', kw['internal_id']), ('fecha', 'le', kw['fechafin']),('fecha', 'ge', kw['fechaini'])]

            elif kw['buscarpor']=='Motivo':
                querymotivo=DBSession.query(Motivo).filter(Motivo.motivo.like('%'+kw['busca']+'%')).filter_by(internal_id=kw['internal_id']).first()
                if querymotivo is not None:
                    idmotivo=querymotivo.id_motivo
                else:
                    idmotivo=''

                filter = [('id_motivo', 'eq', idmotivo), ('internal_id', 'eq', kw['internal_id']),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]


            elif kw['buscarpor']=='Beneficiario':
                querybeneficiario=DBSession.query(BeneficiarioMoral).filter(BeneficiarioMoral.beneficiariomoral.like('%'+kw['busca']+'%')).filter_by(internal_id=kw['internal_id']).first()
                if querybeneficiario is not None:
                    idbeneficiario=querybeneficiario.id_beneficiariomoral
                else:
                    idbeneficiario=''

                filter = [('id_beneficiariomoral', 'eq', idbeneficiario), ('internal_id', 'eq', kw['internal_id']),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]

            elif kw['buscarpor']=='Persona':
                querypersona=DBSession.query(Persona).filter(Persona.nombre.like('%'+kw['busca']+'%')).filter_by(internal_id=kw['internal_id']).first()
                if querypersona is not None:
                    idpersona=querypersona.id_persona
                else:
                    idpersona=''

                filter = [('id_persona', 'eq', idpersona), ('internal_id', 'eq', kw['internal_id']),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]

            elif kw['buscarpor']=='Bolsa':
                querycuenta=DBSession.query(CuentaCargo).filter(CuentaCargo.nombre.like('%'+kw['busca']+'%')).filter_by(internal_id=kw['internal_id']).first()
                if querycuenta is not None:
                    idcuenta=querycuenta.id_cuenta
                else:
                    idcuenta=''

                filter = [('cuentacargo_id', 'eq', idcuenta), ('internal_id', 'eq', kw['internal_id']),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]

        else:
            if kw['buscarpor']=='-Seleccione-':
                filter = [('application_id','eq',kw['application_id']),('fecha','le',kw['fechafin']),('fecha','ge',kw['fechaini'])]

            elif kw['buscarpor']=='Tipo':
                filter = [('application_id','eq',kw['application_id']),('tipo','eq',kw['busca']), ('fecha', 'le', kw['fechafin']),('fecha', 'ge', kw['fechaini'])]


            elif kw['buscarpor']=='Motivo':
                querymotivo=DBSession.query(Motivo).filter(Motivo.motivo.like('%'+kw['busca']+'%')).filter_by(application_id=kw['application_id']).first()
                if querymotivo is not None:
                    idmotivo=querymotivo.id_motivo
                else:
                    idmotivo=''

                filter = [('application_id','eq',kw['application_id']),('id_motivo', 'eq', idmotivo),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]

            elif kw['buscarpor']=='Beneficiario':
                querybeneficiario=DBSession.query(BeneficiarioMoral).filter(BeneficiarioMoral.beneficiariomoral.like('%'+kw['busca']+'%')).filter_by(application_id=kw['application_id']).first()
                if querybeneficiario is not None:
                    idbeneficiario=querybeneficiario.id_beneficiariomoral
                else:
                    idbeneficiario=''

                filter = [('application_id','eq',kw['application_id']),('id_beneficiariomoral', 'eq', idbeneficiario),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]


            elif kw['buscarpor']=='Persona':
                querypersona=DBSession.query(Persona).filter(Persona.nombre.like('%'+kw['busca']+'%')).filter_by(application_id=kw['application_id']).first()
                if querypersona is not None:
                    idpersona=querypersona.id_persona
                else:
                    idpersona=''

                filter = [('application_id','eq',kw['application_id']),('id_persona', 'eq', idpersona),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]

            elif kw['buscarpor']=='Bolsa':
                querycuenta=DBSession.query(CuentaCargo).filter(CuentaCargo.nombre.like('%'+kw['busca']+'%')).filter_by(application_id=kw['application_id']).first()
                if querycuenta is not None:
                    idcuenta=querycuenta.id_cuenta
                else:
                    idcuenta=''

                filter = [('application_id','eq',kw['application_id']),('cuentacargo_id', 'eq', idcuenta),('fecha', 'le', kw['fechafin']), ('fecha', 'ge', kw['fechaini'])]


        return jqgridDataGrabber(Salida, 'id_salida', filter, kw).loadGrid()






    @expose('json')
    def load(self, **kw):
        if kw['soy']=='nouser':
            if kw['app_name']=='TODOveritodo':
                filter = [('internal_id', 'eq', kw['internal_id']),('fecha', 'eq', datetime.now().date() - timedelta(days=1))]
            else:
                filter = [('application_id','eq',kw['appID']),('fecha','eq',datetime.now().date() - timedelta(days=1))]
        else:
            queryelusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).first()
            queryunidades=DBSession.query(DatosUnidad).filter_by(id_persona=queryelusuario.id_persona).all()
            elfilter=''
            for itemunidades in queryunidades:
                elfilter=elfilter+str(itemunidades.id_unidad)+','
            filter = [('id_unidad', 'in', elfilter), ('application_id', 'eq', kw['appID']),('fecha', 'eq', datetime.now().date() - timedelta(days=1))]
        return jqgridDataGrabber(Head, 'id_head', filter, kw).loadGrid()

    @expose('json')
    def load2(self, **kw):
        if 'unidad' in kw:
            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['unidad']).filter_by(app_id=kw['appID']).first()
        if 'operador' in kw:
            queryoperador=DBSession.query(Persona).filter_by(nombre=kw['operador']).filter_by(application_id=kw['appID']).first()
        try:
            idoperador=queryoperador.id_persona
        except:
            idoperador = 0
        try:
            fecha=kw['fecha']
        except:
            fecha=''
        filter = [('application_id','eq', kw['appID']),('id_operador','eq',idoperador),('fecha','le',fecha),('faltante','gt',0)]
        return jqgridDataGrabber(Head, 'id_head', filter, kw).loadGrid()

    @expose('json')
    def load3(self, **kw):
        if 'unidad' in kw:
            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['unidad']).filter_by(app_id=kw['appID']).first()
        if 'operador' in kw:
            queryoperador=DBSession.query(Persona).filter_by(nombre=kw['operador']).filter_by(application_id=kw['appID']).first()
        try:
            idunidad=queryunidad.id_unidad
        except:
            idunidad = 0
        try:
            fecha=kw['fecha']
        except:
            fecha=''
        try:
            idoperador=queryoperador.id_persona
        except:
            idoperador=0
        filter = [('application_id', 'eq', kw['appID']),('id_unidad','eq',idunidad),('id_operador','eq',None),('fecha','le',fecha),('faltante','gt',0)]

        return jqgridDataGrabber(Head, 'id_head', filter, kw).loadGrid()




    @expose('json')
    def searchgrid(self,**kw):
        if kw['status']=='-Seleccione-' and kw['por']=='Unidad' and kw['eco']!='-Seleccione-':
            if kw['app_name']!='TODO':
                queryunidad=DBSession.query(DatosUnidad).filter_by(app_name=kw['app_name']).filter_by(eco1=kw['eco']).filter_by(internal_id=kw['internal_id']).first()
            else:
                queryunidad = DBSession.query(DatosUnidad).filter_by(eco1=kw['eco']).filter_by(internal_id=kw['internal_id']).first()

            filter = [('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal']),('id_unidad','eq',queryunidad.id_unidad)]

        elif kw['status']=='-Seleccione-' and kw['por']=='Operador' and kw['operator']!='-Seleccione-':
            queryoperador=DBSession.query(Persona).filter_by(nombre=kw['operator']).first()
            filter = [('internal_id', 'eq', kw['internal_id']), ('id_operador', 'eq', queryoperador.id_persona),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal'])]

        elif kw['status']!='-Seleccione-' and kw['por']=='Unidad' and kw['eco']!='-Seleccione-':
            if kw['app_name']!='TODO':
                queryunidad=DBSession.query(DatosUnidad).filter_by(app_name=kw['app_name']).filter_by(eco1=kw['eco']).filter_by(internal_id=kw['internal_id']).first()
            else:
                queryunidad = DBSession.query(DatosUnidad).filter_by(eco1=kw['eco']).filter_by(internal_id=kw['internal_id']).first()
            filter = [('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal']),('id_unidad','eq',queryunidad.id_unidad),('estatus','eq',kw['status'])]

        elif kw['status']!='-Seleccione-' and kw['por']=='Operador' and kw['operator']!='-Seleccione-':
            queryoperador=DBSession.query(Persona).filter_by(nombre=kw['operator']).first()
            filter = [('internal_id', 'eq', kw['internal_id']), ('id_operador', 'eq', queryoperador.id_persona),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal']),('estatus','eq',kw['status'])]


        elif kw['status']!='-Seleccione-' and kw['por']=='Unidad' and kw['eco']=='-Seleccione-':
            if kw['soy']=='nouser':
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['eco']).first()
                if kw['app_name']!='TODO':
                    filter = [('app_name','in','TODO,'+kw['app_name']),('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal']),('estatus','eq',kw['status'])]
                else:
                    filter = [('internal_id', 'eq', kw['internal_id']),
                              ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal']),
                              ('estatus', 'eq', kw['status'])]
            else:
                queryelusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
                queryunidades = DBSession.query(DatosUnidad).filter_by(id_persona=queryelusuario.id_persona).all()
                elfilter = ''
                for itemunidades in queryunidades:
                    elfilter = elfilter + str(itemunidades.id_unidad) + ','
                filter = [('id_unidad','in',elfilter),('internal_id', 'eq', kw['internal_id']),
                          ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal']),
                          ('estatus', 'eq', kw['status'])]



        elif kw['status']!= '-Seleccione-' and kw['por'] == 'Operador' and kw['operator'] == '-Seleccione-':
            queryoperador = DBSession.query(Persona).filter_by(nombre=kw['operator']).first()
            if kw['app_name']!='TODO':
                filter = [('app_name','in','TODO,'+kw['app_name']),('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal']),('estatus','eq',kw['status'])]
            else:
                filter = [('internal_id', 'eq', kw['internal_id']),
                          ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal']),
                          ('estatus', 'eq', kw['status'])]

        elif kw['status']=='-Seleccione-' and kw['por']=='Unidad' and (kw['operator'] == '-Seleccione-' or kw['eco']=='-Seleccione-'):
            if kw['soy']=='nouser':
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['eco']).first()
                if kw['app_name']!='TODO':
                    filter = [('app_name','in','TODO,'+kw['app_name']),('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal'])]
                else:
                    filter = [('internal_id', 'eq', kw['internal_id']),
                              ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal'])]
            else:
                queryelusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
                queryunidades = DBSession.query(DatosUnidad).filter_by(id_persona=queryelusuario.id_persona).all()
                elfilter = ''
                for itemunidades in queryunidades:
                    elfilter = elfilter + str(itemunidades.id_unidad) + ','
                filter = [('id_unidad','in',elfilter),('internal_id', 'eq', kw['internal_id']),
                          ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal'])]

        elif kw['status']== '-Seleccione-' and kw['por'] == 'Operador' and (kw['operator'] == '-Seleccione-' or kw['eco']=='-Seleccione-'):
            queryoperador = DBSession.query(Persona).filter_by(nombre=kw['operator']).first()
            if kw['app_name']!='TODO':
                filter = [('app_name','in','TODO,'+kw['app_name']),('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal'])]
            else:
                filter = [('internal_id', 'eq', kw['internal_id']),
                          ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal'])]

        else:
            if kw['app_name']!='TODO':
                filter = [('app_name','in','TODO,'+kw['app_name']),('internal_id', 'eq', kw['internal_id']),('fecha', 'ge', kw['date']),('fecha', 'le', kw['datefinal'])]
            else:
                filter = [('internal_id', 'eq', kw['internal_id']),
                          ('fecha', 'ge', kw['date']), ('fecha', 'le', kw['datefinal'])]
        return jqgridDataGrabber(Head, 'id_head', filter, kw).loadGrid()


    @expose('json')
    def prueba(self,**kw):
        #print("LA KW: ",kw)
        #{'oper': 'edit', 'id_body': '59', 'id_head': '33', 'condonacion': '20'}
        querybody=DBSession.query(Body).filter_by(id_body=kw['id_body']).first()
        operador=querybody.id_operador
        unidad = querybody.id_unidad
        condonacion=kw['condonacion']

        monto=querybody.monto
        saldo=querybody.saldo
        paga=querybody.pagado
        condonacionpasada=querybody.condonacion

        resta=monto-paga

        print("------------------")
        print("operador-->",operador)
        print("unidad--->",unidad)
        print("condonacion--->",condonacion)
        print("monto--->",monto)
        print("saldo-->",saldo)
        print("paga--->",paga)
        print("condonacionpasada-->",condonacionpasada)
        print("resta--->",resta)
        print("------------------")

        if resta<int(condonacion):
            print("NO SE PUEDE CONDONAR ESA CANTIDAD")
            #print("LA RESTA ES: ",resta)
            #print("Condonacion es: ",int(condonacion))
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar")
            return dict(error='masdelmonto')



        if(int(condonacion)!=condonacionpasada):
            querybody.condonacion = int(condonacion)
            querybody.saldo = monto - int(condonacion)
            if querybody.saldo <0:
                print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                querybody.saldo = saldo
                querybody.condonacion = condonacionpasada
                Message.post("testlistener_" + kw['user'],'MSG_' + 'RED' + "|" + "No se puede condonar 2")
                return dict(error='masdelmonto')


            if unidad is not None:
                saldoentero=0
                saldoverdadero=0
                saldocondonacionestotal=0
                queryhead=DBSession.query(Head).filter_by(id_unidad=unidad).filter_by(fecha=querybody.fecha).filter(Head.faltante>0).first()
                if queryhead is not None:
                    querybodies = DBSession.query(Body).filter_by(fecha=queryhead.fecha).filter_by(id_unidad=unidad).all()
                    for itembodies in querybodies:
                        saldoentero=saldoentero+itembodies.saldo
                        saldocondonacionestotal=saldocondonacionestotal+itembodies.condonacion
                        saldoverdadero=saldoverdadero+itembodies.monto
                    porpagar=queryhead.porpagar
                    pagado=queryhead.pagado
                    faltante=queryhead.faltante
                    if saldoentero<queryhead.pagado:
                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                        querybody.saldo=saldo
                        querybody.condonacion=condonacionpasada
                        Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede condonar, cantidad en exceso")
                        return dict(error='exceso')
                    else:
                        print("SI SE PUEDE")
                        querycondonacion = DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(fechainicial=datetime.now().date()). \
                        filter_by(fechafinal=datetime.now().date()).filter_by(descripcion='Realizada: '+str(datetime.now().date())+' por '+kw['user']). \
                        filter_by(para='Unidad').filter_by(monto=int(condonacion)).filter_by(application_id=kw['application_id']). \
                        filter_by(usuario=kw['user']).first()
                        if querycondonacion is None:
                            newcondonacion=Condonaciones()
                            newcondonacion.cancelado=0
                            newcondonacion.fechainicial=datetime.now().date()
                            newcondonacion.fechafinal=datetime.now().date()
                            newcondonacion.descripcion='Realizada: '+str(datetime.now().date())+' por '+kw['user']
                            newcondonacion.para='Unidad'
                            newcondonacion.monto=int(condonacion)
                            newcondonacion.internal_id=kw['internal_id']
                            newcondonacion.usuario=kw['user']
                            newcondonacion.application_id=kw['application_id']
                            DBSession.add(newcondonacion)
                            querycondonacion = DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(fechainicial=datetime.now().date()). \
                            filter_by(fechafinal=datetime.now().date()).filter_by(descripcion='Realizada: '+str(datetime.now().date())+' por '+kw['user']). \
                            filter_by(para='Unidad').filter_by(monto=int(condonacion)).filter_by(application_id=kw['application_id']). \
                            filter_by(usuario=kw['user']).first()

                        querycondonacionunidad = DBSession.query(CondonacionUnidad).filter_by(condonacion_id=querycondonacion.id_condonacion) \
                        .filter_by(unidad_id=queryhead.id_unidad).first()
                        if querycondonacionunidad is None:
                            newcu = CondonacionUnidad()
                            newcu.condonacion_id = querycondonacion.id_condonacion
                            newcu.unidad_id = queryhead.id_unidad
                            if querybody.id_cargounidad is not None:
                                newcu.cargounidad_id=querybody.id_cargounidad

                            DBSession.add(newcu)

                        queryhead.porpagar=saldoentero
                        queryhead.faltante= queryhead.porpagar - queryhead.pagado
                        if queryhead.faltante == 0:
                            queryhead.faltante=0
                            #queryhead.porpagar=0
                            queryhead.estatus = 'PAGADO'


            else:
                saldoentero=0
                saldoverdadero=0
                saldocondonacionestotal=0
                queryhead=DBSession.query(Head).filter_by(id_unidad=None).filter_by(id_operador=operador).filter_by(fecha=querybody.fecha).filter(Head.faltante>0).first()
                if queryhead is not None:
                    querybodies = DBSession.query(Body).filter_by(fecha=queryhead.fecha).filter_by(id_operador=operador).all()
                    for itembodies in querybodies:
                        saldoentero=saldoentero+itembodies.saldo
                        saldocondonacionestotal = saldocondonacionestotal + itembodies.condonacion
                        saldoverdadero=saldoverdadero+itembodies.monto
                    porpagar=queryhead.porpagar
                    pagado=queryhead.pagado
                    faltante=queryhead.faltante
                    if saldoentero<queryhead.pagado:
                        print("NO SE PUEDE CONDONAR ESA CANTIDAD")
                        querybody.saldo=saldo
                        querybody.condonacion=condonacionpasada
                        Message.post("testlistener_" + kw['user'],'MSG_' + 'RED' + "|" + "No se puede condonar, cantidad en exceso")
                        return dict(error='exceso')
                    else:
                        print("SI SE PUEDE")
                        newcondonacion=Condonaciones()
                        newcondonacion.cancelado=0
                        newcondonacion.fechainicial=datetime.now().date()
                        newcondonacion.fechafinal=datetime.now().date()
                        newcondonacion.descripcion='Realizada: '+str(datetime.now().date())+' por '+kw['user']
                        newcondonacion.para='Operador'
                        newcondonacion.monto=int(condonacion)
                        newcondonacion.internal_id=kw['internal_id']
                        newcondonacion.usuario=kw['user']
                        newcondonacion.application_id=kw['application_id']
                        DBSession.add(newcondonacion)

                        querycondonacion=DBSession.query(Condonaciones).filter_by(cancelado=0).filter_by(fechainicial=datetime.now().date()).\
                        filter_by(fechafinal=datetime.now().date()).filter_by(descripcion='Realizada: '+str(datetime.now().date())+' por '+kw['user']).\
                        filter_by(para='Operador').filter_by(monto=int(condonacion)).filter_by(application_id=kw['application_id']).\
                        filter_by(usuario=kw['user']).first()

                        querycondonacionoperador=DBSession.query(CondonacionOperador).filter_by(condonacion_id=querycondonacion.id_condonacion)\
                                        .filter_by(operador_id=queryhead.id_operador).first()
                        if querycondonacionoperador is None:
                            newco=CondonacionOperador()
                            newco.condonacion_id=querycondonacion.id_condonacion
                            newco.operador_id=queryhead.id_operador
                            newco.cargooperador_id=querybody.id_cargopersona
                            DBSession.add(newco)

                        queryhead.porpagar=saldoentero
                        queryhead.faltante= queryhead.porpagar - queryhead.pagado
                        if queryhead.faltante == 0:
                            queryhead.faltante=0
                            #queryhead.porpagar=0
                            queryhead.estatus = 'PAGADO'
        else:
            print("YA SE HIZO LA CONDONACION CON ESE MONTO")
            return dict(error='mismo')
        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Condonacion Exitosa")
        return dict(error='ok')


    @expose('json')
    def loadbody(self, **kw):
        querygethead=DBSession.query(Head).filter_by(id_head=kw['id_head']).first()
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=querygethead.id_unidad).first()

        if queryunidad is not None:
            filter = [('id_head', 'eq',kw['id_head']),('fecha', 'eq', querygethead.fecha)]

        else:
            querypersona = DBSession.query(Persona).filter_by(id_persona=querygethead.id_operador).filter_by().first()
            if querypersona is not None:
                filter = [('id_head', 'eq',kw['id_head']),('fecha', 'eq', querygethead.fecha)]


        return jqgridDataGrabber(Body, 'id_body', filter, kw).loadGrid()


    @expose('json')
    def getunidad(self,**kw):
        id=kw['id']
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=id).first()
        unidad=queryunidad.eco1
        return dict(unidad=unidad)

    @expose('json')
    def getoperador(self,**kw):
        id=kw['id']
        queryoperador=DBSession.query(Persona).filter_by(id_persona=id).first()
        operador=queryoperador.nombre
        return dict(operador=operador)

    @expose('json')
    def espolizasiono(self, **kw):
        id=kw['row_id']
        poliza=''
        querypoliza=DBSession.query(Body).filter_by(id_body=id).first()
        print("POLIZA=>",querypoliza.espoliza)
        espoliza=querypoliza.espoliza
        if espoliza==1:
            poliza='SI'
        else:
            poliza='NO'
        return dict(poliza=poliza)

    @expose('json')
    def getcargounidad(self,**kw):
        id=kw['id']
        print("THE ROW_ID==> ",kw['row_id'])
        querycargounidad=DBSession.query(CargoUnidad).filter_by(id_cargounidad=id).first()
        cargounidad=querycargounidad.cargo
        return dict(cargounidad=cargounidad)

    @expose('json')
    def getcargopersona(self,**kw):
        id=kw['id']
        querycargopersona=DBSession.query(CargoOperador).filter_by(id_cargooperador=id).first()
        cargopersona=querycargopersona.cargo
        return dict(cargopersona=cargopersona)

    @expose('json')
    def templateabonar(self,**kw):
        template = render_template({"id": kw['id'],'internal_id': kw['internal_id'],'user': kw['user']}, "mako",'pythonjupiter.templates.BodyHead.Pagar')
        return dict(template=template)

    @expose('json')
    def save(self,**kw):
        print(kw)
        #{'user': 'milla', 'elid': '30', 'internal_id': '5', 'jupiter_pago': '52'}
        queryhead=DBSession.query(Head).filter_by(id_head=kw['elid']).first()
        anterior=queryhead.pagado
        pagado=anterior + int(kw['jupiter_pago'])
        if(pagado>queryhead.porpagar):
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "No se puede Exceder el monto a pagar")
            return dict(error='error')
        else:
            queryhead.pagado=pagado
            queryhead.faltante=queryhead.porpagar - queryhead.pagado
            if(queryhead.faltante==0):
                queryhead.estatus='Pagado'
            elif(queryhead.faltante>0):
                queryhead.estatus='Falta'

            return dict(error='ok')


    @expose('json')
    def generopdf(self,**kw):
        fechainicial=kw['fechainicial']
        fechafinal=kw['fechafinal']
        por=kw['por']
        valor=kw['valor']
        construir=[]
        construirope=[]
        totalporpagar=0
        totalpagado=0
        totalfaltante=0

        if valor!='-Seleccione-':
            if por=='Unidad':
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=valor).first()
                queryheadunidad=DBSession.query(Head).filter_by(id_unidad=queryunidad.id_unidad).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).all()
                for itemhead in queryheadunidad:
                    querybody=DBSession.query(Body).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=itemhead.fecha).all()
                    for itembody in querybody:
                        querycargounidad = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itembody.id_cargounidad).first()
                        construir.append({'fecha': itembody.fecha,'cargo': querycargounidad.cargo,'monto': itembody.monto,'condonacion': itembody.condonacion,'saldo': itembody.saldo,'pagado': itembody.pagado})

                    totalporpagar=totalporpagar+itemhead.porpagar
                    totalpagado=totalpagado+itemhead.pagado
                    totalfaltante=totalfaltante+itemhead.faltante
                    listatotales=[totalporpagar,totalpagado,totalfaltante]
                file_name = ExportPDF.create({"valor": kw['valor'],"heads": queryheadunidad,"fechainicial": fechainicial,"fechafinal": fechafinal,'body': construir,'listatotales': listatotales},
                                         kw['valor'], "pythonjupiter.templates.BodyHead.pdf",kw['user'])
            else:
                querypersona=DBSession.query(Persona).filter_by(nombre=valor).first()
                queryheadoperador=DBSession.query(Head).filter_by(id_operador=querypersona.id_persona).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).all()
                for item in queryheadoperador:
                    querybody=DBSession.query(Body).filter_by(id_operador=querypersona.id_persona).filter_by(fecha=item.fecha).all()
                    for itembody in querybody:
                        querycargooperador=DBSession.query(CargoOperador).filter_by(id_cargooperador=itembody.id_cargopersona).first()
                        construirope.append({'fecha': itembody.fecha,'cargo': querycargooperador.cargo,'monto': itembody.monto,'condonacion': itembody.condonacion,'saldo': itembody.saldo,'pagado': itembody.pagado})

                    totalporpagar=totalporpagar+item.porpagar
                    totalpagado=totalpagado+item.pagado
                    totalfaltante=totalfaltante+item.faltante
                    listatotales=[totalporpagar,totalpagado,totalfaltante]
                file_name = ExportPDF.create({"valor": kw['valor'],"heads": queryheadoperador,"fechainicial": fechainicial,"fechafinal": fechafinal,'body': construirope,'listatotales': listatotales},
                                         kw['valor'], "pythonjupiter.templates.BodyHead.pdf",kw['user'])

            print("FECHA INICIAL: ", fechainicial)
            print("FECHA fechafinal: ", fechafinal)
            print("por: ",por)
            print("valor: ",valor)
            return dict(name=file_name,error='ok')
        else:
            return dict(name='nada',error='no')



    @expose('json')
    def billingstatement(self,**kw):
        listabodies=[]
        allrecords=[]
        listaallrecords=[]
        #print("FILENAME--->",file_namecsv)
        totalporpagar=0
        totalfaltante=0
        totalpagado=0
        internal_id=kw['internal_id']
        application_id=kw['application_id']
        if kw['por']=='Unidad' and kw['unidad']!='-Seleccione-':
            unidad=kw['unidad']
            listadeheads=[]
            listadequien=[]
            listacuantospagos=[]
            listathetotalescargos=[]
            listasupertotal={}
            #listadebodies=[]
            if unidad!='TODAS':
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=unidad).filter_by(app_id=application_id).all()
            else:
                if kw['app_name']!='TODOveritodo':
                    queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).all()
                else:
                    queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=application_id).all()


            allrecords.append(
                    "PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append("NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
            supertotalporpagar = 0
            supertotalpagado = 0
            supertotalfaltante = 0
            for itemunidad in queryunidad:
                totalporpagar=0
                totalpagado=0
                totalfaltante=0
                queryheads=DBSession.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter(Head.fecha>=kw['fechainicial']).filter(Head.fecha<=kw['fechafinal']).all()
                querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad.id_unidad).filter(AbonoCuenta.fechacuenta>=kw['fechainicial']).filter(AbonoCuenta.fechacuenta<=kw['fechafinal']).all()
                querypersona=DBSession.query(Persona).filter_by(id_persona=itemunidad.id_persona).filter_by(espermisionario=1).first()
                queryempresa=DBSession.query(Linea).filter_by(id_linea=itemunidad.id_linea).first()
                listadeheads.append(queryheads)
                for itemhead in queryheads:
                    querybody=DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(fecha=itemhead.fecha).all()
                    #listadebodies.append(querybody)
                    for itembody in querybody:
                        querycargounidad = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itembody.id_cargounidad).first()
                        if querycargounidad is not None:
                            listabodies.append({'fecha': itembody.fecha, 'cargo': querycargounidad.cargo,'monto': itembody.monto, 'condonacion': itembody.condonacion,
                                            'saldo': itembody.saldo,'pagado': itembody.pagado,'idunidad': itemunidad.id_unidad,'tipodecargo': querycargounidad.tipodecargo})
                            if querycargounidad.cargo in listaallrecords:
                                pass
                            else:
                                listaallrecords.append(querycargounidad.cargo)
                                allrecords.append(querycargounidad.cargo+'\t')
                        else:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado,'idunidad': itemunidad.id_unidad,'tipodecargo': 1})
                            if 'POLIZA' in listaallrecords:
                                pass
                            else:
                                listaallrecords.append('POLIZA')
                                allrecords.append('POLIZA'+'\t')


                    totalporpagar=totalporpagar+itemhead.porpagar
                    totalpagado=totalpagado+itemhead.pagado
                    totalfaltante=totalfaltante+itemhead.faltante

                    supertotalporpagar = supertotalporpagar + itemhead.porpagar
                    supertotalpagado = supertotalpagado + itemhead.pagado
                    supertotalfaltante = supertotalfaltante + itemhead.faltante

                listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                     'persona': querypersona.nombre,'totalporpagar': totalporpagar,'totalpagado': totalpagado,'totalfaltante': totalfaltante,
                                     'empresa': queryempresa.nombre_linea})
            cuantas=len(listadequien)
            allrecords.append("TOTAL DESCUENTOS"+'\t'+"IMPORTE A PAGAR"+'\t'+"DEBERIAS PAGAR"+'\t'+"FALTANTE"+'\t'+'EMPRESA'+'\t'+'OBSERVACIONES'+'\n')


            listasupertotal={'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante}

            thetotalpagado=0
            thetotalsaldoneto=0
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0
            for itemquien in listadequien:
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(Head.fecha<kw['fechainicial']).all()
                totalparafaltante=0
                for itemhead in queryheads:
                    totalparafaltante=totalparafaltante+itemhead.faltante

                saldoneto=itemquien['totalpagado']-totalparafaltante
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                allrecords.append(itemquien['persona']+'\t'+str(itemquien['eco'])+'\t'+str(itemquien['pagos'])+'\t'+str(totalparafaltante)+'\t'
                                  +str(itemquien['totalpagado'])+'\t'+str(saldoneto)+'\t')

                supertotalbody=0
                for itemdebody in listaallrecords:
                    eltotal=0
                    for itemotrobody in listabodies:
                        if itemdebody==itemotrobody['cargo'] and itemquien['id']==itemotrobody['idunidad']:
                            eltotal=eltotal+itemotrobody['pagado']
                    querycargoverificar=DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).filter_by(application_id=application_id).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo==1 or querycargoverificar.tipodecargo==3:
                            supertotalbody=supertotalbody+eltotal
                    elif itemdebody=='POLIZA':
                        supertotalbody = supertotalbody + eltotal
                    #print("EL TOTAL de ",itemdebody," ES: ",eltotal)
                    allrecords.append(str(eltotal)+'\t')
                    mibandera = 0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo') == itemdebody:
                            mibandera = 1
                            break

                    if mibandera == 1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo'] == itemdebody:
                            itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal

                #print("EL SUPERTOTAL:::>",str(supertotalbody))

                #allrecords.append('VALE'+'\t')
                thetotalsupertotalboy=thetotalsupertotalboy+supertotalbody
                allrecords.append(str(supertotalbody)+'\t')
                importeapagar=saldoneto-supertotalbody
                thetotalimporteapagar=thetotalimporteapagar+importeapagar
                thetotaltotalapagar=thetotaltotalapagar+itemquien['totalporpagar']
                thetotaltotalfaltante=thetotaltotalfaltante+itemquien['totalfaltante']
                allrecords.append(str(importeapagar)+'\t'+str(itemquien['totalporpagar'])+'\t'+str(itemquien['totalfaltante'])+'\t'+itemquien['empresa']+'\t')
                if importeapagar<=0:
                    allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

                allrecords.append('\n')
                lastindex=len(listadequien)
                if listadequien[lastindex - 1] == itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total']) + '\t')
                    #allrecords.append('TOTALVALE' + '\t')
                    allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                        thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                    thetotalpagado = 0
                    thetotalsaldoneto = 0
                    listathetotalescargos = []
                    thetotalsupertotalboy = 0
                    thetotalimporteapagar = 0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')



            file_namecsv = ExportCSV.create(allrecords, "UNIDAD"+unidad+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])


            print("listasupertotal========>", listasupertotal)
            file_name = ExportPDF.create({'por': 'UNIDAD','quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                                          'fechainicial': kw['fechainicial'], 'fechafinal':
                                              kw['fechafinal'], 'totalporpagar': totalporpagar,
                                          'totalpagado': totalpagado, 'totalfaltante': totalfaltante,'cuantas':cuantas,'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal},
                                         unidad+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
                                         'prueba')
            template=render_template({'por': 'UNIDAD','quien': listadequien,'queryheads': listadeheads,'listabodies': listabodies,'fechainicial': kw['fechainicial'],'fechafinal':
                                      kw['fechafinal'],'totalporpagar': totalporpagar,'totalpagado': totalpagado,'totalfaltante': totalfaltante,
                                      'cuantas': cuantas,'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal},'mako','pythonjupiter.templates.BillingStatement.htmlpdf')




        elif kw['por']=='Operador' and kw['operador']!='-Seleccione-':
            operador=kw['operador']
            allrecords = []
            listadeheads = []
            listadequien = []
            listacuantospagos = []
            listathetotalescargos=[]
            if operador!='TODOS':
                queryoperador=DBSession.query(Persona).filter_by(nombre=operador).filter_by(application_id=application_id).filter_by(esoperador=1).all()
            else:
                queryoperador = DBSession.query(Persona).filter_by(application_id=application_id).filter_by(esoperador=1).all()

            allrecords.append(
                    "PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append("OPERADOR"+'\t'+"NUMERO OPERADOR"+'\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
            supertotalporpagar = 0
            supertotalpagado = 0
            supertotalfaltante = 0
            for itemoperador in queryoperador:
                totalporpagar = 0
                totalpagado = 0
                totalfaltante = 0
                queryheads=DBSession.query(Head).filter_by(id_unidad=None).filter_by(id_operador=itemoperador.id_persona).filter(Head.fecha>=kw['fechainicial']).filter(Head.fecha<=kw['fechafinal']).all()
                querypagos = DBSession.query(AbonoCuenta).filter_by(operador_id=itemoperador.id_persona).filter(AbonoCuenta.fechacuenta>=kw['fechainicial']).filter(AbonoCuenta.fechacuenta<=kw['fechafinal']).all()
                queryempresa = DBSession.query(Linea).filter_by(id_linea=itemoperador.id_linea).first()
                listadeheads.append(queryheads)
                for itemhead in queryheads:
                    querybody=DBSession.query(Body).filter_by(id_operador=itemoperador.id_persona).filter_by(fecha=itemhead.fecha).all()
                    for itembody in querybody:
                        querycargooperador = DBSession.query(CargoOperador).filter_by(id_cargooperador=itembody.id_cargopersona).first()
                        listabodies.append({'fecha': itembody.fecha, 'cargo': querycargooperador.cargo,'monto': itembody.monto, 'condonacion': itembody.condonacion,
                                            'saldo': itembody.saldo,'pagado': itembody.pagado,'idoperador': itemoperador.id_persona})
                        if querycargooperador.cargo in listaallrecords:
                            pass
                        else:
                            listaallrecords.append(querycargooperador.cargo)
                            allrecords.append(querycargooperador.cargo + '\t')

                    totalporpagar=totalporpagar+itemhead.porpagar
                    totalpagado=totalpagado+itemhead.pagado
                    totalfaltante=totalfaltante+itemhead.faltante

                    supertotalporpagar = supertotalporpagar + itemhead.porpagar
                    supertotalpagado = supertotalpagado + itemhead.pagado
                    supertotalfaltante = supertotalfaltante + itemhead.faltante

                try:
                    queryempresaempresa=queryempresa.nombre_linea
                except:
                    queryempresaempresa=''

                listadequien.append({'nombre': itemoperador.nombre, 'id': itemoperador.id_persona, 'numerooperador': itemoperador.numerooperador,'pagos': len(querypagos),
                                     'totalporpagar': totalporpagar,'totalpagado': totalpagado,'totalfaltante': totalfaltante,
                                     'empresa': queryempresaempresa})

            allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')

            listasupertotal=[]
            listasupertotal={'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante}
            thetotalpagado=0
            thetotalsaldoneto=0
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0
            for itemquien in listadequien:
                queryheads = DBSession.query(Head).filter_by(id_operador=itemquien['id']).filter_by(id_unidad=None).filter(Head.fecha<kw['fechainicial']).all()
                totalparafaltante=0
                for itemhead in queryheads:
                    totalparafaltante=totalparafaltante+itemhead.faltante

                saldoneto=itemquien['totalpagado']-totalparafaltante
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                allrecords.append(str(itemquien['nombre'])+'\t'+str(itemquien['numerooperador'])+'\t'+str(itemquien['pagos'])+'\t'+str(totalparafaltante)+'\t'
                                  +str(itemquien['totalpagado'])+'\t'+str(saldoneto)+'\t')

                supertotalbody=0
                for itemdebody in listaallrecords:
                    eltotal=0
                    for itemotrobody in listabodies:
                        if itemdebody==itemotrobody['cargo'] and itemquien['id']==itemotrobody['idoperador']:
                            eltotal=eltotal+itemotrobody['pagado']
                    querycargoverificar=DBSession.query(CargoOperador).filter_by(application_id=application_id).filter_by(cargo=itemdebody).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo==1 or querycargoverificar.tipodecargo==3:
                            supertotalbody=supertotalbody+eltotal
                    allrecords.append(str(eltotal)+'\t')
                    mibandera = 0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo') == itemdebody:
                            mibandera = 1
                            break

                    if mibandera == 1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo'] == itemdebody:
                            itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal


                #allrecords.append('VALE'+'\t')
                allrecords.append(str(supertotalbody)+'\t')
                importeapagar=saldoneto-supertotalbody
                thetotalsupertotalboy = thetotalsupertotalboy+supertotalbody
                thetotalimporteapagar = thetotalimporteapagar + importeapagar
                thetotaltotalapagar = thetotaltotalapagar + itemquien['totalporpagar']
                thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
                allrecords.append(str(importeapagar)+'\t'+str(itemquien['totalporpagar'])+'\t'+str(itemquien['totalfaltante'])+'\t'+itemquien['empresa']+'\t')


                allrecords.append('\n')
                lastindex=len(listadequien)
                if listadequien[lastindex - 1] == itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total']) + '\t')
                    #allrecords.append('TOTALVALE' + '\t')
                    allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                        thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                    thetotalpagado = 0
                    thetotalsaldoneto = 0
                    listathetotalescargos = []
                    thetotalsupertotalboy = 0
                    thetotalimporteapagar = 0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')




            file_namecsv = ExportCSV.create(allrecords, "OPERADOR"+operador+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])


            print("listasupertotal========>", listasupertotal)
            file_name = ExportPDF.create({'por': 'OPERADOR','quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                                          'fechainicial': kw['fechainicial'], 'fechafinal':
                                              kw['fechafinal'], 'totalporpagar': totalporpagar,
                                          'totalpagado': totalpagado, 'totalfaltante': totalfaltante,'listasupertotal':listasupertotal},
                                         operador+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
                                         'prueba')
            template=render_template({'por': 'OPERADOR','quien': listadequien,'queryheads': listadeheads,'listabodies': listabodies,'fechainicial': kw['fechainicial'],'fechafinal':
                                      kw['fechafinal'],'totalporpagar': totalporpagar,'totalpagado': totalpagado,'totalfaltante': totalfaltante
                                      ,'listasupertotal':listasupertotal},'mako','pythonjupiter.templates.BillingStatement.htmlpdf')



        elif kw['por']=='Empresa':
            empresa=kw['otro']
            thequeryempresa=DBSession.query(Linea).filter_by(nombre_linea=empresa).filter_by(app_id=application_id).first()
            if thequeryempresa is not None:
                if kw['app_name']!='TODO':
                    queryunidad=DBSession.query(DatosUnidad).filter_by(id_linea=thequeryempresa.id_linea).all()
                else:
                    queryunidad = DBSession.query(DatosUnidad).filter_by(id_linea=thequeryempresa.id_linea).all()

            else:
                queryunidad={}
            listadeheads = []
            listadequien = []
            listacuantospagos = []
            listathetotalescargos=[]
            # listadebodies=[]
            allrecords.append("PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append("NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
            supertotalporpagar = 0
            supertotalpagado = 0
            supertotalfaltante = 0
            for itemunidad in queryunidad:
                totalporpagar = 0
                totalpagado = 0
                totalfaltante = 0
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter(
                    Head.fecha >= kw['fechainicial']).filter(Head.fecha <= kw['fechafinal']).all()
                querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad.id_unidad).filter(
                    AbonoCuenta.fechacuenta >= kw['fechainicial']).filter(
                    AbonoCuenta.fechacuenta <= kw['fechafinal']).all()
                querypersona = DBSession.query(Persona).filter_by(id_persona=itemunidad.id_persona).filter_by(
                    espermisionario=1).first()
                queryempresa = DBSession.query(Linea).filter_by(id_linea=itemunidad.id_linea).first()
                listadeheads.append(queryheads)
                for itemhead in queryheads:
                    querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(
                        fecha=itemhead.fecha).all()
                    # listadebodies.append(querybody)
                    for itembody in querybody:
                        querycargounidad = DBSession.query(CargoUnidad).filter_by(
                            id_cargounidad=itembody.id_cargounidad).first()
                        if querycargounidad is not None:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': querycargounidad.cargo, 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': querycargounidad.tipodecargo})
                            if querycargounidad.cargo in listaallrecords:
                                pass
                            else:
                                listaallrecords.append(querycargounidad.cargo)
                                allrecords.append(querycargounidad.cargo + '\t')
                        else:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': 1})
                            if 'POLIZA' in listaallrecords:
                                pass
                            else:
                                listaallrecords.append('POLIZA')
                                allrecords.append('POLIZA' + '\t')

                    totalporpagar = totalporpagar + itemhead.porpagar
                    totalpagado = totalpagado + itemhead.pagado
                    totalfaltante = totalfaltante + itemhead.faltante

                    supertotalporpagar = supertotalporpagar + itemhead.porpagar
                    supertotalpagado = supertotalpagado + itemhead.pagado
                    supertotalfaltante = supertotalfaltante + itemhead.faltante

                listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                     'persona': querypersona.nombre, 'totalporpagar': totalporpagar,
                                     'totalpagado': totalpagado, 'totalfaltante': totalfaltante,
                                     'empresa': queryempresa.nombre_linea})
            cuantas = len(listadequien)
            allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')

            listasupertotal=[]
            listasupertotal={'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante}
            thetotalpagado=0
            thetotalsaldoneto=0
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0
            for itemquien in listadequien:
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(
                    Head.fecha < kw['fechainicial']).all()
                totalparafaltante = 0
                for itemhead in queryheads:
                    totalparafaltante = totalparafaltante + itemhead.faltante

                saldoneto = itemquien['totalpagado'] - totalparafaltante
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                allrecords.append(
                    itemquien['persona'] + '\t' + str(itemquien['eco']) + '\t' + str(itemquien['pagos']) + '\t' + str(
                        totalparafaltante) + '\t'
                    + str(itemquien['totalpagado']) + '\t' + str(saldoneto) + '\t')

                supertotalbody = 0
                for itemdebody in listaallrecords:
                    eltotal = 0
                    for itemotrobody in listabodies:
                        if itemdebody == itemotrobody['cargo'] and itemquien['id'] == itemotrobody['idunidad']:
                            eltotal = eltotal + itemotrobody['pagado']
                    querycargoverificar = DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).filter_by(application_id=application_id).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo == 1 or querycargoverificar.tipodecargo == 3:
                            supertotalbody = supertotalbody + eltotal
                    elif itemdebody == 'POLIZA':
                        supertotalbody = supertotalbody + eltotal
                    # print("EL TOTAL de ",itemdebody," ES: ",eltotal)
                    allrecords.append(str(eltotal) + '\t')
                    mibandera = 0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo') == itemdebody:
                            mibandera = 1
                            break

                    if mibandera == 1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo'] == itemdebody:
                            itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal

                # print("EL SUPERTOTAL:::>",str(supertotalbody))


                #allrecords.append('VALE' + '\t')
                allrecords.append(str(supertotalbody) + '\t')
                importeapagar = saldoneto - supertotalbody
                thetotalsupertotalboy = thetotalsupertotalboy + supertotalbody
                thetotalimporteapagar = thetotalimporteapagar + importeapagar
                thetotaltotalapagar = thetotaltotalapagar + itemquien['totalporpagar']
                thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
                allrecords.append(str(importeapagar) + '\t' + str(itemquien['totalporpagar']) + '\t' + str(
                    itemquien['totalfaltante']) + '\t' + itemquien['empresa'] + '\t')
                if importeapagar <= 0:
                    allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

                allrecords.append('\n')
                lastindex=len(listadequien)
                if listadequien[lastindex - 1] == itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total']) + '\t')
                    #allrecords.append('TOTALVALE' + '\t')
                    allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                        thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                    thetotalpagado = 0
                    thetotalsaldoneto = 0
                    listathetotalescargos = []
                    thetotalsupertotalboy = 0
                    thetotalimporteapagar = 0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')


            file_namecsv = ExportCSV.create(allrecords, "EMPRESA"+empresa+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])


            print("listasupertotal========>", listasupertotal)
            file_name = ExportPDF.create({'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                                'fechainicial': kw['fechainicial'], 'fechafinal':kw['fechafinal'], 'totalporpagar': totalporpagar,
                                          'totalpagado': totalpagado, 'totalfaltante': totalfaltante, 'cuantas': cuantas,
                                    'listacuantospagos': listacuantospagos,'listasupertotal': listasupertotal},empresa+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf','prueba')

            template = render_template({'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,'fechainicial': kw['fechainicial'], 'fechafinal':
                     kw['fechafinal'], 'totalporpagar': totalporpagar, 'totalpagado': totalpagado,
                 'totalfaltante': totalfaltante,
                 'cuantas': cuantas, 'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal}, 'mako',
                'pythonjupiter.templates.BillingStatement.htmlpdf')

        elif kw['por']=='Derrotero':
            derrotero=kw['otro']
            thequeryderrotero = DBSession.query(Derrotero).filter_by(derrotero=derrotero).filter_by(application_id=application_id).first()
            if thequeryderrotero is not None:
                if kw['app_name']!='TODO':
                    queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=application_id).filter_by(id_derrotero=thequeryderrotero.id_derrotero).all()
                else:
                    queryunidad = DBSession.query(DatosUnidad).filter_by(id_derrotero=thequeryderrotero.id_derrotero).all()

            else:
                queryunidad = {}
            listadeheads = []
            listadequien = []
            listacuantospagos = []
            listathetotalescargos=[]
            # listadebodies=[]
            allrecords.append("PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append(
                "NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
            supertotalporpagar = 0
            supertotalpagado = 0
            supertotalfaltante = 0
            for itemunidad in queryunidad:
                totalporpagar = 0
                totalpagado = 0
                totalfaltante = 0
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter(
                    Head.fecha >= kw['fechainicial']).filter(Head.fecha <= kw['fechafinal']).all()
                querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad.id_unidad).filter(
                    AbonoCuenta.fechacuenta >= kw['fechainicial']).filter(
                    AbonoCuenta.fechacuenta <= kw['fechafinal']).all()
                querypersona = DBSession.query(Persona).filter_by(id_persona=itemunidad.id_persona).filter_by(
                    espermisionario=1).first()
                queryempresa = DBSession.query(Linea).filter_by(id_linea=itemunidad.id_linea).first()
                listadeheads.append(queryheads)
                for itemhead in queryheads:
                    querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(
                        fecha=itemhead.fecha).all()
                    # listadebodies.append(querybody)
                    for itembody in querybody:
                        querycargounidad = DBSession.query(CargoUnidad).filter_by(
                            id_cargounidad=itembody.id_cargounidad).first()
                        if querycargounidad is not None:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': querycargounidad.cargo, 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': querycargounidad.tipodecargo})
                            if querycargounidad.cargo in listaallrecords:
                                pass
                            else:
                                listaallrecords.append(querycargounidad.cargo)
                                allrecords.append(querycargounidad.cargo + '\t')
                        else:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': 1})
                            if 'POLIZA' in listaallrecords:
                                pass
                            else:
                                listaallrecords.append('POLIZA')
                                allrecords.append('POLIZA' + '\t')

                    totalporpagar = totalporpagar + itemhead.porpagar
                    totalpagado = totalpagado + itemhead.pagado
                    totalfaltante = totalfaltante + itemhead.faltante

                    supertotalporpagar = supertotalporpagar + itemhead.porpagar
                    supertotalpagado = supertotalpagado + itemhead.pagado
                    supertotalfaltante = supertotalfaltante + itemhead.faltante

                listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                     'persona': querypersona.nombre, 'totalporpagar': totalporpagar,
                                     'totalpagado': totalpagado, 'totalfaltante': totalfaltante,
                                     'empresa': queryempresa.nombre_linea})
            cuantas = len(listadequien)
            allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')

            listasupertotal=[]
            listasupertotal={'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante}
            thetotalpagado=0
            thetotalsaldoneto=0
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0
            for itemquien in listadequien:
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(
                    Head.fecha < kw['fechainicial']).all()
                totalparafaltante = 0
                for itemhead in queryheads:
                    totalparafaltante = totalparafaltante + itemhead.faltante

                saldoneto = itemquien['totalpagado'] - totalparafaltante
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                allrecords.append(
                    itemquien['persona'] + '\t' + str(itemquien['eco']) + '\t' + str(itemquien['pagos']) + '\t' + str(
                        totalparafaltante) + '\t'
                    + str(itemquien['totalpagado']) + '\t' + str(saldoneto) + '\t')

                supertotalbody = 0
                for itemdebody in listaallrecords:
                    eltotal = 0
                    for itemotrobody in listabodies:
                        if itemdebody == itemotrobody['cargo'] and itemquien['id'] == itemotrobody['idunidad']:
                            eltotal = eltotal + itemotrobody['pagado']
                    querycargoverificar = DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).filter_by(application_id=application_id).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo == 1 or querycargoverificar.tipodecargo == 3:
                            supertotalbody = supertotalbody + eltotal
                    elif itemdebody == 'POLIZA':
                        supertotalbody = supertotalbody + eltotal
                    # print("EL TOTAL de ",itemdebody," ES: ",eltotal)
                    allrecords.append(str(eltotal) + '\t')
                    mibandera = 0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo') == itemdebody:
                            mibandera = 1
                            break

                    if mibandera == 1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo'] == itemdebody:
                            itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal

                # print("EL SUPERTOTAL:::>",str(supertotalbody))

                #allrecords.append('VALE' + '\t')

                allrecords.append(str(supertotalbody) + '\t')
                importeapagar = saldoneto - supertotalbody
                thetotalsupertotalboy = thetotalsupertotalboy + supertotalbody
                thetotalimporteapagar = thetotalimporteapagar + importeapagar
                thetotaltotalapagar = thetotaltotalapagar + itemquien['totalporpagar']
                thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
                allrecords.append(str(importeapagar) + '\t' + str(itemquien['totalporpagar']) + '\t' + str(
                    itemquien['totalfaltante']) + '\t' + itemquien['empresa'] + '\t')
                if importeapagar <= 0:
                    allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

                allrecords.append('\n')
                lastindex=len(listadequien)
                if listadequien[lastindex - 1] == itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total']) + '\t')
                    #allrecords.append('TOTALVALE' + '\t')
                    allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                        thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                    thetotalpagado = 0
                    thetotalsaldoneto = 0
                    listathetotalescargos = []
                    thetotalsupertotalboy = 0
                    thetotalimporteapagar = 0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')


            file_namecsv = ExportCSV.create(allrecords, "DERROTERO"+derrotero+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])

            print("listasupertotal========>", listasupertotal)
            file_name = ExportPDF.create(
                {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal': kw['fechafinal'], 'totalporpagar': totalporpagar,
                 'totalpagado': totalpagado, 'totalfaltante': totalfaltante, 'cuantas': cuantas,
                 'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal}, derrotero+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
                'prueba')

            template = render_template(
                {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal':
                     kw['fechafinal'], 'totalporpagar': totalporpagar, 'totalpagado': totalpagado,
                 'totalfaltante': totalfaltante,
                 'cuantas': cuantas, 'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal}, 'mako',
                'pythonjupiter.templates.BillingStatement.htmlpdf')

        elif kw['por']=='Grupo':
            grupo=kw['otro']
            thequerygrupo = DBSession.query(Grupo).filter_by(grupo=grupo).filter_by(application_id=application_id).first()
            if thequerygrupo is not None:
                thequerygrupounidad=DBSession.query(Grupo_DatosUnidad).filter_by(grupo_id=thequerygrupo.id_grupo).all()
            else:
                thequerygrupounidad={}
            listadeheads = []
            listadequien = []
            listacuantospagos = []
            listathetotalescargos=[]
            # listadebodies=[]
            allrecords.append("PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append("NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
            supertotalporpagar = 0
            supertotalpagado = 0
            supertotalfaltante = 0
            for itemgrupounidad in thequerygrupounidad:
                if kw['app_name']!='TODO':
                    itemunidad = DBSession.query(DatosUnidad).filter_by(app_id=application_id).filter_by(id_unidad=itemgrupounidad.datosunidad_id).first()
                else:
                    itemunidad = DBSession.query(DatosUnidad).filter_by(id_unidad=itemgrupounidad.datosunidad_id).first()

                if itemunidad is not None:
                    itemunidad2=itemunidad.id_unidad
                    itemunidadidpersona=itemunidad.id_persona
                    itemunidadidlinea=itemunidad.id_linea
                else:
                    itemunidad2=''
                    itemunidadidpersona=''
                    itemunidadidlinea=''

                totalporpagar = 0
                totalpagado = 0
                totalfaltante = 0
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemunidad2).filter(
                    Head.fecha >= kw['fechainicial']).filter(Head.fecha <= kw['fechafinal']).all()
                querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad2).filter(
                    AbonoCuenta.fechacuenta >= kw['fechainicial']).filter(
                    AbonoCuenta.fechacuenta <= kw['fechafinal']).all()
                querypersona = DBSession.query(Persona).filter_by(id_persona=itemunidadidpersona).filter_by(
                    espermisionario=1).first()
                queryempresa = DBSession.query(Linea).filter_by(id_linea=itemunidadidlinea).first()
                listadeheads.append(queryheads)
                for itemhead in queryheads:
                    querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad2).filter_by(
                        fecha=itemhead.fecha).all()
                    # listadebodies.append(querybody)
                    for itembody in querybody:
                        querycargounidad = DBSession.query(CargoUnidad).filter_by(
                            id_cargounidad=itembody.id_cargounidad).first()
                        if querycargounidad is not None:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': querycargounidad.cargo, 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': querycargounidad.tipodecargo})
                            if querycargounidad.cargo in listaallrecords:
                                pass
                            else:
                                listaallrecords.append(querycargounidad.cargo)
                                allrecords.append(querycargounidad.cargo + '\t')
                        else:
                            listabodies.append(
                                {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                                 'condonacion': itembody.condonacion,
                                 'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                 'tipodecargo': 1})
                            if 'POLIZA' in listaallrecords:
                                pass
                            else:
                                listaallrecords.append('POLIZA')
                                allrecords.append('POLIZA' + '\t')

                    totalporpagar = totalporpagar + itemhead.porpagar
                    totalpagado = totalpagado + itemhead.pagado
                    totalfaltante = totalfaltante + itemhead.faltante

                    supertotalporpagar = supertotalporpagar + itemhead.porpagar
                    supertotalpagado = supertotalpagado + itemhead.pagado
                    supertotalfaltante = supertotalfaltante + itemhead.faltante

                if querypersona is not None:
                    querypersonanombre=querypersona.nombre
                else:
                    querypersonanombre=''

                if queryempresa is not None:
                    queryempresanombre_linea=queryempresa.nombre_linea
                else:
                    queryempresanombre_linea=''

                listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                     'persona': querypersonanombre, 'totalporpagar': totalporpagar,
                                     'totalpagado': totalpagado, 'totalfaltante': totalfaltante,
                                     'empresa': queryempresanombre_linea})
            cuantas = len(listadequien)
            listasupertotal=[]
            listasupertotal={'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante}
            allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')

            thetotalpagado=0
            thetotalsaldoneto=0
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0


            for itemquien in listadequien:
                queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(
                    Head.fecha < kw['fechainicial']).all()
                totalparafaltante = 0
                for itemhead in queryheads:
                    totalparafaltante = totalparafaltante + itemhead.faltante

                saldoneto = itemquien['totalpagado'] - totalparafaltante
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                allrecords.append(
                    itemquien['persona'] + '\t' + str(itemquien['eco']) + '\t' + str(itemquien['pagos']) + '\t' + str(
                        totalparafaltante) + '\t'
                    + str(itemquien['totalpagado']) + '\t' + str(saldoneto) + '\t')

                supertotalbody = 0
                for itemdebody in listaallrecords:
                    eltotal = 0
                    for itemotrobody in listabodies:
                        if itemdebody == itemotrobody['cargo'] and itemquien['id'] == itemotrobody['idunidad']:
                            eltotal = eltotal + itemotrobody['pagado']
                    querycargoverificar = DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).filter_by(application_id=application_id).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo == 1 or querycargoverificar.tipodecargo == 3:
                            supertotalbody = supertotalbody + eltotal
                    elif itemdebody == 'POLIZA':
                        supertotalbody = supertotalbody + eltotal
                    # print("EL TOTAL de ",itemdebody," ES: ",eltotal)
                    allrecords.append(str(eltotal) + '\t')
                    mibandera = 0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo') == itemdebody:
                            mibandera = 1
                            break

                    if mibandera == 1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo'] == itemdebody:
                            itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal

                # print("EL SUPERTOTAL:::>",str(supertotalbody))

                #allrecords.append('VALE' + '\t')

                allrecords.append(str(supertotalbody) + '\t')
                importeapagar = saldoneto - supertotalbody
                thetotalsupertotalboy = thetotalsupertotalboy + supertotalbody
                thetotalimporteapagar = thetotalimporteapagar + importeapagar
                thetotaltotalapagar = thetotaltotalapagar + itemquien['totalporpagar']
                thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
                allrecords.append(str(importeapagar) + '\t' + str(itemquien['totalporpagar']) + '\t' + str(
                    itemquien['totalfaltante']) + '\t' + itemquien['empresa'] + '\t')
                if importeapagar <= 0:
                    allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

                allrecords.append('\n')

                lastindex=len(listadequien)
                if listadequien[lastindex - 1] == itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total']) + '\t')
                    #allrecords.append('TOTALVALE' + '\t')
                    allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                        thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                    thetotalpagado = 0
                    thetotalsaldoneto = 0
                    listathetotalescargos = []
                    thetotalsupertotalboy = 0
                    thetotalimporteapagar = 0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')


            file_namecsv = ExportCSV.create(allrecords, "GRUPO"+grupo+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])

            print("listasupertotal========>",listasupertotal)

            file_name = ExportPDF.create(
                {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal': kw['fechafinal'], 'totalporpagar': totalporpagar,
                 'totalpagado': totalpagado, 'totalfaltante': totalfaltante, 'cuantas': cuantas,
                 'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal}, grupo+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
                'prueba')

            template = render_template(
                {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal':
                     kw['fechafinal'], 'totalporpagar': totalporpagar, 'totalpagado': totalpagado,
                 'totalfaltante': totalfaltante,
                 'cuantas': cuantas, 'listacuantospagos': listacuantospagos,'listasupertotal':listasupertotal}, 'mako',
                'pythonjupiter.templates.BillingStatement.htmlpdf')

        elif kw['por']=='Permisionario':
            permisionario=kw['permisionario']
            if permisionario != 'TODOS':
                thequerypermisionario = DBSession.query(Persona).filter_by(nombre=permisionario).filter_by(application_id=application_id).all()
            else:
                thequerypermisionario = DBSession.query(Persona).filter_by(espermisionario=1).filter_by(application_id=application_id).all()
            listadeheads = []
            listadequien = []
            listacuantospagos = []
            listadepermisionario=[]
            # listadebodies=[]
            allrecords.append("PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" +'\t'+ kw['fechafinal'] + '\n')
            allrecords.append('\n')
            allrecords.append("NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')

            for itempermii in thequerypermisionario:
                cuantasunidadesson=0
                queryunidad = DBSession.query(DatosUnidad).filter_by(id_persona=itempermii.id_persona).all()
                supertotalpagado=0
                supertotalporpagar=0
                supertotalfaltante=0
                for itemunidad in queryunidad:
                    totalporpagar = 0
                    totalpagado = 0
                    totalfaltante = 0
                    queryheads = DBSession.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter(
                        Head.fecha >= kw['fechainicial']).filter(Head.fecha <= kw['fechafinal']).all()
                    querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad.id_unidad).filter(
                        AbonoCuenta.fechacuenta >= kw['fechainicial']).filter(
                        AbonoCuenta.fechacuenta <= kw['fechafinal']).all()
                    querypersona = DBSession.query(Persona).filter_by(id_persona=itemunidad.id_persona).filter_by(
                        espermisionario=1).first()
                    queryempresa = DBSession.query(Linea).filter_by(id_linea=itemunidad.id_linea).first()
                    listadeheads.append(queryheads)
                    for itemhead in queryheads:
                        querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(
                            fecha=itemhead.fecha).all()
                        # listadebodies.append(querybody)
                        for itembody in querybody:
                            querycargounidad = DBSession.query(CargoUnidad).filter_by(
                                id_cargounidad=itembody.id_cargounidad).first()
                            if querycargounidad is not None:
                                listabodies.append(
                                    {'fecha': itembody.fecha, 'cargo': querycargounidad.cargo, 'monto': itembody.monto,
                                     'condonacion': itembody.condonacion,
                                     'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                     'tipodecargo': querycargounidad.tipodecargo})
                                if querycargounidad.cargo in listaallrecords:
                                    pass
                                else:
                                    listaallrecords.append(querycargounidad.cargo)
                                    allrecords.append(querycargounidad.cargo + '\t')
                            else:
                                listabodies.append(
                                    {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                                     'condonacion': itembody.condonacion,
                                     'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                                     'tipodecargo': 1})
                                if 'POLIZA' in listaallrecords:
                                    pass
                                else:
                                    listaallrecords.append('POLIZA')
                                    allrecords.append('POLIZA' + '\t')

                        totalporpagar = totalporpagar + itemhead.porpagar
                        totalpagado = totalpagado + itemhead.pagado
                        totalfaltante = totalfaltante + itemhead.faltante

                        supertotalporpagar=supertotalporpagar+itemhead.porpagar
                        supertotalpagado=supertotalpagado+itemhead.pagado
                        supertotalfaltante=supertotalfaltante+itemhead.faltante

                    listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                         'persona': querypersona.nombre, 'totalporpagar': totalporpagar,
                                         'totalpagado': totalpagado, 'totalfaltante': totalfaltante,
                                         'empresa': queryempresa.nombre_linea,'idpermi': itempermii.id_persona})
                    cuantasunidadesson=cuantasunidadesson+1

                listadepermisionario.append({'supertotalporpagar': supertotalporpagar,'supertotalpagado': supertotalpagado,
                                         'supertotalfaltante': supertotalfaltante,'idpermi': itempermii.id_persona,'nombre': itempermii.nombre,
                                             'cuantasunidadesson': cuantasunidadesson})

            cuantas = len(listadequien)
            allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')


            #for itemdepermisionario in listadepermisionario
            depermi=0
            auxi=0
            lastindex=len(listadequien)
            #thetotalparafaltante=0
            thetotalsaldoneto=0
            thetotalpagado=0
            listathetotalescargos=[]
            thetotalsupertotalboy=0
            thetotalimporteapagar=0
            thetotaltotalapagar=0
            thetotaltotalfaltante=0
            for itemquien in listadequien:
                depermi=itemquien['idpermi']
                if auxi==0:
                    auxdepermi=itemquien['idpermi']
                    auxi=1

                if auxdepermi != depermi:
                    for itemlistapermi in listadepermisionario:
                        if itemlistapermi['idpermi'] == itemquien['idpermi']:
                            allrecords.append("\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                            for itemeste in listathetotalescargos:
                                allrecords.append(str(itemeste['total']) + '\t')
                            #allrecords.append('TOTALVALE' + '\t')
                            allrecords.append(
                                str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                                    thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                            thetotalpagado = 0
                            thetotalsaldoneto = 0
                            listathetotalescargos = []
                            thetotalsupertotalboy = 0
                            thetotalimporteapagar = 0
                            thetotaltotalapagar = 0
                            thetotaltotalfaltante = 0
                            allrecords.append('\n')
                            allrecords.append('\n')
                            auxdepermi=itemquien['idpermi']

                queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(Head.fecha < kw['fechainicial']).all()
                totalparafaltante = 0
                for itemhead in queryheads:
                    totalparafaltante = totalparafaltante + itemhead.faltante
                    #thetotalparafaltante=thetotalparafaltante+itemhead.faltante

                saldoneto = itemquien['totalpagado'] - totalparafaltante
                thetotalsaldoneto=thetotalsaldoneto+saldoneto
                thetotalpagado=thetotalpagado+itemquien['totalpagado']
                allrecords.append(
                    itemquien['persona'] + '\t' + str(itemquien['eco']) + '\t' + str(itemquien['pagos']) + '\t' + str(
                        totalparafaltante) + '\t'
                    + str(itemquien['totalpagado']) + '\t' + str(saldoneto) + '\t')

                supertotalbody = 0
                for itemdebody in listaallrecords:
                    eltotal = 0
                    #thetotal=0
                    for itemotrobody in listabodies:
                        if itemdebody == itemotrobody['cargo'] and itemquien['id'] == itemotrobody['idunidad']:
                            eltotal = eltotal + itemotrobody['pagado']
                    querycargoverificar = DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).filter_by(application_id=application_id).first()
                    if querycargoverificar is not None:
                        if querycargoverificar.tipodecargo == 1 or querycargoverificar.tipodecargo == 3:
                            supertotalbody = supertotalbody + eltotal
                    elif itemdebody == 'POLIZA':
                        supertotalbody = supertotalbody + eltotal
                    # print("EL TOTAL de ",itemdebody," ES: ",eltotal)

                    allrecords.append(str(eltotal) + '\t')
                    #thetotaleltotal=thetotaleltotal+eltotal
                    mibandera=0
                    for itempasando in listathetotalescargos:
                        if itempasando.get('cargo')==itemdebody:
                            mibandera=1
                            break

                    if mibandera==1:
                        pass
                    else:
                        listathetotalescargos.append({'cargo': itemdebody,'total': 0})
                    for itemlistathetotalescargos in listathetotalescargos:
                        if itemlistathetotalescargos['cargo']==itemdebody:
                            itemlistathetotalescargos['total']=itemlistathetotalescargos['total']+eltotal


                # print("EL SUPERTOTAL:::>",str(supertotalbody))

                #allrecords.append('VALE' + '\t')
                allrecords.append(str(supertotalbody) + '\t')
                thetotalsupertotalboy=thetotalsupertotalboy+supertotalbody
                importeapagar = saldoneto - supertotalbody
                thetotalimporteapagar=thetotalimporteapagar+importeapagar
                thetotaltotalapagar=thetotaltotalapagar + itemquien['totalporpagar']
                thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
                allrecords.append(str(importeapagar) + '\t' + str(itemquien['totalporpagar']) + '\t' + str(
                    itemquien['totalfaltante']) + '\t' + itemquien['empresa'] + '\t')
                if importeapagar <= 0:
                    allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

                allrecords.append('\n')

                if listadequien[lastindex-1]==itemquien:
                    print("EL ULTIMO ELEMENTO")
                    allrecords.append("\t"+'\t'+'\t'+'\t'+str(thetotalpagado)+'\t'+str(thetotalsaldoneto)+'\t')
                    for itemeste in listathetotalescargos:
                        allrecords.append(str(itemeste['total'])+'\t')
                    #allrecords.append('TOTALVALE'+'\t')
                    allrecords.append(str(thetotalsupertotalboy)+'\t'+str(thetotalimporteapagar)+'\t'+str(thetotaltotalapagar)+'\t'+str(thetotaltotalfaltante)+'\t')
                    thetotalpagado=0
                    thetotalsaldoneto=0
                    listathetotalescargos=[]
                    thetotalsupertotalboy=0
                    thetotalimporteapagar=0
                    thetotaltotalapagar = 0
                    thetotaltotalfaltante = 0
                    allrecords.append('\n')
                    allrecords.append('\n')

            file_namecsv = ExportCSV.create(allrecords, "PERMISIONARIO"+permisionario+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])

            file_name = ExportPDF.create(
                {'por': 'PERMI', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal': kw['fechafinal'], 'totalporpagar': totalporpagar,
                 'totalpagado': totalpagado, 'totalfaltante': totalfaltante, 'cuantas': cuantas,
                 'listacuantospagos': listacuantospagos,'listadepermisionario':listadepermisionario}, permisionario+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
                'prueba')

            template = render_template(
                {'por': 'PERMI', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
                 'fechainicial': kw['fechainicial'], 'fechafinal':
                     kw['fechafinal'], 'totalporpagar': totalporpagar, 'totalpagado': totalpagado,
                 'totalfaltante': totalfaltante,
                 'cuantas': cuantas, 'listacuantospagos': listacuantospagos,'listadepermisionario': listadepermisionario}, 'mako',
                'pythonjupiter.templates.BillingStatement.htmlpdf')

        elif kw['por']=='Periodo':
            #print(" PERIODOO ")
            queryunidades=DBSession.query(DatosUnidad).filter_by(application_id=application_id).all()
            mes1=kw['mes1']
            mes2=kw['mes2']

            lafechainicial=mes1.split("-")
            lafechafinal=mes2.split("-")

            rango1=calendar.monthrange(int(lafechainicial[0]), int(lafechainicial[1]))[0]

            rango2=calendar.monthrange(int(lafechafinal[0]),int(lafechafinal[1]))[1]
            fechaini=mes1+'-01'
            fechafin=mes2+'-'+str(rango2)

            formatofechaini=datetime.strptime(fechaini, '%Y-%m-%d').date()
            formatofechafin=datetime.strptime(fechafin, '%Y-%m-%d').date()

            if formatofechaini>formatofechafin:
                print("FECHA INI MAYOR")
            else:
                porpagar = 0
                pagadototal = 0
                falta = 0
                auxfecha=formatofechaini
                while auxfecha<=formatofechafin:
                    auxfecha2 = auxfecha + timedelta(14)
                    if auxfecha.day==16:
                        rangodefin = calendar.monthrange(auxfecha.year, auxfecha.month)[1]
                        auxfechaaux=str(auxfecha.year)+'-'+str(auxfecha.month)+'-'+str(rangodefin)
                        auxfecha2=datetime.strptime(auxfechaaux, '%Y-%m-%d').date()

                    print("auxfecha==>", auxfecha)
                    print("auxfecha2==>", auxfecha2)
                    auxfecha=auxfecha2+timedelta(1)

                #queryhead = DBSession.query(Head).filter(Head.id_unidad!=None).filter(Head.fecha >= formatofechaini).filter(Head.fecha <= formatofechafin).all()
                #for itemhead in queryhead:
                #    pagadototal = pagadototal + itemhead.pagado
                #    porpagar = porpagar + itemhead.porpagar
                #    falta = falta + itemhead.faltante
                #print("POR PAGAR: ", porpagar)
                #print("PAGADO: ", pagadototal)
                #print("FALTA: ", falta)

            template=''
            file_name=''
            file_namecsv=''
        else:
            return dict(error='no')


        return dict(template=template,error='ok',file_name=file_name,file_namecsv=file_namecsv)


    @expose('json')
    def billingstatementpersonal(self,**kw):
        listabodies=[]
        totalporpagar=0
        totalpagado=0
        totalfaltante=0
        unidad = kw['unidad']
        listadeheads = []
        listadequien = []
        listacuantospagos = []
        listathetotalescargos = []
        listasupertotal = {}
        allrecords=[]
        listaallrecords=[]
        internal_id=kw['internal_id']
        # listadebodies=[]
        if unidad != 'TODAS':
            queryunidad = DBSession.query(DatosUnidad).filter_by(eco1=unidad).all()
        else:
            querypermisionario = DBSession.query(Persona).filter_by(internal_id=internal_id).filter_by(usuario=kw['user']).first()
            queryunidad = DBSession.query(DatosUnidad).filter_by(id_persona=querypermisionario.id_persona).all()

        allrecords.append(
            "PERIODO DE" + '\t' + kw['fechainicial'] + '\t' + "AL" + '\t' + kw['fechafinal'] + '\n')
        allrecords.append('\n')
        allrecords.append(
            "NOMBRE DEL PERMISIONARIO" + '\t' + "UNIDAD" + '\t' + "DIAS RECAUDADOS" + '\t' + 'SALDO INICIAL' + '\t' + 'IMPORTE INGRESADO' + '\t' + 'SALDO NETO' + '\t')
        supertotalporpagar = 0
        supertotalpagado = 0
        supertotalfaltante = 0
        for itemunidad in queryunidad:
            totalporpagar = 0
            totalpagado = 0
            totalfaltante = 0
            queryheads = DBSession.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter(
                Head.fecha >= kw['fechainicial']).filter(Head.fecha <= kw['fechafinal']).all()
            querypagos = DBSession.query(AbonoCuenta).filter_by(unidad_id=itemunidad.id_unidad).filter(
                AbonoCuenta.fechacuenta >= kw['fechainicial']).filter(AbonoCuenta.fechacuenta <= kw['fechafinal']).all()
            querypersona = DBSession.query(Persona).filter_by(id_persona=itemunidad.id_persona).filter_by(
                espermisionario=1).first()
            queryempresa = DBSession.query(Linea).filter_by(id_linea=itemunidad.id_linea).first()
            listadeheads.append(queryheads)
            for itemhead in queryheads:
                querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(
                    fecha=itemhead.fecha).all()
                # listadebodies.append(querybody)
                for itembody in querybody:
                    querycargounidad = DBSession.query(CargoUnidad).filter_by(
                        id_cargounidad=itembody.id_cargounidad).first()
                    if querycargounidad is not None:
                        listabodies.append(
                            {'fecha': itembody.fecha, 'cargo': querycargounidad.cargo, 'monto': itembody.monto,
                             'condonacion': itembody.condonacion,
                             'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                             'tipodecargo': querycargounidad.tipodecargo})
                        if querycargounidad.cargo in listaallrecords:
                            pass
                        else:
                            listaallrecords.append(querycargounidad.cargo)
                            allrecords.append(querycargounidad.cargo + '\t')
                    else:
                        listabodies.append(
                            {'fecha': itembody.fecha, 'cargo': 'POLIZA', 'monto': itembody.monto,
                             'condonacion': itembody.condonacion,
                             'saldo': itembody.saldo, 'pagado': itembody.pagado, 'idunidad': itemunidad.id_unidad,
                             'tipodecargo': 1})
                        if 'POLIZA' in listaallrecords:
                            pass
                        else:
                            listaallrecords.append('POLIZA')
                            allrecords.append('POLIZA' + '\t')

                totalporpagar = totalporpagar + itemhead.porpagar
                totalpagado = totalpagado + itemhead.pagado
                totalfaltante = totalfaltante + itemhead.faltante

                supertotalporpagar = supertotalporpagar + itemhead.porpagar
                supertotalpagado = supertotalpagado + itemhead.pagado
                supertotalfaltante = supertotalfaltante + itemhead.faltante

            listadequien.append({'eco': itemunidad.eco1, 'id': itemunidad.id_unidad, 'pagos': len(querypagos),
                                 'persona': querypersona.nombre, 'totalporpagar': totalporpagar,
                                 'totalpagado': totalpagado, 'totalfaltante': totalfaltante,
                                 'empresa': queryempresa.nombre_linea})
        cuantas = len(listadequien)
        allrecords.append("TOTAL DESCUENTOS" + '\t' + "IMPORTE A PAGAR" + '\t' + "DEBERIAS PAGAR" + '\t' + "FALTANTE" + '\t' + 'EMPRESA' + '\t' + 'OBSERVACIONES' + '\n')

        listasupertotal = {'supertotalporpagar': supertotalporpagar, 'supertotalpagado': supertotalpagado,
                           'supertotalfaltante': supertotalfaltante}

        thetotalpagado = 0
        thetotalsaldoneto = 0
        thetotalsupertotalboy = 0
        thetotalimporteapagar = 0
        thetotaltotalapagar = 0
        thetotaltotalfaltante = 0
        for itemquien in listadequien:
            queryheads = DBSession.query(Head).filter_by(id_unidad=itemquien['id']).filter(
                Head.fecha < kw['fechainicial']).all()
            totalparafaltante = 0
            for itemhead in queryheads:
                totalparafaltante = totalparafaltante + itemhead.faltante

            saldoneto = itemquien['totalpagado'] - totalparafaltante
            thetotalpagado = thetotalpagado + itemquien['totalpagado']
            thetotalsaldoneto = thetotalsaldoneto + saldoneto
            allrecords.append(
                itemquien['persona'] + '\t' + str(itemquien['eco']) + '\t' + str(itemquien['pagos']) + '\t' + str(
                    totalparafaltante) + '\t'
                + str(itemquien['totalpagado']) + '\t' + str(saldoneto) + '\t')

            supertotalbody = 0
            for itemdebody in listaallrecords:
                eltotal = 0
                for itemotrobody in listabodies:
                    if itemdebody == itemotrobody['cargo'] and itemquien['id'] == itemotrobody['idunidad']:
                        eltotal = eltotal + itemotrobody['pagado']
                querycargoverificar = DBSession.query(CargoUnidad).filter_by(cargo=itemdebody).first()
                if querycargoverificar is not None:
                    if querycargoverificar.tipodecargo == 1 or querycargoverificar.tipodecargo == 3:
                        supertotalbody = supertotalbody + eltotal
                elif itemdebody == 'POLIZA':
                    supertotalbody = supertotalbody + eltotal
                # print("EL TOTAL de ",itemdebody," ES: ",eltotal)
                allrecords.append(str(eltotal) + '\t')
                mibandera = 0
                for itempasando in listathetotalescargos:
                    if itempasando.get('cargo') == itemdebody:
                        mibandera = 1
                        break

                if mibandera == 1:
                    pass
                else:
                    listathetotalescargos.append({'cargo': itemdebody, 'total': 0})
                for itemlistathetotalescargos in listathetotalescargos:
                    if itemlistathetotalescargos['cargo'] == itemdebody:
                        itemlistathetotalescargos['total'] = itemlistathetotalescargos['total'] + eltotal

            # print("EL SUPERTOTAL:::>",str(supertotalbody))

            #allrecords.append('VALE' + '\t')
            thetotalsupertotalboy = thetotalsupertotalboy + supertotalbody
            allrecords.append(str(supertotalbody) + '\t')
            importeapagar = saldoneto - supertotalbody
            thetotalimporteapagar = thetotalimporteapagar + importeapagar
            thetotaltotalapagar = thetotaltotalapagar + itemquien['totalporpagar']
            thetotaltotalfaltante = thetotaltotalfaltante + itemquien['totalfaltante']
            allrecords.append(str(importeapagar) + '\t' + str(itemquien['totalporpagar']) + '\t' + str(
                itemquien['totalfaltante']) + '\t' + itemquien['empresa'] + '\t')
            if importeapagar <= 0:
                allrecords.append("SALDO NEGATIVO-NO HAY PAGO")

            allrecords.append('\n')
            lastindex = len(listadequien)
            if listadequien[lastindex - 1] == itemquien:
                print("EL ULTIMO ELEMENTO")
                allrecords.append(
                    "\t" + '\t' + '\t' + '\t' + str(thetotalpagado) + '\t' + str(thetotalsaldoneto) + '\t')
                for itemeste in listathetotalescargos:
                    allrecords.append(str(itemeste['total']) + '\t')
                #allrecords.append('TOTALVALE' + '\t')
                allrecords.append(str(thetotalsupertotalboy) + '\t' + str(thetotalimporteapagar) + '\t' + str(
                    thetotaltotalapagar) + '\t' + str(thetotaltotalfaltante) + '\t')
                thetotalpagado = 0
                thetotalsaldoneto = 0
                listathetotalescargos = []
                thetotalsupertotalboy = 0
                thetotalimporteapagar = 0
                thetotaltotalapagar = 0
                thetotaltotalfaltante = 0
                allrecords.append('\n')
                allrecords.append('\n')


        file_namecsv = ExportCSV.create(allrecords,"UNIDAD"+unidad+'-'+kw['fechainicial']+'al'+kw['fechafinal'], "/secc/csv", kw['application_id'])

        file_name = ExportPDF.create(
            {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
             'fechainicial': kw['fechainicial'], 'fechafinal':
                 kw['fechafinal'], 'totalporpagar': totalporpagar,
             'totalpagado': totalpagado, 'totalfaltante': totalfaltante, 'cuantas': cuantas,
             'listacuantospagos': listacuantospagos, 'listasupertotal': listasupertotal},
            unidad+'-'+kw['fechainicial']+'al'+kw['fechafinal'], 'pythonjupiter.templates.BillingStatement.htmlpdf',
            'prueba')
        template = render_template(
            {'por': 'UNIDAD', 'quien': listadequien, 'queryheads': listadeheads, 'listabodies': listabodies,
             'fechainicial': kw['fechainicial'], 'fechafinal':
                 kw['fechafinal'], 'totalporpagar': totalporpagar, 'totalpagado': totalpagado,
             'totalfaltante': totalfaltante,
             'cuantas': cuantas, 'listacuantospagos': listacuantospagos, 'listasupertotal': listasupertotal}, 'mako',
            'pythonjupiter.templates.BillingStatement.htmlpdf')

        return dict(template=template, error='ok', file_name=file_name,file_namecsv=file_namecsv)




    @expose('json')
    def thehtmltopdf(self,**kw):


        return dict(error='ok')


    @expose('json')
    def cuentasactualizaselect(self,**kw):
        por=kw['por']
        contenido2=[]
        if por=='Empresa':

            contenido=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()


            for item in contenido:
                contenido2.append({'nombre': item.nombre_linea})
        elif por=='Derrotero':
            if kw['app_name']=='TODOveritodo':
                contenido=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
            else:
                contenido=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()

            for item in contenido:
                contenido2.append({'nombre': item.derrotero})
        elif por=='Grupo':
            if kw['app_name']=='TODOveritodo':
                contenido=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()
            else:
                contenido=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

            for item in contenido:
                contenido2.append({'nombre': item.grupo})
        elif por=='Unidad':
            if kw['app_name']=='TODOveritodo':
                contenido=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_id=kw['application_id']).all()
            else:
                contenido=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_id=kw['application_id']).all()

            for item in contenido:
                contenido2.append({'nombre': item.eco1})
        elif por=='Operador':
            contenido = DBSession.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).filter_by(application_id=kw['application_id']).all()

            for item in contenido:
                contenido2.append({'nombre': item.nombre})

        else:
            domain = app_globals.domainsun+"internal_id="+kw['internal_id']
            appnames = []
            try:
                content = requests.get(domain, auth=("manager", "managepass"))
                content = content.json()
                empresa = content['applications']
            except ValueError:
                empresa = []

            if kw['app_name'] != 'TODO':
                for itemempresa in empresa:
                    if kw['app_name'] == itemempresa['application_name']:
                        contenido2.append({'nombre': itemempresa['application_name']})
            else:
                for itemempresa in empresa:
                    if itemempresa['application_name'] != 'TODO':
                        contenido2.append({'nombre': itemempresa['application_name']})



        return dict(contenido=contenido2)


    @expose('json')
    def traemedog(self,**kw):
        data=[]
        por=kw['por']
        if por=='Derrotero':
            if kw['app_name']=='TODOveritodo':
                query=DBSession.query(Derrotero).filter_by(internal_id=kw['internal_id']).all()
            else:
                query = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()

            for item in query:
                data.append({'value': item.derrotero})
        elif por=='Grupo':
            if kw['app_name']=='TODOveritodo':
                query=DBSession.query(Grupo).filter_by(internal_id=kw['internal_id']).all()
            else:
                query = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

            for item in query:
                data.append({'value': item.grupo})

        elif por=='Empresa':

            query=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()


            for item in query:
                data.append({'value': item.nombre_linea})


        return dict(data=data)



    @expose('json')
    def formsalida(self,**kw):
        querymotivos=DBSession.query(Motivo).filter_by(application_id=kw['application_id']).all()
        kw['querymotivos']=querymotivos
        querybeneficiariomoral=DBSession.query(BeneficiarioMoral).filter_by(application_id=kw['application_id']).all()
        kw['querybeneficiariomoral']=querybeneficiariomoral
        querypersona=DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(esbeneficiario=1).filter_by(habilitadobeneficiario=1).all()
        kw['querypersona']=querypersona
        querycaja=DBSession.query(CuentaCargo).filter_by(application_id=kw['application_id']).all()
        kw['querycaja']=querycaja
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']
        lasempresas=[]
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = []

        for itemempresa in empresa:
            if kw['app_name']=='TODOveritodo':
                if itemempresa['application_name']!='TODO':
                    lasempresas.append({'empresa': itemempresa['application_name']})
            else:
                if itemempresa['application_name'] == kw['app_name']:
                    lasempresas.append({'empresa': itemempresa['application_name']})

        kw['lasempresas']=lasempresas

        template=render_template(kw,'mako','pythonjupiter.templates.Salidas.formsalidas')
        return dict(template=template)

    @expose('json')
    def savesalida(self,**kw):
        motivo=kw['salidademotivo']
        internal_id=kw['internal_id']
        try:
            beneficiariomoral=kw['salidadebeneficiario']
        except:
            beneficiariomoral=None
        app_name=kw['app_nameformsalida']
        application_id=kw['app_id']
        user=kw['user']
        persona=kw['salidadebeneficiariopersona']
        tipo=kw['salidadetipo']
        checks=[]
        for item in kw:
            if "check" in item:
                parachecks=item.split("check")
                checks.append(parachecks[1])

        querymotivo=DBSession.query(Motivo).filter_by(motivo=motivo).filter_by(application_id=application_id).first()
        queryempresaquien=DBSession.query(BeneficiarioMoral).filter_by(beneficiariomoral=beneficiariomoral).filter_by(application_id=application_id).first()
        querypersona = DBSession.query(Persona).filter_by(nombre=persona).filter_by(application_id=application_id).first()
        if tipo == 'Persona':
            for item in kw:
                if "salidademonto" in item:
                    desplit=item.split("salidademonto")
                    if desplit[1] in checks:
                        querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=desplit[1]).filter_by(application_id=application_id).first()
                        newsalida = Salida()
                        newsalida.cancelacion = 0
                        newsalida.tipo = 'Persona'
                        newsalida.monto = kw[item]
                        newsalida.internal_id = internal_id
                        newsalida.app_name = app_name
                        newsalida.application_id=application_id
                        newsalida.id_motivo = querymotivo.id_motivo
                        newsalida.id_persona = querypersona.id_persona
                        try:
                            newsalida.cuentacargo_id = querycuenta.id_cuenta
                        except:
                            newsalida.cuentacargo_id=None
                        newsalida.concepto=kw['conceptodesalida']
                        DBSession.add(newsalida)
        else:
            for item in kw:
                if "salidademonto" in item:
                    desplit=item.split("salidademonto")
                    if desplit[1] in checks:
                        querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=desplit[1]).filter_by(application_id=application_id).first()
                        newsalida = Salida()
                        newsalida.cancelacion = 0
                        newsalida.tipo = 'Empresa u Otro'
                        newsalida.monto = kw[item]
                        newsalida.internal_id = internal_id
                        newsalida.app_name = app_name
                        newsalida.application_id=application_id
                        newsalida.id_motivo = querymotivo.id_motivo
                        newsalida.id_beneficiariomoral = queryempresaquien.id_beneficiariomoral
                        try:
                            newsalida.cuentacargo_id = querycuenta.id_cuenta
                        except:
                            newsalida.cuentacargo_id=None
                        newsalida.concepto = kw['conceptodesalida']
                        DBSession.add(newsalida)



        DBSession.flush()
        return dict(error='ok')



    @expose('json')
    def getmotivosalida(self,**kw):

        querymotivo=DBSession.query(Motivo).filter_by(id_motivo=kw['id']).first()

        try:
            motivo=querymotivo.motivo
        except:
            motivo=''
        return dict(motivo=motivo)

    @expose('json')
    def getbeneficiariosalida(self,**kw):

        querybeneficiario=DBSession.query(BeneficiarioMoral).filter_by(id_beneficiariomoral=kw['id']).first()
        beneficiario=querybeneficiario.beneficiariomoral
        return dict(beneficiario=beneficiario)

    @expose('json')
    def getcuentacargo(self,**kw):
        querycuentacargo=DBSession.query(CuentaCargo).filter_by(id_cuenta=kw['id']).first()
        cuentacargo=querycuentacargo.nombre
        return dict(cuentacargo=cuentacargo)

    @expose('json')
    def getpersonasalida(self,**kw):
        querypersona=DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        try:
            persona=querypersona.nombre
        except:
            persona=''
        return dict(persona=persona)

    @expose('json')
    def cancelapayto(self,**kw):
        querysalida=DBSession.query(Salida).filter_by(id_salida=kw['id']).first()
        if querysalida is not None:
            if querysalida.cancelacion==0:
                querysalida.cancelacion=1


        return dict(error='ok')

    @expose('json')
    def getmaxcuenta(self,**kw):
        cuenta=kw['cuenta']
        #print("CUENTA===>",cuenta)
        querycargooperador=DBSession.query(CargoOperador).filter_by(cargo=cuenta).filter_by(application_id=kw['application_id']).first()
        if querycargooperador is None:
            querycargounidad=DBSession.query(CargoUnidad).filter_by(cargo=cuenta).filter_by(application_id=kw['application_id']).first()
            if querycargounidad is not None:
                querybodyunidad=DBSession.query(Body).filter_by(id_cargounidad=querycargounidad.id_cargounidad).all()
                total=0
                for itembodyunidad in querybodyunidad:
                    queryhead = DBSession.query(Head).filter_by(fecha=itembodyunidad.fecha).filter_by(id_unidad=itembodyunidad.id_unidad).first()
                    if queryhead.app_name == kw['app_name'] or queryhead.app_name=='TODO':

                        total=total+itembodyunidad.pagado

                querycuentacargo=DBSession.query(CuentaCargo).filter_by(nombre=cuenta).filter_by(application_id=kw['application_id']).first()
                querysalidas=DBSession.query(Salida).filter_by(cancelacion=0).filter_by(cuentacargo_id=querycuentacargo.id_cuenta).all()
                totalquita=0
                for itemsalida in querysalidas:
                    if itemsalida.app_name==kw['app_name']:
                        totalquita=totalquita+itemsalida.monto

                total=total-totalquita
                print("El total es==>",total)
                print("Y total quita es==>", totalquita)
                return dict(error='ES DE UNIDAD',total=total)
            else:
                #print("EL MAX EN POLIZA")
                querybodyunidad=DBSession.query(Body).filter_by(espoliza=1).filter_by(application_id=kw['application_id']).all()
                total=0
                for itembodyunidad in querybodyunidad:
                    queryhead = DBSession.query(Head).filter_by(fecha=itembodyunidad.fecha).filter_by(id_unidad=itembodyunidad.id_unidad).first()
                    if queryhead.app_name == kw['app_name']:
                        total=total+itembodyunidad.pagado

                querycuentacargo=DBSession.query(CuentaCargo).filter_by(nombre=cuenta).filter_by(application_id=kw['application_id']).first()
                querysalidas=DBSession.query(Salida).filter_by(cancelacion=0).filter_by(cuentacargo_id=querycuentacargo.id_cuenta).all()
                totalquita=0
                for itemsalida in querysalidas:
                    if itemsalida.app_name==kw['app_name']:
                        totalquita=totalquita+itemsalida.monto

                total=total-totalquita
                print("El total es==>",total)
                print("Y total quita es==>", totalquita)
                return dict(error='ES DE UNIDAD',total=total)
        else:
            querybodyoperador = DBSession.query(Body).filter_by(id_cargopersona=querycargooperador.id_cargooperador).all()

            total = 0
            for itembodyoperador in querybodyoperador:
                    total = total + itembodyoperador.pagado

            querycuentacargo = DBSession.query(CuentaCargo).filter_by(nombre=cuenta).filter_by(application_id=kw['application_id']).first()
            querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(cuentacargo_id=querycuentacargo.id_cuenta).all()
            totalquita = 0
            for itemsalida in querysalidas:
                totalquita = totalquita + itemsalida.monto

            total = total - totalquita
            print("El total es==>", total)
            print("Y total quita es==>", totalquita)
            return dict(error='Es de operador',total=total)
        return dict(error='ok')



    @expose('json')
    def htmlrecibodesalida(self,**kw):
        arreglo = kw['rowObject'].split(",")
        querymotivo=DBSession.query(Motivo).filter_by(id_motivo=arreglo[8]).first()
        if arreglo[3]=='Persona':
            querypersona=DBSession.query(Persona).filter_by(id_persona=arreglo[10]).first()
            benefi=querypersona.nombre
        else:
            querybene=DBSession.query(BeneficiarioMoral).filter_by(id_beneficiariomoral=arreglo[9]).first()
            benefi=querybene.beneficiariomoral
        file_name=ExportPDF.create({'arreglo': arreglo,'motivo': querymotivo.motivo,'bene': benefi},'recibosalida'+str(arreglo[0]),'pythonjupiter.templates.BodyHead.recibodesalida','prueba')
        template=render_template({'arreglo': arreglo,'motivo': querymotivo.motivo,'bene': benefi},'mako','pythonjupiter.templates.BodyHead.recibodesalida')
        return dict(template=template,file_name=file_name)