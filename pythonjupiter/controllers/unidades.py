from tg import expose
from tg import request
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora,Grupo,Persona,CargoUnidad,UnidadCargo
from pythonjupiter.model.tables import Poliza,Grupo_DatosUnidad,CuentaCargo
import os
import requests
from tg import app_globals
from pythonjupiter.lib.base import BaseController
from shutil import copyfileobj,copyfile
from pythonjupiter.lib.helpers import urlsun, url
from datetime import datetime,timedelta, date
from pythonjupiter.lib.messagequeue import Message
from pythonjupiter.lib.helpers import urlsun
from tg import render_template
from base64 import b64encode
import qrcode
import qrcode.image.svg
from io import BytesIO
from pythonjupiter.lib.utility import ExportPDF
import base64

from pythonjupiter.lib.jqgrid import jqgridDataGrabber

class UnidadesTemplates(BaseController):

    @expose('json')
    def loadHistoric(self, **kw):
        filter = [('sobrecampo', 'eq', kw['sobrecampo'])]
        return jqgridDataGrabber(HistorialUnidades, 'id_historialunidades', filter, kw).loadGrid()

    @expose('pythonjupiter.templates.Catalogs.historic_jqgrid')
    def historic(self, **kw):
        return dict(page='historic_jqgrid', idreal='id_historialunidades',loadfrom='unidades',internal_id=kw['internal_id'], sobrecampo=kw['sobrecampo'])

    @expose('json')
    def load(self, **kw):
        filter = [('app_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(DatosUnidad, 'id_unidad', filter, kw).loadImageGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('app_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(DatosUnidad, 'id_unidad', filter, kw).updateGrid()

    @expose('json')
    def unitHistorial(self, dato, valor_anterior, valor_nuevo, sobrecampo, usuario):
        if valor_anterior != valor_nuevo:
            print("CAMBIO:", dato, valor_anterior, valor_nuevo)
            historialpersona = HistorialUnidades()
            historialpersona.dato = dato
            historialpersona.valor_anterior = valor_anterior
            historialpersona.valor_nuevo = valor_nuevo
            historialpersona.sobrecampo = sobrecampo
            historialpersona.usuario = usuario
            DBSession.add(historialpersona)
            DBSession.flush()
            return dict(error="ok", id_historial=historialpersona.id_historialpersona)
        return dict(error="ok", id_historial=0)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = DatosUnidad()
            kw['handler'].app_name = kw['app_name']
            kw['handler'].app_id = kw['app_id']
            kw['handler'].fecha_ingreso = date.today()
        else:
            kw['handler'] = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()

        kw['tipounidad'] = []
        handler = DBSession.query(TipoUnidad).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_tipounidad == kw['handler'].id_tipounidad:
                    kw['tipounidad'].append({'tipo_unidad': item.tipo_unidad, 'id': item.id_tipounidad, 'checked': 'selected'})
                else:
                    kw['tipounidad'].append({'tipo_unidad': item.tipo_unidad, 'id': item.id_tipounidad, 'checked': ''})
            else:
                kw['tipounidad'].append({'tipo_unidad': item.tipo_unidad, 'id': item.id_tipounidad, 'checked': ''})

        kw['tipotarjeta'] = []
        handler = DBSession.query(TipoTarjeta).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_tipotarjeta == kw['handler'].id_tipotarjeta:
                    kw['tipotarjeta'].append({'tipo_tarjeta': item.tipo_tarjeta, 'id': item.id_tipotarjeta, 'checked': 'selected'})
                else:
                    kw['tipotarjeta'].append({'tipo_tarjeta': item.tipo_tarjeta, 'id': item.id_tipotarjeta, 'checked': ''})
            else:
                kw['tipotarjeta'].append({'tipo_tarjeta': item.tipo_tarjeta, 'id': item.id_tipotarjeta, 'checked': ''})

        kw['enterprice'] = []
        handler = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_linea == kw['handler'].id_linea:
                    kw['enterprice'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': 'selected'})
                else:
                    kw['enterprice'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': ''})
            else:
                kw['enterprice'].append({'nombre_linea': item.nombre_linea, 'id': item.id_linea, 'checked': ''})

        kw['permisionario'] = []
        handler = DBSession.query(Persona).filter_by(espermisionario=1).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_persona == kw['handler'].id_persona:
                    kw['permisionario'].append( {'nombre': item.nombre, 'id': item.id_persona, 'checked': 'selected'})
                else:
                    kw['permisionario'].append({'nombre': item.nombre, 'id': item.id_persona, 'checked': ''})
            else:
                kw['permisionario'].append({'nombre': item.nombre, 'id': item.id_persona, 'checked': ''})

        kw['assurance'] = []
        handler = DBSession.query(Aseguradora).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_aseguradora == kw['handler'].id_aseguradora:
                    kw['assurance'].append({'aseguradora': item.aseguradora, 'id': item.id_aseguradora, 'checked': 'selected'})
                else:
                    kw['assurance'].append({'aseguradora': item.aseguradora, 'id': item.id_aseguradora, 'checked': ''})
            else:
                kw['assurance'].append({'aseguradora': item.aseguradora, 'id': item.id_aseguradora, 'checked': ''})

        kw['derrotero'] = []
        handler = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_derrotero == kw['handler'].id_derrotero:
                    kw['derrotero'].append({'derrotero': item.derrotero, 'id': item.id_derrotero, 'checked': 'selected'})
                else:
                    kw['derrotero'].append({'derrotero': item.derrotero, 'id': item.id_derrotero, 'checked': ''})
            else:
                kw['derrotero'].append({'derrotero': item.derrotero, 'id': item.id_derrotero, 'checked': ''})

        kw['group'] = []
        groups = []
        all_groups = DBSession.query(Grupo_DatosUnidad).filter_by(datosunidad_id=kw['id']).all()
        for item in all_groups:
            groups.append(item.grupo_id)

        handler = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()
        for item in handler:
            if kw['id'] != "0":
                if item.id_grupo in groups:
                    kw['group'].append({'grupo': item.grupo, 'id': item.id_grupo, 'checked': 'checked'})
                else:
                    kw['group'].append({'grupo': item.grupo, 'id': item.id_grupo, 'checked': ''})
            else:
                kw['group'].append({'grupo': item.grupo, 'id': item.id_grupo, 'checked': ''})

        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.UnitsTemplates.unit_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('json')
    def save(self,**kw):
        print("SAVE")
        print(kw)
        list = {}
        if kw['jupiterUnit_id'] == "0":
            handler = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['jupiterUnit_id']).filter_by(internal_id=kw['jupiterUnit_internal_id']).first()
            if handler == None:
                handler = DatosUnidad()
                handler.estatus = "ALTA"
                handler.fecha_estatus = date.today()
                handler.habilitado = 1  # ACTIVA
                handler.esunidadtransportes = "SI"
            i = 0
        else:
            handler = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['jupiterUnit_id']).first()
            list = {'jupiterUnit_vin': handler.vin, 'jupiterUnit_placas': handler.placas,
                    'jupiterUnit_numero_motor': handler.numero_motor, 'jupiterUnit_anio': handler.anio,'jupiterUnit_modelo': handler.modelo,
                    'jupiterUnit_marca': handler.marca, 'jupiterUnit_color': handler.color,
                    'jupiterUnit_kilometraje': handler.kilometraje, 'jupiterUnit_rasgos': handler.rasgos,'jupiterUnit_fecha_ingreso': handler.fecha_ingreso,
                    'jupiterUnit_observaciones': handler.observaciones,
                    'jupiterUnit_internal_id': handler.internal_id, 'jupiterUnit_eco1': handler.eco1,
                    'jupiterUnit_eco2': handler.eco2, 'jupiterUnit_app_id': handler.app_id,
                     'jupiterUnit_id_linea': handler.id_linea,
                    'jupiterUnit_id_tipounidad': handler.id_tipounidad, 'jupiterUnit_id_derrotero': handler.id_derrotero,
                    'jupiterUnit_assurance': handler.id_aseguradora,
                    'jupiterUnit_id_persona': handler.id_persona,
                    'jupiterUnit_id_tipotarjeta': handler.id_tipotarjeta,
                    'app_name': handler.app_name,
                    'foto': handler.foto, 'tarjetadecirculacion': handler.tarjetadecirculacion ,
                    }
            i = 1

        handler.vin = kw['jupiterUnit_vin']
        handler.placas = kw['jupiterUnit_placas']
        handler.numero_motor = kw['jupiterUnit_numero_motor']
        handler.modelo = kw['jupiterUnit_modelo']
        handler.marca = kw['jupiterUnit_marca']
        handler.color = kw['jupiterUnit_color']
        handler.kilometraje = kw['jupiterUnit_kilometraje']
        handler.rasgos = kw['jupiterUnit_rasgos']
        handler.observaciones = kw['jupiterUnit_observaciones']
        handler.internal_id = kw['jupiterUnit_internal_id']
        handler.eco1 = kw['jupiterUnit_eco1']
        handler.eco2 = kw['jupiterUnit_eco2']
        handler.app_id = kw['application_id']

        if kw['jupiterUnit_anio'] != "":
            handler.anio = kw['jupiterUnit_anio']
        handler.fecha_ingreso = kw['jupiterUnit_fecha_ingreso']
        handler.app_name = kw['app_name']

        id = DBSession.query(Linea).filter_by(id_linea=kw['jupiterUnit_id_linea']).first()
        if id != None:
            handler.id_linea = id.id_linea

        id = DBSession.query(TipoUnidad).filter_by(id_tipounidad=kw['jupiterUnit_id_tipounidad']).first()
        if id != None:
            handler.id_tipounidad = id.id_linea

        id = DBSession.query(Derrotero).filter_by(id_derrotero=kw['jupiterUnit_id_derrotero']).first()
        if id != None:
            handler.id_derrotero = id.id_derrotero

        id = DBSession.query(Aseguradora).filter_by(id_aseguradora=kw['jupiterUnit_assurance']).first()
        if id != None:
            handler.id_aseguradora = id.id_aseguradora

        id = DBSession.query(Persona).filter_by(id_persona=kw['jupiterUnit_id_persona']).first()
        if id != None:
            handler.id_persona = id.id_persona

        id = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=kw['jupiterUnit_id_tipotarjeta']).first()
        if id != None:
            handler.id_tipotarjeta = id.id_tipotarjeta
        #NO ENTIENDO QUE ES   handler.ruta_foto = kw['app_name']
        # NO ENTIENDO QUE ES   handler.concesion = kw['app_name']
        # NO ENTIENDO QUE ES   handler.cuentaespecial = kw['app_name']

        foto = kw["foto"]
        if foto != "undefined":
            if foto.file:
                fileName = foto.filename
                if fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".png") > 0:
                    ftyoe = "image"
                else:
                    return dict(error="Formato de imagen no valido")
        if foto != "undefined":
            handler.foto = foto.file.read()
            kw["foto"]  = foto.file.read()
        foto = kw["tarjetadecirculacion"]
        if foto != "undefined":
            if foto.file:
                fileName = foto.filename
                if fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".png") > 0:
                    ftyoe = "image"
                else:
                    return dict(error="Formato de imagen no valido")
        if foto != "undefined":
            handler.tarjetadecirculacion = foto.file.read()
            kw["tarjetadecirculacion"] = foto.file.read()

        if i == 0:
            DBSession.add(handler)
            for attr, value in kw.items():
                dato = attr.replace("jupiterPeople_","")
                dato = dato.replace("_", " ")
                dato = dato.capitalize()
                if attr in list:
                    cambio = self.personHistorial(dato=dato,valor_anterior=list[attr],valor_nuevo=kw[attr],sobrecampo=people.id_persona,usuario=kw['jupiterPeople_owner'])

        DBSession.flush()

        all = DBSession.query(Grupo_DatosUnidad).filter_by(datosunidad_id=kw['jupiterUnit_id']).all()
        for item in all:
            DBSession.delete(item)

        if 'jupiterUnit_id_grupo' in kw:
            for id in kw['jupiterUnit_id_grupo']:
                id_grupo= Grupo()
                get_id = DBSession.query(Grupo).filter_by(id_grupo=id).first()
                if get_id != None:
                    id_grupo.grupo_id = get_id.id_grupopersona
                    id_grupo.persona_id = handler.id_persona
                    DBSession.add(id_grupo)
                    DBSession.flush()

        return dict(error="ok", id=handler.id_aseguradora)

    #UNIDADES MARIA
    # @expose('pythonjupiter.templates.UnitsTemplates.unit_jqgrid')
    # def unidades(self, **kw):
    #     list = []
    #     internal_id = kw['internal_id']
    #     empresa = kw['app_name']
    #     application_id = kw['application_id']
    #     # laempresa=DBSession.query(Empresa).filter_by(nombre_empresa=empresa).first()
    #     list = [{'show': 'id_unidad'}, {'show': 'VIN'}, {'show': 'PLACAS'}, {'show': 'MARCA'},
    #             {'show': 'FOTO'}, {'show': 'ESTADO_POLIZA'}, {'show': 'PERMISIONARIO'}, {'show': 'OBSERVACIONES'},
    #             {'show': 'TIPOUNIDAD'}, {'show': 'eco1'}, {'show': 'ECO2'}, {'show': 'CONCESION'},
    #             {'show': 'TIPOTARJETA'}, {'show': 'HABILITADO'}, {'show': 'APP_NAME'},
    #             {'show': 'NOMBRE_EMPRESA'}, {'show': 'GRUPO'}]
    #
    #     queryusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
    #     perms = 'Add Unit,Enable Unit,Edit Unit,Disable Unit,AddPoliza'
    #     url = urlsun() + "/services/perms?perms=" + str(perms) + "&user=" + kw['user']
    #     r = requests.get(url, auth=('manager', 'managepass'))
    #     json_object = r.json()
    #     if json_object['error'] == "ok":
    #         perms = json_object['perms']
    #     else:
    #         perms = {}
    #     if queryusuario is None:
    #
    #         if kw['app_name'] == 'TODOchecarprueba':
    #             handler = DBSession.query(DatosUnidad).filter_by(internal_id=kw['internal_id']).all()
    #         else:
    #             handler = DBSession.query(DatosUnidad).filter_by(app_id=application_id).all()
    #
    #     else:
    #         handler = DBSession.query(DatosUnidad).filter_by(id_persona=queryusuario.id_persona).all()
    #     hd = []
    #     for item in handler:
    #         querytipotransporte = DBSession.query(TipoUnidad).filter_by(id_tipounidad=item.id_tipounidad).first()
    #         querypermisionario = DBSession.query(Persona).filter_by(id_persona=item.id_persona).first()
    #         querytipotarjeta = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=item.id_tipotarjeta).first()
    #         querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
    #         querygrupo = DBSession.query(Grupo).filter_by(id_grupo=item.id_grupo).first()
    #
    #         querypoliza = DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).filter(
    #             Poliza.fecha_inicio <= datetime.now().date()) \
    #             .filter(Poliza.fecha_fin >= datetime.now().date()).first()
    #
    #         queryallpolizas = DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).all()
    #         # print("Poliza de unidad: ",item.eco1)
    #         for item2 in queryallpolizas:
    #             pass
    #             # print("Fecha ini poliza: ", item2.fecha_inicio)
    #             # print("Fecha fin poliza: ", item2.fecha_fin)
    #
    #         # print("%%%%%%%%%%%%%%")
    #
    #         variablesobrepoliza = ''
    #         if querypoliza is not None:
    #             print("not None")
    #             hoy = datetime.now().date()
    #             fechafin = querypoliza.fecha_fin
    #             fechaunmesatras = fechafin - timedelta(days=30)
    #             if hoy >= fechaunmesatras and hoy <= fechafin:
    #                 variablesobrepoliza = 'VENCERA'
    #             elif hoy > fechafin:
    #                 variablesobrepoliza = 'VENCIO'
    #             elif hoy < fechaunmesatras:
    #                 variablesobrepoliza = 'BIEN'
    #         else:
    #             print("None polIZA ES =>")
    #             variablesobrepoliza = 'SIN'
    #
    #         if (item.habilitado == 1):
    #             varhabilitado = "ACTIVA"
    #         else:
    #             varhabilitado = "DESACTIVADA"
    #
    #         if querytipotarjeta is not None:
    #             variabletipotarjeta = querytipotarjeta.tipo_tarjeta
    #         else:
    #             variabletipotarjeta = ''
    #
    #         if querylinea is not None:
    #             variablelinea = querylinea.nombre_linea
    #         else:
    #             variablelinea = ''
    #
    #         if querytipotransporte is not None:
    #             variabletipotransporte = querytipotransporte.tipo_unidad
    #         else:
    #             variabletipotransporte = ''
    #
    #         if querygrupo is not None:
    #             variablegrupo = querygrupo.grupo
    #         else:
    #             variablegrupo = ''
    #
    #         if querypermisionario is not None:
    #             variablepermisionario = querypermisionario.nombre
    #         else:
    #             variablepermisionario = ''
    #
    #         hd.append({"id_unidad": item.id_unidad, "PLACAS": item.placas,
    #                    "MARCA": item.marca, "FOTO": item.ruta_foto, "VIN": item.vin,
    #                    "ESTADO_POLIZA": variablesobrepoliza, "PERMISIONARIO": variablepermisionario,
    #                    "APP_NAME": item.app_name,
    #                    "NOMBRE_EMPRESA": variablelinea, "OBSERVACIONES": item.observaciones,
    #                    "TIPOUNIDAD": variabletipotransporte, "eco1": item.eco1, "ECO2": item.eco2,
    #                    "CONCESION": item.concesion, "GRUPO": variablegrupo,
    #                    "TIPOTARJETA": variabletipotarjeta, "HABILITADO": varhabilitado})
    #
    #     # allrecords = DBSession.query(DatosUnidad).filter_by(internal_id=internal_id).all()
    #     # print("HAY EN HD: ", hd)
    #     contador = 0
    #     for item in hd:
    #         if item['HABILITADO'] == 'ACTIVA':
    #             contador = contador + 1
    #     return dict(page='unit_jqgrid', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'],user=kw['user'], application_id=kw['application_id'], contador=contador, perms=perms)

    @expose('pythonjupiter.templates.PersonasTemplates.unit_files')
    def files(self, **kw):
        handler = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        photoUnit = url() + "/img/personasilueta.gif"
        card = url() + "/img/credencial.jpg"
        #inePeople = url() + "/img/credencial.jpg"
        if handler != None:
            if str(handler.foto) != "None":
                photoUnit = "data:image/png;base64," + str(b64encode(handler.foto), 'utf-8')
            if str(handler.tarjetadecirculacion) != "None":
                card = "data:image/png;base64," + str(b64encode(handler.tarjetadecirculacion), 'utf-8')
            # if str(handler.ine) != "None":
            #     inePeople = "data:image/png;base64," + str(b64encode(handler.ine), 'utf-8')
        return dict(page='people_files', photoUnit=photoUnit, card=card)

    @expose('pythonjupiter.templates.PersonasTemplates.people_credential')
    def createCredential(self, **kw):
        person = DBSession.query(Persona).filter_by(id_persona=kw['id']).first()
        enterprise = DBSession.query(Linea).filter_by(id_linea=person.id_linea).first()
        photoPeople = url() + "/img/personasilueta.gif"
        if str(person.foto) != "None":
            photoPeople = "data:image/png;base64," + str(b64encode(person.foto), 'utf-8')
        if enterprise != None:
            laempresa = enterprise.nombre_linea
        else:
            laempresa = person.app_name

        json = {"ID": person.id_persona, "Name": person.nombre, "OperatorNumber": person.numerooperador,
                "Nickname": person.apodo, "Cellphone": person.celular}
        qr_info = json
        qr = qrcode.QRCode(
            box_size=40,
            border=4
        )
        qr.add_data(qr_info)
        buf = BytesIO()
        img = qr.make_image(fill_color="black", back_color="transparent")
        img.save(buf)
        person.codigoqr = buf.getvalue()
        DBSession.flush()

        QRcode = "data:image/png;base64," + str(b64encode(buf.getvalue()), 'utf-8')

        return dict(page="credential", QRcode=QRcode, photoPeople=photoPeople, internal_id=kw['internal_id'],
                    app_name=kw['app_name'], user=kw['user'], application_id=kw['application_id'], persona=person,
                    empresa=laempresa)

    # UNIDADES DIEGO
    @expose('pythonjupiter.templates.UnitsTemplates.unidades')
    def unidades(self, **kw):
        list = []
        internal_id = kw['internal_id']
        empresa = kw['app_name']
        application_id=kw['application_id']
        # laempresa=DBSession.query(Empresa).filter_by(nombre_empresa=empresa).first()
        list = [{'show': 'id_unidad'},{'show': 'VIN'}, {'show': 'PLACAS'},{'show': 'MARCA'},
                {'show': 'FOTO'}, {'show': 'ESTADO_POLIZA'},{'show': 'PERMISIONARIO'}, {'show': 'OBSERVACIONES'},
                {'show': 'TIPOUNIDAD'}, {'show': 'eco1'}, {'show': 'ECO2'}, {'show': 'CONCESION'},
                {'show': 'TIPOTARJETA'}, {'show': 'HABILITADO'}, {'show': 'APP_NAME'},
                {'show': 'NOMBRE_EMPRESA'},{'show': 'GRUPO'}]

        queryusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        perms='Add Unit,Enable Unit,Edit Unit,Disable Unit,AddPoliza'
        url = urlsun() + "/services/perms?perms=" + str(perms) + "&user=" + kw['user']
        r = requests.get(url, auth=('manager', 'managepass'))
        json_object = r.json()
        if json_object['error'] == "ok":
            perms = json_object['perms']
        else:
            perms = {}
        if queryusuario is None:

            if kw['app_name'] == 'TODOchecarprueba':
                handler = DBSession.query(DatosUnidad).filter_by(internal_id=kw['internal_id']).all()
            else:
                handler = DBSession.query(DatosUnidad).filter_by(app_id=application_id).all()

        else:
            handler = DBSession.query(DatosUnidad).filter_by(id_persona=queryusuario.id_persona).all()
        hd = []
        for item in handler:
            querytipotransporte = DBSession.query(TipoUnidad).filter_by(id_tipounidad=item.id_tipounidad).first()
            querypermisionario=DBSession.query(Persona).filter_by(id_persona=item.id_persona).first()
            querytipotarjeta = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=item.id_tipotarjeta).first()
            querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
            querygrupo=DBSession.query(Grupo).filter_by(id_grupo=item.id_grupo).first()

            querypoliza = DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).filter(Poliza.fecha_inicio<=datetime.now().date()) \
                            .filter(Poliza.fecha_fin>=datetime.now().date()).first()


            queryallpolizas=DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).all()
            #print("Poliza de unidad: ",item.eco1)
            for item2 in queryallpolizas:
                pass
                #print("Fecha ini poliza: ", item2.fecha_inicio)
                #print("Fecha fin poliza: ", item2.fecha_fin)

            #print("%%%%%%%%%%%%%%")

            variablesobrepoliza = ''
            if querypoliza is not None:
                print("not None")
                hoy = datetime.now().date()
                fechafin = querypoliza.fecha_fin
                fechaunmesatras = fechafin - timedelta(days=30)
                if hoy >= fechaunmesatras and hoy <= fechafin:
                    variablesobrepoliza = 'VENCERA'
                elif hoy > fechafin:
                    variablesobrepoliza = 'VENCIO'
                elif hoy < fechaunmesatras:
                    variablesobrepoliza = 'BIEN'
            else:
                print("None polIZA ES =>")
                variablesobrepoliza = 'SIN'


            if (item.habilitado == 1):
                varhabilitado = "ACTIVA"
            else:
                varhabilitado = "DESACTIVADA"

            if querytipotarjeta is not None:
                variabletipotarjeta = querytipotarjeta.tipo_tarjeta
            else:
                variabletipotarjeta = ''

            if querylinea is not None:
                variablelinea = querylinea.nombre_linea
            else:
                variablelinea = ''

            if querytipotransporte is not None:
                variabletipotransporte = querytipotransporte.tipo_unidad
            else:
                variabletipotransporte = ''

            if querygrupo is not None:
                variablegrupo=querygrupo.grupo
            else:
                variablegrupo=''

            if querypermisionario is not None:
                variablepermisionario=querypermisionario.nombre
            else:
                variablepermisionario=''

            hd.append({"id_unidad": item.id_unidad, "PLACAS": item.placas,
                       "MARCA": item.marca, "FOTO": item.ruta_foto,"VIN": item.vin,
                       "ESTADO_POLIZA": variablesobrepoliza,"PERMISIONARIO": variablepermisionario ,"APP_NAME": item.app_name,
                       "NOMBRE_EMPRESA": variablelinea, "OBSERVACIONES": item.observaciones,
                       "TIPOUNIDAD": variabletipotransporte, "eco1": item.eco1, "ECO2": item.eco2,
                       "CONCESION": item.concesion,"GRUPO": variablegrupo,
                       "TIPOTARJETA": variabletipotarjeta, "HABILITADO": varhabilitado})


        #allrecords = DBSession.query(DatosUnidad).filter_by(internal_id=internal_id).all()
        #print("HAY EN HD: ", hd)
        contador = 0
        for item in hd:
            if item['HABILITADO'] == 'ACTIVA':
                contador = contador + 1
        return dict(page='unidades', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'],
                    user=kw['user'], application_id=kw['application_id'],contador=contador,perms=perms)

    @expose('pythonjupiter.templates.UnitsTemplates.nuevaunidad')
    def nuevaunidad(self, **kw):
        typeunit = DBSession.query(TipoUnidad).filter_by(application_id=kw['application_id']).all()
        typecard = DBSession.query(TipoTarjeta).filter_by(application_id=kw['application_id']).all()
        typelinea = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        allpermisionarios=DBSession.query(Persona).filter_by(espermisionario=1).filter_by(habilitadopermisionario=1).filter_by(application_id=kw['application_id']).all()
        typeaseguradora = DBSession.query(Aseguradora).filter_by(application_id=kw['application_id']).all()
        if kw['app_name']!='TODOverifica':
            typederrotero = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
            typegrupo=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()



        typeempresa = []
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']

        #ecos2 = DBSession.query(DatosUnidad).all()
        ecosin = []

        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = []




        for item in empresa:  # AQUI SE PUEDE MODIFICAR PARA QUE SE CREEN UNIDADES DE AMZA Y ZAPATA EN EL MISMO
            if (item['application_name'] == kw['app_name'] and kw['app_name']!='TODO'):
                typeempresa.append({'name': item['application_name'], 'app_id': item['application_id'], 'owner': item['owner'],'internal_id': item['internal_id'],'client_id': item['client_id']})
            elif kw['app_name']=='TODO' and item['application_name'] != kw['app_name']:
                typeempresa.append({'name': item['application_name'], 'app_id': item['application_id'], 'owner': item['owner'],'internal_id': item['internal_id'],'client_id': item['client_id']})


        variableparatodo=''
        #print(typeempresa)
        if kw['app_name']=='TODOverifica':
            typederrotero = DBSession.query(Derrotero).filter_by(app_name=typeempresa[0]['name']).all()
            typegrupo=DBSession.query(Grupo).filter_by(app_name=typeempresa[0]['name']).all()
            variableparatodo=typeempresa[0]['name']
            client_id=typeempresa[0]['client_id']

        else:
            variableparatodo=kw['app_name']
            client_id = typeempresa[0]['client_id']



        #print("variableparatodo=",variableparatodo)
        #print("typelinea[0].nombre_linea=",typelinea[0].nombre_linea)
        #print("derrotero=typederrotero[0].derrotero=",typederrotero[0].derrotero)
        #print("typegrupo[0].grupo",typegrupo[0].grupo)
        try:
            cargosempresa=DBSession.query(CargoUnidad).filter_by(empresa=typelinea[0].nombre_linea).filter_by(app_name=variableparatodo).filter_by(application_id=kw['application_id']).all()
        except:
            cargosempresa={}
        try:
            cargoderrotero=DBSession.query(CargoUnidad).filter_by(derrotero=typederrotero[0].derrotero).filter_by(app_name=variableparatodo).filter_by(application_id=kw['application_id']).all()
        except:
            cargoderrotero={}
        #cargogrupo=DBSession.query(CargoUnidad).filter_by(grupo=typegrupo[0].grupo).filter_by(app_name=variableparatodo).all()

        empresatodas=DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name=variableparatodo).filter_by(application_id=kw['application_id']).all()
        derroterotodas=DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name=variableparatodo).filter_by(application_id=kw['application_id']).all()
        #grupotodas=DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name=variableparatodo).all()

        enapp_nametodo=DBSession.query(CargoUnidad).filter_by(app_name='TODO').filter_by(application_id=kw['application_id']).all()

        construiraux = []
        construir=[]

        for item in cargosempresa:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in cargoderrotero:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        # for item in cargogrupo:
        #     if item.cargo in construiraux:
        #         pass
        #     else:
        #         #print(item.cargo)
        #         construiraux.append(item.cargo)
        #         construir.append({'cargo': item.cargo,'monto': item.montodefault})


        for item in empresatodas:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in derroterotodas:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        # for item in grupotodas:
        #     if item.cargo in construiraux:
        #         pass
        #     else:
        #         #print(item.cargo)
        #         construiraux.append(item.cargo)
        #         construir.append({'cargo': item.cargo,'monto': item.montodefault})


        for item in enapp_nametodo:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})


        return dict(page='nuevaunidad', typeunit=typeunit, typecard=typecard, typelinea=typelinea,
                    typeempresa=typeempresa, internal_id=kw['internal_id'],
                    app_name=kw['app_name'], user=kw['user'], application_id=kw['application_id'], ecos=ecosin,client_id=client_id,
                    typederrotero=typederrotero, typeaseguradora=typeaseguradora,typegrupo=typegrupo,allpermisionarios=allpermisionarios,construir=construir)

    @expose('json')
    def uploadUnit(self, **kw):
        #print(kw)
        #print("lo del checkbox: ",kw['jupiter_cuentaespecial'])
        listagrupos=[]
        for item in kw:
            querygrupo=DBSession.query(Grupo).filter_by(grupo=item).filter_by(application_id=kw['application_id']).first()
            if querygrupo is not None:
                listagrupos.append(item)
        queryexiste=DBSession.query(DatosUnidad).filter_by(vin=kw['jupiter_vin']).filter_by(app_id=kw['application_id']).first()
        queryecodisponible=DBSession.query(DatosUnidad).filter_by(eco1=kw['jupiter_eco1']).filter_by(app_id=kw['application_id']).filter_by(habilitado=1).first()
        if queryecodisponible is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "ECO ya Asignado")
            return ''
        if queryexiste is None:
            try:
                print(kw['jupiter_fotounit'])
            except KeyError:
                print("NO VIENE")

            newitem = DatosUnidad()
            domain = app_globals.domainsun+"internal_id="+kw['internal_id']
            listaempresas = []
            empresa = ''
            try:
                content = requests.get(domain, auth=("manager", "managepass"))
                content = content.json()
                empresa = content['applications']
            except ValueError:
                content = ''
            #
            for item in empresa:
                newitem.internal_id = item['internal_id']
                #newitem.app_id = item['application_id']

            newitem.app_id = kw['application_id']
            query = DBSession.query(TipoUnidad).filter_by(tipo_unidad=kw['jupiter_tipo_unidad']).filter_by(application_id=kw['application_id']).first()
            query2 = DBSession.query(TipoTarjeta).filter_by(tipo_tarjeta=kw['jupiter_tipo_tarjeta']).filter_by(application_id=kw['application_id']).first()
            querylinea = DBSession.query(Linea).filter_by(nombre_linea=kw['jupiter_linea']).filter_by(app_id=kw['application_id']).first()
            try:
                queryderrotero = DBSession.query(Derrotero).filter_by(derrotero=kw['jupiter_derrotero']).filter_by(application_id=kw['application_id']).first()
            except:
                queryderrotero=''
            queryaseguradora = DBSession.query(Aseguradora).filter_by(aseguradora=kw['jupiter_aseguradora']).filter_by(application_id=kw['application_id']).first()
            #querygrupo=DBSession.query(Grupo).filter_by(grupo=kw['jupiter_grupo']).filter_by(internal_id=kw['internal_id']).first()
            try:
                querypermisionario=DBSession.query(Persona).filter_by(nombre=kw['jupiter_permisionario']).filter_by(application_id=kw['application_id']).first()
            except:
                querypermisionario=None
            #
            try:
                print("lo del checkbox: ", kw['jupiter_cuentaespecial'])
                variable = 1
            except KeyError:
                print("VACIO")
                variable = 0
            newitem.vin = kw['jupiter_vin']
            newitem.placas = kw['jupiter_placas']
            newitem.numero_motor = kw['jupiter_numero_motor']
            try:
                newitem.anio = 2
            except:
                newitem.anio=2
            newitem.modelo = kw['jupiter_modelo']
            newitem.marca = kw['jupiter_marca']
            newitem.color = kw['jupiter_color']
            newitem.kilometraje = kw['jupiter_kilometraje']
            newitem.rasgos = kw['jupiter_rasgos']
            newitem.estatus = 'ALTA'
            #newitem.fecha_estatus = kw['jupiter_fecha_estatus']
            try:
                newitem.id_aseguradora = queryaseguradora.id_aseguradora
            except:
                newitem.id_aseguradora=None
            newitem.observaciones = kw['jupiter_observaciones']
            newitem.app_name = kw['jupiter_empresa']
            newitem.app_id=kw['application_id']
            newitem.id_tipounidad = query.id_tipounidad
            newitem.esunidadtransportes = kw['jupiter_esunidad']
            newitem.cuentaespecial=variable
            newitem.habilitado = 1
            #
            #
            if (kw['jupiter_esunidad'] == 'SI'):
                newitem.eco1 = kw['jupiter_eco1']
                newitem.eco2 = kw['jupiter_eco2']
                newitem.concesion = kw['jupiter_concesion']
                try:
                    newitem.id_derrotero = queryderrotero.id_derrotero
                except:
                    pass
                newitem.id_tipotarjeta = query2.id_tipotarjeta
                newitem.id_linea = querylinea.id_linea
                # try:
                #     newitem.id_grupo = querygrupo.id_grupo
                # except:
                #     newitem.id_grupo=None
                if querypermisionario != None:
                    newitem.id_persona=querypermisionario.id_persona
                DBSession.add(newitem)
                DBSession.flush()
            else:
                DBSession.add(newitem)
                DBSession.flush()

            queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(vin=kw['jupiter_vin']).first()
            for item in kw:
                if 'montode' in item:
                    cargo=item.split('montode')
                    querycargo=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=cargo[1]).first()
                    queryexisteunidadcargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargo.id_cargounidad)\
                                            .filter_by(unidad_id=queryunidad.id_unidad).first()
                    if queryexisteunidadcargo is None:
                        newunidadcargo=UnidadCargo()
                        querycontador=DBSession.query(UnidadCargo).all()
                        contador = 0
                        for item2 in querycontador:
                            contador = int(item2.id)
                        contador = contador + 1
                        newunidadcargo.id=contador
                        newunidadcargo.cargo_id=querycargo.id_cargounidad
                        newunidadcargo.unidad_id=queryunidad.id_unidad
                        newunidadcargo.monto=kw[item]
                        DBSession.add(newunidadcargo)
                        DBSession.flush()


            queryparaunidad=DBSession.query(DatosUnidad).filter_by(vin=kw['jupiter_vin']).filter_by(app_id=kw['application_id']).first()
            for itemlistagrupo in listagrupos:
                querygrupo = DBSession.query(Grupo).filter_by(grupo=itemlistagrupo).filter_by(application_id=kw['application_id']).first()
                newgrupo_datosunidad = Grupo_DatosUnidad()
                newgrupo_datosunidad.grupo_id = querygrupo.id_grupo
                newgrupo_datosunidad.datosunidad_id = queryparaunidad.id_unidad
                DBSession.add(newgrupo_datosunidad)
            file = request.POST['jupiter_fotounit']
            if file != '':
                app_dir = os.getenv('JUPITER_DIR')
                if app_dir is None:
                    app_dir = os.getcwd()
                ruta = app_dir + os.sep
            #
                destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-"
                #print(destino)
                try:
                    os.makedirs(destino)
                except FileExistsError:
                    pass
                try:
                    destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/" + file.filename.lstrip(os.sep)
                    with open(destino, 'wb') as fdestino:
                        copyfileobj(file.file, fdestino)
                        print("Archivo copiado")
            #     #
                    file.file.close()
                    fdestino.close()
                    filename = request.params["jupiter_fotounit"].filename
                #print(filename)
            #
                    (this_file_name, this_file_extension) = os.path.splitext(filename)
                    if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg'):
                        print("ES IMAGEN")
                        #print(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/"  + filename)
                        f = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/"  + filename, 'rb').read()
                        queryparaunidad.foto = f
                        queryparaunidad.ruta_foto = filename
                except:
                    pass



            file=request.POST['jupiterpersona_tarjetacirculacion']
            if file != '':
                app_dir = os.getenv('JUPITER_DIR')
                if app_dir is None:
                    app_dir = os.getcwd()
                ruta = app_dir + os.sep
            #
                destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-"
                #print(destino)
                try:
                    os.makedirs(destino)
                except FileExistsError:
                    pass
                destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/" + file.filename.lstrip(os.sep)
                with open(destino, 'wb') as fdestino:
                    copyfileobj(file.file, fdestino)
                    print("Archivo copiado")
            #     #
                file.file.close()
                fdestino.close()
                filename = request.params["jupiterpersona_tarjetacirculacion"].filename
                #print(filename)
            #
                (this_file_name, this_file_extension) = os.path.splitext(filename)
                if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension == '.pdf'):
                    #print("ES IMAGEN")
                    #print(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/"  + filename)
                    f = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(queryparaunidad.id_unidad)+".-/"  + filename, 'rb').read()
                    #queryparaunidad.foto = f
                    queryparaunidad.tarjetadecirculacion = filename


            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Unidad Insertada")
            return ''
        else:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "Unidad con VIN Existente")
            return ''

    @expose('pythonjupiter.templates.UnitsTemplates.editunidad')
    def editunit(self, **kw):
        typeunit = DBSession.query(TipoUnidad).filter_by(application_id=kw['application_id']).all()  # checar bien los filtros
        typecard = DBSession.query(TipoTarjeta).filter_by(application_id=kw['application_id']).all()
        typeaseguradora = DBSession.query(Aseguradora).filter_by(application_id=kw['application_id']).all()
        typelinea = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        permisionario=DBSession.query(Persona).filter_by(espermisionario=1).filter_by(habilitadopermisionario=1).filter_by(application_id=kw['application_id']).all()
        if kw['app_name']!='TODOverificaeste':
            typederrotero = DBSession.query(Derrotero).filter_by(app_name=kw['app_name']).filter_by(application_id=kw['application_id']).all()
            typegrupo=DBSession.query(Grupo).filter_by(app_name=kw['app_name']).filter_by(application_id=kw['application_id']).all()
        typeempresa = []
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']
        domaincygnus = app_globals.domaincygnus+"app_id="+kw['application_id']
        ecosin = []
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = ''

        try:
            ecos = requests.get(domaincygnus, auth=("manager", "managepass"))
            ecos = ecos.json()
            ecos = ecos['ecos']
            for item in ecos:
                ecosin.append(item)

        except ValueError:
            ecosin = []

        for item in empresa:
            if item['application_name'] == kw['app_name']:
                typeempresa.append({'nombre_empresa': item['application_name'], 'app_id': item['application_id'], 'owner': item['owner'],
                 'internal_id': item['internal_id'],'client_id': item['client_id']})



        toedit = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        valueofempresa = toedit.app_name
        valueoflinea = DBSession.query(Linea).filter_by(id_linea=toedit.id_linea).first()
        valueoftipotarjeta = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=toedit.id_tipotarjeta).first()
        valueoftipounidad = DBSession.query(TipoUnidad).filter_by(id_tipounidad=toedit.id_tipounidad).first()
        valueofderrotero = DBSession.query(Derrotero).filter_by(id_derrotero=toedit.id_derrotero).first()
        valueofaseguradora = DBSession.query(Aseguradora).filter_by(id_aseguradora=toedit.id_aseguradora).first()
        valueofgrupo=DBSession.query(Grupo_DatosUnidad).filter_by(datosunidad_id=toedit.id_unidad).all()
        valueofpermisionario=DBSession.query(Persona).filter_by(id_persona=toedit.id_persona).first()
        todossuscargos=DBSession.query(UnidadCargo).filter_by(unidad_id=toedit.id_unidad).all()
        construir=[]
        listadegrupos=[]
        for itemgrupo in valueofgrupo:
            querygrupo = DBSession.query(Grupo).filter_by(id_grupo=itemgrupo.grupo_id).first()
            listadegrupos.append(querygrupo.grupo)
        for item in todossuscargos:
            cargo=DBSession.query(CargoUnidad).filter_by(id_cargounidad=item.cargo_id).first()
            construir.append({'cargo': cargo.cargo,'monto': item.monto})

        if valueoftipotarjeta is not None:
            variabletipotarjeta = valueoftipotarjeta.tipo_tarjeta
        else:
            variabletipotarjeta = ''

        if valueoflinea is not None:
            variablelinea = valueoflinea.nombre_linea
        else:
            variablelinea = ''

        if valueoftipounidad is not None:
            variabletipounidad = valueoftipounidad.tipo_unidad
        else:
            variabletipounidad = ''

        if valueofderrotero is not None:
            variablederrotero = valueofderrotero.derrotero
        else:
            variablederrotero = ''

        if valueofaseguradora is not None:
            variableaseguradora = valueofaseguradora.aseguradora
        else:
            variableaseguradora = ''

        # if valueofgrupo is not None:
        #     variablegrupo=valueofgrupo.grupo
        # else:
        #     variablegrupo=''

        if valueofpermisionario is not None:
            variablepermisionario=valueofpermisionario.nombre
        else:
            variablepermisionario=''

        if kw['app_name']=='TODOveritodo':
            typederrotero = DBSession.query(Derrotero).filter_by(app_name=toedit.app_name).all()
            typegrupo=DBSession.query(Grupo).filter_by(app_name=toedit.app_name).all()
            #typelinea = DBSession.query(Linea).filter_by(app_name=toedit.app_name).all()

        variabledeserie=''
        for itemdegrupos in listadegrupos:
            variabledeserie=variabledeserie+','+itemdegrupos


        listunidad = {'vin': toedit.vin, 'placas': toedit.placas, 'numero_motor': toedit.numero_motor,
                      'anio': toedit.anio, 'modelo': toedit.modelo, 'marca': toedit.marca, 'color': toedit.color,
                      'kilometraje': toedit.kilometraje, 'rasgos': toedit.rasgos, 'ruta_foto': toedit.ruta_foto,
                      'estatus': toedit.estatus, 'fecha_ingreso': toedit.fecha_ingreso.strftime("%Y-%m-%d"),
                      'observaciones': toedit.observaciones, 'nombre_empresa': valueofempresa,
                      'nombre_linea': variablelinea, 'tipo_unidad': variabletipounidad,
                      'esunidad': toedit.esunidadtransportes, 'tipo_tarjeta': variabletipotarjeta,
                      'aseguradora': variableaseguradora,"cuentaespecial": toedit.cuentaespecial,"listadegrupos": listadegrupos}

        listtransporte = {'eco1': toedit.eco1, 'eco2': toedit.eco2, 'concesion': toedit.concesion,
                          'nombre_empresa': valueofempresa, 'derrotero': variablederrotero,'permisionario': variablepermisionario}

        #print("listadegrupos-->",listadegrupos)
        client_id = typeempresa[0]['client_id']
        return dict(page='nuevaunidad', typeunit=typeunit, typecard=typecard, toedit=listunidad,
                    toedittransport=listtransporte, id=kw['id'], typeempresa=typeempresa, typelinea=typelinea,
                    internal_id=kw['internal_id'],
                    application_id=kw['application_id'], app_id=kw['application_id'], client_id=client_id,app_name=kw['app_name'], user=kw['user'], ecosin=ecosin,variabledeserie=variabledeserie,
                    typederrotero=typederrotero, typeaseguradora=typeaseguradora,typegrupo=typegrupo,allpermisionarios=permisionario,construir=construir)

    @expose('json')
    def editUnit(self, **kw):
        id = kw['id']
        #print("SOY EL KW DE editUnit: ", kw)
        querytoedit = DBSession.query(DatosUnidad).filter_by(id_unidad=id).first()
        queryvin=None
        if querytoedit is not None:
            if (querytoedit.vin != kw['vin']):
                queryvin=DBSession.query(DatosUnidad).filter_by(vin=kw['vin']).filter_by(application_id=kw['application_id']).first()


        queryecodisponible=DBSession.query(DatosUnidad).filter(DatosUnidad.id_unidad!=id).filter_by(eco1=kw['jupiter_eco1'])\
            .filter_by(app_id=kw['application_id']).filter_by(habilitado=1).first()
        if queryecodisponible is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "ECO ya Asignado")
            return ''
        if querytoedit is not None and queryvin is None:

            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            file = request.POST['jupiter_fotounit']

            if file != b'':
                print('No viene vacio')
                destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/unidades/" + str(id) + ".-/"
                try:
                    os.makedirs(destino)
                except FileExistsError:
                    pass
                destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/" + file.filename.lstrip(os.sep)
                with open(destino, 'wb') as fdestino:
                    copyfileobj(file.file, fdestino)
                    print("Archivo copiado")
                fdestino.close()
                filename = request.params["jupiter_fotounit"].filename
                #
                (this_file_name, this_file_extension) = os.path.splitext(filename)
                if (
                                this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg'):

                    f = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/"  + filename, 'rb').read()
                    try:
                        f2 = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/"  + querytoedit.ruta_foto, 'rb').read()
                    except TypeError:
                        f2 = 0
                    if (f == f2):
                        print('FOTOS SON IGUALES')
                    else:
                        print("DISTINTOS")
                        try:
                            os.remove(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/" + querytoedit.ruta_foto)
                        except FileNotFoundError:
                            pass
                        except TypeError:
                            pass
                        newhistoriaunidad = HistorialUnidades()
                        newhistoriaunidad.dato = 'foto'
                        newhistoriaunidad.valor_anterior = querytoedit.ruta_foto
                        newhistoriaunidad.valor_nuevo = filename
                        newhistoriaunidad.usuario = kw['who']
                        newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                        querytoedit.foto=f
                        querytoedit.ruta_foto = filename
                        DBSession.add(newhistoriaunidad)

            file = request.POST['jupiterpersona_tarjetacirculacion']
            if file != b'':
                #print('No viene vacio')
                destino = ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/" + file.filename.lstrip(os.sep)
                with open(destino, 'wb') as fdestino:
                    copyfileobj(file.file, fdestino)
                    print("Archivo copiado")
                fdestino.close()
                filename = request.params["jupiterpersona_tarjetacirculacion"].filename
                #
                (this_file_name, this_file_extension) = os.path.splitext(filename)
                if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension == '.pdf'):

                    f = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/"  + filename, 'rb').read()
                    try:
                        f2 = open(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/"  + querytoedit.tarjetadecirculacion, 'rb').read()
                    except:
                        f2='no'
                        pass
                    if (f == f2):
                        print('FOTOS SON IGUALES')
                    else:
                        print("DISTINTOS")
                        try:
                            os.remove(ruta + "pythonjupiter/public/img/"+kw['internal_id']+"/unidades/"+str(id)+".-/" + querytoedit.tarjetadecirculacion)
                        except FileNotFoundError:
                            pass
                        except TypeError:
                            pass
                        newhistoriaunidad = HistorialUnidades()
                        newhistoriaunidad.dato = 'Tarjeta de Circulacion'
                        newhistoriaunidad.valor_anterior = querytoedit.tarjetadecirculacion
                        newhistoriaunidad.valor_nuevo = filename
                        newhistoriaunidad.usuario = kw['who']
                        newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                        querytoedit.tarjetadecirculacion = filename
                        DBSession.add(newhistoriaunidad)






            query = DBSession.query(TipoUnidad).filter_by(application_id=kw['application_id']).filter_by(tipo_unidad=kw['tipo_unidad']).first()
            query2 = DBSession.query(TipoTarjeta).filter_by(application_id=kw['application_id']).filter_by(tipo_tarjeta=kw['tipo_tarjeta']).first()
            querylinea = DBSession.query(Linea).filter_by(app_id=kw['application_id']).filter_by(nombre_linea=kw['nombre_linea']).first()
            queryderrotero = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).filter_by(derrotero=kw['derrotero']).first()
            queryaseguradora = DBSession.query(Aseguradora).filter_by(application_id=kw['application_id']).filter_by(aseguradora=kw['aseguradora']).first()
            #querygrupo=DBSession.query(Grupo).filter_by(grupo=kw['grupo']).first()
            querypermisionario=DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(nombre=kw['jupiter_permisionarioedit']).first()

            try:
                print("lo del checkbox: ", kw['jupiter_cuentaespecial'])
                variable=1
                es='SI'
            except KeyError:
                print("VACIO")
                variable=0
                es = 'NO'


            if(querytoedit.cuentaespecial!=variable):
                newhistoriaunidad=HistorialUnidades()
                newhistoriaunidad.dato='cuentaespecial'
                if querytoedit.cuentaespecial==1:
                    anterior='SI'
                else:
                    anterior='NO'
                newhistoriaunidad.valor_anterior=anterior
                newhistoriaunidad.valor_nuevo=es
                newhistoriaunidad.usuario=kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.cuentaespecial=variable
                DBSession.add(newhistoriaunidad)



            if(querytoedit.id_persona!=querypermisionario.id_persona):
                newhistoriaunidad=HistorialUnidades()
                newhistoriaunidad.dato='Permisionario'
                anterior=DBSession.query(Persona).filter_by(id_persona=querytoedit.id_persona).first()
                if anterior is not None:
                    newhistoriaunidad.valor_anterior=anterior.nombre
                else:
                    newhistoriaunidad.valor_anterior=''
                newhistoriaunidad.valor_nuevo=querypermisionario.nombre
                newhistoriaunidad.usuario=kw['who']
                newhistoriaunidad.sobrecampo=querytoedit.id_unidad
                querytoedit.id_persona = querypermisionario.id_persona
                DBSession.add(newhistoriaunidad)

            try:
                variabledeaseguradora=queryaseguradora.id_aseguradora
            except:
                variabledeaseguradora=None
            if (querytoedit.id_aseguradora != variabledeaseguradora):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'aseguradora'
                anterior = DBSession.query(Aseguradora).filter_by(id_aseguradora=querytoedit.id_aseguradora).first()
                if anterior is not None:
                    newhistoriaunidad.valor_anterior = anterior.aseguradora
                else:
                    newhistoriaunidad.valor_anterior = ''
                nuevo = DBSession.query(Aseguradora).filter_by(id_aseguradora=variabledeaseguradora).first()
                if nuevo is not None:
                    newhistoriaunidad.valor_nuevo = nuevo.aseguradora
                else:
                    newhistoriaunidad.valor_nuevo = ''
                #newhistoriaunidad.valor_nuevo = nuevo.aseguradora
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.id_aseguradora = variabledeaseguradora
                DBSession.add(newhistoriaunidad)

            if (querytoedit.id_derrotero != queryderrotero.id_derrotero):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'derrotero'
                anterior = DBSession.query(Derrotero).filter_by(id_derrotero=querytoedit.id_derrotero).first()
                if anterior is not None:
                    newhistoriaunidad.valor_anterior = anterior.derrotero
                else:
                    newhistoriaunidad.valor_anterior = ''
                nuevo = DBSession.query(Derrotero).filter_by(id_derrotero=queryderrotero.id_derrotero).first()
                newhistoriaunidad.valor_nuevo = nuevo.derrotero
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.id_derrotero = queryderrotero.id_derrotero
                DBSession.add(newhistoriaunidad)

            # try:
            #     variabledegrupo=querygrupo.id_grupo
            # except:
            #     variabledegrupo=None

            # if (querytoedit.id_grupo != variabledegrupo):
            #     newhistoriaunidad = HistorialUnidades()
            #     newhistoriaunidad.dato = 'grupo'
            #     anterior = DBSession.query(Grupo).filter_by(id_grupo=querytoedit.id_grupo).first()
            #     if anterior is not None:
            #         newhistoriaunidad.valor_anterior = anterior.grupo
            #     else:
            #         newhistoriaunidad.valor_anterior = ''
            #     nuevo = DBSession.query(Grupo).filter_by(id_grupo=variabledegrupo).first()
            #     if nuevo is not None:
            #         newhistoriaunidad.valor_nuevo = nuevo.grupo
            #     else:
            #         newhistoriaunidad.valor_nuevo = ''
            #     #newhistoriaunidad.valor_nuevo = nuevo.grupo
            #     newhistoriaunidad.usuario = kw['who']
            #     newhistoriaunidad.sobrecampo = querytoedit.id_unidad
            #     querytoedit.id_grupo = variabledegrupo
            #     DBSession.add(newhistoriaunidad)


            listagrupos = []
            for item in kw:
                querygrupo = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(grupo=item).first()
                if querygrupo is not None:
                    listagrupos.append(item)


            for itemlistagrupo in listagrupos:
                querygrupo = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(grupo=itemlistagrupo).first()
                queryexisteelid=DBSession.query(Grupo_DatosUnidad).filter_by(grupo_id=querygrupo.id_grupo)\
                                    .filter_by(datosunidad_id=querytoedit.id_unidad).first()
                if queryexisteelid is None:
                    newgrupo_unidad = Grupo_DatosUnidad()
                    newgrupo_unidad.grupo_id = querygrupo.id_grupo
                    newgrupo_unidad.datosunidad_id = querytoedit.id_unidad
                    DBSession.add(newgrupo_unidad)
                    newhistorialunidad = HistorialUnidades()
                    newhistorialunidad.dato = 'Nuevo Grupo'
                    newhistorialunidad.valor_anterior = ''
                    newhistorialunidad.valor_nuevo = querygrupo.grupo
                    newhistorialunidad.sobrecampo = querytoedit.id_unidad
                    newhistorialunidad.usuario = kw['who']
                    DBSession.add(newhistorialunidad)
                else:
                    pass

            querygruposdeunidades=DBSession.query(Grupo_DatosUnidad).filter_by(datosunidad_id=querytoedit.id_unidad).all()
            for itemgruposdeunidad in querygruposdeunidades:
                queryelgrupo=DBSession.query(Grupo).filter_by(id_grupo=itemgruposdeunidad.grupo_id).first()
                if queryelgrupo.grupo not in listagrupos:
                    newhistorialunidad = HistorialUnidades()
                    newhistorialunidad.dato = 'Quita Grupo'
                    newhistorialunidad.valor_anterior = queryelgrupo.grupo
                    newhistorialunidad.valor_nuevo = ''
                    newhistorialunidad.sobrecampo = querytoedit.id_unidad
                    newhistorialunidad.usuario = kw['who']
                    DBSession.add(newhistorialunidad)
                    DBSession.delete(itemgruposdeunidad)

            if (querytoedit.id_linea != querylinea.id_linea):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'empresa'
                anterior = DBSession.query(Linea).filter_by(id_linea=querytoedit.id_linea).first()
                if anterior is not None:
                    newhistoriaunidad.valor_anterior = anterior.nombre_linea
                else:
                    newhistoriaunidad.valor_anterior = ''
                nuevo = DBSession.query(Linea).filter_by(id_linea=querylinea.id_linea).first()
                newhistoriaunidad.valor_nuevo = nuevo.nombre_linea
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.id_linea = querylinea.id_linea
                DBSession.add(newhistoriaunidad)

            if(querytoedit.vin !=  kw['vin']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato='vin'
                newhistoriaunidad.valor_anterior=querytoedit.vin
                newhistoriaunidad.valor_nuevo=kw['vin']
                newhistoriaunidad.usuario=kw['who']
                newhistoriaunidad.sobrecampo=querytoedit.id_unidad
                DBSession.add(newhistoriaunidad)
                querytoedit.vin = kw['vin']


            if (querytoedit.placas != kw['placas']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'placas'
                newhistoriaunidad.valor_anterior = querytoedit.placas
                newhistoriaunidad.valor_nuevo = kw['placas']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.placas = kw['placas']
                DBSession.add(newhistoriaunidad)

            if (querytoedit.numero_motor != kw['numero_motor']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'numero_motor'
                newhistoriaunidad.valor_anterior = querytoedit.numero_motor
                newhistoriaunidad.valor_nuevo = kw['numero_motor']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.numero_motor = kw['numero_motor']
                DBSession.add(newhistoriaunidad)

            # if (querytoedit.anio != int(kw['anio'])):
            #     newhistoriaunidad = HistorialUnidades()
            #     newhistoriaunidad.dato = 'anio'
            #     newhistoriaunidad.valor_anterior = querytoedit.anio
            #     newhistoriaunidad.valor_nuevo = int(kw['anio'])
            #     newhistoriaunidad.usuario = kw['who']
            #     newhistoriaunidad.sobrecampo = querytoedit.id_unidad
            #     querytoedit.anio = int(kw['anio'])
            #     DBSession.add(newhistoriaunidad)

            if (querytoedit.modelo != kw['modelo']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'modelo'
                newhistoriaunidad.valor_anterior = querytoedit.modelo
                newhistoriaunidad.valor_nuevo = kw['modelo']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.modelo = kw['modelo']
                DBSession.add(newhistoriaunidad)

            if (querytoedit.marca != kw['marca']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'marca'
                newhistoriaunidad.valor_anterior = querytoedit.marca
                newhistoriaunidad.valor_nuevo = kw['marca']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.marca = kw['marca']
                DBSession.add(newhistoriaunidad)

            if (querytoedit.color != kw['color']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'color'
                newhistoriaunidad.valor_anterior = querytoedit.color
                newhistoriaunidad.valor_nuevo = kw['color']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.color = kw['color']
                DBSession.add(newhistoriaunidad)

            if (querytoedit.kilometraje != kw['kilometraje']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'kilometraje'
                newhistoriaunidad.valor_anterior = querytoedit.kilometraje
                newhistoriaunidad.valor_nuevo = kw['kilometraje']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.kilometraje = kw['kilometraje']
                DBSession.add(newhistoriaunidad)

            if (querytoedit.rasgos != kw['rasgos']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'rasgos'
                newhistoriaunidad.valor_anterior = querytoedit.rasgos
                newhistoriaunidad.valor_nuevo = kw['rasgos']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.rasgos = kw['rasgos']
                DBSession.add(newhistoriaunidad)

            # if (querytoedit.estatus != kw['estatus']):
            #     newhistoriaunidad = HistorialUnidades()
            #     newhistoriaunidad.dato = 'estatus'
            #     newhistoriaunidad.valor_anterior = querytoedit.estatus
            #     newhistoriaunidad.valor_nuevo = kw['estatus']
            #     newhistoriaunidad.usuario = kw['who']
            #     newhistoriaunidad.sobrecampo = querytoedit.id_unidad
            #     querytoedit.estatus = kw['estatus']
            #     querytoedit.fecha_ingreso=datetime.now()
            #     DBSession.add(newhistoriaunidad)

            # fechanew = datetime.strptime(kw['fecha_estatus'], '%Y-%m-%d').date()
            #
            # if (querytoedit.fecha_estatus != fechanew):
            #     newhistoriaunidad = HistorialUnidades()
            #     newhistoriaunidad.dato = 'fecha_estatus'
            #     newhistoriaunidad.valor_anterior = querytoedit.fecha_estatus
            #     newhistoriaunidad.valor_nuevo = fechanew
            #     newhistoriaunidad.usuario = kw['who']
            #     newhistoriaunidad.sobrecampo = querytoedit.id_unidad
            #     querytoedit.fecha_estatus = fechanew
            #     DBSession.add(newhistoriaunidad)

            if (querytoedit.observaciones != kw['observaciones']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'observaciones'
                newhistoriaunidad.valor_anterior = querytoedit.observaciones
                newhistoriaunidad.valor_nuevo = kw['observaciones']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.observaciones = kw['observaciones']
                DBSession.add(newhistoriaunidad)

            querytoedit.internal_id = kw['internal_id']

            domain = app_globals.domainsun+"internal_id="+kw['internal_id']
            try:
                content = requests.get(domain, auth=("manager", "managepass"))
                content = content.json()
                empresa = content['applications']
            except ValueError:
                empresa = ''

            for item in empresa:
                if (kw['nombre_empresa'] == item['application_name']):
                    if (querytoedit.app_name != item['application_name']):
                        newhistoriaunidad = HistorialUnidades()
                        newhistoriaunidad.dato = 'app_name'
                        newhistoriaunidad.valor_anterior = querytoedit.app_name
                        newhistoriaunidad.valor_nuevo = item['application_name']
                        newhistoriaunidad.usuario = kw['who']
                        newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                        querytoedit.app_name = item['application_name']
                        querytoedit.app_id = item['application_id']
                        DBSession.add(newhistoriaunidad)

            if (querytoedit.id_tipounidad != query.id_tipounidad):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'tipo_unidad'
                anterior = DBSession.query(TipoUnidad).filter_by(id_tipounidad=querytoedit.id_tipounidad).first()
                if anterior is not None:
                    newhistoriaunidad.valor_anterior = anterior.tipo_unidad
                else:
                    newhistoriaunidad.valor_anterior = ''
                despues = DBSession.query(TipoUnidad).filter_by(id_tipounidad=query.id_tipounidad).first()
                newhistoriaunidad.valor_nuevo = despues.tipo_unidad
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.id_tipounidad = query.id_tipounidad
                DBSession.add(newhistoriaunidad)

            if (querytoedit.esunidadtransportes != kw['esunidad']):
                newhistoriaunidad = HistorialUnidades()
                newhistoriaunidad.dato = 'es_unidad'
                newhistoriaunidad.valor_anterior = querytoedit.esunidadtransportes
                newhistoriaunidad.valor_nuevo = kw['esunidad']
                newhistoriaunidad.usuario = kw['who']
                newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                querytoedit.esunidadtransportes = kw['esunidad']
                DBSession.add(newhistoriaunidad)

            if (kw['esunidad'] == 'SI'):
                # query3 = DBSession.query(DatosUnidad).filter_by(vin=kw['vin']).first()

                if (querytoedit.eco1 != kw['jupiter_eco1']):
                    newhistoriaunidad = HistorialUnidades()
                    newhistoriaunidad.dato = 'eco1'
                    newhistoriaunidad.valor_anterior = querytoedit.eco1
                    newhistoriaunidad.valor_nuevo = kw['jupiter_eco1']
                    newhistoriaunidad.usuario = kw['who']
                    newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                    querytoedit.eco1 = kw['jupiter_eco1']
                    DBSession.add(newhistoriaunidad)

                if (querytoedit.eco2 != kw['jupiter_eco2']):
                    newhistoriaunidad = HistorialUnidades()
                    newhistoriaunidad.dato = 'eco2'
                    newhistoriaunidad.valor_anterior = querytoedit.eco2
                    newhistoriaunidad.valor_nuevo = kw['jupiter_eco2']
                    newhistoriaunidad.usuario = kw['who']
                    newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                    querytoedit.eco2 = kw['jupiter_eco2']
                    DBSession.add(newhistoriaunidad)

                if (querytoedit.concesion != kw['concesion']):
                    newhistoriaunidad = HistorialUnidades()
                    newhistoriaunidad.dato = 'concesion'
                    newhistoriaunidad.valor_anterior = querytoedit.concesion
                    newhistoriaunidad.valor_nuevo = kw['concesion']
                    newhistoriaunidad.usuario = kw['who']
                    newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                    querytoedit.concesion = kw['concesion']
                    DBSession.add(newhistoriaunidad)

                    # checar internal_id
                    # querytoedit.internal_id = 0

                if (querytoedit.id_tipotarjeta != query2.id_tipotarjeta):
                    newhistoriaunidad = HistorialUnidades()
                    newhistoriaunidad.dato = 'tarjeta'
                    anterior = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=querytoedit.id_tipotarjeta).first()
                    if anterior is not None:
                        newhistoriaunidad.valor_anterior = anterior.tipo_tarjeta
                    else:
                        newhistoriaunidad.valor_anterior = ''
                    despues = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=query2.id_tipotarjeta).first()
                    newhistoriaunidad.valor_nuevo = despues.tipo_tarjeta
                    newhistoriaunidad.usuario = kw['who']
                    newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                    querytoedit.id_tipotarjeta = query2.id_tipotarjeta
                    DBSession.add(newhistoriaunidad)


                DBSession.flush()
                querycargosdelaunidad = DBSession.query(UnidadCargo).filter_by(unidad_id=querytoedit.id_unidad).all()
                cargosunidad = []
                for item2 in querycargosdelaunidad:
                    querynombre = DBSession.query(CargoUnidad).filter_by(id_cargounidad=item2.cargo_id).first()
                    cargosunidad.append(querynombre.cargo)

                #print("cargosunidad==", cargosunidad)
                for item in kw:
                    if 'montode' in item:
                        cargo = item.split('montode')
                        if cargo[1] in cargosunidad:
                            cargosunidad.remove(cargo[1])

                #print("cargosunidad==", cargosunidad)


                for elem in cargosunidad:
                    newhistoriaunidad = HistorialUnidades()
                    newhistoriaunidad.dato = 'Quita cargo'
                    newhistoriaunidad.valor_anterior = elem
                    newhistoriaunidad.valor_nuevo = ''
                    newhistoriaunidad.usuario = kw['user']
                    newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                    DBSession.add(newhistoriaunidad)

                    querycargo=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=elem).first()
                    queryaeliminar=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargo.id_cargounidad).filter_by(unidad_id=querytoedit.id_unidad).first()
                    DBSession.delete(queryaeliminar)

                for item in kw:
                    if 'montode' in item:
                        cargo = item.split('montode')
                        querycargo = DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=cargo[1]).first()

                        queryexisteelcargo=DBSession.query(UnidadCargo).filter_by(unidad_id=querytoedit.id_unidad).filter_by(cargo_id=querycargo.id_cargounidad).first()
                        if queryexisteelcargo is None:
                            newunidadcargo = UnidadCargo()
                            querycontador = DBSession.query(UnidadCargo).all()
                            contador = 0
                            for item2 in querycontador:
                                contador = item2.id
                            contador = contador + 1
                            newunidadcargo.id = contador
                            newunidadcargo.cargo_id = querycargo.id_cargounidad
                            newunidadcargo.unidad_id = querytoedit.id_unidad
                            newunidadcargo.monto = kw[item]
                            DBSession.add(newunidadcargo)
                            newhistoriaunidad = HistorialUnidades()
                            newhistoriaunidad.dato = 'Nuevo cargo'
                            newhistoriaunidad.valor_anterior = ''
                            newhistoriaunidad.valor_nuevo = querycargo.cargo
                            newhistoriaunidad.usuario = kw['user']
                            newhistoriaunidad.sobrecampo = querytoedit.id_unidad
                            DBSession.add(newhistoriaunidad)

                        else:

                            if(queryexisteelcargo.monto!=int(kw[item])):
                                newhistoriaunidad=HistorialUnidades()
                                newhistoriaunidad.dato=cargo[1]+' monto'
                                newhistoriaunidad.valor_anterior=queryexisteelcargo.monto
                                newhistoriaunidad.valor_nuevo=kw[item]
                                newhistoriaunidad.usuario=kw['user']
                                newhistoriaunidad.sobrecampo=querytoedit.id_unidad
                                queryexisteelcargo.monto = kw[item]
                                DBSession.add(newhistoriaunidad)
                                DBSession.flush()



            DBSession.flush()
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Unidad Editada")
            return ""
        else:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "VIN Existente")
            return ""

    @expose('json')
    def disableunit(self, **kw):
        id = kw['id']
        #eco = kw['eco']
        #print("ECO: ", eco)
        #if eco == '' or eco == 'null':
        #   print("NO TRAE")
        queryunidad = DBSession.query(DatosUnidad).filter_by(id_unidad=id).first()
        queryunidad.habilitado = 0
        return "Unidad deshabilitada"

    @expose('pythonjupiter.templates.UnitsTemplates.habilitar')
    def habilitarunidad(self,**kw):
        if kw['app_name']!='TODOveritodo':
            allrecords = DBSession.query(DatosUnidad).filter_by(habilitado=0).filter_by(app_id=kw['application_id']).filter_by(app_name=kw['app_name']).all()
        else:
            allrecords = DBSession.query(DatosUnidad).filter_by(habilitado=0).filter_by(internal_id=kw['internal_id']).all()
        return dict(page='habilitar', allrecords=allrecords)

    @expose('json')
    def enable(self, **kw):
        id = kw['id']
        internal_id=kw['internal_id']
        application_id=kw['application_id']
        query = DBSession.query(DatosUnidad).filter_by(id_unidad=id).first()
        queryexiste=DBSession.query(DatosUnidad).filter_by(eco1=query.eco1).filter(DatosUnidad.id_unidad!=query.id_unidad).filter_by(app_id=application_id).filter_by(habilitado=1).first()
        if queryexiste is None:
            query.habilitado = 1
            return "Unidad Habilitada"
        else:
            return "Existe una Unidad Habilitada con el mismo eco no se puede activar esta unidad"

    @expose('json')
    def searchunits(self, **kw):
        listabusca = []
        searchfor = kw['searchfor']
        if kw['name'] != '' or kw['name'] != ' ':
            list = [{'show': 'id_unidad'}, {'show': 'vin'},{'show': 'placas'},{'show': 'permisionario'}, {'show': 'marca'},
                    {'show': 'ruta_foto'}, {'show': 'estatus'}, {'show': 'observaciones'},
                    {'show': 'id_tipounidad'}, {'show': 'eco1'}, {'show': 'eco2'}, {'show': 'concesion'},
                    {'show': 'id_tipotarjeta'},
                    {'show': 'habilitado'}, {'show': 'nombre_empresa'}, {'show': 'nombre_linea'},{'show': 'id_grupo'}]

            queryusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
            print("queryusuario===>",queryusuario)
            if queryusuario is None:
                if kw['app_name']!='TODO':

                    if (searchfor != 'nombre empresa' and searchfor!='permisionario'):
                        queryname = DBSession.query(DatosUnidad).filter(getattr(DatosUnidad,searchfor).like('%' + kw['name'] + '%')).filter(DatosUnidad.app_id == kw['application_id']).all()

                    if (searchfor == 'nombre empresa'):
                        querylinea = DBSession.query(Linea).filter(Linea.nombre_linea==kw['name']).filter(Linea.app_id == kw['application_id']).first()
                        if querylinea is not None:
                            queryname = DBSession.query(DatosUnidad).filter(DatosUnidad.id_linea == querylinea.id_linea).filter(DatosUnidad.app_id == kw['application_id']).all()
                        else:
                            queryname = {}

                    if(searchfor=='permisionario'):
                        querypermisionario=DBSession.query(Persona).filter(Persona.nombre.like('%'+kw['name']+'%')).filter(Persona.application_id==kw['application_id']).first()
                        if querypermisionario is not None:
                            queryname=DBSession.query(DatosUnidad).filter(DatosUnidad.id_persona==querypermisionario.id_persona).filter(Persona.application_id==kw['application_id']).filter_by(app_id=kw['application_id']).all()
                        else:
                            queryname={}

                else:
                    if (searchfor != 'nombre empresa' and searchfor!='permisionario'):
                        queryname = DBSession.query(DatosUnidad).filter(getattr(DatosUnidad,searchfor).like('%' + kw['name'] + '%')).filter(DatosUnidad.app_id == kw['application_id']).all()

                    if (searchfor == 'nombre empresa'):
                        querylinea = DBSession.query(Linea).filter(Linea.nombre_linea.like('%' + kw['name'] + '%')).filter(DatosUnidad.app_id == kw['application_id']).first()
                        if querylinea is not None:
                            queryname = DBSession.query(DatosUnidad).filter(DatosUnidad.id_linea == querylinea.id_linea).filter(DatosUnidad.app_id == kw['application_id']).all()
                        else:
                            queryname = {}

                    if(searchfor=='permisionario'):
                        querypermisionario=DBSession.query(Persona).filter(Persona.nombre.like('%'+kw['name']+'%')).filter(Persona.application_id==kw['application_id']).first()
                        if querypermisionario is not None:
                            queryname=DBSession.query(DatosUnidad).filter(DatosUnidad.id_persona==querypermisionario.id_persona).filter(Persona.application_id==kw['application_id']).all()
                        else:
                            queryname={}

            else:
                if (searchfor != 'nombre empresa' and searchfor != 'permisionario'):
                    print("SEARCHFOR !=")
                    queryname = DBSession.query(DatosUnidad).filter(
                        getattr(DatosUnidad, searchfor).like('%' + kw['name'] + '%')).filter(
                        DatosUnidad.app_id == kw['application_id']).filter_by(id_persona=queryusuario.id_persona).all()

                if (searchfor == 'nombre empresa'):
                    print("NOMBRE EMPRESA ==")
                    querylinea = DBSession.query(Linea).filter(Linea.nombre_linea.like('%' + kw['name'] + '%')).filter(
                        DatosUnidad.app_id == kw['application_id']).first()
                    if querylinea is not None:
                        queryname = DBSession.query(DatosUnidad).filter(
                            DatosUnidad.id_linea == querylinea.id_linea).filter(
                            DatosUnidad.app_id == kw['application_id']).filter(DatosUnidad.id_persona==queryusuario.id_persona).all()
                    else:
                        queryname = {}

                if (searchfor == 'permisionario'):
                    print("permisionario==")
                    querypermisionario = DBSession.query(Persona).filter(
                        Persona.nombre.like('%' + kw['name'] + '%')).filter(
                        Persona.application_id == kw['application_id']).filter_by(id_persona=queryusuario.id_persona).first()
                    if querypermisionario is not None:
                        queryname = DBSession.query(DatosUnidad).filter(
                            DatosUnidad.id_persona == querypermisionario.id_persona).filter(
                            Persona.application_id == kw['application_id']).filter(DatosUnidad.id_persona==queryusuario.id_persona).all()
                    else:
                        queryname = {}

            if queryname is not []:
                for item in queryname:

                    querytipotransporte = DBSession.query(TipoUnidad).filter_by(id_tipounidad=item.id_tipounidad).first()
                    querytipotarjeta = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=item.id_tipotarjeta).first()
                    querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
                    querypermisionario=DBSession.query(Persona).filter_by(id_persona=item.id_persona).first()

                    querygrupo=DBSession.query(Grupo).filter_by(id_grupo=item.id_grupo).first()

                    querypoliza = DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).filter(
                        Poliza.fecha_inicio <= datetime.now().date()) \
                        .filter(Poliza.fecha_fin >= datetime.now().date()).first()

                    variablesobrepoliza = ''
                    if querypoliza is not None:
                        hoy = datetime.now().date()
                        fechafin = querypoliza.fecha_fin
                        fechaunmesatras = fechafin - timedelta(days=30)
                        if hoy >= fechaunmesatras and hoy <= fechafin:
                            variablesobrepoliza = 'VENCERA'
                        elif hoy > fechafin:
                            variablesobrepoliza = 'VENCIO'
                        elif hoy < fechaunmesatras:
                            variablesobrepoliza = 'BIEN'
                    else:
                        variablesobrepoliza = 'SIN'


                    if (item.habilitado == 1):
                        varhabilitado = "ACTIVA"
                    else:
                        varhabilitado = "DESACTIVADA"

                    if querytipotarjeta is not None:
                        variabletipotarjeta = querytipotarjeta.tipo_tarjeta
                    else:
                        variabletipotarjeta = ''

                    if querygrupo is not None:
                        variablegrupo=querygrupo.grupo
                    else:
                        variablegrupo=''

                    if querypermisionario is not None:
                        variablepermisionario=querypermisionario.nombre
                    else:
                        variablepermisionario=''

                    if querytipotransporte is not None:
                        variabletipotransporte = querytipotransporte.tipo_unidad
                    else:
                        variabletipotransporte = ''

                    listabusca.append(
                        {"id_unidad": item.id_unidad,"vin": item.vin ,"placas": item.placas,
                         "marca": item.marca, "ruta_foto": item.ruta_foto,
                         "estatus": variablesobrepoliza,"permisionario": variablepermisionario ,"nombre_empresa": item.app_name,
                         "nombre_linea": querylinea.nombre_linea, "observaciones": item.observaciones,
                         "id_tipounidad": variabletipotransporte, "eco1": item.eco1, "eco2": item.eco2,
                         "concesion": item.concesion,
                         "id_tipotarjeta": variabletipotarjeta, "habilitado": varhabilitado,"id_grupo": variablegrupo})
                #print("LISTABUSCA", listabusca)
                contador = 0
                for item in listabusca:
                    if item['habilitado'] == 'ACTIVA':
                        contador = contador + 1
                return dict(error='ok', listabusca=listabusca, list=list,contador=contador)
            else:
                contador = 0
                for item in listabusca:
                    if item['habilitado'] == 'ACTIVA':
                        contador = contador + 1
                return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list,contador=contador)

    @expose('pythonjupiter.templates.UnitsTemplates.historialunidad')
    def historialde(self,**kw):
        id=kw['id']
        allrecords=DBSession.query(HistorialUnidades).filter_by(sobrecampo=id).all()
        return dict(page='historialde',allrecords=allrecords)

    @expose('json')
    def actualizalineas(self,**kw):
        #empresa=DBSession.query(Empresa).filter_by(nombre_empresa=kw['empresa']).first()
        lineas = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        return dict(lineas=lineas)

    @expose('json')
    def actualizaderrotero(self,**kw):
        #empresa=DBSession.query(Empresa).filter_by(nombre_empresa=kw['empresa']).first()
        if kw['empresa']!='TODO':
            derroteros = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
        else:
            derroteros = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()

        return dict(derroteros=derroteros)

    @expose('json')
    def actualizagrupos(self,**kw):
        #empresa=DBSession.query(Empresa).filter_by(nombre_empresa=kw['empresa']).first()
        if kw['empresa']!='TODO':
            grupos = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()
        else:
            grupos = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

        return dict(grupos=grupos)

    @expose('json')
    def reloadunits(self, **kw):
        listabusca = []
        list = [{'show': 'id_unidad'},{'show': 'vin'}, {'show': 'placas'}, {'show': 'permisionario'}, {'show': 'marca'},
                {'show': 'ruta_foto'}, {'show': 'estatus'}, {'show': 'observaciones'},
                {'show': 'id_tipounidad'}, {'show': 'eco1'}, {'show': 'eco2'}, {'show': 'concesion'},
                {'show': 'id_tipotarjeta'},
                {'show': 'habilitado'}, {'show': 'nombre_empresa'}, {'show': 'nombre_linea'},{'show': 'grupo'}]

        queryusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        if queryusuario is None:
            if kw['app_name']!='TODOveritodo':
                queryname = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).all()
            else:
                queryname = DBSession.query(DatosUnidad).filter_by(internal_id=kw['internal_id']).all()
        else:
            queryname=DBSession.query(DatosUnidad).filter_by(id_persona=queryusuario.id_persona).all()
        if queryname is not []:
            for item in queryname:
                querytipotransporte = DBSession.query(TipoUnidad).filter_by(id_tipounidad=item.id_tipounidad).first()
                querytipotarjeta = DBSession.query(TipoTarjeta).filter_by(id_tipotarjeta=item.id_tipotarjeta).first()
                querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
                querygrupo=DBSession.query(Grupo).filter_by(id_grupo=item.id_grupo).first()
                querypermisionario=DBSession.query(Persona).filter_by(id_persona=item.id_persona).first()
                querypoliza = DBSession.query(Poliza).filter_by(id_unidad=item.id_unidad).filter(
                    Poliza.fecha_inicio <= datetime.now().date()) \
                    .filter(Poliza.fecha_fin >= datetime.now().date()).first()

                variablesobrepoliza = ''
                if querypoliza is not None:
                    hoy = datetime.now().date()
                    fechafin = querypoliza.fecha_fin
                    fechaunmesatras = fechafin - timedelta(days=30)
                    if hoy >= fechaunmesatras and hoy <= fechafin:
                        variablesobrepoliza = 'VENCERA'
                    elif hoy > fechafin:
                        variablesobrepoliza = 'VENCIO'
                    elif hoy < fechaunmesatras:
                        variablesobrepoliza = 'BIEN'
                else:
                    variablesobrepoliza = 'SIN'

                if (item.habilitado == 1):
                    varhabilitado = "ACTIVA"
                else:
                    varhabilitado = "DESACTIVADA"

                if querytipotarjeta is not None:
                    variabletipotarjeta = querytipotarjeta.tipo_tarjeta
                else:
                    variabletipotarjeta = ''

                if querygrupo is not None:
                    variablegrupo=querygrupo.grupo
                else:
                    variablegrupo=''

                if querypermisionario is not None:
                    variablepermisionario=querypermisionario.nombre
                else:
                    variablepermisionario=''

                if querytipotransporte is not None:
                    variabletipotransporte=querytipotransporte.tipo_unidad
                else:
                    variabletipotransporte=''

                try:
                    nl=querylinea.nombre_linea
                except:
                    nl=''



                listabusca.append(
                    {"id_unidad": item.id_unidad,"vin": item.vin ,"placas": item.placas, "permisionario": variablepermisionario,
                     "marca": item.marca, "ruta_foto": item.ruta_foto,
                     "estatus": variablesobrepoliza, "nombre_empresa": item.app_name,
                     "nombre_linea": nl, "observaciones": item.observaciones,
                     "id_tipounidad": variabletipotransporte, "eco1": item.eco1, "eco2": item.eco2,
                     "concesion": item.concesion,
                     "id_tipotarjeta": variabletipotarjeta, "habilitado": varhabilitado,"id_grupo": variablegrupo})
            #print("LISTABUSCA", listabusca)
            contador = 0
            for item in listabusca:
                if item['habilitado'] == 'ACTIVA':
                    contador = contador + 1
            return dict(error='ok', listabusca=listabusca, list=list,contador=contador)
        else:
            contador = 0
            for item in listabusca:
                if item['habilitado'] == 'ACTIVA':
                    contador = contador + 1
            return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list,contador=contador)

    @expose('pythonjupiter.templates.UnitsTemplates.poliza')
    def polizas(self,**kw):
        perms='Edit Poliza,Delete Poliza'
        url = urlsun() + "/services/perms?perms=" + str(perms) + "&user=" + kw['user']
        r = requests.get(url, auth=('manager', 'managepass'))
        json_object = r.json()
        if json_object['error'] == "ok":
            perms = json_object['perms']
        else:
            perms = {}
        querypolizas=DBSession.query(Poliza).filter_by(id_unidad=kw['id']).all()
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        tarjetacirculacion=queryunidad.tarjetadecirculacion
        contador=1
        listapoliza=[]
        for itempoliza in querypolizas:
            if itempoliza.fecha_fin<=datetime.now().date():
                variableeditar=1
            else:
                variableeditar=0
            listapoliza.append({'contador' :contador,'archivo': itempoliza.archivo,'montototal': itempoliza.montototal,
                                'periodo': itempoliza.periodo,'montodividido': itempoliza.montodividido,'id_poliza': itempoliza.id_poliza,'fecha_inicio': itempoliza.fecha_inicio,
                                'fecha_fin': itempoliza.fecha_fin,'variableeditar': variableeditar})
            contador=contador+1
        return dict(page='polizas',listapoliza=listapoliza,app_name=kw['app_name'],internal_id=kw['internal_id'],user=kw['user'],
                    application_id=kw['application_id'],idunidad=kw['id'],tarjetacirculacion=tarjetacirculacion,perms=perms)

    @expose('pythonjupiter.templates.UnitsTemplates.newpoliza')
    def templatenewpoliza(self,**kw):
        queryexistepolizavigente = DBSession.query(Poliza).filter(Poliza.fecha_fin >= datetime.now().date()).filter_by(id_unidad=kw['id']).first()
        if queryexistepolizavigente is None:
            puedes=1
        else:
            puedes=0
        return dict(page='templatenewpoliza',internal_id=kw['internal_id'],application_id=kw['application_id'],app_name=kw['app_name']
                    ,user=kw['user'],idunidad=kw['id'],puedes=puedes,queryexistepolizavigente=queryexistepolizavigente)

    @expose('json')
    def calculofechafinpoliza(self,**kw):
        fechafin=kw['date']
        try:
            auxfechafin=datetime.strptime(fechafin, '%Y-%m-%d')
        except ValueError:
            return dict(error='Valueerror')

        if kw['periodo']=='':
            return dict(error='noperiodo')
        else:
            try:
                if kw['periodo']=='anual':
                    fechafin2=auxfechafin+timedelta(days=365)
                elif kw['periodo']=='semestral':
                    fechafin2 = auxfechafin + timedelta(days=183)
                elif kw['periodo']=='mensual':
                    fechafin2 = auxfechafin + timedelta(days=30)
            except:
                return dict(error='Valueerror')


            return dict(error='ok',fechafin=fechafin2.date())

    @expose('json')
    def savenewpoliza(self,**kw):
        # {'internal_id': '5', 'id_unidad': '8', 'jupiter_montototalpoliza': '8', 'jupiter_selectperiodpoliza': 'semestral', 'jupiter_filepoliza': FieldStorage('jupiter_filepoliza', 'CARRO5.jpg'),
        # 'jupiter_montocalculadopoliza': ''}
        internal_id=kw['internal_id']
        file = request.POST['jupiter_filepoliza']
        filename=request.params["jupiter_filepoliza"].filename
        idunidad=kw['id_unidad']
        querycuentacargo = DBSession.query(CuentaCargo).filter_by(application_id=kw['application_id']).filter_by(nombre='POLIZA').first()
        if querycuentacargo is None:
            newcuentacargo = CuentaCargo()
            newcuentacargo.nombre = 'POLIZA'
            newcuentacargo.total = 0
            newcuentacargo.internal_id = kw['internal_id']
            newcuentacargo.app_name = ''
            DBSession.add(newcuentacargo)
        else:
            pass


        #queryexistepoliza=DBSession.query(Poliza).filter_by(archivo=filename).filter_by(montototal=kw['jupiter_montototalpoliza']).filter_by(periodo=kw['jupiter_selectperiodpoliza']).\
        #                            filter_by(montodividido=kw['jupiter_montocalculadopoliza']).filter_by(fecha_fin=kw['jupiter_fechafinpoliza'])
        queryexistepolizavigente=DBSession.query(Poliza).filter(Poliza.fecha_fin>=datetime.now().date()).filter_by(id_unidad=kw['id_unidad']).first()
        if queryexistepolizavigente is None:
            newpoliza=Poliza()

            if file != b'' and file!='':
                app_dir = os.getenv('JUPITER_DIR')
                if app_dir is None:
                    app_dir = os.getcwd()
                ruta = app_dir + os.sep

                destino = ruta + "pythonjupiter/public/img/"+internal_id+"/polizas/"+str(idunidad)+".-"
                #print(destino)
                try:
                    os.makedirs(destino)
                except FileExistsError:
                    pass
                destino = ruta + "pythonjupiter/public/img/"+internal_id+"/polizas/"+ str(idunidad) + ".-/" + file.filename.lstrip(os.sep)
                with open(destino, 'wb') as fdestino:
                    copyfileobj(file.file, fdestino)
                    print("Archivo copiado")
                fdestino.close()
                #file.close()

                (this_file_name, this_file_extension) = os.path.splitext(filename)
                if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension=='.pdf'):
                    #print(ruta + "pythonjupiter/public/img/"+str(idunidad)+".-/"+ filename)
                    f = open(ruta+"pythonjupiter/public/img/"+internal_id+"/polizas/"+ str(idunidad)+".-/"+ filename, 'rb').read()
                    newpoliza.archivofile=f
            newpoliza.archivo=filename
            newpoliza.montototal=kw['jupiter_montototalpoliza']
            newpoliza.periodo=kw['jupiter_selectperiodpoliza']
            newpoliza.montodividido=kw['jupiter_montocalculadopoliza']
            newpoliza.fecha_inicio=kw['jupiter_fechainiciopoliza']
            newpoliza.fecha_fin=kw['jupiter_fechafinpoliza']
            newpoliza.id_unidad=kw['id_unidad']
            newpoliza.internal_id=internal_id
            DBSession.add(newpoliza)

            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "POLIZA subida con Exito")
            return dict(error='ok')
        else:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "La unidad ya cuenta con una poliza vigente")


    @expose('json')
    def editarpoliza(self,**kw):
        querypoliza=DBSession.query(Poliza).filter_by(id_poliza=kw['id']).first()
        idpoliza=querypoliza.id_poliza
        idunidad=querypoliza.id_unidad
        periodo=querypoliza.periodo
        montodividido=querypoliza.montodividido
        fecha_inicio=querypoliza.fecha_inicio
        fecha_fin=querypoliza.fecha_fin
        montototal=querypoliza.montototal
        mydict=dict(querypoliza=querypoliza,idunidad=idunidad,user=kw['user'],internal_id=kw['internal_id'],periodo=periodo,montodividido=montodividido,
                    fecha_inicio=fecha_inicio,fecha_fin=fecha_fin,montototal=montototal,id_poliza=idpoliza)
        template=render_template(mydict,'mako','pythonjupiter.templates.UnitsTemplates.editpoliza')
        return dict(template=template)

    @expose('json')
    def saveeditpoliza(self,**kw):
        #print(kw)
        id=kw['id_unidad']
        querypoliza=DBSession.query(Poliza).filter_by(id_poliza=kw['id_poliza']).first()
        querypoliza.montototal=kw['jupiter_montototalpoliza2']
        querypoliza.periodo=kw['jupiter_selectperiodpoliza2']
        querypoliza.montodividido=kw['jupiter_montocalculadopoliza2']
        querypoliza.fecha_inicio=kw['jupiter_fechainiciopoliza2']
        querypoliza.fecha_fin=kw['jupiter_fechafinpoliza2']
        file = request.POST['jupiter_filepoliza2']
        antes=querypoliza.archivo
        try:
            filename = request.params["jupiter_filepoliza2"].filename
        except:
            filename=''
        if file!=b'':
            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/polizas/" + str(id) + ".-"
            # print(destino)
            destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/polizas/" + str(id) + ".-/" + file.filename.lstrip(os.sep)
            with open(destino, 'wb') as fdestino:
                copyfileobj(file.file, fdestino)
                print("Archivo copiado")
            fdestino.close()
            # file.close()

            (this_file_name, this_file_extension) = os.path.splitext(filename)
            if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg' or this_file_extension == '.pdf'):

                f = open(ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/polizas/" + str(id) + ".-/" + filename,
                         'rb').read()
                try:
                    f2 = open(ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/polizas/" + str(id) + ".-/" + antes,'rb').read()
                    ok = 1
                except FileNotFoundError:
                    ok = 0
                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "filenotfound")
                    return ''

                if (f != f2):
                    if ok == 1:
                        os.remove(
                            ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/polizas/" + str(id) + ".-/" + antes)
                        print("DISTINTOS F Y F2")
                        querypoliza.archivo=filename
                        #return 'distintos'
                else:
                    print("IGUALES F Y F2")
                    #return 'iguales'
            print("NO VACIO")

        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "POLIZA editada con Exito")
        return ''

    @expose('json')
    def eliminarpoliza(self,**kw):
        querypoliza=DBSession.query(Poliza).filter_by(id_poliza=kw['id']).first()
        if querypoliza is not None:
            if querypoliza.fecha_fin < datetime.now().date():
                DBSession.delete(querypoliza)
                return dict(error='Eliminado',tipo=1)
            else:
                return dict(error="No se puede eliminar una poliza vigente",tipo=2)
        else:
            return dict(error="Ya no existe la poliza",tipo=3)


    @expose('json')
    def archivopoliza(self,**kw):
        querypoliza=DBSession.query(Poliza).filter_by(id_poliza=kw['id']).first()
        kw['archivo']=querypoliza.archivo
        template=render_template(kw,'mako','pythonjupiter.templates.UnitsTemplates.archivopoliza')
        return dict(template=template)

    @expose('json')
    def tarjetadecirculacion(self,**kw):
        template=render_template(kw,'mako','pythonjupiter.templates.UnitsTemplates.archivotarjeta')
        return dict(template=template)

    @expose('json')
    def unidadestsv(self,**kw):
        file = request.POST['file-input']
        if file != '':
            app_dir = os.getenv('JUPITER_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            destino = ruta + "pythonjupiter/public/"+file.filename.lstrip(os.sep)
            #print(destino)
            with open(destino, 'wb') as fdestino:
                copyfileobj(file.file, fdestino)
                print("Archivo copiado")

            name=file.filename.split(".")[0]
            queryempresa=DBSession.query(Linea).filter_by(nombre_linea=name).filter_by(app_id=kw['application_id']).first()
            if queryempresa is None:
                Message.post("testlistener_" + kw['user'],'MSG_' + 'YELLOW' + "|" + "La empresa no existe")
            else:
                with open(destino, encoding='utf-8', errors='ignore') as f:
                    i=0
                    for item in f.readlines():
                        if i==0:
                            i=i+1
                        else:
                            it = item.split('\t')
                            querypermisionario=DBSession.query(Persona).filter_by(espermisionario=1).filter_by(application_id=kw['application_id']).filter_by(nombre=it[3]).first()
                            if querypermisionario is not None:
                                idpermisionario=querypermisionario.id_persona
                                queryunidad=DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(eco1=it[2]).filter_by(habilitado=1).first()
                                if queryunidad is None:
                                    newunidad=DatosUnidad()
                                    newunidad.eco1 = it[2]
                                    newunidad.eco2=it[2]
                                    newunidad.marca=it[4]
                                    newunidad.modelo=it[6]
                                    newunidad.vin=it[7]
                                    newunidad.numero_motor=it[8]
                                    newunidad.placas=it[9]
                                    newunidad.app_name=kw['app_name']
                                    newunidad.estatus='ALTA'
                                    newunidad.internal_id=kw['internal_id']
                                    newunidad.app_id=kw['application_id']
                                    newunidad.esunidadtransportes='SI'
                                    newunidad.habilitado=1
                                    newunidad.id_linea=queryempresa.id_linea
                                    newunidad.id_persona=querypermisionario.id_persona
                                    DBSession.add(newunidad)
                                else:
                                    pass

                            else:
                                newpersona=Persona()
                                newpersona.nombre=it[3]
                                newpersona.espermisionario=1
                                newpersona.habilitadopermisionario=1
                                newpersona.internal_id=kw['internal_id']
                                newpersona.estatus='ALTA'
                                DBSession.add(newpersona)
                                queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(eco1=it[2]).filter_by(habilitado=1).first()
                                if queryunidad is None:
                                    newunidad=DatosUnidad()
                                    newunidad.eco1 = it[2]
                                    newunidad.eco2=it[2]
                                    newunidad.marca=it[4]
                                    newunidad.modelo=it[6]
                                    newunidad.vin=it[7]
                                    newunidad.numero_motor=it[8]
                                    newunidad.placas=it[9]
                                    newunidad.app_name=kw['app_name']
                                    newunidad.estatus='ALTA'
                                    newunidad.internal_id=kw['internal_id']
                                    newunidad.app_id=kw['application_id']
                                    newunidad.esunidadtransportes='SI'
                                    newunidad.habilitado=1
                                    newunidad.id_linea=queryempresa.id_linea
                                    querypermisionario = DBSession.query(Persona).filter_by(
                                        espermisionario=1).filter_by(internal_id=kw['internal_id']).filter_by(
                                        nombre=it[3]).first()
                                    newunidad.id_persona=querypermisionario.id_persona
                                    DBSession.add(newunidad)
                                else:
                                    pass

        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Archivo Subido con exito")
        return dict(error="")


    @expose('pythonjupiter.templates.UnitsTemplates.credencial')
    def generarcredencial(self,**kw):
        unidad=DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        empresa=DBSession.query(Linea).filter_by(id_linea=unidad.id_linea).first()
        try:
            laempresa=empresa.nombre_linea
        except:
            laempresa=''

        sobreque = str(unidad.id_unidad)+"-Eco:"+str(unidad.eco1)
        qr = qrcode.QRCode(box_size=40)
        qr.add_data(sobreque)
        # qr.make(fit=True)
        img = qr.make_image()
        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/codigosqr/"
        try:
            os.mkdir(destino)
        except:
            pass
        img.save(destino + "qr_" + str(unidad.id_unidad) + ".jpg")
        laimagen = destino + "qr_" + str(unidad.id_unidad) + ".jpg"
        with open(laimagen, "rb") as image_file:
            encoded_stringqr = base64.b64encode(image_file.read())


        permi=DBSession.query(Persona).filter_by(id_persona=unidad.id_persona).first()
        return dict(page="Pagina",internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],application_id=kw['application_id'],unidad=unidad,empresa=laempresa,
                    codigoqr=encoded_stringqr.decode("utf-8"),permi=permi.nombre)


    @expose('json')
    def getqr(self,**kw):
        unidad = DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        if unidad.codigoqr == None:
            sobreque = str(unidad.eco1)
            qr = qrcode.QRCode(box_size=30, image_factory=qrcode.image.svg.SvgPathImage)
            qr.add_data(sobreque)
            img = qr.make_image()
            buf = BytesIO()
            img.save(buf)
            image_stream = buf.getvalue()
            unidad.codigoqr = image_stream
        else:
            image_stream = unidad.codigoqr

        return dict(codigoqr=image_stream.decode("UTF-8"))

    @expose('json')
    def credencialpdf(self,**kw):
        unidad=DBSession.query(DatosUnidad).filter_by(id_unidad=kw['id']).first()
        empresa=DBSession.query(Linea).filter_by(id_linea=unidad.id_linea).first()
        try:
            laempresa=empresa.nombre_linea
        except:
            laempresa=''

        sobreque = str(unidad.eco1)
        qr = qrcode.QRCode(box_size=40)
        qr.add_data(sobreque)
        # qr.make(fit=True)
        img = qr.make_image()
        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/codigosqr/"
        try:
            os.mkdir(destino)
        except:
            pass
        img.save(destino + "qr_" + str(unidad.id_unidad) + ".jpg")
        laimagen = destino + "qr_" + str(unidad.id_unidad) + ".jpg"
        with open(laimagen, "rb") as image_file:
            encoded_stringqr = base64.b64encode(image_file.read())

        app_dir = os.getenv('JUPITER_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonjupiter/public/img/" + kw['internal_id'] + "/unidades/" + str(unidad.id_unidad)+".-/"
        imagen = destino + str(unidad.ruta_foto)
        # with open(imagen, 'r') as fdestino:
        # print(imagen)
        try:
            with open(imagen, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
        except:
            encoded_string = ""

        # print(encoded_string.decode("utf-8"))

        fondocredencial = ruta + "pythonjupiter/public/img/fondocredencial3.jpg"
        with open(fondocredencial, "rb") as image_file2:
            elfondocredencial = base64.b64encode(image_file2.read())

        siluetapersona = ruta + "pythonjupiter/public/img/carrosilueta.png"
        with open(siluetapersona, "rb") as image_file2:
            lasilueta = base64.b64encode(image_file2.read())

        try:
            encoded_string = encoded_string.decode("utf-8")
        except:
            encoded_string = ''

        permi = DBSession.query(Persona).filter_by(id_persona=unidad.id_persona).first()

        file_name = ExportPDF.create(dict(i=51,internal_id=kw['internal_id'],permi=permi.nombre,app_name=kw['app_name'],user=kw['user'],application_id=kw['application_id'],unidad=unidad,empresa=laempresa,
                    codigoqr=encoded_stringqr.decode("utf-8"),elfondocredencial=elfondocredencial.decode("utf-8"),unidadstring=encoded_string,lasilueta=lasilueta.decode("utf-8")),'Unidad_'+unidad.eco1, 'pythonjupiter.templates.UnitsTemplates.credencial2',
                'prueba')
        return dict(id=kw['id'],file_name=file_name)

    @expose('json')
    def g(self,**kw):

        return dict(value=90)