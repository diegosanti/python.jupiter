from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora,HistoricoUnidad,GrupoPersona_Persona
from pythonjupiter.model.tables import TipoLicencia,Persona,Punto,HistorialPersona,CargoOperador,OperadorCargo,GrupoPersona,Correos,Head
import os
import requests
from tg import app_globals
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.error import ErrorController
from shutil import copyfileobj,copyfile
from datetime import datetime,timedelta
from tg import render_template
from pythonjupiter.lib.messagequeue import Message
from pythonjupiter.lib.helpers import urlsun
import time
import qrcode
import qrcode.image.svg
from io import BytesIO
from pythonjupiter.lib.utility import ExportPDF

class QRCodeTemplates(BaseController):

    @expose('pythonjupiter.templates.QRCode.qr')
    def qrcode(self,**kw):
        operadores=DBSession.query(Persona).filter_by(esoperador=1).filter_by(internal_id=kw['internal_id']).filter_by(habilitadooperador=1).all()
        if kw['app_name']=='TODO':
            unidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).all()
        else:
            unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).filter_by(app_name=kw['app_name']).all()


        return dict(page='qr',app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],operadores=operadores,unidades=unidades)

    @expose('json')
    def getQRGraphs(self, **kw):
        por=kw['por']
        size=kw['size']
        msg=kw['msg']
        if por=='Unidad':
            queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=msg).first()
            if queryunidad.codigoqr==None:
                sobreque=queryunidad.eco1
                qr = qrcode.QRCode(box_size=int(size), image_factory=qrcode.image.svg.SvgPathImage)
                qr.add_data(sobreque)
                img = qr.make_image()
                buf = BytesIO()
                img.save(buf)
                image_stream = buf.getvalue()
                queryunidad.codigoqr=image_stream
            else:
                image_stream=queryunidad.codigoqr
            dee=queryunidad.eco1
        else:
            queryoperador=DBSession.query(Persona).filter_by(id_persona=msg).first()
            if queryoperador.codigoqr==None:
                sobreque = str(queryoperador.numerooperador)+'.-'+queryoperador.nombre
                qr = qrcode.QRCode(box_size=int(size), image_factory=qrcode.image.svg.SvgPathImage)
                qr.add_data(sobreque)
                img = qr.make_image()
                buf = BytesIO()
                img.save(buf)
                image_stream = buf.getvalue()
                queryoperador.codigoqr=image_stream
            else:
                image_stream = queryoperador.codigoqr

            dee = str(queryoperador.numerooperador)+'.-'+queryoperador.nombre

        file_name=ExportPDF.create({'graph_data':image_stream.decode("utf-8")},'CodigoQR'+dee,'pythonjupiter.templates.QRCode.elqr','pro')
        template=render_template({'graph_data':image_stream.decode("utf-8")},'mako','pythonjupiter.templates.QRCode.elqr')
        return dict(template=template,file_name=file_name,graph_data=image_stream.decode("utf-8"))