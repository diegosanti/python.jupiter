from tg import RestController
from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import Pets
import base64
import os
__all__ = ['PetsController']


class PetsController(RestController):

    @expose('json')
    def get_all(self):
        pets=[]
        allrecords = DBSession.query(Pets).all()
        for item in allrecords:
            pets.append(dict(id=item.id,name=item.name,breed=item.breed))

        return dict(pets=pets)


    @expose('json')
    def put(self, name,breed,id):
        query = DBSession.query(Pets).filter_by(id=int(id)).first()
        if query is not None:
            query.name = name
            query.breed = breed
            DBSession.flush()
        print("PUT Name: {} Breed: {} id:".format(name,breed,id))
        return dict(name=name,breed=breed,id=id)

    @expose('json')
    def post(self, name,breed,foto):
        newitem = Pets()
        newitem.name = name
        newitem.breed = breed
        newitem.foto=base64.decodebytes(bytes(foto,'utf-8'))
        DBSession.add(newitem)
        DBSession.flush()
        return dict(name=name, breed=breed,id=newitem.id,foto=foto)

    @expose('json')
    def post_delete(self, record):
        query = DBSession.query(Pets).filter_by(id=int(record)).first()
        if query is not None:
            DBSession.delete(query)
            DBSession.flush()
        print("DELETE Id: {} ".format(record))
        return dict(id=record)



    @expose('pythonjupiter.templates.tests.imagenmovil')
    def imagenmovil(self):
        query=DBSession.query(Pets).all()
        list=[]
        for every_picture in query:
            imagen=base64.b64encode(every_picture.foto)
            list.append(imagen.decode("utf-8"))
        return dict(list=list)
