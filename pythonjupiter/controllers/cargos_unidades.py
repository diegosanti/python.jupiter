from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora,Grupo,Persona,CargoUnidad,UnidadCargo
from pythonjupiter.model.tables import CargoOperador,CuentaCargo,Grupo_DatosUnidad
import os
import requests
from tg import app_globals
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.error import ErrorController
from shutil import copyfileobj,copyfile
from datetime import datetime
from pythonjupiter.lib.messagequeue import Message
from tg import render_template
from sqlalchemy import or_


class CargosTemplates(BaseController):
    bandera=0

    @expose('pythonjupiter.templates.CargosTemplates.cargosunidades')
    def cargo_unidades(self,**kw):
        if kw['app_name']=='TODO':
            allrecords=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).all()
            contador=len(allrecords)
        else:
            allrecords=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter(or_(CargoUnidad.app_name==kw['app_name'],CargoUnidad.app_name=='TODO')).all()
            contador=len(allrecords)
        return dict(page='cargosunidades',app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],
                    allrecords=allrecords,contador=contador)

    @expose('pythonjupiter.templates.CargosTemplates.nuevocargounidad')
    def nuevocargo(self,**kw):
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = []

        newempresa=[]

        #TODOporveri .------BUENA OPCION EL TODO??-----
        if kw['app_name']!='TODOporveri':
            for item in empresa:
                if item['application_name']==kw['app_name']:
                    newempresa.append({'application_name': item['application_name']})
        else:
            for item in empresa:
                if item['application_name'] != 'TODO':
                    newempresa.append({'application_name': item['application_name']})

            for item in empresa:
                if item['application_name'] == 'TODO':
                    newempresa.append({'application_name': item['application_name']})



        #print("NEWEMPRESA===>",newempresa)
        allempresas = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        #print("newempresa[0]",newempresa)
        verificar=newempresa
        #print("verificar=",verificar)
        listaverificar=[]
        for itemverificar in verificar:
            if itemverificar['application_name']!='TODO':
                listaverificar.append(itemverificar['application_name'])
            else:
                listaverificar.append(itemverificar['application_name'])

        #print("LISTAVERIFICAR==>",listaverificar)
        try:

            if listaverificar[0]!='TODO':
                allderroteros=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).filter_by(app_name=listaverificar[0]).all()
            else:
                allderroteros = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).filter_by(app_name=listaverificar[1]).all()

            if listaverificar[0]!='TODO':
                allgrupos=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(app_name=listaverificar[0]).all()
            else:
                allgrupos = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(app_name=listaverificar[1]).all()

            print("TRY")
            #print("allempresas====>", allempresas)
            #print("ALDERROETROS====>", allderroteros)
            #print("allgrupos====>", allgrupos)
        except:
            allderroteros = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
            allgrupos = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()
            print("except")
            #print("allempresas====>", allempresas)
            #print("ALDERROETROS====>",allderroteros)
            #print("allgrupos====>", allgrupos)
        return dict(page='nuevocargounidad',app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],
                    empresa=newempresa,allempresas=allempresas,allderroteros=allderroteros,allgrupos=allgrupos)
    @expose('json')
    def uploadCargoUnidad(self,**kw):
        print("entre")
        #print(kw)
        try:
            diasseleccion=kw['diasseleccion']
            tipocargo=kw['jupiter_tipocargo']
        except KeyError:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "Elige por lo menos un dia!")
            return ''

        queryexiste=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()
        if queryexiste is None:
            print("NO EXISTE")
            querycuentacargo=DBSession.query(CuentaCargo).filter_by(nombre=kw['jupiter_chargename']).filter_by(application_id=kw['application_id']).first()
            newcuentacargo = CuentaCargo()
            if querycuentacargo is None:
                newcuentacargo.nombre=kw['jupiter_chargename']
                newcuentacargo.total=0
                newcuentacargo.internal_id=kw['internal_id']
                newcuentacargo.application_id=kw['application_id']
                newcuentacargo.app_name = kw['jupiter_selectapp_nameforcharges']

            else:
                Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "La bolsita ya existe")
                return ''
            newcargo=CargoUnidad()
            newcargo.cargo=kw['jupiter_chargename']
            newcargo.montodefault=kw['jupiter_amountdefault']
            newcargo.app_name=kw['jupiter_selectapp_nameforcharges']
            newcargo.internal_id=kw['internal_id']
            newcargo.application_id = kw['application_id']
            print("PRIMERA SECCION")
            #verificar el TODOf
            if kw['jupiter_selectapp_nameforcharges']=='TODOporverific':
                newcargo.empresa='TODAS'
                newcargo.derrotero='TODAS'
                newcargo.grupo='TODAS'
            else:
                noseleccionado=0
                listaseleccion=[]

                if kw['companiescheck']== 'true':
                    listaseleccion.append('company')
                else:
                    noseleccionado=noseleccionado+1

                if kw['derroteroscheck'] == 'true':
                    listaseleccion.append('derrotero')
                else:
                    noseleccionado=noseleccionado+1

                if kw['gruposcheck'] == 'true':
                    listaseleccion.append('group')
                else:
                    noseleccionado=noseleccionado+1

                if noseleccionado==3:
                    newcargo.empresa = 'TODAS'
                    newcargo.derrotero = 'TODAS'
                    newcargo.grupo = 'TODAS'
                else:
                    variablecompany=''
                    variablederrotero=''
                    variablegrupo=''
                    if 'company' in listaseleccion:
                        variablecompany=kw['jupiter_selectcompaniesforcharges']
                        newcargo.empresa=variablecompany
                        newcargo.derrotero=variablederrotero
                        newcargo.grupo=variablegrupo

                    if 'derrotero' in listaseleccion:
                        variablederrotero=kw['jupiter_selectderroterosforcharges']
                        newcargo.derrotero=variablederrotero
                        newcargo.empresa=variablecompany
                        newcargo.grupo=variablegrupo

                    if 'group' in listaseleccion:
                        variablegrupo=kw['jupiter_selectgruposforcharges']
                        newcargo.grupo=variablegrupo
                        newcargo.empresa=variablecompany
                        newcargo.derrotero=variablederrotero

            if int(kw['jupiter_tipocargo'])==1 or int(kw['jupiter_tipocargo'])==2 or int(kw['jupiter_tipocargo'])==3:
                anexo=''
                if kw['jupiter_chargeunidadallweek']=='true':
                    newcargo.frecuencia='Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo'

                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Insertado")
                else:
                    anexo=''

                    if kw['jupiter_chargeunidadlunes']=='true':
                        anexo='Lunes,'
                    else:
                        anexo=''


                    if kw['jupiter_chargeunidadmartes']=='true':
                        anexo=anexo+'Martes,'
                    else:
                        anexo=anexo

                    if kw['jupiter_chargeunidadmiercoles']=='true':
                        anexo=anexo+'Miercoles,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadjueves']=='true':
                        anexo=anexo+'Jueves,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadviernes']=='true':
                        anexo=anexo+'Viernes,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadsabado']=='true':
                        anexo=anexo+'Sabado,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidaddomingo']=='true':
                        anexo=anexo+'Domingo'
                    else:
                        anexo=anexo



                    newcargo.frecuencia=anexo
            else:
                pass
                    #newcargo.frecuencia='Por dia Trabajado'
            newcargo.tipodecargo=kw['jupiter_tipocargo']
            DBSession.add(newcargo)
            querynuevocargo=DBSession.query(CargoUnidad).filter_by(cargo=kw['jupiter_chargename']).filter_by(application_id=kw['application_id']).first()
            newcuentacargo.cargounidad_id=querynuevocargo.id_cargounidad
            DBSession.add(newcuentacargo)
            DBSession.flush()

            if kw['permiso']=='1':
                querycargonuevo=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()


                #verificareltodo
                if kw['jupiter_selectapp_nameforcharges']=='TODOverificarsobre':
                    queryunidades = DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).all()
                    for item in queryunidades:
                        queryexisteunidadcargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargonuevo.id_cargounidad)\
                            .filter_by(unidad_id=item.id_unidad).first()
                        if queryexisteunidadcargo is None:
                            newunidadcargo = UnidadCargo()
                            querycontador = DBSession.query(UnidadCargo).all()
                            contador=0
                            for item2 in querycontador:
                                contador = int(item2.id)
                            contador=contador+1
                            newunidadcargo.id = contador
                            newunidadcargo.cargo_id =querycargonuevo.id_cargounidad
                            newunidadcargo.unidad_id = item.id_unidad
                            newunidadcargo.monto =kw['jupiter_amountdefault']
                            DBSession.add(newunidadcargo)

                else:
                    noseleccionado = 0
                    listaseleccion = []

                    if kw['companiescheck'] == 'true':
                        listaseleccion.append('company')
                    else:
                        noseleccionado = noseleccionado + 1

                    if kw['derroteroscheck'] == 'true':
                        listaseleccion.append('derrotero')
                    else:
                        noseleccionado = noseleccionado + 1

                    if kw['gruposcheck'] == 'true':
                        listaseleccion.append('group')
                    else:
                        noseleccionado = noseleccionado + 1

                    if noseleccionado == 3:
                        variableempresacargo = 'TODAS'
                        variablederroterocargo = 'TODAS'
                        variablegrupocargo = 'TODAS'
                    else:
                        variablecompany = ''
                        variablederrotero = ''
                        variablegrupo = ''
                        if 'company' in listaseleccion:
                            variablecompany = kw['jupiter_selectcompaniesforcharges']
                            variableempresacargo = variablecompany
                            variablederroterocargo = variablederrotero
                            variablegrupocargo = variablegrupo
                        if 'derrotero' in listaseleccion:
                            variablederrotero = kw['jupiter_selectderroterosforcharges']
                            variableempresacargo = variablecompany
                            variablederroterocargo = variablederrotero
                            variablegrupocargo = variablegrupo
                        if 'group' in listaseleccion:
                            variablegrupo = kw['jupiter_selectgruposforcharges']
                            variableempresacargo = variablecompany
                            variablederroterocargo = variablederrotero
                            variablegrupocargo = variablegrupo



                    if variableempresacargo=='TODAS':
                        print("all")
                        querylinea=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
                    elif variableempresacargo=='':
                        print("vacio")
                        querylinea={}
                    else:
                        print("nombre_linea")
                        querylinea = DBSession.query(Linea).filter_by(app_id=kw['application_id']).filter_by(nombre_linea=variableempresacargo).all()


                    for item in querylinea:
                        queryunidades=DBSession.query(DatosUnidad).filter_by(id_linea=item.id_linea).all()

                        for item2 in queryunidades:
                            queryexisteunidadcargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargonuevo.id_cargounidad)\
                                .filter_by(unidad_id=item2.id_unidad).first()



                            if queryexisteunidadcargo is None:
                                print("ES NONE E INSERTARE EN UNIDADCARGO")
                                newunidadcargo = UnidadCargo()
                                querycontador = DBSession.query(UnidadCargo).all()
                                contador = 0
                                for item in querycontador:
                                    contador = item.id
                                contador = contador + 1
                                newunidadcargo.id = contador
                                newunidadcargo.cargo_id = querycargonuevo.id_cargounidad
                                newunidadcargo.unidad_id = item2.id_unidad
                                newunidadcargo.monto = kw['jupiter_amountdefault']
                                DBSession.add(newunidadcargo)

                    if variablederroterocargo=='TODAS':
                        queryderrotero=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()
                    elif variablederroterocargo=='':
                        queryderrotero={}
                    else:
                        queryderrotero = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).filter_by(derrotero=variablederroterocargo).all()

                    for item in queryderrotero:
                        queryunidades=DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(id_derrotero=item.id_derrotero).all()
                        for item2 in queryunidades:
                            queryexisteunidadcargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargonuevo.id_cargounidad)\
                                .filter_by(unidad_id=item2.id_unidad).first()

                            if queryexisteunidadcargo is None:
                                newunidadcargo = UnidadCargo()
                                querycontador = DBSession.query(UnidadCargo).all()
                                contador = 0
                                for item in querycontador:
                                    contador = item.id
                                contador = contador + 1
                                newunidadcargo.id = contador
                                newunidadcargo.cargo_id = querycargonuevo.id_cargounidad
                                newunidadcargo.unidad_id = item2.id_unidad
                                newunidadcargo.monto = kw['jupiter_amountdefault']
                                DBSession.add(newunidadcargo)


                    print('variable grupo cargo: ',variablegrupocargo)
                    if variablegrupocargo=='TODAS':
                        querygrupo=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

                    elif variablegrupocargo== '':
                        querygrupo={}
                    else:
                        querygrupo = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(grupo=variablegrupocargo).all()

                    for item in querygrupo:
                        querytodoslosgrupos=DBSession.query(Grupo_DatosUnidad).filter_by(grupo_id=item.id_grupo).all()
                        for itemdegrupo in querytodoslosgrupos:
                            queryunidades=DBSession.query(DatosUnidad).filter_by(id_unidad=itemdegrupo.datosunidad_id).all()
                            for item2 in queryunidades:
                                queryexisteunidadcargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargonuevo.id_cargounidad)\
                                    .filter_by(unidad_id = item2.id_unidad).first()

                                if queryexisteunidadcargo is None:
                                    newunidadcargo = UnidadCargo()
                                    querycontador = DBSession.query(UnidadCargo).all()
                                    contador = 0
                                    for item in querycontador:
                                        contador = item.id
                                    contador = contador + 1
                                    newunidadcargo.id = contador
                                    newunidadcargo.cargo_id = querycargonuevo.id_cargounidad
                                    newunidadcargo.unidad_id = item2.id_unidad
                                    newunidadcargo.monto = kw['jupiter_amountdefault']
                                    DBSession.add(newunidadcargo)


            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Insertado")


            #template=render_template({}, "mako", 'pythonjupiter.templates.CargosTemplates.botoncargartodos')

            return ''
        else:

            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "El cargo ya existe")
            return ''


    @expose('pythonjupiter.templates.CargosTemplates.editcargounidad')
    def editcargo(self,**kw):
        querytoedit=DBSession.query(CargoUnidad).filter_by(id_cargounidad=kw['id']).first()
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = []

        newempresa=[]
        for item in empresa:
            if item['application_name']==kw['app_name']:
                newempresa.append({'application_name': item['application_name']})


        allempresas = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()

        if querytoedit.app_name!='TODO':
            allderroteros=DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).filter_by(app_name=querytoedit.app_name).all()
        else:
            allderroteros = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']).all()

        if querytoedit.app_name!='TODO':
            allgrupos=DBSession.query(Grupo).filter_by(application_id=kw['application_id']).filter_by(app_name=querytoedit.app_name).all()
        else:
            allgrupos = DBSession.query(Grupo).filter_by(application_id=kw['application_id']).all()

        dias=querytoedit.frecuencia.split(',')
        contadordias=len(dias)
        print("CONTADOR DE DIAS: ",contadordias)
        #print("DIAS: ",dias)
        if contadordias==7:
            try:
                dias.remove('')
                contadordias = len(dias)
            except:
                pass
        #print("DIAS: ", dias)
        return dict(page='editcargounidad',user=kw['user'],app_name=kw['app_name'],application_id=kw['application_id'],internal_id=kw['internal_id'],
                    empresa=newempresa, allempresas=allempresas, allderroteros=allderroteros, allgrupos=allgrupos,querytoedit=querytoedit,dias=dias,contadordias=contadordias)


    @expose('json')
    def editamonto(self,**kw):
        monto=kw['monto']
        id=kw['id']
        querycargo=DBSession.query(CargoUnidad).filter_by(id_cargounidad=id).first()
        queryunidad_cargo=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargo.id_cargounidad).all()
        for item in queryunidad_cargo:
            if item.monto != int(monto):
                newhistorialunidad=HistorialUnidades()
                querycargo=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(id_cargounidad=item.cargo_id).first()
                newhistorialunidad.dato=querycargo.cargo+' monto'
                newhistorialunidad.valor_nuevo=monto
                newhistorialunidad.valor_anterior=item.monto
                queryunidad=DBSession.query(DatosUnidad).filter_by(app_id=kw['application_id']).filter_by(id_unidad=item.unidad_id).first()
                newhistorialunidad.sobrecampo=queryunidad.id_unidad
                newhistorialunidad.usuario=kw['user']
                item.monto = monto
                DBSession.add(newhistorialunidad)

        return dict(data='Monto Editado a todas')

    @expose('json')
    def editandoCargoUnidad(self,**kw):
        #print(kw)
        querynombrenuevo=DBSession.query(CargoUnidad).filter_by(cargo=kw['jupiter_chargename']).filter(CargoUnidad.id_cargounidad!=kw['id']).first()
        if querynombrenuevo is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "El Cargo ya existe")
            return ''

        queryexiste=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(id_cargounidad=kw['id']).first()

        queryexiste2 = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()
        if queryexiste2 is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "Cargo Existe en Operador")
            return ''
        if queryexiste is not None:
            #newcargo=CargoUnidad()
            querycuentacargo=DBSession.query(CuentaCargo).filter_by(application_id=kw['application_id']).filter_by(nombre=queryexiste.cargo).first()
            if querycuentacargo is not None:
                querycuentacargo.nombre=kw['jupiter_chargename']
            queryexiste.cargo=kw['jupiter_chargename']
            queryexiste.montodefault=kw['jupiter_amountdefault']
            queryexiste.app_name=kw['jupiter_selectapp_nameforcharges']
            queryexiste.internal_id=kw['internal_id']
            queryexiste.application_id = kw['application_id']
            queryexiste.tipodecargo=kw['tipocargounidad']
            if kw['jupiter_selectapp_nameforcharges']=='TODO':
                queryexiste.empresa='TODAS'
                queryexiste.derrotero='TODAS'
                queryexiste.grupo='TODAS'
            else:
                noseleccionado=0
                listaseleccion=[]
                try:
                    validando=kw['companiescheck']
                    listaseleccion.append('company')
                except:
                    noseleccionado=noseleccionado+1
                try:
                    validando=kw['derroteroscheck']
                    listaseleccion.append('derrotero')
                except:
                    noseleccionado=noseleccionado+1
                try:
                    validando=kw['gruposcheck']
                    listaseleccion.append('group')
                except:
                    noseleccionado=noseleccionado+1
                if noseleccionado==3:
                    queryexiste.empresa = 'TODAS'
                    queryexiste.derrotero = 'TODAS'
                    queryexiste.grupo = 'TODAS'
                else:
                    variablecompany=''
                    variablederrotero=''
                    variablegrupo=''
                    if 'company' in listaseleccion:
                        variablecompany=kw['jupiter_selectcompaniesforcharges']
                        queryexiste.empresa=variablecompany
                        queryexiste.derrotero=variablederrotero
                        queryexiste.grupo=variablegrupo
                    if 'derrotero' in listaseleccion:
                        variablederrotero=kw['jupiter_selectderroterosforcharges']
                        queryexiste.empresa=variablecompany
                        queryexiste.derrotero=variablederrotero
                        queryexiste.grupo=variablegrupo
                    if 'group' in listaseleccion:
                        variablegrupo=kw['jupiter_selectgruposforcharges']
                        queryexiste.empresa=variablecompany
                        queryexiste.derrotero=variablederrotero
                        queryexiste.grupo=variablegrupo

            if int(kw['tipocargounidad'])==1 or int(kw['tipocargounidad'])==2 or int(kw['tipocargounidad'])==3:
                try:
                    anexo=kw['jupiter_chargeunidadallweek']
                    queryexiste.frecuencia='Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo'
                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Editado")

                except:
                    anexo=''

                    try:
                        veri=kw['jupiter_chargeunidadlunes']
                        anexo='Lunes,'
                    except:
                        anexo=''

                    try:
                        veri=kw['jupiter_chargeunidadmartes']
                        anexo=anexo+'Martes,'
                    except:
                        anexo=anexo


                    try:
                        veri=kw['jupiter_chargeunidadmiercoles']
                        anexo=anexo+'Miercoles,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadjueves']
                        anexo=anexo+'Jueves,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadviernes']
                        anexo=anexo+'Viernes,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadsabado']
                        anexo=anexo+'Sabado,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidaddomingo']
                        anexo=anexo+'Domingo'
                    except:
                        anexo=anexo


                #print("ANEXO: ",anexo)
                    queryexiste.frecuencia=anexo
            else:
                    pass

            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Editado")
        return ''



    @expose('json')
    def reloadcargosunidades(self,**kw):
        if kw['app_name']=='TODOveriiio':
            allrecords=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).all()
            contador=len(allrecords)
        else:
            allrecords=DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter(or_(CargoUnidad.app_name==kw['app_name'],CargoUnidad.app_name=='TODO')).all()
            contador=len(allrecords)
        return dict(listabusca=allrecords,contador=contador)



    @expose('json')
    def searchcargounidad(self,**kw):
        if kw['app_name']=='TODOverrio':
            listabusca=DBSession.query(CargoUnidad).filter(CargoUnidad.cargo.like('%'+kw['name']+'%')).filter_by(application_id=kw['application_id']).all()
            contador=len(listabusca)
        else:
            listabusca=DBSession.query(CargoUnidad).filter(CargoUnidad.cargo.like('%'+kw['name']+'%')).filter_by(application_id=kw['application_id']).filter(or_(CargoUnidad.app_name==kw['app_name'],CargoUnidad.app_name=='TODO')).all()
            contador=len(listabusca)

        return dict(listabusca=listabusca,contador=contador)


    @expose('json')
    def eliminacargounidad(self,**kw):

        queryeliminar = DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(id_cargounidad=kw['id']).first()
        print("queryeliminar=>>",queryeliminar)
        queryexisteenunidad=DBSession.query(UnidadCargo).filter_by(cargo_id=queryeliminar.id_cargounidad).first()
        print("queryexisteenunidad=>>", queryexisteenunidad)
        if queryeliminar is not None:
            if queryexisteenunidad is None:
                querycuentacargo=DBSession.query(CuentaCargo).filter_by(cargounidad_id=queryeliminar.id_cargounidad).filter_by(application_id=queryeliminar.application_id).first()
                print("querycuentacargo=>>>", querycuentacargo)
                if querycuentacargo is not None:
                    print("ELIMINARE CARGO CUENTA")
                    DBSession.delete(querycuentacargo)
                    DBSession.flush()
                    print("ELIMINE CARGO CUENTA")
                    DBSession.delete(queryeliminar)
                    DBSession.flush()



                    return dict(error='Eliminado exitosamente')
                else:
                    return dict(error='El Cargo no se puede eliminar')
            else:
                return dict(error='El Cargo esta asiganada a una unidad no se puede eliminar')
        else:
            return dict(error='No existe el id')



    @expose('json')
    def actualizacargosunidades(self,**kw):
        empresa=kw['empresa']
        app_name=kw['app_name']
        derrotero=kw['derrotero']
        grupo=kw['grupo']
        splitgrupo=grupo.split(",")
        splitgrupo.remove('')
        construiraux=[]
        construir = []

        for itemsplit in splitgrupo:
            cargogrupo = DBSession.query(CargoUnidad).filter_by(grupo=itemsplit).filter_by(application_id=kw['application_id']).all()
            for item in cargogrupo:
                if item.cargo in construiraux:
                    pass
                else:
                    #print(item.cargo)
                    construiraux.append(item.cargo)
                    construir.append({'cargo': item.cargo, 'monto': item.montodefault})


        cargosempresa=DBSession.query(CargoUnidad).filter_by(empresa=empresa).filter_by(app_name=app_name).filter_by(application_id=kw['application_id']).all()
        cargoderrotero=DBSession.query(CargoUnidad).filter_by(derrotero=derrotero).filter_by(app_name=app_name).filter_by(application_id=kw['application_id']).all()
        #cargogrupo=DBSession.query(CargoUnidad).filter_by(grupo=grupo).filter_by(app_name=app_name).all()

        empresatodas=DBSession.query(CargoUnidad).filter_by(empresa='TODAS').filter_by(app_name=app_name).filter_by(application_id=kw['application_id']).all()
        derroterotodas=DBSession.query(CargoUnidad).filter_by(derrotero='TODAS').filter_by(app_name=app_name).filter_by(application_id=kw['application_id']).all()
        grupotodas=DBSession.query(CargoUnidad).filter_by(grupo='TODAS').filter_by(app_name=app_name).filter_by(application_id=kw['application_id']).all()

        enapp_nametodo=DBSession.query(CargoUnidad).filter_by(app_name='TODO').filter_by(application_id=kw['application_id']).all()



        for item in cargosempresa:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in cargoderrotero:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})


        for item in empresatodas:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in derroterotodas:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in grupotodas:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})

        for item in enapp_nametodo:
            if item.cargo in construiraux:
                pass
            else:
                #print(item.cargo)
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo,'monto': item.montodefault})


        construir2=[]
        if 'id' in kw:
            #print("construir: ",construir)
            for elem in construir:
                querycargo=DBSession.query(CargoUnidad).filter_by(cargo=elem['cargo']).first()
                querycargos=DBSession.query(UnidadCargo).filter_by(cargo_id=querycargo.id_cargounidad).filter_by(unidad_id=kw['id']).first()
                if querycargos is not None:
                    construir2.append({'cargo': elem['cargo'],'monto': querycargos.monto})
                else:
                    construir2.append({'cargo': elem['cargo'], 'monto': 0})



        return dict(error='ok',construir=construir,construir2=construir2)






