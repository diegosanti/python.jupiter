#
#
# ##ESTE NO SE OCUPA TODO ESTA EN PERSONA
#
# from tg import expose, flash, require, url, lurl
# from tg import request, redirect, tmpl_context
# from tg.i18n import ugettext as _, lazy_ugettext as l_
# from tg.exceptions import HTTPFound
# from tg import predicates
# from pythonjupiter import model
# from pythonjupiter.controllers.secure import SecureController
# from pythonjupiter.model import DBSession
# from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
# from tgext.admin.controller import AdminController
# from pythonjupiter.lib.CRUD import Crud
# from pythonjupiter.lib.create import Create
# from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora
# from pythonjupiter.model.tables import TipoLicencia
# import os
# import requests
# from tg import app_globals
# from pythonjupiter.lib.base import BaseController
# from pythonjupiter.controllers.error import ErrorController
# from shutil import copyfileobj,copyfile
# from datetime import datetime
# from tg import render_template
#
#
# class OperadoresTemplates(BaseController):
#
#     def calculalicencia(self,item):
#         print(item.fecha_vencimiento)
#         compara=datetime.now()
#         if compara.year==item.fecha_vencimiento.year:
#             if (item.fecha_vencimiento.month - 1) <= compara.month:
#                 print("Vencera")
#                 return "VENCERA"
#         else:
#             if (item.fecha_vencimiento.year - 1) == compara.year and item.fecha_vencimiento.month == 1 and compara.month == 12:
#                 print("Vencera")
#                 return "VENCERA"
#
#         print("LEJOS")
#         return "LEJOS"
#
#     @expose('pythonjupiter.templates.OperadoresTemplates.operadores')
#     def operadores(self, **kw):
#         list = []
#         internal_id = kw['internal_id']
#
#         empresa = kw['app_name']
#         # laempresa=DBSession.query(Empresa).filter_by(nombre_empresa=empresa).first()
#
#         list = [{'show': 'id_operador'}, {'show': 'operador'}, {'show': 'apodo'}, {'show': 'foto'},{'show': 'id_linea'}, {'show': 'rfc'},
#                 {'show': 'estatus'}, {'show': 'telefono'}, {'show': 'id_licencia'},{'show': 'fecha_vencimiento'},{'show': 'estadodelicencia'},{'show': 'habilitado'},{'show': 'observaciones'}]
#
#         if internal_id == '0':
#             handler = DBSession.query(Operador).all()
#         else:
#             handler = DBSession.query(Operador).filter_by(internal_id=internal_id).all()
#         hd = []
#         for item in handler:
#             item.estadodelicencia=self.calculalicencia(item)
#             querytipolicencia = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=item.id_tipolicencia).first()
#             querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
#
#             varhabilitado=''
#
#             if (item.habilitado == 1):
#                 varhabilitado = "ACTIVA"
#             else:
#                 varhabilitado = "DESACTIVADA"
#
#             if querylinea is not None:
#                 variablelinea = querylinea.nombre_linea
#             else:
#                 variablelinea = ''
#
#             if querytipolicencia is not None:
#                 variablelicencia = querytipolicencia.tipo_licencia
#             else:
#                 variablelicencia = ''
#
#
#
#             hd.append({"id_operador": item.id_operador, "operador": item.operador, "apodo": item.apodo, "foto": item.foto,
#                        "id_linea": variablelinea, "rfc": item.rfc,
#                        "estatus": item.estatus, "telefono": item.telefono,"id_licencia": variablelicencia,
#                        "fecha_vencimiento": item.fecha_vencimiento,"estadodelicencia": item.estadodelicencia,"habilitado": varhabilitado,"observaciones": item.observaciones})
#
#         print("hd tiene: ",hd)
#         print("retornare")
#
#
#
#         return dict(page='operadores', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'],user=kw['user'], application_id=kw['application_id'])
#
#
#
#
#
#     @expose('pythonjupiter.templates.OperadoresTemplates.nuevaoperador')
#     def nuevaoperador(self, **kw):
#         typelicence = DBSession.query(TipoLicencia).filter_by(internal_id=kw['internal_id']).all()  # VER SI SE FILTRA POR
#         typelinea = DBSession.query(Linea).filter_by(internal_id=kw['internal_id']).all()
#
#         return dict(page='nuevaunidad', typelicence=typelicence, typelinea=typelinea, internal_id=kw['internal_id'],app_name=kw['app_name'], user=kw['user'], application_id=kw['application_id'])
#
#     @expose('json')
#     def uploadOperator(self,**kw):
#         print(kw)
#         file = request.POST['jupiter_fotooperador']
#         filelicence=request.POST['jupiteroperadores_licencearchivo']
#         filecurp=request.POST['jupiteroperadores_curp']
#         fileine=request.POST['jupiteroperadores_ine']
#         newitem = Operador()
#         # print(file)
#         if file != '' and filelicence!='' and filecurp!='' and fileine!='':
#             app_dir = os.getenv('JUPITER_DIR')
#             if app_dir is None:
#                 app_dir = os.getcwd()
#             ruta = app_dir + os.sep
#             contador=1
#             cualid=DBSession.query(Operador).all()
#             if cualid == []:
#                 contador=1
#             else:
#                 contador=len(cualid)+1
#             destino = ruta + "pythonjupiter/public/img/"+str(contador)+".-"
#             print(destino)
#             try:
#                 os.makedirs(destino)
#             except FileExistsError:
#                 pass
#             destino = ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+file.filename.lstrip(os.sep)
#             with open(destino, 'wb') as fdestino:
#                  copyfileobj(file.file, fdestino)
#                  print("Archivo copiado")
#             fdestino.close()
#             destino = ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+filelicence.filename.lstrip(os.sep)
#             with open(destino, 'wb') as fdestino:
#                  copyfileobj(filelicence.file, fdestino)
#                  print("Archivo copiado")
#             fdestino.close()
#             destino = ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+filecurp.filename.lstrip(os.sep)
#             with open(destino, 'wb') as fdestino:
#                  copyfileobj(filecurp.file, fdestino)
#                  print("Archivo copiado")
#             fdestino.close()
#             destino = ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+fileine.filename.lstrip(os.sep)
#             with open(destino, 'wb') as fdestino:
#                  copyfileobj(fileine.file, fdestino)
#                  print("Archivo copiado")
#             #     #
#             file.file.close()
#             filelicence.file.close()
#             filecurp.file.close()
#             fileine.file.close()
#
#             filename = request.params["jupiter_fotooperador"].filename
#             #
#             (this_file_name, this_file_extension) = os.path.splitext(filename)
#             if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg'):
#                  print("ES IMAGEN")
#                  print(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename)
#                  f = open(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename, 'rb').read()
#                  newitem.foto = filename
#
#
#
#             filename = request.params["jupiteroperadores_licencearchivo"].filename
#             print(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename)
#             f = open(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename, 'rb').read()
#             newitem.licencia = filename
#
#
#             filename = request.params["jupiteroperadores_curp"].filename
#             print(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename)
#             f = open(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename, 'rb').read()
#             newitem.curp = filename
#
#
#
#             filename = request.params["jupiteroperadores_ine"].filename
#             print(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename)
#             f = open(ruta + "pythonjupiter/public/img/"+str(contador)+".-/"+ filename, 'rb').read()
#             newitem.ine = filename
#
#         newitem.operador=kw['jupiteroperadores_operador']
#         newitem.apodo=kw['jupiteroperadores_apodo']
#         newitem.rfc=kw['jupiteroperadores_rfc']
#         newitem.fecha_nacimiento=kw['jupiteroperadores_fecha_nacimiento']
#         newitem.correo=kw['jupiteroperadores_email']
#         newitem.sexo=kw['jupiteroperadores_genero']
#         newitem.estadocivil=kw['jupiteroperadores_estadocivil']
#         newitem.estatus=kw['jupiteroperadores_estatus']
#         #newitem.fecha_estatus=kw['jupiteroperadores_fecha_estatus']
#         newitem.direccion=kw['jupiteroperadores_address']
#         newitem.direccion2=kw['jupiteroperadores_address2']
#         newitem.direccion3=kw['jupiteroperadores_address3']
#         newitem.telefono=kw['jupiteroperadores_phone']
#         newitem.telefono2=kw['jupiteroperadores_phone2']
#         newitem.celular=kw['jupiteroperadores_mobilephone']
#         newitem.otro_celular=kw['jupiteroperadores_mobilephone2']
#         newitem.numero_segurosocial=kw['jupiteroperadores_noss']
#         newitem.fecha_vencimiento=kw['jupiteroperadores_duedate']
#         newitem.habilitado=1
#         newitem.observaciones=kw['jupiteroperadores_observaciones']
#         newitem.internal_id=kw['internal_id']
#
#         querylinea=DBSession.query(Linea).filter_by(nombre_linea=kw['jupiteroperadores_linea']).first()
#         newitem.id_linea=querylinea.id_linea
#
#         querytipolicencia=DBSession.query(TipoLicencia).filter_by(tipo_licencia=kw['jupiteroperadores_licencia']).first()
#         newitem.id_tipolicencia=querytipolicencia.id_tipolicencia
#
#
#         DBSession.add(newitem)
#
#
#         return 'Operador insertado'
#
#
#
#
#     @expose('pythonjupiter.templates.OperadoresTemplates.editoperador')
#     def editoperador(self, **kw):
#
#         typelicence = DBSession.query(TipoLicencia).filter_by(internal_id=kw['internal_id']).all()  # VER SI SE FILTRA POR
#         typelinea = DBSession.query(Linea).filter_by(internal_id=kw['internal_id']).all()
#
#         toedit = DBSession.query(Operador).filter_by(id_operador=kw['id']).first()
#
#         valueoflinea = DBSession.query(Linea).filter_by(id_linea=toedit.id_linea).first()
#         valueoftipolicencia=DBSession.query(TipoLicencia).filter_by(id_tipolicencia=toedit.id_tipolicencia).first()
#
#
#
#         if valueoflinea is not None:
#             variablelinea = valueoflinea.nombre_linea
#         else:
#             variablelinea = ''
#
#         if valueoftipolicencia is not None:
#             variabletipolicencia = valueoftipolicencia.tipo_licencia
#         else:
#             variabletipolicencia = ''
#
#
#         list = {"operador": toedit.operador, "apodo": toedit.apodo, "foto": toedit.foto,"rfc": toedit.rfc,"fecha_nacimiento": toedit.fecha_nacimiento,
#                        "correo":toedit.correo,"sexo": toedit.sexo,"estadocivil": toedit.estadocivil,"estatus": toedit.estatus,"fecha_estatus": toedit.fecha_ingreo ,
#                        "direccion": toedit.direccion,"direccion2": toedit.direccion2,"direccion3": toedit.direccion3,"telefono": toedit.telefono,"telefono2": toedit.telefono2,
#                        "celular": toedit.celular,"otro_celular": toedit.otro_celular,"numero_segurosocial": toedit.numero_segurosocial,"licencia": toedit.licencia,
#                        "fecha_vencimiento": toedit.fecha_vencimiento,"curp": toedit.curp,"ine": toedit.ine,"observaciones": toedit.observaciones,
#                        "id_tipolicencia": variabletipolicencia,"id_linea": variablelinea}
#
#
#         return dict(page='nuevaunidad', typelicence=typelicence, toedit=list, id=kw['id'], typelinea=typelinea,
#                     internal_id=kw['internal_id'],app_id=kw['application_id'], app_name=kw['app_name'], user=kw['user'])
#
#
#
#     @expose('json')
#     def editOperator(self,**kw):
#         id = kw['id']
#         querytoedit = DBSession.query(Operador).filter_by(id_operador=id).first()
#         app_dir = os.getenv('JUPITER_DIR')
#         if app_dir is None:
#             app_dir = os.getcwd()
#         ruta = app_dir + os.sep
#         try:
#             print(kw['jupiter_fotooperador'])
#             file = request.POST['jupiter_fotooperador']
#             if file!=b'':
#                 print('No viene vacio')
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-"
#                 print(destino)
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + file.filename.lstrip(os.sep)
#                 with open(destino, 'wb') as fdestino:
#                     copyfileobj(file.file, fdestino)
#                     print("Archivo copiado")
#                 fdestino.close()
#                 filename = request.params["jupiter_fotooperador"].filename
#                 #
#                 (this_file_name, this_file_extension) = os.path.splitext(filename)
#                 if (this_file_extension == '.jpeg' or this_file_extension == '.bmp' or this_file_extension == '.png' or this_file_extension == '.jpg'):
#                     print("ES IMAGEN")
#                     print(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename)
#                     print("FOTO EN BASE DE DATOS: ",querytoedit.foto)
#                     print("NUEVA IMAGEN",filename)
#                     f = open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename, 'rb').read()
#                     f2=open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.foto, 'rb').read()
#                     if(f==f2):
#                         print('FOTOS SON IGUALES')
#                     else:
#                         print("DISTINTOS")
#                         os.remove(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.foto)
#                         newhistoriaoperador = HistorialOperadores()
#                         newhistoriaoperador.dato = 'foto'
#                         newhistoriaoperador.valor_anterior = querytoedit.foto
#                         newhistoriaoperador.valor_nuevo = filename
#                         newhistoriaoperador.usuario = kw['who']
#                         newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                         querytoedit.foto = filename
#                         DBSession.add(newhistoriaoperador)
#
#         except KeyError:
#             print('NO VIENE jupiter_fotooperador')
#
#
#
#         try:
#             print(kw['jupiteroperadores_licencearchivo'])
#             file = request.POST['jupiteroperadores_licencearchivo']
#             if file!=b'':
#                 print('No viene vacio')
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-"
#                 print(destino)
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + file.filename.lstrip(os.sep)
#                 with open(destino, 'wb') as fdestino:
#                     copyfileobj(file.file, fdestino)
#                     print("Archivo copiado")
#                 fdestino.close()
#                 filename = request.params["jupiteroperadores_licencearchivo"].filename
#                 #
#                 (this_file_name, this_file_extension) = os.path.splitext(filename)
#                 if (this_file_extension == '.pdf'):
#                     print("ES PDF")
#                     print(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename)
#                     print("LICENCIA EN BASE DE DATOS: ",querytoedit.licencia)
#                     print("NUEVA LICENCIA",filename)
#                     f = open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename, 'rb').read()
#                     f2=open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.licencia, 'rb').read()
#                     if(f==f2):
#                         print('LICENCIAS SON IGUALES')
#                     else:
#                         print("DISTINTOS")
#                         os.remove(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.licencia)
#                         newhistoriaoperador = HistorialOperadores()
#                         newhistoriaoperador.dato = 'licencia'
#                         newhistoriaoperador.valor_anterior = querytoedit.licencia
#                         newhistoriaoperador.valor_nuevo = filename
#                         newhistoriaoperador.usuario = kw['who']
#                         newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                         querytoedit.licencia = filename
#                         DBSession.add(newhistoriaoperador)
#         except KeyError:
#             print('NO VIENE jupiteroperadores_licencearchivo')
#
#         try:
#             print(kw['jupiteroperadores_curp'])
#             file = request.POST['jupiteroperadores_curp']
#             if file!=b'':
#                 print('No viene vacio')
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-"
#                 print(destino)
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + file.filename.lstrip(os.sep)
#                 with open(destino, 'wb') as fdestino:
#                     copyfileobj(file.file, fdestino)
#                     print("Archivo copiado")
#                 fdestino.close()
#                 filename = request.params["jupiteroperadores_curp"].filename
#                 #
#                 (this_file_name, this_file_extension) = os.path.splitext(filename)
#                 if (this_file_extension == '.pdf'):
#                     print("ES PDF")
#                     print(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename)
#                     print("CURP EN BASE DE DATOS: ",querytoedit.curp)
#                     print("NUEVA CURP",filename)
#                     f = open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename, 'rb').read()
#                     f2=open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.curp, 'rb').read()
#                     if(f==f2):
#                         print('CURPS SON IGUALES')
#                     else:
#                         print("DISTINTOS")
#                         os.remove(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.curp)
#                         newhistoriaoperador = HistorialOperadores()
#                         newhistoriaoperador.dato = 'curp'
#                         newhistoriaoperador.valor_anterior = querytoedit.curp
#                         newhistoriaoperador.valor_nuevo = filename
#                         newhistoriaoperador.usuario = kw['who']
#                         newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                         querytoedit.curp = filename
#                         DBSession.add(newhistoriaoperador)
#         except KeyError:
#             print('NO VIENE jupiteroperadores_curp')
#
#
#
#         try:
#             print(kw['jupiteroperadores_ine'])
#             file = request.POST['jupiteroperadores_ine']
#             if file!=b'':
#                 print('No viene vacio')
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-"
#                 print(destino)
#                 destino = ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + file.filename.lstrip(os.sep)
#                 with open(destino, 'wb') as fdestino:
#                     copyfileobj(file.file, fdestino)
#                     print("Archivo copiado")
#                 fdestino.close()
#                 filename = request.params["jupiteroperadores_ine"].filename
#                 #
#                 (this_file_name, this_file_extension) = os.path.splitext(filename)
#                 if (this_file_extension == '.pdf'):
#                     print("ES PDF")
#                     print(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename)
#                     print("INE EN BASE DE DATOS: ",querytoedit.ine)
#                     print("NUEVA INE",filename)
#                     f = open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + filename, 'rb').read()
#                     f2=open(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.ine, 'rb').read()
#                     if(f==f2):
#                         print('INES SON IGUALES')
#                     else:
#                         print("DISTINTOS")
#                         os.remove(ruta + "pythonjupiter/public/img/" + str(id) + ".-/" + querytoedit.ine)
#                         newhistoriaoperador = HistorialOperadores()
#                         newhistoriaoperador.dato = 'INE'
#                         newhistoriaoperador.valor_anterior = querytoedit.ine
#                         newhistoriaoperador.valor_nuevo = filename
#                         newhistoriaoperador.usuario = kw['who']
#                         newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                         querytoedit.ine = filename
#                         DBSession.add(newhistoriaoperador)
#         except KeyError:
#             print('NO VIENE jupiteroperadores_ine')
#
#
#         if querytoedit is not None:
#
#             querylinea = DBSession.query(Linea).filter_by(nombre_linea=kw['jupiteroperadores_linea']).first()
#             querytipolicencia=DBSession.query(TipoLicencia).filter_by(tipo_licencia=kw['jupiteroperadores_licencia']).first()
#
#
#             if (querytoedit.id_linea != querylinea.id_linea):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'linea'
#                 anterior = DBSession.query(Linea).filter_by(id_linea=querytoedit.id_linea).first()
#                 if anterior is not None:
#                     newhistoriaoperador.valor_anterior = anterior.nombre_linea
#                 else:
#                     newhistoriaoperador.valor_anterior = ''
#                 nuevo = DBSession.query(Linea).filter_by(id_linea=querylinea.id_linea).first()
#                 newhistoriaoperador.valor_nuevo = nuevo.nombre_linea
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.id_linea = querylinea.id_linea
#                 DBSession.add(newhistoriaoperador)
#
#
#             if (querytoedit.id_tipolicencia != querytipolicencia.id_tipolicencia):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'tipo_licencia'
#                 anterior = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=querytoedit.id_tipolicencia).first()
#                 if anterior is not None:
#                     newhistoriaoperador.valor_anterior = anterior.tipo_licencia
#                 else:
#                     newhistoriaoperador.valor_anterior = ''
#                 nuevo = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=querytipolicencia.id_tipolicencia).first()
#                 newhistoriaoperador.valor_nuevo = nuevo.tipo_licencia
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.id_tipolicencia = querytipolicencia.id_tipolicencia
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.operador != kw['jupiteroperadores_operador']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'operador'
#                 newhistoriaoperador.valor_anterior = querytoedit.operador
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_operador']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.operador = kw['jupiteroperadores_operador']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.apodo != kw['jupiteroperadores_apodo']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'apodo'
#                 newhistoriaoperador.valor_anterior = querytoedit.apodo
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_apodo']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.apodo = kw['jupiteroperadores_apodo']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.rfc != kw['jupiteroperadores_rfc']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'rfc'
#                 newhistoriaoperador.valor_anterior = querytoedit.rfc
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_rfc']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.rfc = kw['jupiteroperadores_rfc']
#                 DBSession.add(newhistoriaoperador)
#
#             fechanewbirth = datetime.strptime(kw['jupiteroperadores_fecha_nacimiento'], '%Y-%m-%d').date()
#             if (querytoedit.fecha_nacimiento != fechanewbirth):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'fecha nacimiento'
#                 newhistoriaoperador.valor_anterior = querytoedit.fecha_nacimiento
#                 newhistoriaoperador.valor_nuevo = fechanewbirth
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.fecha_nacimiento = fechanewbirth
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.correo != kw['jupiteroperadores_email']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'correo'
#                 newhistoriaoperador.valor_anterior = querytoedit.correo
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_email']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.correo = kw['jupiteroperadores_email']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.sexo != kw['jupiteroperadores_genero']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'sexo'
#                 newhistoriaoperador.valor_anterior = querytoedit.sexo
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_genero']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.sexo = kw['jupiteroperadores_genero']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.estadocivil != kw['jupiteroperadores_estadocivil']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'estado civil'
#                 newhistoriaoperador.valor_anterior = querytoedit.estadocivil
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_estadocivil']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.estadocivil = kw['jupiteroperadores_estadocivil']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.estatus != kw['jupiteroperadores_estatus']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'estatus'
#                 newhistoriaoperador.valor_anterior = querytoedit.estatus
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_estatus']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.estatus = kw['jupiteroperadores_estatus']
#                 querytoedit.fecha_ingreo=datetime.now()
#                 DBSession.add(newhistoriaoperador)
#
#
#             # fechanew = datetime.strptime(kw['jupiteroperadores_fecha_estatus'], '%Y-%m-%d').date()
#             #
#             #
#             # if (querytoedit.fecha_estatus != fechanew):
#             #     newhistorioperador = HistorialOperadores()
#             #     newhistorioperador.dato = 'fecha_estatus'
#             #     newhistorioperador.valor_anterior = querytoedit.fecha_estatus
#             #     newhistorioperador.valor_nuevo = fechanew
#             #     newhistorioperador.usuario = kw['who']
#             #     newhistorioperador.sobrecampo = querytoedit.id_operador
#             #     querytoedit.fecha_estatus = fechanew
#             #     DBSession.add(newhistorioperador)
#
#             if (querytoedit.direccion != kw['jupiteroperadores_address']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'direccion'
#                 newhistoriaoperador.valor_anterior = querytoedit.direccion
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_address']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.direccion = kw['jupiteroperadores_address']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.direccion2 != kw['jupiteroperadores_address2']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'direccion2'
#                 newhistoriaoperador.valor_anterior = querytoedit.direccion2
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_address2']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.direccion2 = kw['jupiteroperadores_address2']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.direccion3 != kw['jupiteroperadores_address3']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'direccion3'
#                 newhistoriaoperador.valor_anterior = querytoedit.direccion3
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_address3']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.direccion3 = kw['jupiteroperadores_address3']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.telefono != kw['jupiteroperadores_phone']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'telefono'
#                 newhistoriaoperador.valor_anterior = querytoedit.telefono
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_phone']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.telefono = kw['jupiteroperadores_phone']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.telefono2 != kw['jupiteroperadores_phone2']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'telefono2'
#                 newhistoriaoperador.valor_anterior = querytoedit.telefono2
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_phone2']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.telefono2 = kw['jupiteroperadores_phone2']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.celular != kw['jupiteroperadores_mobilephone']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'celular'
#                 newhistoriaoperador.valor_anterior = querytoedit.celular
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_mobilephone']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.celular = kw['jupiteroperadores_mobilephone']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.otro_celular != kw['jupiteroperadores_mobilephone2']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'otro_celular'
#                 newhistoriaoperador.valor_anterior = querytoedit.otro_celular
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_mobilephone2']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.otro_celular = kw['jupiteroperadores_mobilephone2']
#                 DBSession.add(newhistoriaoperador)
#
#             if (querytoedit.numero_segurosocial != kw['jupiteroperadores_noss']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'Seguro Social'
#                 newhistoriaoperador.valor_anterior = querytoedit.numero_segurosocial
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_noss']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.numero_segurosocial = kw['jupiteroperadores_noss']
#                 DBSession.add(newhistoriaoperador)
#
#             querytoedit.internal_id = kw['internal_id']
#
#             fecha_vencimiento = datetime.strptime(kw['jupiteroperadores_duedate'], '%Y-%m-%d').date()
#
#             if (querytoedit.fecha_vencimiento != fecha_vencimiento):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'Vencimiento Licencia'
#                 newhistoriaoperador.valor_anterior = querytoedit.fecha_vencimiento
#                 newhistoriaoperador.valor_nuevo = fecha_vencimiento
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.fecha_vencimiento = fecha_vencimiento
#                 DBSession.add(newhistoriaoperador)
#
#
#             if (querytoedit.observaciones != kw['jupiteroperadores_observaciones']):
#                 newhistoriaoperador = HistorialOperadores()
#                 newhistoriaoperador.dato = 'observaciones'
#                 newhistoriaoperador.valor_anterior = querytoedit.observaciones
#                 newhistoriaoperador.valor_nuevo = kw['jupiteroperadores_observaciones']
#                 newhistoriaoperador.usuario = kw['who']
#                 newhistoriaoperador.sobrecampo = querytoedit.id_operador
#                 querytoedit.observaciones = kw['jupiteroperadores_observaciones']
#                 DBSession.add(newhistoriaoperador)
#
#
#
#         return 'Operador Editado'
#
#
#     @expose('json')
#     def disableoperador(self, **kw):
#         id = kw['id']
#         operador = kw['operador']
#         queryoperador = DBSession.query(Operador).filter_by(id_operador=id).first()
#         queryoperador.habilitado = 0
#         return "Operador deshabilitado"
#
#
#     @expose('pythonjupiter.templates.OperadoresTemplates.habilitaroperador')
#     def habilitaroperador(self):
#         allrecords = DBSession.query(Operador).filter_by(habilitado=0).all()
#         return dict(page='habilitar', allrecords=allrecords)
#
#
#     @expose('json')
#     def enable(self, **kw):
#         apodo = kw['apodo']
#         id = kw['id']
#         query = DBSession.query(Operador).filter_by(id_operador=id).first()
#         query.habilitado = 1
#
#         return "Operador Habilitado"
#
#     @expose('pythonjupiter.templates.UnitsTemplates.historialunidad')
#     def historialde(self,**kw):
#         id=kw['id']
#         allrecords=DBSession.query(HistorialOperadores).filter_by(sobrecampo=id).all()
#         return dict(page='historialde',allrecords=allrecords)
#
#
#
#     @expose('json')
#     def searchoperadores(self, **kw):
#         listabusca = []
#         searchfor = kw['searchfor']
#         if kw['name'] != '' or kw['name'] != ' ':
#             list = [{'show': 'id_operador'}, {'show': 'operador'}, {'show': 'apodo'}, {'show': 'foto'},{'show': 'id_linea'}, {'show': 'rfc'},
#                 {'show': 'estatus'}, {'show': 'telefono'},{'show': 'id_licencia'}, {'show': 'fecha_vencimiento'},{'show': 'estadodelicencia'},{'show': 'habilitado'},{'show': 'observaciones'}]
#
#             if (searchfor == 'credencial'):
#                 queryname = DBSession.query(Operador).filter_by(id_operador=int(kw['name'])).first()
#             if (searchfor == 'nombre'):
#                 queryname = DBSession.query(Operador).filter(Operador.operador.like('%' + kw['name'] + '%')).all()
#             if (searchfor == 'apodo'):
#                 queryname = DBSession.query(Operador).filter(Operador.apodo.like('%' + kw['name'] + '%')).all()
#             if (searchfor == 'estatus'):
#                 queryname = DBSession.query(Operador).filter(Operador.estatus.like('%' + kw['name'] + '%')).all()
#             if (searchfor == 'linea'):
#                 querylinea = DBSession.query(Linea).filter(Linea.nombre_linea.like('%' + kw['name'] + '%')).first()
#                 if querylinea is not None:
#                     queryname = DBSession.query(Operador).filter(Operador.id_linea == querylinea.id_linea).all()
#                 else:
#                     queryname = {}
#
#             if(searchfor=='Licencia por Vencer'):
#                 queryname=DBSession.query(Operador).filter(Operador.estadodelicencia=='VENCERA').all()
#
#             if(searchfor=='credencial'):
#                 if queryname is not None:
#                     querylinea = DBSession.query(Linea).filter_by(id_linea=queryname.id_linea).first()
#                     querytipolicencia = DBSession.query(TipoLicencia).filter_by(id_tipolicencia=queryname.id_tipolicencia).first()
#
#                     if (queryname.habilitado == 1):
#                         varhabilitado = "ACTIVA"
#                     else:
#                         varhabilitado = "DESACTIVADA"
#
#                     listabusca.append(
#                         {"id_operador": queryname.id_operador, "operador": queryname.operador, "apodo": queryname.apodo, "foto": queryname.foto,
#                          "id_linea": querylinea.nombre_linea, "rfc": queryname.rfc,
#                         "estatus": queryname.estatus, "telefono": queryname.telefono,"id_licencia": querytipolicencia.tipo_licencia,
#                         "fecha_vencimiento": queryname.fecha_vencimiento,"estadodelicencia": queryname.estadodelicencia,
#                         "habilitado": varhabilitado, "observaciones": queryname.observaciones})
#
#                     return dict(error='ok', listabusca=listabusca, list=list)
#                 else:
#                     listabusca.append({})
#             else:
#
#                 if queryname is not []:
#                     for item in queryname:
#
#
#                         querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
#                         querytipolicencia=DBSession.query(TipoLicencia).filter_by(id_tipolicencia=item.id_tipolicencia).first()
#
#                         if (item.habilitado == 1):
#                             varhabilitado = "ACTIVA"
#                         else:
#                             varhabilitado = "DESACTIVADA"
#
#
#
#                         listabusca.append({"id_operador": item.id_operador, "operador": item.operador, "apodo": item.apodo, "foto": item.foto,
#                      "id_linea": querylinea.nombre_linea, "rfc": item.rfc,
#                      "estatus": item.estatus, "telefono": item.telefono,"id_licencia": querytipolicencia.tipo_licencia,
#                      "fecha_vencimiento": item.fecha_vencimiento,"estadodelicencia": item.estadodelicencia,
#                      "habilitado": varhabilitado, "observaciones": item.observaciones})
#
#                     return dict(error='ok', listabusca=listabusca, list=list)
#                 else:
#                     return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list)
#
#
#
#     @expose('json')
#     def reloadoperadores(self, **kw):
#         listabusca = []
#         list = [{'show': 'id_operador'}, {'show': 'operador'}, {'show': 'apodo'}, {'show': 'foto'},{'show': 'id_linea'}, {'show': 'rfc'},
#                 {'show': 'estatus'}, {'show': 'telefono'},{'show': 'id_licencia'}, {'show': 'fecha_vencimiento'},{'show': 'estadodelicencia'},{'show': 'habilitado'},{'show': 'observaciones'}]
#         queryname = DBSession.query(Operador).all()
#
#         if queryname is not []:
#             for item in queryname:
#                 item.estadodelicencia = self.calculalicencia(item)
#
#
#                 querylinea = DBSession.query(Linea).filter_by(id_linea=item.id_linea).first()
#                 querytipolicencia=DBSession.query(TipoLicencia).filter_by(id_tipolicencia=item.id_tipolicencia).first()
#
#                 if (item.habilitado == 1):
#                     varhabilitado = "ACTIVA"
#                 else:
#                     varhabilitado = "DESACTIVADA"
#
#
#
#                 listabusca.append({"id_operador": item.id_operador, "operador": item.operador, "apodo": item.apodo, "foto": item.foto,
#                      "id_linea": querylinea.nombre_linea, "rfc": item.rfc,
#                      "estatus": item.estatus, "telefono": item.telefono,"id_licencia": querytipolicencia.tipo_licencia,
#                      "fecha_vencimiento": item.fecha_vencimiento,"estadodelicencia": item.estadodelicencia,
#                      "habilitado": varhabilitado, "observaciones": item.observaciones})
#
#             return dict(error='ok', listabusca=listabusca, list=list)
#         else:
#             return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list)
#
#
#     @expose('pythonjupiter.templates.OperadoresTemplates.archivos')
#     def archivosde(self,**kw):
#         soy=DBSession.query(Operador).filter_by(id_operador=kw['id']).first()
#         return dict(page='archivosde',soy=soy,app_name=kw['app_name'],application_id=kw['application_id'],internal_id=kw['internal_id'],user=kw['user'])
#
#     @expose('json')
#     def mostrararchivo(self,**kw):
#         archivo=kw['archivo']
#         id=kw['id']
#         template=render_template({'archivo': archivo,'id': id},"mako",'pythonjupiter.templates.OperadoresTemplates.elarchivo')
#         return dict(template=template)