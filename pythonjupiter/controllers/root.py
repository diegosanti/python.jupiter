# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
#from pythonjupiter.model.tables import Permisionario
from tg import render_template
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.catalog import CatalogController
from pythonjupiter.controllers.error import ErrorController
from pythonjupiter.model.tables import DatosUnidad, TipoUnidad,TipoTarjeta,Linea,HistorialUnidades,Derrotero,Aseguradora,Persona,GrupoPersona
from pythonjupiter.model.tables import Motivo,Periodo,Punto,TipoLicencia,Grupo,EstadoCuenta,OperadorCargo,UnidadOperador,UnidadCargo,CargoOperador,CargoUnidad
from pythonjupiter.model.tables import Body,Head,Condonaciones,CondonacionOperador,CondonacionUnidad,CuentaCargo,Poliza,BeneficiarioMoral
# from pythonjupiter.controllers.operadores import OperadoresTemplates
import os
from datetime import datetime,date,timedelta
from pythonjupiter.lib.helpers import urlsun
import requests
from tg import app_globals
from pythonjupiter.lib.services import Services
from pythonjupiter.controllers.lineas import LineasTemplates
from pythonjupiter.controllers.unidades import UnidadesTemplates
from pythonjupiter.controllers.reportes import ReportesTemplates
from pythonjupiter.controllers.personas import PersonasTemplates
from pythonjupiter.controllers.cargos_unidades import CargosTemplates
from pythonjupiter.controllers.PetsController import PetsController
from pythonjupiter.controllers.cargos_operadores import CargosOperadoresTemplates
from pythonjupiter.controllers.qrcode import QRCodeTemplates
from base64 import b64encode
from shutil import copyfileobj,copyfile
from pythonjupiter.lib.tests import TestClass
from pythonjupiter.controllers.probando_abono import AbonoCuentaController
from pythonjupiter.controllers.condonaciones import CondonacionController
from pythonjupiter.controllers.BodyHead import BodyHeadController
from pythonjupiter.controllers.cuentas import CuentasController


from pythonjupiter.controllers.Catalogs.unit_type import UnitTypeController
from pythonjupiter.controllers.Catalogs.card_type import CardTypeController
from pythonjupiter.controllers.Catalogs.derrotero import DerroteroController
from pythonjupiter.controllers.Catalogs.insurance import InsuranceController
from pythonjupiter.controllers.Catalogs.payment_motive import PaymentMotiveController
from pythonjupiter.controllers.Catalogs.period import PeriodController
from pythonjupiter.controllers.Catalogs.check_point import CheckPointController
from pythonjupiter.controllers.Catalogs.licence_type import LicenceTypeController
from pythonjupiter.controllers.Catalogs.group import GroupController
from pythonjupiter.controllers.Catalogs.operator_group import OperatorGroupController
from pythonjupiter.controllers.Catalogs.extern_beneficiary import ExternBeneficiaryController

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for the python.jupiter application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    secc = SecureController()
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)

    error = ErrorController()

    services=Services()
    lineas=LineasTemplates()
    unidades=UnidadesTemplates()
    # operadores=OperadoresTemplates()
    reportes=ReportesTemplates()
    test=TestClass()
    cargosunidades=CargosTemplates()
    cargosoperadores=CargosOperadoresTemplates()
    abono = AbonoCuentaController()
    condonacion=CondonacionController()
    bodyhead=BodyHeadController()
    cuenta=CuentasController()
    qr=QRCodeTemplates()
    pets=PetsController()

    personas = PersonasTemplates()

    unit_type = UnitTypeController()
    card_type = CardTypeController()
    derrotero = DerroteroController()
    insurance = InsuranceController()
    payment_motive = PaymentMotiveController()
    period = PeriodController()
    check_point = CheckPointController()
    license = LicenceTypeController()
    group = GroupController()
    operator_group = OperatorGroupController()
    extern_beneficiary = ExternBeneficiaryController()


    def _before(self, *args, **kw):
        tmpl_context.project_name = "pythonjupiter"

    @expose('pythonjupiter.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('pythonjupiter.templates.about')
    def about(self):
        """Handle the 'about' page."""
        return dict(page='about')

    @expose('pythonjupiter.templates.environ')
    def environ(self):
        """This method showcases TG's access to the wsgi environment."""
        return dict(page='environ', environment=request.environ)

    @expose('pythonjupiter.templates.data')
    @expose('json')
    def data(self, **kw):
        """
        This method showcases how you can use the same controller
        for a data page and a display page.
        """
        return dict(page='data', params=kw)
    @expose('pythonjupiter.templates.index')
    @require(predicates.has_permission('manage', msg=l_('Only for managers')))
    def manage_permission_only(self, **kw):
        """Illustrate how a page for managers only works."""
        return dict(page='managers stuff')

    @expose('pythonjupiter.templates.index')
    @require(predicates.is_user('editor', msg=l_('Only for the editor')))
    def editor_user_only(self, **kw):
        """Illustrate how a page exclusive for the editor works."""
        return dict(page='editor stuff')

    @expose()
    def check(self):
        return "UP"

    @expose('pythonjupiter.templates.main')
    def main(self):
        """Handle the front-page."""
        return dict(page='index')

    # @expose('pythonjupiter.templates.showData')
    # def tipo_unidad(self, **kw):
    #     cat = CatalogController(TipoUnidad, 'id_tipounidad', kw, '', 'Tipo Unidad', 'tipo_unidad')
    #     if (kw['action'] == 'show'):
    #         return cat.show()
    #     if (kw['action'] == 'edit'):
    #         return cat.Form()
    #     if (kw['action'] == 'new'):
    #         return cat.Save()

    @expose('pythonjupiter.templates.showData')
    def tipo_tarjeta(self, **kw):
        cat = CatalogController(TipoTarjeta, 'id_tipotarjeta', kw, '', 'Tipo Tarjeta', 'tipo_tarjeta')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()

    # @expose('pythonjupiter.templates.showData')
    # def derrotero(self, **kw):
    #     #print("ESTE ES EL KW==>",kw)
    #     cat = CatalogController(Derrotezro, 'id_derrotero', kw, '', 'Derrotero', 'derrotero')
    #     if (kw['action'] == 'show'):
    #         return cat.show()
    #     if (kw['action'] == 'edit'):
    #         return cat.Form()
    #     if (kw['action'] == 'new'):
    #         return cat.Save()

    @expose('pythonjupiter.templates.showData')
    def grupo(self, **kw):
        cat = CatalogController(Grupo, 'id_grupo', kw, '', 'Grupo', 'grupo')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()

    @expose('pythonjupiter.templates.showData')
    def grupopersona(self, **kw):
        cat = CatalogController(GrupoPersona, 'id_grupopersona', kw, '', 'GrupoPersona', 'grupopersona')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()

    # @expose('pythonjupiter.templates.showData')
    # def aseguradora(self, **kw):
    #     cat = CatalogController(Aseguradora, 'id_aseguradora', kw, '', 'Aseguradora', 'aseguradora')
    #     if (kw['action'] == 'show'):
    #         return cat.show()
    #     if (kw['action'] == 'edit'):
    #         return cat.Form()
    #     if (kw['action'] == 'new'):
    #         return cat.Save()


    # @expose('pythonjupiter.templates.showData')
    # def motivo(self, **kw):
    #     cat = CatalogController(Motivo, 'id_motivo', kw, '', 'Motivo', 'motivo')
    #     if (kw['action'] == 'show'):
    #         return cat.show()
    #     if (kw['action'] == 'edit'):
    #         return cat.Form()
    #     if (kw['action'] == 'new'):
    #         return cat.Save()

    @expose('pythonjupiter.templates.showData')
    def beneficiariomoral(self, **kw):
        cat = CatalogController(BeneficiarioMoral, 'id_beneficiariomoral', kw, '', 'BeneficiarioMoral', 'beneficiariomoral')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()

    # @expose('pythonjupiter.templates.showData')
    # def periodo(self, **kw):
    #     cat = CatalogController(Periodo, 'id_periodo', kw, '', 'Periodo', 'periodo')
    #     if (kw['action'] == 'show'):
    #         return cat.show()
    #     if (kw['action'] == 'edit'):
    #         return cat.Form()
    #     if (kw['action'] == 'new'):
    #         return cat.Save()

    @expose('pythonjupiter.templates.showData')
    def punto(self, **kw):
        cat = CatalogController(Punto, 'id_punto', kw, '', 'Punto', 'punto')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()


    @expose('pythonjupiter.templates.showData')
    def tipo_licencia(self, **kw):
        cat = CatalogController(TipoLicencia, 'id_tipolicencia', kw, '', 'TipoLicencia', 'tipo_licencia')
        if (kw['action'] == 'show'):
            return cat.show()
        if (kw['action'] == 'edit'):
            return cat.Form()
        if (kw['action'] == 'new'):
            return cat.Save()

    @expose('json')
    def searchwhat(self,**kw):
        #print(kw['thetitle'])
        if(kw['thetitle']=='TipoUnidad'):
           cat = CatalogController(TipoUnidad, 'id_tipounidad', kw, '', 'Tipo Unidad', 'tipo_unidad')
           value=cat.show()
        if (kw['thetitle'] == 'TipoTarjeta'):
            cat = CatalogController(TipoTarjeta, 'id_tipotarjeta', kw, '', 'Tipo Tarjeta', 'tipo_tarjeta')
            value = cat.show()
        if (kw['thetitle'] == 'Derrotero'):
            cat = CatalogController(Derrotero, 'id_derrotero', kw, '', 'Derrotero', 'derrotero')
            value = cat.show()
        if (kw['thetitle'] == 'Grupo'):
            cat = CatalogController(Grupo, 'id_grupo', kw, '', 'Grupo', 'grupo')
            value = cat.show()
        if (kw['thetitle'] == 'Aseguradora'):
            cat = CatalogController(Aseguradora,'id_aseguradora', kw, '', 'Aseguradora', 'aseguradora')
            value = cat.show()
        if(kw['thetitle']=='Motivo'):
            cat = CatalogController(Motivo, 'id_motivo', kw, '', 'Motivo', 'motivo')
            value=cat.show()
        if (kw['thetitle'] == 'Periodo'):
            cat = CatalogController(Periodo, 'id_periodo', kw, '', 'Periodo', 'periodo')
            value = cat.show()
        if (kw['thetitle'] == 'Punto'):
            cat = CatalogController(Punto, 'id_punto', kw, '', 'Punto', 'punto')
            value = cat.show()
        if (kw['thetitle'] == 'TipoLicencia'):
            cat = CatalogController(TipoLicencia, 'id_tipolicencia', kw, '', 'TipoLicencia', 'tipo_licencia')
            value = cat.show()
        if (kw['thetitle'] == 'GrupoPersona'):
            cat = CatalogController(GrupoPersona, 'id_grupopersona', kw, '', 'GrupoPersona', 'grupopersona')
            value = cat.show()
        if (kw['thetitle'] == 'BeneficiarioMoral'):
            cat = CatalogController(BeneficiarioMoral, 'id_beneficiariomoral', kw, '', 'BeneficiarioMoral', 'beneficiariomoral')
            value=cat.show()


        return value

    @expose('json')
    def reloadwhat(self,**kw):
        if(kw['thetitle']=='TipoUnidad'):
           cat = CatalogController(TipoUnidad, 'id_tipounidad', kw, '', 'Tipo Unidad', 'tipo_unidad')
           value=cat.show()
        if (kw['thetitle'] == 'TipoTarjeta'):
            cat = CatalogController(TipoTarjeta, 'id_tipotarjeta', kw, '', 'Tipo Tarjeta', 'tipo_tarjeta')
            value = cat.show()
        if (kw['thetitle'] == 'Derrotero'):
            cat = CatalogController(Derrotero, 'id_derrotero', kw, '', 'Derrotero', 'derrotero')
            value = cat.show()
        if (kw['thetitle'] == 'Grupo'):
            cat = CatalogController(Grupo, 'id_grupo', kw, '', 'Grupo', 'grupo')
            value = cat.show()
        if (kw['thetitle'] == 'Aseguradora'):
            cat = CatalogController(Aseguradora, 'id_aseguradora', kw, '', 'Aseguradora', 'aseguradora')
            value = cat.show()
        if(kw['thetitle']=='Motivo'):
            cat = CatalogController(Motivo, 'id_motivo', kw, '', 'Motivo', 'motivo')
            value=cat.show()
        if (kw['thetitle'] == 'Periodo'):
            cat = CatalogController(Periodo, 'id_periodo', kw, '', 'Periodo', 'periodo')
            value = cat.show()
        if (kw['thetitle'] == 'Punto'):
            cat = CatalogController(Punto, 'id_punto', kw, '', 'Punto', 'punto')
            value = cat.show()
        if (kw['thetitle'] == 'TipoLicencia'):
            cat = CatalogController(TipoLicencia, 'id_tipolicencia', kw, '', 'TipoLicencia', 'tipo_licencia')
            value = cat.show()
        if (kw['thetitle'] == 'GrupoPersona'):
            cat = CatalogController(GrupoPersona, 'id_grupopersona', kw, '', 'GrupoPersona', 'grupopersona')
            value = cat.show()
        if (kw['thetitle'] == 'BeneficiarioMoral'):
            cat = CatalogController(BeneficiarioMoral, 'id_beneficiariomoral', kw, '', 'BeneficiarioMoral', 'beneficiariomoral')
            value=cat.show()

        return value


    @expose('json')
    def ecos2(self,**kw):
        domaincygnus=app_globals.domaincygnus+"app_id="+kw["client_id"]
        lista=[]
        try:
            ecos = requests.get(domaincygnus, auth=("manager", "managepass"))
            ecos = ecos.json()
            ecos = ecos['ecos']
            for item in ecos:
                lista.append({'ecos': item['eco']})

        except ValueError:
            ecosin = []
        data = {"rows": lista}
        return data

    @expose('json')
    def ecostemplate(self,**kw):
        template=render_template(kw, "mako", 'pythonjupiter.templates.UnitsTemplates.jqGridecos')
        return dict(template=template)

    @expose('pythonjupiter.templates.Abonos.probando_abono')
    #@require(predicates.not_anonymous())
    def Abono(self, **kw):
        print("Modules")

        # kw['application_id'] = 1
        # kw['internal_id'] = 2
        fecha=datetime.now().date()
        queryusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        if queryusuario is None:
            if kw['app_name']=='TODO':
                unidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).all()
            else:
                unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_name=kw['app_name']).filter_by(app_id=kw['application_id']).all()

            operadores=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).all()
        else:
            unidades={}
            operadores={}

        perms='Search for Pay,Add Pay,Cancel Pay'
        url = urlsun() + "/services/perms?perms=" + str(perms) + "&user=" + kw['user']
        r = requests.get(url, auth=('manager', 'managepass'))
        json_object = r.json()
        if json_object['error'] == "ok":
            perms = json_object['perms']

        else:
            perms = {}
        return dict(page='Head', application_id=kw['application_id'], internal_id=kw['internal_id'],
                    app_name=kw['app_name'], user=kw['user'],fecha=fecha,unidades=unidades,operadores=operadores,perms=perms)



    @expose('pythonjupiter.templates.BillingStatement.BillingStatement')
    def BillingStatement(self,**kw):
        queryusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        fecha = datetime.now().date()-timedelta(days=1)
        year=fecha.year
        mess=fecha.month
        perms = 'Sec1 Billing'
        url = urlsun() + "/services/perms?perms=" + str(perms) + "&user=" + kw['user']
        r = requests.get(url, auth=('manager', 'managepass'))
        json_object = r.json()
        if json_object['error'] == "ok":
            perms = json_object['perms']

        else:
            perms = {}
        if queryusuario is None:
            if kw['app_name']=='TODO':
                unidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).all()
                queryempresa=DBSession.query(Linea).filter_by(internal_id=kw['internal_id']).all()

            else:
                unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_name=kw['app_name']).filter_by(internal_id=kw['internal_id']).all()
                queryempresa = DBSession.query(Linea).filter_by(internal_id=kw['internal_id']).all()

            operadores=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter_by(internal_id=kw['internal_id']).all()
            permisionarios = DBSession.query(Persona).filter_by(habilitadopermisionario=1).filter_by(espermisionario=1).filter_by(internal_id=kw['internal_id']).all()
        else:
            unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).filter_by(id_persona=queryusuario.id_persona).all()

            operadores={}
            queryempresa={}
            permisionarios={}
        if mess<10:
            mess='0'+str(mess)
        fechames=str(year)+'-'+str(mess)

        return dict(page='BillingStatement', application_id=kw['application_id'], internal_id=kw['internal_id'],app_name=kw['app_name'], user=kw['user'],
                    fecha=fecha,unidades=unidades,operadores=operadores,queryempresa=queryempresa,perms=perms,permisionarios=permisionarios,fechames=fechames)


    @expose('pythonjupiter.templates.Condonaciones.condonaciones')
    #@require(predicates.not_anonymous())
    def Condonacion(self, **kw):

        # kw['application_id'] = 1
        # kw['internal_id'] = 2
        return dict(page='Condonacion', application_id=kw['application_id'], internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'])

    @expose('pythonjupiter.templates.BodyHead.bodyhead')
    #@require(predicates.not_anonymous())
    def Head(self, **kw):
        fecha=datetime.now().date()-timedelta(days=1)
        # kw['application_id'] = 1
        # kw['internal_id'] = 2
        queryesusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).filter_by(internal_id=kw['internal_id']).first()
        operadores = DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter_by(internal_id=kw['internal_id']).all()
        if queryesusuario is None:
            soy='nouser'
            if kw['app_name']=='TODO':
                unidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).all()
            else:
                unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=kw['internal_id']).filter_by(app_name=kw['app_name']).all()

        else:
            soy='siuser'
            unidades=DBSession.query(DatosUnidad).filter_by(id_persona=queryesusuario.id_persona).all()
            operadores={}

        return dict(page='Head', application_id=kw['application_id'], internal_id=kw['internal_id'],
                    app_name=kw['app_name'], user=kw['user'],fecha=fecha,unidades=unidades,operadores=operadores,soy=soy)

    @expose('pythonjupiter.templates.Cuentas.cuentas')
    #@require(predicates.not_anonymous())
    def Cuentas(self, **kw):
        print("Modules")
        # kw['application_id'] = 1
        # kw['internal_id'] = 2
        domain = app_globals.domainsun+"internal_id="+kw['internal_id']
        appnames=[]
        try:
            content = requests.get(domain, auth=("manager", "managepass"))
            content = content.json()
            empresa = content['applications']
        except ValueError:
            empresa = []


        queryempresa=DBSession.query(Linea).filter_by(internal_id=kw['internal_id']).all()
        #print("EMPRESA==>",empresa)
        if kw['app_name']!='TODO':
            for itemempresa in empresa:
                #print(itemempresa)
                if kw['app_name']==itemempresa['application_name']:
                    appnames.append({'empresa': itemempresa['application_name']})
        else:
            for itemempresa in empresa:
                if itemempresa['application_name']!='TODO':
                    appnames.append({'empresa': itemempresa['application_name']})


        #print("appnames==>",appnames)
        fecha=datetime.now().date()
        todayDate = datetime.now().date()
        if todayDate.day > 25:
            todayDate += timedelta(7)
        fechauno=todayDate.replace(day=1)
        return dict(appnames=appnames,page='estadocuenta',fecha=fecha,application_id=kw['application_id'], internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],queryempresa=queryempresa,
                    fechauno=fechauno)



    @expose('pythonjupiter.templates.botoncargar')
    @require(predicates.not_anonymous())
    def cargarenedo(self, **kw):
        #print("Modules")

        # kw['application_id'] = 1
        # kw['internal_id'] = 2
        return dict(page='Abono')

    @expose('pythonjupiter.templates.Salidas.salidas')
    #@require(predicates.not_anonymous())
    def Salidas(self, **kw):
        fecha=datetime.now().date()

        return dict(application_id=kw['application_id'], internal_id=kw['internal_id'],app_name=kw['app_name'],user=kw['user'],fecha=fecha)

    @expose('json')
    @require(predicates.not_anonymous())
    def cargaall(self):
        internal_id=5
        querycargosunidades=DBSession.query(CargoUnidad).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()
        querycargosoperadores=DBSession.query(CargoOperador).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()

        for itemcu in querycargosunidades:
            querytablados=DBSession.query(UnidadCargo).filter_by(cargo_id=itemcu.id_cargounidad).all()
            frecuencia=itemcu.frecuencia.split(",")
            prueba=(datetime.now().date()).strftime('%A')

            if prueba=='Monday':
                prueba='Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'

            if prueba in frecuencia:
                for itemuc in querytablados:
                    queryunidad=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(id_unidad=itemuc.unidad_id).first()
                    if queryunidad is not None:
                        querycondonacionesunidad = DBSession.query(CondonacionUnidad).filter_by(unidad_id=queryunidad.id_unidad).filter_by(cargounidad_id= itemcu.id_cargounidad).all()
                        if querycondonacionesunidad !=[]:
                            condonacion=0
                            for itemcondo in querycondonacionesunidad:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion=condonacion
                        else:
                            condonacion=0

                        if itemuc.monto > 0:
                            newbody=Body()
                            newbody.monto=itemuc.monto
                            if condonacion>newbody.monto:
                                newbody.condonacion=0
                            else:
                                newbody.condonacion=condonacion
                            newbody.saldo=itemuc.monto-newbody.condonacion
                            newbody.pagado=0
                            newbody.fecha=datetime.now().date()
                            newbody.id_unidad=queryunidad.id_unidad
                            newbody.id_cargounidad=itemcu.id_cargounidad
                            newbody.internal_id=internal_id
                            DBSession.add(newbody)

        querypoliza = DBSession.query(Poliza).all()
        for itempoliza in querypoliza:
            if datetime.now().date() <=itempoliza.fecha_fin and datetime.now().date()>=itempoliza.fecha_inicio:
                #print("DENTRO DE LA FECHA")
                newbody = Body()
                newbody.monto = itempoliza.montodividido
                newbody.condonacion = 0
                newbody.saldo = itempoliza.montodividido - newbody.condonacion
                newbody.pagado = 0
                newbody.fecha = datetime.now().date()
                newbody.id_unidad = itempoliza.id_unidad
                newbody.internal_id = itempoliza.internal_id
                newbody.espoliza=1
                DBSession.add(newbody)

        for itemco in querycargosoperadores:
            querytablados=DBSession.query(OperadorCargo).filter_by(cargo_id=itemco.id_cargooperador).all()
            frecuencia=itemco.frecuencia.split(",")
            prueba=(datetime.now().date()).strftime('%A')
        #
            if prueba=='Monday':
                prueba='Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'
        #
            if prueba in frecuencia:
                for itemoc in querytablados:
                    querypersona=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(id_persona=itemoc.operador_id).first()
                    if querypersona is not None:
                        querycondonacioneoperadores = DBSession.query(CondonacionOperador).filter_by(operador_id=querypersona.id_persona).filter_by(cargooperador_id=itemco.id_cargooperador).all()

                        if querycondonacioneoperadores!=[]:
                            condonacion=0
                            for itemcondo in querycondonacioneoperadores:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion=condonacion
                        else:
                            condonacion=0

                        if itemoc.monto > 0:
                            newbody=Body()
                            newbody.monto=itemoc.monto
                            if condonacion>newbody.monto:
                                newbody.condonacion=0
                            else:
                                newbody.condonacion=condonacion
                            newbody.saldo=itemoc.monto-newbody.condonacion
                            newbody.pagado = 0
                            newbody.fecha=datetime.now().date()
                            newbody.id_operador=querypersona.id_persona
                            newbody.id_cargopersona=itemco.id_cargooperador
                            newbody.internal_id = internal_id
                            DBSession.add(newbody)
        #
        queryunidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=internal_id).all()
        queryoperadores=DBSession.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).filter_by(internal_id=internal_id).all()

        for itemunidad in queryunidades:
            saldo = 0
            querybody=DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(fecha=datetime.now().date()).all()
            for itembody in querybody:
                print("itembody.saldo=",itembody.saldo)
                saldo=saldo+itembody.saldo
        #
            if saldo > 0:
                newhead=Head()
                newhead.fecha=datetime.now().date()
                newhead.estatus='Por Pagar'
                newhead.porpagar=saldo
                newhead.pagado=0
                newhead.faltante=saldo
                newhead.id_unidad=itemunidad.id_unidad
                newhead.internal_id = internal_id
                newhead.app_name=itemunidad.app_name
                DBSession.add(newhead)
        #
        for itemoperador in queryoperadores:
            saldo=0
            querybody=DBSession.query(Body).filter_by(id_operador=itemoperador.id_persona).filter_by(fecha=datetime.now().date()).all()
            for itembody in querybody:
                saldo=saldo+itembody.saldo

            if saldo > 0:
                newhead=Head()
                newhead.fecha=datetime.now().date()
                newhead.estatus='Por Pagar'
                newhead.porpagar=saldo
                newhead.pagado=0
                newhead.faltante=saldo
                newhead.id_operador=itemoperador.id_persona
                newhead.internal_id=internal_id
                newhead.app_name='TODO'
                DBSession.add(newhead)
        DBSession.flush()
        return dict(error='ok')





    @expose('json')
    @require(predicates.not_anonymous())
    def cargaall2(self):
        internal_id=5
        querycargosunidades=DBSession.query(CargoUnidad).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()
        querycargosoperadores=DBSession.query(CargoOperador).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()

        for itemcu in querycargosunidades:
            querytablados=DBSession.query(UnidadCargo).filter_by(cargo_id=itemcu.id_cargounidad).all()
            frecuencia=itemcu.frecuencia.split(",")
            prueba=(datetime.now().date()-timedelta(days=1)).strftime('%A')

            if prueba=='Monday':
                prueba='Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'

            if prueba in frecuencia:
                for itemuc in querytablados:
                    queryunidad=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(id_unidad=itemuc.unidad_id).first()
                    if queryunidad is not None:
                        querycondonacionesunidad = DBSession.query(CondonacionUnidad).filter_by(unidad_id=queryunidad.id_unidad).filter_by(cargounidad_id= itemcu.id_cargounidad).all()
                        if querycondonacionesunidad !=[]:
                            condonacion=0
                            for itemcondo in querycondonacionesunidad:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()-timedelta(days=1)).filter(Condonaciones.fechafinal >= datetime.now().date()-timedelta(days=1)).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion=condonacion
                        else:
                            condonacion=0

                        if itemuc.monto > 0:
                            newbody=Body()
                            newbody.monto=itemuc.monto
                            if condonacion>newbody.monto:
                                newbody.condonacion=0
                            else:
                                newbody.condonacion=condonacion
                            newbody.saldo=itemuc.monto-newbody.condonacion
                            newbody.pagado = 0
                            newbody.fecha=datetime.now().date()-timedelta(days=1)
                            newbody.id_unidad=queryunidad.id_unidad
                            newbody.id_cargounidad=itemcu.id_cargounidad
                            newbody.internal_id=internal_id
                            DBSession.add(newbody)

        querypoliza = DBSession.query(Poliza).all()
        for itempoliza in querypoliza:
            if datetime.now().date()-timedelta(days=1)  <= itempoliza.fecha_fin and datetime.now().date()-timedelta(days=1) >=itempoliza.fecha_inicio:
                print("DENTRO DE LA FECHA")
                newbody = Body()
                newbody.monto = itempoliza.montodividido
                newbody.condonacion = 0
                newbody.saldo = itempoliza.montodividido - newbody.condonacion
                newbody.pagado = 0
                newbody.fecha = datetime.now().date()-timedelta(days=1)
                newbody.id_unidad = itempoliza.id_unidad
                newbody.internal_id = itempoliza.internal_id
                newbody.espoliza = 1
                DBSession.add(newbody)


        for itemco in querycargosoperadores:
            querytablados=DBSession.query(OperadorCargo).filter_by(cargo_id=itemco.id_cargooperador).all()
            frecuencia=itemco.frecuencia.split(",")
            prueba=(datetime.now().date()-timedelta(days=1)).strftime('%A')
        #
            if prueba=='Monday':
                prueba='Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'
        #
            if prueba in frecuencia:
                for itemoc in querytablados:
                    querypersona=DBSession.query(Persona).filter_by(id_persona=itemoc.operador_id).first()
                    querypersona=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(id_persona=itemoc.operador_id).first()
                    if querypersona is not None:
                        querycondonacioneoperadores = DBSession.query(CondonacionOperador).filter_by(operador_id=querypersona.id_persona).filter_by(cargooperador_id=itemco.id_cargooperador).all()

                        if querycondonacioneoperadores!=[]:
                            condonacion=0
                            for itemcondo in querycondonacioneoperadores:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                                .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()-timedelta(days=1)).filter(Condonaciones.fechafinal >= datetime.now().date()-timedelta(days=1)).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion=condonacion
                        else:
                            condonacion=0

                        if itemoc.monto > 0:
                            newbody=Body()
                            newbody.monto=itemoc.monto
                            if condonacion>newbody.monto:
                                newbody.condonacion=0
                            else:
                                newbody.condonacion=condonacion
                            newbody.saldo=itemoc.monto-newbody.condonacion
                            newbody.pagado = 0
                            newbody.fecha=datetime.now().date()-timedelta(days=1)
                            newbody.id_operador=querypersona.id_persona
                            newbody.id_cargopersona=itemco.id_cargooperador
                            newbody.internal_id = internal_id
                            DBSession.add(newbody)
        #
        queryunidades=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(internal_id=internal_id).all()
        queryoperadores=DBSession.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).filter_by(internal_id=internal_id).all()

        for itemunidad in queryunidades:
            saldo = 0
            querybody=DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(fecha=datetime.now().date()-timedelta(days=1)).all()
            for itembody in querybody:
                print("itembody.saldo=",itembody.saldo)
                saldo=saldo+itembody.saldo
        #

            if saldo > 0:
                newhead=Head()
                newhead.fecha=datetime.now().date()-timedelta(days=1)
                newhead.estatus='Por Pagar'
                newhead.porpagar=saldo
                newhead.pagado=0
                newhead.faltante=saldo
                newhead.id_unidad=itemunidad.id_unidad
                # thequeryoperador=DBSession.query(Persona).filter_by(id_unidad=itemunidad.id_unidad).first()
                # try:
                #     variable=thequeryoperador.id_persona
                # except:
                #     variable=None
                #newhead.id_operador=variable
                newhead.internal_id = internal_id
                newhead.app_name = itemunidad.app_name
                DBSession.add(newhead)
        #
        for itemoperador in queryoperadores:
            saldo=0
            querybody=DBSession.query(Body).filter_by(id_operador=itemoperador.id_persona).filter_by(fecha=datetime.now().date()-timedelta(days=1)).all()
            for itembody in querybody:
                saldo=saldo+itembody.saldo

            if saldo > 0:
                newhead=Head()
                newhead.fecha=datetime.now().date()-timedelta(days=1)
                newhead.estatus='Por Pagar'
                newhead.porpagar=saldo
                newhead.pagado=0
                newhead.faltante=saldo
                newhead.id_operador=itemoperador.id_persona
                newhead.internal_id=internal_id
                newhead.app_name='TODO'
                DBSession.add(newhead)
        DBSession.flush()
        return dict(error='ok')

    @expose('json')
    @require(predicates.not_anonymous())
    def cargaall3(self):
        internal_id = 5
        querycargosunidades = DBSession.query(CargoUnidad).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()
        querycargosoperadores = DBSession.query(CargoOperador).filter_by(tipodecargo=1).filter_by(internal_id=internal_id).all()


        for itemcu in querycargosunidades:
            querytablados = DBSession.query(UnidadCargo).filter_by(cargo_id=itemcu.id_cargounidad).all()
            frecuencia = itemcu.frecuencia.split(",")
            prueba = (datetime.now().date() - timedelta(days=2)).strftime('%A')

            if prueba == 'Monday':
                prueba = 'Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'

            if prueba in frecuencia:
                for itemuc in querytablados:
                    queryunidad = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(id_unidad=itemuc.unidad_id).first()
                    if queryunidad is not None:
                        querycondonacionesunidad = DBSession.query(CondonacionUnidad).filter_by(
                                unidad_id=queryunidad.id_unidad).filter_by(cargounidad_id=itemcu.id_cargounidad).all()
                        if querycondonacionesunidad != []:
                            condonacion = 0
                            for itemcondo in querycondonacionesunidad:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(
                                        id_condonacion=itemcondo.condonacion_id) \
                                        .filter_by(cancelado=0).filter(
                                        Condonaciones.fechainicial <= datetime.now().date() - timedelta(days=2)).filter(
                                        Condonaciones.fechafinal >= datetime.now().date() - timedelta(days=2)).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion = condonacion
                        else:
                            condonacion = 0

                        print("itemuc.monto",itemuc.monto)
                        if itemuc.monto>0:
                            newbody = Body()
                            newbody.monto = itemuc.monto
                            if condonacion > newbody.monto:
                                newbody.condonacion = 0
                            else:
                                newbody.condonacion = condonacion
                            newbody.saldo = itemuc.monto - newbody.condonacion
                            newbody.pagado = 0
                            newbody.fecha = datetime.now().date() - timedelta(days=2)
                            newbody.id_unidad = queryunidad.id_unidad
                            newbody.id_cargounidad = itemcu.id_cargounidad
                            newbody.internal_id = internal_id
                            DBSession.add(newbody)

        querypoliza = DBSession.query(Poliza).all()
        for itempoliza in querypoliza:
            if datetime.now().date()-timedelta(days=2) <= itempoliza.fecha_fin and datetime.now().date()-timedelta(days=2)>=itempoliza.fecha_inicio:
                print("DENTRO DE LA FECHA")
                newbody = Body()
                newbody.monto = itempoliza.montodividido
                newbody.condonacion = 0
                newbody.saldo = itempoliza.montodividido - newbody.condonacion
                newbody.pagado = 0
                newbody.fecha = datetime.now().date() - timedelta(days=2)
                newbody.id_unidad = itempoliza.id_unidad
                newbody.internal_id = itempoliza.internal_id
                newbody.espoliza = 1
                DBSession.add(newbody)



        for itemco in querycargosoperadores:
            querytablados = DBSession.query(OperadorCargo).filter_by(cargo_id=itemco.id_cargooperador).all()
            frecuencia = itemco.frecuencia.split(",")
            prueba = (datetime.now().date() - timedelta(days=2)).strftime('%A')
                #
            if prueba == 'Monday':
                prueba = 'Lunes'
            if prueba == 'Tuesday':
                prueba = 'Martes'
            if prueba == 'Wednesday':
                prueba = 'Miercoles'
            if prueba == 'Thursday':
                prueba = 'Jueves'
            if prueba == 'Friday':
                prueba = 'Viernes'
            if prueba == 'Saturday':
                prueba = 'Sabado'
            if prueba == 'Sunday':
                prueba = 'Domingo'
                    #
            if prueba in frecuencia:
                for itemoc in querytablados:
                    querypersona = DBSession.query(Persona).filter_by(id_persona=itemoc.operador_id).first()
                    querypersona = DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(
                            id_persona=itemoc.operador_id).first()
                    if querypersona is not None:
                        querycondonacioneoperadores = DBSession.query(CondonacionOperador).filter_by(
                                operador_id=querypersona.id_persona).filter_by(
                                cargooperador_id=itemco.id_cargooperador).all()

                        if querycondonacioneoperadores != []:
                            condonacion = 0
                            for itemcondo in querycondonacioneoperadores:
                                querycondonacion = DBSession.query(Condonaciones).filter_by(
                                        id_condonacion=itemcondo.condonacion_id) \
                                        .filter_by(cancelado=0).filter(
                                        Condonaciones.fechainicial <= datetime.now().date() - timedelta(days=2)).filter(
                                        Condonaciones.fechafinal >= datetime.now().date() - timedelta(days=2)).first()
                                if querycondonacion is not None:
                                    condonacion = condonacion + querycondonacion.monto
                                else:
                                    condonacion = condonacion
                        else:
                            condonacion = 0

                        print("itemoc.monto",itemoc.monto)
                        if itemoc.monto > 0:
                            newbody = Body()
                            newbody.monto = itemoc.monto
                            if condonacion > newbody.monto:
                                newbody.condonacion = 0
                            else:
                                newbody.condonacion = condonacion
                            newbody.saldo = itemoc.monto - newbody.condonacion
                            newbody.pagado = 0
                            newbody.fecha = datetime.now().date() - timedelta(days=2)
                            newbody.id_operador = querypersona.id_persona
                            newbody.id_cargopersona = itemco.id_cargooperador
                            newbody.internal_id = internal_id
                            DBSession.add(newbody)
            #
        queryunidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(
                internal_id=internal_id).all()
        queryoperadores = DBSession.query(Persona).filter_by(esoperador=1).filter_by(
                habilitadooperador=1).filter_by(internal_id=internal_id).all()

        for itemunidad in queryunidades:
            saldo = 0
            querybody = DBSession.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(
                    fecha=datetime.now().date() - timedelta(days=2)).all()
            for itembody in querybody:
                print("itembody.saldo=", itembody.saldo)
                saldo = saldo + itembody.saldo
                    #
            if saldo>0:
                newhead = Head()
                newhead.fecha = datetime.now().date() - timedelta(days=2)
                newhead.estatus = 'Por Pagar'
                newhead.porpagar = saldo
                newhead.pagado = 0
                newhead.faltante = saldo
                newhead.id_unidad = itemunidad.id_unidad
                # thequeryoperador=DBSession.query(Persona).filter_by(id_unidad=itemunidad.id_unidad).first()
                # try:
                #     variable=thequeryoperador.id_persona
                # except:
                #     variable=None
                # newhead.id_operador=variable
                newhead.internal_id = internal_id
                newhead.app_name = itemunidad.app_name
                DBSession.add(newhead)
            #
        for itemoperador in queryoperadores:
            saldo = 0
            querybody = DBSession.query(Body).filter_by(id_operador=itemoperador.id_persona).filter_by(
                    fecha=datetime.now().date() - timedelta(days=2)).all()
            for itembody in querybody:
                saldo = saldo + itembody.saldo
            if saldo>0:
                newhead = Head()
                newhead.fecha = datetime.now().date() - timedelta(days=2)
                newhead.estatus = 'Por Pagar'
                newhead.porpagar = saldo
                newhead.pagado = 0
                newhead.faltante = saldo
                newhead.id_operador = itemoperador.id_persona
                newhead.internal_id = internal_id
                newhead.app_name='TODO'
                DBSession.add(newhead)
        DBSession.flush()
        return dict(error='ok')


    # @expose('json')
    # @require(predicates.not_anonymous())
    # def cargaall(self):
    #     internal_id=5
    #     cargosoperadores=DBSession.query(CargoOperador).filter_by(internal_id=internal_id).all()
    #     cargosunidades = DBSession.query(CargoUnidad).filter_by(internal_id=internal_id).all()
    #     fecha = datetime.now().date()
    #     ayer=date.today() - timedelta(1)
    #     print('HOY:' ,fecha)
    #     print('DIA ANTERIOR: ', ayer)
    #     dia=fecha.day
    #     while dia < 26:
    #         fechaingresa=str(fecha.year)+'-'+str(fecha.month)+'-'+str(dia)
    #         fechaingresa=fechaingresa.format('YYYY-MM-DD')
    #         #ayer = fechaingresa - timedelta(1)
    #
    #         for item in cargosoperadores:
    #             operadorcargos=DBSession.query(OperadorCargo).filter_by(cargo_id=item.id_cargooperador).all()
    #
    #             for item2 in operadorcargos:
    #                 queryoperador = DBSession.query(Persona).filter_by(id_persona=item2.operador_id).first()
    #
    #                 queryultimoayer = DBSession.query(EstadoCuenta).filter_by(operador_id=item2.operador_id).all()
    #
    #                 if queryultimoayer == []:
    #                     saldo = 0
    #                 else:
    #                     for item3 in queryultimoayer:
    #                         saldo = item3.saldo
    #
    #
    #                 newestadodecuenta=EstadoCuenta()
    #                 newestadodecuenta.fecha_movimiento=fechaingresa
    #                 newestadodecuenta.motivo=item.cargo
    #                 newestadodecuenta.cargo=item2.monto
    #                 newestadodecuenta.abono=0
    #                 newestadodecuenta.saldo=saldo - item2.monto
    #                 newestadodecuenta.unidad_id=0
    #                 newestadodecuenta.operador_id=item2.operador_id
    #                 DBSession.add(newestadodecuenta)
    #
    #
    #                 print('motivo: ',item.cargo)
    #                 print('cargo: ',item2.monto)
    #                 print('abono: ',0)
    #                 print('saldo: ',saldo - item2.monto)
    #                 print('operador_id: ',item2.operador_id)
    #             print('----')
    #         print('----ACABO OPERADORES----')
    #         for item in cargosunidades:
    #             unidadcargos=DBSession.query(UnidadCargo).filter_by(cargo_id=item.id_cargounidad).all()
    #             for item2 in unidadcargos:
    #                 queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=item2.unidad_id).first()
    #                 queryedodecuentaanterior = DBSession.query(EstadoCuenta).filter_by(unidad_id=item2.unidad_id).all()
    #                 if queryedodecuentaanterior == []:
    #                     saldo = 0
    #                 else:
    #                     for item3 in queryedodecuentaanterior:
    #                         saldo = item3.saldo
    #                 newestadodecuenta=EstadoCuenta()
    #                 newestadodecuenta.fecha_movimiento = fechaingresa
    #                 newestadodecuenta.motivo=item.cargo
    #                 newestadodecuenta.cargo=item2.monto
    #                 newestadodecuenta.abono=0
    #                 newestadodecuenta.saldo=saldo - item2.monto
    #                 newestadodecuenta.unidad_id=item2.unidad_id
    #                 newestadodecuenta.operador_id=0
    #                 DBSession.add(newestadodecuenta)
    #
    #
    #                 print('motivo: ',item.cargo)
    #                 print('cargo: ',item2.monto)
    #                 print('abono: ',0)
    #                 print('saldo: ',saldo - item2.monto)
    #                 print('unidad_id: ',item2.unidad_id)
    #
    #
    #             print('----')
    #         print('----ACABO UNIDADES----')
    #         dia=dia+1
    #     return dict(error='ok')

    # @expose('pythonjupiter.templates.login')
    # def login(self, came_from=lurl('/'), failure=None, login=''):
    #     """Start the user login."""
    #     if failure is not None:
    #         if failure == 'user-not-found':
    #             flash(_('User not found'), 'error')
    #         elif failure == 'invalid-password':
    #             flash(_('Invalid Password'), 'error')
    #
    #     login_counter = request.environ.get('repoze.who.logins', 0)
    #     if failure is None and login_counter > 0:
    #         flash(_('Wrong credentials'), 'warning')
    #
    #     return dict(page='login', login_counter=str(login_counter),
    #                 came_from=came_from, login=login)
    #
    # @expose()
    # def post_login(self, came_from=lurl('/')):
    #     """
    #     Redirect the user to the initially requested page on successful
    #     authentication or redirect her back to the login page if login failed.
    #
    #     """
    #     if not request.identity:
    #         login_counter = request.environ.get('repoze.who.logins', 0) + 1
    #         redirect('/login',
    #                  params=dict(came_from=came_from, __logins=login_counter))
    #     userid = request.identity['repoze.who.userid']
    #     flash(_('Welcome back, %s!') % userid)
    #
    #     # Do not use tg.redirect with tg.url as it will add the mountpoint
    #     # of the application twice.
    #     return HTTPFound(location=came_from)
    #
    # @expose()
    # def post_logout(self, came_from=lurl('/')):
    #     """
    #     Redirect the user to the initially requested page on logout and say
    #     goodbye as well.
    #
    #     """
    #     flash(_('We hope to see you soon!'))
    #     return HTTPFound(location=came_from)


