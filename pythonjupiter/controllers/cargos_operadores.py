from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora,Grupo,Persona,CargoUnidad,UnidadCargo,HistorialPersona
from pythonjupiter.model.tables import CargoOperador,OperadorCargo,CuentaCargo,GrupoPersona,GrupoPersona_Persona
import os
import requests
from tg import app_globals
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.error import ErrorController
from shutil import copyfileobj,copyfile
from datetime import datetime
from pythonjupiter.lib.messagequeue import Message
from tg import render_template
from sqlalchemy import or_


class CargosOperadoresTemplates(BaseController):
    @expose('pythonjupiter.templates.CargosOperadorTemplates.cargosoperador')
    def cargo_operadores(self,**kw):

        allrecords=DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).all()
        contador=len(allrecords)

        return dict(page='cargosunidades',app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],
                    allrecords=allrecords,contador=contador)

    @expose('pythonjupiter.templates.CargosOperadorTemplates.nuevocargooperador')
    def nuevocargooperador(self,**kw):

        allempresas = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        allgrupos = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()

        return dict(page='nuevocargounidad',app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],
                    allgrupos=allgrupos,allempresas=allempresas)

    @expose('json')
    def uploadCargoOperador(self,**kw):
        print("entre")
        #print(kw)
        try:
            seleccion = kw['selecciongrupoempresa']
        except:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "Elige por lo menos grupo o empresa y los dias!")
            return ''

        try:
            diasseleccion=kw['diaseleccion']
            tipocargo=kw['tipo_cargo']
        except KeyError:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "Elige por lo menos un dia!")
            return ''


        queryexiste=DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()
        if queryexiste is None:
            print("NO EXISTE")
            querycuentacargo=DBSession.query(CuentaCargo).filter_by(application_id=kw['application_id']).filter_by(nombre=kw['jupiter_chargename']).first()
            if querycuentacargo is None:
                newcuentacargo=CuentaCargo()
                newcuentacargo.nombre=kw['jupiter_chargename']
                newcuentacargo.total=0
                newcuentacargo.internal_id=kw['internal_id']
                newcuentacargo.application_id=kw['application_id']
                newcuentacargo.app_name=kw['app_name']
                #newcuentacargo.app_name = ''
            else:
                Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "La bolsita ya existe")
                return ''
            newcargo=CargoOperador()
            newcargo.cargo=kw['jupiter_chargename']
            newcargo.montodefault=kw['jupiter_amountdefault']
            newcargo.tipodecargo=kw['tipo_cargo']
            newcargo.app_name=kw['app_name']
            newcargo.application_id=kw['application_id']
            newcargo.internal_id=kw['internal_id']
            print("PRIMERA SECCION")
            ######################################
            #print("companiescheck-->",kw['companiescheck'])
            if kw['companiescheck']== 'true':
                newcargo.empresa=kw['jupiter_selectcompaniesforcharges']
                print("ENTRE AL IF DE COMPANIES")
            else:
                newcargo.empresa=''

            #########################################
            #print("grupospersonascheck-->", kw['grupospersonascheck'])
            if kw['grupospersonascheck'] == 'true':
                newcargo.grupopersona=kw['jupiter_selectgrupospersonasforcharges']
                print("ENTRE AL IF DE GRUPOS")
            else:
                newcargo.grupopersona=''
            #########################################

            if int(kw['tipo_cargo'])==1 or int(kw['tipo_cargo'])==2 or int(kw['tipo_cargo'])==3:
                anexo=''
                if kw['jupiter_chargeunidadallweek']=='true':
                    newcargo.frecuencia='Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo'

                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Insertado")
                else:
                    anexo=''

                    if kw['jupiter_chargeunidadlunes']=='true':
                        anexo='Lunes,'
                    else:
                        anexo=''


                    if kw['jupiter_chargeunidadmartes']=='true':
                        anexo=anexo+'Martes,'
                    else:
                        anexo=anexo

                    if kw['jupiter_chargeunidadmiercoles']=='true':
                        anexo=anexo+'Miercoles,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadjueves']=='true':
                        anexo=anexo+'Jueves,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadviernes']=='true':
                        anexo=anexo+'Viernes,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidadsabado']=='true':
                        anexo=anexo+'Sabado,'
                    else:
                        anexo=anexo


                    if kw['jupiter_chargeunidaddomingo']=='true':
                        anexo=anexo+'Domingo'
                    else:
                        anexo=anexo

                    newcargo.frecuencia=anexo
            else:
                    pass
            DBSession.add(newcargo)
            querynuevocargo=DBSession.query(CargoOperador).filter_by(cargo=kw['jupiter_chargename']).filter_by(application_id=kw['application_id']).first()
            newcuentacargo.cargooperador_id = querynuevocargo.id_cargooperador
            DBSession.add(newcuentacargo)
            DBSession.flush()

            if kw['permiso']=='1':
                querycargonuevo=DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()


                if querycargonuevo.empresa=='TODAS':
                    querylinea=DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
                elif querycargonuevo.empresa=='':
                    querylinea={}
                else:
                    querylinea = DBSession.query(Linea).filter_by(app_id=kw['application_id']).filter_by(nombre_linea=querycargonuevo.empresa).all()

                for item in querylinea:
                    querypersona=DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(id_linea=item.id_linea).filter_by(esoperador=1).all()
                    for item2 in querypersona:
                        queryexisteoperadorcargo = DBSession.query(OperadorCargo).filter_by(cargo_id=querycargonuevo.id_cargooperador) \
                                    .filter_by(operador_id=item2.id_persona).first()
                        if queryexisteoperadorcargo is None:
                            newpersonacargo = OperadorCargo()
                            querycontador = DBSession.query(OperadorCargo).all()
                            contador = 0
                            for item in querycontador:
                                contador = item.id
                            contador = contador + 1
                            newpersonacargo.id = contador
                            newpersonacargo.cargo_id = querycargonuevo.id_cargooperador
                            newpersonacargo.operador_id = item2.id_persona
                            newpersonacargo.monto = kw['jupiter_amountdefault']
                            DBSession.add(newpersonacargo)



                if querycargonuevo.grupopersona == 'TODAS':
                    querygrupo = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()
                elif querycargonuevo.grupopersona == '':
                    querygrupo = {}
                else:
                    querygrupo = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).filter_by(grupopersona=querycargonuevo.grupopersona).all()


                for item in querygrupo:
                    querytabla=DBSession.query(GrupoPersona_Persona).filter_by(grupo_id=item.id_grupopersona).all()
                    for itemtabla in querytabla:
                        querypersona = DBSession.query(Persona).filter_by(id_persona=itemtabla.persona_id).filter_by(esoperador=1).all()
                        for item2 in querypersona:
                            queryexisteoperadorcargo = DBSession.query(OperadorCargo).filter_by(
                                    cargo_id=querycargonuevo.id_cargooperador) \
                                    .filter_by(operador_id=item2.id_persona).first()
                            if queryexisteoperadorcargo is None:
                                newpersonacargo = OperadorCargo()
                                querycontador = DBSession.query(OperadorCargo).all()
                                contador = 0
                                for item in querycontador:
                                    contador = item.id
                                contador = contador + 1
                                newpersonacargo.id = contador
                                newpersonacargo.cargo_id = querycargonuevo.id_cargooperador
                                newpersonacargo.operador_id = item2.id_persona
                                newpersonacargo.monto = kw['jupiter_amountdefault']
                                DBSession.add(newpersonacargo)

            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Insertado")

            return ''
        else:

            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "El cargo ya existe")
            return ''




    @expose('pythonjupiter.templates.CargosOperadorTemplates.editcargooperador')
    def editcargo(self,**kw):
        querytoedit=DBSession.query(CargoOperador).filter_by(id_cargooperador=kw['id']).first()

        allempresas = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
        allgrupos = DBSession.query(GrupoPersona).filter_by(application_id=kw['application_id']).all()



        dias=querytoedit.frecuencia.split(',')
        contadordias=len(dias)
        if contadordias==7:
            try:
                dias.remove('')
                contadordias = len(dias)
            except:
                pass

        return dict(page='editcargounidad',user=kw['user'],app_name=kw['app_name'],application_id=kw['application_id'],internal_id=kw['internal_id'],
                    allgrupos=allgrupos,querytoedit=querytoedit,dias=dias,contadordias=contadordias,allempresas=allempresas)


    @expose('json')
    def editamonto(self,**kw):
        monto=kw['monto']
        id=kw['id']
        querycargo=DBSession.query(CargoOperador).filter_by(id_cargooperador=id).first()
        queryunidad_cargo=DBSession.query(OperadorCargo).filter_by(cargo_id=querycargo.id_cargooperador).all()
        for item in queryunidad_cargo:
            if item.monto != int(monto):
                newhistorialpersona = HistorialPersona()
                querycargo = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(id_cargooperador=item.cargo_id).first()
                newhistorialpersona.dato = querycargo.cargo+' monto'
                newhistorialpersona.valor_nuevo = monto
                newhistorialpersona.valor_anterior = item.monto
                querypersona = DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(id_persona=item.operador_id).first()
                newhistorialpersona.sobrecampo = querypersona.id_persona
                newhistorialpersona.usuario = kw['user']
                item.monto=monto
                DBSession.add(newhistorialpersona)
        return dict(data='Monto Editado a todos')


    @expose('json')
    def editandoCargoOperador(self,**kw):
        #print(kw)
        querynombrenuevo=DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).filter(CargoOperador.id_cargooperador!=kw['id']).first()
        if querynombrenuevo is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "El Cargo ya existe")
            return ''
        queryexiste=DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(id_cargooperador=kw['id']).first()

        queryexiste2 = DBSession.query(CargoUnidad).filter_by(application_id=kw['application_id']).filter_by(cargo=kw['jupiter_chargename']).first()
        if queryexiste2 is not None:
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'RED' + "|" + "Cargo Existe en Unidad")
            return ''
        if queryexiste is not None:
            #newcargo=CargoUnidad()

            queryexiste.cargo=kw['jupiter_chargename']
            querycuentacargo=DBSession.query(CuentaCargo).filter_by(application_id=kw['application_id']).filter_by(nombre=queryexiste.cargo).first()
            if querycuentacargo is not None:
                querycuentacargo.nombre=kw['jupiter_chargename']
            DBSession.flush()
            queryexiste.montodefault=kw['jupiter_amountdefault']
            #queryexiste.app_name=kw['jupiter_selectapp_nameforcharges']
            queryexiste.internal_id=kw['internal_id']
            queryexiste.tipodecargo=int(kw['tipocargooperador'])
            ######################################

            try:
                a=kw['companiescheck']
                queryexiste.empresa=kw['jupiter_selectcompaniesforcharges']
            except:
                queryexiste.empresa=''

            #########################################

            try:
                a=kw['grupospersonascheck']
                queryexiste.grupopersona=kw['jupiter_selectgrupospersonasforcharges']
            except:
                queryexiste.grupopersona=''
            #########################################

            if int(kw['tipocargooperador'])==1 or int(kw['tipocargooperador'])==2 or int(kw['tipocargooperador'])==3:
                try:
                    anexo=kw['jupiter_chargeunidadallweek']
                    queryexiste.frecuencia='Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo'
                    Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Editado")

                except:
                    anexo=''

                    try:
                        veri=kw['jupiter_chargeunidadlunes']
                        anexo='Lunes,'
                    except:
                        anexo=''

                    try:
                        veri=kw['jupiter_chargeunidadmartes']
                        anexo=anexo+'Martes,'
                    except:
                        anexo=anexo


                    try:
                        veri=kw['jupiter_chargeunidadmiercoles']
                        anexo=anexo+'Miercoles,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadjueves']
                        anexo=anexo+'Jueves,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadviernes']
                        anexo=anexo+'Viernes,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidadsabado']
                        anexo=anexo+'Sabado,'
                    except:
                        anexo=anexo

                    try:
                        veri=kw['jupiter_chargeunidaddomingo']
                        anexo=anexo+'Domingo'
                    except:
                        anexo=anexo


                #print("ANEXO: ",anexo)
                    queryexiste.frecuencia=anexo
            else:
                    pass
            Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Cargo Editado")
        return ''


    @expose('json')
    def actualizacargosoperadores(self,**kw):
        empresa=kw['empresa']
        grupo=kw['grupo']
        splitgrupo=grupo.split(",")
        splitgrupo.remove('')
        construiraux = []
        construir = []
        #print(splitgrupo)


        cargosempresa = DBSession.query(CargoOperador).filter_by(empresa=empresa).filter_by(application_id=kw['application_id']).all()
        for itemsplit in splitgrupo:
            cargosgrupo = DBSession.query(CargoOperador).filter_by(grupopersona=itemsplit).filter_by(application_id=kw['application_id']).all()
            for item in cargosgrupo:
                if item.cargo in construiraux:
                    pass
                else:
                    construiraux.append(item.cargo)
                    construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        cargosapp_nametodas = DBSession.query(CargoOperador).filter_by(empresa='TODAS').filter_by(application_id=kw['application_id']).all()
        if grupo!='Sin Grupo':
            cargosgrupostodas = DBSession.query(CargoOperador).filter_by(grupopersona='TODAS').filter_by(application_id=kw['application_id']).all()
        else:
            cargosgrupostodas={}


        for item in cargosempresa:
            if item.cargo in construiraux:
                pass
            else:
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        for item in cargosgrupostodas:
            if item.cargo in construiraux:
                pass
            else:
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo, 'monto': item.montodefault})

        for item in cargosapp_nametodas:
            if item.cargo in construiraux:
                pass
            else:
                construiraux.append(item.cargo)
                construir.append({'cargo': item.cargo, 'monto': item.montodefault})


        construir2=[]
        if 'id' in kw:
            #print("construir: ",construir)
            for elem in construir:
                querycargo=DBSession.query(CargoOperador).filter_by(cargo=elem['cargo']).first()
                querycargos=DBSession.query(OperadorCargo).filter_by(cargo_id=querycargo.id_cargooperador).filter_by(operador_id=kw['id']).first()
                if querycargos is not None:
                    construir2.append({'cargo': elem['cargo'],'monto': querycargos.monto})
                else:
                    construir2.append({'cargo': elem['cargo'], 'monto': 0})



        return dict(error='ok',construir=construir,construir2=construir2)


    @expose('json')
    def eliminacargooperador(self,**kw):
        print("ENTRE PA BOPRARR")
        queryeliminar = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).filter_by(id_cargooperador=kw['id']).first()
        print("queryeliminar=..",queryeliminar)
        queryexisteoperador=DBSession.query(OperadorCargo).filter_by(cargo_id=queryeliminar.id_cargooperador).first()
        print("queryexisteoperador=..", queryexisteoperador)
        if queryeliminar is not None:
            if queryexisteoperador is None:
                querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargooperador_id=queryeliminar.id_cargooperador).filter_by(application_id=queryeliminar.application_id).first()
                print("querycuentacargo==>",querycuentacargo)
                if querycuentacargo is not None:
                    print("ELIMINARE CARGO CUENTA")
                    DBSession.delete(querycuentacargo)
                    print("ELIMINARE CARGO CUENTA")
                    DBSession.flush()
                    DBSession.delete(queryeliminar)
                    DBSession.flush()

                    return dict(error='Eliminado exitosamente')
                else:
                    return dict(error='El Cargo no se puede eliminar')
            else:
                return dict(error='El Cargo esta asiganado a un operador no se puede eliminar')
        else:
            return dict(error='No existe el id')


    @expose('json')
    def searchcargooperador(self,**kw):

        listabusca = DBSession.query(CargoOperador).filter(CargoOperador.cargo.like('%' + kw['name'] + '%')).filter_by(application_id=kw['application_id']).all()
        contador = len(listabusca)



        return dict(listabusca=listabusca,contador=contador)


    @expose('json')
    def reloadcargosoperador(self,**kw):
        allrecords = DBSession.query(CargoOperador).filter_by(application_id=kw['application_id']).all()
        contador = len(allrecords)
        return dict(listabusca=allrecords,contador=contador)

