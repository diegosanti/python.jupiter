from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonjupiter import model
from pythonjupiter.controllers.secure import SecureController
from pythonjupiter.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
from pythonjupiter.model.tables import DatosUnidad,Linea,HistorialUnidades,TipoUnidad,TipoTarjeta,Derrotero,Aseguradora
from pythonjupiter.model.tables import TipoLicencia,Reporte,ReportePersona,Persona
import os
import requests
from tg import app_globals
from pythonjupiter.lib.base import BaseController
from pythonjupiter.controllers.error import ErrorController
from shutil import copyfileobj,copyfile
from datetime import datetime
from tg import render_template
from pythonjupiter.lib.messagequeue import Message


class ReportesTemplates(BaseController):
    @expose('pythonjupiter.templates.CastigosTemplates.castigos')
    def castigos(self,**kw):
        allrecords=DBSession.query(ReportePersona).filter_by(internal_id=kw['internal_id']).all()
        lista=[]
        for item in allrecords:
            queryoperador=DBSession.query(Persona).filter_by(id_persona=item.persona_id).first()
            if queryoperador is not None:
                variableoperador=queryoperador.nombre
                variableapodo=queryoperador.apodo
            else:
                variableoperador=''
                variableapodo=''

            queryreporte=DBSession.query(Reporte).filter_by(id_reporte=item.reporte_id).first()

            if queryreporte is not None:
                variablereporte=queryreporte.reporte
            else:
                variablereporte=''

            lista.append({"id": item.id,"fecha": item.fecha,"castigo": variablereporte,"magnitud": item.gravedad,"operador": variableoperador,"apodo": variableapodo,"unidad": item.unidad,"descripcion": item.descripcion})

        contador=len(lista)
        return dict(page='castigos',lista=lista,app_name=kw['app_name'],application_id=kw['application_id'],user=kw['user'],internal_id=kw['internal_id'],contador=contador)

    @expose('pythonjupiter.templates.CastigosTemplates.nuevoreporte')
    def nuevoreporte(self,**kw):
        allcastigos=DBSession.query(Reporte).all()
        return dict(page='nuevoreporte',allcastigos=allcastigos,user=kw['user'],internal_id=kw['internal_id'],app_name=kw['app_name'],application_id=kw['application_id'])

    @expose('json')
    def insertaoperador(self,**kw):
        searchfor=kw['searchfor']
        what=kw['what']
        parameterdata={}
        if searchfor=='credencial':
            parameterdata = DBSession.query(Persona).filter_by(id_persona=int(what)).all()
        if searchfor=='nombre':
            parameterdata=DBSession.query(Persona).filter(Persona.nombre.like('%'+what+'%')).all()
        if searchfor == 'apodo':
            parameterdata = DBSession.query(Persona).filter(Persona.apodo.like('%'+what+'%')).all()
        return dict(data=parameterdata)

    @expose('json')
    def insertaunidad(self,**kw):
        searchfor=kw['searchfor']
        what=kw['what']
        #print("searchfor: ",searchfor)
        #print("what: ",what)
        parameterdata={}
        if searchfor == 'eco1':
             parameterdata = DBSession.query(DatosUnidad).filter_by(eco1=what).first()
        if searchfor == 'eco2':
            parameterdata = DBSession.query(DatosUnidad).filter_by(eco2=what).first()


        if parameterdata is not None:
            eco1=parameterdata.eco1
        else:
            eco1='nada'

        return {'eco1': eco1}

    @expose('json')
    def uploadReporte(self,**kw):
        #print(kw)
        if kw['inputcastigo'] != '':
            newitemreporte=Reporte()
            newitemreporte.reporte=kw['inputcastigo']
            newitemreporte.internal_id=kw['internal_id']
            DBSession.add(newitemreporte)

            reporte_query = DBSession.query(Reporte).filter_by(reporte=kw['inputcastigo']).first()
        else:
            reporte_query = DBSession.query(Reporte).filter_by(reporte=kw['selectcastigos']).first()


        separa=kw['readonlyoperador'].split('/')
        query_operador = DBSession.query(Persona).filter_by(apodo=separa[1]).first()
        query_contar=DBSession.query(ReportePersona).all()
        if query_contar is []:
            contador=1
        else:
            contador=len(query_contar)+1
        newitemreporte_operador=ReportePersona()
        newitemreporte_operador.id=contador
        newitemreporte_operador.reporte_id=reporte_query.id_reporte
        newitemreporte_operador.persona_id=query_operador.id_persona
        newitemreporte_operador.usuario=kw['user']
        newitemreporte_operador.internal_id=kw['internal_id']
        newitemreporte_operador.unidad=kw['readonlyunidad']
        newitemreporte_operador.gravedad=int(kw['selectgravedad'])
        newitemreporte_operador.descripcion=kw['jupiter_descripcionreporte']
        DBSession.add(newitemreporte_operador)
        #DBSession.flush()
        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Reporte dado de alta")
        return ''


    @expose('pythonjupiter.templates.CastigosTemplates.editreporte')
    def editreporte(self,**kw):
        allcastigos = DBSession.query(Reporte).all()
        id=kw['id']
        sobrecastigo=DBSession.query(ReportePersona).filter_by(id=id).first()
        valueofreporte=DBSession.query(Reporte).filter_by(id_reporte=sobrecastigo.reporte_id).first()
        variablereporte=valueofreporte.reporte
        variablegravedad=sobrecastigo.gravedad
        variableunidad=sobrecastigo.unidad
        valueofoperador=DBSession.query(Persona).filter_by(id_persona=sobrecastigo.persona_id).first()
        variableoperador=valueofoperador.nombre
        variableapodo=valueofoperador.apodo
        variabledescripcion=sobrecastigo.descripcion
        return dict(page='nuevoreporte', allcastigos=allcastigos, user=kw['user'], internal_id=kw['internal_id'],app_name=kw['app_name'], application_id=kw['application_id'],id=id,
                    variablereporte=variablereporte,variablegravedad=variablegravedad,variableunidad=variableunidad,variabledescripcion=variabledescripcion,variableoperador=variableoperador,variableapodo=variableapodo)

    @expose('json')
    def editandoreporte(self,**kw):
        #print(kw)
        #{'selectgravedad': '5', 'internal_id': '6', 'readonlyoperador': 'PINGUINO/PINGU', 'selectcastigos': 'ggggg', 'inputcastigo': '', 'jupiter_submit_uploadreporte': 'GUARDAR',
        # 'readonlyunidad': 'Eco_338', 'user': 'diego', 'id': '4'}
        querytoedit=DBSession.query(ReportePersona).filter_by(id=kw['id']).first()
        querytoedit.usuario=kw['user']
        querytoedit.unidad=kw['readonlyunidad']
        querytoedit.gravedad=kw['selectgravedad']
        querytoedit.descripcion=kw['jupiter_descripcionreporte']

        if kw['inputcastigo']=='':
            queryreporte=DBSession.query(Reporte).filter_by(reporte=kw['selectcastigos']).first()
            querytoedit.reporte_id=queryreporte.id_reporte
            separa=kw['readonlyoperador'].split('/')
            queryoperador=DBSession.query(Persona).filter_by(apodo=separa[1]).first()
            querytoedit.persona_id=queryoperador.id_persona
        else:
            newreporte=Reporte()
            newreporte.reporte=kw['inputcastigo']
            newreporte.internal_id=kw['internal_id']
            DBSession.add(newreporte)
            querynew=DBSession.query(Reporte).filter_by(reporte=kw['inputcastigo']).first()
            querytoedit.reporte_id=querynew.id_reporte
            separa = kw['readonlyoperador'].split('/')
            queryoperador=DBSession.query(Persona).filter_by(apodo=separa[1]).first()
            querytoedit.persona_id=queryoperador.id_persona

        Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Reporte Editado")
        return ''



    @expose('json')
    def ecostemplate(self,**kw):
        template=render_template({}, "mako", 'pythonjupiter.templates.CastigosTemplates.jqGridecos')
        return dict(template=template)

    @expose('json')
    def reloadreportes(self, **kw):
        allrecords=DBSession.query(ReportePersona).all()
        lista=[]
        for item in allrecords:
            queryoperador=DBSession.query(Persona).filter_by(id_persona=item.persona_id).first()
            if queryoperador is not None:
                variableoperador=queryoperador.nombre
                variableapodo=queryoperador.apodo
            else:
                variableoperador=''
                variableapodo=''

            queryreporte=DBSession.query(Reporte).filter_by(id_reporte=item.reporte_id).first()

            if queryreporte is not None:
                variablereporte=queryreporte.reporte

            else:
                variablereporte=''


            #print("variablereporte:",variablereporte)
            lista.append({"id": item.id,"fecha": item.fecha,"castigo": variablereporte,"magnitud": item.gravedad,"operador": variableoperador,"apodo": variableapodo,"unidad": item.unidad,"descripcion": item.descripcion})

        contador = len(lista)
        return dict(error='Ok', lista=lista,contador=contador)

    @expose('json')
    def searchreporte(self,**kw):
        lista=[]
        searchfor=kw['searchfor']
        name=kw['name']
        if searchfor=='id' or searchfor=='fecha' or searchfor=='unidad' or searchfor=='gravedad':
            queryprueba=DBSession.query(ReportePersona).filter(getattr(ReportePersona, searchfor).like('%'+name+'%')).all()
            if queryprueba is not []:
                for item in queryprueba:
                    query_reporte = DBSession.query(Reporte).filter_by(id_reporte=item.reporte_id).first()
                    if query_reporte is not None:
                        variablereporte = query_reporte.reporte

                    else:
                        variablereporte = ''

                    query_operador = DBSession.query(Persona).filter_by(id_persona=item.persona_id).first()
                    if query_operador is not None:
                        variableoperador = query_operador.nombre
                        variableapodo= query_operador.apodo
                    else:
                        variableoperador = ''
                        variableapodo=''
                    lista.append({"id": item.id, "fecha": item.fecha, "castigo": variablereporte, "magnitud": item.gravedad,"operador": variableoperador,"apodo": variableapodo,"unidad": item.unidad,"descripcion": item.descripcion})

        if searchfor=='reporte':
            query_reporte=DBSession.query(Reporte).filter(getattr(Reporte, searchfor).like('%'+name+'%')).all()

            if query_reporte is not []:

                for item in query_reporte:
                    query_reporteoperador=DBSession.query(ReportePersona).filter_by(reporte_id=item.id_reporte).all()
                    if query_reporteoperador is not []:
                        for item2 in query_reporteoperador:
                            variableid=item2.id
                            variablefecha=item2.fecha
                            variableunidad=item2.unidad
                            variablegravedad=item2.gravedad
                            variabledescripcion=item2.descripcion
                            query_operador = DBSession.query(Persona).filter_by(id_persona=item2.persona_id).first()
                            if query_operador is not None:
                                variableoperador = query_operador.nombre
                                variableapodo=query_operador.apodo
                            else:
                                variableoperador = ''
                                variableapodo=''

                            lista.append({"id": variableid, "fecha": variablefecha, "castigo": item.reporte,"magnitud": variablegravedad, "operador": variableoperador,"apodo": variableapodo,"unidad": variableunidad,"descripcion": variabledescripcion})


        if searchfor=='operador' or searchfor=='apodo':
            if searchfor=='operador':
                searchfor='nombre'
            query_operador=DBSession.query(Persona).filter(getattr(Persona,searchfor).like('%'+name+'%')).all()
            if query_operador is not []:
                for item in query_operador:
                    query_reporteoperador=DBSession.query(ReportePersona).filter_by(persona_id=item.id_persona).all()
                    if query_reporteoperador is not []:
                        for item2 in query_reporteoperador:
                            variableid=item2.id
                            variablefecha=item2.fecha
                            variableunidad=item2.unidad
                            variablegravedad=item2.gravedad
                            variabledescripcion=item2.descripcion
                            query_reporte = DBSession.query(Reporte).filter_by(id_reporte=item2.reporte_id).first()
                            if query_reporte is not None:
                                variablereporte=query_reporte.reporte

                            else:
                                variablereporte=''

                            lista.append({"id": variableid, "fecha": variablefecha, "castigo": variablereporte, "magnitud": variablegravedad,"operador": item.nombre,"apodo": item.apodo, "unidad": variableunidad,"descripcion": variabledescripcion})

        contador = len(lista)
        return dict(error='Ok', lista=lista,contador=contador)



