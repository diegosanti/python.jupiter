from tg import expose
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import DatosUnidad,Linea,Persona
from pythonjupiter.lib.base import BaseController
from tg.i18n import lazy_ugettext as l_
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from tg import render_template


class LineasTemplates(BaseController):


    @expose('json')
    def load(self, **kw):
        filter = [('app_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Linea, 'id_linea', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        filter = [('app_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(Linea, 'id_linea', filter, kw).updateGrid()

    @expose('json')
    def delete(self, **kw):
        handler = DBSession.query(Linea).filter_by(id_linea=kw['id']).first()
        if handler.unidades != []:
            return dict(error=l_('This company is in use'))
        DBSession.delete(handler)
        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def save(self, **kw):
        if kw['id'] == "0":
            handler = DBSession.query(Linea).filter_by(nombre_linea=kw['nombre_linea']).filter_by(internal_id=kw['internal_id']).first()
            if handler == None:
                handler = Linea()
            else:
                return dict(error=l_('This company alredy exist'))

            i = 0
        else:
            handler = DBSession.query(Linea).filter_by(id_linea=kw['id']).first()
            i = 1
        handler.nombre_linea = kw['nombre_linea']
        handler.internal_id = kw['internal_id']
        handler.app_id = kw['app_id']
        handler.app_name = kw['app_name']

        if i == 0:
            DBSession.add(handler)
        DBSession.flush()
        return dict(error="ok", id=handler.id_linea)

    @expose('json')
    def form(self, **kw):
        if kw['id'] == "0":
            kw['handler'] = Linea()
        else:
            kw['handler'] = DBSession.query(Linea).filter_by(id_linea=kw['id']).first()
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.LinesTemplates.lines_form')
        return dict(dialogtemplate=dialogtemplate)

    @expose('pythonjupiter.templates.LinesTemplates.lines_jqgrid')
    # @require(predicates.not_anonymous())
    def lineas(self, **kw):
        return dict(page='lineas_jqgrid',internal_id=kw['internal_id'], app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'])

    # #DIEGO
    # @expose('pythonjupiter.templates.LinesTemplates.lineas')
    # #@require(predicates.not_anonymous())
    # def lineas(self, **kw):
    #     internal_id = kw['internal_id']
    #     list = [{'show': 'id_linea'}, {'show': 'nombre_linea'}]
    #     handler = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
    #     hd = []
    #     for item in handler:
    #         hd.append({"nombre_linea": item.nombre_linea, "id_linea": item.id_linea})
    #     contador=len(hd)
    #     return dict(page='lineas', list=list, hd=hd, internal_id=internal_id, app_name=kw['app_name'], user=kw['user'],application_id=kw['application_id'],contador=contador)

    # @expose('pythonjupiter.templates.LinesTemplates.newlinea')
    # def newlinea(self, **kw):
    #     return dict(page='newlinea',user=kw['user'][0],internal_id=kw['internal_id'],app_id=kw['application_id'],app_name=kw['app_name'])
    #
    # @expose('json')
    # def uploadlinea(self, **kw):
    #     querylinea=DBSession.query(Linea).filter_by(nombre_linea=kw['nombre_linea']).filter_by(app_id=kw['app_id']).first()
    #     if querylinea is None:
    #         newlinea = Linea()
    #         newlinea.internal_id = kw['internal_id']
    #         newlinea.app_id=kw['app_id']
    #         newlinea.app_name=kw['app_name']
    #         newlinea.nombre_linea = kw['nombre_linea']
    #         DBSession.add(newlinea)
    #         Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Empresa Ingresada")
    #         return dict(error='ok')
    #     else:
    #         Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "La Empresa ya Existe")
    #         return dict(error='existe')
    #
    # @expose('pythonjupiter.templates.LinesTemplates.editlinea')
    # def editlinea(self, **kw):
    #     id = kw['id']
    #     toedit = DBSession.query(Linea).filter_by(id_linea=id).first()
    #     nombre_linea = toedit.nombre_linea
    #     id_linea = toedit.id_linea
    #     int_id = toedit.internal_id
    #     return dict(page='editlinea', toedit=toedit,nombre_linea=nombre_linea,
    #                 id=id_linea, internal_id=int_id,user=kw['user'],app_id=kw['application_id'],app_name=kw['app_name'])
    #
    # @expose('json')
    # def editLine(self, **kw):
    #     id = kw['id']
    #     query = DBSession.query(Linea).filter_by(id_linea=id).first()
    #     queryexiste=DBSession.query(Linea).filter_by(nombre_linea=kw['nombre_linea']).filter_by(app_id=kw['app_id']).first()
    #     if queryexiste is None:
    #         query.nombre_linea=kw['nombre_linea']
    #         Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Empresa Editada")
    #         return dict(error='ok')
    #     else:
    #         Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "La Empresa ya Existe")
    #         return dict(error='no existe')
    #
    # @expose('json')
    # def eliminalinea(self, **kw):
    #     queryeliminar = DBSession.query(Linea).filter_by(id_linea=kw['id']).first()
    #     queryexisteenunidad=DBSession.query(DatosUnidad).filter_by(id_linea=queryeliminar.id_linea).first()
    #     queryexisteoperador = DBSession.query(Persona).filter_by(id_linea=queryeliminar.id_linea).first()
    #     if queryexisteenunidad is None:
    #         if queryexisteoperador is None:
    #             DBSession.delete(queryeliminar)
    #             Message.post("testlistener_" + kw['user'], 'MSG_' + 'GREEN' + "|" + "Eliminado exitosamente")
    #             return dict(error='Eliminado exitosamente')
    #         else:
    #             Message.post("testlistener_" + kw['user'],'MSG_' + 'YELLOW' + "|" + "La Linea esta asiganada a un operador no se puede eliminar")
    #             return dict(error='La Linea esta asiganada a un operador no se puede eliminar')
    #
    #     else:
    #         Message.post("testlistener_" + kw['user'], 'MSG_' + 'YELLOW' + "|" + "La Linea esta asiganada a una unidad no se puede eliminar")
    #         return dict(error='La Linea esta asiganada a una unidad no se puede eliminar')
    #
    # @expose('json')
    # def searchlinea(self, **kw):
    #     listabusca = []
    #     if kw['name'] != '' or kw['name'] != ' ':
    #         list = [{'show': 'id_linea'}, {'show': 'nombre_linea'}]
    #         queryname = DBSession.query(Linea).filter(Linea.nombre_linea.like('%' + kw['name'] + '%')).filter(Linea.app_id == kw['application_id']).all()
    #         if queryname is not []:
    #             for item in queryname:
    #                 listabusca.append({'id_linea': item.id_linea, 'nombre_linea': item.nombre_linea})
    #             contador = len(listabusca)
    #             return dict(error='ok', listabusca=listabusca, list=list,contador=contador)
    #         else:
    #             contador = len(listabusca)
    #             return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list,contador=contador)
    #
    # @expose('json')
    # def reloadlinea(self, **kw):
    #     listabusca = []
    #     list = [{'show': 'id_linea'}, {'show': 'nombre_linea'}]
    #
    #     queryname = DBSession.query(Linea).filter_by(app_id=kw['application_id']).all()
    #     if queryname is not []:
    #         for item in queryname:
    #             listabusca.append({'id_linea': item.id_linea, 'nombre_linea': item.nombre_linea})
    #         contador = len(listabusca)
    #         return dict(error='ok', listabusca=listabusca, list=list,contador=contador)
    #     else:
    #         contador = len(listabusca)
    #         return dict(error='No existe el registro que Intenta Buscar', listabusca=listabusca, list=list,contador=contador)
