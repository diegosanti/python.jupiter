from pythonjupiter.lib.base import BaseController
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import AbonoCuenta
from tg import expose
from datetime import datetime,timedelta
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from pythonjupiter.model.tables import Linea,DatosUnidad,Persona,Derrotero,Grupo,UnidadOperador,EstadoCuenta,Head,Body,CargoUnidad,CargoOperador
from pythonjupiter.model.tables import PagoHead,CuentaCargo,PagoBody,Correos,Grupo_DatosUnidad
from pythonjupiter.lib.utility import ExportPDF
from tg import render_template
from sqlalchemy import or_
#from tgext.mailer.mailer import Mailer
#from tgext.mailer import Message
import requests
from pythonjupiter.lib.helpers import urlsun


class AbonoCuentaController(BaseController):

    def __init__(self):
        pass

    @expose('json')
    def load(self, **kw):
        queryusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        if queryusuario is None:
            if kw['app_name']=='TODOveritodo':
                filter = [('internal_id', 'eq', kw['internal_id']),('fechaingreso','eq',datetime.now().date())]
            else:
                filter = [('application_id', 'eq', kw['appID']), ('internal_id', 'eq', kw['internal_id']),('fechaingreso', 'eq', datetime.now().date())]
        else:
            filter = [('permisionario', 'eq',queryusuario.id_persona), ('application_id', 'eq', kw['appID']),('fechaingreso', 'eq', datetime.now().date())]
        return jqgridDataGrabber(AbonoCuenta, 'id_abonocuenta', filter, kw).loadGrid()

    @expose('json')
    def update(self, **kw):
        print("UPDATE")
        filter = [('application_id', 'eq', kw['application_id'])]
        return jqgridDataGrabber(AbonoCuenta, 'id_abonocuenta', filter, kw).updateGrid()


    @expose('json')
    def searchgrid(self,**kw):
        if kw['por']=='Unidad' and kw['eco']=='-Seleccione-':
            if kw['app_name'] == 'TODOveritodo':
                filter = [('internal_id', 'eq', kw['internal_id']), ('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal'])]
            else:
                filter = [('application_id', 'eq', kw['application_id']), ('internal_id', 'eq', kw['internal_id']),('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal'])]

        elif kw['por'] == 'Operador' and kw['operator'] == '-Seleccione-':
            if kw['app_name'] == 'TODOveritodo':
                filter = [('internal_id', 'eq', kw['internal_id']), ('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal'])]
            else:
                filter = [('application_id', 'eq', kw['application_id']), ('internal_id', 'eq', kw['internal_id']),('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal'])]

        elif kw['por']=='Unidad' and kw['eco']!='-Seleccione-':
            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['eco']).filter_by(app_id=kw['application_id']).first()
            filter = [('application_id', 'eq', kw['application_id']), ('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal']),('unidad_id','eq',queryunidad.id_unidad)]

        elif kw['por'] == 'Operador' and kw['operator'] != '-Seleccione-':
            queryoperador=DBSession.query(Persona).filter_by(nombre=kw['operator']).filter_by(application_id=kw['application_id']).first()
            filter = [('application_id', 'eq', kw['application_id']), ('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal']),('operador_id','eq',queryoperador.id_persona)]
        else:
            filter = [('application_id', 'eq', kw['application_id']), ('fechaingreso', 'eq', datetime.now().date()-timedelta(days=1))]
        return jqgridDataGrabber(AbonoCuenta, 'id_abonocuenta', filter, kw).loadGrid()


    @expose('json')
    def searchgridpersonal(self,**kw):
        queryusuario = DBSession.query(Persona).filter_by(usuario=kw['user']).first()
        filter = [('permisionario', 'eq', queryusuario.id_persona), ('application_id', 'eq', kw['application_id']),('fechaingreso', 'ge', kw['date']),('fechaingreso','le',kw['datefinal'])]
        return jqgridDataGrabber(AbonoCuenta, 'id_abonocuenta', filter, kw).loadGrid()

    @expose('json')
    def save(self,**kw):
        print("SAVE")
        listaheaduni=[]
        listaheadoperador=[]

        listaunibody=[]
        listaopebody=[]
        if kw['abono_id'] == "0":
            abono = AbonoCuenta()
            i = 0
        else:
            abono = DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=kw['abono_id']).first()
            i = 1

        abono.cancelado = 0
        abono.fechaingreso = datetime.now().date()
        abono.fechacuenta = kw['datecuenta']
        try:
            queryunidadglobal = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(eco1=kw['eco']).filter_by(app_id=kw['application_id']).first()
            abono.unidad_id = queryunidadglobal.id_unidad
        except:
            return dict(error="nounidad")
        try:
            querypersonaglobal = DBSession.query(Persona).filter_by(nombre=kw['operador']).filter_by(application_id=kw['application_id']).first()
            abono.operador_id = querypersonaglobal.id_persona
        except:
            return dict(error="nooperador")

        abono.totalapagaruni=int(kw['totaluni'])
        abono.montoingresado = int(kw['monto'])
        #+ int(kw['montooperador'])
        #int(kw['totalapagar'])
        faltauni = int(kw['totaluni']) - int(kw['monto'])
        print("faltauni===>",faltauni)
        if faltauni < 0:
            return dict(error='montomas')

        abono.faltante = faltauni
        abono.total = int(kw['monto'])  - int(faltauni)

        abono.totalapagarope=int(kw['totalope'])
        abono.montoingresadoope = int(kw['montooperador'])
        #+ int(kw['montooperador'])
        #int(kw['totalapagar'])
        faltaoper = int(kw['totalope']) - int(kw['montooperador'])
        print("faltaoper===>", faltaoper)
        if faltaoper < 0:
            return dict(error='montomas')

        abono.faltanteope = faltaoper

        abono.totalope = int(kw['montooperador'])  - int(faltaoper)

        abono.observaciones = kw['observaciones']
        abono.usuario = kw['user']
        abono.internal_id = kw['internal_id']
        abono.application_id=kw['application_id']
        abono.app_name=queryunidadglobal.app_name
        abono.permisionario=queryunidadglobal.id_persona


        newunidadoperador = UnidadOperador()
        newunidadoperador.id_unidad = queryunidadglobal.id_unidad
        newunidadoperador.id_operador = querypersonaglobal.id_persona

        queryunidad=DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(eco1=kw['eco']).filter_by(app_id=kw['application_id']).first()
        querypersona = DBSession.query(Persona).filter_by(nombre=kw['operador']).filter_by(application_id=kw['application_id']).first()
        queryheads = DBSession.query(Head).filter(Head.fecha <= kw['datecuenta']).filter(Head.faltante > 0).filter(Head.id_unidad==queryunidad.id_unidad).filter_by(id_operador=None).order_by(Head.pagado.desc()).all()
        queryheadsoperador=DBSession.query(Head).filter(Head.fecha <= kw['datecuenta']).filter(Head.faltante > 0).filter_by(id_operador=querypersona.id_persona).order_by(Head.pagado.desc()).all()
        totalporpagar=0
        totalpagado=0
        totalfaltante=0
        for itemhead in queryheads:
            porpagar=itemhead.porpagar
            pagado=itemhead.pagado
            faltante=itemhead.faltante
            totalporpagar=totalporpagar+porpagar
            totalpagado=totalpagado+pagado
            totalfaltante=totalfaltante+faltante

        totalporpagar2=0
        totalpagado2=0
        totalfaltante2=0
        for itemheadope in queryheadsoperador:
            porpagar = itemheadope.porpagar
            pagado = itemheadope.pagado
            faltante = itemheadope.faltante
            totalporpagar2 = totalporpagar2 + porpagar
            totalpagado2 = totalpagado2 + pagado
            totalfaltante2 = totalfaltante2 + faltante

        if int(kw['montooperador']) > totalfaltante2 or int(kw['monto'])>totalfaltante:
            print("====EL IF OR===")
            print("kw['montooperador']==>",kw['montooperador'])
            print("totalfaltante2==>",totalfaltante2)
            print("kw['monto']==>", kw['monto'])
            print("totalfaltante==>",totalfaltante)
            return dict(error='montomas')

        else:
            aux=0
            monto=int(kw['monto'])
            for itemhead in queryheads:
                aux=(monto - itemhead.faltante)
                auxmonto=monto
                monto=aux
                if aux<=0:
                    if aux==0:
                        print("***cubre")
                        itemhead.pagado= itemhead.pagado + auxmonto
                        querythebodies = DBSession.query(Body).filter_by(id_unidad=itemhead.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemhead.fecha).all()
                        for itembody in querythebodies:
                            querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                            ladif = itembody.saldo - itembody.pagado
                            itembody.pagado=itembody.pagado + ladif
                            querycuentacargo.total=querycuentacargo.total + ladif
                            listaunibody.append({'body': itembody.id_body,'monto': ladif})
                            #itembody.id_operador=querypersona.id_persona
                        itemhead.faltante=itemhead.faltante - auxmonto
                        listaheaduni.append({'head': itemhead.id_head,'monto': auxmonto})
                        if itemhead.faltante == 0:
                            itemhead.estatus='PAGADO'
                        itemhead.id_operador=querypersona.id_persona
                    else:
                        print("***Apenas cubre y debe")
                        itemhead.pagado= itemhead.pagado + auxmonto
                        itemhead.estatus = 'ADEUDO'
                        querythebodies = DBSession.query(Body).filter_by(id_unidad=itemhead.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemhead.fecha).all()
                        auxmonto2 = auxmonto
                        for itembody in querythebodies:
                            querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                            print("Desglozar")
                            falta=itembody.saldo - itembody.pagado
                            if auxmonto2>falta:
                                itembody.pagado = itembody.pagado + falta
                                auxmonto2=auxmonto2-falta
                                listaunibody.append({'body': itembody.id_body, 'monto': falta})
                                querycuentacargo.total=querycuentacargo.total+falta
                                #itembody.id_operador = querypersona.id_persona
                            elif auxmonto2<falta:
                                itembody.pagado=itembody.pagado + auxmonto2
                                listaunibody.append({'body': itembody.id_body, 'monto': auxmonto2})
                                querycuentacargo.total = querycuentacargo.total + auxmonto2
                                #itembody.id_operador = querypersona.id_persona
                                break
                            else:
                                itembody.pagado=itembody.pagado + falta
                                listaunibody.append({'body': itembody.id_body, 'monto': falta})
                                querycuentacargo.total = querycuentacargo.total + falta
                                #itembody.id_operador = querypersona.id_persona
                                break



                        itemhead.faltante=itemhead.faltante - auxmonto
                        listaheaduni.append({'head': itemhead.id_head,'monto': auxmonto})
                        if auxmonto>0:
                            itemhead.id_operador = querypersona.id_persona


                    break
                else:
                    print("**Alcanza para el otro")
                    auxfaltante=itemhead.faltante
                    itemhead.faltante=itemhead.faltante - itemhead.faltante
                    monto=auxmonto - auxfaltante
                    itemhead.pagado=itemhead.pagado + auxfaltante
                    querythebodies = DBSession.query(Body).filter_by(id_unidad=itemhead.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemhead.fecha).all()
                    for itembody in querythebodies:
                        querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                        ladif = itembody.saldo - itembody.pagado
                        itembody.pagado = itembody.pagado + ladif
                        querycuentacargo.total = querycuentacargo.total + ladif
                        listaunibody.append({'body': itembody.id_body, 'monto': ladif})
                        #itembody.id_operador = querypersona.id_persona
                    listaheaduni.append({'head': itemhead.id_head,'monto': auxfaltante})
                    if itemhead.faltante == 0:
                        itemhead.estatus = 'PAGADO'
                    itemhead.id_operador = querypersona.id_persona


            aux=0
            monto=int(kw['montooperador'])
            for itemheadope in queryheadsoperador:
                aux=(monto - itemheadope.faltante)
                auxmonto=monto
                monto=aux
                if aux<=0:
                    if aux==0:
                        print("***cubre")
                        itemheadope.pagado = itemheadope.pagado + auxmonto
                        if itemheadope.id_unidad==None:
                            querythebodies = DBSession.query(Body).filter_by(id_operador=itemheadope.id_operador).filter(Body.id_cargopersona!=None).filter_by(fecha=itemheadope.fecha).all()
                        else:
                            querythebodies = DBSession.query(Body).filter_by(id_unidad=itemheadope.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemheadope.fecha).all()

                        for itembody in querythebodies:
                            querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargooperador_id=itembody.id_cargopersona).first()
                            if querycuentacargo is None:
                                querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                            ladif = itembody.saldo - itembody.pagado
                            itembody.pagado=itembody.pagado + ladif
                            listaopebody.append({'body': itembody.id_body, 'monto': ladif})
                            querycuentacargo.total=querycuentacargo.total + ladif
                        itemheadope.faltante = itemheadope.faltante - auxmonto
                        listaheadoperador.append({'head': itemheadope.id_head,'monto': auxmonto})
                        if itemheadope.faltante == 0:
                            itemheadope.estatus='PAGADO'
                    else:
                        print("***Apenas cubre y debe")
                        itemheadope.pagado = itemheadope.pagado + auxmonto
                        itemheadope.estatus = 'ADEUDO'
                        if itemheadope.id_unidad==None:
                            querythebodies = DBSession.query(Body).filter_by(id_operador=itemheadope.id_operador).filter(Body.id_cargopersona!=None).filter_by(fecha=itemheadope.fecha).all()
                        else:
                            querythebodies = DBSession.query(Body).filter_by(id_unidad=itemheadope.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemheadope.fecha).all()

                        auxmonto2 = auxmonto
                        for itembody in querythebodies:
                            querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargooperador_id=itembody.id_cargopersona).first()
                            if querycuentacargo is None:
                                querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                            print("Desglozar")
                            falta=itembody.saldo - itembody.pagado
                            if auxmonto2>falta:
                                itembody.pagado = itembody.pagado + falta
                                auxmonto2=auxmonto2-falta
                                listaopebody.append({'body': itembody.id_body, 'monto': falta})
                                querycuentacargo.total = querycuentacargo.total + falta
                            elif auxmonto2<falta:
                                itembody.pagado=itembody.pagado + auxmonto2
                                listaopebody.append({'body': itembody.id_body, 'monto': auxmonto2})
                                querycuentacargo.total = querycuentacargo.total + auxmonto2
                                break
                            else:
                                itembody.pagado=itembody.pagado + falta
                                listaopebody.append({'body': itembody.id_body, 'monto': falta})
                                querycuentacargo.total = querycuentacargo.total + falta
                                break

                        itemheadope.faltante = itemheadope.faltante - auxmonto
                        listaheadoperador.append({'head': itemheadope.id_head,'monto': auxmonto})

                    break
                else:
                    print("**Alcanza para el otro")
                    auxfaltante = itemheadope.faltante
                    itemheadope.faltante = itemheadope.faltante - itemheadope.faltante
                    monto = auxmonto - auxfaltante
                    itemheadope.pagado = itemheadope.pagado + auxfaltante
                    if itemheadope.id_unidad == None:
                        querythebodies = DBSession.query(Body).filter_by(id_operador=itemheadope.id_operador).filter(Body.id_cargopersona != None).filter_by(fecha=itemheadope.fecha).all()
                    else:
                        querythebodies = DBSession.query(Body).filter_by(id_unidad=itemheadope.id_unidad).filter_by(id_operador=None).filter_by(fecha=itemheadope.fecha).all()
                    for itembody in querythebodies:
                        querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargooperador_id=itembody.id_cargopersona).first()
                        if querycuentacargo is None:
                            querycuentacargo = DBSession.query(CuentaCargo).filter_by(cargounidad_id=itembody.id_cargounidad).first()
                        ladif = itembody.saldo - itembody.pagado
                        itembody.pagado = itembody.pagado + ladif
                        listaopebody.append({'body': itembody.id_body, 'monto': ladif})
                        querycuentacargo.total = querycuentacargo.total + ladif
                    listaheadoperador.append({'head': itemheadope.id_head,'monto': auxfaltante})
                    if itemheadope.faltante == 0:
                        itemheadope.estatus = 'PAGADO'


        if i == 0:
            DBSession.add(abono)
        DBSession.add(newunidadoperador)

        #print("EL ABONO CUBRIO head unidad: ",listaheaduni)
        #print("EL ABONO CUBRIO head operador: ",listaheadoperador)
        queryabono=DBSession.query(AbonoCuenta).filter_by(cancelado=0).filter_by(fechaingreso = datetime.now().date()).\
                filter_by(fechacuenta = kw['datecuenta']).filter_by(unidad_id = queryunidadglobal.id_unidad).filter_by(operador_id = querypersonaglobal.id_persona).\
                filter_by(totalapagaruni = int(kw['totaluni'])).filter_by(montoingresado = int(kw['monto'])).\
                filter_by(faltante = faltauni).filter_by(total = int(kw['monto'])  - int(faltauni)).\
                filter_by(totalapagarope=int(kw['totalope'])).filter_by(montoingresadoope = int(kw['montooperador'])).\
                filter_by(faltanteope = faltaoper).filter_by(totalope = int(kw['montooperador'])  - int(faltaoper)).\
                filter_by(observaciones = kw['observaciones']).filter_by(usuario = kw['user']).filter_by(application_id = kw['application_id']).first()

        for item in listaheaduni:
            newpagohead=PagoHead()
            newpagohead.head_id=item['head']
            newpagohead.pago_id=queryabono.id_abonocuenta
            newpagohead.caracter='U'
            newpagohead.monto=item['monto']
            DBSession.add(newpagohead)

        for item in listaheadoperador:
            newpagohead=PagoHead()
            newpagohead.head_id=item['head']
            newpagohead.pago_id = queryabono.id_abonocuenta
            newpagohead.caracter='O'
            newpagohead.monto = item['monto']
            DBSession.add(newpagohead)

        for item in listaunibody:
            newpagobody=PagoBody()
            newpagobody.body_id=item['body']
            newpagobody.pago_id=queryabono.id_abonocuenta
            newpagobody.caracter='U'
            newpagobody.monto=item['monto']
            DBSession.add(newpagobody)

        for item in listaopebody:
            newpagobody=PagoBody()
            newpagobody.body_id=item['body']
            newpagobody.pago_id=queryabono.id_abonocuenta
            newpagobody.caracter='O'
            newpagobody.monto=item['monto']
            DBSession.add(newpagobody)

        DBSession.flush()
        return dict(error="ok")

    @expose('json')
    def form(self, **kw):
        print("FORM")
        if kw['app_name']=='TODOveritodo':

            unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_id=kw['application_id']).all()

            operadores=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).all()

        else:

            unidades = DBSession.query(DatosUnidad).filter_by(habilitado=1).filter_by(app_id=kw['application_id']).all()

            operadores=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).all()


        kw['unidades']=unidades
        kw['operadores']=operadores
        today = datetime.now()
        oneday = timedelta(days=1)
        yesterday = today - oneday
        kw['today']=today.date()
        kw['yesterday']=yesterday.date()

        # if kw['id'] == "0":
        #     kw['actions'] = AbonoCuenta()
        # else:
        #     kw['actions'] = DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=kw['id']).first()
        dialogtemplate = render_template(kw, "mako", 'pythonjupiter.templates.Abonos.probando_abono_form')
        return dict(dialogtemplate=dialogtemplate)


    @expose('json')
    def actualizaselectunidad(self,**kw):
        unidades=[]
        if 'soy' in kw:
            if kw['soy']=='nouser':
                if kw['app_name'] == 'TODOveritodo':
                    unidadesbd = DBSession.query(DatosUnidad).filter(
                        DatosUnidad.eco1.like('%' + kw['unidad'] + '%')).filter_by(habilitado=1).filter_by(
                        app_id=kw['application_id']) \
                        .all()
                else:
                    unidadesbd = DBSession.query(DatosUnidad).filter(
                        DatosUnidad.eco1.like('%' + kw['unidad'] + '%')).filter_by(habilitado=1).filter_by(
                        app_id=kw['application_id']) \
                        .all()

                for item in unidadesbd:
                    unidades.append({'eco1': item.eco1})
            else:
                queryusuario=DBSession.query(Persona).filter_by(usuario=kw['user']).filter_by(application_id=kw['application_id']).first()

                unidadesbd = DBSession.query(DatosUnidad).filter_by(id_persona=queryusuario.id_persona).filter_by(habilitado=1) \
                        .all()

                for item in unidadesbd:
                    unidades.append({'eco1': item.eco1})


        else:
            if kw['app_name']=='TODOveritodo':
                unidadesbd=DBSession.query(DatosUnidad).filter(DatosUnidad.eco1.like('%'+kw['unidad']+'%')).filter_by(habilitado=1).filter_by(app_id=kw['application_id'])\
                    .all()
            else:
                unidadesbd=DBSession.query(DatosUnidad).filter(DatosUnidad.eco1.like('%'+kw['unidad']+'%')).filter_by(habilitado=1).filter_by(app_id=kw['application_id'])\
                    .all()

            for item in unidadesbd:
                unidades.append({'eco1': item.eco1})

        return dict(data=unidades)


    @expose('json')
    def actualizalistacargos(self,**kw):
        print("ENTRE AQUIII ACTUALIZA LISTA DE CARGOOS")
        print(kw)
        cargos=[]
        body=[]
        bodyope=[]
        auxtotal = 0
        total = 0
        totalpagado = 0
        totalfaltante = 0
        total2=0
        if kw['fecha']=='':
            pass
            print("fecha nada")
            return dict(paraunidades=[], paraoperadores=[],body=body,bodyope=bodyope,auxtotal=auxtotal,total=total,totalpagado=totalpagado,
                        totalfaltante=totalfaltante,total2=total2)
        else:
            fecha=kw['fecha']
            if kw['unidad'] != '-':
                #{'user': 'diegoL1', 'unidad': '321', 'operador': 'DIEGO SANTILLAN LóPEZ', 'app_name': 'TransporteLinea1', 'fecha': '2019-05-12', 'internal_id': '5'}

                queryoperador = DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(nombre=kw['operador']).first()
                queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=kw['unidad']).filter_by(app_id=kw['application_id']).first()
                #thequeryoperador=DBSession.query(Persona).filter_by(id_unidad=queryunidad.id_unidad).first()
                print("queryunidad=====>",queryunidad)
                try:
                    elidd=queryoperador.id_persona
                except:
                    elidd=None

                try:
                    esteidd=queryunidad.id_unidad
                except:
                    esteidd=''

                print("TRATARE DE HACER HEAD prueba ss")
                queryheadunidad = DBSession.query(Head).filter(Head.id_unidad == esteidd).filter(
                    Head.id_operador == None).filter(Head.faltante > 0).filter(Head.fecha <= kw['fecha']).order_by(Head.pagado.desc()).all()
                print(queryheadunidad)
                print("LO HICE SII prueba")
                print("TRATARE DE HACER HEAD")
                #queryheadunidad = DBSession.query(Head).filter(Head.id_unidad==esteidd).filter(Head.id_operador==None).filter(Head.faltante > 0).filter(Head.fecha<=kw['fecha']).order_by("pagado desc").all()
                #print("queryheadunidad=====>", queryheadunidad)
                print("LO HICE SII")
                for item in queryheadunidad:
                    querybody=DBSession.query(Body).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=item.fecha).all()
                    for itembody in querybody:
                        querycargounidad=DBSession.query(CargoUnidad).filter_by(id_cargounidad=itembody.id_cargounidad).first()
                        if querycargounidad is not None:
                            body.append({'fecha': itembody.fecha,'cargo': querycargounidad.cargo
                                ,'monto': itembody.monto,'condonacion': itembody.condonacion,'saldo': itembody.saldo})
                        else:
                            body.append({'fecha': itembody.fecha, 'cargo': 'POLIZA'
                                            , 'monto': itembody.monto, 'condonacion': itembody.condonacion,
                                         'saldo': itembody.saldo})


            else:
                queryheadunidad={}
            if kw['operador']!='-':

                queryoperador=DBSession.query(Persona).filter_by(application_id=kw['application_id']).filter_by(nombre=kw['operador']).first()
                try:
                    idd=queryoperador.id_persona
                except:
                    idd=''

                #queryheadoperador = DBSession.query(Head).filter_by(id_operador=idd).filter(Head.faltante > 0).filter(Head.fecha<=kw['fecha']).order_by("pagado desc").all()
                queryheadoperador = DBSession.query(Head).filter_by(id_operador=idd).filter(Head.faltante > 0).filter(Head.fecha <= kw['fecha']).order_by(Head.pagado.desc()).all()


                for item in queryheadoperador:
                    querybody=DBSession.query(Body).filter_by(id_operador=queryoperador.id_persona).filter_by(fecha=item.fecha).all()
                    for itembody in querybody:
                        querycargooperador=DBSession.query(CargoOperador).filter_by(id_cargooperador=itembody.id_cargopersona).first()
                        bodyope.append({'fecha': itembody.fecha,'cargo': querycargooperador.cargo
                                ,'monto': itembody.monto,'condonacion': itembody.condonacion,'saldo': itembody.saldo})
            else:
                queryheadoperador={}


            for itemunidades in queryheadunidad:
                auxtotal=auxtotal+itemunidades.porpagar
                total=total+itemunidades.faltante
                totalpagado=totalpagado+itemunidades.pagado
                totalfaltante=totalfaltante+itemunidades.faltante

            for itemope in queryheadoperador:
                total=total+itemope.faltante
                total2=total2+itemope.porpagar
                totalpagado=totalpagado+itemope.pagado
                totalfaltante=totalfaltante+itemope.faltante



            return dict(paraunidades=queryheadunidad,paraoperadores=queryheadoperador,body=body,bodyope=bodyope,auxtotal=auxtotal,total=total,totalpagado=totalpagado,
                        totalfaltante=totalfaltante,total2=total2)

    @expose('json')
    def actualizaselectoperador(self,**kw):
        operadores=[]
        operadoresbd=DBSession.query(Persona).filter_by(habilitadooperador=1).filter_by(esoperador=1).filter(Persona.numerooperador==kw['operador']).filter_by(application_id=kw['application_id'])\
                .all()

        try:
            queryunidad = DBSession.query(DatosUnidad).filter_by(id_unidad=operadoresbd[0].id_unidad).first()
        except:
            queryunidad=None

        if queryunidad is not None:
            unidad=queryunidad.eco1
        else:
            unidad=''
        for item in operadoresbd:
            operadores.append({'nombre': item.nombre})

        return dict(data=operadores,unidad=unidad)


    @expose('json')
    def actualizaselectpermi(self,**kw):
        permisionario=[]
        permisionariobd=DBSession.query(Persona).filter_by(habilitadopermisionario=1).filter_by(espermisionario=1).filter(Persona.nombre.like('%'+kw['operador']+'%')).filter_by(application_id=kw['application_id'])\
                .all()

        for item in permisionariobd:
            permisionario.append({'nombre': item.nombre})

        return dict(data=permisionario)



    @expose('json')
    def actualizaallselect(self,**kw):
        unidades=[]
        operadores=[]
        cargosunidades=[]
        cargosoperadores=[]
        empresa=kw['empresa']
        total=0
        if empresa=='Seleccione':
            pass
            return dict(unidades=unidades, operadores=operadores,total=total,cargosunidades=cargosunidades,cargosoperadores=cargosoperadores)
        else:
            querylinea = DBSession.query(Linea).filter_by(nombre_linea=empresa).filter_by(application_id=kw['application_id']).first()
            queryunidades=DBSession.query(DatosUnidad).filter_by(id_linea=querylinea.id_linea).filter_by(app_id=kw['application_id']).all()
            queryoperadores = DBSession.query(Persona).filter_by(id_linea=querylinea.id_linea).filter_by(esoperador=1).filter_by(application_id=kw['application_id']).all()
            for item in queryunidades:
                unidades.append({'eco': item.eco1,'id_unidad':item.id_unidad})
            for item in queryoperadores:
                operadores.append({'nombre': item.nombre,'id_persona': item.id_persona})



            return dict(unidades=unidades,operadores=operadores,total=total,cargosunidades=cargosunidades,cargosoperadores=cargosoperadores)

    @expose('json')
    def actualizatodopdgo(self,**kw):
        operador=kw['operador']
        permisionario=''
        derrotero=''
        grupo=''
        linea=''
        unidad=kw['unidad']
        if operador=='' or unidad=='':
            return dict(permisionario=permisionario, derrotero=derrotero, grupo=grupo,linea=linea,unidad=unidad)
        else:
            #queryoperador=DBSession.query(Persona).filter_by(nombre=operador).filter_by(internal_id=kw['internal_id']).first()

            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=unidad).filter_by(app_id=kw['application_id']).first()


            if queryunidad is not None:
                launidad=queryunidad.eco1
                querylinea = DBSession.query(Linea).filter_by(id_linea=queryunidad.id_linea).filter_by(app_id=kw['application_id']).first()
                querypermisionario = DBSession.query(Persona).filter_by(espermisionario=1).filter_by(application_id=kw['application_id']) \
                    .filter_by(id_persona=queryunidad.id_persona).first()
                queryderrotero = DBSession.query(Derrotero).filter_by(application_id=kw['application_id']) \
                    .filter_by(id_derrotero=queryunidad.id_derrotero).first()
                querygrupo = DBSession.query(Grupo_DatosUnidad).filter_by(datosunidad_id=queryunidad.id_unidad) \
                    .all()
                if querygrupo==[]:
                    grupo=''
                else:
                    for itemgrupo in querygrupo:
                        queryparagrupo=DBSession.query(Grupo).filter_by(id_grupo=itemgrupo.grupo_id).first()
                        grupo=grupo+','+queryparagrupo.grupo

                permisionario=querypermisionario.nombre
                try:
                    derrotero=queryderrotero.derrotero
                except:
                    derrotero=''

                #grupo=querygrupo.grupo
                try:
                    linea=querylinea.nombre_linea
                except:
                    linea=''
            else:
                launidad='Sin Unidad'

        return dict(permisionario=permisionario,derrotero=derrotero,grupo=grupo,unidad=launidad,linea=linea)

    @expose('json')
    def getunit(self,**kw):
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=kw['unidad_id']).first()
        eco=queryunidad.eco1
        return dict(eco=eco)

    @expose('json')
    def getoperador(self,**kw):
        queryoperador=DBSession.query(Persona).filter_by(id_persona=kw['operador_id']).first()
        if queryoperador is not None:
            operador='('+str(queryoperador.numerooperador)+')'+queryoperador.nombre
        else:
            operador=''
        return dict(operador=operador)

    @expose('json')
    def cancel(self,**kw):
        id=kw['id']
        queryabono=DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=id).first()
        if queryabono.cancelado==0:
            queryabono.cancelado=1
            queryabono.usuario=queryabono.usuario+' cancelo: '+kw['user']
            querypagohead=DBSession.query(PagoHead).filter_by(pago_id=queryabono.id_abonocuenta).all()
            querypagobody=DBSession.query(PagoBody).filter_by(pago_id=queryabono.id_abonocuenta).all()

            for itembody in querypagobody:
                querybody=DBSession.query(Body).filter_by(id_body=itembody.body_id).first()
                if itembody.caracter=='U':
                    querybody.pagado=querybody.pagado - itembody.monto
                else:
                    querybody.pagado = querybody.pagado - itembody.monto

            for itempagohead in querypagohead:
                print("caracter: ", itempagohead.caracter)
                queryhead=DBSession.query(Head).filter_by(id_head=itempagohead.head_id).first()
                if itempagohead.caracter=='U':
                    print("Unidad")
                    print("Unidad: ",queryhead.id_head)
                    print("Viejo Pagado: ",queryhead.pagado)
                    print("Nuevo Pagado: ",(queryhead.pagado - itempagohead.monto))
                    print("Viejo faltante: ", queryhead.faltante)
                    print("Nuevo faltante: ",(queryhead.porpagar)-(queryhead.pagado - itempagohead.monto))
                    queryhead.faltante=(queryhead.porpagar)-(queryhead.pagado - itempagohead.monto)
                    queryhead.pagado = (queryhead.pagado - itempagohead.monto)
                    queryhead.estatus = 'Por Pagar'
                    if queryhead.pagado==0:
                        queryhead.id_operador=None

                else:
                    print("Operador")
                    print("Operador: ", queryhead.id_head)
                    print("Viejo Pagado: ", queryhead.pagado)
                    print("Nuevo Pagado: ",(queryhead.pagado - itempagohead.monto))
                    print("Viejo faltante: ", queryhead.faltante)
                    print("Nuevo faltante: ",(queryhead.porpagar)-(queryhead.pagado - itempagohead.monto))
                    queryhead.faltante = (queryhead.porpagar) - (queryhead.pagado - itempagohead.monto)
                    queryhead.pagado = (queryhead.pagado - itempagohead.monto)
                    queryhead.estatus='Por Pagar'
                    if queryhead.pagado==0 and queryhead.id_unidad!=None:
                        queryhead.id_operador=None


        return dict(error='ok')


    @expose('json')
    def cargosadelante(self,**kw):
        print("CARGOS ADELANTE")
        print("FECHA: ",kw['fecha'])
        newfecha=datetime.strptime(kw['fecha'], '%m/%d/%Y')
        print("New fecha: ",newfecha.date())
        return dict(error='ok')


    @expose('json')
    def htmlreciboabono(self,**kw):
        arreglo=kw['rowObject'].split(",")
        # 5
        # 0
        # 2018 - 09 - 24
        # 2018 - 09 - 24
        # 11
        # 13
        # 30
        # 30
        # 0
        # 30
        # 10
        # 10
        # 0
        # 10
        #
        # milla
        # 5
        queryabono=DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=arreglo[0]).first()

        querypagohead=DBSession.query(PagoHead).filter_by(pago_id=queryabono.id_abonocuenta).all()
        querypagobody=DBSession.query(PagoBody).filter_by(pago_id=queryabono.id_abonocuenta).all()

        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=queryabono.unidad_id).first()
        queryoperador=DBSession.query(Persona).filter_by(id_persona=queryabono.operador_id).first()

        listaqueryheadunidad=[]
        listaquerybodyunidad=[]

        listaqueryheadoperador=[]
        listaquerybodyoperador=[]

        for itemhead in querypagohead:
            queryhead=DBSession.query(Head).filter_by(id_head=itemhead.head_id).first()
            unidad=DBSession.query(DatosUnidad).filter_by(id_unidad=queryhead.id_unidad).first()
            opera=DBSession.query(Persona).filter_by(id_persona=queryhead.id_operador).first()
            try:
                varunidad=unidad.eco1
            except:
                varunidad=''

            try:
                varopera=opera.nombre
            except:
                varopera=''
            if queryhead.id_unidad is not None:
                listaqueryheadunidad.append({'fecha': queryhead.fecha,'unidad': varunidad ,'operador': varopera,'porpagar': queryhead.porpagar, 'pagado': queryhead.pagado, 'faltante': queryhead.faltante,'sepago': itemhead.monto})
            else:
                listaqueryheadoperador.append({'fecha': queryhead.fecha,'unidad': varunidad ,'operador': varopera,'porpagar': queryhead.porpagar, 'pagado': queryhead.pagado,'faltante': queryhead.faltante,'sepago': itemhead.monto})


        for itembody in querypagobody:
            querybody = DBSession.query(Body).filter_by(id_body=itembody.body_id).first()
            if querybody.id_unidad is not None:
                querycargounidad = DBSession.query(CargoUnidad).filter_by(id_cargounidad=querybody.id_cargounidad).first()
                try:
                    querycargodeunidad=querycargounidad.cargo
                except:
                    querycargodeunidad='POLIZA'
                listaquerybodyunidad.append({'fecha': querybody.fecha, 'cargo': querycargodeunidad
                            , 'monto': querybody.monto, 'condonacion': querybody.condonacion, 'saldo': querybody.saldo,'sepago': itembody.monto})
            else:
                querycargooperador = DBSession.query(CargoOperador).filter_by(id_cargooperador=querybody.id_cargopersona).first()
                listaquerybodyoperador.append({'fecha': querybody.fecha, 'cargo': querycargooperador.cargo
                            , 'monto': querybody.monto, 'condonacion': querybody.condonacion, 'saldo': querybody.saldo,'sepago': itembody.monto})

        totalheadunidad=0
        for itemheadunidad in listaqueryheadunidad:
            totalheadunidad=totalheadunidad+itemheadunidad['sepago']

        totalheadoperador=0
        for itemheadoperador in listaqueryheadoperador:
            totalheadoperador=totalheadoperador+itemheadoperador['sepago']


        file_name = ExportPDF.create({'queryabono': queryabono,'listaqueryheadunidad': listaqueryheadunidad,'listaquerybodyunidad': listaquerybodyunidad,
                                 'listaqueryheadoperador':listaqueryheadoperador,'listaquerybodyoperador': listaquerybodyoperador,'queryunidad': queryunidad,
                                  'queryoperador': queryoperador,'querypagohead': querypagohead,'querypagobody': querypagobody,'totalheadunidad': totalheadunidad,
                                      'totalheadoperador': totalheadoperador},'recibo'+str(queryabono.id_abonocuenta),
                                     'pythonjupiter.templates.Abonos.reciboabono','prueba')


        template=render_template({'queryabono': queryabono,'listaqueryheadunidad': listaqueryheadunidad,'listaquerybodyunidad': listaquerybodyunidad,
                                 'listaqueryheadoperador':listaqueryheadoperador,'listaquerybodyoperador': listaquerybodyoperador,'queryunidad': queryunidad,
                                  'queryoperador': queryoperador,'querypagohead': querypagohead,'querypagobody': querypagobody,'totalheadunidad': totalheadunidad
                                  ,'totalheadoperador': totalheadoperador},
                                 'mako','pythonjupiter.templates.Abonos.reciboabono')



        return dict(template=template,file_name=file_name)


    @expose('json')
    def formulariocorreos(self,**kw):
        print('PARA FORMULARIOS CORREOS',kw['rowObject'])
        lista=kw['rowObject'].split(',')
        queryabono=DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=int(lista[0])).first()
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=queryabono.unidad_id).first()
        querypermisionario=DBSession.query(Persona).filter_by(id_persona=queryunidad.id_persona).first()
        queryoperador=DBSession.query(Persona).filter_by(id_persona=queryabono.operador_id).first()
        correopermi=DBSession.query(Correos).filter_by(id_persona=querypermisionario.id_persona).first()
        if correopermi is None:
            varcorreopermi=''
        else:
            varcorreopermi=correopermi.correo
        correooperador=DBSession.query(Correos).filter_by(id_persona=queryoperador.id_persona).first()
        if correooperador is None:
            varcorreooperador=''
        else:
            varcorreooperador=correooperador.correo
        template=render_template({'permisionario': querypermisionario.nombre,'operador': queryoperador.nombre,'varcorreopermi':varcorreopermi
                                  ,'varcorreooperador': varcorreooperador},'mako','pythonjupiter.templates.Abonos.formcorreos')
        return dict(template=template)


    @expose('json')
    def formulariocorreos2(self,**kw):
        unidad=kw['unidad']
        operador=kw['operador']
        por=kw['por']
        if por=='Unidad' and unidad!='-Seleccione-':
            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=unidad).filter_by(app_id=kw['application_id']).first()
            querypersona=DBSession.query(Persona).filter_by(id_persona=queryunidad.id_persona).first()
            permisionario=querypersona.nombre
            querycorreopersona=DBSession.query(Correos).filter_by(id_persona=querypersona.id_persona).first()
            if querycorreopersona is None:
                varcorreopersona=''
            else:
                varcorreopersona=querycorreopersona.correo

            template = render_template({'persona': permisionario,
                                       'varcorreopersona': varcorreopersona}, 'mako',
                                   'pythonjupiter.templates.Abonos.formcorreos2')

        elif por=='Operador' and operador!='-Seleccione-':
            queryoperador=DBSession.query(Persona).filter_by(nombre=operador).filter_by(application_id=kw['application_id']).first()
            operador=queryoperador.nombre
            querycorreopersona=DBSession.query(Correos).filter_by(id_persona=queryoperador.id_persona).first()
            if querycorreopersona is None:
                varcorreopersona=''

            else:
                varcorreopersona=querycorreopersona.correo

            template = render_template({'persona': operador,
                                       'varcorreopersona': varcorreopersona}, 'mako',
                                   'pythonjupiter.templates.Abonos.formcorreos2')

        return dict(template=template)




    @expose('json')
    def sendemail(self,**kw):
        correo1=kw['correo1']
        correo2=kw['correo2']
        correo3=kw['correo3']
        filepath=kw['file_path']
        object=kw['object']
        id=object.split(',')
        queryabono=DBSession.query(AbonoCuenta).filter_by(id_abonocuenta=int(id[0])).first()
        queryoperador=DBSession.query(Persona).filter_by(id_persona=queryabono.operador_id).first()
        queryunidad=DBSession.query(DatosUnidad).filter_by(id_unidad=queryabono.unidad_id).first()
        querypermi=DBSession.query(Persona).filter_by(id_persona=queryunidad.id_persona).first()

        querycorreopermi=DBSession.query(Correos).filter_by(id_persona=querypermi.id_persona).first()
        if querycorreopermi is None:
            newcorreo=Correos()
            newcorreo.correo=correo1
            newcorreo.id_persona=querypermi.id_persona
            DBSession.add(newcorreo)
        else:
            querycorreopermi.correo=correo1

        querycorreooperador=DBSession.query(Correos).filter_by(id_persona=queryoperador.id_persona).first()
        if querycorreooperador is None:
            newcorreo=Correos()
            newcorreo.correo=correo2
            newcorreo.id_persona=queryoperador.id_persona
            DBSession.add(newcorreo)
        else:
            querycorreooperador.correo=correo2

        #url = urlsun()+"config/emailConfig?internal_id=" + str(kw['internal_id'])
        # response = requests.get(url, auth=('manager', 'managepass'))
        # r = response.json()
        # print(r)
        # if r["error"] != "ok":
        #     mailer = Mailer(host=r["host"], port=r["port"], tls=True, username=r["username"], password=r["password"],
        #                     debug=0)
        # message = Message(
        #     subject="NUEVO VENUS Test",
        #     sender="Admin <" + r["sender"] + ">",
        #     recipients=["maria@dwim.mx"],
        #     html="<h1>Test</h1><p>Esto es un avance para dejar listo los correos</p>",
        # )
        # mailer.send_immediately(message, fail_silently=False)

        return dict(error='Correo Enviado')

    @expose('json')
    def sendemail2(self,**kw):
        correo1=kw['correo1']
        correo3=kw['correo3']
        filepath=kw['file_path']
        unidad=kw['unidad']
        operador=kw['operador']
        por=kw['por']
        if por=='Unidad':
            queryunidad=DBSession.query(DatosUnidad).filter_by(eco1=unidad).filter_by(app_id=kw['application_id']).first()
            querypermisionario=DBSession.query(Persona).filter_by(id_persona=queryunidad.id_persona).first()
            querycorreopermi=DBSession.query(Correos).filter_by(id_persona=querypermisionario.id_persona).first()
            if querycorreopermi is None:
                newcorreo=Correos()
                newcorreo.correo=correo1
                newcorreo.id_persona=querypermisionario.id_persona
                DBSession.add(newcorreo)
            else:
                querycorreopermi.correo=correo1
        else:

            queryoperador=DBSession.query(Persona).filter_by(nombre=operador).filter_by(application_id=kw['application_id']).first()
            querycorreopermi=DBSession.query(Correos).filter_by(id_persona=queryoperador.id_persona).first()
            if querycorreopermi is None:
                newcorreo=Correos()
                newcorreo.correo=correo1
                newcorreo.id_persona=queryoperador.id_persona
                DBSession.add(newcorreo)
            else:
                querycorreopermi.correo=correo1


        # url = urlsun()+"config/emailConfig?internal_id=" + str(kw['internal_id'])
        # response = requests.get(url, auth=('manager', 'managepass'))
        # r = response.json()
        # print(r)
        # if r["error"] != "ok":
        #     mailer = Mailer(host=r["host"], port=r["port"], tls=True, username=r["username"], password=r["password"],
        #                     debug=0)
        # message = Message(
        #     subject="NUEVO VENUS Test",
        #     sender="Admin <" + r["sender"] + ">",
        #     recipients=["maria@dwim.mx"],
        #     html="<h1>Test</h1><p>Esto es un avance para dejar listo los correos</p>",
        # )
        # mailer.send_immediately(message, fail_silently=False)

        return dict(error='Correo Enviado')