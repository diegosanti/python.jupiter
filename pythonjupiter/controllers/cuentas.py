from pythonjupiter.lib.base import BaseController
from pythonjupiter.model import DBSession
from pythonjupiter.model.tables import AbonoCuenta
from tg import app_globals
import requests
from tg import expose
from pythonjupiter.lib.jqgrid import jqgridDataGrabber
from pythonjupiter.model.tables import Linea,DatosUnidad,Persona,CargoOperador,Derrotero,Grupo,UnidadOperador,EstadoCuenta,Condonaciones,CargoUnidad,GrupoPersona
from pythonjupiter.model.tables import UnidadCargo,OperadorCargo,Body,Head,CondonacionOperador,CondonacionUnidad,CuentaCargo,Salida
from pythonjupiter.lib.messagequeue import Message
from datetime import datetime
from tg import render_template

class CuentasController(BaseController):

    def __init__(self):
        pass

    @expose('json')
    def load(self, **kw):
        filter = [('internal_id', 'eq', kw['internal_id'])]
        return jqgridDataGrabber(CuentaCargo, 'id_cuenta', filter, kw).loadGrid()


    @expose('json')
    def datajson(self,**kw):
        empresa=kw['empresa']
        ayer=kw['fecha']
        fechauno=kw['fechauno']
        contenido=[]
        comparacion=[]
        listatotales=[]
        listacondo=[]
        internal_id=kw['internal_id']
        application_id=kw['appID']
        total=0
        totaloperador=0
        totalbody=0
        totalbody2=0
        posicion=0

        queryunidades=DBSession.query(DatosUnidad).filter_by(app_id=application_id).all()
        queryoperadores = DBSession.query(Persona).filter_by(esoperador=1).filter_by(application_id=application_id).all()

        condonacion2=0
        for itemope in queryoperadores:
            querybody = DBSession.query(Body).filter_by(id_operador=itemope.id_persona).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).all()
            for itembody in querybody:
                condonacion2 = condonacion2 + itembody.condonacion


        for itemoperadores in queryoperadores:
            queryhead = DBSession.query(Head).filter_by(id_operador=itemoperadores.id_persona).filter(Head.fecha<=ayer).filter(Head.fecha>=fechauno).all()
            for itemhead in queryhead:
                totaloperador = totaloperador + itemhead.pagado


        for itemunidades in queryunidades:
            queryhead = DBSession.query(Head).filter_by(id_unidad=itemunidades.id_unidad).filter(Head.fecha<=ayer).filter(Head.fecha>=fechauno).all()
            for itemhead in queryhead:
                total = total + itemhead.pagado


        condonacion = 0
        for itemunidades in queryunidades:
            querybody = DBSession.query(Body).filter_by(id_unidad=itemunidades.id_unidad).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).all()
            for itembody in querybody:
                condonacion = condonacion + itembody.condonacion
            #print("condonacion unidad==>",condonacion)
            querycondonaciones=DBSession.query(CondonacionUnidad).filter_by(unidad_id=itemunidades.id_unidad).all()
            for itemcondona in querycondonaciones:
                querycon=DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondona.condonacion_id).first()
                #condonacion=condonacion+querycon.monto
            queryunidadcargo=DBSession.query(UnidadCargo).filter_by(unidad_id=itemunidades.id_unidad).all()
            maxqueryunidades=len(queryunidadcargo)
            for itemunidadcargo in queryunidadcargo:
                querycargo=DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                querybody=DBSession.query(Body).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=itemunidades.id_unidad).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).all()
                totalbody=0
                condonacionbodycargo=0
                for itembody in querybody:
                    totalbody=totalbody+itembody.pagado
                    condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                #print("totalbody==>",totalbody)
                #print("condonacionbodycargo==>",condonacionbodycargo)
                if querycargo.cargo in comparacion:
                    posicion=comparacion.index(querycargo.cargo)
                    listatotales[posicion]=listatotales[posicion] + totalbody
                    listacondo[posicion]=listacondo[posicion]+condonacionbodycargo

                else:
                    comparacion.append(querycargo.cargo)
                    listatotales.append(totalbody)
                    listacondo.append(condonacionbodycargo)
                    posicion2=comparacion.index(querycargo.cargo)
                    contenido.append({'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

                if itemunidadcargo==queryunidadcargo[maxqueryunidades-1]:
                    querybodypoliza = DBSession.query(Body).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).filter_by(espoliza=1).filter_by(id_unidad=itemunidades.id_unidad).all()
                    totalbody = 0
                    condonacionbodycargo = 0
                    for itembody in querybodypoliza:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                    if 'POLIZA' in comparacion:
                        posicion = comparacion.index('POLIZA')
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                    else:
                        comparacion.append('POLIZA')
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index('POLIZA')
                        contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0,"condonacion": 0})



        condonacion = 0
        for itemoperadores in queryoperadores:
            querybody = DBSession.query(Body).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).filter_by(id_operador=itemoperadores.id_persona).all()
            for itembody in querybody:
                condonacion = condonacion + itembody.condonacion
            querycondonaciones=DBSession.query(CondonacionOperador).filter_by(operador_id=itemoperadores.id_persona).all()
            for itemcondona in querycondonaciones:
                querycon=DBSession.query(Condonaciones).filter_by(id_condonacion=itemcondona.condonacion_id).first()
                #condonacion=condonacion+querycon.monto
            queryoperadorcargo=DBSession.query(OperadorCargo).filter_by(operador_id=itemoperadores.id_persona).all()
            for itemoperadorcargo in queryoperadorcargo:
                querycargo=DBSession.query(CargoOperador).filter_by(id_cargooperador=itemoperadorcargo.cargo_id).first()
                querybody=DBSession.query(Body).filter(Body.fecha<=ayer).filter(Body.fecha>=fechauno).filter_by(id_cargopersona=querycargo.id_cargooperador).filter_by(id_operador=itemoperadores.id_persona).all()
                totalbody2=0
                condonacionbodycargo2=0
                for itembody in querybody:
                    totalbody2=totalbody2+itembody.pagado
                    condonacionbodycargo2=condonacionbodycargo2+itembody.condonacion


                if querycargo.cargo in comparacion:
                    posicion=comparacion.index(querycargo.cargo)
                    listatotales[posicion]=listatotales[posicion] + totalbody2
                    listacondo[posicion]=listacondo[posicion]+condonacionbodycargo2
                else:
                    comparacion.append(querycargo.cargo)
                    listatotales.append(totalbody2)
                    listacondo.append(condonacionbodycargo2)
                    posicion2=comparacion.index(querycargo.cargo)
                    contenido.append({'id_cuenta': querycargo.id_cargooperador, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})




        for item in comparacion:
            for item2 in contenido:
                if item2['nombre']==item:
                    querycuenta=DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(internal_id=internal_id).first()
                    totalquita = 0
                    if querycuenta is not None:
                        querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter(Salida.fecha<=ayer).filter(Salida.fecha>=fechauno).filter_by(cuentacargo_id=querycuenta.id_cuenta).all()
                        for itemsalida in querysalidas:
                            if itemsalida.app_name == kw['empresa']:
                                totalquita = totalquita + itemsalida.monto
                        querycuenta.total=listatotales[posicion] - totalquita
                        if kw['app_name']!='TODO':
                            pass
                            #querycuenta.app_name=kw['app_name']
                    posicion=comparacion.index(item)
                    item2['total']=listatotales[posicion] -totalquita
                    item2['condonacion']=listacondo[posicion]


        #contenido.append({'id_cuenta': '', "nombre": 'TOTAL', "total": total})
        json={"rows": contenido}
        return json


    @expose('json')
    def datajsondinamico(self,**kw):
        por=kw['por']
        conte=kw['conte']
        internal_id=kw['internal_id']
        application_id=kw['appID']
        contenido=[]
        fechainicial=kw['fechainicial']
        fechafinal=kw['fechafinal']
        comparacion=[]
        listatotales=[]
        listacondo = []
        total=0
        if por=='Empresa':
            queryempresa = DBSession.query(Linea).filter_by(nombre_linea=conte).filter_by(app_id=application_id).first()
            try:
                empp=queryempresa.id_linea
            except:
                empp=''
            queryunidades = DBSession.query(DatosUnidad).filter_by(id_linea=empp).all()

            for itemunidades in queryunidades:
                queryhead = DBSession.query(Head).filter_by(id_unidad=itemunidades.id_unidad).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).all()
                for itemhead in queryhead:
                    total = total + itemhead.pagado

            for itemunidades in queryunidades:
                queryunidadcargo = DBSession.query(UnidadCargo).filter_by(unidad_id=itemunidades.id_unidad).all()
                maxqueryunidades=len(queryunidadcargo)
                for itemunidadcargo in queryunidadcargo:
                    querycargo = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=itemunidades.id_unidad).all()
                    totalbody = 0
                    condonacionbodycargo = 0
                    for itembody in querybody:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append(
                            {'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion":0})

                    if itemunidadcargo == queryunidadcargo[maxqueryunidades - 1]:
                        querybodypoliza = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(espoliza=1).filter_by(
                            id_unidad=itemunidades.id_unidad).all()
                        totalbody = 0
                        condonacionbodycargo = 0
                        for itembody in querybodypoliza:
                            totalbody = totalbody + itembody.pagado
                            condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                        if 'POLIZA' in comparacion:
                            posicion = comparacion.index('POLIZA')
                            listatotales[posicion] = listatotales[posicion] + totalbody
                            listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                        else:
                            comparacion.append('POLIZA')
                            listatotales.append(totalbody)
                            listacondo.append(condonacionbodycargo)
                            posicion2 = comparacion.index('POLIZA')
                            contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0, "condonacion": 0})

            queryoperadores = DBSession.query(Persona).filter_by(application_id=kw['appID']).all()
            condonacion2 = 0
            for itemope in queryoperadores:
                querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_operador=itemope.id_persona).all()
                for itembody in querybody:
                    condonacion2 = condonacion2 + itembody.condonacion

            condonacion = 0
            for itemunidades in queryunidades:
                querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_unidad=itemunidades.id_unidad).all()
                for itembody in querybody:
                    condonacion = condonacion + itembody.condonacion

            #contenido.append({'id_cuenta': 0, "nombre": 'Condonacion Unidades', "total": condonacion})
            #contenido.append({'id_cuenta': 0, "nombre": 'Condonacion Operadores', "total": condonacion2})


            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        # querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(
                        #     internal_id=internal_id).first()
                        # totalquita = 0
                        # if querycuenta is not None:
                        #     querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(
                        #         cuentacargo_id=querycuenta.id_cuenta).all()
                        #     for itemsalida in querysalidas:
                        #         if itemsalida.app_name == conte:
                        #             totalquita = totalquita + itemsalida.monto
                        #     querycuenta.total = listatotales[posicion] - totalquita
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion'] = listacondo[posicion]



            try:
                empp=queryempresa.id_linea
            except:
                empp=''
            queryoperadores = DBSession.query(Persona).filter_by(id_linea=empp).all()

            for itemoperadores in queryoperadores:
                queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_operador=itemoperadores.id_persona).all()
                for itemhead in queryhead:
                    total = total + itemhead.pagado

            for itemoperadores in queryoperadores:
                queryoperadorcargo = DBSession.query(OperadorCargo).filter_by(operador_id=itemoperadores.id_persona).all()
                for itemoperadorcargo in queryoperadorcargo:
                    querycargo = DBSession.query(CargoOperador).filter_by(id_cargooperador=itemoperadorcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargopersona=querycargo.id_cargooperador).filter_by(id_operador=itemoperadores.id_persona).all()
                    totalbody = 0
                    condonacionbodycargo=0
                    for itembody in querybody:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion]=listacondo[posicion]+condonacionbodycargo
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append(
                            {'id_cuenta': querycargo.id_cargooperador, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        # querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(
                        #     internal_id=internal_id).first()
                        # totalquita = 0
                        # if querycuenta is not None:
                        #     querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(
                        #         cuentacargo_id=querycuenta.id_cuenta).all()
                        #     for itemsalida in querysalidas:
                        #         if itemsalida.app_name == conte:
                        #             totalquita = totalquita + itemsalida.monto
                        #     querycuenta.total = listatotales[posicion] - totalquita
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion']=listacondo[posicion]






        elif por=='Derrotero':
            queryderrotero = DBSession.query(Derrotero).filter_by(derrotero=conte).filter_by(application_id=application_id).first()
            try:
                eldderr=queryderrotero.id_derrotero
            except:
                eldderr=''
            queryunidades = DBSession.query(DatosUnidad).filter_by(id_derrotero=eldderr).filter_by(habilitado=1).all()

            for itemunidades in queryunidades:
                queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_unidad=itemunidades.id_unidad).all()
                for itemhead in queryhead:
                    total = total + itemhead.pagado


            for itemunidades in queryunidades:
                queryunidadcargo = DBSession.query(UnidadCargo).filter_by(unidad_id=itemunidades.id_unidad).all()
                maxqueryunidades = len(queryunidadcargo)
                for itemunidadcargo in queryunidadcargo:
                    querycargo = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=itemunidades.id_unidad).all()
                    totalbody = 0
                    condonacionbodycargo=0
                    for itembody in querybody:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion]=listacondo[posicion]+condonacionbodycargo
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append({'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

                    if itemunidadcargo == queryunidadcargo[maxqueryunidades - 1]:
                        querybodypoliza = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(espoliza=1).filter_by(
                            id_unidad=itemunidades.id_unidad).all()
                        totalbody = 0
                        condonacionbodycargo = 0
                        for itembody in querybodypoliza:
                            totalbody = totalbody + itembody.pagado
                            condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                        if 'POLIZA' in comparacion:
                            posicion = comparacion.index('POLIZA')
                            listatotales[posicion] = listatotales[posicion] + totalbody
                            listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                        else:
                            comparacion.append('POLIZA')
                            listatotales.append(totalbody)
                            listacondo.append(condonacionbodycargo)
                            posicion2 = comparacion.index('POLIZA')
                            contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0, "condonacion": 0})

            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        # querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(
                        #     internal_id=internal_id).first()
                        # totalquita = 0
                        # if querycuenta is not None:
                        #     querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(
                        #         cuentacargo_id=querycuenta.id_cuenta).all()
                        #     for itemsalida in querysalidas:
                        #         if itemsalida.app_name == conte:
                        #             totalquita = totalquita + itemsalida.monto
                        #     querycuenta.total = listatotales[posicion] - totalquita
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion']=listacondo[posicion]

        elif por=='Grupo':
            querygrupo = DBSession.query(Grupo).filter_by(grupo=conte).filter_by(application_id=application_id).first()
            try:
                queryunidades = DBSession.query(DatosUnidad).filter_by(id_grupo=querygrupo.id_grupo).filter_by(habilitado=1).all()
            except:
                queryunidades={}

            for itemunidades in queryunidades:
                queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_unidad=itemunidades.id_unidad).all()
                for itemhead in queryhead:
                    total = total + itemhead.pagado

            for itemunidades in queryunidades:
                queryunidadcargo = DBSession.query(UnidadCargo).filter_by(unidad_id=itemunidades.id_unidad).all()
                maxqueryunidades=len(queryunidadcargo)
                for itemunidadcargo in queryunidadcargo:
                    querycargo = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=itemunidades.id_unidad).all()
                    totalbody = 0
                    condonacionbodycargo=0
                    for itembody in querybody:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion]=listacondo[posicion]+condonacionbodycargo
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append({'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

                    if itemunidadcargo == queryunidadcargo[maxqueryunidades - 1]:
                        querybodypoliza = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(espoliza=1).filter_by(
                            id_unidad=itemunidades.id_unidad).all()
                        totalbody = 0
                        condonacionbodycargo = 0
                        for itembody in querybodypoliza:
                            totalbody = totalbody + itembody.pagado
                            condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                        if 'POLIZA' in comparacion:
                            posicion = comparacion.index('POLIZA')
                            listatotales[posicion] = listatotales[posicion] + totalbody
                            listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                        else:
                            comparacion.append('POLIZA')
                            listatotales.append(totalbody)
                            listacondo.append(condonacionbodycargo)
                            posicion2 = comparacion.index('POLIZA')
                            contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0, "condonacion": 0})

            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        # querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(
                        #     internal_id=internal_id).first()
                        # totalquita = 0
                        # if querycuenta is not None:
                        #     querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(
                        #         cuentacargo_id=querycuenta.id_cuenta).all()
                        #     for itemsalida in querysalidas:
                        #         if itemsalida.app_name == conte:
                        #             totalquita = totalquita + itemsalida.monto
                        #     querycuenta.total = listatotales[posicion] - totalquita
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion']=listacondo[posicion]


        elif por=='Unidad':
            queryunidad = DBSession.query(DatosUnidad).filter_by(app_id=kw['appID']).filter_by(eco1=conte).first()
            try:
                iduni=queryunidad.id_unidad
            except:
                iduni=''
            queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_unidad=iduni).all()
            for itemhead in queryhead:
                total = total + itemhead.pagado


            queryunidadcargo = DBSession.query(UnidadCargo).filter_by(unidad_id=iduni).all()
            maxqueryunidades=len(queryunidadcargo)
            for itemunidadcargo in queryunidadcargo:
                querycargo = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=queryunidad.id_unidad).all()
                totalbody = 0
                condonacionbodycargo=0
                for itembody in querybody:
                    totalbody = totalbody + itembody.pagado
                    condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                if querycargo.cargo in comparacion:
                    posicion = comparacion.index(querycargo.cargo)
                    listatotales[posicion] = listatotales[posicion] + totalbody
                    listacondo[posicion]=listacondo[posicion]+condonacionbodycargo
                else:
                    comparacion.append(querycargo.cargo)
                    listatotales.append(totalbody)
                    listacondo.append(condonacionbodycargo)
                    posicion2 = comparacion.index(querycargo.cargo)
                    contenido.append({'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

                if itemunidadcargo == queryunidadcargo[maxqueryunidades - 1]:
                    querybodypoliza = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(espoliza=1).filter_by(
                        id_unidad=iduni).all()
                    totalbody = 0
                    condonacionbodycargo = 0
                    for itembody in querybodypoliza:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                    if 'POLIZA' in comparacion:
                        posicion = comparacion.index('POLIZA')
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                    else:
                        comparacion.append('POLIZA')
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index('POLIZA')
                        contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0, "condonacion": 0})

            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion']=listacondo[posicion]

        elif por=='Operador':
            queryoperador = DBSession.query(Persona).filter_by(application_id=kw['appID']).filter_by(nombre=conte).first()

            queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_operador=queryoperador.id_persona).all()
            for itemhead in queryhead:
                total = total + itemhead.pagado

            queryoperadorcargo = DBSession.query(OperadorCargo).filter_by(operador_id=queryoperador.id_persona).all()
            for itemoperadorcargo in queryoperadorcargo:
                querycargo = DBSession.query(CargoOperador).filter_by(id_cargooperador=itemoperadorcargo.cargo_id).first()
                querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargopersona=querycargo.id_cargooperador).filter_by(id_operador=queryoperador.id_persona).all()
                totalbody = 0
                condonacionbodycargo=0
                for itembody in querybody:
                    totalbody = totalbody + itembody.pagado
                    condonacionbodycargo=condonacionbodycargo+itembody.condonacion

                if querycargo.cargo in comparacion:
                    posicion = comparacion.index(querycargo.cargo)
                    listatotales[posicion] = listatotales[posicion] + totalbody
                    listacondo[posicion]=listacondo[posicion]+condonacionbodycargo
                else:
                    comparacion.append(querycargo.cargo)
                    listatotales.append(totalbody)
                    listacondo.append(condonacionbodycargo)
                    posicion2 = comparacion.index(querycargo.cargo)
                    contenido.append({'id_cuenta': querycargo.id_cargooperador, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        posicion = comparacion.index(item)
                        item2['total'] = listatotales[posicion]
                        item2['condonacion']=listacondo[posicion]

        elif por=='App_name':

            queryunidades = DBSession.query(DatosUnidad).filter_by(app_id=kw['appID']).filter_by(app_name=conte).all()##APP_NAME PUEDE CAMBIAR
            queryoperadores = DBSession.query(Persona).filter_by(application_id=kw['appID']).all()

            for itemunidades in queryunidades:
                queryhead = DBSession.query(Head).filter(Head.fecha>=fechainicial).filter(Head.fecha<=fechafinal).filter_by(id_unidad=itemunidades.id_unidad).all()
                for itemhead in queryhead:
                    total = total + itemhead.pagado

            for itemunidades in queryunidades:
                queryunidadcargo = DBSession.query(UnidadCargo).filter_by(unidad_id=itemunidades.id_unidad).all()
                maxqueryunidades=len(queryunidadcargo)
                for itemunidadcargo in queryunidadcargo:
                    querycargo = DBSession.query(CargoUnidad).filter_by(id_cargounidad=itemunidadcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargounidad=querycargo.id_cargounidad).filter_by(id_unidad=itemunidades.id_unidad).all()
                    totalbody = 0
                    condonacionbodycargo=0
                    for itembody in querybody:
                        totalbody = totalbody + itembody.pagado
                        condonacionbodycargo=condonacionbodycargo+itembody.condonacion
                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody
                        listacondo[posicion]=listacondo[posicion] + condonacionbodycargo
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody)
                        listacondo.append(condonacionbodycargo)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append(
                            {'id_cuenta': querycargo.id_cargounidad, "nombre": querycargo.cargo, "total": 0,"condonacion": 0})

                    if itemunidadcargo == queryunidadcargo[maxqueryunidades - 1]:
                        querybodypoliza = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(espoliza=1).filter_by(
                            id_unidad=itemunidades.id_unidad).all()
                        totalbody = 0
                        condonacionbodycargo = 0
                        for itembody in querybodypoliza:
                            totalbody = totalbody + itembody.pagado
                            condonacionbodycargo = condonacionbodycargo + itembody.condonacion

                        if 'POLIZA' in comparacion:
                            posicion = comparacion.index('POLIZA')
                            listatotales[posicion] = listatotales[posicion] + totalbody
                            listacondo[posicion] = listacondo[posicion] + condonacionbodycargo
                        else:
                            comparacion.append('POLIZA')
                            listatotales.append(totalbody)
                            listacondo.append(condonacionbodycargo)
                            posicion2 = comparacion.index('POLIZA')
                            contenido.append({'id_cuenta': 0, "nombre": 'POLIZA', "total": 0, "condonacion": 0})

            condonacion = 0
            for itemoperadores in queryoperadores:
                querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_operador=itemoperadores.id_persona).all()
                for itembody in querybody:
                    condonacion = condonacion + itembody.condonacion
                querycondonaciones = DBSession.query(CondonacionOperador).filter_by(
                    operador_id=itemoperadores.id_persona).all()
                for itemcondona in querycondonaciones:
                    querycon = DBSession.query(Condonaciones).filter_by(
                        id_condonacion=itemcondona.condonacion_id).first()
                    # condonacion=condonacion+querycon.monto
                queryoperadorcargo = DBSession.query(OperadorCargo).filter_by(
                    operador_id=itemoperadores.id_persona).all()
                for itemoperadorcargo in queryoperadorcargo:
                    querycargo = DBSession.query(CargoOperador).filter_by(
                        id_cargooperador=itemoperadorcargo.cargo_id).first()
                    querybody = DBSession.query(Body).filter(Body.fecha>=fechainicial).filter(Body.fecha<=fechafinal).filter_by(id_cargopersona=querycargo.id_cargooperador).filter_by(
                        id_operador=itemoperadores.id_persona).all()
                    totalbody2 = 0
                    condonacionbodycargo2 = 0
                    for itembody in querybody:
                        totalbody2 = totalbody2 + itembody.pagado
                        condonacionbodycargo2 = condonacionbodycargo2 + itembody.condonacion

                    if querycargo.cargo in comparacion:
                        posicion = comparacion.index(querycargo.cargo)
                        listatotales[posicion] = listatotales[posicion] + totalbody2
                        listacondo[posicion] = listacondo[posicion] + condonacionbodycargo2
                    else:
                        comparacion.append(querycargo.cargo)
                        listatotales.append(totalbody2)
                        listacondo.append(condonacionbodycargo2)
                        posicion2 = comparacion.index(querycargo.cargo)
                        contenido.append(
                            {'id_cuenta': querycargo.id_cargooperador, "nombre": querycargo.cargo, "total": 0,
                             "condonacion": 0})





            for item in comparacion:
                for item2 in contenido:
                    if item2['nombre'] == item:
                        posicion = comparacion.index(item)
                        querycuenta = DBSession.query(CuentaCargo).filter_by(nombre=item).filter_by(
                            application_id=kw['appID']).first()
                        totalquita = 0
                        if querycuenta is not None:
                            querysalidas = DBSession.query(Salida).filter_by(cancelacion=0).filter_by(
                                cuentacargo_id=querycuenta.id_cuenta).filter(Salida.fecha>=fechainicial).filter(Salida.fecha<=fechafinal).all()
                            for itemsalida in querysalidas:
                                if itemsalida.app_name == conte:
                                    totalquita = totalquita + itemsalida.monto
                            querycuenta.total = listatotales[posicion] - totalquita
                        item2['total'] = listatotales[posicion] - totalquita
                        item2['condonacion'] = listacondo[posicion]


        json={'rows': contenido}
        return json