# -*- coding: utf-8 -*-
"""Sample controller with all its actions protected."""
from tg import expose, flash
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.predicates import has_permission
from tg import predicates,url

from pythonjupiter.lib.base import BaseController

__all__ = ['SecureController']


class SecureController(BaseController):
    """Sample controller-wide authorization"""

    # The predicate that must be met for all the actions in this controller:
    allow_only = predicates.not_anonymous()

    @expose('pythonearth.templates.index2')
    def index(self):
        """Let the user know that's visiting a protected controller."""
        return dict(page='index2')

    @expose('pythonearth.templates.index')
    def some_where(self):
        """Let the user know that this action is protected too."""
        return dict(page='some_where')
