# -*- coding: utf-8 -*-
"""Catalog controller"""
from tg import request, expose, redirect
from pythonjupiter.lib.base import BaseController
from pythonjupiter.lib.CRUD import Crud
from pythonjupiter.lib.create import Create
from pythonjupiter.lib.app_globals import Globals
from pythonjupiter.model import DBSession, Dictionary
from pythonjupiter.lib.messagequeue import Message
#from pythonjupiter.model.tables import Type, Hierarchy, PhoneType
__all__ = ['CatalogController']


class CatalogController(BaseController):

    def __init__(self,currentmodel,key,kwargs,filter,title,pwd):
        self.kw = kwargs
        app = Globals()
        self.kw['url'] = app.host
        self.model = currentmodel
        self.indexkey = key
        self.kw['title'] = title
        self.kw['indexkey'] = key
        self.kw['pwd'] = pwd
        #print("LOS KW DE CATALOG: ",self.kw)

    @expose('pythonpluton.templates.showData')
    #@require(predicates.has_permission('Read System Users', msg=l_('You dont have this access')))
    #@require(predicates.Not(predicates.has_permission('Expired', msg=l_('You account has expired'))))
    def show(self):
        column = []
        for c in self.model.__table__.columns:
            handler = DBSession.query(Dictionary).filter_by(database=str(c.name)).first()
            if handler is not None:
                if handler.visible != False:
                    if (c.name != 'internal_id'):
                        column.append({'show':handler.name,'db':c.name})
            else:
                if(c.name!='internal_id'):
                    column.append({'show': c.name, 'db': c.name})

        app_name=self.kw['app_name']
        try:
            if self.kw['perm'] == '1':
                try:
                    print("perm=1 try tartare de hacer filterApp")
                    hd = self.model.filterApp(self.kw['filter'], self.kw['internal_id'], self.kw['application_id'])
                    print("forma perm=1 y filterApp")
                except:
                    print("except perm=1 no hice filterApp hare filter")
                    hd = self.model.filter(self.kw['filter'], self.kw['internal_id'])
                    print("forma perm=1 y filter ")
            else:
                try:
                    print("perm!=1 try tratare de hacer filter")
                    hd = self.model.filter(self.kw['filter'], self.kw['internal_id'])#cambie a app_id
                    print("forma perm!=1 y filter")
                except:
                    print("except perm!=1 no hice filter tratare de hacer filterApp")
                    if app_name!='TODO':
                        hd = self.model.filterApp(self.kw['filter'], self.kw['application_id'])
                        print("forma perm!=1 y filterApp")
                    else:
                        hd = self.model.filterInternal(self.kw['filter'], self.kw['internal_id'])


            reload = "block"
        except:
            if self.kw['perm'] == '1':
                print("perm=1 no pude hacer ningun filter tratare de hacer all")
                hd = self.model.all(self.kw['internal_id'])
                print("forma perm=1 y all")
            else:
                try:
                    print("perm!=1 tratare de hacer allApp")
                    if app_name!='TODO':
                        hd = self.model.allApp(self.kw['application_id'])
                        print("forma perm!=1 y allApp")
                    else:
                        hd = self.model.allInternal(self.kw['internal_id'])
                except:
                    print("except perm!=1 no puede hacer allApp hare all")
                    hd = self.model.all(self.kw['internal_id']) #cambie a app_id
                    print("forma perm!=1 y all")
            reload = "none"

        #print("hd tieeneee: ",hd)

        print("reload tiene= ",reload)
        contador=len(hd)
        return dict(page='showData',column=column,hd=hd, reload=reload, kw=self.kw, title=self.kw['title'].replace(' ', ''),user=self.kw['user'],app_name=self.kw['app_name'],internal_id=self.kw['internal_id'],
                    application_id=self.kw['application_id'],contador=contador)


    @expose()
    #https://pluto.dudewhereismy.mx/cat/typeForm?op=add&internal_id=1
    def Form(self):
        filter = []
        error = Create(self.model,self.indexkey, filter, self.kw).formFieldSet(self.kw['url']+'/'+self.kw['pwd']+'?action=new&perm='+self.kw['perm']+'&application_id='+self.kw['application_id'])
        return error

    @expose()
    def Save(self):
        #print(self.kw)
        print("----------ESTOY EN SAVEEEE--------")
        error = Crud(self.model,self.indexkey, filter, self.kw).update()
        #print("ERROR ",error)
        if(self.kw['op']=='add'):
            #print("LOS KW DE CATALOG 2: ",self.kw)
            if(error['error']=='agregue'):
                Message.post("testlistener_" + self.kw['user'], 'MSG_' + 'GREEN' + "|" + ""+self.kw['title']+" insertado con exito")
                return ""
            else:
                Message.post("testlistener_" + self.kw['user'],'MSG_' + 'RED' + "|" + ""  + self.kw['title'] + " no se inserto ya existe")
                return ""

        if(self.kw['op']=='edit'):
            if(error['error']=='disponible'):
                Message.post("testlistener_" + self.kw['user'],'MSG_' + 'GREEN' + "|" + "" + self.kw['title'] + " se edito con exito")
                return ""
            else:
                Message.post("testlistener_" + self.kw['user'],'MSG_' + 'RED' + "|" + "" + self.kw['title'] + " no se edito ya existe")
                return ""

        if(self.kw['op']=='del'):
            if(error['error']=='ok'):
                return "" + self.kw['title'] + " se elimino con exito"
            else:
                return error['error']
