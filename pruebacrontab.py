from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from pythonjupiter.model.tables import PruebasCron,CargoUnidad,CargoOperador,UnidadCargo,DatosUnidad,Poliza
from pythonjupiter.model.tables import CondonacionUnidad,Condonaciones,Body,OperadorCargo,Persona,CondonacionOperador
from pythonjupiter.model.tables import Head
from datetime import datetime,date,timedelta
import time

some_engine = create_engine('mysql://gpscontrol:qazwsxedc@127.0.0.1/jupiter')
Session = sessionmaker(bind=some_engine)
session = Session()

Base = declarative_base()

querycargosunidades = session.query(CargoUnidad).filter_by(tipodecargo=1).all()
querycargosoperadores = session.query(CargoOperador).filter_by(tipodecargo=1).all()



queryunidades=session.query(DatosUnidad).filter_by(habilitado=1).all()
for itemunits in queryunidades:
    newheadnew = Head()
    newheadnew.id_unidad = itemunits.id_unidad
    newheadnew.fecha = datetime.now().date()
    newheadnew.app_name = itemunits.app_name
    newheadnew.application_id = itemunits.app_id
    newheadnew.internal_id = itemunits.internal_id
    print("eco: ", itemunits.eco1)
    print("internal: ", itemunits.internal_id)
    print("application_id: ", itemunits.app_id)
    session.add(newheadnew)
    session.commit()

queryoperadores=session.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).all()
for itemoperato in queryoperadores:
    newheadnew = Head()
    newheadnew.id_operador = itemoperato.id_persona
    newheadnew.fecha = datetime.now().date()
    newheadnew.app_name = itemoperato.app_name
    newheadnew.application_id = itemoperato.application_id
    newheadnew.internal_id = itemoperato.internal_id
    print("name: ", itemoperato.nombre)
    print("internal: ", itemoperato.internal_id)
    print("application_id: ", itemoperato.application_id)
    session.add(newheadnew)
    session.commit()



for itemcu in querycargosunidades:
    querytablados = session.query(UnidadCargo).filter_by(cargo_id=itemcu.id_cargounidad).all()
    frecuencia = itemcu.frecuencia.split(",")
    prueba = (datetime.now().date()).strftime('%A')
    if prueba == 'Monday':
        prueba = 'Lunes'
    if prueba == 'Tuesday':
        prueba = 'Martes'
    if prueba == 'Wednesday':
        prueba = 'Miercoles'
    if prueba == 'Thursday':
        prueba = 'Jueves'
    if prueba == 'Friday':
        prueba = 'Viernes'
    if prueba == 'Saturday':
        prueba = 'Sabado'
    if prueba == 'Sunday':
        prueba = 'Domingo'
    if prueba in frecuencia:
        for itemuc in querytablados:
            queryunidad = session.query(DatosUnidad).filter_by(habilitado=1).filter_by(id_unidad=itemuc.unidad_id).first()
            if queryunidad is not None:
                querycondonacionesunidad = session.query(CondonacionUnidad).filter_by(unidad_id=queryunidad.id_unidad).filter_by(cargounidad_id=itemcu.id_cargounidad).all()
                if querycondonacionesunidad != []:
                    condonacion = 0
                    for itemcondo in querycondonacionesunidad:
                        querycondonacion = session.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                        .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                        if querycondonacion is not None:
                            condonacion = condonacion + querycondonacion.monto
                        else:
                            condonacion = condonacion
                else:
                    condonacion = 0

                if itemuc.monto > 0:
                    querythehead=session.query(Head).filter_by(id_unidad=queryunidad.id_unidad).filter_by(fecha=datetime.now().date()).first()
                    newbody = Body()
                    newbody.monto = itemuc.monto
                    if condonacion > newbody.monto:
                        newbody.condonacion = 0
                    else:
                        newbody.condonacion = condonacion
                    newbody.saldo = itemuc.monto - newbody.condonacion
                    newbody.pagado = 0
                    newbody.fecha = datetime.now().date()
                    newbody.id_unidad = queryunidad.id_unidad
                    newbody.id_cargounidad = itemcu.id_cargounidad
                    newbody.internal_id = itemcu.internal_id
                    newbody.id_head=querythehead.id_head
                    newbody.application_id=querythehead.application_id
                    session.add(newbody)
                    session.commit()
                    time.sleep(1)


querypoliza=session.query(Poliza).all()
for itempoliza in querypoliza:
    if datetime.now().date() <=itempoliza.fecha_fin and datetime.now().date()>=itempoliza.fecha_inicio:
        querythehead = session.query(Head).filter_by(id_unidad=itempoliza.id_unidad).filter_by(fecha=datetime.now().date()).first()
        newbody = Body()
        newbody.monto = itempoliza.montodividido
        newbody.condonacion = 0
        newbody.saldo = itempoliza.montodividido - newbody.condonacion
        newbody.pagado = 0
        newbody.fecha = datetime.now().date()
        newbody.id_unidad = itempoliza.id_unidad
        newbody.internal_id = itempoliza.internal_id
        if querythehead is not None:
            newbody.id_head = querythehead.id_head
            newbody.application_id = querythehead.application_id
        else:
            queryunidaduu = session.query(DatosUnidad).filter_by(habilitado=1).filter_by(id_unidad=itempoliza.id_unidad).first()
            if queryunidaduu is not None:
                newheadnew=Head()
                newheadnew.id_unidad=queryunidaduu.id_unidad
                newheadnew.fecha=datetime.now().date()
                newheadnew.app_name=queryunidad.app_name
                newheadnew.application_id=queryunidad.app_id
                newheadnew.internal_id=queryunidad.internal_id
                session.add(newheadnew)
                session.commit()
            querythehead = session.query(Head).filter_by(id_unidad=itempoliza.id_unidad).filter_by(fecha=datetime.now().date()).first()
            newbody.id_head = querythehead.id_head
            newbody.application_id = querythehead.application_id


        newbody.espoliza=1
        session.add(newbody)
        session.commit()
        time.sleep(1)


for itemco in querycargosoperadores:
    querytablados=session.query(OperadorCargo).filter_by(cargo_id=itemco.id_cargooperador).all()
    frecuencia=itemco.frecuencia.split(",")
    prueba=(datetime.now().date()).strftime('%A')
#
    if prueba=='Monday':
        prueba='Lunes'
    if prueba == 'Tuesday':
        prueba = 'Martes'
    if prueba == 'Wednesday':
        prueba = 'Miercoles'
    if prueba == 'Thursday':
        prueba = 'Jueves'
    if prueba == 'Friday':
        prueba = 'Viernes'
    if prueba == 'Saturday':
        prueba = 'Sabado'
    if prueba == 'Sunday':
        prueba = 'Domingo'
        #
    if prueba in frecuencia:
        for itemoc in querytablados:
                querypersona=session.query(Persona).filter_by(habilitadooperador=1).filter_by(id_persona=itemoc.operador_id).first()

                querycondonacioneoperadores = session.query(CondonacionOperador).filter_by(operador_id=querypersona.id_persona).filter_by(cargooperador_id=itemco.id_cargooperador).all()

                if querycondonacioneoperadores!=[]:
                    condonacion=0
                    for itemcondo in querycondonacioneoperadores:
                        querycondonacion = session.query(Condonaciones).filter_by(id_condonacion=itemcondo.condonacion_id) \
                        .filter_by(cancelado=0).filter(Condonaciones.fechainicial <= datetime.now().date()).filter(Condonaciones.fechafinal >= datetime.now().date()).first()
                        if querycondonacion is not None:
                            condonacion = condonacion + querycondonacion.monto
                        else:
                            condonacion=condonacion
                else:
                    condonacion=0

                if itemoc.monto > 0:
                    querythehead = session.query(Head).filter_by(id_operador=querypersona.id_persona).filter_by(fecha=datetime.now().date()).first()
                    newbody=Body()
                    newbody.monto=itemoc.monto
                    if condonacion>newbody.monto:
                        newbody.condonacion=0
                    else:
                        newbody.condonacion=condonacion
                    newbody.saldo=itemoc.monto-newbody.condonacion
                    newbody.pagado = 0
                    newbody.fecha=datetime.now().date()
                    newbody.id_operador=querypersona.id_persona
                    newbody.id_cargopersona=itemco.id_cargooperador
                    newbody.internal_id = itemco.internal_id
                    newbody.id_head = querythehead.id_head
                    newbody.application_id = querythehead.application_id
                    session.add(newbody)
                    session.commit()
                    time.sleep(1)

queryunidades=session.query(DatosUnidad).filter_by(habilitado=1).all()

queryoperadores=session.query(Persona).filter_by(esoperador=1).filter_by(habilitadooperador=1).all()

for itemunidad in queryunidades:
    saldo = 0
    querybody=session.query(Body).filter_by(id_unidad=itemunidad.id_unidad).filter_by(fecha=datetime.now().date()).all()
    for itembody in querybody:
        #print("itembody.saldo=",itembody.saldo)
        saldo=saldo+itembody.saldo
#
    querythehead = session.query(Head).filter_by(id_unidad=itemunidad.id_unidad).filter_by(fecha=datetime.now().date()).first()
    if saldo > 0:

        #newhead=Head()
        #newhead.fecha=datetime.now().date()
        querythehead.estatus='Por Pagar'
        querythehead.porpagar=saldo
        querythehead.pagado=0
        querythehead.faltante=saldo
        #newhead.id_unidad=itemunidad.id_unidad
        # thequeryoperador = session.query(Persona).filter_by(id_unidad=itemunidad.id_unidad).first()
        # try:
        #     variable = thequeryoperador.id_persona
        # except:
        #     variable = None
        # newhead.id_operador = variable
        #newhead.internal_id = itemunidad.internal_id
        #newhead.app_name=itemunidad.app_name
        #session.add(newhead)
        session.commit()
    else:
        if querythehead is not None:
            session.delete(querythehead)
            session.commit()

        #
for itemoperador in queryoperadores:
    saldo=0
    querybody=session.query(Body).filter_by(id_operador=itemoperador.id_persona).filter_by(fecha=datetime.now().date()).all()
    for itembody in querybody:
        saldo=saldo+itembody.saldo

    querythehead = session.query(Head).filter_by(id_operador=itemoperador.id_persona).filter_by(fecha=datetime.now().date()).first()
    if saldo > 0:
        #newhead=Head()
        #newhead.fecha=datetime.now().date()
        querythehead.estatus='Por Pagar'
        querythehead.porpagar=saldo
        querythehead.pagado=0
        querythehead.faltante=saldo
        #newhead.id_operador=itemoperador.id_persona
        #newhead.internal_id=itemoperador.internal_id
        #newhead.app_name = itemoperador.app_name
        #session.add(newhead)
        session.commit()
    else:
        if querythehead is not None:
            session.delete(querythehead)
            session.commit()


session.flush()
