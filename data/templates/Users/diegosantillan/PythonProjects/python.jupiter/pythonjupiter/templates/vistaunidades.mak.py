# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1529509249.924787
_enable_loop = True
_template_filename = '/Users/diegosantillan/PythonProjects/python.jupiter/pythonjupiter/templates/vistaunidades.mak'
_template_uri = '/Users/diegosantillan/PythonProjects/python.jupiter/pythonjupiter/templates/vistaunidades.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _ = context.get('_', UNDEFINED)
        allrecords = context.get('allrecords', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('<!DOCTYPE html>\n\n<html lang="en">\n<head>\n<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n  <script src="//cdn.jsdelivr.net/free-jqgrid/4.8.0/js/i18n/grid.locale-es.js"></script>\n  <script src="//cdn.jsdelivr.net/free-jqgrid/4.8.0/js/jquery.jqgrid.min.js"></script>\n  <link rel="stylesheet" href="//cdn.jsdelivr.net/free-jqgrid/4.8.0/css/ui.jqgrid.css">\n\n  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">\n  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">\n  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>\n\n  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>\n  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/redmond/jquery-ui.css" type="text/css"/>\n    <meta charset="utf-8" />\n    <title>jqGrid Loading Data - Million Rows from a REST service</title>\n</head>\n<body>\n    <table style="width:100%;overflow:auto;">\n    <table id="jqGridTable" class="scroll" cellpadding="0" cellspacing="0"></table>\n    <div id="listPagerTables" class="scroll" style="text-align:center;"></div>\n    <div id="listPsetcols" class="scroll" style="text-align:center;"></div>\n    </table>\n<script type="text/javascript">\n')
        __M_writer('\n    </script>\n    \n<table class="table table-hover" style="width: 500px;text-align: center">\n    \t<thead>\n\t\t<tr>\n            <th></th>\n\t\t\t<th>')
        __M_writer(escape(_('VIN')))
        __M_writer('</th>\n\t\t\t<th>')
        __M_writer(escape(_('MODEL')))
        __M_writer('</th>\n\t\t\t<th>')
        __M_writer(escape(_('YEAR')))
        __M_writer('</th>\n\n\t\t</tr>\n\t\t</thead>\n')
        for item in allrecords:
            __M_writer('\n    <tr>\n        <td class="col_0"><span class="glyphicon glyphicon-pencil"></span></td>\n         <td>')
            __M_writer(escape(item.vin))
            __M_writer('</td>\n        <td>')
            __M_writer(escape(item.modelo))
            __M_writer('</td>\n         <td>')
            __M_writer(escape(item.anio))
            __M_writer('</td>\n\n')
            __M_writer('\n\n    </tr>\n')
        __M_writer('\n\n</table>\n\n</body>\n</html>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "filename": "/Users/diegosantillan/PythonProjects/python.jupiter/pythonjupiter/templates/vistaunidades.mak", "uri": "/Users/diegosantillan/PythonProjects/python.jupiter/pythonjupiter/templates/vistaunidades.mak", "line_map": {"32": 107, "33": 108, "34": 111, "35": 111, "36": 112, "37": 112, "38": 113, "39": 113, "40": 122, "41": 126, "47": 41, "17": 0, "24": 1, "25": 94, "26": 101, "27": 101, "28": 102, "29": 102, "30": 103, "31": 103}}
__M_END_METADATA
"""
